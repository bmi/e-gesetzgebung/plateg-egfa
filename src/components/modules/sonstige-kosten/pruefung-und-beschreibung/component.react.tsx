// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Radio, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import { BASE_PATH, EgfaModuleEntityRequestDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (
    form?: FormInstance,
    status?: EgfaModuleStatusType,
    isDraftSaving?: boolean,
  ) => (successCallback?: () => void) => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
}
export function SimpleModuleSonstigeKostenBeschreibung(
  props: SimpleModuleProps<EgfaModuleEntityRequestDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [answer, setAnswer] = useState<boolean>();
  const [isDirty, setIsDirty] = useState(false);

  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>(`/egfa/:id/${routes.MODULE}/:moduleName/:pageName`);
  const MODULE_LINK = `#/egfa/${routeMatcherVorbereitung?.params?.id as string}/module`;

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      setAnswer(props.formData.influenceExpected);
      setIsDirty(false);
    }
  }, [props.formData]);

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.EINLEITUNG}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.FERTIGSTELLUNG}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) {
            props.handleFormChange();
          }
        }}
      >
        <fieldset className="fieldset-form-items">
          <legend className="seo">{t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.question`)}</legend>
          <Form.Item
            name={'influenceExpected'}
            label={
              <span
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.question`, {
                    link: BASE_PATH + '/arbeitshilfen/download/34#page=35',
                  }),
                }}
              />
            }
            rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
          >
            <Radio.Group
              className="horizontal-radios"
              onChange={(event) => setAnswer(event?.target?.value as boolean)}
              name={'influenceExpected'}
            >
              <Radio id="egfa-auswirkungen-influenceExpectedJa-radio" className="horizontal-radios" value={true}>
                Ja
              </Radio>
              <Radio id="egfa-auswirkungen-influenceExpectedNein-radio" className="horizontal-radios" value={false}>
                Nein
              </Radio>
            </Radio.Group>
          </Form.Item>
        </fieldset>
        {answer === false && <p>{t(`egfa.modules.${props.moduleName}.fertigstellung.noImpact`)}</p>}
        {answer && (
          <FormItemWithInfo
            name={'influenceSummary'}
            label={
              <span>
                {t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.additionalExplanation`)}
                <InfoComponent
                  title={t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.additionalExplanation`)}
                  withLabelRequired
                >
                  <div
                    dangerouslySetInnerHTML={{
                      __html: t(`egfa.modules.${props.moduleName}.pruefungSonstigeKosten.drawerText`, {
                        link44GGO: BASE_PATH + '/arbeitshilfen/download/34#page=35',
                        auswirkungOHH: `${MODULE_LINK}/einnahmenAusgaben`,
                        erfuellungsaufwand: `${MODULE_LINK}/erfuellungsaufwand`,
                        auswirkungEinzelpreise: `${MODULE_LINK}/preise`,
                        auswirkungKMU: `${MODULE_LINK}/kmu`,
                      }),
                    }}
                  ></div>
                </InfoComponent>
              </span>
            }
            rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
          >
            <TextArea rows={8} />
          </FormItemWithInfo>
        )}
      </FormWrapper>
    </div>
  );
}
