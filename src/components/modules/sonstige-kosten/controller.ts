// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleType,
  EgfaModuleWeitereEntityDTOAllOf,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportSonstigeKostenToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleSonstigeKostenControllerApi>(
    'egfaModuleSonstigeKostenControllerApi',
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule2({ egfaId }).subscribe({
    next: (sonstigeKostenData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.SonstigeKosten,
          egfaDatenuebernahmeDTO: generateSonstigeKostenJSON(sonstigeKostenData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Sonstige Kosten data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Sonstige Kosten data', error);
    },
  });
};
export const generateSonstigeKostenJSON = (
  sonstigeKostenData: EgfaModuleEntityRequestDTO & EgfaModuleWeitereEntityDTOAllOf,
): EgfaDatenuebernahmeDTO => {
  const sonstigeKostenJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'sonstigeKostenErgebnisdokumentation',
    title: i18n.t(`egfa.modules.sonstigeKosten.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  if (sonstigeKostenData.influenceExpected && sonstigeKostenData.influenceSummary) {
    sonstigeKostenJSONExport.content.push({
      id: 'sonstigeKostenSummary',
      type: EgfaItemType.Text,
      data: sonstigeKostenData.influenceSummary,
    });
  } else {
    sonstigeKostenJSONExport.content.push({
      id: 'sonstigeKostenSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.sonstigeKosten.fertigstellung.noImpact`).toString(),
    });
  }
  return sonstigeKostenJSONExport;
};
