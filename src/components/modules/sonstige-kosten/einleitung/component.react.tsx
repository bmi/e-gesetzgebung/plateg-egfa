// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import { BASE_PATH } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';

export function SonstigeKostenEinleitungContentComponent(): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>(`/egfa/:id/${routes.MODULE}/:moduleName/:pageName`);
  const MODULE_LINK = `#/egfa/${routeMatcherVorbereitung?.params?.id as string}/module`;
  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.sonstigeKosten.einleitung.text`, {
            basePath: BASE_PATH,
            auswirkungOHH: `${MODULE_LINK}/einnahmenAusgaben`,
            erfuellungsaufwand: `${MODULE_LINK}/erfuellungsaufwand`,
            auswirkungEinzelpreise: `${MODULE_LINK}/preise`,
            auswirkungKMU: `${MODULE_LINK}/kmu`,
          }),
        }}
      />
    </>
  );
}
