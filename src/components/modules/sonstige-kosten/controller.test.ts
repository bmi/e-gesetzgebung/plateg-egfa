// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { Observable, of, throwError } from 'rxjs';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleSonstigeKostenEntityResponseDTO,
  OperationOpts,
  SendEgfaDatenuebernahmeRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { exportSonstigeKostenToEditor } from './controller';

describe(`Test: exportSonstigeKostenToEditor`, () => {
  let egfaControllerStub: Sinon.SinonStub<
    [SendEgfaDatenuebernahmeRequest, (OperationOpts | undefined)?],
    Observable<void | AjaxResponse<void>>
  >;
  let egfaModuleControllerStub: sinon.SinonStub;
  let i18nStub: sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleSonstigeKostenControllerApi.prototype, 'getEgfaModule2');

    i18nStub = Sinon.stub(i18n, 't');
  });
  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
    i18nStub.resetHistory();
  });
  it(`correct sonstigeKosten data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleSonstigeKostenEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');

    // Call the function under test (exportSonstigeKostenToEditor)
    let result: boolean;
    exportSonstigeKostenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('hello World');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct sonstigeKosten data, influence not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleSonstigeKostenEntityResponseDTO = {
      dto: {
        influenceExpected: false,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportSonstigeKostenToEditor)
    let result: boolean;
    exportSonstigeKostenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('some text');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct sonstigeKosten data, influenceSummary  not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleSonstigeKostenEntityResponseDTO = {
      dto: {
        influenceExpected: true,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportSonstigeKostenToEditor)
    let result: boolean;
    exportSonstigeKostenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('some text');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(` getting eGFA error`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(throwError(new Error('error getting eGFA')));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportSonstigeKostenToEditor)
    let result: boolean;
    exportSonstigeKostenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      Sinon.assert.notCalled(egfaControllerStub);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`error on sending to editor`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleSonstigeKostenEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(throwError(new Error('error getting eGFA')));
    i18nStub.returns('some text');
    // Call the function under test (exportSonstigeKostenToEditor)
    let result: boolean;
    exportSonstigeKostenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('hello World');
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
