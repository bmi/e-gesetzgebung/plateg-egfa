// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router';

import {
  AktionType,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleSonstigeKostenEntityDTO,
  EgfaModuleSonstigeKostenEntityResponseDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateSonstigeKostenJSON } from './controller';
import { SonstigeKostenEinleitungContentComponent } from './einleitung/component.react';
import { SimpleModuleSonstigeKostenBeschreibung } from './pruefung-und-beschreibung/component.react';

export function SonstigeKosten(props: ModuleProps) {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [sonstigeKostenData, setSonstigeKostenData] = useState<EgfaModuleSonstigeKostenEntityDTO>();
  const egfaModuleSonstigeKostenController = GlobalDI.get<EgfaModuleSonstigeKostenControllerApi>(
    'egfaModuleSonstigeKostenControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaModuleSonstigeKostenController.getEgfaModule2({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleSonstigeKostenEntityDTO) =>
    egfaModuleSonstigeKostenController.modifyEgfaModule2({
      egfaId: egfaId,
      egfaModuleSonstigeKostenEntityDTO: localData,
    });

  useEffect(() => {
    props.setEgfaName(routes.SONSTIGE_KOSTEN);
  }, []);

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
  ) => {
    const contentPageProps = { egfaId, setFormInstance, formData: sonstigeKostenData, onSave, handleFormChange };
    const PDFExportBtn = ctrl.getPDFExportAction(
      sonstigeKostenData?.egfaId as string,
      moduleName,
      sonstigeKostenData?.status,
    );
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName={`${routes.SONSTIGE_KOSTEN}`}
            previousLinkName={routes.SONSTIGE_KOSTEN_PRUEFUNG}
            customEditLink={routes.SONSTIGE_KOSTEN_PRUEFUNG}
            additionalActions={PDFExportBtn}
          />
        </Route>
        {(sonstigeKostenData?.aktionen?.includes(AktionType.Schreiben) === false ||
          sonstigeKostenData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.SONSTIGE_KOSTEN}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.SONSTIGE_KOSTEN_PRUEFUNG}
            customContent={<SonstigeKostenEinleitungContentComponent />}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.SONSTIGE_KOSTEN_PRUEFUNG}`]}>
          <SimpleModuleSonstigeKostenBeschreibung {...contentPageProps} moduleName={`${routes.SONSTIGE_KOSTEN}`} />
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.ZUSAMMENFASSUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.FERTIGSTELLUNG}`}
            />
          )}
        />
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.SONSTIGE_KOSTEN}/${routes.EINLEITUNG}`} />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleSonstigeKostenEntityDTO, EgfaModuleSonstigeKostenEntityResponseDTO>
      setFormData={setSonstigeKostenData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      generateJson={generateSonstigeKostenJSON}
    />
  );
}
