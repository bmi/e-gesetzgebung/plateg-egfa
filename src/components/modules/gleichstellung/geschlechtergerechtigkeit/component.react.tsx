// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './geschelchtergerechtigkeit.less';

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleGleichstellungEntityDTOAllOf } from '@plateg/rest-api';
import { Constants, FormWrapper, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface Relevanzpruefung1Props<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName?: string;
  sectionName: string;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}
export function Geschlechtergerechtigkeit(
  props: Relevanzpruefung1Props<EgfaModuleGleichstellungEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    form.setFieldsValue(props.formData);
    setIsDirty(false);
  }, [props.formData]);

  return (
    <div className="gleichstellung-geschelchtergerechtigkeit">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={props.formData}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={false}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <section>
          <div
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.introText`),
            }}
          />
          <strong className="geschlechtergerechtigkeit-info">
            {t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.info.title`)}&nbsp;
            <InfoComponent title={t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.info.drawerTitle`)}>
              <p
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.info.content`, {
                    interpolation: { escapeValue: false },
                  }),
                }}
              />
            </InfoComponent>
          </strong>
        </section>
        {Array.from({ length: 2 }).map((_, i) => {
          const sectionName = `section${i + 1}`;
          return (
            <section>
              <Title level={2}>
                {t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.${sectionName}.title`)}
              </Title>
              <p
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.${sectionName}.part1`),
                }}
              />
              <EgfaInfoCollapse
                panelKey="1"
                headerText={t(
                  `egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.${sectionName}.hiddenInfo.trigger`,
                )}
                text={t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.${sectionName}.hiddenInfo.content`)}
              />
              <p
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.${sectionName}.part2`),
                }}
              />
            </section>
          );
        })}
        {props.referenzen}
        <section>
          <Title level={2}>{t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.section3.title`)}</Title>
          <p>
            <strong>{t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.section3.content`)}</strong>
          </p>
          <Form.Item
            label={t(`egfa.modules.${props.moduleName}.geschlechtergerechtigkeit.section3.massnahmenInputLabel`)}
            name="massnahmenGeschlechtergerechtigkeit"
            required={false}
            rules={[
              {
                max: Constants.TEXT_AREA_LENGTH,
                message: t('egfa.textLengthError', {
                  maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                }),
              },
            ]}
          >
            <TextArea rows={9} />
          </Form.Item>
        </section>
      </FormWrapper>
    </div>
  );
}
