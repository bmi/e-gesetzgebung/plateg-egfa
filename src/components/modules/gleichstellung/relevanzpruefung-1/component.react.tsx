// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './relevanzpruefung1.less';

import { Form, Radio, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleGleichstellungEntityDTOAllOf } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface Relevanzpruefung1Props<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string | string[], isDisabled: boolean) => void;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}
export function Relevanzpruefung1(
  props: Relevanzpruefung1Props<EgfaModuleGleichstellungEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [gleichstellungAlsExplizitesZiel, setGleichstellungAlsExplizitesZiel] = useState<boolean>();
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName || '');
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    setGleichstellungAlsExplizitesZiel(props.formData?.gleichstellungAlsExplizitesZiel ?? false);
    setIsDirty(false);
    form.setFieldsValue(props.formData);
  }, [props.formData]);

  // enable / disable next link in sidebar
  useEffect(() => {
    if (gleichstellungAlsExplizitesZiel !== undefined) {
      if (isDirty) {
        props.updateNextLink?.(
          [routes.HAUPTPRUEFUNG, routes.GESCHLECHTERGERECHTIGKEIT],
          !gleichstellungAlsExplizitesZiel,
        );
      }
      setNextLink(gleichstellungAlsExplizitesZiel ? routes.HAUPTPRUEFUNG : props.nextLinkName);
    }
  }, [gleichstellungAlsExplizitesZiel]);

  return (
    <div className="relevanzpruefung-1">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.relevanzpruefung1.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={props.formData}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <div
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung1.text`),
          }}
        />
        {props.referenzen}
        <Title level={3}>{t(`egfa.modules.${props.moduleName}.relevanzpruefung1.subheading1`)}</Title>
        <fieldset className="fieldset-form-items">
          <legend className="seo">{t(`egfa.modules.${props.moduleName}.relevanzpruefung1.question1`)}</legend>
          <Form.Item
            name={'gleichstellungAlsExplizitesZiel'}
            label={t(`egfa.modules.${props.moduleName}.relevanzpruefung1.question1`)}
            rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
          >
            <Radio.Group
              className="horizontal-radios"
              onChange={(event) => setGleichstellungAlsExplizitesZiel(event?.target?.value as boolean)}
              name={'gleichstellungAlsExplizitesZiel'}
            >
              <Radio
                id="egfa-gleichstellung-relevanzpruefung1-gleichstellungAlsExplizitesZielJa-radio"
                className="horizontal-radios"
                value={true}
              >
                Ja
              </Radio>
              <Radio
                id="egfa-gleichstellung-relevanzpruefung1-gleichstellungAlsExplizitesZielNein-radio"
                className="horizontal-radios"
                value={false}
              >
                Nein
              </Radio>
            </Radio.Group>
          </Form.Item>
        </fieldset>
      </FormWrapper>
    </div>
  );
}
