// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './preview.less';

import { Image } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

interface PreviewComponentProps {
  moduleName: string;
}
export function PreviewComponent(props: PreviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const kernbereiche: { name: string; text: string }[] = t(`egfa.modules.${props.moduleName}.kernbereiche`, {
    returnObjects: true,
  });

  return (
    <ul className="gleichstellung-preview-component">
      {kernbereiche.map((kernbereich, i) => {
        return (
          <li>
            <Image
              key={`${kernbereich.name}-${i}`}
              preview={false}
              src={
                (require(`../../../../media/gleichstellung/Icon-Gleichstellung_${kernbereich.name}.svg`) as ImageModule)
                  .default
              }
              alt=""
              aria-hidden={true}
            />
            <span
              dangerouslySetInnerHTML={{
                __html: kernbereich.text,
              }}
            />
          </li>
        );
      })}
    </ul>
  );
}
