// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './einleitung.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';

export function EinleitungGleichstellung(): React.ReactElement {
  const { t } = useTranslation();
  const BIB_LINK = `${BASE_PATH}/arbeitshilfen/download`;

  return (
    <div className="gleichstellung-einleitung-content">
      <p>
        <span
          dangerouslySetInnerHTML={{
            __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph1.content1', {
              GGO_LINK_2: `${BIB_LINK}/34#page=7`,
              interpolation: { escapeValue: false },
            }),
          }}
        />
        <span
          dangerouslySetInnerHTML={{
            __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph1.content2', {
              interpolation: { escapeValue: false },
            }),
          }}
        />
      </p>
      <div>
        <span
          dangerouslySetInnerHTML={{
            __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph2.content1', {
              interpolation: { escapeValue: false },
            }),
          }}
        />
        <InfoComponent
          title={t(`egfa.modules.gleichstellung.einleitung.content.paragraph2.info.title`)}
          id={`egfa-gleichstellung-einleitung-paragraph2-info`}
          titleWithoutPrefix={true}
        >
          <p
            className="gleichstellung-drawer-content"
            dangerouslySetInnerHTML={{
              __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph2.info.content', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </InfoComponent>
        <span
          dangerouslySetInnerHTML={{
            __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph2.content2', {
              interpolation: { escapeValue: false },
            }),
          }}
        />
      </div>
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.gleichstellung.einleitung.content.paragraph3.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      />
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`egfa.modules.gleichstellung.einleitung.content.paragraph4.title`)}
        text={t(`egfa.modules.gleichstellung.einleitung.content.paragraph4.content`)}
      />
    </div>
  );
}
