// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './gleichstellung.less';

import { FormInstance } from 'antd/lib/form';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleGleichstellungControllerApi,
  EgfaModuleGleichstellungEntityDTO,
  EgfaModuleGleichstellungEntityResponseDTO,
  EgfaModuleStatusType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import {
  DemografieReferenzenTypes,
  DisabilityReferenzenTypes,
  EnapReferenzenTypes,
  GleichwertigkeitReferenzenTypes,
} from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateGleichstellungsorientierteJSON } from './controller';
import { EinleitungGleichstellung } from './einleitung/component.react';
import { Geschlechtergerechtigkeit } from './geschlechtergerechtigkeit/component.react';
import { AuswirkungenHauptpruefung } from './hauptpruefung/auswirkungen/component.react';
import { Hauptpruefung } from './hauptpruefung/component.react';
import { Wirkbereich } from './hauptpruefung/wirkbereich/component.react';
import { Relevanzpruefung1 } from './relevanzpruefung-1/component.react';
import { Relevanzpruefung2 } from './relevanzpruefung-2/component.react';
import { Zusammenfassung } from './zusammenfassung/component.react';

export function Gleichstellung(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [gleichstellungData, setGleichstellungData] = useState<
    EgfaModuleGleichstellungEntityDTO & ExtendedModuleData
  >();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const egfaGleichstellungControllerApi = GlobalDI.get<EgfaModuleGleichstellungControllerApi>(
    'egfaGleichstellungControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const { t } = useTranslation();

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  useEffect(() => {
    if (gleichstellungData && gleichstellungData.referenzenData) {
      setReferenzenData(gleichstellungData.referenzenData);
    }
  }, [gleichstellungData]);

  const getData = () => {
    return egfaGleichstellungControllerApi.getEgfaModule6({ egfaId: egfaId });
  };

  const putData = (localData: EgfaModuleGleichstellungEntityDTO) => {
    return egfaGleichstellungControllerApi.modifyEgfaModule6({
      egfaId: egfaId,
      egfaModuleGleichstellungEntityDTO: localData,
    });
  };

  const handleInfluence = (influence: boolean, formData?: EgfaModuleGleichstellungEntityDTO) => {
    if (formData) {
      const noInfluenceSummaries = [
        t(`egfa.modules.${routes.GLEICHSTELLUNG}.${routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}.irrelevant.case1`),
        t(`egfa.modules.${routes.GLEICHSTELLUNG}.${routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}.irrelevant.case2`),
      ];
      const summary = (() => {
        if (influence) {
          return !noInfluenceSummaries.includes(gleichstellungData?.influenceSummary || '')
            ? gleichstellungData?.influenceSummary
            : undefined;
        }
        return noInfluenceSummaries[gleichstellungData?.maennerUndFrauenBetroffenExpected ? 0 : 1];
      })();

      formData.influenceExpected = influence || false;
      formData.influenceSummary = summary;
    }
  };

  useEffect(() => {
    props.setEgfaName(routes.DISABILITY);
  }, []);

  const PageRoutingGleichstellung = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ): React.ReactElement => {
    const contentPageProps = { egfaId, setFormInstance, formData: gleichstellungData, onSave, handleFormChange };
    const influence =
      contentPageProps?.formData?.gleichstellungAlsExplizitesZiel ||
      contentPageProps?.formData?.maennerUndFrauenUnterschiedlicheWirkungExpected ||
      false;

    useEffect(() => {
      if (
        updateItemDisableStatus &&
        contentPageProps.formData &&
        contentPageProps.formData.status === EgfaModuleStatusType.InBearbeitung
      ) {
        const linksList = [routes.HAUPTPRUEFUNG, routes.GESCHLECHTERGERECHTIGKEIT];
        updateItemDisableStatus(linksList, !influence);
      }
    }, [gleichstellungData]);

    handleInfluence(influence || false, contentPageProps?.formData);

    const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
    const PDFExportBtn = ctrl.getPDFExportAction(props.moduleProps.egfaId, moduleName, gleichstellungData?.status);

    const EGFA_LINK = `#/egfa/${egfaId}/module`;

    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };

    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName="gleichstellung"
            customEditLink={routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}
            previousLinkName={routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}
            customNoImpactText={gleichstellungData?.influenceSummary}
            additionalActions={PDFExportBtn}
          />
        </Route>
        {(gleichstellungData?.aktionen?.includes(AktionType.Schreiben) === false ||
          gleichstellungData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.RELEVANZPRUEFUNG_1}
            customTitle={
              <Title level={1}>
                {t(`egfa.modules.gleichstellung.einleitung.title`)}
                <InfoComponent
                  title={t(`egfa.modules.gleichstellung.einleitung.infoTitle`)}
                  id={`egfa-gleichstellung-info`}
                  titleWithoutPrefix={true}
                >
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t('egfa.modules.gleichstellung.einleitung.infoText', {
                        interpolation: { escapeValue: false },
                      }),
                    }}
                  />
                </InfoComponent>
              </Title>
            }
            customContent={<EinleitungGleichstellung />}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.RELEVANZPRUEFUNG_1}`]}>
          <Relevanzpruefung1
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.RELEVANZPRUEFUNG_1}
            previousLinkName={routes.EINLEITUNG}
            nextLinkName={routes.RELEVANZPRUEFUNG_2}
            updateNextLink={updateItemDisableStatus}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg5],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.RELEVANZPRUEFUNG_2}`]}>
          <Relevanzpruefung2
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.RELEVANZPRUEFUNG_2}
            previousLinkName={routes.RELEVANZPRUEFUNG_1}
            nextLinkName={routes.HAUPTPRUEFUNG}
            updateNextLink={updateItemDisableStatus}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}`]}>
          <Hauptpruefung
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.HAUPTPRUEFUNG}
            previousLinkName={routes.RELEVANZPRUEFUNG_2}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ARBEIT}`}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.ARBEIT}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.ARBEIT}
            previousLinkName={routes.HAUPTPRUEFUNG}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GELD}`}
            imageFileName={'Icon-Gleichstellung_Arbeit.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg42,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg85,
                    ],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungDemografieReferenzen,
                    conditionsList: [
                      DemografieReferenzenTypes.hasSelectedFamilie,
                      DemografieReferenzenTypes.hasSelectedArbeit,
                    ],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.GELD}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.GELD}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ARBEIT}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.WISSEN}`}
            imageFileName={'Icon-Gleichstellung_Geld.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg1,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg10,
                    ],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.WISSEN}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.WISSEN}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GELD}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ZEIT}`}
            imageFileName={'Icon-Gleichstellung_Wissen.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg4],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedArbeit],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.ZEIT}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.ZEIT}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.WISSEN}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ENTSCHEIDUNGSMACHT}`}
            imageFileName={'Icon-Gleichstellung_Zeit.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg42],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge,
                      GleichwertigkeitReferenzenTypes.hasSelectedEngagement,
                      GleichwertigkeitReferenzenTypes.hasSelectedMobilitaet,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedFamilie],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.ENTSCHEIDUNGSMACHT}`,
          ]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.ENTSCHEIDUNGSMACHT}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ZEIT}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GESUNDHEIT}`}
            imageFileName={'Icon-Gleichstellung_Entscheidungsmacht.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedEngagement],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.GESUNDHEIT}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.GESUNDHEIT}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.ENTSCHEIDUNGSMACHT}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GEWALT}`}
            imageFileName={'Icon-Gleichstellung_Gesundheit.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg31],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.gleichstellungGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.GEWALT}`]}
        >
          <Wirkbereich
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.GEWALT}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GESUNDHEIT}`}
            nextLinkName={`${routes.HAUPTPRUEFUNG}/${routes.AUSWIRKUNGEN}`}
            imageFileName={'Icon-Gleichstellung_Gewalt.svg'}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg161],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.HAUPTPRUEFUNG}/${routes.AUSWIRKUNGEN}`]}
        >
          <AuswirkungenHauptpruefung
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            parentSectionName={routes.HAUPTPRUEFUNG}
            sectionName={routes.AUSWIRKUNGEN}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.GEWALT}`}
            nextLinkName={routes.GESCHLECHTERGERECHTIGKEIT}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg5],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                ]}
              />
            }
            referenzen_2={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungDisabilityReferenzen,
                    conditionsList: [DisabilityReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DISABILITY,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.GESCHLECHTERGERECHTIGKEIT}`]}>
          <Geschlechtergerechtigkeit
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.GESCHLECHTERGERECHTIGKEIT}
            previousLinkName={`${routes.HAUPTPRUEFUNG}/${routes.AUSWIRKUNGEN}`}
            nextLinkName={routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichstellungEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg5],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}`]}
        >
          <Zusammenfassung
            {...contentPageProps}
            moduleName={`${routes.GLEICHSTELLUNG}`}
            sectionName={routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG}
            previousLinkName={influence ? routes.GESCHLECHTERGERECHTIGKEIT : routes.RELEVANZPRUEFUNG_2}
            nextLinkName={routes.FERTIGSTELLUNG}
            hasInfluence={influence}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${routes.EINLEITUNG}`} />
          )}
        />
      </Switch>
    );
  };

  return (
    <div className="gleichstellung-component">
      <ModuleWrapper<EgfaModuleGleichstellungEntityDTO, EgfaModuleGleichstellungEntityResponseDTO>
        setFormData={setGleichstellungData}
        getData={getData}
        putData={putData}
        moduleIdentifier={moduleName}
        egfaId={egfaId}
        form={formInstance}
        pageRouting={PageRoutingGleichstellung}
        pageName={currentPageName}
        generateJson={generateGleichstellungsorientierteJSON}
      />
    </div>
  );
}
