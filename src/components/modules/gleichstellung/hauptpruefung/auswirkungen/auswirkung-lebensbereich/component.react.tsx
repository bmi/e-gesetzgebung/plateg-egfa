// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './auswirkung-lebensbereich.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { CheckOutlined, ImageModule } from '@plateg/theme';

import { WirkbereichOption } from '../../wirkbereich/component.react';
import { LebensbereichWithUnterbereichen } from '../component.react';

export interface AuswirkungLebensbereichProps {
  lebensbereich: LebensbereichWithUnterbereichen;
  parentSectionName: string;
  sectionName: string;
}
export function AuswirkungLebensbereich(props: AuswirkungLebensbereichProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const bereichName =
    props.lebensbereich.hauptbereich.charAt(0).toUpperCase() + props.lebensbereich.hauptbereich.toLowerCase().slice(1);

  if (!props.lebensbereich.unterbereiche.length) {
    return <></>;
  }
  return (
    <div className="auswirkung-lebensbereich">
      <div className="auswirkung-lebensbereich-head">
        <div className="auswirkung-lebensbereich-icon-container">
          <Image
            loading="lazy"
            preview={false}
            src={
              (require(`../../../../../../media/gleichstellung/Icon-Gleichstellung_${bereichName}.svg`) as ImageModule)
                .default
            }
            alt={`Logo Lebensbereich ${props.sectionName}`}
          />
        </div>
        <div className="auswirkung-lebensbereich-title-container">
          <Title level={3}>
            {t(
              `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.lebensbereichTitle`,
              {
                title: bereichName,
              },
            )}
          </Title>
        </div>
      </div>
      <ul>
        {props.lebensbereich.unterbereiche.map((unterbereich) => {
          return (
            <li>
              <CheckOutlined />
              &nbsp;
              {
                (
                  t(
                    `egfa.modules.gleichstellung.${props.parentSectionName}.${props.lebensbereich.hauptbereich}.options`,
                    {
                      returnObjects: true,
                    },
                  ) as [WirkbereichOption]
                ).find((option) => {
                  return option.key === unterbereich;
                })?.title
              }
            </li>
          );
        })}
      </ul>
    </div>
  );
}
