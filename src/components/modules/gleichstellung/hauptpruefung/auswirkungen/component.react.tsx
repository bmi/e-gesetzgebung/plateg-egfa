// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './auswirkungen.less';

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleGleichstellungEntityDTOAllOf } from '@plateg/rest-api';
import { Constants, FormWrapper, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';
import { AuswirkungLebensbereich } from './auswirkung-lebensbereich/component.react';

export interface AuswirkungenHauptpruefungProps<P> extends ContentPageProps<P> {
  nextLinkName?: string;
  previousLinkName?: string;
  moduleName: string;
  parentSectionName: string;
  sectionName: string;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
  referenzen_2?: React.ReactElement;
}

export interface LebensbereichWithUnterbereichen {
  hauptbereich: string;
  unterbereiche: string[];
}

export function AuswirkungenHauptpruefung(
  props: AuswirkungenHauptpruefungProps<EgfaModuleGleichstellungEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);
  const [lebensbereiche, setLebensbereiche] = useState<LebensbereichWithUnterbereichen[]>([]);

  const possibleLebensbereiche = [
    routes.ARBEIT,
    routes.GELD,
    routes.WISSEN,
    routes.ZEIT,
    routes.ENTSCHEIDUNGSMACHT,
    routes.GESUNDHEIT,
    routes.GEWALT,
  ];

  const translationPrefix = (() => {
    if (!lebensbereiche.length) {
      return 'noBereich';
    }
    return lebensbereiche.length > 1 || lebensbereiche[0].unterbereiche.length > 1
      ? 'multipleBereiche'
      : 'singleBereich';
  })();

  useEffect(() => {
    const bereiche = possibleLebensbereiche
      .filter((lebensbereich) => {
        return (props.formData?.[lebensbereich as keyof EgfaModuleGleichstellungEntityDTOAllOf] as [])?.length;
      })
      .map((bereich) => {
        return {
          hauptbereich: bereich,
          unterbereiche: props.formData?.[bereich as keyof EgfaModuleGleichstellungEntityDTOAllOf] as string[],
        };
      });
    setLebensbereiche(bereiche);
    form.setFieldsValue({
      ...props.formData,
      weitereAuswirkungenSummary: bereiche.length === 0 ? undefined : props.formData?.weitereAuswirkungenSummary,
    });
    setIsDirty(false);
  }, [props.formData]);

  return (
    <div className="auswirkungen-hauptrpruefung">
      <FormWrapper
        projectName="eGFA"
        title={
          <Title level={1}>
            {t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.title`)}
          </Title>
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${
          props.previousLinkName as string
        }`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${props.nextLinkName as string}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={props.formData}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        {props.referenzen}
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.intro`),
          }}
        />
        <EgfaInfoCollapse
          panelKey="1"
          headerText={t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph1.title`)}
          text={t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph1.content`)}
        />
        {props.referenzen_2}
        <Title level={2}>
          {t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph2.title`)}
          <InfoComponent
            title={t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph2.title`)}
          >
            <p
              className="auswirkungen-hauptpruefung-info"
              dangerouslySetInnerHTML={{
                __html: t(
                  t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph2.info`),
                  {
                    interpolation: { escapeValue: false },
                  },
                ),
              }}
            />
          </InfoComponent>
        </Title>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph2.content`),
          }}
        />
        <>
          <Title level={2}>
            {t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.title`)}
          </Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t(
                `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.${translationPrefix}.content`,
              ),
            }}
          />
          {lebensbereiche.map((lebensbereich) => {
            return <AuswirkungLebensbereich {...props} lebensbereich={lebensbereich} />;
          })}
          <Title level={3}>
            {t(
              `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.${translationPrefix}.question1`,
            )}
          </Title>
          <Form.Item
            label={t(
              `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.${translationPrefix}.auswirkungenTitle`,
            )}
            name="maennerUndFrauenBetroffenSummary"
            required={true}
            rules={[
              { required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') },
              {
                max: Constants.TEXT_AREA_LENGTH,
                message: t('egfa.textLengthError', {
                  maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                }),
              },
            ]}
          >
            <TextArea rows={9} />
          </Form.Item>
          {lebensbereiche.length > 0 && (
            <Title level={3}>
              {t(
                `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.${translationPrefix}.question2`,
              )}
            </Title>
          )}
          <Form.Item
            label={t(
              `egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.paragraph3.weitereAuswirkungenTitle`,
            )}
            hidden={lebensbereiche.length === 0}
            name="weitereAuswirkungenSummary"
            rules={[
              {
                max: Constants.TEXT_AREA_LENGTH,
                message: t('egfa.textLengthError', {
                  maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                }),
              },
            ]}
          >
            <TextArea rows={9} />
          </Form.Item>
        </>
      </FormWrapper>
    </div>
  );
}
