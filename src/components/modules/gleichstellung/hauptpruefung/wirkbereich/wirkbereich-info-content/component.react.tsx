// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './wirkbereich-info-content.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

export interface WirkbereichInfoProps {
  drawerContent: { introText: string; ergebnisse: string[]; ziele: string[] };
  wirkbereichKey: string;
}

export function WirkbereichInfo(props: WirkbereichInfoProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="wirkbereich-info">
      <p
        dangerouslySetInnerHTML={{
          __html: props.drawerContent.introText,
        }}
      />
      <Title level={2}>{t('egfa.modules.gleichstellung.wirkbereich.info.moreInfo')}</Title>
      <Title level={3}>{t('egfa.modules.gleichstellung.wirkbereich.info.studienergebnisse')}</Title>
      <ul>
        {props.drawerContent.ergebnisse.map((ergebnis, index) => {
          return (
            <li
              key={`${props.wirkbereichKey}-info-ergebnis-${index}`}
              dangerouslySetInnerHTML={{
                __html: ergebnis,
              }}
            />
          );
        })}
      </ul>
      {props.drawerContent.ziele && (
        <>
          <Title level={3}>
            {props.drawerContent.ziele.length > 1
              ? t('egfa.modules.gleichstellung.wirkbereich.info.ziele')
              : t('egfa.modules.gleichstellung.wirkbereich.info.ziel')}
          </Title>
          {props.drawerContent.ziele.map((ziel, index) => {
            return (
              <p
                key={`${props.wirkbereichKey}-info-ziel-${index}`}
                dangerouslySetInnerHTML={{
                  __html: ziel,
                }}
              />
            );
          })}
          <p
            dangerouslySetInnerHTML={{
              __html: t('egfa.modules.gleichstellung.wirkbereich.info.link', {
                link: 'https://www.gleichstellungsstrategie.de/',
              }),
            }}
          />
        </>
      )}
    </div>
  );
}
