// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './wirkbereich.less';

import { Checkbox, Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';
import { WirkbereichHeader } from './wirbereich-header/component.react';
import { WirkbereichInfo } from './wirkbereich-info-content/component.react';

export interface WirkbereichProps<P> extends ContentPageProps<P> {
  nextLinkName?: string;
  previousLinkName?: string;
  moduleName: string;
  parentSectionName: string;
  sectionName: string;
  imageFileName: string;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}

interface FormDataInterface {
  [s: string]: any;
}

export interface WirkbereichOption {
  key: string;
  title: string;
  drawerContent?: { introText: string; ergebnisse: string[]; ziele: string[] };
}

export function Wirkbereich(props: WirkbereichProps<any>): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const data: FormDataInterface = props.formData;
      if (data[props.sectionName] === null) {
        data[props.sectionName] = [];
      }
      form.setFieldsValue(data);
      setIsDirty(false);
    }
  }, [props.formData]);

  return (
    <div className="wirkbereich">
      <FormWrapper
        projectName="eGFA"
        title={
          <WirkbereichHeader
            parentSectionName={props.parentSectionName}
            sectionName={props.sectionName}
            fileName={props.imageFileName}
          />
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${
          props.previousLinkName as string
        }`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.GLEICHSTELLUNG}/${props.nextLinkName as string}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <fieldset className="fieldset-form-items">
          <legend>
            {
              <span>
                {t('egfa.modules.gleichstellung.wirkbereich.question')}
                <br />
                <br />
                <span className="wirkbereich-subtitle">{t('egfa.modules.gleichstellung.wirkbereich.subtitle')}</span>
              </span>
            }
          </legend>
          <Form.Item name={[props.sectionName]}>
            <Checkbox.Group name="beruecksichtigungen">
              {(
                t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.options`, {
                  returnObjects: true,
                }) as []
              )?.map((option: WirkbereichOption) => {
                return (
                  <Checkbox
                    id={`egfa-${props.moduleName}-${props.sectionName}-${option.key}`}
                    value={option.key}
                    aria-label={option.title}
                    key={option.key}
                  >
                    {option.title}&nbsp;
                    {option.drawerContent && (
                      <InfoComponent title={option.title}>
                        <WirkbereichInfo drawerContent={option.drawerContent} wirkbereichKey={option.key} />
                      </InfoComponent>
                    )}
                  </Checkbox>
                );
              })}
            </Checkbox.Group>
          </Form.Item>
        </fieldset>
        {props.referenzen}
      </FormWrapper>
    </div>
  );
}
