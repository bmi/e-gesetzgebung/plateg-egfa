// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './wirkbereich-header.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

interface WirkbereichHeaderProps {
  sectionName: string;
  parentSectionName: string;
  fileName: string;
}

export function WirkbereichHeader(props: WirkbereichHeaderProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  return (
    <div className="wirkbereich-heading">
      <div className="wirkbereich-heading-holder">
        <Image
          loading="lazy"
          preview={false}
          src={(require(`../../../../../../media/gleichstellung/${props.fileName}`) as ImageModule).default}
          alt={`Logo ${props.sectionName}`}
        />
        <div className="wirkbereich-heading-text">
          <Title level={1}>
            Lebensbereich:
            <br />
            {t(`egfa.modules.gleichstellung.${props.parentSectionName}.${props.sectionName}.title`)}
          </Title>
        </div>
      </div>
    </div>
  );
}
