// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { LeftOutlined, RightOutlined } from '@plateg/theme';

import { PreviewComponent } from '../preview/component.react';

export interface HauptpruefungProps {
  moduleName: string;
  nextLinkName: string;
  previousLinkName: string;
  sectionName: string;
}
export function Hauptpruefung(props: HauptpruefungProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <div style={{ marginBottom: '120px' }}>
      <Title level={1}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.title`)}</Title>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.${props.moduleName}.${props.sectionName}.text`),
        }}
      />
      <PreviewComponent moduleName={props.moduleName} />
      <div className="form-control-buttons">
        <Button
          id={`egfa-${props.previousLinkName}-navigation-btn-${props.sectionName}-prev`}
          type="text"
          size="large"
          className="btn-prev"
          onClick={() => history.push(props.previousLinkName)}
        >
          <LeftOutlined /> Vorheriger Schritt
        </Button>
        <Button
          id={`egfa-${props.nextLinkName}-navigation-btn-${props.sectionName}-next`}
          type="text"
          size="large"
          className="btn-next"
          onClick={() => history.push(props.nextLinkName)}
        >
          Nächster Schritt <RightOutlined />
        </Button>
      </div>
    </div>
  );
}
