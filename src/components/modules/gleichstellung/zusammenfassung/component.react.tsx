// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../general/zusammenfassung-wrapper/zusammenfassung.less';
import './zusammenfassung.less';

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleGleichstellungEntityDTOAllOf, EgfaModuleStatusType } from '@plateg/rest-api';
import { Constants, FormWrapper } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';

export interface ZusammenfassungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
  hasInfluence: boolean;
}
export function Zusammenfassung(
  props: ZusammenfassungProps<EgfaModuleGleichstellungEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    setIsDirty(false);
    form.setFieldsValue(props.formData);
  }, [props.formData]);

  return (
    <div className="gleichstellung-zusammenfassung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={props.formData}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={props.hasInfluence}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        {props.hasInfluence && (
          <>
            <section
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.${props.moduleName}.${props.sectionName}.section1`),
              }}
            />
            <div className="zusammenfassung-div">
              <Title level={2}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.auswirkungen.title`)}</Title>
              <ZusammenfassungItem
                title={t(`egfa.modules.${props.moduleName}.${props.sectionName}.auswirkungen.unmittelbareUndMittelbar`)}
                bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.HAUPTPRUEFUNG}/${routes.AUSWIRKUNGEN}`}
                onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
                egfaId={props.egfaId}
                allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
              >
                <span>
                  {props.formData?.maennerUndFrauenBetroffenSummary ||
                    t(`egfa.modules.${props.moduleName}.${props.sectionName}.auswirkungen.keineAuswirkungen`)}
                </span>
              </ZusammenfassungItem>
              <ZusammenfassungItem
                title={t(`egfa.modules.${props.moduleName}.${props.sectionName}.auswirkungen.weitere`)}
                bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.HAUPTPRUEFUNG}/${routes.AUSWIRKUNGEN}`}
                onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
                egfaId={props.egfaId}
                allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
              >
                <span>
                  {props.formData?.weitereAuswirkungenSummary ||
                    t(`egfa.modules.${props.moduleName}.${props.sectionName}.auswirkungen.keineAuswirkungen`)}
                </span>
              </ZusammenfassungItem>
            </div>
            <div className="zusammenfassung-div">
              <Title level={2}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.massnahmen.title`)}</Title>
              <ZusammenfassungItem
                title={t(`egfa.modules.${props.moduleName}.${props.sectionName}.massnahmen.maßnahmenGerechtigkeit`)}
                bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.GESCHLECHTERGERECHTIGKEIT}`}
                onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
                egfaId={props.egfaId}
                allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
              >
                <span>
                  {props.formData?.massnahmenGeschlechtergerechtigkeit ||
                    t(`egfa.modules.${props.moduleName}.${props.sectionName}.massnahmen.keineMassnahmen`)}
                </span>
              </ZusammenfassungItem>
            </div>
            <Title level={2}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.ergebnisse.title`)}</Title>
            <Form.Item
              label={t(
                `egfa.modules.${props.moduleName}.${props.sectionName}.ergebnisse.abschliessendeZusammenfassung`,
              )}
              name="influenceSummary"
              required={true}
              rules={[
                { required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') },
                {
                  max: Constants.TEXT_AREA_LENGTH,
                  message: t('egfa.textLengthError', {
                    maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                  }),
                },
              ]}
            >
              <TextArea rows={9} />
            </Form.Item>
          </>
        )}
        {!props.hasInfluence && (
          <div className="zusammenfassung-div irrelevant-case">
            <div className="divider-div" />
            <ZusammenfassungItem
              title={props.formData?.influenceSummary || ''}
              bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.RELEVANZPRUEFUNG_2}`}
              onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
              egfaId={props.egfaId}
              allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
            />
          </div>
        )}
      </FormWrapper>
    </div>
  );
}
