// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './relevanzpruefung-2.less';

import { Form, Radio, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleGleichstellungEntityDTOAllOf } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { PreviewComponent } from '../preview/component.react';

export interface Relevanzpruefung2Props<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string | string[], isDisabled: boolean) => void;
  handleFormChange?: () => void;
}
export function Relevanzpruefung2(
  props: Relevanzpruefung2Props<EgfaModuleGleichstellungEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [maennerUndFrauenBetroffenExpected, setMaennerUndFrauenBetroffenExpected] = useState<boolean>();
  const [maennerUndFrauenUnterschiedlicheWirkungExpected, setMaennerUndFrauenUnterschiedlicheWirkungExpected] =
    useState<boolean>();
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName);
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    setMaennerUndFrauenBetroffenExpected(props.formData?.maennerUndFrauenBetroffenExpected ?? false);
    setMaennerUndFrauenUnterschiedlicheWirkungExpected(
      props.formData?.maennerUndFrauenUnterschiedlicheWirkungExpected ?? false,
    );
    setIsDirty(false);
    form.setFieldsValue(props.formData);
  }, [props.formData]);

  useEffect(() => {
    if (!maennerUndFrauenBetroffenExpected) {
      form.setFieldsValue({ maennerUndFrauenUnterschiedlicheWirkungExpected: false });
      setMaennerUndFrauenUnterschiedlicheWirkungExpected(false);
    }
  }, [maennerUndFrauenBetroffenExpected]);

  // enable / disable next link in sidebar
  useEffect(() => {
    if (maennerUndFrauenUnterschiedlicheWirkungExpected !== undefined) {
      if (isDirty) {
        props.updateNextLink?.(
          [routes.HAUPTPRUEFUNG, routes.GESCHLECHTERGERECHTIGKEIT],
          !(maennerUndFrauenUnterschiedlicheWirkungExpected || props.formData?.gleichstellungAlsExplizitesZiel),
        );
      }
      setNextLink(
        maennerUndFrauenUnterschiedlicheWirkungExpected || props.formData?.gleichstellungAlsExplizitesZiel
          ? props.nextLinkName
          : routes.GLEICHSTELLUNG_ZUSAMMENFASSUNG,
      );
    }
  }, [maennerUndFrauenUnterschiedlicheWirkungExpected]);

  return (
    <div className="relevanzpruefung-2">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.relevanzpruefung2.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={props.formData}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <div
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung2.introText`),
          }}
        />
        <fieldset className="fieldset-form-items">
          <legend className="seo">{t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question1`)}</legend>
          <FormItemWithInfo
            htmlFor="id-maennerUndFrauenBetroffenExpected"
            name="maennerUndFrauenBetroffenExpected"
            customName="id-maennerUndFrauenBetroffenExpected"
            label={
              <span>
                {t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question1`)}
                <InfoComponent title={t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question1`)} withLabel>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question1Info`),
                    }}
                  />
                </InfoComponent>
              </span>
            }
          >
            <Radio.Group
              id="id-maennerUndFrauenBetroffenExpected"
              className="horizontal-radios"
              onChange={(event) => setMaennerUndFrauenBetroffenExpected(event?.target?.value as boolean)}
              name={'maennerUndFrauenBetroffenExpected'}
            >
              <Radio
                id="egfa-gleichstellung-relevanzpruefung2-maennerUndFrauenBetroffenExpectedJa-radio"
                className="horizontal-radios"
                value={true}
              >
                Ja
              </Radio>
              <Radio
                id="egfa-gleichstellung-relevanzpruefung2-maennerUndFrauenBetroffenExpectedNein-radio"
                className="horizontal-radios"
                value={false}
              >
                Nein
              </Radio>
            </Radio.Group>
          </FormItemWithInfo>
        </fieldset>
        {maennerUndFrauenBetroffenExpected && (
          <>
            <p>{t(`egfa.modules.${props.moduleName}.relevanzpruefung2.text`)}</p>
            <EgfaInfoCollapse
              panelKey="1"
              headerText={t(`egfa.modules.${props.moduleName}.relevanzpruefung2.hiddenInfo1.title`)}
              text={t(`egfa.modules.${props.moduleName}.relevanzpruefung2.hiddenInfo1.content`)}
            />
            <EgfaInfoCollapse
              panelKey="1"
              headerText={t(`egfa.modules.${props.moduleName}.relevanzpruefung2.hiddenInfo2.title`)}
              text={
                <>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung2.hiddenInfo2.content.text1`),
                    }}
                  />
                  <PreviewComponent moduleName={props.moduleName} />
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung2.hiddenInfo2.content.text2`),
                    }}
                  />
                </>
              }
            />
            <fieldset className="fieldset-form-items">
              <legend className="seo">{t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question1`)}</legend>
              <FormItemWithInfo
                htmlFor="id-maennerUndFrauenUnterschiedlicheWirkungExpected"
                name="maennerUndFrauenUnterschiedlicheWirkungExpected"
                customName="id-maennerUndFrauenUnterschiedlicheWirkungExpected"
                label={
                  <span>
                    {t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question2`)}
                    <InfoComponent title={t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question2`)} withLabel>
                      <div
                        dangerouslySetInnerHTML={{
                          __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung2.question2Info`),
                        }}
                      />
                    </InfoComponent>
                  </span>
                }
              >
                <Radio.Group
                  id="id-maennerUndFrauenUnterschiedlicheWirkungExpected"
                  className="horizontal-radios"
                  onChange={(event) =>
                    setMaennerUndFrauenUnterschiedlicheWirkungExpected(event?.target?.value as boolean)
                  }
                  name={'maennerUndFrauenUnterschiedlicheWirkungExpected'}
                >
                  <Radio
                    id="egfa-gleichstellung-relevanzpruefung2-maennerUndFrauenUnterschiedlicheWirkungExpectedJa-radio"
                    className="horizontal-radios"
                    value={true}
                  >
                    Ja
                  </Radio>
                  <Radio
                    id="egfa-gleichstellung-relevanzpruefung2-maennerUndFrauenUnterschiedlicheWirkungExpectedNein-radio"
                    className="horizontal-radios"
                    value={false}
                  >
                    Nein
                  </Radio>
                </Radio.Group>
              </FormItemWithInfo>
            </fieldset>
          </>
        )}
      </FormWrapper>
    </div>
  );
}
