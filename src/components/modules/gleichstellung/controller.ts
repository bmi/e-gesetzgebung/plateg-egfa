// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleGleichstellungControllerApi,
  EgfaModuleGleichstellungEntityDTOAllOf,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportGleichstellungsorientierteToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleGleichstellungControllerApi>('egfaGleichstellungControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule6({ egfaId }).subscribe({
    next: (gleichstellungsorientierteData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Gleichstellung,
          egfaDatenuebernahmeDTO: generateGleichstellungsorientierteJSON(gleichstellungsorientierteData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Gleichstellungsorientierte GFA data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Gleichstellungsorientierte GFA data', error);
    },
  });
};
export const generateGleichstellungsorientierteJSON = (
  gleichstellungsorientierteData: EgfaModuleEntityRequestDTO & EgfaModuleGleichstellungEntityDTOAllOf,
): EgfaDatenuebernahmeDTO => {
  const gleichstellungsorientierteJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'gleichstellungsorientierteErgebnisdokumentation',
    title: i18n.t(`egfa.modules.gleichstellung.name`),
    content: [],
    children: [],
  };
  if (gleichstellungsorientierteData.influenceExpected && gleichstellungsorientierteData.influenceSummary) {
    gleichstellungsorientierteJSONExport.content.push({
      id: 'gleichstellungsorientierteDataSummary',
      type: EgfaItemType.Text,
      data: gleichstellungsorientierteData.influenceSummary,
    });
  } else {
    gleichstellungsorientierteJSONExport.content.push({
      id: 'gleichstellungsorientierteDataSummary',
      type: EgfaItemType.Text,
      data: i18n
        .t(
          `egfa.modules.gleichstellung.gleichstellungZusammenfassung.irrelevant.${
            gleichstellungsorientierteData.maennerUndFrauenBetroffenExpected ? 'case1' : 'case2'
          }`,
        )
        .toString(),
    });
  }
  return gleichstellungsorientierteJSONExport;
};
