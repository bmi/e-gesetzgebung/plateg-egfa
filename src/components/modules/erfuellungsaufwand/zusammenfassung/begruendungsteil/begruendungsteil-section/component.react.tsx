// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './begruendungsteil-section.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';

import { GesamtsummenNormadressatenItemInterface } from '../../../gesamtsummenNormadressaten/controller';
import { VorblattTable } from '../../vorblatt-wrapper/vorblatt-table/component.react';
import { SummaryDefaultValues } from '../component.react';
import { VorhabenListTable } from '../items-list-table/component.react';
import { SummaryTextarea } from '../summaryTextarea/component.react';

interface BegruendungsteilSectionComponentInterface {
  egfaId: string;
  normadressat: EgfaErfuellungItemNormadressat;
  items: EgfaModuleErfuellungsaufwandItemEntityDTO[];
  restoreSummaryTextarea?: (fieldName: SummaryDefaultValues) => void;
  summaryFieldName: SummaryDefaultValues;
  summaryTableVals: GesamtsummenNormadressatenItemInterface[];
  isFertigstellung?: boolean;
  isExpertMode?: boolean;
}

export function BegruendungsteilSectionComponent(props: BegruendungsteilSectionComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.${props.normadressat}`;
  const vorblattTitle = `egfa.modules.erfuellungsaufwand.fertigstellung.gesamtsummenTables.${props.normadressat}`;
  const [vorgabenItems, setVorgabenItems] = useState<EgfaModuleErfuellungsaufwandItemEntityDTO[]>([]);

  const [informationspflichtenItems, setInformationspflichtenItems] = useState<
    EgfaModuleErfuellungsaufwandItemEntityDTO[]
  >([]);

  useEffect(() => {
    const filteredItems = props.items.filter((item) => item.normadressat === props.normadressat);
    if (props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft) {
      setInformationspflichtenItems(
        filteredItems.filter((item) => item.art === EgfaErfuellungItemArtType.Informationspflicht),
      );
      setVorgabenItems(filteredItems.filter((item) => item.art === EgfaErfuellungItemArtType.Vorgabe));
    } else {
      setVorgabenItems(filteredItems);
    }
  }, [props.items]);
  return (
    <div className="begruendungsteil-section">
      <VorblattTable
        sectionKey={props.normadressat}
        tableVals={props.summaryTableVals || []}
        egfaId={props.egfaId}
        title={props.isFertigstellung ? t(vorblattTitle) : t(`${translationKey}.title`)}
        isFertigstellung={props.isFertigstellung}
      />
      <VorhabenListTable
        title={t(`${translationKey}.jaehrlichenTitle`)}
        normadressat={props.normadressat}
        itemsList={vorgabenItems}
        egfaId={props.egfaId}
        type="jaehrlichen"
        isFertigstellung={props.isFertigstellung}
        isExpertMode={props.isExpertMode}
      />
      {props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft && (
        <VorhabenListTable
          title={t(`${translationKey}.informationspflichtenTitle`)}
          normadressat={props.normadressat}
          itemsList={informationspflichtenItems}
          egfaId={props.egfaId}
          type="jaehrlichen"
          art={EgfaErfuellungItemArtType.Informationspflicht}
          isFertigstellung={props.isFertigstellung}
          isExpertMode={props.isExpertMode}
        />
      )}
      <VorhabenListTable
        title={t(`${translationKey}.einmaligerTitle`)}
        normadressat={props.normadressat}
        itemsList={vorgabenItems}
        egfaId={props.egfaId}
        type="einmaliger"
        isFertigstellung={props.isFertigstellung}
        isExpertMode={props.isExpertMode}
      />
      {!props.isFertigstellung && (
        <SummaryTextarea
          normadressat={props.normadressat}
          fieldName={props.summaryFieldName}
          restoreSummaryTextarea={() => props.restoreSummaryTextarea?.(props.summaryFieldName)}
        />
      )}
    </div>
  );
}
