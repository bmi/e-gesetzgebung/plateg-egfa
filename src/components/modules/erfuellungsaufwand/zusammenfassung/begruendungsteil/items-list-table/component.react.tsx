// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './items-list-table.less';

import { Table } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';

import { generateVorhabenTableVals } from '../../vorblatt-wrapper/component.react';

interface VorhabenListTableInterface {
  title: string;
  normadressat: EgfaErfuellungItemNormadressat;
  itemsList: EgfaModuleErfuellungsaufwandItemEntityDTO[];
  egfaId: string;
  type: 'jaehrlichen' | 'einmaliger';
  art?: EgfaErfuellungItemArtType.Informationspflicht;
  isFertigstellung?: boolean;
  isExpertMode?: boolean;
}
interface DataRowTab {
  [s: string]: string | React.ReactElement;
}

export function VorhabenListTable(props: VorhabenListTableInterface): React.ReactElement {
  const { t } = useTranslation();
  const normadressatValue = props.normadressat === EgfaErfuellungItemNormadressat.Buerger ? 'Buerger' : '';
  const transaltionSuffix = props.art || normadressatValue;
  const translationsKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.vorhabenList.tableHeading${transaltionSuffix}`;
  const [data, setData] = useState<DataRowTab[]>([]);

  useEffect(() => {
    setData(generateVorhabenTableVals(props.itemsList, normadressatValue, props.isFertigstellung, props.egfaId));
  }, [props.itemsList]);

  let columns = [
    {
      title: t(`${translationsKey}.bezeichnung`),
      dataIndex: 'bezeichnung',
      fixed: false,
      className: 'bezeichnung-column',
    },
    {
      title: t(`${translationsKey}.personalaufwand`),
      dataIndex: `${props.type}Personalaufwand`,
      fixed: false,
      className: 'summary-column',
    },
    {
      title: t(`${translationsKey}.sachaufwand`),
      dataIndex: `${props.type}Sachaufwand`,
      fixed: false,
      className: 'summary-column',
    },
  ];

  if (props.art) {
    columns = [
      ...columns,
      {
        title: t(`${translationsKey}.einmaligerPersonalaufwand`),
        dataIndex: `einmaligerPersonalaufwand`,
        fixed: false,
        className: 'summary-column',
      },
      {
        title: t(`${translationsKey}.einmaligerSachaufwand`),
        dataIndex: `einmaligerSachaufwand`,
        fixed: false,
        className: 'summary-column',
      },
    ];
  }

  if (!normadressatValue) {
    const dataIndexPrefix = props.art || props.type;
    columns.push({
      title: t(`${translationsKey}.gesamtaufwand`),
      dataIndex: `${dataIndexPrefix.toLowerCase()}Gesamtaufwand`,
      fixed: false,
      className: 'summary-column',
    });
  }

  columns.push({
    title: '',
    dataIndex: 'empty-column',
    fixed: false,
    className: 'empty-column',
  });

  return (
    <div className="items-list-table-holder">
      <Title level={4}>{props.title}</Title>
      <Table columns={columns} dataSource={data} pagination={false} className="items-list-table" />
    </div>
  );
}
