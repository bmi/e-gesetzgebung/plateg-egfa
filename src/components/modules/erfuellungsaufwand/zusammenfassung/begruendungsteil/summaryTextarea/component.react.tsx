// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './summary-textarea.less';

import { Button } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { v1 as uuidv4 } from 'uuid';

import { EgfaErfuellungItemNormadressat } from '@plateg/rest-api';
import { FormItemWithInfo, Restore } from '@plateg/theme';

interface SummaryTextareaInterface {
  normadressat: EgfaErfuellungItemNormadressat;
  fieldName: string;
  restoreSummaryTextarea: () => void;
}

export function SummaryTextarea(props: SummaryTextareaInterface): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.summaryTextarea`;
  const uniqueId = uuidv4();
  return (
    <div className="summary-textarea-holder">
      <FormItemWithInfo
        name={props.fieldName}
        label={<span>{t(`${translationKey}.${props.normadressat}.label`)}</span>}
        rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
      <Button
        id={`restore-summary-text-${props.normadressat}`}
        size="small"
        icon={<Restore uniqueId={uniqueId} />}
        type="link"
        onClick={() => props.restoreSummaryTextarea()}
        className="blue-text-button"
      >
        {t(`${translationKey}.resetButton`)}
      </Button>
    </div>
  );
}
