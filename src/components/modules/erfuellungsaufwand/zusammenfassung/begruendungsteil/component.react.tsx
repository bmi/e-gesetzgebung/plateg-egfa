// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemNormadressat, EgfaModuleErfuellungsaufwandEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { SimpleModuleProps } from '../../../general/simple-module/auswirkungen/component.react';
import { generateSummaryTableVals, TablesValsState } from '../vorblatt-wrapper/component.react';
import { BegruendungsteilSectionComponent } from './begruendungsteil-section/component.react';

export interface ZusammenfassungBegruendungsteilComponentProps<P> extends SimpleModuleProps<P> {
  moduleName: string;
  sectionName: string;
  previousLinkName: string;
  nextLinkName: string;
  isLight?: boolean;
  handleFormChange: (() => void) | undefined;
}
export type SummaryDefaultValues = 'zusammenfassungBuerger' | 'zusammenfassungWirtschaft' | 'zusammenfassungVerwaltung';

export function ZusammenfassungBegruendungsteilComponent(
  props: ZusammenfassungBegruendungsteilComponentProps<EgfaModuleErfuellungsaufwandEntityDTO>,
): React.ReactElement {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const [isDirty, setIsDirty] = useState(false);
  const [summaryTablesVals, setSummaryTablesVals] = useState<TablesValsState>();
  const defaultTextKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.summaryTextarea`;
  const [summaryDefaultValues, setSummaryDefaultValues] = useState<{
    zusammenfassungBuerger: string;
    zusammenfassungWirtschaft: string;
    zusammenfassungVerwaltung: string;
  }>({
    zusammenfassungBuerger: t(`${defaultTextKey}.${EgfaErfuellungItemNormadressat.Buerger}.defaultText`, {
      jaehrlcherZeitaufwand: '0 Stunden',
      jaehrlicherSachaufwand: '0,00 €',
      einmaligerZeitaufwand: '0 Stunden',
      einmaligerSachaufwand: '0,00 €',
    }),
    zusammenfassungWirtschaft: t(`${defaultTextKey}.${EgfaErfuellungItemNormadressat.Wirtschaft}.defaultText`, {
      jaehrlicherPersonalaufwand: '0,00 €',
      jaehrlicherPersonalaufwandBuerokratiekosten: '0,00 €',
      einmaligerPersonalaufwand: '0,00 €',
    }),
    zusammenfassungVerwaltung: t(`${defaultTextKey}.${EgfaErfuellungItemNormadressat.Verwaltung}.defaultText`, {
      jaehrlicherPersonalaufwand: '0,00 €',
      bundesebeneJaehrlicherPersonalaufwand: '0,00 €',
      landesebeneJaehrlicherPersonalaufwand: '0,00 €',
      einmaligerPersonalaufwand: '0,00 €',
      bundesebeneEinmaligerPersonalaufwand: '0,00 €',
      landesebeneEinmaligerPersonalaufwand: '0,00 €',
    }),
  });

  type SetSuBuDeValArgType = {
    einmaligerSachaufwand: string;
    einmaligerPersonalaufwand: string;
    jaehrlichenSachaufwands: string;
    jaehrlichenPersonalaufwands: string;
  };

  // helper func to reduce complexity
  const setSummaryBuergerDefaultValues = (buergerinnenUndBuergerTableVals: SetSuBuDeValArgType[]) => {
    return t(`${defaultTextKey}.${EgfaErfuellungItemNormadressat.Buerger}.defaultText`, {
      jaehrlcherZeitaufwand: buergerinnenUndBuergerTableVals[0]
        ? buergerinnenUndBuergerTableVals[0].jaehrlichenPersonalaufwands.replace('Std.', 'Stunden')
        : '0 Stunden',
      jaehrlicherSachaufwand: buergerinnenUndBuergerTableVals[0]
        ? buergerinnenUndBuergerTableVals[0].jaehrlichenSachaufwands
        : '0,00 €',
      einmaligerZeitaufwand: buergerinnenUndBuergerTableVals[0]
        ? buergerinnenUndBuergerTableVals[0].einmaligerPersonalaufwand.replace('Std.', 'Stunden')
        : '0 Stunden',
      einmaligerSachaufwand: buergerinnenUndBuergerTableVals[0]
        ? buergerinnenUndBuergerTableVals[0].einmaligerSachaufwand
        : '0,00 €',
    });
  };

  useEffect(() => {
    form.setFieldsValue(props.formData);
    const clonedFormDataItems = structuredClone(props.formData?.items ?? []);
    const buergerinnenUndBuergerTableVals = generateSummaryTableVals(
      clonedFormDataItems,
      EgfaErfuellungItemNormadressat.Buerger,
      'Buerger',
      props.formData?.expertenModus,
    );
    const wirtschaftTableVals = generateSummaryTableVals(
      clonedFormDataItems,
      EgfaErfuellungItemNormadressat.Wirtschaft,
      '',
      props.formData?.expertenModus,
    );
    const verwaltungTableVals = generateSummaryTableVals(
      clonedFormDataItems,
      EgfaErfuellungItemNormadressat.Verwaltung,
    );

    const summaryBuergerDefaultValues = setSummaryBuergerDefaultValues(buergerinnenUndBuergerTableVals);

    const summaryWirtschaftDefaultValues = t(
      `${defaultTextKey}.${EgfaErfuellungItemNormadressat.Wirtschaft}.defaultText`,
      {
        jaehrlicherPersonalaufwand: wirtschaftTableVals[0]?.jaehrlichenPersonalaufwands ?? '0,00 €',
        jaehrlicherPersonalaufwandBuerokratiekosten: wirtschaftTableVals[1]?.jaehrlichenPersonalaufwands ?? '0,00 €',
        einmaligerPersonalaufwand: wirtschaftTableVals[0]?.einmaligerPersonalaufwand ?? '0,00 €',
        jaehrlicherSachaufwand: wirtschaftTableVals[0]?.jaehrlichenSachaufwands ?? '0,00 €',
        jaehrlicherSachaufwandInformationspflicht: wirtschaftTableVals[1]?.jaehrlichenSachaufwands ?? '0,00 €',
        einmaligerSachaufwand: wirtschaftTableVals[0]?.einmaligerSachaufwand ?? '0,00 €',
      },
    );

    const summaryVerwaltungDefaultValues = t(
      `${defaultTextKey}.${EgfaErfuellungItemNormadressat.Verwaltung}.defaultText`,
      {
        jaehrlicherPersonalaufwand: verwaltungTableVals[0]?.jaehrlichenSachaufwands ?? '0,00 €',
        bundesebeneJaehrlicherPersonalaufwand: verwaltungTableVals[1]?.jaehrlichenSachaufwands ?? '0,00 €',
        landesebeneJaehrlicherPersonalaufwand: verwaltungTableVals[2]?.jaehrlichenSachaufwands ?? '0,00 €',
        einmaligerPersonalaufwand: verwaltungTableVals[0]?.einmaligerSachaufwand ?? '0,00 €',
        bundesebeneEinmaligerPersonalaufwand: verwaltungTableVals[1]?.einmaligerSachaufwand ?? '0,00 €',
        landesebeneEinmaligerPersonalaufwand: verwaltungTableVals[2]?.einmaligerSachaufwand ?? '0,00 €',
      },
    );
    form.setFieldsValue({
      zusammenfassungBuerger: (props.formData?.zusammenfassungBuerger as string) ?? summaryBuergerDefaultValues,
      zusammenfassungWirtschaft:
        (props.formData?.zusammenfassungWirtschaft as string) ?? summaryWirtschaftDefaultValues,
      zusammenfassungVerwaltung:
        (props.formData?.zusammenfassungVerwaltung as string) ?? summaryVerwaltungDefaultValues,
    });
    setSummaryDefaultValues({
      zusammenfassungBuerger: summaryBuergerDefaultValues,
      zusammenfassungWirtschaft: summaryWirtschaftDefaultValues,
      zusammenfassungVerwaltung: summaryVerwaltungDefaultValues,
    });
    setIsDirty(false);

    setSummaryTablesVals({
      buergerinnenUndBuergerTableVals: buergerinnenUndBuergerTableVals,
      wirtschaftTableVals: wirtschaftTableVals,
      verwaltungTableVals: verwaltungTableVals,
    });
  }, [props.formData]);

  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };
  const restoreSummaryTextarea = (fieldName: SummaryDefaultValues) => {
    form.setFieldValue(fieldName, summaryDefaultValues[fieldName]);
    void form.validateFields([fieldName]);
  };

  return (
    <div className="zusammenfassung-div">
      <FormWrapper
        projectName={props.isLight ? 'eNAP' : 'eGFA'}
        title={<Title level={1}>{t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${routes.ALGEMEINEANGABEN}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) {
            props.handleFormChange();
          }
        }}
        showMandatoryFieldInfo={true}
      >
        <div className="intro-text">
          <p className="ant-typography p-no-style">
            {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.intro`)}
          </p>
          <Title level={2}>{t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.inhaltTitle`)}</Title>
          <p>{t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.inhaltText`)}</p>
        </div>
        <BegruendungsteilSectionComponent
          egfaId={props.egfaId}
          normadressat={EgfaErfuellungItemNormadressat.Buerger}
          items={props.formData?.items || []}
          restoreSummaryTextarea={restoreSummaryTextarea}
          summaryFieldName="zusammenfassungBuerger"
          summaryTableVals={summaryTablesVals?.buergerinnenUndBuergerTableVals || []}
          isExpertMode={props.formData?.expertenModus}
        />
        <BegruendungsteilSectionComponent
          egfaId={props.egfaId}
          normadressat={EgfaErfuellungItemNormadressat.Wirtschaft}
          items={props.formData?.items || []}
          restoreSummaryTextarea={restoreSummaryTextarea}
          summaryFieldName="zusammenfassungWirtschaft"
          summaryTableVals={summaryTablesVals?.wirtschaftTableVals || []}
          isExpertMode={props.formData?.expertenModus}
        />
        <BegruendungsteilSectionComponent
          egfaId={props.egfaId}
          normadressat={EgfaErfuellungItemNormadressat.Verwaltung}
          items={props.formData?.items || []}
          restoreSummaryTextarea={restoreSummaryTextarea}
          summaryFieldName="zusammenfassungVerwaltung"
          summaryTableVals={summaryTablesVals?.verwaltungTableVals || []}
          isExpertMode={props.formData?.expertenModus}
        />
        <div className="summary-textarea-holder">
          <Title level={4}>
            {t(
              `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.zusammenfassungBegruendungsteil.title`,
            )}
          </Title>
          <FormItemWithInfo
            name={'zusammenfassungBegruendungsteil'}
            label={
              <span>
                {t(
                  `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.zusammenfassungBegruendungsteil.label`,
                )}
              </span>
            }
            rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
          >
            <TextArea rows={8} />
          </FormItemWithInfo>
        </div>
      </FormWrapper>
    </div>
  );
}
