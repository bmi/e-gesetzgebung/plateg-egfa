// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../zusammenfassung.less';

import { Form, FormInstance, Radio, Select } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemNormadressat,
  EgfaErfuellungsaufwandAnfertigendeStelleType,
  EgfaErfuellungsaufwandTatbestandType,
  EgfaModuleErfuellungsaufwandEntityDTO,
} from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';
import { VorblattWrapper } from '../vorblatt-wrapper/component.react';

export interface ZusammenfassungComponentProps<P> extends ContentPageProps<P> {
  moduleName: string;
  sectionName: string;
  handleFormChange?: () => void;
}
interface CategoryItem {
  text: string;
  value: EgfaErfuellungsaufwandTatbestandType;
}

export function ZusammenfassungAlgemeineangaben(
  props: ZusammenfassungComponentProps<EgfaModuleErfuellungsaufwandEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const [categoryItems, setCategoryItems] = useState<CategoryItem[]>([]);
  const [lastVerwaltungId, setLastVerwaltungId] = useState<string>('');
  useEffect(() => {
    setCategoryItems(
      (Object.keys(EgfaErfuellungsaufwandTatbestandType) as (keyof typeof EgfaErfuellungsaufwandTatbestandType)[]).map(
        (item) => {
          const val = EgfaErfuellungsaufwandTatbestandType[item];
          return {
            text: t(
              `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetztenTatbestandOptions.${val}`,
            ),
            value: val,
          };
        },
      ),
    );
  }, []);
  const [isDirty, setIsDirty] = useState(false);
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };
  useEffect(() => {
    form.setFieldsValue({
      ...props.formData,
      anfertigendeStelle: props.formData?.anfertigendeStelle || EgfaErfuellungsaufwandAnfertigendeStelleType.Destatis,
    });
    setIsDirty(false);
    setLastVerwaltungId(
      props.formData?.items
        ?.filter((vorgabe) => vorgabe.normadressat === EgfaErfuellungItemNormadressat.Verwaltung)
        .sort((a, b) => Number(a.erstelltAm) - Number(b.erstelltAm))[0]?.id || '',
    );
  }, [props.formData]);

  return (
    <div className="erfuellungsaufwand-zusammenfassung-div zusammenfassung-div">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.VERWALTUNG}/${lastVerwaltungId}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${routes.BEGRUENDUNGSTEIL}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) props.handleFormChange();
        }}
        showMandatoryFieldInfo={true}
      >
        <div
          className="main-description"
          dangerouslySetInnerHTML={{ __html: t(`egfa.modules.erfuellungsaufwand.zusammenfassung.description`) }}
        />
        <Title className="main-title" level={2}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.ausarbeitungen`)}
        </Title>
        <Title level={3}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.anfertigendeWaehlen`)}
        </Title>
        <Form.Item name="anfertigendeStelle">
          <Radio.Group name="anfertigendeStelle" className="horizontal-radios">
            <Radio
              value={EgfaErfuellungsaufwandAnfertigendeStelleType.Destatis}
              id="egfa-erfuellungsaufwand-statistischesBundesamt-radio"
              className="horizontal-radios"
            >
              {t(
                `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.${EgfaErfuellungsaufwandAnfertigendeStelleType.Destatis}`,
              )}
            </Radio>
            <Radio
              value={EgfaErfuellungsaufwandAnfertigendeStelleType.Ressort}
              id="egfa-erfuellungsaufwand-resort-radio"
              className="horizontal-radios"
            >
              {t(
                `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.${EgfaErfuellungsaufwandAnfertigendeStelleType.Ressort}`,
              )}
            </Radio>
          </Radio.Group>
        </Form.Item>
        <div className="umgesetztenTatbestand-select-container">
          <FormItemWithInfo
            name="umgesetzterTatbestand"
            label={t('egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetztenTatbestandTitle')}
            rules={[
              {
                required: true,
                whitespace: true,
                message: t(
                  'egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetzterTatbestandRequired',
                ),
              },
            ]}
          >
            <Select
              placeholder={t(
                `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetztenTatbestandPlaceHolder`,
              )}
            >
              {categoryItems.map((category) => (
                <Select.Option key={category.value} value={category.value}>
                  {category.text}
                </Select.Option>
              ))}
            </Select>
          </FormItemWithInfo>
        </div>
        <div className="divider-div" />
        <Title level={2}>{t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.title`)}</Title>
        <VorblattWrapper egfaId={props.egfaId} formData={props.formData} />
        <div className="summary-textarea-holder">
          <Title level={4}>
            {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.eZusammenfassung`)}
          </Title>
          <FormItemWithInfo
            name="zusammenfassungVorblatt"
            label={t('egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.zusammenfassungVorblatt')}
            rules={[
              {
                required: true,
                whitespace: true,
                message: t(
                  'egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.zusammenfassungVorblattRequired',
                ),
              },
            ]}
          >
            <TextArea className="vorblatt-textarea" rows={8} />
          </FormItemWithInfo>
        </div>
      </FormWrapper>
    </div>
  );
}
