// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18next from 'i18next';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { GesamtsummenNormadressatenItemInterface } from '../../gesamtsummenNormadressaten/controller';
import { vollzugsebeneTypes } from '../../vorgabe-modal/component.react';
import { sumZeilenExpert, sumZeilenFormatter } from '../../vorgabe/controller';
import { VorblattTable } from './vorblatt-table/component.react';

interface VorblattWrapperProps {
  egfaId: string;
  formData: EgfaModuleErfuellungsaufwandEntityDTO | undefined;
  ergebnisdokumentation?: boolean;
}
export interface TablesValsState {
  buergerinnenUndBuergerTableVals: GesamtsummenNormadressatenItemInterface[];
  wirtschaftTableVals: GesamtsummenNormadressatenItemInterface[];
  verwaltungTableVals: GesamtsummenNormadressatenItemInterface[];
}
export const generateSummaryTableVals = (
  formDataItems: EgfaModuleErfuellungsaufwandItemEntityDTO[] | undefined,
  normadressat: EgfaErfuellungItemNormadressat,
  typBuerger?: '' | 'Buerger',
  isExpertMode?: boolean,
) => {
  const jaehrlichenPersonalaufwandsSum = {
    sum: 0,
    label: normadressat === EgfaErfuellungItemNormadressat.Buerger ? 'Std.' : '€',
  };
  const jaehrlichenSachaufwandsSum = { sum: 0, label: '€' };
  const einmaligerPersonalaufwandSum = {
    sum: 0,
    label: normadressat === EgfaErfuellungItemNormadressat.Buerger ? 'Std.' : '€',
  };
  const einmaligerSachaufwandSum = { sum: 0, label: '€' };

  const informationPflicht_jaehrlichenPersonalaufwandsSum = { sum: 0, label: '€' };
  const informationPflicht_jaehrlichenSachaufwandsSum = { sum: 0, label: '€' };
  const informationPflicht_einmaligerPersonalaufwandSum = { sum: 0, label: '€' };
  const informationPflicht_einmaligerSachaufwandSum = { sum: 0, label: '€' };

  const bund_jaehrlichenPersonalaufwandsSum = { sum: 0, label: '€' };
  const bund_jaehrlichenSachaufwandsSum = { sum: 0, label: '€' };
  const bund_einmaligerPersonalaufwandSum = { sum: 0, label: '€' };
  const bund_einmaligerSachaufwandSum = { sum: 0, label: '€' };

  const land_jaehrlichenPersonalaufwandsSum = { sum: 0, label: '€' };
  const land_jaehrlichenSachaufwandsSum = { sum: 0, label: '€' };
  const land_einmaligerPersonalaufwandSum = { sum: 0, label: '€' };
  const land_einmaligerSachaufwandSum = { sum: 0, label: '€' };

  const rowList = [];
  const sumMethod = sumZeilenExpert;
  formDataItems
    ?.filter((item) => item.normadressat === normadressat)
    .forEach((item) => {
      const jaehrlichenPersonalaufwands = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        typBuerger,
      );
      const jaehrlichenSachaufwands = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        typBuerger,
      );
      const einmaligerPersonalaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        typBuerger,
      );

      const einmaligerSachaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        typBuerger,
      );

      const aufwandstabellenBund = item.vollzugsebene === vollzugsebeneTypes.bund ? item.aufwandstabellen : [];
      const bundJaehrlichenPersonalaufwands = sumMethod(
        structuredClone(aufwandstabellenBund).filter(
          (personalaufwandJaehrlichItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandJaehrlichItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        typBuerger,
      );
      const bundPersonalaufwandEinmalig = sumMethod(
        structuredClone(aufwandstabellenBund).filter(
          (personalaufwandJaehrlichItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandJaehrlichItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        typBuerger,
      );

      const aufwandstabellenLand = item.vollzugsebene === vollzugsebeneTypes.land ? item.aufwandstabellen : [];
      const landJaehrlichenPersonalaufwands = sumMethod(
        structuredClone(aufwandstabellenLand).filter(
          (personalaufwandEinmaligItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandEinmaligItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        typBuerger,
      );

      const landPersonalaufwandEinmalig = sumMethod(
        structuredClone(aufwandstabellenLand).filter(
          (personalaufwandEinmaligItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandEinmaligItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        typBuerger,
      );

      const landJaehrlichenSachaufwands = sumMethod(
        structuredClone(aufwandstabellenLand).filter(
          (personalaufwandEinmaligItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandEinmaligItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        typBuerger,
      );

      const landEinmaligerSachaufwand = sumMethod(
        structuredClone(aufwandstabellenLand).filter(
          (personalaufwandEinmaligItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandEinmaligItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        typBuerger,
      );

      const bundJaehrlichenSachaufwands = sumMethod(
        structuredClone(aufwandstabellenBund).filter(
          (personalaufwandJaehrlichItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandJaehrlichItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        typBuerger,
      );

      const bundEinmaligerSachaufwand = sumMethod(
        structuredClone(aufwandstabellenBund).filter(
          (personalaufwandJaehrlichItem: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO) =>
            personalaufwandJaehrlichItem.gesamtsummeType ===
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        ),
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        typBuerger,
      );

      bund_jaehrlichenPersonalaufwandsSum.sum += bundJaehrlichenPersonalaufwands.sum;
      bund_einmaligerPersonalaufwandSum.sum += bundPersonalaufwandEinmalig.sum;
      land_jaehrlichenPersonalaufwandsSum.sum += landJaehrlichenPersonalaufwands.sum;
      land_einmaligerPersonalaufwandSum.sum += landPersonalaufwandEinmalig.sum;

      bund_einmaligerSachaufwandSum.sum += bundEinmaligerSachaufwand.sum;
      bund_jaehrlichenSachaufwandsSum.sum += bundJaehrlichenSachaufwands.sum;
      land_jaehrlichenSachaufwandsSum.sum += landJaehrlichenSachaufwands.sum;
      land_einmaligerSachaufwandSum.sum += landEinmaligerSachaufwand.sum;

      jaehrlichenPersonalaufwandsSum.sum += jaehrlichenPersonalaufwands.sum;
      jaehrlichenSachaufwandsSum.sum += jaehrlichenSachaufwands.sum;
      einmaligerPersonalaufwandSum.sum += einmaligerPersonalaufwand.sum;
      einmaligerSachaufwandSum.sum += einmaligerSachaufwand.sum;

      if (item.art === EgfaErfuellungItemArtType.Informationspflicht) {
        informationPflicht_jaehrlichenPersonalaufwandsSum.sum += jaehrlichenPersonalaufwands.sum;
        informationPflicht_jaehrlichenSachaufwandsSum.sum += jaehrlichenSachaufwands.sum;
        informationPflicht_einmaligerPersonalaufwandSum.sum += einmaligerPersonalaufwand.sum;
        informationPflicht_einmaligerSachaufwandSum.sum += einmaligerSachaufwand.sum;
      }
    });
  if (normadressat === EgfaErfuellungItemNormadressat.Wirtschaft) {
    rowList.push({
      id: 'informationPflichtSumme',
      key: 'informationPflichtSumme',
      bezeichnung: i18next.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.davonBuerokratiekosten`),
      jaehrlichenPersonalaufwands: sumZeilenFormatter(informationPflicht_jaehrlichenPersonalaufwandsSum),
      jaehrlichenSachaufwands: sumZeilenFormatter(informationPflicht_jaehrlichenSachaufwandsSum),
      einmaligerPersonalaufwand: sumZeilenFormatter(informationPflicht_einmaligerPersonalaufwandSum),
      einmaligerSachaufwand: sumZeilenFormatter(informationPflicht_einmaligerSachaufwandSum),
      summe: false,
      details: false,
      className: 'vorblatt-underlined',
    });
  }
  if (normadressat === EgfaErfuellungItemNormadressat.Verwaltung) {
    rowList.push({
      id: 'bundesebene',
      key: 'bundesebene',
      bezeichnung: i18next.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.bundesebene`),
      jaehrlichenPersonalaufwands: sumZeilenFormatter(bund_jaehrlichenPersonalaufwandsSum),
      jaehrlichenSachaufwands: sumZeilenFormatter(bund_jaehrlichenSachaufwandsSum),
      einmaligerPersonalaufwand: sumZeilenFormatter(bund_einmaligerPersonalaufwandSum),
      einmaligerSachaufwand: sumZeilenFormatter(bund_einmaligerSachaufwandSum),
      summe: false,
      details: false,
    });
    rowList.push({
      id: 'landesebene',
      key: 'landesebene',
      bezeichnung: i18next.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.landesebene`),
      jaehrlichenPersonalaufwands: sumZeilenFormatter(land_jaehrlichenPersonalaufwandsSum),
      jaehrlichenSachaufwands: sumZeilenFormatter(land_jaehrlichenSachaufwandsSum),
      einmaligerPersonalaufwand: sumZeilenFormatter(land_einmaligerPersonalaufwandSum),
      einmaligerSachaufwand: sumZeilenFormatter(land_einmaligerSachaufwandSum),
      summe: false,
      details: false,
      className: 'vorblatt-underlined',
    });
  }
  rowList.unshift({
    id: 'tableSumme',
    key: 'tableSumme',
    bezeichnung: 'Gesamtsumme',
    jaehrlichenPersonalaufwands: sumZeilenFormatter(jaehrlichenPersonalaufwandsSum),
    jaehrlichenSachaufwands: sumZeilenFormatter(jaehrlichenSachaufwandsSum),
    einmaligerPersonalaufwand: sumZeilenFormatter(einmaligerPersonalaufwandSum),
    einmaligerSachaufwand: sumZeilenFormatter(einmaligerSachaufwandSum),
    summe: true,
    details: false,
    className: `vorblatt-summe-row ${typBuerger === 'Buerger' ? 'vorblatt-underlined' : ''}`,
  });
  return rowList;
};

export const generateVorhabenTableVals = (
  formDataItems: EgfaModuleErfuellungsaufwandItemEntityDTO[] | undefined,
  normadressatValue: 'Buerger' | '' = '',
  isFertigstellung = false,
  egfaId?: string,
) => {
  const sumMethod = sumZeilenExpert;
  return (
    formDataItems?.map((item, index) => {
      const jaehrlichenPersonalaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
        normadressatValue,
      );
      const jaehrlichenSachaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
        normadressatValue,
      );
      const einmaligerPersonalaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
        normadressatValue,
      );

      const einmaligerSachaufwand = sumMethod(
        item.aufwandstabellen,
        EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
        normadressatValue,
      );

      const vorgabeName =
        !isFertigstellung && egfaId ? (
          <Link
            style={{ fontWeight: 500 }}
            to={`/egfa/${egfaId}/${routes.MODULE}/${
              routes.ERFUELLUNGSAUFWAND
            }/${item.normadressat.toLocaleLowerCase()}/${item.id || ''}`}
          >
            {item.bezeichnung}
          </Link>
        ) : (
          item.bezeichnung
        );
      return {
        key: `item-${index}`,
        bezeichnung: vorgabeName,
        jaehrlichenPersonalaufwand: sumZeilenFormatter(jaehrlichenPersonalaufwand),
        jaehrlichenSachaufwand: sumZeilenFormatter(jaehrlichenSachaufwand),
        einmaligerPersonalaufwand: sumZeilenFormatter(einmaligerPersonalaufwand),
        einmaligerSachaufwand: sumZeilenFormatter(einmaligerSachaufwand),
        jaehrlichenGesamtaufwand: sumZeilenFormatter({
          sum: jaehrlichenPersonalaufwand.sum + jaehrlichenSachaufwand.sum,
          label: jaehrlichenPersonalaufwand.label,
        }),
        einmaligerGesamtaufwand: sumZeilenFormatter({
          sum: einmaligerPersonalaufwand.sum + einmaligerSachaufwand.sum,
          label: einmaligerPersonalaufwand.label,
        }),
        informationspflichtGesamtaufwand: sumZeilenFormatter({
          sum:
            jaehrlichenPersonalaufwand.sum +
            jaehrlichenSachaufwand.sum +
            einmaligerPersonalaufwand.sum +
            einmaligerSachaufwand.sum,
          label: jaehrlichenPersonalaufwand.label,
        }),
      };
    }) || []
  );
};

export function VorblattWrapper(props: VorblattWrapperProps): React.ReactElement {
  const { t } = useTranslation();
  const [tablesVals, setTablesVals] = useState<TablesValsState>();
  const [isLinkDisabled, setIsLinkDisabled] = useState(false);

  useEffect(() => {
    const clonedFormDataItems = props.formData?.items ? structuredClone(props.formData.items) : [];
    setTablesVals({
      buergerinnenUndBuergerTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Buerger,
        'Buerger',
        props.formData?.expertenModus,
      ),
      wirtschaftTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Wirtschaft,
        '',
        props.formData?.expertenModus,
      ),
      verwaltungTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Verwaltung,
        '',
        props.formData?.expertenModus,
      ),
    });
    setIsLinkDisabled(
      props.ergebnisdokumentation || props.formData?.status === EgfaModuleStatusType.VollstaendigBearbeitet,
    );
  }, [props.formData]);
  return (
    <div className="inhalte-vorblatt">
      <VorblattTable
        sectionKey={EgfaErfuellungItemNormadressat.Buerger}
        tableVals={tablesVals?.buergerinnenUndBuergerTableVals || []}
        egfaId={props.egfaId}
        title={t(
          `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.${EgfaErfuellungItemNormadressat.Buerger.toLowerCase()}Title`,
        )}
        isFertigstellung={isLinkDisabled}
      />
      <VorblattTable
        sectionKey={EgfaErfuellungItemNormadressat.Wirtschaft}
        tableVals={tablesVals?.wirtschaftTableVals || []}
        egfaId={props.egfaId}
        title={t(
          `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.${EgfaErfuellungItemNormadressat.Wirtschaft.toLowerCase()}Title`,
        )}
        isFertigstellung={isLinkDisabled}
      />
      <VorblattTable
        sectionKey={EgfaErfuellungItemNormadressat.Verwaltung}
        tableVals={tablesVals?.verwaltungTableVals || []}
        egfaId={props.egfaId}
        title={t(
          `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.${EgfaErfuellungItemNormadressat.Verwaltung.toLowerCase()}Title`,
        )}
        isFertigstellung={isLinkDisabled}
      />
    </div>
  );
}
