// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vorblatt-table.less';

import { Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { EgfaErfuellungItemNormadressat } from '@plateg/rest-api';

import { routes } from '../../../../../../shares/routes';
import { GesamtsummenNormadressatenItemInterface } from '../../../gesamtsummenNormadressaten/controller';

interface VorblattTableProps {
  sectionKey: EgfaErfuellungItemNormadressat;
  tableVals: GesamtsummenNormadressatenItemInterface[];
  egfaId: string;
  title: string;
  isFertigstellung?: boolean;
}

export function VorblattTable(props: VorblattTableProps): React.ReactElement {
  const { t } = useTranslation();
  const typeTable = props.sectionKey;
  const columns: ColumnsType<GesamtsummenNormadressatenItemInterface> = [
    {
      title: t('egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.bezeichnung'),
      key: 'bezeichnung',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.bezeichnung}</Text>;
      },
      className: 'bezeichnung-column',
    },
    {
      title: t(
        `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.${typeTable}.jaehrlichenPersonalaufwands`,
      ),
      key: 'jaehrlichenPersonalaufwands',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.jaehrlichenPersonalaufwands}</Text>;
      },
      className: 'summary-column',
    },
    {
      title: t(
        `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.${typeTable}.jaehrlichenSachaufwands`,
      ),
      key: 'jaehrlichenSachaufwands',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.jaehrlichenSachaufwands}</Text>;
      },
      className: 'summary-column',
    },
    {
      title: t(
        `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.${typeTable}.einmaligerPersonalaufwand`,
      ),
      key: 'einmaligerPersonalaufwand',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.einmaligerPersonalaufwand}</Text>;
      },
      className: 'summary-column',
    },
    {
      title: t(
        `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.${typeTable}.einmaligerSachaufwand`,
      ),
      key: 'einmaligerSachaufwand',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.einmaligerSachaufwand}</Text>;
      },
      className: 'summary-column',
    },
    {
      title: '',
      key: 'empty-column',
      className: 'empty-column',
    },
  ];

  return (
    <>
      <div className="vorblatt-table-holder">
        <div className="title-div">
          {!props.isFertigstellung && (
            <Link
              to={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.GESAMTSUMMEN_NORMADRESSATEN}`}
              className="blue-text-button"
            >
              {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.zurBearbeitung`)}
            </Link>
          )}
          <Title level={!props.isFertigstellung ? 3 : 4}>{props.title}</Title>
        </div>
        <Table
          rowKey={(record) => record.id}
          rowClassName={(record) => (record.className as string) || ''}
          columns={columns}
          dataSource={props.tableVals}
          pagination={false}
          className="gesamtsummen-normadressaten-table"
        />
      </div>
    </>
  );
}
