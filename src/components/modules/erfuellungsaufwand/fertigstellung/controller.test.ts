// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { of, throwError } from 'rxjs';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleErfuellungsaufwandEntityResponseDTO,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { ErfuellungsaufwandExportJsonController } from './controller';

describe(`Test: erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor`, () => {
  const erfuellungsaufwandExportJsonCtrl = new ErfuellungsaufwandExportJsonController();
  let egfaControllerStub: sinon.SinonStub;
  let egfaModuleControllerStub: sinon.SinonStub;
  let i18nStub: sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    GlobalDI.getOrRegister(
      'egfaModuleErfuellungsaufwandControllerApi',
      () => new EgfaModuleErfuellungsaufwandControllerApi(),
    );
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleErfuellungsaufwandControllerApi.prototype, 'getEgfaModule9');

    i18nStub = Sinon.stub(i18n, 't');
  });
  const erfuellungsaufwandData: EgfaModuleErfuellungsaufwandEntityResponseDTO = {
    dto: {
      egfaId: 'ba2f89f2-533a-4f83-8af3-531f367c1f3e',
      title: 'Erfüllungsaufwand',
      influenceExpected: false,
      deleted: false,
      influenceSummary: '',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      items: [
        {
          normadressat: 'BUERGER',
          id: '91ec5a73-9f30-4bfe-b4ad-0f80ecc30214',
          erstelltAm: '2023-07-13T12:57:15Z',
          spiegelvorgabenIds: [],
          fallgruppe: null,
          bezeichnung: 'Second Buerger',
          art: 'VORGABE',
          berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
          paragrafUndRechtsnorm: '2 Paragraf ',
          bemerkungen: '2 Bemerkungen',
          vollzugsebene: null,
          zusammenfassung: null,
          aufwandstabellen: [
            {
              aufwandstabellenZeilen: [
                {
                  id: 'b88b8f35-67af-4411-aa7a-4b0b09498f75',
                  summe: true,
                  erstelltAm: '2023-07-13T12:57:15Z',
                  bezeichnung: 'Expert Mode',
                  bemerkungen: null,
                  berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
                  orderIndex: -1,
                  einheit: null,
                  wirtschaftszweig: null,
                  vollzugsebene: null,
                  zeitaufwandBisher: null,
                  zeitaufwandNeu: null,
                  zeitaufwandDifferenz: 120,
                  lohnsatzBisher: null,
                  lohnsatzNeu: null,
                  lohnsatzDifferenz: null,
                  personalaufwandBisher: null,
                  personalaufwandNeu: null,
                  personalaufwandDifferenz: null,
                  sachaufwandBisher: null,
                  sachaufwandNeu: null,
                  sachaufwandDifferenz: null,
                  anzahlBisher: null,
                  anzahlNeu: null,
                  anzahlDifferenz: null,
                  lohnkosten: null,
                  personalaufwandProFall: null,
                  zeitaufwandProFall: null,
                  sachaufwandProFall: null,
                  personalaufwandSumme: 4,
                  arbeitszeitInStunden: null,
                  gewichteterLohnsatz: null,
                },
              ],
              berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
              gesamtsummeType: 'PERSONALAUFWAND_JAEHRLICH',
              kategorie: null,
              veraenderungAnzahl: null,
              veraenderungHaeufigkeit: null,
              veraenderungFallzahl: 2,
              veraenderungHinweise: null,
            },
            {
              aufwandstabellenZeilen: [
                {
                  id: 'b50762e0-7441-489a-9080-b926ce5b426a',
                  summe: true,
                  erstelltAm: '2023-07-13T12:57:15Z',
                  bezeichnung: 'Expert Mode',
                  bemerkungen: null,
                  berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
                  orderIndex: -1,
                  einheit: null,
                  wirtschaftszweig: null,
                  vollzugsebene: null,
                  zeitaufwandBisher: null,
                  zeitaufwandNeu: null,
                  zeitaufwandDifferenz: null,
                  lohnsatzBisher: null,
                  lohnsatzNeu: null,
                  lohnsatzDifferenz: null,
                  personalaufwandBisher: null,
                  personalaufwandNeu: null,
                  personalaufwandDifferenz: null,
                  sachaufwandBisher: null,
                  sachaufwandNeu: null,
                  sachaufwandDifferenz: 200,
                  anzahlBisher: null,
                  anzahlNeu: null,
                  anzahlDifferenz: null,
                  lohnkosten: null,
                  personalaufwandProFall: null,
                  zeitaufwandProFall: null,
                  sachaufwandProFall: null,
                  personalaufwandSumme: 2000,
                  arbeitszeitInStunden: null,
                  gewichteterLohnsatz: null,
                },
              ],
              berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
              gesamtsummeType: 'SACHAUFWAND_EINMALIG',
              kategorie: null,
              veraenderungAnzahl: null,
              veraenderungHaeufigkeit: null,
              veraenderungFallzahl: 10,
              veraenderungHinweise: null,
            },
            {
              aufwandstabellenZeilen: [
                {
                  id: '33ce1f02-0634-4600-9d4a-919b2f683262',
                  summe: true,
                  erstelltAm: '2023-07-13T12:57:15Z',
                  bezeichnung: 'Expert Mode',
                  bemerkungen: null,
                  berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
                  orderIndex: -1,
                  einheit: null,
                  wirtschaftszweig: null,
                  vollzugsebene: null,
                  zeitaufwandBisher: null,
                  zeitaufwandNeu: null,
                  zeitaufwandDifferenz: 15,
                  lohnsatzBisher: null,
                  lohnsatzNeu: null,
                  lohnsatzDifferenz: null,
                  personalaufwandBisher: null,
                  personalaufwandNeu: null,
                  personalaufwandDifferenz: null,
                  sachaufwandBisher: null,
                  sachaufwandNeu: null,
                  sachaufwandDifferenz: null,
                  anzahlBisher: null,
                  anzahlNeu: null,
                  anzahlDifferenz: null,
                  lohnkosten: null,
                  personalaufwandProFall: null,
                  zeitaufwandProFall: null,
                  sachaufwandProFall: null,
                  personalaufwandSumme: 2,
                  arbeitszeitInStunden: null,
                  gewichteterLohnsatz: null,
                },
              ],
              berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
              gesamtsummeType: 'PERSONALAUFWAND_EINMALIG',
              kategorie: null,
              veraenderungAnzahl: null,
              veraenderungHaeufigkeit: null,
              veraenderungFallzahl: 8,
              veraenderungHinweise: null,
            },
            {
              aufwandstabellenZeilen: [
                {
                  id: '7fa9fd84-a245-4813-80a5-c471740ee785',
                  summe: true,
                  erstelltAm: '2023-07-13T12:57:15Z',
                  bezeichnung: 'Expert Mode',
                  bemerkungen: null,
                  berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
                  orderIndex: -1,
                  einheit: null,
                  wirtschaftszweig: null,
                  vollzugsebene: null,
                  zeitaufwandBisher: null,
                  zeitaufwandNeu: null,
                  zeitaufwandDifferenz: null,
                  lohnsatzBisher: null,
                  lohnsatzNeu: null,
                  lohnsatzDifferenz: null,
                  personalaufwandBisher: null,
                  personalaufwandNeu: null,
                  personalaufwandDifferenz: null,
                  sachaufwandBisher: null,
                  sachaufwandNeu: null,
                  sachaufwandDifferenz: 200,
                  anzahlBisher: null,
                  anzahlNeu: null,
                  anzahlDifferenz: null,
                  lohnkosten: null,
                  personalaufwandProFall: null,
                  zeitaufwandProFall: null,
                  sachaufwandProFall: null,
                  personalaufwandSumme: 1000,
                  arbeitszeitInStunden: null,
                  gewichteterLohnsatz: null,
                },
              ],
              berechnungsart: 'GESAMTAUFWAND_PRO_FALL',
              gesamtsummeType: 'SACHAUFWAND_JAEHRLICH',
              kategorie: null,
              veraenderungAnzahl: null,
              veraenderungHaeufigkeit: null,
              veraenderungFallzahl: 5,
              veraenderungHinweise: null,
            },
          ],
        },
      ],
      anfertigendeStelle: 'DESTATIS',
      umgesetzterTatbestand: 'RECHTSPRECHUNG_BVERFG',
      zusammenfassungVorblatt: 'TEXT für das Vorblatt',
      zusammenfassungBegruendungsteil: 'Erfüllungsaufwand Zusammenfassung für den Begründungsteil',
      zusammenfassungWirtschaft:
        'Für die Wirtschaft entsteht jährlicher Personalaufwand in Höhe von 183 €, davon entfallen auf Bürokratiekosten aus Informationspflichten 100 €.\nEs entsteht außerdem einmaliger Personalaufwand in Höhe von 4.039 €. \n\nZudem entstehen jährliche Sachkosten in Höhe von 2.500 €, davon 2.000 € aus Informationspflichten. Es entsteht einmaliger Sachaufwand in Höhe von 3.560 €.',
      zusammenfassungVerwaltung:
        'Für die Verwaltung entsteht jährlicher Sachaufwand in Höhe von 6.000 €, davon entfallen auf die Bundesebene -- € und auf die Landesebene -- €. \nEs entsteht außerdem einmaliger Sachaufwand in Höhe von 11.200 €, davon entfallen auf die Bundesebene -- € und auf die Landesebene -- €. \nDiese Kosten beinhalten ebenfalls den Umstellungsaufwand.',
      zusammenfassungBuerger:
        'Für die Bürgerinnen und Bürger entsteht ein jährlicher Zeitaufwand in Höhe von 6 Stunden. Es entsteht dazu einmaliger Zeitaufwand in Höhe von 3 Stunden. \nEs entsteht außerdem jährlicher Sachaufwand in Höhe von 1.300 € sowie einmaliger Sachaufwand in Höhe von 2.500 €.',
      expertenModus: true,
      typ: 'ERFUELLUNGSAUFWAND',
    },
  };
  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
    i18nStub.resetHistory();
  });
  it(`correct erfuellungsaufwand data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(of(erfuellungsaufwandData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');

    // Call the function under test (erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor)
    let result: boolean;
    erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      const expectedOutput = {
        id: 'erfuellungsaufwandErgebnisdokumentation',
        content: [],
        children: [
          {
            id: 'allgemeineInformationen',
            title: 'Allgemeine Informationen',
            content: [
              {
                id: 'allgemeineInformationenAnfertigung',
                data: 'Anfertigung:  Statistisches Bundesamt (Destatis)',
                type: 'TEXT',
              },
              {
                id: 'allgemeineInformationenUmgesetzten',
                data: 'Umgesetzter Tatbestand:  Rechtsprechung BVerfG',
                type: 'TEXT',
              },
            ],
          },
          {
            id: 'vorblatt',
            title: 'Vorblatt',
            content: [],
            children: [
              {
                id: 'vorblattE',
                title: 'E. Erfüllungsaufwand',
                content: [
                  {
                    id: 'textVorblattE',
                    data: 'TEXT für das Vorblatt',
                    type: 'TEXT',
                  },
                ],
                children: [
                  {
                    id: 'vorblattBUERGERTable',
                    title: 'E. 1 Erfüllungsaufwand für Bürgerinnen und Bürger',
                    content: [
                      {
                        id: 'vorblattBuergerTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Zeitaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Zeitaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '4 Std.', '1.000 €', '2 Std.', '2.000 €'],
                        ],
                        type: 'TABLE',
                      },
                    ],
                  },
                  {
                    id: 'vorblattWIRTSCHAFTTable',
                    title: 'E. 2 Erfüllungsaufwand für die Wirtschaft',
                    content: [
                      {
                        id: 'vorblattBuergerTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Personalaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Personalaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Bürokratiekosten', '-- €', '-- €', '-- €', '-- €'],
                        ],
                        type: 'TABLE',
                      },
                    ],
                  },
                  {
                    id: 'vorblattVERWALTUNGTable',
                    title: 'E. 3 Erfüllungsaufwand für die Verwaltung',
                    content: [
                      {
                        id: 'vorblattBuergerTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Personalaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Personalaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Bundesebene', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Landesebene', '-- €', '-- €', '-- €', '-- €'],
                        ],
                        type: 'TABLE',
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            id: 'begruendung',
            title: 'Begründung - Allgemeiner Teil',
            content: [],
            children: [
              {
                id: 'zusammenfassungBegruendungsteil',
                title: 'A. VI. 4 Erfüllungsaufwand',
                content: [
                  {
                    id: 'textZusammenfassungBegruendungsteil',
                    data: 'Erfüllungsaufwand Zusammenfassung für den Begründungsteil',
                    type: 'TEXT',
                  },
                ],
                children: [],
              },
              {
                id: 'zusammenfassungBegruendungBUERGER',
                title: 'A. VI. 4. a) Erfüllungsaufwand für Bürgerinnen und Bürger',
                content: [
                  {
                    id: 'textZusammenfassungBegruendungBUERGER',
                    data: 'Für die Bürgerinnen und Bürger entsteht ein jährlicher Zeitaufwand in Höhe von 6 Stunden. Es entsteht dazu einmaliger Zeitaufwand in Höhe von 3 Stunden. \nEs entsteht außerdem jährlicher Sachaufwand in Höhe von 1.300 € sowie einmaliger Sachaufwand in Höhe von 2.500 €.',
                    type: 'TEXT',
                  },
                ],
                children: [
                  {
                    id: 'zusammenfassungBegruendungBUERGERgesamtsummenTable',
                    title: 'Gesamtsummen des Erfüllungsaufwandes für Bürgerinnen und Bürger',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungBUERGERgesamtsummenTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Zeitaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Zeitaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '4 Std.', '1.000 €', '2 Std.', '2.000 €'],
                        ],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungBUERGERjaehrlichenTable',
                    title: 'Veränderung des jährlichen Aufwandes für Bürgerinnen und Bürger',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungBUERGERjaehrlichenTableData',
                        data: [
                          ['Bezeichnung', 'Zeitaufwand Gesamt in Std.', 'Sachaufwand Gesamt'],
                          ['Second Buerger', '4 Std.', '1.000 €'],
                        ],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungBUERGEReinmaligerTable',
                    title: 'Einmaliger Aufwand für Bürgerinnen und Bürger',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungBUERGEReinmaligerTableData',
                        data: [
                          ['Bezeichnung', 'Zeitaufwand Gesamt in Std.', 'Sachaufwand Gesamt'],
                          ['Second Buerger', '2 Std.', '2.000 €'],
                        ],
                      },
                    ],
                    children: [],
                  },
                ],
              },
              {
                id: 'zusammenfassungBegruendungWIRTSCHAFT',
                title: 'A. VI. 4 b) Erfüllungsaufwand für die Wirtschaft',
                content: [
                  {
                    id: 'textZusammenfassungBegruendungWIRTSCHAFT',
                    data: 'Für die Wirtschaft entsteht jährlicher Personalaufwand in Höhe von 183 €, davon entfallen auf Bürokratiekosten aus Informationspflichten 100 €.\nEs entsteht außerdem einmaliger Personalaufwand in Höhe von 4.039 €. \n\nZudem entstehen jährliche Sachkosten in Höhe von 2.500 €, davon 2.000 € aus Informationspflichten. Es entsteht einmaliger Sachaufwand in Höhe von 3.560 €.',
                    type: 'TEXT',
                  },
                ],
                children: [
                  {
                    id: 'zusammenfassungBegruendungWIRTSCHAFTgesamtsummenTable',
                    title: 'Gesamtsummen des Erfüllungsaufwands für die Wirtschaft',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungWIRTSCHAFTgesamtsummenTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Personalaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Personalaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Bürokratiekosten', '-- €', '-- €', '-- €', '-- €'],
                        ],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungWIRTSCHAFTjaehrlichenTable',
                    title: 'Veränderung des jährlichen Aufwandes für die Wirtschaft (nur weitere Vorgaben)',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungWIRTSCHAFTjaehrlichenTableData',
                        data: [['Bezeichnung', 'Personalaufwand Gesamt', 'Sachaufwand Gesamt', 'Gesamtaufwand']],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungWIRTSCHAFTinformationspflichtenTable',
                    title: 'Bürokratiekosten aus Informationspflichten (ohne weitere Vorgaben)',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungWIRTSCHAFTinformationspflichtenTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Personalaufwand Gesamt',
                            'Jährlicher Sachaufwand Gesamt',
                            'Einmaliger Personalaufwand Gesamt',
                            'Einmaliger Sachaufwand Gesamt',
                            'Gesamtaufwand',
                          ],
                        ],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungWIRTSCHAFTeinmaligerTable',
                    title: 'Einmaliger Aufwand für die Wirtschaft',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungWIRTSCHAFTeinmaligerTableData',
                        data: [['Bezeichnung', 'Personalaufwand Gesamt', 'Sachaufwand Gesamt', 'Gesamtaufwand']],
                      },
                    ],
                    children: [],
                  },
                ],
              },
              {
                id: 'zusammenfassungBegruendungVERWALTUNG',
                title: 'A. VI. 4. 3 Erfüllungsaufwand für die Verwaltung',
                content: [
                  {
                    id: 'textZusammenfassungBegruendungVERWALTUNG',
                    data: 'Für die Verwaltung entsteht jährlicher Sachaufwand in Höhe von 6.000 €, davon entfallen auf die Bundesebene -- € und auf die Landesebene -- €. \nEs entsteht außerdem einmaliger Sachaufwand in Höhe von 11.200 €, davon entfallen auf die Bundesebene -- € und auf die Landesebene -- €. \nDiese Kosten beinhalten ebenfalls den Umstellungsaufwand.',
                    type: 'TEXT',
                  },
                ],
                children: [
                  {
                    id: 'zusammenfassungBegruendungVERWALTUNGgesamtsummenTable',
                    title: 'Gesamtsummen des Erfüllungsaufwands für die Verwaltung',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungVERWALTUNGgesamtsummenTableData',
                        data: [
                          [
                            'Bezeichnung',
                            'Jährlicher Personalaufwand',
                            'Jährlicher Sachaufwand',
                            'Einmaliger Personalaufwand',
                            'Einmaliger Sachaufwand',
                          ],
                          ['Gesamtsumme', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Bundesebene', '-- €', '-- €', '-- €', '-- €'],
                          ['davon Landesebene', '-- €', '-- €', '-- €', '-- €'],
                        ],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungVERWALTUNGjaehrlichenTable',
                    title: 'Veränderung des jährlichen Aufwandes der Verwaltung',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungVERWALTUNGjaehrlichenTableData',
                        data: [['Bezeichnung', 'Personalaufwand Gesamt', 'Sachaufwand Gesamt', 'Gesamtaufwand']],
                      },
                    ],
                    children: [],
                  },
                  {
                    id: 'zusammenfassungBegruendungVERWALTUNGeinmaligerTable',
                    title: 'Einmaliger Aufwand der Verwaltung',
                    content: [
                      {
                        type: 'TABLE',
                        id: 'zusammenfassungBegruendungVERWALTUNGeinmaligerTableData',
                        data: [['Bezeichnung', 'Personalaufwand Gesamt', 'Sachaufwand Gesamt', 'Gesamtaufwand']],
                      },
                    ],
                    children: [],
                  },
                ],
              },
            ],
          },
        ],
      };
      // Assert the result

      egfaControllerStub.calledWith({
        id: 'ba2f89f2-533a-4f83-8af3-531f367c1f3e',
        moduleType: EgfaModuleType.Erfuellungsaufwand,
        egfaDatenuebernahmeDTO: expectedOutput,
      });

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });

  it(` getting eGFA error`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(throwError(new Error('error getting eGFA')));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor)
    let result: boolean;
    erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      Sinon.assert.notCalled(egfaControllerStub);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`error on sending to editor`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(of(erfuellungsaufwandData));
    egfaControllerStub.returns(throwError(new Error('error getting eGFA')));
    i18nStub.returns('some text');
    // Call the function under test (erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor)
    let result: boolean;
    erfuellungsaufwandExportJsonCtrl.exportErfuellungsaufwandToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
