// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { map, switchMap } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemNormadressat,
  EgfaItemType,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

import {
  generateSummaryTableVals,
  generateVorhabenTableVals,
} from '../zusammenfassung/vorblatt-wrapper/component.react';

type ZusammenfassungTypes = 'zusammenfassungBuerger' | 'zusammenfassungWirtschaft' | 'zusammenfassungVerwaltung';
interface TableRowData {
  [s: string]: string | React.ReactElement | boolean;
}

export class ErfuellungsaufwandExportJsonController {
  public erfuellungData: EgfaModuleErfuellungsaufwandEntityDTO = {};

  public exportErfuellungsaufwandToEditor(
    egfaId: string,
    onFinish: (success: boolean, error?: AjaxError) => void,
  ): void {
    const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
    const egfaModuleErfuellungsaufwandController = GlobalDI.get<EgfaModuleErfuellungsaufwandControllerApi>(
      'egfaModuleErfuellungsaufwandControllerApi',
    );
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    loadingStatusController.setLoadingStatus(true);
    egfaModuleErfuellungsaufwandController
      .getEgfaModule9({ egfaId })
      .pipe(
        map((erfuellungData) => {
          this.erfuellungData = erfuellungData.dto;
          const erfuellungsaufwandJSONExport = this.createErfuellungsaufwandJSON();
          return erfuellungsaufwandJSONExport;
        }),
        switchMap((erfuellungsaufwandJSONExportData) => {
          return egfaController.sendEgfaDatenuebernahme({
            id: egfaId,
            moduleType: EgfaModuleType.Erfuellungsaufwand,
            egfaDatenuebernahmeDTO: erfuellungsaufwandJSONExportData,
          });
        }),
      )
      .subscribe({
        next: () => {
          onFinish(true);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error) => {
          onFinish(false, error as AjaxError);
          loadingStatusController.setLoadingStatus(false);
          console.error('Erfuellungsaufwand data export to Editor failed', error);
        },
      });
  }

  public createErfuellungsaufwandJSON(): EgfaDatenuebernahmeDTO {
    const erfuellungsaufwandJSONExport: EgfaDatenuebernahmeDTO = {
      id: 'erfuellungsaufwandErgebnisdokumentation',
      content: [],
      children: [],
    };
    erfuellungsaufwandJSONExport?.children?.push(this.prepareAllgemeineInformationen());
    erfuellungsaufwandJSONExport?.children?.push(this.prepareVorblatt());
    erfuellungsaufwandJSONExport?.children?.push(this.prepareBegruendung());

    return erfuellungsaufwandJSONExport;
  }
  private prepareAllgemeineInformationen(): EgfaDatenuebernahmeDTO {
    return {
      id: 'allgemeineInformationen',
      title: i18n.t(`egfa.modules.erfuellungsaufwand.fertigstellung.generalInfo.title`),
      content: [
        {
          id: 'allgemeineInformationenAnfertigung',
          data:
            i18n.t(`egfa.modules.erfuellungsaufwand.fertigstellung.generalInfo.anfertigungTitle`) +
            ' ' +
            i18n.t(
              `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.${
                this.erfuellungData.anfertigendeStelle as string
              }`,
            ),
          type: EgfaItemType.Text,
        },
        {
          id: 'allgemeineInformationenUmgesetzten',
          data:
            i18n.t(`egfa.modules.erfuellungsaufwand.fertigstellung.generalInfo.umgesetzterTitle`) +
            ' ' +
            i18n.t(
              `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetztenTatbestandOptions.${
                this.erfuellungData.umgesetzterTatbestand as string
              }`,
            ),
          type: EgfaItemType.Text,
        },
      ],
    };
  }

  private prepareVorblatt(): EgfaDatenuebernahmeDTO {
    return {
      id: 'vorblatt',
      title: i18n.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.title`),
      content: [],
      children: [
        {
          id: 'vorblattE',
          title: i18n.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.eZusammenfassung`),
          content: [
            {
              id: 'textVorblattE',
              data: this.erfuellungData.zusammenfassungVorblatt,
              type: EgfaItemType.Text,
            },
          ],
          children: [
            ...[
              EgfaErfuellungItemNormadressat.Buerger,
              EgfaErfuellungItemNormadressat.Wirtschaft,
              EgfaErfuellungItemNormadressat.Verwaltung,
            ].map((key) => {
              const summaryTableVals = generateSummaryTableVals(
                this.erfuellungData.items,
                key,
                key === EgfaErfuellungItemNormadressat.Buerger ? 'Buerger' : '',
              );
              return {
                id: `vorblatt${key}Table`,
                title: i18n.t(
                  `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.${key.toLowerCase()}Title`,
                ),
                content: [
                  {
                    id: `vorblatt${key}TableData`,
                    data: [
                      this.getTableHeadings(key),
                      ...this.extractTableData(summaryTableVals, [
                        'bezeichnung',
                        'jaehrlichenPersonalaufwands',
                        'jaehrlichenSachaufwands',
                        'einmaligerPersonalaufwand',
                        'einmaligerSachaufwand',
                      ]),
                    ],
                    type: EgfaItemType.Table,
                  },
                ],
              };
            }),
          ],
        },
      ],
    };
  }

  private prepareBegruendung(): EgfaDatenuebernahmeDTO {
    return {
      id: 'begruendung',
      title: i18n.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.inhaltTitle`),
      content: [],
      children: [
        // To prepare section with A. VI. 4 Erfüllungsaufwand
        {
          id: 'zusammenfassungBegruendungsteil',
          title: i18n.t(
            `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.zusammenfassungBegruendungsteil.title`,
          ),
          content: [
            {
              id: 'textZusammenfassungBegruendungsteil',
              data: this.erfuellungData.zusammenfassungBegruendungsteil,
              type: EgfaItemType.Text,
            },
          ],
          children: [],
        },
        // To prepare sections:
        // - A. VI. 4. a) Erfüllungsaufwand für Bürgerinnen und Bürger
        // - A. VI. 4 b) Erfüllungsaufwand für die Wirtschaft
        // - A. VI. 4. 3 Erfüllungsaufwand für die Verwaltung
        ...[
          EgfaErfuellungItemNormadressat.Buerger,
          EgfaErfuellungItemNormadressat.Wirtschaft,
          EgfaErfuellungItemNormadressat.Verwaltung,
        ].map((normadressatItem) => {
          const zusammenfassungFieldName: ZusammenfassungTypes = `zusammenfassung${
            normadressatItem.charAt(0) + normadressatItem.toLowerCase().slice(1)
          }` as ZusammenfassungTypes;

          return {
            id: `zusammenfassungBegruendung${normadressatItem}`,
            title: i18n.t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.${normadressatItem}.title`),
            content: [
              {
                id: `textZusammenfassungBegruendung${normadressatItem}`,
                data: this.erfuellungData[zusammenfassungFieldName],
                type: EgfaItemType.Text,
              },
            ],
            children: this.prepareBegruendungChildren(normadressatItem),
          };
        }),
      ],
    };
  }

  private prepareBegruendungChildren(normadressatItem: EgfaErfuellungItemNormadressat): EgfaDatenuebernahmeDTO[] {
    // If it is a Buerger we have different table headings and less collumns in table
    const isBuerger = normadressatItem === EgfaErfuellungItemNormadressat.Buerger ? 'Buerger' : '';
    // Prepare data for summary table
    const summaryTableVals = generateSummaryTableVals(this.erfuellungData.items, normadressatItem, isBuerger);

    // Take only items for current normadressat
    let filteredItems = this.erfuellungData.items?.filter((item) => item.normadressat === normadressatItem);
    // To have 3 common types of tabels
    const tableNames = ['gesamtsummen', 'jaehrlichen', 'einmaliger'];

    // If normadressat is Wirtschaft have to separate Vorgabe and Informationspflicht
    let ipItems;
    if (normadressatItem === EgfaErfuellungItemNormadressat.Wirtschaft) {
      ipItems = filteredItems?.filter((item) => item.art === EgfaErfuellungItemArtType.Informationspflicht);
      filteredItems = filteredItems?.filter((item) => item.art === EgfaErfuellungItemArtType.Vorgabe);
      // To add 3rd informationspflichten type for Wirtschaft
      tableNames.splice(2, 0, 'informationspflichten');
    }

    // Prepare data for rest of the tables
    const vorhabenTableVals = generateVorhabenTableVals(filteredItems, isBuerger);
    const ipTableVals = generateVorhabenTableVals(ipItems, isBuerger);

    const result = tableNames.map((tableName) => {
      // Fill data for common tables
      const childrenId = `zusammenfassungBegruendung${normadressatItem}${tableName}Table`;
      let titleKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.${normadressatItem}.${tableName}Title`;
      let tableHeadings = this.getTableHeadings(normadressatItem, true, !isBuerger.length);
      let tableData = this.extractTableData(vorhabenTableVals, [
        'bezeichnung',
        `${tableName}Personalaufwand`,
        `${tableName}Sachaufwand`,
        !isBuerger.length ? `${tableName}Gesamtaufwand` : '',
      ]);
      // Fill data for gesamtsummen table
      if (tableName === 'gesamtsummen') {
        titleKey = `egfa.modules.erfuellungsaufwand.fertigstellung.${tableName}Tables.${normadressatItem}`;
        tableHeadings = this.getTableHeadings(normadressatItem);
        tableData = this.extractTableData(summaryTableVals, [
          'bezeichnung',
          'jaehrlichenPersonalaufwands',
          'jaehrlichenSachaufwands',
          'einmaligerPersonalaufwand',
          'einmaligerSachaufwand',
        ]);
      }
      // Fill data for informationspflichten table
      if (tableName === 'informationspflichten') {
        tableHeadings = this.getTableHeadings(
          normadressatItem,
          false,
          true,
          EgfaErfuellungItemArtType.Informationspflicht,
        );
        tableData = this.extractTableData(ipTableVals, [
          'bezeichnung',
          'jaehrlichenPersonalaufwand',
          'jaehrlichenSachaufwand',
          'einmaligerPersonalaufwand',
          'einmaligerSachaufwand',
          'informationspflichtGesamtaufwand',
        ]);
      }
      return {
        id: `${childrenId}`,
        title: i18n.t(titleKey),
        content: [
          {
            type: EgfaItemType.Table,
            id: `${childrenId}Data`,
            data: [tableHeadings, ...tableData],
          },
        ],
        children: [],
      };
    });

    return result;
  }

  private extractTableData(tableVals: TableRowData[], keys: string[]): string[][] {
    const dataRows: string[][] = [];
    tableVals.forEach((tableVal) => {
      const dataRow: string[] = [];
      keys
        .filter((key) => key.length)
        .forEach((key) => {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
          dataRow.push(tableVal[key]);
        });
      dataRows.push(dataRow);
    });
    return dataRows;
  }

  private getTableHeadings(
    typeTable: EgfaErfuellungItemNormadressat,
    isShortTable = false,
    hasFinalGesamt = false,
    art?: EgfaErfuellungItemArtType.Informationspflicht,
  ): string[] {
    // Use Bezeichnung label for all tables
    const bezeichnungHeader = i18n.t(
      'egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.bezeichnung',
    );
    return [bezeichnungHeader, ...this.specifiedHeadings(typeTable, isShortTable, hasFinalGesamt, art)];
  }

  private specifiedHeadings(
    typeTable: EgfaErfuellungItemNormadressat,
    isShortTable = false,
    hasFinalGesamt = false,
    art?: EgfaErfuellungItemArtType.Informationspflicht,
  ): string[] {
    const commonTranslationKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.table.th.${typeTable}`;
    const normadressatValue = typeTable === EgfaErfuellungItemNormadressat.Buerger ? 'Buerger' : '';
    const transaltionSuffix = art || normadressatValue;
    const artTranslationsKey = `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.vorhabenList.tableHeading${transaltionSuffix}`;
    const translationKey = art || isShortTable ? artTranslationsKey : commonTranslationKey;
    let headings = [
      i18n.t(`${translationKey}.${art ? 'personalaufwand' : 'jaehrlichenPersonalaufwands'}`),
      i18n.t(`${translationKey}.${art ? 'sachaufwand' : 'jaehrlichenSachaufwands'}`),
      i18n.t(`${translationKey}.einmaligerPersonalaufwand`),
      i18n.t(`${translationKey}.einmaligerSachaufwand`),
    ];
    if (isShortTable) {
      headings = [i18n.t(`${translationKey}.personalaufwand`), i18n.t(`${translationKey}.sachaufwand`)];
    }
    if (hasFinalGesamt) {
      headings.push(
        i18n.t(
          `egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.vorhabenList.tableHeading${normadressatValue}.gesamtaufwand`,
        ),
      );
    }
    return headings;
  }
}
