// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './fertigstellung.less';

import React from 'react';

import { EgfaModuleErfuellungsaufwandEntityDTO, EgfaModuleStatusType } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { SimpleModuleProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import { FertigstellungErfuellungsaufwandContent } from './fertigstellung-content/component.react';

interface SimpleModuleFertigstellungProps extends SimpleModuleProps<EgfaModuleErfuellungsaufwandEntityDTO> {
  additionalActions?: React.ReactElement[];
  isLight?: boolean;
  previousLinkName?: string;
  customEditLink?: string;
}
export function FertigstellungErfuellungsaufwandComponent(props: SimpleModuleFertigstellungProps): React.ReactElement {
  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}` as keyof typeof routes;
  const moduleNameRoute: string = routes[routeKey];

  return (
    <ZusammenfassungWrapper
      egfaId={props.egfaId}
      prevPage={`${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${props.previousLinkName as string}`}
      onSave={(successCallback: () => void) => {
        props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)(successCallback);
      }}
      moduleData={props.formData}
      additionalActions={props.additionalActions}
      moduleNameRoute={moduleNameRoute}
      isLight={props.isLight}
    >
      <FertigstellungErfuellungsaufwandContent
        formData={props.formData}
        moduleName={props.moduleName}
        egfaId={props.egfaId}
        onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
      />
    </ZusammenfassungWrapper>
  );
}
