// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../fertigstellung.less';

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import {
  AktionType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleErfuellungsaufwandEntityDTOAllOf,
} from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { BegruendungsteilSectionComponent } from '../../zusammenfassung/begruendungsteil/begruendungsteil-section/component.react';
import {
  generateSummaryTableVals,
  TablesValsState,
  VorblattWrapper,
} from '../../zusammenfassung/vorblatt-wrapper/component.react';

interface FertigstellungErfuellungsaufwandProps {
  formData?: EgfaModuleErfuellungsaufwandEntityDTOAllOf;
  moduleName: string;
  egfaId: string;
  ergebnisdokumentation?: boolean;
  onSave: (successCallback: () => void) => void;
}
export function FertigstellungErfuellungsaufwandContent(
  props: FertigstellungErfuellungsaufwandProps,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const [summaryTablesVals, setSummaryTablesVals] = useState<TablesValsState>();
  useEffect(() => {
    const clonedFormDataItems = props.formData?.items ? structuredClone(props.formData.items) : [];
    setSummaryTablesVals({
      buergerinnenUndBuergerTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Buerger,
        'Buerger',
        props.formData?.expertenModus,
      ),
      wirtschaftTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Wirtschaft,
        '',
        props.formData?.expertenModus,
      ),
      verwaltungTableVals: generateSummaryTableVals(
        clonedFormDataItems,
        EgfaErfuellungItemNormadressat.Verwaltung,
        '',
        props.formData?.expertenModus,
      ),
    });
  }, [props.formData]);
  const getZusammenfassungLink = (pageName: string) => {
    if (props.ergebnisdokumentation || !props.formData?.aktionen?.includes(AktionType.Schreiben)) {
      return null;
    }
    return (
      <Link
        to={`/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${pageName}`}
        className="blue-text-button"
        onClick={() =>
          props.onSave(() =>
            history.push(
              `/egfa/${props.egfaId}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${pageName}`,
            ),
          )
        }
      >
        {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.zurBearbeitung`)}
      </Link>
    );
  };
  return (
    <div className="zusammenfassung-div erfuellungsaufwand-fertigstellung">
      <Title level={props.ergebnisdokumentation ? 3 : 2}>
        {t(`egfa.modules.${props.moduleName}.fertigstellung.generalInfo.title`)}
      </Title>
      <dl className="general-info">
        <dt>{t(`egfa.modules.${props.moduleName}.fertigstellung.generalInfo.anfertigungTitle`)}</dt>
        <dd>
          {t(
            `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.${
              props.formData?.anfertigendeStelle as string
            }`,
          )}
        </dd>
        <dt>{t(`egfa.modules.${props.moduleName}.fertigstellung.generalInfo.umgesetzterTitle`)}</dt>
        <dd>
          {t(
            `egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.umgesetztenTatbestandOptions.${
              props.formData?.umgesetzterTatbestand as string
            }`,
          )}
        </dd>
      </dl>
      <div className="divider-div" />

      <div className="title-div">
        {getZusammenfassungLink(routes.ALGEMEINEANGABEN)}
        <Title level={props.ergebnisdokumentation ? 3 : 2}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.inhalteVorblatt.title`)}
        </Title>
      </div>
      <Title level={props.ergebnisdokumentation ? 4 : 3}>
        {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.algemeineangaben.eZusammenfassung`)}
      </Title>
      <p className="zusammenfassung-text">{props.formData?.zusammenfassungVorblatt}</p>
      <VorblattWrapper
        ergebnisdokumentation={true}
        egfaId={props.egfaId}
        formData={props.formData as EgfaModuleErfuellungsaufwandEntityDTO}
      />
      <div className="divider-div" />
      <div className="title-div">
        {getZusammenfassungLink(routes.BEGRUENDUNGSTEIL)}
        <Title level={props.ergebnisdokumentation ? 3 : 2}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.inhaltTitle`)}
        </Title>
      </div>
      <Title level={props.ergebnisdokumentation ? 4 : 3}>
        {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.zusammenfassungBegruendungsteil.title`)}
      </Title>
      <p className="zusammenfassung-text">{props.formData?.zusammenfassungBegruendungsteil}</p>
      <div className="divider-div" />
      <div className="title-div">
        <Title level={props.ergebnisdokumentation ? 4 : 3}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.BUERGER.title`)}
        </Title>
      </div>
      <p className="zusammenfassung-text">{props.formData?.zusammenfassungBuerger}</p>
      <BegruendungsteilSectionComponent
        egfaId={props.egfaId}
        normadressat={EgfaErfuellungItemNormadressat.Buerger}
        items={props.formData?.items || []}
        summaryFieldName="zusammenfassungBuerger"
        summaryTableVals={summaryTablesVals?.buergerinnenUndBuergerTableVals || []}
        isFertigstellung={true}
        isExpertMode={props.formData?.expertenModus}
      />
      <div className="title-div">
        <Title level={props.ergebnisdokumentation ? 4 : 3}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.WIRTSCHAFT.title`)}
        </Title>
      </div>
      <p className="zusammenfassung-text">{props.formData?.zusammenfassungWirtschaft}</p>
      <BegruendungsteilSectionComponent
        egfaId={props.egfaId}
        normadressat={EgfaErfuellungItemNormadressat.Wirtschaft}
        items={props.formData?.items || []}
        summaryFieldName="zusammenfassungWirtschaft"
        summaryTableVals={summaryTablesVals?.wirtschaftTableVals || []}
        isFertigstellung={true}
        isExpertMode={props.formData?.expertenModus}
      />
      <div className="title-div">
        <Title level={props.ergebnisdokumentation ? 4 : 3}>
          {t(`egfa.modules.erfuellungsaufwand.zusammenfassung.begruendungsteil.VERWALTUNG.title`)}
        </Title>
      </div>
      <p className="zusammenfassung-text">{props.formData?.zusammenfassungVerwaltung}</p>
      <BegruendungsteilSectionComponent
        egfaId={props.egfaId}
        normadressat={EgfaErfuellungItemNormadressat.Verwaltung}
        items={props.formData?.items || []}
        summaryFieldName="zusammenfassungVerwaltung"
        summaryTableVals={summaryTablesVals?.verwaltungTableVals || []}
        isFertigstellung={true}
        isExpertMode={props.formData?.expertenModus}
      />
    </div>
  );
}
