// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './gesamtsummenNormadressaten.less';

import { Button, Form } from 'antd';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import saveAs from 'file-saver';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
  EgfaModuleStatusType,
  FileControllerApi,
} from '@plateg/rest-api';
import {
  CloseOutlined,
  DropdownMenu,
  ErrorController,
  ExclamationCircleFilled,
  FormWrapper,
  GlobalDI,
  ModalWrapper,
  TableComponentProps,
} from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { VorgabeItems } from '../controller';
import { VorgabeModalComponent } from '../vorgabe-modal/component.react';
import { sumZeilenExpert, sumZeilenFormatter } from '../vorgabe/controller';
import { GesamtsummenNormadressatenItemInterface, getTableVals } from './controller';
import { SectionExpertModeComponent } from './section-expert-mode/component.react';
import { SectionExpertTableComponent } from './section-expert-table/component.react';
import { SectionTableComponent } from './section-table-component/component.react';

export interface GesamtsummenNormadressatenComponentProps<P> extends ContentPageProps<P> {
  moduleName: string;
  sectionName: string;
  isExpertMode: boolean;
  setIsExpertMode: (state: boolean) => void;
  handleFormChange?: () => void;
}
export interface GesamtsummenNormadressatenComponentState {
  buergerinnenUndBuergerTableVals: TableComponentProps<GesamtsummenNormadressatenItemInterface>;
  wirtschaftTableVals: TableComponentProps<GesamtsummenNormadressatenItemInterface>;
  verwaltungTableVals: TableComponentProps<GesamtsummenNormadressatenItemInterface>;
}

export function GesamtsummenNormadressatenComponent(
  props: GesamtsummenNormadressatenComponentProps<EgfaModuleErfuellungsaufwandEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const [isNewVorgabeModalVisisble, setIsNewVorgabeModalVisisble] = useState<boolean>(false);
  const fileControllerApi = GlobalDI.getOrRegister('fileControllerApi', () => new FileControllerApi());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [selectedCategory, setSelectedCategory] = useState<EgfaErfuellungItemNormadressat>(
    EgfaErfuellungItemNormadressat.Buerger,
  );
  const [updatedVorgabe, setUpdatedVorgabe] = useState<EgfaModuleErfuellungsaufwandItemEntityDTO | undefined>(
    undefined,
  );
  const [showUpdatedVorgabeModal, setShowUpdatedVorgabeModal] = useState<boolean>(false);
  const [itemsList, setItemsList] = useState<VorgabeItems[]>();
  const [isFirstPageLoad, setIsFirstPageLoad] = useState<boolean>(true);
  const [updatedVorgabeId, setUpdatedVorgabeId] = useState<string>('');

  useEffect(() => {
    setIsFirstPageLoad(true);
  }, []);

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue({
        ...props.formData,
      });
      goToNewVorgabe(itemsList, isFirstPageLoad, props.formData?.items);
      setIsFirstPageLoad(false);
      setItemsList(props.formData?.items || []);
    }
  }, [props.formData]);

  const goToNewVorgabe = (items: VorgabeItems[] | undefined, firstPageLoad: boolean, newItems: VorgabeItems[] = []) => {
    if (items && !firstPageLoad) {
      const itemsId = items.map((item) => item.id);
      const newItem = newItems.filter((item) => !itemsId.includes(item.id))[0];
      if (newItem?.id) {
        setUpdatedVorgabeId(newItem?.id || '');
      }
    }
  };
  useEffect(() => {
    if (updatedVorgabe) {
      setShowUpdatedVorgabeModal(true);
    }
  }, [updatedVorgabe]);
  const tableValsPattern = {
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
    id: '',
  };
  const [tablesVals, setTablesVals] = useState<GesamtsummenNormadressatenComponentState>({
    buergerinnenUndBuergerTableVals: { ...tableValsPattern },
    wirtschaftTableVals: { ...tableValsPattern },
    verwaltungTableVals: { ...tableValsPattern },
  });
  const [deleteInfo, setDeleteInfo] = useState<{
    id: string | undefined;
    showDeleteModal: boolean;
    vorgabeName: string;
  }>({
    id: '',
    showDeleteModal: false,
    vorgabeName: '',
  });

  const generateTableVals = (
    formDataItems: EgfaModuleErfuellungsaufwandItemEntityDTO[] | undefined,
    normadressat: EgfaErfuellungItemNormadressat,
    typBuerger?: '' | 'Buerger',
  ) => {
    const noAnswer = <i>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.keineAngabe')}</i>;
    const jaehrlichenPersonalaufwandsSum = { sum: 0, label: '' };
    const jaehrlichenSachaufwandsSum = { sum: 0, label: '' };
    const einmaligerPersonalaufwandSum = { sum: 0, label: '' };
    const einmaligerSachaufwandSum = { sum: 0, label: '' };

    const sumMethod = sumZeilenExpert;
    const rowList =
      formDataItems
        ?.filter((item) => item.normadressat === normadressat)
        .map((item, index) => {
          const jaehrlichenPersonalaufwands = sumMethod(
            item.aufwandstabellen,
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
            typBuerger,
          );
          const jaehrlichenSachaufwands = sumMethod(
            item.aufwandstabellen,
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
            typBuerger,
          );
          const einmaligerPersonalaufwand = sumMethod(
            item.aufwandstabellen,
            EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
            typBuerger,
          );

          const einmaligerSachaufwand = sumMethod(
            item.aufwandstabellen,
            EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
            typBuerger,
          );
          jaehrlichenPersonalaufwandsSum.sum += jaehrlichenPersonalaufwands.sum;
          jaehrlichenPersonalaufwandsSum.label = jaehrlichenPersonalaufwands.label;
          jaehrlichenSachaufwandsSum.sum += jaehrlichenSachaufwands.sum;
          jaehrlichenSachaufwandsSum.label = jaehrlichenSachaufwands.label;
          einmaligerPersonalaufwandSum.sum += einmaligerPersonalaufwand.sum;
          einmaligerPersonalaufwandSum.label = einmaligerPersonalaufwand.label;
          einmaligerSachaufwandSum.sum += einmaligerSachaufwand.sum;
          einmaligerSachaufwandSum.label = einmaligerSachaufwand.label;
          const spiegelvorgabeList = props.formData?.items?.filter(
            (spiegelvorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) =>
              item.spiegelvorgabenIds.includes(spiegelvorgabe.id as string),
          );
          return {
            id: item.id,
            key: item.id,
            bezeichnung: (
              <Link
                style={{ fontWeight: 'bolder' }}
                to={`/egfa/${props.egfaId}/${routes.MODULE}/${
                  routes.ERFUELLUNGSAUFWAND
                }/${item.normadressat.toLocaleLowerCase()}/${item.id || ''}`}
              >
                {item.bezeichnung}
              </Link>
            ),
            jaehrlichenPersonalaufwands: sumZeilenFormatter(jaehrlichenPersonalaufwands),
            jaehrlichenSachaufwands: sumZeilenFormatter(jaehrlichenSachaufwands),
            einmaligerPersonalaufwand: sumZeilenFormatter(einmaligerPersonalaufwand),
            einmaligerSachaufwand: sumZeilenFormatter(einmaligerSachaufwand),
            description: (
              <div className="vorgabe-metadata-section">
                <dl>
                  <div className="review-row">
                    <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.paragrafUndRechtsnorm')}</dt>
                    <dd>{item.paragrafUndRechtsnorm || noAnswer}</dd>
                  </div>
                  <div className="review-row">
                    <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabe')}</dt>
                    <dd>
                      {spiegelvorgabeList?.map((spiegelvorgabe) => {
                        return (
                          <div key={spiegelvorgabe.id}>
                            <Link
                              to={`/egfa/${props.egfaId}/${routes.MODULE}/${
                                routes.ERFUELLUNGSAUFWAND
                              }/${spiegelvorgabe.normadressat.toLocaleLowerCase()}/${spiegelvorgabe.id || ''}`}
                            >
                              "{spiegelvorgabe.bezeichnung}" (NA{' '}
                              {t(
                                `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${spiegelvorgabe.normadressat}.textTitle`,
                              )}
                              )
                            </Link>
                          </div>
                        );
                      })}
                      {spiegelvorgabeList?.length === 0 && noAnswer}
                    </dd>
                  </div>
                  <div className="review-row">
                    <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.bemerkungen')}</dt>
                    <dd>{item.bemerkungen || noAnswer}</dd>
                  </div>
                </dl>
              </div>
            ),
            action: (
              <DropdownMenu
                items={[
                  {
                    element: t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.offnen'),
                    onClick: () => {
                      history.push(
                        `/egfa/${props.egfaId}/${routes.MODULE}/${
                          routes.ERFUELLUNGSAUFWAND
                        }/${item.normadressat.toLocaleLowerCase()}/${item.id || ''}`,
                      );
                    },
                  },
                  {
                    element: t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.bearbeiten'),
                    onClick: () => {
                      setUpdatedVorgabe(item);
                    },
                  },
                  {
                    element: t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.loeschen'),
                    onClick: () => {
                      setDeleteInfo({
                        showDeleteModal: true,
                        id: item.id,
                        vorgabeName: item.bezeichnung,
                      });
                    },
                  },
                ]}
                elementId={item.id || `${item.bezeichnung}-${index}`}
              />
            ),
          } as GesamtsummenNormadressatenItemInterface;
        }) || [];
    if (rowList.length > 0) {
      rowList.unshift({
        id: 'tableDetails',
        key: 'tableDetails',
        bezeichnung: 'Details',
        jaehrlichenPersonalaufwands: '',
        jaehrlichenSachaufwands: '',
        einmaligerPersonalaufwand: '',
        einmaligerSachaufwand: '',
        summe: false,
        details: true,
      });
      rowList.unshift({
        id: 'tableSumme',
        key: 'tableSumme',
        bezeichnung: 'Gesamtsumme',
        jaehrlichenPersonalaufwands: sumZeilenFormatter(jaehrlichenPersonalaufwandsSum),
        jaehrlichenSachaufwands: sumZeilenFormatter(jaehrlichenSachaufwandsSum),
        einmaligerPersonalaufwand: sumZeilenFormatter(einmaligerPersonalaufwandSum),
        einmaligerSachaufwand: sumZeilenFormatter(einmaligerSachaufwandSum),
        summe: true,
        details: false,
      });
    }
    return rowList;
  };

  const [expertData, setExpertData] = useState<VorgabeItems[]>([]);
  useEffect(() => {
    setTablesVals({
      buergerinnenUndBuergerTableVals: {
        ...tableValsPattern,
        ...getTableVals(
          generateTableVals(
            form.getFieldValue('items') as VorgabeItems[],
            EgfaErfuellungItemNormadressat.Buerger,
            'Buerger',
          ),
          'Buerger',
          props.isExpertMode,
        ),
      },
      wirtschaftTableVals: {
        ...tableValsPattern,
        ...getTableVals(
          generateTableVals(form.getFieldValue('items') as VorgabeItems[], EgfaErfuellungItemNormadressat.Wirtschaft),
          '',
          props.isExpertMode,
        ),
      },
      verwaltungTableVals: {
        ...tableValsPattern,
        ...getTableVals(
          generateTableVals(form.getFieldValue('items') as VorgabeItems[], EgfaErfuellungItemNormadressat.Verwaltung),
          '',
          props.isExpertMode,
        ),
      },
    });
    setExpertData(form.getFieldValue('items') as VorgabeItems[]);
  }, [form.getFieldValue('items')]);

  const addNewVorgabe = (vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
    const categoryItems = form.getFieldValue('items') as VorgabeItems[];
    form.setFieldsValue({ items: [...categoryItems, vorgabe] });
    props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)();
  };
  const openModal = (sectionKey: EgfaErfuellungItemNormadressat) => {
    setIsNewVorgabeModalVisisble(true);
    setSelectedCategory(sectionKey);
  };
  const saveUpdatedVorgabe = (updateVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
    setUpdatedVorgabe(undefined);
    setShowUpdatedVorgabeModal(false);
    const categoryItems = (form.getFieldValue('items') as VorgabeItems[]).filter(
      (item) => item.id !== updateVorgabe.id,
    );
    form.setFieldsValue({ items: [...categoryItems, updateVorgabe] });
    props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)();
    setUpdatedVorgabeId(updateVorgabe.id || '');
  };
  const exportCsv = (normadressat: EgfaErfuellungItemNormadressat) => {
    fileControllerApi
      .createEgfaErfuellungsaufwandCsvSummary({ egfaId: props.egfaId, normadressat: normadressat })
      .subscribe({
        next: (data) => {
          saveAs(
            data,
            t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${normadressat}.textTitle`) + '.csv',
          );
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
        },
      });
  };

  useEffect(() => {
    setExpandTableHeadLabel();
  }, []);

  const setExpandTableHeadLabel = () => {
    document.querySelectorAll('td.ant-table-row-expand-icon-cell')?.forEach((item) => {
      const invisibleLabel = document.createElement('span');
      invisibleLabel.textContent = t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expandIconCellHeader`);
      invisibleLabel.className = 'sr-only';
      item.appendChild(invisibleLabel);
    });
  };

  return (
    <>
      <div className="gesamtsummenNormadressaten-title-holder">
        <Title level={1}>{t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.title`)}</Title>
        <SectionExpertModeComponent form={form} isExpertMode={props.isExpertMode} onSave={props.onSave} />
      </div>
      <SectionTableComponent
        sectionKey={EgfaErfuellungItemNormadressat.Buerger}
        tableVals={tablesVals.buergerinnenUndBuergerTableVals}
        openModal={openModal}
        isVisible={isNewVorgabeModalVisisble}
        isExpertMode={props.isExpertMode}
        exportCsv={exportCsv}
        elementToShowId={updatedVorgabeId}
      />

      {/* This part with hidden form is used for Zwischenspeichern functionality */}
      <div style={{ display: 'none' }}>
        <FormWrapper
          projectName="eGFA"
          title={<></>}
          previousPage={`/egfa/`}
          nextPage={`/egfa/`}
          saveDraft={props.onSave(form)}
          isDirty={() => false}
          form={form}
          formInitialValue={{}}
          setFormInstance={props.setFormInstance}
        >
          <></>
        </FormWrapper>
      </div>
      {props.isExpertMode && (
        <SectionExpertTableComponent
          sectionKey={EgfaErfuellungItemNormadressat.Buerger}
          expertData={expertData}
          form={form}
          handleFormChange={props.handleFormChange}
          onSave={() => props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)()}
          setExpertData={setExpertData}
        />
      )}

      <SectionTableComponent
        sectionKey={EgfaErfuellungItemNormadressat.Wirtschaft}
        tableVals={tablesVals.wirtschaftTableVals}
        openModal={openModal}
        isVisible={isNewVorgabeModalVisisble}
        isExpertMode={props.isExpertMode}
        exportCsv={exportCsv}
        elementToShowId={updatedVorgabeId}
      />

      {props.isExpertMode && (
        <SectionExpertTableComponent
          sectionKey={EgfaErfuellungItemNormadressat.Wirtschaft}
          expertData={expertData}
          form={form}
          handleFormChange={props.handleFormChange}
          onSave={() => props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)()}
          setExpertData={setExpertData}
        />
      )}
      <SectionTableComponent
        sectionKey={EgfaErfuellungItemNormadressat.Verwaltung}
        tableVals={tablesVals.verwaltungTableVals}
        openModal={openModal}
        isVisible={isNewVorgabeModalVisisble}
        isExpertMode={props.isExpertMode}
        exportCsv={exportCsv}
        elementToShowId={updatedVorgabeId}
      />

      {props.isExpertMode && (
        <SectionExpertTableComponent
          sectionKey={EgfaErfuellungItemNormadressat.Verwaltung}
          expertData={expertData}
          form={form}
          handleFormChange={props.handleFormChange}
          onSave={() => props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)()}
          setExpertData={setExpertData}
        />
      )}

      <VorgabeModalComponent
        isVisible={isNewVorgabeModalVisisble}
        setIsVisible={setIsNewVorgabeModalVisisble}
        normadressat={selectedCategory}
        sectionKey={t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${selectedCategory}.textTitle`)}
        isWirtschaft={selectedCategory === EgfaErfuellungItemNormadressat.Wirtschaft}
        itemsList={itemsList}
        successCallback={(newVorgabe) => {
          addNewVorgabe(newVorgabe);
        }}
      />
      {updatedVorgabe && (
        <VorgabeModalComponent
          isVisible={showUpdatedVorgabeModal}
          setIsVisible={(visible) => {
            if (!visible) {
              setUpdatedVorgabe(undefined);
            }
            setShowUpdatedVorgabeModal(visible);
          }}
          sectionKey={t(
            `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${updatedVorgabe.normadressat}.textTitle`,
          )}
          isWirtschaft={updatedVorgabe.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft}
          itemsList={itemsList}
          vorgabe={updatedVorgabe}
          normadressat={updatedVorgabe.normadressat}
          successCallback={(toUpdateVorgabe) => {
            saveUpdatedVorgabe(toUpdateVorgabe);
          }}
          isEditMode={true}
        />
      )}

      <ModalWrapper
        width={500}
        closeIcon={<CloseOutlined />}
        onCancel={() =>
          setDeleteInfo({
            showDeleteModal: false,
            id: '',
            vorgabeName: '',
          })
        }
        className={` gesamtsummenNormadressaten-modal`}
        open={deleteInfo.showDeleteModal}
        title={
          <>
            <h3>
              <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
                <ExclamationCircleFilled />
              </span>
              <Text className="ant-popover-message-title">
                {t(`egfa.modules.erfuellungsaufwand.vorgabePage.preview.loeschentitle`)}
              </Text>
            </h3>
          </>
        }
        maskClosable={false}
        footer={[
          <div key="footer-left" className="plateg-modal-footer-left">
            <Button
              className="ant-btn ant-btn-primary"
              onClick={() => {
                const newItems = (form.getFieldValue('items') as VorgabeItems[]).filter(
                  (vorgabe) => vorgabe.id !== deleteInfo.id,
                );
                form.setFieldsValue({ items: [...newItems] });
                props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)();
                setDeleteInfo({
                  showDeleteModal: false,
                  id: '',
                  vorgabeName: '',
                });
              }}
            >
              {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.loeschenConfirm')}
            </Button>
          </div>,
          <div key="footer-right" className="plateg-modal-footer-right">
            <Button
              className="ant-btn"
              onClick={() => {
                setDeleteInfo({
                  showDeleteModal: false,
                  id: '',
                  vorgabeName: '',
                });
              }}
            >
              {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.loeschenCancel')}
            </Button>
          </div>,
        ]}
      >
        <p
          style={{ marginTop: '20px' }}
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.erfuellungsaufwand.vorgabePage.preview.loeschenContent`, {
              vorgabeName: `<b>"${deleteInfo.vorgabeName}"</b>`,
            }),
          }}
        />
      </ModalWrapper>
    </>
  );
}
