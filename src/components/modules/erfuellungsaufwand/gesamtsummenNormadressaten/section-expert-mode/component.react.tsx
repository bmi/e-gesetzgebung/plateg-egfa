// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './section-expert-mode.less';

import { Button, FormInstance, Switch } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleStatusType } from '@plateg/rest-api';
import { InfoComponent, ModalWrapper } from '@plateg/theme';

interface SectionExpertModeProps {
  form: FormInstance;
  isExpertMode: boolean;
  onSave: (
    form?: FormInstance,
    status?: EgfaModuleStatusType,
    isDraftSaving?: boolean,
  ) => (successCallback?: () => void) => void;
}
export function SectionExpertModeComponent(props: SectionExpertModeProps): React.ReactElement {
  const { t } = useTranslation();
  const [showConfirmExpertMode, setShowConfirmExpertMode] = useState<boolean>(false);

  const saveExpertMode = (value: boolean) => {
    props.form.setFieldsValue({ expertenModus: value });
    props.onSave(props.form, EgfaModuleStatusType.InBearbeitung, false)();
  };

  return (
    <>
      <div className="expert-mode-switch-holder">
        <Switch
          checked={props.isExpertMode}
          onChange={() => {
            setShowConfirmExpertMode(true);
          }}
          aria-label={t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.switchLabel`)}
        />
        <span className="expert-mode-switch-label">
          {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.switchLabel`)}
          <InfoComponent
            isContactPerson={false}
            title={`Hilfe zu: ${t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.switchLabel`,
            )}`}
            titleWithoutPrefix={true}
          >
            <div
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.drawerSwitchLabel`),
              }}
            />
          </InfoComponent>
        </span>
        <ModalWrapper
          className="expert-mode-confirm"
          open={showConfirmExpertMode}
          onCancel={() => setShowConfirmExpertMode(false)}
          footer={[
            <Button
              id="egfa-expert-mode-apply-btn"
              onClick={() => {
                saveExpertMode(!props.isExpertMode);
                setShowConfirmExpertMode(false);
              }}
              type="primary"
            >
              {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.btnOk`)}
            </Button>,
            <Button id="egfa-expert-mode-cancel-btn" onClick={() => setShowConfirmExpertMode(false)}>
              {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.btnCancel`)}
            </Button>,
          ]}
        >
          <strong>
            {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.confirmModalTitle`)}
          </strong>
          <p>
            {t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.${
                props.isExpertMode ? 'disableText' : 'enableText'
              }`,
            )}
          </p>
        </ModalWrapper>
      </div>
    </>
  );
}
