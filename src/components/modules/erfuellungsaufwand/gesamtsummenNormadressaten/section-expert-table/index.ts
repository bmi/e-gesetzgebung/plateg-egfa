// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BUERGERExpertTableComponent } from './BUERGER/component.react';
import { VERWALTUNGExpertTableComponent } from './VERWALTUNG/component.react';
import { WIRTSCHAFTExpertTableComponent } from './WIRTSCHAFT/component.react';

export const ExpertTableComponent = {
  BUERGER: BUERGERExpertTableComponent,
  WIRTSCHAFT: WIRTSCHAFTExpertTableComponent,
  VERWALTUNG: VERWALTUNGExpertTableComponent,
};
