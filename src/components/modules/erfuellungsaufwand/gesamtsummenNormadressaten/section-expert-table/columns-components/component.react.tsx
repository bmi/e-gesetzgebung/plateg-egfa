// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, Radio } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import i18next from 'i18next';
import React from 'react';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';
import { DeleteOutlined } from '@plateg/theme';

import { vollzugsebeneTypes } from '../../../vorgabe-modal/component.react';
import { AufwandTableDatarow } from '../../../vorgabe/aufwand-tables';
import {
  getCellInputNumber,
  getCellTextarea,
  getTreeSelect,
  prepareFilteredTreeData,
} from '../../../vorgabe/controller';
import { UpdateKeyInterface } from '../BUERGER/component.react';
import { TreeSelectItem } from '../component.react';

interface CommonTableComponentProps {
  translationKey: string;
  treeSelectData: TreeSelectItem[];
  sectionKey: EgfaErfuellungItemNormadressat;
  updateHandler: () => void;
  deleteRow: (index: number) => void;
}

export const getCommonColumns = (props: CommonTableComponentProps) => {
  return (
    <>
      <Column
        title={i18next.t(`${props.translationKey}.bezeichnungSimple`).toString()}
        dataIndex="bezeichnung"
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellTextarea(['items', index, 'bezeichnung'], props.updateHandler, true)}
              <div className="btn-remove-holder">
                <Button
                  id={`button-remove-row-${record.key}`}
                  onClick={() => props.deleteRow?.(index)}
                  type="link"
                  className="blue-text-button"
                >
                  <DeleteOutlined />
                  {i18next.t(`${props.translationKey}.btnRemove`).toString()}
                </Button>
              </div>
            </>
          );
        }}
        key="bezeichnung"
      />
      {props.sectionKey === EgfaErfuellungItemNormadressat.Wirtschaft && (
        <Column
          title={i18next.t(`${props.translationKey}.art`).toString()}
          dataIndex="art"
          shouldCellUpdate={() => false}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return (
              <>
                <div className="art-holder">
                  <Form.Item name={['items', index, 'art']}>
                    <Radio.Group className="horizontal-radios" name="art" onChange={props.updateHandler}>
                      <Radio
                        id={`egfa-Vorgabe-radio-${index}`}
                        className="horizontal-radios"
                        value={EgfaErfuellungItemArtType.Vorgabe}
                      >
                        {'wV'}
                      </Radio>
                      <Radio
                        id={`egfa-Informationspflicht-radio-${index}`}
                        className="horizontal-radios"
                        value={EgfaErfuellungItemArtType.Informationspflicht}
                      >
                        {'IP'}
                      </Radio>
                    </Radio.Group>
                  </Form.Item>
                </div>
              </>
            );
          }}
          key="art"
        />
      )}

      {props.sectionKey === EgfaErfuellungItemNormadressat.Verwaltung && (
        <Column
          title={i18next.t(`${props.translationKey}.ebene`).toString()}
          dataIndex="vollzugsebene"
          shouldCellUpdate={() => false}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return (
              <>
                <div className="art-holder">
                  <Form.Item name={['items', index, 'vollzugsebene']}>
                    <Radio.Group className="horizontal-radios" name="vollzugsebene" onChange={props.updateHandler}>
                      <Radio
                        id={`egfa-Bund-radio-vollzugsebene${index}`}
                        className="horizontal-radios"
                        value={vollzugsebeneTypes.bund}
                      >
                        {'Bund'}
                      </Radio>
                      <Radio
                        id={`egfa-Land-radio-vollzugsebene${index}`}
                        className="horizontal-radios"
                        value={vollzugsebeneTypes.land}
                      >
                        {'Land'}
                      </Radio>
                    </Radio.Group>
                  </Form.Item>
                </div>
              </>
            );
          }}
          key="vollzugsebene"
        />
      )}

      <Column
        title={i18next.t(`${props.translationKey}.paragraf`).toString()}
        dataIndex="paragrafUndRechtsnorm"
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return <>{getCellTextarea(['items', index, 'paragrafUndRechtsnorm'], props.updateHandler)}</>;
        }}
        key="paragrafUndRechtsnorm"
      />
      <Column
        title={i18next.t(`${props.translationKey}.spiegelvorgaben`).toString()}
        dataIndex="spiegelvorgabenIds"
        render={(_: any, record: AufwandTableDatarow, index) => {
          const filteredTreeData = prepareFilteredTreeData(props.treeSelectData, props.sectionKey, record.id || '');
          return getTreeSelect(['items', index, 'spiegelvorgabenIds'], props.updateHandler, filteredTreeData);
        }}
        key="spiegelvorgabenIds"
      />
      <Column
        title={i18next.t(`${props.translationKey}.bemerkungen`).toString()}
        dataIndex="bemerkungen"
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return <>{getCellTextarea(['items', index, 'bemerkungen'], props.updateHandler)}</>;
        }}
        key="bemerkungen"
      />
    </>
  );
};

interface GroupColumnsProps {
  translationKey: string;
  sectionKey: EgfaErfuellungItemNormadressat;
  updateHandler: () => void;
  recalculateDynamicRowDataOnFly: (
    updateKey: string,
    vorgabeInd: number,
    tableInd: number,
    dependentKey?: string,
  ) => void;
  aufwandstableIndex: number;
  aufwandType: 'einmalig' | 'jaehrlich';
}

export const getGroupWithLohn = (props: GroupColumnsProps) => {
  return (
    <ColumnGroup
      key={`personalaufwand_${props.aufwandType}-key`}
      title={i18next
        .t(`${props.translationKey}.${props.sectionKey}.personalaufwand_${props.aufwandType}_title`)
        .toString()}
      className="grouping"
    >
      <Column
        title={i18next.t(`${props.translationKey}.zeitaufwandProFall`).toString()}
        key={`zeitaufwandProFall-personalaufwand_${props.aufwandType}`}
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  UpdateKeyInterface.zeitaufwandDifferenz,
                ],
                'Min.',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.zeitaufwandDifferenz,
                    index,
                    props.aufwandstableIndex,
                  ),
                false,
                true,
              )}
            </>
          );
        }}
        dataIndex="zeitaufwandProFall"
      />
      <Column
        title={i18next.t(`${props.translationKey}.lohnsatz`).toString()}
        key={`lohnkosten-personalaufwand_${props.aufwandType}`}
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  UpdateKeyInterface.lohnkosten,
                ],
                '€/Std.',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.lohnkosten,
                    index,
                    props.aufwandstableIndex,
                    UpdateKeyInterface.zeitaufwandDifferenz,
                  ),
                true,
                false,
              )}
            </>
          );
        }}
        dataIndex="lohnkosten"
      />
      <Column
        title={i18next.t(`${props.translationKey}.personalaufwandProFall`).toString()}
        key={`personalaufwandProFall-personalaufwand_${props.aufwandType}`}
        shouldCellUpdate={(record, prevRecord) => {
          const isZeitaufwandDifferenzChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.zeitaufwandDifferenz,
            props.aufwandstableIndex,
            0,
          );

          const isLohnkostenChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.lohnkosten,
            props.aufwandstableIndex,
            0,
          );

          return isZeitaufwandDifferenzChanged || isLohnkostenChanged;
        }}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  UpdateKeyInterface.personalaufwandProFall,
                ],
                '€',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.personalaufwandProFall,
                    index,
                    props.aufwandstableIndex,
                  ),
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="personalaufwandProFall"
      />
      <Column
        title={i18next.t(`${props.translationKey}.fallzahl`).toString()}
        key={`fallzahl-personalaufwand_${props.aufwandType}`}
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                ['items', index, 'aufwandstabellen', props.aufwandstableIndex, UpdateKeyInterface.veraenderungFallzahl],
                ' ',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.veraenderungFallzahl,
                    index,
                    props.aufwandstableIndex,
                    UpdateKeyInterface.personalaufwandProFall,
                  ),
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="fallzahl"
      />
      <Column
        title={i18next.t(`${props.translationKey}.personalaufwandGesamt`).toString()}
        key={`zeitaufwandGesamt-personalaufwand_${props.aufwandType}`}
        shouldCellUpdate={(record, prevRecord) => {
          const isZeitaufwandDifferenzChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.zeitaufwandDifferenz,
            props.aufwandstableIndex,
            0,
          );

          const isLohnkostenChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.lohnkosten,
            props.aufwandstableIndex,
            0,
          );

          const isPersonalaufwandProFallChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.personalaufwandProFall,
            props.aufwandstableIndex,
            0,
          );

          const isVeraenderungFallzahlChanged = handleTabellenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.veraenderungFallzahl,
            props.aufwandstableIndex,
          );

          return (
            isZeitaufwandDifferenzChanged ||
            isLohnkostenChanged ||
            isPersonalaufwandProFallChanged ||
            isVeraenderungFallzahlChanged
          );
        }}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  'personalaufwandSumme',
                ],
                '€',
                props.updateHandler,
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="personalaufwandSumme"
        className="selected gesamt-euro"
      />
    </ColumnGroup>
  );
};

export const getGroupWithoutLohn = (props: GroupColumnsProps) => {
  return (
    <ColumnGroup
      title={i18next.t(`${props.translationKey}.${props.sectionKey}.sachaufwand_${props.aufwandType}_title`).toString()}
      key={`sachaufwand_${props.aufwandType}-key`}
      className="grouping"
    >
      <Column
        title={i18next.t(`${props.translationKey}.sachaufwandProFall`).toString()}
        key={`sachaufwandProFall-sachaufwand_${props.aufwandType}`}
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  UpdateKeyInterface.sachaufwandDifferenz,
                ],
                '€',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.sachaufwandDifferenz,
                    index,
                    props.aufwandstableIndex,
                  ),
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="sachaufwandProFall"
      />
      <Column
        title={i18next.t(`${props.translationKey}.fallzahl`).toString()}
        key={`fallzahl-sachaufwand_${props.aufwandType}`}
        shouldCellUpdate={() => false}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                ['items', index, 'aufwandstabellen', props.aufwandstableIndex, UpdateKeyInterface.veraenderungFallzahl],
                ' ',
                () =>
                  props.recalculateDynamicRowDataOnFly(
                    UpdateKeyInterface.veraenderungFallzahl,
                    index,
                    props.aufwandstableIndex,
                    UpdateKeyInterface.sachaufwandDifferenz,
                  ),
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="fallzahl"
      />
      <Column
        title={i18next.t(`${props.translationKey}.sachaufwandGesamt`).toString()}
        key={`sachaufwandGesamt-sachaufwand_${props.aufwandType}`}
        shouldCellUpdate={(record, prevRecord) => {
          const isSachaufwandDifferenzChanged = handleZeilenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.sachaufwandDifferenz,
            props.aufwandstableIndex,
            0,
          );

          const isVeraenderungFallzahlChanged = handleTabellenCondition(
            record as EgfaModuleErfuellungsaufwandItemEntityDTO,
            prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
            UpdateKeyInterface.veraenderungFallzahl,
            props.aufwandstableIndex,
          );

          return isSachaufwandDifferenzChanged || isVeraenderungFallzahlChanged;
        }}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <>
              {getCellInputNumber(
                [
                  'items',
                  index,
                  'aufwandstabellen',
                  props.aufwandstableIndex,
                  'aufwandstabellenZeilen',
                  0,
                  'personalaufwandSumme',
                ],
                '€',
                props.updateHandler,
                true,
                true,
              )}
            </>
          );
        }}
        dataIndex="sachaufwandGesamt"
        className="selected gesamt-euro"
      />
    </ColumnGroup>
  );
};

export const handleZeilenCondition = (
  record: EgfaModuleErfuellungsaufwandItemEntityDTO,
  prevRecord: EgfaModuleErfuellungsaufwandItemEntityDTO,
  updateKey: keyof EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
  index1: number,
  index2: number,
) => {
  const getValue = (record: EgfaModuleErfuellungsaufwandItemEntityDTO) =>
    record.aufwandstabellen[index1]?.aufwandstabellenZeilen[index2]?.[updateKey];

  return getValue(record) !== getValue(prevRecord);
};
export const handleTabellenCondition = (
  record: EgfaModuleErfuellungsaufwandItemEntityDTO,
  prevRecord: EgfaModuleErfuellungsaufwandItemEntityDTO,
  updateKey: keyof EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  index1: number,
) => {
  const getValue = (record: EgfaModuleErfuellungsaufwandItemEntityDTO) => record.aufwandstabellen[index1]?.[updateKey];

  return getValue(record) !== getValue(prevRecord);
};
