// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './section-expert-table.less';

import { Button, Form, FormInstance } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';
import { GeneralFormWrapper, GlobalDI, PlusOutlined } from '@plateg/theme';

import { ErfuellungsaufwandController, VorgabeItems } from '../../controller';
import { vollzugsebeneTypes } from '../../vorgabe-modal/component.react';
import { AufwandTableDatarow } from '../../vorgabe/aufwand-tables';
import { ExpertTableComponent } from './';
import { createPatternAufwandstabelle, patternExpertVorgabe, patternExpertZeil } from './controller';

export interface TreeSelectItem {
  title: string;
  value: string;
  selectable: boolean;
  children: TreeSelectChild[];
}
interface TreeSelectChild {
  title: string;
  value?: string;
}
interface SectionTableComponentProps {
  sectionKey: EgfaErfuellungItemNormadressat;
  expertData: EgfaModuleErfuellungsaufwandItemEntityDTO[];
  form: FormInstance;
  handleFormChange?: () => void;
  onSave: () => void;
  setExpertData: (items: VorgabeItems[]) => void;
}
export function SectionExpertTableComponent(props: SectionTableComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [localForm] = Form.useForm();
  const translationKey = `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.expertMode.table`;
  const ctrl = GlobalDI.getOrRegister('erfuellungsaufwandController', () => new ErfuellungsaufwandController());

  const [tableContent, setTableContent] = useState<AufwandTableDatarow[]>([]);
  const [treeSelectData, setTreeSelectData] = useState<TreeSelectItem[]>([]);
  const ExpertTableComponentTag = ExpertTableComponent[props.sectionKey];

  const orderGesamtsummeType = {
    [EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich]: 0,
    [EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich]: 1,
    [EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig]: 2,
    [EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig]: 3,
  };

  // Prepare Aufwandstabellen for expert mode for Vorgaben what were created from basic mode
  // and don't have all types of Aufwandstabellen
  const prepareAufwandstabellen = (tables: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO[]) => {
    // Collect types of existing Aufwandstabellen
    const existingTableTypes = tables.map((table) => table.gesamtsummeType);
    // Get types of missed Aufwandstabellen
    // Take all possible types from EgfaErfuellungItemAufwandGesamtsummeType
    // and reduce from this list types of already existed tables
    const missiedTableTypes = Object.values(EgfaErfuellungItemAufwandGesamtsummeType).filter(
      (type) => !existingTableTypes.includes(type),
    );
    const preparedTables = [...tables];
    // Add missed Aufwandstabellen
    missiedTableTypes.forEach((type) => {
      preparedTables.push(createPatternAufwandstabelle(type));
    });
    // Sort tables in right order
    preparedTables.sort(
      (firstItem, secondItem) =>
        orderGesamtsummeType[firstItem.gesamtsummeType] - orderGesamtsummeType[secondItem.gesamtsummeType],
    );
    return preparedTables.map((table) => {
      return {
        ...table,
        aufwandstabellenZeilen: prepareExpertZeil(table.aufwandstabellenZeilen),
      };
    });
  };

  // Prepare expert zeil
  // it it is firts time for vorgabe in Expert mode - add expert zeil and
  // put this teil to first position for easier manipulation with zeilen list
  const prepareExpertZeil = (zeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO[]) => {
    const existedZeilen = [...zeilen];
    const hasExpertZeil = existedZeilen.some((zeil) => zeil.summe);
    if (!hasExpertZeil) {
      existedZeilen.push(patternExpertZeil);
    }
    // Move Expert Zeil to first position
    // to make easy to manipulate with form data in expert table
    // so we always working only with first zeil which is created special for expert mode
    const expertZeil = existedZeilen.find((zeil) => zeil.summe);

    // Put Expert zeil to first position
    return [expertZeil, ...existedZeilen.filter((zeil) => !zeil.summe)];
  };

  // Keep always updated data in table
  const updateFormData = (itemsList: EgfaModuleErfuellungsaufwandItemEntityDTO[]) => {
    const preparedData = itemsList
      // Get only relevant vorgabe
      .filter((vorgabe) => vorgabe.normadressat === props.sectionKey)
      .map((vorgabe, ind) => {
        return {
          ...vorgabe,
          aufwandstabellen: prepareAufwandstabellen(vorgabe.aufwandstabellen),
          // Reduce deleted vorgabe from selected spiegelvorgaben
          spiegelvorgabenIds: vorgabe.spiegelvorgabenIds.filter((id) =>
            treeSelectData
              .map((item) => item.children)
              .flat()
              .some((child) => child.value === id),
          ),
          key: `expert-row-${vorgabe.normadressat}-${ind}`,
        };
      });
    setTableContent(preparedData);
    localForm.setFieldValue('items', preparedData);
  };

  useEffect(() => {
    // Prefill Spiegelvorgabe tree select
    setTreeSelectData(getTreeSelectData(props.expertData || []));
  }, [props.expertData]);

  useEffect(() => {
    updateFormData(props.expertData || []);
  }, [treeSelectData]);

  // Method to keep data always relevand in parent component
  const updateHandler = () => {
    props.handleFormChange?.();
    // Merge vorgaben from current table with anoter in module

    const currentItems = localForm.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[];

    const anoterItems = (props.form.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[]).filter(
      (vorgabe) => vorgabe.normadressat !== props.sectionKey,
    );

    let combinedItems = [...anoterItems, ...currentItems];
    combinedItems = [...anoterItems, ...currentItems].map((vorgabe) => {
      return {
        ...vorgabe,
        // Reduce deleted vorgabe from selected spiegelvorgaben
        spiegelvorgabenIds: vorgabe.spiegelvorgabenIds.filter((id) => combinedItems.some((child) => child.id === id)),
      };
    });
    props.form.setFieldsValue({ items: [...combinedItems] });
    props.setExpertData([...anoterItems, ...currentItems]);
  };

  // Remove vorgabe from table
  const deleteRow = (index: number) => {
    const formValues = localForm.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[];
    formValues.splice(index, 1);
    updateFormData(formValues);
    updateHandler();
  };

  // Add new vorgabe to table
  const addNewRow = () => {
    const formValues = localForm.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[];
    const patternItem = {
      ...patternExpertVorgabe,
      normadressat: props.sectionKey,
      vollzugsebene: vollzugsebeneTypes.bund,
    };
    formValues.push(patternItem);
    updateFormData(formValues);
    props.handleFormChange?.();
  };

  // Prepare data for tree select of Spiegelvorgaben
  const getTreeSelectData = (itemsList: EgfaModuleErfuellungsaufwandItemEntityDTO[]): TreeSelectItem[] => {
    const dataList: TreeSelectItem[] = [];
    const preparedItems = ctrl.prepareItems(itemsList);

    Object.keys(preparedItems).forEach((key) => {
      if (preparedItems[key].length) {
        dataList.push({
          title: t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${key}.textTitle`),
          value: `${key}Value`,
          selectable: false,
          children: getCategoryChildren(preparedItems[key]),
        });
      }
    });

    return dataList;
  };
  // Prepare data for tree select of Spiegelvorgaben
  const getCategoryChildren = (list: EgfaModuleErfuellungsaufwandItemEntityDTO[]): TreeSelectChild[] => {
    return list
      .filter((item) => item.id)
      .map((item) => {
        const paragrafText = item.paragrafUndRechtsnorm ? `(${item.paragrafUndRechtsnorm})` : '';
        const artType = t(`egfa.modules.erfuellungsaufwand.vorgabePage.artType.${item.art}`);
        return {
          title: `${artType}: "${item.bezeichnung}" ${paragrafText}`,
          value: item.id,
        };
      });
  };

  return (
    <>
      <GeneralFormWrapper
        form={localForm}
        layout="vertical"
        scrollToFirstError={true}
        noValidate={false}
        className="expert-mode-form"
      >
        <div className="aufwand-table-wrapper expert-table">
          {/* Name of this component based on seceleted type Normadressat and prepared on line 40 */}
          <ExpertTableComponentTag
            treeSelectData={treeSelectData}
            tableContent={tableContent}
            translationKey={translationKey}
            sectionKey={props.sectionKey}
            updateHandler={updateHandler}
            deleteRow={deleteRow}
            localForm={localForm}
          />
          <div className="buttons-holder">
            <Button id={`button-add-new-expert-row`} onClick={addNewRow} type="link" className="blue-text-button">
              <PlusOutlined />
              {t(`${translationKey}.btnAddNewItem`)}
            </Button>
            <br />
            <Button
              id={`button-save-expert-mode`}
              onClick={() => {
                void localForm
                  .validateFields()
                  .then(() => {
                    props.onSave();
                  })
                  .catch((err) => {
                    console.error(err);
                  });
              }}
              type="default"
            >
              {t(`${translationKey}.btnSave`)}
            </Button>
          </div>
        </div>
      </GeneralFormWrapper>
    </>
  );
}
