// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';
import { DeleteOutlined } from '@plateg/theme';

import { AufwandTableDatarow } from '../../../vorgabe/aufwand-tables';
import { ExpertTableComponentProps, UpdateKeyInterface } from '../BUERGER/component.react';
import { getCommonColumns, getGroupWithLohn, getGroupWithoutLohn } from '../columns-components/component.react';

export function WIRTSCHAFTExpertTableComponent(props: ExpertTableComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const recalculateDynamicRowDataOnFly = (
    updateKey: string,
    vorgabeInd: number,
    tableInd: number,
    dependentKey?: string,
  ) => {
    const currentItems = props.localForm.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[];
    const currentZeile = currentItems[vorgabeInd].aufwandstabellen[tableInd].aufwandstabellenZeilen;
    const expertZeil = { ...currentZeile[0] }; // Take first one because in Expert mode it is always on top
    const personalaufwandSummeProFall =
      (expertZeil.personalaufwandProFall || 0) *
      (currentItems[vorgabeInd].aufwandstabellen[tableInd].veraenderungFallzahl || 0);
    const personalaufwandSummeDifferenz =
      (expertZeil.sachaufwandDifferenz || 0) *
      (currentItems[vorgabeInd].aufwandstabellen[tableInd].veraenderungFallzahl || 0);
    // Calculation for 5 cells
    switch (updateKey as UpdateKeyInterface) {
      case UpdateKeyInterface.zeitaufwandDifferenz:
      case UpdateKeyInterface.lohnkosten: {
        expertZeil.personalaufwandProFall =
          Math.round(
            (((expertZeil.zeitaufwandDifferenz || 0) / 60) * (expertZeil.lohnkosten || 0) + Number.EPSILON) * 100,
          ) / 100;
        expertZeil.personalaufwandSumme = personalaufwandSummeProFall > 0 ? personalaufwandSummeProFall : undefined;
        break;
      }
      case UpdateKeyInterface.veraenderungFallzahl:
        if (dependentKey === UpdateKeyInterface.sachaufwandDifferenz) {
          expertZeil.personalaufwandSumme = personalaufwandSummeDifferenz;
        } else {
          expertZeil.personalaufwandSumme = personalaufwandSummeProFall;
        }
        break;
      case UpdateKeyInterface.personalaufwandProFall:
        expertZeil.personalaufwandSumme = personalaufwandSummeProFall;
        break;
      case UpdateKeyInterface.sachaufwandDifferenz:
        expertZeil.personalaufwandSumme = personalaufwandSummeDifferenz;
        break;

      default:
        break;
    }
    currentZeile[0] = expertZeil;
    props.localForm.setFieldsValue({ items: currentItems });
    props.updateHandler();
  };
  const groupProps = {
    sectionKey: props.sectionKey,
    translationKey: props.translationKey,
    updateHandler: props.updateHandler,
    recalculateDynamicRowDataOnFly: recalculateDynamicRowDataOnFly,
  };

  useEffect(() => {
    setDeleteTableHeadLabel();
  }, []);

  const setDeleteTableHeadLabel = () => {
    const item = document.querySelector(`thead td.deleteIconCell-${props.sectionKey}`);
    if (item) {
      const invisibleLabel = document.createElement('span');
      invisibleLabel.textContent = t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.deleteIconCellHeader`);
      invisibleLabel.className = 'sr-only';
      item.appendChild(invisibleLabel);
    }
  };

  return (
    <>
      <Table dataSource={props.tableContent} pagination={false} className="aufwand-table expert-mode-table">
        {getCommonColumns({
          treeSelectData: props.treeSelectData,
          sectionKey: props.sectionKey,
          translationKey: props.translationKey,
          updateHandler: props.updateHandler,
          deleteRow: props.deleteRow,
        })}
        {getGroupWithLohn({
          ...groupProps,
          aufwandstableIndex: 0,
          aufwandType: 'jaehrlich',
        })}
        {getGroupWithoutLohn({
          ...groupProps,
          aufwandstableIndex: 1,
          aufwandType: 'jaehrlich',
        })}

        {getGroupWithLohn({
          ...groupProps,
          aufwandstableIndex: 2,
          aufwandType: 'einmalig',
        })}
        {getGroupWithoutLohn({
          ...groupProps,
          aufwandstableIndex: 3,
          aufwandType: 'einmalig',
        })}

        <Column
          key="deleteRow"
          className={'deleteIconCell-' + props.sectionKey}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return (
              <div className="button-delete-cell">
                <Button
                  id={`button-remove-row-${record.key}`}
                  onClick={() => props.deleteRow?.(index)}
                  aria-label="Löschen"
                >
                  <DeleteOutlined />
                </Button>
              </div>
            );
          }}
        />
      </Table>
    </>
  );
}
