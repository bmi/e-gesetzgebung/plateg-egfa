// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';

export const patternExpertZeil = {
  summe: true,
  bezeichnung: 'Expert Mode',
  orderIndex: -1,
  berechnungsart: EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall,
};
export const createPatternAufwandstabelle = (type: EgfaErfuellungItemAufwandGesamtsummeType) => {
  return {
    berechnungsart: EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall,
    gesamtsummeType: type,
    aufwandstabellenZeilen: [patternExpertZeil],
  };
};
export const patternExpertVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO = {
  spiegelvorgabenIds: [],
  bezeichnung: '',
  art: EgfaErfuellungItemArtType.Vorgabe,
  normadressat: EgfaErfuellungItemNormadressat.Buerger, // Will be changed in add new vorgabe method
  berechnungsart: EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten,
  fallgruppen: [],
  aufwandstabellen: [
    {
      ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich),
    },
    {
      ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich),
    },
    {
      ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig),
    },
    {
      ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig),
    },
  ],
};
