// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../section-expert-table.less';

import { Button, FormInstance, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemNormadressat, EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';
import { DeleteOutlined } from '@plateg/theme';

import { AufwandTableDatarow } from '../../../vorgabe/aufwand-tables';
import {
  getCellInputNumber,
  getCellTextarea,
  getTreeSelect,
  prepareFilteredTreeData,
} from '../../../vorgabe/controller';
import { handleTabellenCondition, handleZeilenCondition } from '../columns-components/component.react';
import { TreeSelectItem } from '../component.react';

export interface ExpertTableComponentProps {
  treeSelectData: TreeSelectItem[];
  tableContent: AufwandTableDatarow[];
  translationKey: string;
  sectionKey: EgfaErfuellungItemNormadressat;
  updateHandler: () => void;
  deleteRow: (index: number) => void;
  localForm: FormInstance;
}
export enum UpdateKeyInterface {
  zeitaufwandDifferenz = 'zeitaufwandDifferenz',
  veraenderungFallzahl = 'veraenderungFallzahl',
  sachaufwandDifferenz = 'sachaufwandDifferenz',
  lohnkosten = 'lohnkosten',
  personalaufwandProFall = 'personalaufwandProFall',
}
export function BUERGERExpertTableComponent(props: ExpertTableComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const recalculateDynamicRowDataOnFly = (
    updateKey: UpdateKeyInterface,
    vorgabeInd: number,
    tableInd: number,
    dependentKey?: string,
  ) => {
    const currentItems = props.localForm.getFieldValue('items') as EgfaModuleErfuellungsaufwandItemEntityDTO[];
    const currentZeile = currentItems[vorgabeInd].aufwandstabellen[tableInd].aufwandstabellenZeilen;
    const expertZeil = { ...currentZeile[0] }; // Take first one because in Expert mode it is always on top
    if (
      updateKey === UpdateKeyInterface.zeitaufwandDifferenz ||
      (updateKey === UpdateKeyInterface.veraenderungFallzahl &&
        dependentKey === UpdateKeyInterface.zeitaufwandDifferenz)
    ) {
      expertZeil.personalaufwandSumme = Math.ceil(
        ((expertZeil.zeitaufwandDifferenz || 0) *
          (currentItems[vorgabeInd].aufwandstabellen[tableInd].veraenderungFallzahl || 0)) /
          60,
      );
    }

    if (
      updateKey === UpdateKeyInterface.sachaufwandDifferenz ||
      (updateKey === UpdateKeyInterface.veraenderungFallzahl &&
        dependentKey === UpdateKeyInterface.sachaufwandDifferenz)
    ) {
      expertZeil.personalaufwandSumme =
        (expertZeil.sachaufwandDifferenz || 0) *
        (currentItems[vorgabeInd].aufwandstabellen[tableInd].veraenderungFallzahl || 0);
    }
    currentZeile[0] = expertZeil;
    props.localForm.setFieldsValue({ items: currentItems });
    props.updateHandler();
  };

  useEffect(() => {
    setDeleteTableHeadLabel();
  }, []);

  const setDeleteTableHeadLabel = () => {
    const item = document.querySelector(`thead td.deleteIconCell-${props.sectionKey}`);
    if (item) {
      const invisibleLabel = document.createElement('span');
      invisibleLabel.textContent = t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.deleteIconCellHeader`);
      invisibleLabel.className = 'sr-only';
      item.appendChild(invisibleLabel);
    }
  };

  return (
    <>
      <Table dataSource={props.tableContent} pagination={false} className="aufwand-table expert-mode-table">
        <Column
          title={t(`${props.translationKey}.bezeichnungSimple`).toString()}
          dataIndex="bezeichnung"
          shouldCellUpdate={() => false}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return (
              <>
                {getCellTextarea(['items', index, 'bezeichnung'], props.updateHandler, true)}
                <div className="btn-remove-holder">
                  <Button
                    id={`button-remove-row-${record.key}`}
                    onClick={() => props.deleteRow?.(index)}
                    type="link"
                    className="blue-text-button"
                  >
                    <DeleteOutlined />
                    {t(`${props.translationKey}.btnRemove`)}
                  </Button>
                </div>
              </>
            );
          }}
          key="bezeichnung"
        />
        <Column
          title={t(`${props.translationKey}.paragraf`).toString()}
          dataIndex="paragrafUndRechtsnorm"
          render={(_: any, record: AufwandTableDatarow, index) => {
            return <>{getCellTextarea(['items', index, 'paragrafUndRechtsnorm'], props.updateHandler)}</>;
          }}
          key="paragrafUndRechtsnorm"
        />
        <Column
          title={t(`${props.translationKey}.spiegelvorgaben`).toString()}
          dataIndex="spiegelvorgabenIds"
          render={(_: any, record: AufwandTableDatarow, index) => {
            const filteredTreeData = prepareFilteredTreeData(props.treeSelectData, props.sectionKey, record.id || '');
            return getTreeSelect(['items', index, 'spiegelvorgabenIds'], props.updateHandler, filteredTreeData);
          }}
          key="spiegelvorgabenIds"
        />
        <Column
          title={t(`${props.translationKey}.bemerkungen`).toString()}
          dataIndex="bemerkungen"
          shouldCellUpdate={() => false}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return <>{getCellTextarea(['items', index, 'bemerkungen'], props.updateHandler)}</>;
          }}
          key="bemerkungen"
        />

        <ColumnGroup
          key="personalaufwand_jaehrlich-key"
          title={t(`${props.translationKey}.${props.sectionKey}.personalaufwand_jaehrlich_title`).toString()}
          className="grouping"
        >
          <Column
            title={t(`${props.translationKey}.zeitaufwandProFall`).toString()}
            key="zeitaufwandProFall-personalaufwand_jaehrlich"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    [
                      'items',
                      index,
                      'aufwandstabellen',
                      0,
                      'aufwandstabellenZeilen',
                      0,
                      UpdateKeyInterface.zeitaufwandDifferenz,
                    ],
                    'Min.',
                    () => recalculateDynamicRowDataOnFly(UpdateKeyInterface.zeitaufwandDifferenz, index, 0),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="zeitaufwandProFall"
          />
          <Column
            title={t(`${props.translationKey}.fallzahl`).toString()}
            key="fallzahl-personalaufwand_jaehrlich"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 0, UpdateKeyInterface.veraenderungFallzahl],
                    ' ',
                    () =>
                      recalculateDynamicRowDataOnFly(
                        UpdateKeyInterface.veraenderungFallzahl,
                        index,
                        0,
                        UpdateKeyInterface.zeitaufwandDifferenz,
                      ),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="fallzahl"
          />
          <Column
            title={t(`${props.translationKey}.zeitaufwandGesamt`).toString()}
            key="zeitaufwandGesamt-personalaufwand_jaehrlich"
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 0, 'aufwandstabellenZeilen', 0, 'personalaufwandSumme'],
                    'Std.',
                    props.updateHandler,
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="zeitaufwandGesamt"
            className="selected"
          />
        </ColumnGroup>

        <ColumnGroup
          title={t(`${props.translationKey}.${props.sectionKey}.sachaufwand_jaehrlich_title`).toString()}
          key="sachaufwand_jaehrlich-key"
          className="grouping"
        >
          <Column
            title={t(`${props.translationKey}.sachaufwandProFall`).toString()}
            key="sachaufwandProFall-sachaufwand_jaehrlich"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    [
                      'items',
                      index,
                      'aufwandstabellen',
                      1,
                      'aufwandstabellenZeilen',
                      0,
                      UpdateKeyInterface.sachaufwandDifferenz,
                    ],
                    '€',
                    () => recalculateDynamicRowDataOnFly(UpdateKeyInterface.sachaufwandDifferenz, index, 1),
                    true,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="sachaufwandProFall"
          />
          <Column
            title={t(`${props.translationKey}.fallzahl`).toString()}
            key="fallzahl-sachaufwand_jaehrlich"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 1, UpdateKeyInterface.veraenderungFallzahl],
                    ' ',
                    () =>
                      recalculateDynamicRowDataOnFly(
                        UpdateKeyInterface.veraenderungFallzahl,
                        index,
                        1,
                        UpdateKeyInterface.sachaufwandDifferenz,
                      ),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="fallzahl"
          />
          <Column
            title={t(`${props.translationKey}.sachaufwandGesamt`).toString()}
            key="sachaufwandGesamt-sachaufwand_jaehrlich"
            shouldCellUpdate={(record, prevRecord) => {
              const isVeraenderungFallzahlChanged = handleTabellenCondition(
                record as EgfaModuleErfuellungsaufwandItemEntityDTO,
                prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
                UpdateKeyInterface.veraenderungFallzahl,
                1,
              );

              const isSachaufwandDifferenzChanged = handleZeilenCondition(
                record as EgfaModuleErfuellungsaufwandItemEntityDTO,
                prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
                UpdateKeyInterface.sachaufwandDifferenz,
                1,
                0,
              );

              return isVeraenderungFallzahlChanged || isSachaufwandDifferenzChanged;
            }}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 1, 'aufwandstabellenZeilen', 0, 'personalaufwandSumme'],
                    '€',
                    props.updateHandler,
                    true,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="sachaufwandGesamt"
            className="selected gesamt-euro"
          />
        </ColumnGroup>
        <ColumnGroup
          key="personalaufwand_einmalig-key"
          title={t(`${props.translationKey}.${props.sectionKey}.personalaufwand_einmalig_title`).toString()}
          className="grouping"
        >
          <Column
            title={t(`${props.translationKey}.zeitaufwandProFall`).toString()}
            key="zeitaufwandProFall-personalaufwand_einmalig"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    [
                      'items',
                      index,
                      'aufwandstabellen',
                      2,
                      'aufwandstabellenZeilen',
                      0,
                      UpdateKeyInterface.zeitaufwandDifferenz,
                    ],
                    'Min.',
                    () => recalculateDynamicRowDataOnFly(UpdateKeyInterface.zeitaufwandDifferenz, index, 2),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="zeitaufwandProFall"
          />
          <Column
            key="fallzahl-personalaufwand_einmalig"
            title={t(`${props.translationKey}.fallzahl`).toString()}
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 2, UpdateKeyInterface.veraenderungFallzahl],
                    ' ',
                    () =>
                      recalculateDynamicRowDataOnFly(
                        UpdateKeyInterface.veraenderungFallzahl,
                        index,
                        2,
                        UpdateKeyInterface.zeitaufwandDifferenz,
                      ),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="fallzahl"
          />
          <Column
            title={t(`${props.translationKey}.zeitaufwandGesamt`).toString()}
            key="zeitaufwandGesamt-personalaufwand_einmalig"
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 2, 'aufwandstabellenZeilen', 0, 'personalaufwandSumme'],
                    'Std.',
                    props.updateHandler,
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="zeitaufwandGesamt"
            className="selected"
          />
        </ColumnGroup>
        <ColumnGroup
          key="sachaufwand_einmalig-key"
          title={t(`${props.translationKey}.${props.sectionKey}.sachaufwand_einmalig_title`).toString()}
          className="grouping"
        >
          <Column
            title={t(`${props.translationKey}.sachaufwandProFall`).toString()}
            key="sachaufwandProFall-sachaufwand_einmalig"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    [
                      'items',
                      index,
                      'aufwandstabellen',
                      3,
                      'aufwandstabellenZeilen',
                      0,
                      UpdateKeyInterface.sachaufwandDifferenz,
                    ],
                    '€',
                    () => recalculateDynamicRowDataOnFly(UpdateKeyInterface.sachaufwandDifferenz, index, 3),
                    true,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="sachaufwandProFall"
          />
          <Column
            title={t(`${props.translationKey}.fallzahl`).toString()}
            key="fallzahl-sachaufwand_einmalig"
            shouldCellUpdate={() => false}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 3, UpdateKeyInterface.veraenderungFallzahl],
                    ' ',
                    () =>
                      recalculateDynamicRowDataOnFly(
                        UpdateKeyInterface.veraenderungFallzahl,
                        index,
                        3,
                        UpdateKeyInterface.sachaufwandDifferenz,
                      ),
                    false,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="fallzahl"
          />
          <Column
            title={t(`${props.translationKey}.sachaufwandGesamt`).toString()}
            key="sachaufwandGesamt-sachaufwand_einmalig"
            shouldCellUpdate={(record, prevRecord) => {
              const isVeraenderungFallzahlChanged = handleTabellenCondition(
                record as EgfaModuleErfuellungsaufwandItemEntityDTO,
                prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
                UpdateKeyInterface.veraenderungFallzahl,
                3,
              );
              const isSachaufwandDifferenzChanged = handleZeilenCondition(
                record as EgfaModuleErfuellungsaufwandItemEntityDTO,
                prevRecord as EgfaModuleErfuellungsaufwandItemEntityDTO,
                UpdateKeyInterface.sachaufwandDifferenz,
                3,
                0,
              );
              return isVeraenderungFallzahlChanged || isSachaufwandDifferenzChanged;
            }}
            render={(_: any, record: AufwandTableDatarow, index) => {
              return (
                <>
                  {getCellInputNumber(
                    ['items', index, 'aufwandstabellen', 3, 'aufwandstabellenZeilen', 0, 'personalaufwandSumme'],
                    '€',
                    props.updateHandler,
                    true,
                    true,
                  )}
                </>
              );
            }}
            dataIndex="sachaufwandGesamt"
            className="selected gesamt-euro"
          />
        </ColumnGroup>
        <Column
          key="deleteRow"
          className={'deleteIconCell-' + props.sectionKey}
          render={(_: any, record: AufwandTableDatarow, index) => {
            return (
              <div className="button-delete-cell">
                <Button
                  id={`button-remove-row-${record.key}`}
                  onClick={() => props.deleteRow?.(index)}
                  aria-label="Löschen"
                >
                  <DeleteOutlined />
                </Button>
              </div>
            );
          }}
        />
      </Table>
    </>
  );
}
