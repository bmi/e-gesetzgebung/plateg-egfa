// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { ExpertTableComponentProps } from '../BUERGER/component.react';
import { WIRTSCHAFTExpertTableComponent } from '../WIRTSCHAFT/component.react';

export function VERWALTUNGExpertTableComponent(props: ExpertTableComponentProps): React.ReactElement {
  return <WIRTSCHAFTExpertTableComponent {...props} />;
}
