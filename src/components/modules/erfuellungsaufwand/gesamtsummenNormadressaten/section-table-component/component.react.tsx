// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import { ExpandableConfig } from 'antd/lib/table/interface';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemNormadressat } from '@plateg/rest-api';
import { TableComponentProps } from '@plateg/theme';
import { DirectionDownOutlined } from '@plateg/theme/src/components/icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '@plateg/theme/src/components/icons/DirectionRightOutlined';

import { GesamtsummenNormadressatenItemInterface } from '../controller';

interface SectionTableComponentProps {
  sectionKey: EgfaErfuellungItemNormadressat;
  tableVals: TableComponentProps<GesamtsummenNormadressatenItemInterface>;
  openModal: (sectionKey: EgfaErfuellungItemNormadressat) => void;
  isVisible: boolean;
  isExpertMode: boolean;
  exportCsv: (normadressat: EgfaErfuellungItemNormadressat) => void;
  elementToShowId: string;
}
export function SectionTableComponent(props: SectionTableComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const btnNewVorgabe = useRef<HTMLButtonElement>(null);
  const [isFocus, setFocus] = useState(false);
  const [hasItemToScroll, setHasItemToScroll] = useState(false);
  const openButton = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-openDetailView-btn-${elementId}-${props.sectionKey}`}
      aria-label="Detailansicht (zuklappbar) wird in der nächsten Tabellenzeile angezeigt"
      onClick={onClick}
      type="text"
    >
      <DirectionDownOutlined />
    </Button>
  );
  const closeButton = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-expnadDetailView-btn-${elementId}-${props.sectionKey}`}
      aria-label="Detailsicht aufklappen"
      onClick={onClick}
      type="text"
    >
      <DirectionRightOutlined />
    </Button>
  );
  const [state, setState] = useState<{
    tableExpandedRowKeys: string[];
    expandAll: boolean;
    content: GesamtsummenNormadressatenItemInterface[];
  }>({
    tableExpandedRowKeys: [],
    expandAll: false,
    content: [],
  });
  const changeRowsKey = (expanded: boolean, record: GesamtsummenNormadressatenItemInterface) => {
    if (!expanded) {
      setState({ ...state, tableExpandedRowKeys: [...state.tableExpandedRowKeys, record.key] });
    } else {
      setState({ ...state, tableExpandedRowKeys: state.tableExpandedRowKeys.filter((key) => key !== record.key) });
    }
  };
  const expandedConfig: ExpandableConfig<GesamtsummenNormadressatenItemInterface> = {
    expandedRowRender: (record: GesamtsummenNormadressatenItemInterface) => {
      return !record.summe && !record.details ? <div>{record.description}</div> : undefined;
    },
    rowExpandable: (record: GesamtsummenNormadressatenItemInterface) => {
      {
        return !record.summe && !record.details;
      }
    },
    expandIcon: ({ expanded, record }) => {
      if (!record.summe) {
        if (!record.details) {
          if (expanded) {
            return openButton(() => changeRowsKey(expanded, record), record.id);
          } else {
            return closeButton(() => changeRowsKey(expanded, record), record.id);
          }
        } else {
          if (state.expandAll) {
            return openButton(() => setState({ ...state, expandAll: false }), record.id);
          } else {
            return closeButton(() => setState({ ...state, expandAll: true }), record.id);
          }
        }
      }
      return undefined;
    },
  };
  useEffect(() => {
    if (state.expandAll && !props.isExpertMode) {
      setState({ ...state, content: props.tableVals.content });
    } else {
      if (props.tableVals.content.length) {
        const tableContent = [props.tableVals.content[0]];
        // Show the rest of the table content
        if (!props.isExpertMode) {
          tableContent.push(props.tableVals.content[1]);
        }
        setState({ ...state, content: tableContent });
      } else {
        setState({ ...state, content: [] });
      }
    }
  }, [props.tableVals.content, state.expandAll, props.isExpertMode]);

  useEffect(() => {
    const hasElement = props.tableVals.content.some((item) => item.id === props.elementToShowId);
    if (props.elementToShowId && hasElement) {
      setState((state) => {
        setHasItemToScroll(true);
        return { ...state, tableExpandedRowKeys: [props.elementToShowId], expandAll: hasElement };
      });
    }
  }, [props.tableVals.content, props.elementToShowId]);

  useEffect(() => {
    // Use setTimeout to wait when all items shown and take selected one
    setTimeout(() => {
      if (hasItemToScroll) {
        const element = document.body.querySelector(`tr[data-row-key="${props.elementToShowId}"]`);
        if (element) {
          const y = element.getBoundingClientRect().top + window.scrollY - 110;
          window.scroll({
            top: y,
            behavior: 'smooth',
          });
        }
        setHasItemToScroll(false);
      }
    }, 10);
  }, [state.tableExpandedRowKeys, hasItemToScroll]);

  useEffect(() => {
    if (!props.isVisible && isFocus) {
      btnNewVorgabe.current?.focus();
      btnNewVorgabe.current?.blur();
    }
  }, [props.isVisible]);
  const contentLength = props.tableVals.content.filter(
    (item) => item.key !== 'tableSumme' && item.key !== 'tableDetails',
  ).length;
  return (
    <>
      <Title level={2}>
        {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${props.sectionKey}.title`)}
      </Title>
      {props.isExpertMode && (
        <>
          <Button
            disabled={contentLength === 0}
            id={`egfa-erfuellungsaufwand-btnCsvExport-${props.sectionKey}`}
            onClick={() => props.exportCsv(props.sectionKey)}
            className="normadressaten-export-csv-btn"
          >
            {t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${props.sectionKey}.${
                contentLength > 1 ? 'btnCsvsExport' : 'btnCsvExport'
              }`,
            )}
          </Button>
        </>
      )}
      <div className={`gesamtsummen-normadressaten-table-holder ${props.isExpertMode ? 'expert-mode' : ''}`}>
        <div className="buttons-holder">
          {!props.isExpertMode && (
            <>
              <Button
                disabled={contentLength === 0}
                id={`egfa-erfuellungsaufwand-btnCsvExport-${props.sectionKey}`}
                onClick={() => props.exportCsv(props.sectionKey)}
              >
                {t(
                  `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${props.sectionKey}.${
                    contentLength > 1 ? 'btnCsvsExport' : 'btnCsvExport'
                  }`,
                )}
              </Button>
              <Button
                id={`egfa-erfuellungsaufwand-btnNewVorgabe-${props.sectionKey}`}
                onClick={() => {
                  props.openModal(props.sectionKey);
                  setFocus(true);
                }}
                ref={btnNewVorgabe}
              >
                {t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${props.sectionKey}.btnNewVorgabe`)}
              </Button>
            </>
          )}
        </div>
        <Table
          expandedRowKeys={state.tableExpandedRowKeys}
          rowKey={(record) => record.id}
          rowClassName={(record) => (record.summe ? 'summe-row' : '') || (record.details ? 'details-row' : '')}
          columns={props.tableVals.columns}
          dataSource={state.content}
          pagination={false}
          className="gesamtsummen-normadressaten-table"
          expandable={{ ...expandedConfig, rowExpandable: (record) => !record.summe }}
          locale={{
            emptyText: (
              <div
                className="no-data-placeholder"
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.noDataSimple`, {
                    sectionKey: t(
                      `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${props.sectionKey}.textTitle`,
                    ),
                  }),
                }}
              ></div>
            ),
          }}
        />
      </div>
    </>
  );
}
