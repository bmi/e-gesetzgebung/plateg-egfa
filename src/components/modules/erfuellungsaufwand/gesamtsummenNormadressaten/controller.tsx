// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import { TableComponentProps } from '@plateg/theme';

export interface GesamtsummenNormadressatenItemInterface {
  id: string;
  bezeichnung: string | ReactElement;
  jaehrlichenPersonalaufwands: string;
  jaehrlichenSachaufwands: string;
  einmaligerPersonalaufwand: string;
  einmaligerSachaufwand: string;
  description?: React.ReactElement;
  summe?: boolean;
  details?: boolean;
  key: string;
  action?: ReactElement;
  className?: string;
}
export function getTableVals(
  content: GesamtsummenNormadressatenItemInterface[],
  typ?: string,
  isExpertMode?: boolean,
): TableComponentProps<GesamtsummenNormadressatenItemInterface> {
  const typeTable = typ || '';
  const columns: ColumnsType<GesamtsummenNormadressatenItemInterface> = [
    {
      title: i18n.t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.bezeichnung'),
      key: 'bezeichnung',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.bezeichnung}</Text>;
      },
      className: 'bezeichnung-column',
    },
    {
      title: i18n.t(
        `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.jaehrlichenPersonalaufwands${typeTable}`,
      ),
      key: 'jaehrlichenPersonalaufwands',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.jaehrlichenPersonalaufwands}</Text>;
      },
    },
    {
      title: i18n.t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.jaehrlichenSachaufwands'),
      key: 'jaehrlichenSachaufwands',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.jaehrlichenSachaufwands}</Text>;
      },
    },
    {
      title: i18n.t(
        `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.einmaligerPersonalaufwand${typeTable}`,
      ),
      key: 'einmaligerPersonalaufwand',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.einmaligerPersonalaufwand}</Text>;
      },
    },
    {
      title: i18n.t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.einmaligerSachaufwand'),
      key: 'einmaligerSachaufwand',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement => {
        return <Text>{record.einmaligerSachaufwand}</Text>;
      },
    },
  ];

  if (!isExpertMode) {
    columns.push({
      title: i18n.t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.table.th.actions'),
      key: 'actions',
      render: (record: GesamtsummenNormadressatenItemInterface): ReactElement | undefined => {
        return record.action ?? undefined;
      },
    });
  }
  return {
    expandable: false,
    columns,
    content,
    id: '',
  };
}
