// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './comment-section.less';

import { Button, Checkbox, Form, Input } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';
import { GeneralFormWrapper } from '@plateg/theme';

interface CommentSectionComponentProps {
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  saveUpdatedVorgabe: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
  handleFormChange: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
}
interface CommentSectionComponentState {
  isCommentVisible: boolean;
  isCommentEditable: boolean;
  comment: string;
  commentChecked: boolean;
}
export function CommentSectionComponent(props: CommentSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [state, setState] = useState<CommentSectionComponentState>({
    isCommentVisible: false,
    isCommentEditable: false,
    comment: '',
    commentChecked: false,
  });
  const [form] = Form.useForm();
  const noAnswer = <i>keine Angabe</i>;

  useEffect(() => {
    if (props.vorgabe) {
      form.setFieldsValue({
        ...props.vorgabe,
      });
      setState({
        ...state,
        comment: (props.vorgabe.bemerkungen as string) || '',
        commentChecked: false,
        isCommentVisible: false,
        isCommentEditable: false,
      });
    }
  }, [props.vorgabe]);

  return (
    <>
      {!state.isCommentEditable && state.comment?.length > 0 && (
        <div className="comment-metadata-section">
          <dl>
            <div className="review-row">
              <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.commentPreviewLabel')}</dt>
              <dd>{state.comment || noAnswer}</dd>
            </div>
          </dl>
          <div className="button-holder">
            <Button
              id="egfa-erfuellungsaufwand-vorgabe-edit-comment-btn"
              type="text"
              size="large"
              className="blue-text-button"
              onClick={() => {
                setState({ ...state, isCommentEditable: true, isCommentVisible: true });
              }}
            >
              {t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.btnEdit')}
            </Button>
          </div>
        </div>
      )}

      {(state.isCommentEditable || state.comment?.length < 1) && (
        <GeneralFormWrapper form={form} layout="vertical" scrollToFirstError={true} noValidate>
          <div className="comment-section">
            <Checkbox
              id="vorgabe-review-commentCheckbox"
              aria-label={t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.commentCheckboxLabel')}
              onChange={(e) => {
                setState({ ...state, commentChecked: e.target.checked, isCommentVisible: e.target.checked });
              }}
              defaultChecked={state.comment?.length > 0}
              checked={state.commentChecked}
            >
              {t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.commentCheckboxLabel')}
            </Checkbox>
            {state.isCommentVisible && (
              <>
                <Form.Item
                  label={<span>{t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.textareaLabel')}</span>}
                  name="bemerkungen"
                >
                  <Input.TextArea rows={4} cols={2} />
                </Form.Item>
                <Button
                  id="egfa-erfuellungsaufwand-vorgabe-save-btn"
                  type="primary"
                  size="middle"
                  onClick={() => {
                    const updatedVorgabe = form.getFieldsValue(true) as EgfaModuleErfuellungsaufwandItemEntityDTO;
                    setState({
                      ...state,
                      comment: updatedVorgabe.bemerkungen as string,
                      isCommentEditable: false,
                      isCommentVisible: false,
                      commentChecked: (updatedVorgabe.bemerkungen as string).length !== 0,
                    });
                    props.handleFormChange(form.getFieldsValue(true) as EgfaModuleErfuellungsaufwandItemEntityDTO);
                    props.saveUpdatedVorgabe(updatedVorgabe);
                  }}
                >
                  {t('egfa.modules.erfuellungsaufwand.vorgabePage.comment.btnSave')}
                </Button>
              </>
            )}
          </div>
        </GeneralFormWrapper>
      )}
    </>
  );
}
