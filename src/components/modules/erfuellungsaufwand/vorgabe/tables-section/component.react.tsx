// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Switch } from 'antd';
import { FormInstance } from 'antd/es/form';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemAufwandKategorieType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';
import { GeneralFormWrapper } from '@plateg/theme';

import { AUFWAND_TABLES } from '../aufwand-tables';
import { ErfuellungsaufwandTableItem, sumZeilen } from '../controller';
import { ExpensesDrawerType } from '../expenses-drawer/component.react';
import { FallzahlTable } from '../fallzahl-table/component.react';
import { BerechnungsartRadios } from './berechnungsart-radios/component.react';
import { CategorySelect } from './category-select/component.react';

interface TablesSectionProps {
  sectionName: EgfaErfuellungItemAufwandGesamtsummeType;
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  setUpdatedVorgabe: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
  selectedSection: string | null;
  handleFormChange: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
  setShowCategory: (val: boolean) => void;
  formAufwandsTable: FormInstance;
}
export function TablesSection(props: TablesSectionProps): React.ReactElement {
  const { t } = useTranslation();
  const formAufwandsTable = props.formAufwandsTable;
  const typBuerger =
    props.vorgabe.normadressat === EgfaErfuellungItemNormadressat.Buerger &&
    (props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich ||
      props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig)
      ? 'Buerger'
      : '';
  const patternAufwandsTable: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO = {
    aufwandstabellenZeilen: [],
    berechnungsart: EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall,
    gesamtsummeType: props.sectionName,
    kategorie: undefined,
    veraenderungAnzahl: undefined,
    veraenderungHaeufigkeit: undefined,
    veraenderungFallzahl: undefined,
    veraenderungHinweise: undefined,
  };
  if (
    props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig ||
    props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich
  ) {
    patternAufwandsTable.berechnungsart = EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten;
  }
  const [showZeitwerte, setShowZeitwerte] = useState(false);
  const [selectedAufwandsTable, setSelectedAufwandsTable] =
    useState<EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO>(patternAufwandsTable);
  const [selectedBerechnungsart, setSelectedBerechnungsart] = useState<EgfaErfuellungItemBerechnungsartType>(
    patternAufwandsTable.berechnungsart,
  );
  const [drawerType, setDrawerType] = useState<ExpensesDrawerType>(ExpensesDrawerType.verwaltung);

  useEffect(() => {
    let existingTable;
    if (props.vorgabe && props.vorgabe.aufwandstabellen.length) {
      existingTable = props.vorgabe.aufwandstabellen.filter((item) => item.gesamtsummeType === props.sectionName)[0];
    }

    setSelectedAufwandsTable(existingTable || patternAufwandsTable);
    setSelectedBerechnungsart(existingTable?.berechnungsart || patternAufwandsTable.berechnungsart);

    switch (props.vorgabe.normadressat) {
      case EgfaErfuellungItemNormadressat.Verwaltung:
        setDrawerType(ExpensesDrawerType.verwaltung);
        break;
      case EgfaErfuellungItemNormadressat.Wirtschaft:
        setDrawerType(ExpensesDrawerType.wirtschaft);
        break;
      default:
        break;
    }
  }, [props.vorgabe, props.sectionName]);

  // Apply data to from elements
  useEffect(() => {
    formAufwandsTable.setFieldsValue({
      ...selectedAufwandsTable,
    });
    update();
  }, [selectedAufwandsTable]);

  /**
   * Apply updated Aufwands table to current vorgabe
   * @param updatedTable
   */
  const updateAufwandsTable = (
    updatedTable: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  ): EgfaModuleErfuellungsaufwandItemEntityDTO => {
    const prepareTables = props.vorgabe.aufwandstabellen.filter((item) => item.gesamtsummeType !== props.sectionName);

    const summeRow = updatedTable.aufwandstabellenZeilen.find((zeil) => zeil.summe);
    updatedTable.aufwandstabellenZeilen = updatedTable.aufwandstabellenZeilen.filter((zeil) => !zeil.summe);
    if (summeRow) {
      summeRow.personalaufwandSumme = Math.round(sumZeilen([updatedTable], props.sectionName, typBuerger).sum);
      updatedTable.aufwandstabellenZeilen = [...updatedTable.aufwandstabellenZeilen, summeRow];
    }
    const newVorgabe = {
      ...props.vorgabe,
      aufwandstabellen: [...prepareTables, updatedTable],
    };
    return newVorgabe;
  };

  const update = (triggerFormChange = false) => {
    const newVorgabe = updateAufwandsTable(
      formAufwandsTable.getFieldsValue(true) as EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
    );
    props.setUpdatedVorgabe(newVorgabe);
    if (triggerFormChange) {
      props.handleFormChange(newVorgabe);
    }
  };

  const updateAufwandsTableZeilenHandler = (updatedZeilen: ErfuellungsaufwandTableItem[]) => {
    const formValues = formAufwandsTable.getFieldsValue(
      true,
    ) as EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO;

    const onlyWithDifferentBerechnungsart = formValues.aufwandstabellenZeilen.filter(
      (item) => item.berechnungsart !== formValues.berechnungsart || item.summe, // item.summe is reserved for Expert mode
    );
    formValues.aufwandstabellenZeilen = [...onlyWithDifferentBerechnungsart, ...updatedZeilen];
    // Actualize selected table with updated zeilen
    setSelectedAufwandsTable({ ...formValues });
    update(true);
  };

  const showCategory =
    props.vorgabe.normadressat !== EgfaErfuellungItemNormadressat.Verwaltung &&
    (selectedBerechnungsart !== EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten ||
      (selectedBerechnungsart === EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten &&
        props.vorgabe.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft) ||
      props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig) &&
    (props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig ||
      props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig);

  props.setShowCategory(showCategory);

  const generateTable = (
    sectionName: EgfaErfuellungItemAufwandGesamtsummeType,
    berechnungsart: EgfaErfuellungItemBerechnungsartType,
    aufwandstabellenZeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO[],
    updateAufwandsTableZeilen: (updatedZeilen: ErfuellungsaufwandTableItem[]) => void,
  ): React.ReactElement => {
    let SelectedTableComponent = AUFWAND_TABLES[sectionName][berechnungsart];
    if (
      sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig ||
      sectionName === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich
    ) {
      SelectedTableComponent =
        AUFWAND_TABLES[sectionName][EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten];
    }

    return (
      <SelectedTableComponent
        showZeitwerte={showZeitwerte}
        isBurger={typBuerger === 'Buerger'}
        drawerType={drawerType}
        aufwandstabellenZeilen={aufwandstabellenZeilen
          .filter((item) => item.berechnungsart === berechnungsart && !item.summe) // item.summe is reserved for Expert mode
          .sort((a, b) => {
            return a.orderIndex - b.orderIndex;
          })}
        updateAufwandsTableZeilen={updateAufwandsTableZeilen}
        berechnungsart={berechnungsart}
        normadressat={props.vorgabe.normadressat}
        vorgabeType={props.vorgabe.art}
      />
    );
  };

  return (
    <>
      <div className="tables-section">
        <GeneralFormWrapper
          form={formAufwandsTable}
          layout="vertical"
          scrollToFirstError={true}
          noValidate={false}
          className="gesamtsummen-form"
          onChange={() => {
            update(true);
          }}
        >
          <Title level={2}>
            {t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.title`, {
              sectionDefinition: t(
                `egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.${props.sectionName}${typBuerger}`,
              ),
            })}
          </Title>
          <Title level={3}>{t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.fallzahlTitle`)}</Title>

          <FallzahlTable
            aufwandsTabelle={selectedAufwandsTable}
            form={formAufwandsTable}
            normadressat={props.vorgabe.normadressat}
            berechnungsart={selectedBerechnungsart}
          />
          {props.sectionName !== EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich &&
          props.sectionName !== EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig ? (
            <>
              <BerechnungsartRadios
                onChange={(value: EgfaErfuellungItemBerechnungsartType) => setSelectedBerechnungsart(value)}
                isBurger={typBuerger === 'Buerger'}
              />
              {(props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig ||
                props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich) &&
                selectedBerechnungsart === EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten && (
                  <>
                    <Switch
                      style={{ margin: '15px' }}
                      defaultChecked={showZeitwerte}
                      onChange={(val) => {
                        setShowZeitwerte(val);
                      }}
                    />
                    {t(
                      `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN.zeitwerteLabel`,
                    )}
                  </>
                )}
            </>
          ) : (
            <Title level={3}>
              {t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.aufwandBerechnenTitle`)}
            </Title>
          )}
        </GeneralFormWrapper>
        {generateTable(
          props.sectionName,
          selectedBerechnungsart,
          selectedAufwandsTable.aufwandstabellenZeilen,
          updateAufwandsTableZeilenHandler,
        )}
        {showCategory && (
          <CategorySelect
            onChange={(value: EgfaErfuellungItemAufwandKategorieType) => {
              formAufwandsTable.setFieldsValue({ kategorie: value });
              update(true);
            }}
            value={formAufwandsTable.getFieldValue('kategorie') as string}
            drawerType={drawerType}
            vorgabeType={props.vorgabe.art}
            normAdressatType={props.vorgabe.normadressat}
            berechnungsArt={selectedBerechnungsart}
            sectionName={props?.selectedSection}
          />
        )}
      </div>
    </>
  );
}
