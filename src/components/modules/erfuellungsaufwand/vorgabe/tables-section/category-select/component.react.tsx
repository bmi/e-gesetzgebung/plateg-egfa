// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './category-select.less';

import { Form, Select } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemAufwandKategorieType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
} from '@plateg/rest-api';

import { ExpensesDrawerType } from '../../expenses-drawer/component.react';

export interface CategorySelectProps {
  onChange: (value: EgfaErfuellungItemAufwandKategorieType) => void;
  value: EgfaErfuellungItemAufwandKategorieType | string;
  drawerType: ExpensesDrawerType;
  vorgabeType?: EgfaErfuellungItemArtType;
  normAdressatType?: EgfaErfuellungItemNormadressat;
  berechnungsArt?: EgfaErfuellungItemBerechnungsartType;
  sectionName?: string | null;
}
interface CategoryItem {
  text: string;
  value: EgfaErfuellungItemAufwandKategorieType;
}

export function CategorySelect(props: CategorySelectProps): React.ReactElement {
  const { t } = useTranslation();
  const [categoryItems, setCategoryItems] = useState<CategoryItem[]>([]);
  const selectContainerRef = useRef<HTMLDivElement>(null);

  const categoryItemsOhneInfopflicht = (
    Object.keys(EgfaErfuellungItemAufwandKategorieType) as (keyof typeof EgfaErfuellungItemAufwandKategorieType)[]
  )
    .filter((item) => item !== 'Informationspflicht')
    .map((item) => {
      return {
        text: t(
          `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.categoryOptions.${
            item.charAt(0).toLowerCase() + item.slice(1)
          }`,
        ),
        value: EgfaErfuellungItemAufwandKategorieType[item],
      };
    });

  const exceptionForCategoryItemsBasedOnVorgabeAndWirtschaft = props.vorgabeType === EgfaErfuellungItemArtType.Vorgabe;

  const isInfoPflichtExcluded = exceptionForCategoryItemsBasedOnVorgabeAndWirtschaft;

  const refinedCategories = isInfoPflichtExcluded
    ? categoryItemsOhneInfopflicht
    : (
        Object.keys(EgfaErfuellungItemAufwandKategorieType) as (keyof typeof EgfaErfuellungItemAufwandKategorieType)[]
      ).map((item) => {
        return {
          text: t(
            `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.categoryOptions.${
              item.charAt(0).toLowerCase() + item.slice(1)
            }`,
          ),
          value: EgfaErfuellungItemAufwandKategorieType[item],
        };
      });

  useEffect(() => {
    setCategoryItems(refinedCategories);
  }, []);

  useEffect(() => {
    setCategoryItems(refinedCategories);
  }, [props.berechnungsArt, props.sectionName]);

  return (
    <>
      <Title level={3}>
        {t(`egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.categoryTitle`)}
      </Title>
      <div className="aufwand-select-container" ref={selectContainerRef}>
        <Form.Item
          rules={[
            {
              required: true,
              whitespace: true,
              message: t(
                `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.categoryRequiredMessage`,
              ),
            },
          ]}
          name={'kategorie'}
          label={t(
            `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.chooseCategory`,
          )}
        >
          <Select
            getPopupContainer={() => selectContainerRef.current as HTMLElement}
            placeholder={t(
              `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL.categoryOptions.placeHolder`,
            )}
            value={props.value as EgfaErfuellungItemAufwandKategorieType}
            onChange={props.onChange}
          >
            {categoryItems.map((category) => (
              <Select.Option key={category.value} value={category.value}>
                {category.text}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </div>
    </>
  );
}
