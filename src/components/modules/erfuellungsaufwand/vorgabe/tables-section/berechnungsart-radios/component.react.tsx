// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemBerechnungsartType } from '@plateg/rest-api';

export interface BerechnungsartRadiosProps {
  onChange: (berechnungsart: EgfaErfuellungItemBerechnungsartType) => void;
  isBurger?: boolean;
}

export function BerechnungsartRadios(props: BerechnungsartRadiosProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <Title level={3}>{t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.aufwandBerechnenTitle`)}</Title>
      <p>{t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.aufwandBerechnenInfo`)}</p>
      <Form.Item name="berechnungsart">
        <Radio.Group
          className="horizontal-radios"
          onChange={(event) => props.onChange(event.target.value as EgfaErfuellungItemBerechnungsartType)}
          name="berechnungsart"
        >
          <Radio
            id="egfa-erfuellungsaufwand-berechnungsart-radio"
            className="horizontal-radios"
            value={EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall}
          >
            {t(
              `egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.berechnungsart.${EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall}`,
            )}
          </Radio>
          <Radio
            id="egfa-erfuellungsaufwand-ufwandBerechnen-radio"
            className="horizontal-radios"
            value={EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten}
          >
            {t(
              `egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.berechnungsart.${EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten}`,
            )}
          </Radio>
          {!props.isBurger && (
            <Radio
              id="egfa-erfuellungsaufwand-ufwandBerechnen-radio"
              className="horizontal-radios"
              value={EgfaErfuellungItemBerechnungsartType.PersonenTageMonate}
            >
              {t(
                `egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.berechnungsart.${EgfaErfuellungItemBerechnungsartType.PersonenTageMonate}`,
              )}
            </Radio>
          )}
        </Radio.Group>
      </Form.Item>
    </>
  );
}
