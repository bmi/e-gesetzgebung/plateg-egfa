// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
} from '@plateg/rest-api';

import { ErfuellungsaufwandTableItem } from '../controller';
import { ExpensesDrawerType } from '../expenses-drawer/component.react';
import { PERSONALAUFWAND_EINMALIG } from './PERSONALAUFWAND_EINMALIG/index';
import { PERSONALAUFWAND_JAEHRLICH } from './PERSONALAUFWAND_JAEHRLICH/index';
import { SACHAUFWAND_EINMALIG } from './SACHAUFWAND_EINMALIG/index';
import { SACHAUFWAND_JAEHRLICH } from './SACHAUFWAND_JAEHRLICH/index';

export interface AufwandTablesInterface {
  [s: string]: AufwandTablesItemInterface;
}
export interface AufwandTablesItemInterface {
  [s: string]: (props: AufwandTableProps) => React.ReactElement;
}
export interface AufwandTableProps {
  berechnungsart: EgfaErfuellungItemBerechnungsartType;
  drawerType: ExpensesDrawerType;
  isBurger?: boolean;
  aufwandstabellenZeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO[];
  updateAufwandsTableZeilen: (updatedZeilen: ErfuellungsaufwandTableItem[]) => void;
  isEinmalig?: boolean;
  normadressat: EgfaErfuellungItemNormadressat;
  showZeitwerte?: boolean;
  vorgabeType?: EgfaErfuellungItemArtType;
}
export interface AufwandTableDatarow {
  key: string;
  id?: string;
  [s: number]: number | React.ReactElement;
}
export const AUFWAND_TABLES: AufwandTablesInterface = {
  PERSONALAUFWAND_JAEHRLICH,
  SACHAUFWAND_JAEHRLICH,
  PERSONALAUFWAND_EINMALIG,
  SACHAUFWAND_EINMALIG,
};
