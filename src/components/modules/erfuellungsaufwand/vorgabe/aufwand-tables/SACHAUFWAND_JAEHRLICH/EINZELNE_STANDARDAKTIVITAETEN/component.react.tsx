// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { EgfaErfuellungItemBerechnungsartType } from '@plateg/rest-api';
import { DeleteOutlined, PlusOutlined, Restore } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  convertFirstCellToHeader,
  ErfuellungsaufwandTableItem,
  formCellRender,
  getCellTextarea,
  zeitaufwandDifferenz,
} from '../../../controller';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';

export function SACHAUFWAND_JAEHRLICH_EINZELNE_STANDARDAKTIVITAETEN(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = 'egfa.modules.erfuellungsaufwand.vorgabePage.SACHAUFWAND_JAEHRLICH_GESAMTAUFWAND_PRO_FALL';
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);
  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    bezeichnung: '',
    bemerkungen: '',
    sachaufwandBisher: undefined,
    sachaufwandNeu: undefined,
    sachaufwandDifferenz: undefined,
    berechnungsart: EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall,
    orderIndex: 0,
  };
  const patternForCheckinEmptyFields = {
    bezeichnung: '',
    bemerkungen: '',
    sachaufwandBisher: undefined,
    sachaufwandNeu: undefined,
    sachaufwandDifferenz: undefined,
  };
  useEffect(() => {
    convertFirstCellToHeader();
  }, []);
  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
    index?: number,
    updateKey?: string,
  ): ErfuellungsaufwandTableItem[] => {
    const rowList = rowItemsList.map((item) => {
      return {
        ...item,
      };
    });
    if (updateKey === 'sachaufwandBisher' || updateKey === 'sachaufwandNeu') {
      rowList[(index || 0) - 1].sachaufwandDifferenz = zeitaufwandDifferenz(
        rowList[(index || 0) - 1].sachaufwandNeu,
        rowList[(index || 0) - 1].sachaufwandBisher,
      );
    }
    return rowList;
  };

  const intialDataKeys = [
    'aufwandbeschaffung',
    'anschaffungKosten',
    'aufwandAnlagen',
    'wartungsAufwand',
    'aufwandInanspruchnahme',
    'fortbildungen',
    'sachmittel',
    'wegesachkosten',
    'sonstigeSachkosten',
  ];
  const staticInitialData = intialDataKeys.map((key) => ({
    ...patternItem,
    bezeichnung: t(`${translationKey}.zeileKategorien.${key}`),
    id: uuidv4(),
  }));
  const bezeichnungIsNotStandard = (item: string): boolean => {
    return intialDataKeys.find((key) => t(`${translationKey}.zeileKategorien.${key}`) === item) ? false : true;
  };
  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      staticInitialData={staticInitialData}
      sumAtStart
      renderTable={(
        data: AufwandTableDatarow[],
        updateHandler,
        deleteRow?: (itemRowKey: string) => void,
        addNewRow?: () => void,
        restoreDefaultRows?: () => void,
        deleteAllEmptyRows?: (pattern: { [s: string]: string | undefined }, standardBezeichnungen?: string[]) => void,
        triggerKey?: string,
      ) => {
        return (
          <>
            <Table
              dataSource={data}
              pagination={false}
              className="aufwand-table"
              rowClassName={(record) => (record.key.indexOf('summeRow') !== -1 ? 'summe-row' : '')}
            >
              <Column
                title={t(`${translationKey}.tableHeaders.bezeichnung`).toString()}
                dataIndex="bezeichnung"
                key="bezeichnung"
                className="first-heading"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key.indexOf('hidden-row-') !== -1) {
                    return undefined;
                  }

                  const isSummeRow = record.key === 'summeRow' || record.key === 'summeRowStart';
                  const isStandardBezeichnung = !bezeichnungIsNotStandard(dynamicDataOnFly[index - 1]?.bezeichnung);

                  if (!isSummeRow && isStandardBezeichnung) {
                    return <div className="text-cell text">{dynamicDataOnFly[index - 1].bezeichnung}</div>;
                  }

                  if (!isSummeRow) {
                    return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bezeichnung'], updateHandler);
                  }

                  return <div className="text-cell text">Summe</div>;
                }}
              />
              <Column
                title={t(`${translationKey}.tableHeaders.bemerkungen`).toString()}
                dataIndex="bemerkungen"
                key="bemerkungen"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key === 'summeRow' || record.key === 'summeRowStart') {
                    return <div className="hinweis-cell"></div>;
                  }
                  return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bemerkungen'], updateHandler);
                }}
              />
              <ColumnGroup
                title={t(`${translationKey}.tableHeaders.sachAufwandProFallGroup`).toString()}
                key="sachAufwandProFallGroup"
                className="grouping"
              >
                <Column
                  title={t(`${translationKey}.tableHeaders.sachaufwandBisher`).toString()}
                  dataIndex="sachaufwandBisher"
                  key="sachaufwandBisher"
                  className="sachaufwand-jahrlich"
                  shouldCellUpdate={(record: AufwandTableDatarow) => {
                    return record.key === 'summeRow' || record.key === 'summeRowStart';
                  }}
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'sachaufwandBisher',
                      currencyLabel: '€',
                      updateTableValues: () => updateHandler(index, 'sachaufwandBisher'),
                      hasDecimal: true,
                      allowNegative: false,
                      oneDigitAfterComma: false,
                    });
                  }}
                />
                <Column
                  title={t(`${translationKey}.tableHeaders.sachaufwandNeu`).toString()}
                  dataIndex="sachaufwandNeu"
                  key="sachaufwandNeu"
                  className="sachaufwand-jahrlich"
                  shouldCellUpdate={(record: AufwandTableDatarow) => {
                    return record.key === 'summeRow' || record.key === 'summeRowStart';
                  }}
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'sachaufwandNeu',
                      currencyLabel: '€',
                      updateTableValues: () => updateHandler(index, 'sachaufwandNeu'),
                      hasDecimal: true,
                      allowNegative: false,
                      oneDigitAfterComma: false,
                    });
                  }}
                />
                <Column
                  title={t(`${translationKey}.tableHeaders.sachaufwandDifferenz`).toString()}
                  dataIndex="sachaufwandDifferenz"
                  key="sachaufwandDifferenz"
                  className="selected sachaufwand-jahrlich"
                  shouldCellUpdate={(record: AufwandTableDatarow) => {
                    return record.key === 'summeRow' || record.key === 'summeRowStart';
                  }}
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'sachaufwandDifferenz',
                      currencyLabel: '€',
                      updateTableValues: updateHandler,
                      hasDecimal: true,
                      allowNegative: true,
                      oneDigitAfterComma: false,
                      triggerKey,
                    });
                  }}
                />
              </ColumnGroup>
              <Column
                title=""
                dataIndex=""
                key=""
                render={(_: any, record: AufwandTableDatarow) => {
                  return (
                    <div className="button-delete-cell">
                      {record.key !== 'summeRow' && record.key !== 'summeRowStart' && (
                        <Button
                          id={`button-remove-row-${record.key}`}
                          onClick={() => deleteRow?.(record.key)}
                          aria-label="Löschen"
                        >
                          <DeleteOutlined />
                        </Button>
                      )}
                    </div>
                  );
                }}
              />
            </Table>
            <div className="buttons-holder">
              <Button
                id="button-add-row"
                size="small"
                icon={<PlusOutlined />}
                type="link"
                onClick={() => addNewRow?.()}
                className="blue-text-button"
              >
                {t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnAddNewItemMaterial`)}
              </Button>

              <Button
                id="button-restore-default-rows"
                size="small"
                icon={<Restore />}
                type="link"
                className="blue-text-button"
                onClick={() => restoreDefaultRows?.()}
              >
                {t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnRestoreDefaultDataMaterial`)}
              </Button>
              <Button
                id="button-delete-all-empty-rows"
                size="small"
                icon={<DeleteOutlined />}
                type="link"
                className="blue-text-button"
                onClick={() =>
                  deleteAllEmptyRows?.(
                    patternForCheckinEmptyFields,
                    staticInitialData.map((item) => item.bezeichnung),
                  )
                }
              >
                {t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnDeleteEmptyRows`)}
              </Button>
            </div>
          </>
        );
      }}
    />
  );
}
