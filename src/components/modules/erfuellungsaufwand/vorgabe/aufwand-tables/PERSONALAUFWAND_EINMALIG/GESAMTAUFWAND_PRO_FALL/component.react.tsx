// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { InfoComponent } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  averageProYear,
  convertFirstCellToHeader,
  dynamicCellRender,
  ErfuellungsaufwandTableItem,
  formCellRender,
  getCellTextarea,
  zeitaufwandDifferenz,
} from '../../../controller';
import { ExpensesDrawer } from '../../../expenses-drawer/component.react';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';

export function PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = 'egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL';
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);

  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    bezeichnung: t(`${translationKey}.defaultBezeichnung`),
    bemerkungen: '',
    zeitaufwandProFall: undefined,
    lohnkosten: undefined,
    personalaufwandProFall: undefined,
    zeitaufwandBisher: undefined,
    zeitaufwandNeu: undefined,
    zeitaufwandDifferenz: undefined,
    berechnungsart: props.berechnungsart,
    orderIndex: 0,
  };

  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
    index = -1,
    updateKey = '',
  ): ErfuellungsaufwandTableItem[] => {
    const rowList = rowItemsList.map((item) => {
      return {
        ...item,
        personalaufwandProFall: averageProYear(item.zeitaufwandProFall, item.lohnkosten),
      };
    });
    if (updateKey === 'zeitaufwandBisher' || updateKey === 'zeitaufwandNeu') {
      rowList[index].zeitaufwandDifferenz = zeitaufwandDifferenz(
        rowList[index].zeitaufwandNeu,
        rowList[index].zeitaufwandBisher,
      );
    }
    return rowList;
  };

  useEffect(() => {
    convertFirstCellToHeader();
  }, []);

  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      renderTable={(data: AufwandTableDatarow[], updateHandler) => {
        return (
          <>
            <Table
              dataSource={data}
              pagination={false}
              className="aufwand-table"
              rowClassName={(record) => {
                if (record.key.indexOf('summeRow') !== -1) {
                  return 'summe-row';
                }
                return '';
              }}
            >
              <Column
                title={t(`${translationKey}.tableHeaders.bezeichnung`).toString()}
                dataIndex="bezeichnung"
                key="bezeichnung"
                className="first-heading"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  return (
                    <div className="text-cell text">
                      {record.key !== 'summeRow' ? dynamicDataOnFly[index]?.bezeichnung : 'Summe'}
                    </div>
                  );
                }}
              />
              <Column
                title={t(`${translationKey}.tableHeaders.bemerkungen`).toString()}
                dataIndex="bemerkungen"
                key="bemerkungen"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key === 'summeRow') {
                    return <div className="hinweis-cell"></div>;
                  }
                  return getCellTextarea(['aufwandstabellenZeilen', index, 'bemerkungen'], updateHandler);
                }}
              />
              {!props.isBurger ? (
                <>
                  <Column
                    title={t(`${translationKey}.tableHeaders.zeitaufwandProFallGroup`).toString()}
                    dataIndex="zeitaufwandProFall"
                    key="zeitaufwandProFall"
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index: index,
                        localVals: dynamicDataOnFly,
                        fieldName: 'zeitaufwandProFall',
                        currencyLabel: 'Min.',
                        updateTableValues: updateHandler,
                        hasDecimal: false,
                        allowNegative: true,
                        oneDigitAfterComma: false,
                      });
                    }}
                  />

                  <Column
                    title={
                      <div>
                        {t(`${translationKey}.tableHeaders.standardlohnsatzGroup`)}{' '}
                        <InfoComponent
                          title={t(
                            `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                          )}
                          titleWithoutPrefix={true}
                          id="verwaltung-2"
                        >
                          <ExpensesDrawer displayType={props.drawerType} />
                        </InfoComponent>
                      </div>
                    }
                    colSpan={2}
                    dataIndex="lohnkosten"
                    key="lohnkosten"
                    shouldCellUpdate={() => false}
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index,
                        localVals: dynamicDataOnFly,
                        fieldName: 'lohnkosten',
                        currencyLabel: '€/Std.',
                        updateTableValues: updateHandler,
                        hasDecimal: true,
                        allowNegative: false,
                        oneDigitAfterComma: false,
                      });
                    }}
                  />
                  <Column
                    title=""
                    dataIndex="lohnsatzInfo"
                    key="lohnsatzInfo"
                    colSpan={0}
                    render={(_: any, record: AufwandTableDatarow) => {
                      return (
                        <div className="text-cell static-text" style={{ textAlign: 'center' }}>
                          {record.key !== 'summeRow' && (
                            <InfoComponent
                              title={t(
                                `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                              )}
                              titleWithoutPrefix={true}
                              id="verwaltung-3"
                            >
                              <ExpensesDrawer displayType={props.drawerType} />
                            </InfoComponent>
                          )}
                        </div>
                      );
                    }}
                  />
                  <Column
                    title={t(`${translationKey}.tableHeaders.personalaufwandProFallGroup`).toString()}
                    dataIndex="personalaufwandProFall"
                    key="personalaufwandProFall"
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return dynamicCellRender(record.key, index, dynamicDataOnFly, 'personalaufwandProFall', '€');
                    }}
                  />
                </>
              ) : (
                <ColumnGroup
                  title={t(`${translationKey}.tableHeaders.zeitaufwandProFallGroup`).toString()}
                  key="zeitaufwandProFallGroup"
                  className="grouping"
                >
                  <Column
                    title={t(`${translationKey}.tableHeaders.zeitaufwandBisher`).toString()}
                    dataIndex="zeitaufwandBisher"
                    key="zeitaufwandBisher"
                    className="sachaufwand-jahrlich"
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index,
                        localVals: dynamicDataOnFly,
                        fieldName: 'zeitaufwandBisher',
                        currencyLabel: 'Min.',
                        updateTableValues: () => updateHandler(index, 'zeitaufwandBisher'),
                        hasDecimal: false,
                        allowNegative: false,
                        oneDigitAfterComma: false,
                      });
                    }}
                  />
                  <Column
                    title={t(`${translationKey}.tableHeaders.zeitaufwandNeu`).toString()}
                    dataIndex="zeitaufwandNeu"
                    key="zeitaufwandNeu"
                    className="sachaufwand-jahrlich"
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index,
                        localVals: dynamicDataOnFly,
                        fieldName: 'zeitaufwandNeu',
                        currencyLabel: 'Min.',
                        updateTableValues: () => updateHandler(index, 'zeitaufwandNeu'),
                        hasDecimal: false,
                        allowNegative: false,
                        oneDigitAfterComma: false,
                      });
                    }}
                  />
                  <Column
                    title={t(`${translationKey}.tableHeaders.zeitaufwandDifferenz`).toString()}
                    dataIndex="zeitaufwandDifferenz"
                    key="zeitaufwandDifferenz"
                    className="selected sachaufwand-jahrlich"
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index,
                        localVals: dynamicDataOnFly,
                        fieldName: 'zeitaufwandDifferenz',
                        currencyLabel: 'Min.',
                        updateTableValues: updateHandler,
                        hasDecimal: false,
                        allowNegative: true,
                        oneDigitAfterComma: false,
                      });
                    }}
                  />
                </ColumnGroup>
              )}
            </Table>
          </>
        );
      }}
    />
  );
}
