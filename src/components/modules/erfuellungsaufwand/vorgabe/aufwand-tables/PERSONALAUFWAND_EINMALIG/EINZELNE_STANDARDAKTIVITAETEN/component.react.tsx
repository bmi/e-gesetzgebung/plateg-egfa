// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { EgfaErfuellungItemBerechnungsartType, EgfaErfuellungItemNormadressat } from '@plateg/rest-api';
import { DeleteOutlined, InfoComponent, PlusOutlined, Restore } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  averageProYear,
  convertFirstCellToHeader,
  dynamicCellRender,
  ErfuellungsaufwandTableItem,
  formCellRender,
  generateStandardActivities,
  generateTableButton,
  getCellTextarea,
} from '../../../controller';
import { ExpensesDrawer, ExpensesDrawerType } from '../../../expenses-drawer/component.react';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';

export type UpdateHandlerType = (index?: number | undefined, updateKey?: string | undefined) => void;

export function PERSONALAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey =
    'egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN';
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);
  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    bezeichnung: '',
    bemerkungen: '',
    lohnsatzDifferenz: undefined,
    personalaufwandProFall: undefined,
    berechnungsart: props.berechnungsart,
    orderIndex: 0,
    zeitaufwandDifferenz: undefined,
  };

  const patternForCheckinEmptyFields = {
    bezeichnung: '',
    bemerkungen: '',
    zeitaufwandDifferenz: undefined,
    lohnsatzDifferenz: undefined,
    personalaufwandProFall: undefined,
  };

  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
  ): ErfuellungsaufwandTableItem[] => {
    return rowItemsList.map((item) => {
      return {
        ...item,
        personalaufwandProFall: averageProYear(item.zeitaufwandDifferenz, item.lohnsatzDifferenz),
      };
    });
  };

  const staticInitialData = generateStandardActivities(
    props.normadressat,
    patternItem,
    props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft ? 'einmalig' : 'vorgabe',
  );
  const bezeichnungIsNotStandard = (bezeichnung: string): boolean => {
    return staticInitialData.find((item) => item.bezeichnung === bezeichnung) ? false : true;
  };

  useEffect(() => {
    convertFirstCellToHeader();
  }, []);

  // helper func to reduce complexity
  const columnRenderHelper = (updateHandler: UpdateHandlerType) => (
    <Column
      render={(_: any, record: AufwandTableDatarow, index) => {
        if (record.key.indexOf('hidden-row-') !== -1) {
          return undefined;
        }

        if (record.key !== 'summeRow' && record.key !== 'summeRowStart') {
          if (bezeichnungIsNotStandard(dynamicDataOnFly[index - 1]?.bezeichnung)) {
            return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bezeichnung'], updateHandler);
          }
          const staticItem = staticInitialData.find(
            (item) => item.bezeichnung === dynamicDataOnFly[index - 1].bezeichnung,
          );
          return (
            <div className="text-cell text">
              <>
                {dynamicDataOnFly[index - 1].bezeichnung}
                {staticItem?.translationKey === 'wartezeiten' && (
                  <>
                    {' '}
                    <InfoComponent
                      title={t(`egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.title`)}
                      titleWithoutPrefix={true}
                      id={`info-drawer-wartezeiten-${uuidv4()}`}
                    >
                      <ExpensesDrawer displayType={ExpensesDrawerType.wegezeiten} />
                    </InfoComponent>
                  </>
                )}
                {staticItem?.zeitwerte && props.showZeitwerte && (
                  <p
                    className="zeitwerte"
                    dangerouslySetInnerHTML={{
                      __html: staticItem.zeitwerte as string,
                    }}
                  />
                )}
              </>
            </div>
          );
        }
        return <div className="text-cell text">Summe</div>;
      }}
      title={t(`${translationKey}.tableHeaders.bezeichnung`).toString()}
      dataIndex="bezeichnung"
      key="bezeichnung"
      className="first-heading"
    />
  );

  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      staticInitialData={staticInitialData}
      sumAtStart
      renderTable={(
        data: AufwandTableDatarow[],
        updateHandler,
        deleteRow?: (itemRowKey: string) => void,
        addNewRow?: () => void,
        restoreDefaultRows?: () => void,
        deleteAllEmptyRows?: (pattern: { [s: string]: string | undefined }, standardBezeichnungen?: string[]) => void,
      ) => {
        return (
          <>
            <Table
              dataSource={data}
              pagination={false}
              className="aufwand-table"
              rowClassName={(record) => {
                if (record.key.indexOf('summeRow') !== -1) {
                  return 'summe-row';
                }
                return '';
              }}
            >
              {columnRenderHelper(updateHandler)}
              <Column
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key === 'summeRow' || record.key === 'summeRowStart') {
                    return <div className="hinweis-cell"></div>;
                  }
                  return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bemerkungen'], updateHandler);
                }}
                title={t(`${translationKey}.tableHeaders.bemerkungen`).toString()}
                key="bemerkungen"
                dataIndex="bemerkungen"
              />
              <Column
                render={(_: any, record: AufwandTableDatarow, index) => {
                  return formCellRender({
                    recordKey: record.key,
                    index: index - 1,
                    localVals: dynamicDataOnFly,
                    fieldName: 'zeitaufwandDifferenz',
                    currencyLabel: 'Min.',
                    updateTableValues: () => updateHandler(index, 'zeitaufwandDifferenz'),
                    hasDecimal: false,
                    allowNegative: false,
                    oneDigitAfterComma: false,
                  });
                }}
                title={t(`${translationKey}.tableHeaders.zeitaufwandProFall`).toString()}
                key="zeitaufwandDifferenz"
                dataIndex="zeitaufwandDifferenz"
              />
              {props.normadressat !== EgfaErfuellungItemNormadressat.Buerger && (
                <>
                  <Column
                    shouldCellUpdate={(record: AufwandTableDatarow) => {
                      return record.key === 'summeRow' || record.key === 'summeRowStart';
                    }}
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index: index - 1,
                        localVals: dynamicDataOnFly,
                        fieldName: 'lohnsatzDifferenz',
                        currencyLabel: '€/Std',
                        updateTableValues: () => updateHandler(index, 'lohnsatzDifferenz'),
                        hasDecimal: true,
                        allowNegative: false,
                        oneDigitAfterComma: false,
                        averageSum:
                          props.berechnungsart === EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten &&
                          (props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft ||
                            props.normadressat === EgfaErfuellungItemNormadressat.Verwaltung),
                      });
                    }}
                    title={
                      <div>
                        {t(`${translationKey}.tableHeaders.standardlohnsatzGroup`)}{' '}
                        <InfoComponent
                          key={`info-drawer-personalaufwand-einmaliger-${props.drawerType}`}
                          title={t(
                            `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                          )}
                          titleWithoutPrefix={true}
                          id={`info-drawer-personalaufwand-einmaliger-${props.drawerType}`}
                        >
                          <ExpensesDrawer displayType={props.drawerType} />
                        </InfoComponent>
                      </div>
                    }
                    key="lohnsatzDifferenz"
                    dataIndex="lohnsatzDifferenz"
                    className="standardlohnsatz"
                    colSpan={2}
                  />
                  <Column
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return (
                        <div className="text-cell static-text" style={{ textAlign: 'center' }}>
                          {record.key !== 'summeRow' && record.key !== 'summeRowStart' && (
                            <InfoComponent
                              key={`info-drawer-${index - 1}`}
                              title={t(
                                `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                              )}
                              titleWithoutPrefix={true}
                              id={`info-drawer-personalaufwand-einmaliger-${index - 1}`}
                            >
                              <ExpensesDrawer displayType={props.drawerType} />
                            </InfoComponent>
                          )}
                        </div>
                      );
                    }}
                    key="lohnsatzInfo"
                    dataIndex="lohnsatzInfo"
                    colSpan={0}
                  />

                  <Column
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return dynamicCellRender(record.key, index - 1, dynamicDataOnFly, 'personalaufwandProFall', '€');
                    }}
                    title={t(`${translationKey}.tableHeaders.personalaufwandProFall`).toString()}
                    key="personalaufwandProFall"
                    dataIndex="personalaufwandProFall"
                  />
                </>
              )}
              <Column
                render={(_: any, record: AufwandTableDatarow) => {
                  return (
                    <div className="button-delete-cell">
                      {record.key !== 'summeRow' && record.key !== 'summeRowStart' && (
                        <Button
                          id={`button-remove-row-${record.key}`}
                          onClick={() => deleteRow?.(record.key)}
                          aria-label="Löschen"
                        >
                          <DeleteOutlined />
                        </Button>
                      )}
                    </div>
                  );
                }}
                key="deleteRow"
              />
            </Table>
            <div className="buttons-holder">
              {generateTableButton(
                'button-add-row',
                <PlusOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnAddNewItem`),
                () => addNewRow?.(),
              )}
              {generateTableButton(
                'button-restore-default-rows',
                <Restore />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnRestoreDefaultData`),
                () => restoreDefaultRows?.(),
              )}
              {generateTableButton(
                'button-delete-all-empty-rows',
                <DeleteOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnDeleteEmptyRows`),
                () =>
                  deleteAllEmptyRows?.(
                    patternForCheckinEmptyFields,
                    staticInitialData.map((item) => item.bezeichnung),
                  ),
              )}
            </div>
          </>
        );
      }}
    />
  );
}
