// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { AufwandTableProps } from '../..';
import { PERSONALAUFWAND_JAEHRLICH_PERSONEN_TAGE_MONATE } from '../../PERSONALAUFWAND_JAEHRLICH/PERSONEN_TAGE_MONATE/component.react';

export function PERSONALAUFWAND_EINMALIG_PERSONEN_TAGE_MONATE(props: AufwandTableProps): React.ReactElement {
  return <PERSONALAUFWAND_JAEHRLICH_PERSONEN_TAGE_MONATE {...props} isEinmalig={true} />;
}
