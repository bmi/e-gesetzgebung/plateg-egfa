// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { AUFWAND_TABLES } from './index';

// test AUFWAND_TABLES
describe('AUFWAND_TABLES', () => {
  it('should be an object', () => {
    expect(AUFWAND_TABLES).to.be.an('object');
  }).timeout(0);
  it('should have the right keys', () => {
    expect(AUFWAND_TABLES).to.have.all.keys([
      'PERSONALAUFWAND_JAEHRLICH',
      'SACHAUFWAND_JAEHRLICH',
      'PERSONALAUFWAND_EINMALIG',
      'SACHAUFWAND_EINMALIG',
    ]);
  }).timeout(0);
  it('each key showld be have three keys', () => {
    expect(AUFWAND_TABLES.PERSONALAUFWAND_JAEHRLICH).to.have.all.keys([
      'EINZELNE_STANDARDAKTIVITAETEN',
      'GESAMTAUFWAND_PRO_FALL',
      'PERSONEN_TAGE_MONATE',
    ]);
    expect(AUFWAND_TABLES.SACHAUFWAND_JAEHRLICH).to.have.all.keys(['EINZELNE_STANDARDAKTIVITAETEN']);
    expect(AUFWAND_TABLES.SACHAUFWAND_EINMALIG).to.have.all.keys(['EINZELNE_STANDARDAKTIVITAETEN']);
    expect(AUFWAND_TABLES.PERSONALAUFWAND_EINMALIG).to.have.all.keys([
      'EINZELNE_STANDARDAKTIVITAETEN',
      'GESAMTAUFWAND_PRO_FALL',
      'PERSONEN_TAGE_MONATE',
    ]);
  }).timeout(0);
}).timeout(0);
