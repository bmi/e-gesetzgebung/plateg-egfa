// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './aufand-table-wrapper.less';

import { Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO } from '@plateg/rest-api';
import { GeneralFormWrapper } from '@plateg/theme';

import { AufwandTableDatarow } from '../';
import { ErfuellungsaufwandTableItem } from '../../controller';

interface AufwandTableWrapperProps {
  aufwandstabellenZeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO[];
  setDynamicDataOnFly: (itemsList: ErfuellungsaufwandTableItem[]) => void;
  recalculateDynamicRowDataOnFly: (
    rowItemsList: ErfuellungsaufwandTableItem[],
    index?: number,
    updateKey?: string,
  ) => ErfuellungsaufwandTableItem[];
  patternItem: ErfuellungsaufwandTableItem;
  updateAufwandsTableZeilen: (updatedZeilen: ErfuellungsaufwandTableItem[]) => void;
  renderTable: (
    data: AufwandTableDatarow[],
    updateHandler: (index?: number, updateKey?: string) => void,
    deleteRow?: (itemRowKey: string) => void,
    addNewRow?: () => void,
    restoreDefaultRows?: () => void,
    deleteAllEmptyRows?: (pattern: { [s: string]: string | undefined }, standardBezeichnungen?: string[]) => void,
    triggerKey?: string,
  ) => React.ReactElement;
  staticInitialData?: ErfuellungsaufwandTableItem[];
  sumAtStart?: boolean;
  customSummaryRows?: string[];
}
export function AufwandTableWrapperComponent(props: AufwandTableWrapperProps): React.ReactElement {
  const [form] = Form.useForm();
  const [data, setData] = useState<AufwandTableDatarow[]>([]);
  const [staticInitialData, setStaticInitialData] = useState<ErfuellungsaufwandTableItem[] | undefined>(
    props.staticInitialData,
  );
  const [triggerKey, setTriggerKey] = useState<string>('');

  useEffect(() => {
    let initialData = props.aufwandstabellenZeilen;

    // If no saved data and has static data - apply static data
    if (!initialData.length && staticInitialData) {
      initialData = [...staticInitialData];
    }
    // If Zeil has no data set it with default row according to pattern
    if (!initialData.length) {
      initialData = [props.patternItem];
    }

    // Prepare data for using it as tables data
    const preparedTableData: AufwandTableDatarow[] = initialData.map((item) => {
      return { key: `regular-row-${item.id || uuidv4()}`, ...item };
    });
    // Add summary row
    const summeRow = { key: 'summeRow' };
    const summeRowStart = { key: 'summeRowStart' };
    preparedTableData.push(summeRow);
    if (props.sumAtStart) {
      preparedTableData.unshift(summeRowStart);
    }
    props.customSummaryRows?.map((rowName) => {
      preparedTableData.push({ key: rowName });
    });
    setData(preparedTableData);

    // Set ready data for using it in the table
    form.setFieldsValue({ aufwandstabellenZeilen: initialData });
    const recalculatedData = props.recalculateDynamicRowDataOnFly(initialData as ErfuellungsaufwandTableItem[]);
    props.setDynamicDataOnFly(recalculatedData);
  }, [props.aufwandstabellenZeilen]);

  const updateAufwandsTableZeilen = (zeilen: ErfuellungsaufwandTableItem[]) => {
    zeilen.forEach((zeile, idx) => {
      zeile.orderIndex = idx;
    });
    props.updateAufwandsTableZeilen(zeilen);
  };

  const updateHandler = (index = -1, updateKey = '') => {
    const formValues = form.getFieldValue('aufwandstabellenZeilen') as ErfuellungsaufwandTableItem[];
    const recalculatedData = props.recalculateDynamicRowDataOnFly(formValues, index, updateKey);
    if (updateKey) {
      setTriggerKey(updateKey);
    }
    props.setDynamicDataOnFly(recalculatedData);
    updateAufwandsTableZeilen(recalculatedData);
  };

  const deleteRow = (itemRowKey: string) => {
    const itemId = itemRowKey.split('regular-row-')[1];
    const formValues = form.getFieldValue('aufwandstabellenZeilen') as ErfuellungsaufwandTableItem[];
    const newFormValues = formValues.filter((item) => item.id !== itemId);
    if (newFormValues.length === 0) {
      setStaticInitialData([]);
    }
    updateAufwandsTableZeilen(newFormValues);
  };

  const addNewRow = () => {
    const formValues = form.getFieldValue('aufwandstabellenZeilen') as ErfuellungsaufwandTableItem[];
    formValues.push({
      ...props.patternItem,
      id: uuidv4(),
    });
    updateAufwandsTableZeilen(formValues);
  };

  const filterDefaultRows = (staticRows: ErfuellungsaufwandTableItem[], formValues: ErfuellungsaufwandTableItem[]) => {
    let filteredRows: ErfuellungsaufwandTableItem[] = [];
    let additionalRows: ErfuellungsaufwandTableItem[] | undefined = [];
    additionalRows = formValues.filter((formRow) => {
      return !staticRows.some((item) => item.bezeichnung === formRow.bezeichnung);
    });
    filteredRows = staticRows.map(({ ...stRow }) => {
      formValues.forEach((formRow) => {
        if (stRow.bezeichnung === formRow.bezeichnung) {
          Object.keys(formRow).forEach((key) => {
            if (key in stRow && key !== 'id') {
              stRow[key] = formRow[key];
            }
          });
        }
      });
      return stRow;
    });
    return filteredRows.concat(additionalRows);
  };

  const restoreDefaultRows = () => {
    const formValues = form.getFieldValue('aufwandstabellenZeilen') as ErfuellungsaufwandTableItem[];
    if (props.staticInitialData) {
      updateAufwandsTableZeilen(filterDefaultRows([...props.staticInitialData], [...formValues]));
    }
  };

  const deleteAllEmptyRows = (
    patternFieldsToCheck: { [s: string]: string | undefined },
    standardBezeichnungen?: string[],
  ) => {
    const formValues = form.getFieldValue('aufwandstabellenZeilen') as ErfuellungsaufwandTableItem[];

    const updatedList = formValues.filter((item) => {
      return !Object.keys(patternFieldsToCheck).every(
        (key) =>
          item[key] === '' ||
          item[key] === undefined ||
          item[key] === null ||
          item[key] === true ||
          (key === 'bezeichnung' && standardBezeichnungen && standardBezeichnungen.includes(item[key])),
      );
    });
    setStaticInitialData([]);
    updateAufwandsTableZeilen(updatedList);
  };

  return (
    <div className="aufwand-table-wrapper">
      <GeneralFormWrapper
        form={form}
        layout="vertical"
        scrollToFirstError={true}
        noValidate={false}
        className="gesamtsummen-form"
        onBlur={() => updateHandler()}
      >
        {props.renderTable(
          data,
          updateHandler,
          deleteRow,
          addNewRow,
          restoreDefaultRows,
          deleteAllEmptyRows,
          triggerKey,
        )}
      </GeneralFormWrapper>
    </div>
  );
}
