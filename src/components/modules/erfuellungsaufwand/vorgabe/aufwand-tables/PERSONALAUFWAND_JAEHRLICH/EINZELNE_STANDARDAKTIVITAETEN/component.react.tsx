// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
} from '@plateg/rest-api';
import { DeleteOutlined, InfoComponent, PlusOutlined, Restore } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  averageProYear,
  convertFirstCellToHeader,
  dynamicCellRender,
  ErfuellungsaufwandTableItem,
  formCellRender,
  generateStandardActivities,
  generateTableButton,
  getCellTextarea,
  zeitaufwandDifferenz,
} from '../../../controller';
import { ExpensesDrawer, ExpensesDrawerType } from '../../../expenses-drawer/component.react';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';
import { UpdateHandlerType } from '../../PERSONALAUFWAND_EINMALIG/EINZELNE_STANDARDAKTIVITAETEN/component.react';

export function PERSONALAUFWAND_JAEHRLICH_EINZELNE_STANDARDAKTIVITAETEN(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey =
    'egfa.modules.erfuellungsaufwand.vorgabePage.PERSONALAUFWAND_JAEHRLICH_EINZELNE_STANDARDAKTIVITAETEN';
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);
  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    personalaufwandDifferenz: undefined,
    bemerkungen: '',
    zeitaufwandBisher: undefined,

    lohnsatzDifferenz: undefined,
    zeitaufwandDifferenz: undefined,

    berechnungsart: props.berechnungsart,
    bezeichnung: '',
    orderIndex: 0,
    zeitaufwandNeu: undefined,
  };

  const patternForCheckinEmptyFields = {
    bezeichnung: '',
    zeitaufwandDifferenz: undefined,
    zeitaufwandBisher: undefined,
    lohnsatzDifferenz: undefined,
    zeitaufwandNeu: undefined,
    bemerkungen: '',
  };

  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
    index = -2,
    updateKey = '',
  ): ErfuellungsaufwandTableItem[] => {
    const rowList = rowItemsList.map((item) => {
      return {
        ...item,
        personalaufwandDifferenz: averageProYear(item.zeitaufwandDifferenz, item.lohnsatzDifferenz),
      };
    });
    if (updateKey === 'zeitaufwandBisher' || updateKey === 'zeitaufwandNeu') {
      rowList[index - 1].zeitaufwandDifferenz = zeitaufwandDifferenz(
        rowList[index - 1].zeitaufwandNeu,
        rowList[index - 1].zeitaufwandBisher,
      );
    }
    return rowList;
  };

  const staticData = generateStandardActivities(
    props.normadressat,
    patternItem,
    props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft &&
      props.vorgabeType === EgfaErfuellungItemArtType.Informationspflicht
      ? 'informationspflicht'
      : 'vorgabe',
  );
  const bezeichnungIsNotStandard = (bezeichnung: string): boolean => {
    return staticData.find((item) => item.bezeichnung === bezeichnung) ? false : true;
  };

  // helper func to reduce complexity
  const columnRenderHelper = (updateHandler: UpdateHandlerType) => (
    <Column
      title={t(`${translationKey}.tableHeaders.bezeichnung`).toString()}
      render={(_: any, record: AufwandTableDatarow, index) => {
        if (record.key.indexOf('hidden-row-') !== -1) {
          return undefined;
        }

        if (record.key.indexOf('summeRow') === -1) {
          if (bezeichnungIsNotStandard(dynamicDataOnFly[index - 1]?.bezeichnung)) {
            return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bezeichnung'], updateHandler);
          }
          const staticItem = staticData.find((item) => item.bezeichnung === dynamicDataOnFly[index - 1].bezeichnung);
          return (
            <div className="text-cell text">
              <div>
                {dynamicDataOnFly[index - 1].bezeichnung}
                {staticItem?.translationKey === 'wartezeiten' && (
                  <>
                    {' '}
                    <InfoComponent
                      title={t(`egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.title`)}
                      titleWithoutPrefix={true}
                      id={`info-drawer-wartezeiten-5`}
                    >
                      <ExpensesDrawer displayType={ExpensesDrawerType.wegezeiten} />
                    </InfoComponent>
                  </>
                )}
                {staticItem?.zeitwerte && props.showZeitwerte && (
                  <p
                    className="zeitwerte"
                    dangerouslySetInnerHTML={{
                      __html: staticItem.zeitwerte as string,
                    }}
                  />
                )}
              </div>
            </div>
          );
        }
        return <div className="text-cell text">Summe</div>;
      }}
      key="bezeichnung"
      dataIndex="bezeichnung"
      className="first-heading"
    />
  );

  useEffect(() => {
    convertFirstCellToHeader();
  }, []);

  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      staticInitialData={staticData}
      sumAtStart
      renderTable={(
        data: AufwandTableDatarow[],
        updateHandler,
        deleteRow?: (itemRowKey: string) => void,
        addNewRow?: () => void,
        restoreDefaultRows?: () => void,
        deleteAllEmptyRows?: (pattern: { [s: string]: string | undefined }, standardBezeichnungen?: string[]) => void,
      ) => {
        return (
          <>
            <Table
              dataSource={data}
              pagination={false}
              className="aufwand-table"
              rowClassName={(record) => {
                if (record.key.indexOf('summeRow') !== -1) {
                  return 'summe-row';
                }
                return '';
              }}
            >
              {columnRenderHelper(updateHandler)}
              <Column
                title={t(`${translationKey}.tableHeaders.bemerkungen`).toString()}
                dataIndex="bemerkungen"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key.indexOf('summeRow') !== -1) {
                    return <div className="hinweis-cell"></div>;
                  }
                  return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bemerkungen'], updateHandler);
                }}
                key="bemerkungen"
              />
              <ColumnGroup
                key="zeitaufwandProFallGroup"
                title={t(`${translationKey}.tableHeaders.zeitaufwandProFallGroup`).toString()}
                className="grouping"
              >
                <Column
                  title={t(`${translationKey}.tableHeaders.zeitaufwandBisher`).toString()}
                  key="zeitaufwandBisher"
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'zeitaufwandBisher',
                      currencyLabel: 'Min.',
                      updateTableValues: () => updateHandler(index, 'zeitaufwandBisher'),
                      hasDecimal: false,
                      allowNegative: false,
                      oneDigitAfterComma: false,
                    });
                  }}
                  dataIndex="zeitaufwandBisher"
                />
                <Column
                  key="zeitaufwandNeu"
                  title={t(`${translationKey}.tableHeaders.zeitaufwandNeu`).toString()}
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'zeitaufwandNeu',
                      currencyLabel: 'Min.',
                      updateTableValues: () => updateHandler(index, 'zeitaufwandNeu'),
                      hasDecimal: false,
                      allowNegative: false,
                      oneDigitAfterComma: false,
                    });
                  }}
                  dataIndex="zeitaufwandNeu"
                />
                <Column
                  dataIndex="zeitaufwandDifferenz"
                  key="zeitaufwandDifferenz"
                  render={(_: any, record: AufwandTableDatarow, index) => {
                    return formCellRender({
                      recordKey: record.key,
                      index: index - 1,
                      localVals: dynamicDataOnFly,
                      fieldName: 'zeitaufwandDifferenz',
                      currencyLabel: 'Min.',
                      updateTableValues: () => updateHandler(index, 'zeitaufwandDifferenz'),
                      hasDecimal: false,
                      allowNegative: true,
                      oneDigitAfterComma: false,
                    });
                  }}
                  className="selected"
                  title={t(`${translationKey}.tableHeaders.zeitaufwandDifferenz`).toString()}
                />
              </ColumnGroup>
              {props.normadressat !== EgfaErfuellungItemNormadressat.Buerger && (
                <>
                  <Column
                    colSpan={2}
                    title={
                      <div>
                        {t(`${translationKey}.tableHeaders.standardlohnsatzGroup`)}{' '}
                        <InfoComponent
                          key={`info-drawer-personalaufwand-jahrlich-${props.drawerType}`}
                          title={t(
                            `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                          )}
                          titleWithoutPrefix={true}
                          id={`info-drawer-personalaufwand-jahrlich-${props.drawerType}`}
                        >
                          <ExpensesDrawer displayType={props.drawerType} />
                        </InfoComponent>
                      </div>
                    }
                    key="lohnsatzDifferenz"
                    shouldCellUpdate={(record: AufwandTableDatarow) => {
                      return record.key === 'summeRow' || record.key === 'summeRowStart';
                    }}
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return formCellRender({
                        recordKey: record.key,
                        index: index - 1,
                        localVals: dynamicDataOnFly,
                        fieldName: 'lohnsatzDifferenz',
                        currencyLabel: '€/Std',
                        updateTableValues: () => updateHandler(index, 'lohnsatzDifferenz'),
                        hasDecimal: true,
                        allowNegative: false,
                        oneDigitAfterComma: false,
                        averageSum:
                          props.berechnungsart === EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten &&
                          (props.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft ||
                            props.normadressat === EgfaErfuellungItemNormadressat.Verwaltung),
                      });
                    }}
                    dataIndex="lohnsatzDifferenz"
                    className="standardlohnsatz"
                  />
                  <Column
                    dataIndex="lohnsatzInfo"
                    colSpan={0}
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return (
                        <div className="text-cell static-text" style={{ textAlign: 'center' }}>
                          {record.key.indexOf('summeRow') === -1 && (
                            <InfoComponent
                              key={`info-drawer-${index - 1}`}
                              title={t(
                                `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                              )}
                              titleWithoutPrefix={true}
                              id={`info-drawer-personalaufwand-jahrlich-${index - 1}`}
                            >
                              <ExpensesDrawer displayType={props.drawerType} />
                            </InfoComponent>
                          )}
                        </div>
                      );
                    }}
                    key="lohnsatzInfo"
                  />

                  <Column
                    dataIndex="personalaufwandDifferenz"
                    title={t(`${translationKey}.tableHeaders.personalaufwandProFall`).toString()}
                    render={(_: any, record: AufwandTableDatarow, index) => {
                      return dynamicCellRender(
                        record.key,
                        index - 1,
                        dynamicDataOnFly,
                        'personalaufwandDifferenz',
                        '€',
                      );
                    }}
                    key="personalaufwandDifferenz"
                  />
                </>
              )}
              <Column
                key="deleteRow"
                render={(_: any, record: AufwandTableDatarow) => {
                  return (
                    <div className="button-delete-cell">
                      {record.key.indexOf('summeRow') === -1 && (
                        <Button
                          id={`button-remove-row-${record.key}`}
                          onClick={() => deleteRow?.(record.key)}
                          aria-label="Löschen"
                        >
                          <DeleteOutlined />
                        </Button>
                      )}
                    </div>
                  );
                }}
              />
            </Table>
            <div className="buttons-holder" role="list">
              {generateTableButton(
                'button-row-add',
                <PlusOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnAddNewItem`),
                () => addNewRow?.(),
              )}
              {generateTableButton(
                'button-restore-rows-default',
                <Restore />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnRestoreDefaultData`),
                () => restoreDefaultRows?.(),
              )}
              {generateTableButton(
                'button-delete-all-rows-empty',
                <DeleteOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnDeleteEmptyRows`),
                () =>
                  deleteAllEmptyRows?.(
                    patternForCheckinEmptyFields,
                    staticData.map((item) => item.bezeichnung),
                  ),
              )}
            </div>
          </>
        );
      }}
    />
  );
}
