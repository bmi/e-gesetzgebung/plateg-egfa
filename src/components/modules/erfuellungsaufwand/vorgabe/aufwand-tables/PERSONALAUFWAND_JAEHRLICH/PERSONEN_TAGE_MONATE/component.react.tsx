// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Table } from 'antd';
import Column from 'antd/lib/table/Column';
import ColumnGroup from 'antd/lib/table/ColumnGroup';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { EgfaErfuellungItemAufwandEinheitType, EgfaErfuellungItemBerechnungsartType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  convertFirstCellToHeader,
  dynamicCellRender,
  ErfuellungsaufwandTableItem,
  forceDecimalFraction,
  formCellRender,
  getCellSelect,
  getCellTextarea,
  getColumnsAttr,
  getSumme,
  getSummeAsNumber,
  zeitaufwandDifferenz,
} from '../../../controller';
import { ExpensesDrawer } from '../../../expenses-drawer/component.react';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';

const getMultiplier = (einheit: EgfaErfuellungItemAufwandEinheitType | undefined): number => {
  return einheit === EgfaErfuellungItemAufwandEinheitType.PersonenmonatePm ? 134 : 8;
};
export const calculateArbeitszeitInStunden = (
  anzahlDifferenz: number | undefined,
  einheit: EgfaErfuellungItemAufwandEinheitType | undefined,
): number | undefined => {
  if (!anzahlDifferenz) {
    anzahlDifferenz = 0;
  }
  return anzahlDifferenz * getMultiplier(einheit);
};
export function PERSONALAUFWAND_JAEHRLICH_PERSONEN_TAGE_MONATE(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.erfuellungsaufwand.vorgabePage.PERSONEN_TAGE_MONATE_${props.normadressat}`;
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);
  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    bezeichnung: '',
    bemerkungen: '',
    anzahlBisher: undefined,
    anzahlNeu: undefined,
    anzahlDifferenz: undefined,
    berechnungsart: EgfaErfuellungItemBerechnungsartType.PersonenTageMonate,
    einheit: EgfaErfuellungItemAufwandEinheitType.PersonenmonatePm,
    orderIndex: 0,
    vollzugsebene: 'itemA',
  };

  const STATIC_INITIAL_DATA: ErfuellungsaufwandTableItem[] = Object.keys(
    t(`${translationKey}.staticData`, { returnObjects: true }),
  ).map((item, index) => {
    return {
      ...patternItem,
      id: uuidv4(),
      bezeichnung: t(`${translationKey}.staticData.${item}`),
      orderIndex: index,
    };
  });

  /**
   * Used for calculation static data in table according to the given formula in ticket
   * in this case formula is personalaufwandBisher = zeitaufwandBisher * lohnsatzBisher / 60
   * @param rowItemsList
   * @returns
   */
  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
    index = -1,
    updateKey = '',
  ): ErfuellungsaufwandTableItem[] => {
    const rowList = rowItemsList.map((item) => {
      const anzahlDifferenz = item.anzahlDifferenz;
      const personalaufwandProFall = calculatePersonalaufwandProFall(anzahlDifferenz, item.lohnkosten, item.einheit);
      const arbeitszeitInStunden = calculateArbeitszeitInStunden(anzahlDifferenz, item.einheit);
      const gewichteterLohnsatz = (personalaufwandProFall || 0) / (arbeitszeitInStunden || 1);
      return {
        ...item,
        personalaufwandProFall,
        arbeitszeitInStunden,
        gewichteterLohnsatz,
        personalaufwandDifferenz: personalaufwandProFall,
      };
    });
    if (updateKey === 'anzahlNeu' || updateKey === 'anzahlBisher') {
      rowList[index].anzahlDifferenz = zeitaufwandDifferenz(rowList[index].anzahlNeu, rowList[index].anzahlBisher);
    }
    return rowList;
  };

  const calculatePersonalaufwandProFall = (
    anzahlDifferenz: number | undefined,
    lohnkosten: number | undefined,
    einheit: EgfaErfuellungItemAufwandEinheitType | undefined,
  ): number | undefined => {
    if (!anzahlDifferenz) {
      anzahlDifferenz = 0;
    }
    if (!lohnkosten) {
      lohnkosten = 0;
    }
    return anzahlDifferenz * lohnkosten * getMultiplier(einheit);
  };

  const summaryRows: { [s: string]: string } = {
    summeRow: t(`${translationKey}.summaryRows.summeRow`),
    arbeitszeitInStunden: t(`${translationKey}.summaryRows.arbeitszeitInStunden`),
    gewichteterLohnsatz: t(`${translationKey}.summaryRows.gewichteterLohnsatz`),
  };

  const columnWithInput = (
    fieldName: string,
    updateHandler: (index?: number, updateKey?: string) => void,
    isSelected = false,
    isEinmalig = false,
  ): React.ReactElement => {
    const translation = isEinmalig ? `${translationKey}.einmalig` : translationKey;
    return (
      <Column
        {...getColumnsAttr(fieldName, translation)}
        className={isSelected ? 'selected' : ''}
        shouldCellUpdate={(record: AufwandTableDatarow, prevRecord: AufwandTableDatarow) => {
          return (
            (record as unknown as ErfuellungsaufwandTableItem).einheit !==
            (prevRecord as unknown as ErfuellungsaufwandTableItem).einheit
          );
        }}
        render={(_: any, record: AufwandTableDatarow, index) => {
          if (summaryRows[record.key]) {
            return <div className="cell-holder"></div>;
          }
          const label = t(
            `${translationKey}.einheitSelect.label.${
              dynamicDataOnFly[index]?.einheit || EgfaErfuellungItemAufwandEinheitType.PersonenmonatePm
            }`,
          );
          return formCellRender({
            recordKey: record.key,
            index,
            localVals: dynamicDataOnFly,
            fieldName,
            currencyLabel: label,
            updateTableValues: () => updateHandler(index, fieldName),
            hasDecimal: true,
            allowNegative: true,
            oneDigitAfterComma: true,
          });
        }}
      />
    );
  };

  const isSummaryRow = (key: string): boolean => {
    return summaryRows[key] ? true : false;
  };

  // helper var to reduce complexity
  const additionalColumns = (
    <>
      <Column
        {...getColumnsAttr('lohnsatzInfo', translationKey)}
        render={(_: any, record: AufwandTableDatarow, index) => {
          return (
            <div className="text-cell static-text">
              {!isSummaryRow(record.key) && (
                <InfoComponent
                  title={t(
                    `egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.${props.drawerType.toLowerCase()}.title`,
                  )}
                  titleWithoutPrefix={true}
                  id={`lohnsatzInfo-${index}`}
                >
                  <ExpensesDrawer displayType={props.drawerType} />
                </InfoComponent>
              )}
            </div>
          );
        }}
      />
      <Column
        {...getColumnsAttr('personalaufwandProFall', translationKey)}
        fixed="right"
        render={(_: any, record: AufwandTableDatarow, index) => {
          if (record.key === 'arbeitszeitInStunden') {
            return (
              <div className="text-cell static-text">
                {getSumme(dynamicDataOnFly, 'arbeitszeitInStunden', true) || '--'} Std.
              </div>
            );
          }
          if (record.key === 'gewichteterLohnsatz') {
            const gewichteterLohnsatzGeneral =
              getSummeAsNumber(dynamicDataOnFly, 'personalaufwandProFall') /
              (getSummeAsNumber(dynamicDataOnFly, 'arbeitszeitInStunden') || 1);
            return (
              <div className="text-cell static-text">
                {forceDecimalFraction(gewichteterLohnsatzGeneral) || '--'} €/h
              </div>
            );
          }
          return dynamicCellRender(record.key, index, dynamicDataOnFly, 'personalaufwandProFall', '€');
        }}
      />
    </>
  );

  useEffect(() => {
    convertFirstCellToHeader();
  }, []);

  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      staticInitialData={STATIC_INITIAL_DATA}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      customSummaryRows={['arbeitszeitInStunden', 'gewichteterLohnsatz']}
      renderTable={(data: AufwandTableDatarow[], updateHandler) => {
        return (
          <Table
            dataSource={data}
            pagination={false}
            className="aufwand-table"
            rowClassName={(record) => {
              let rowClassName = '';
              if (record.key.indexOf('summeRow') !== -1) {
                rowClassName = 'summe-row';
              }
              if (isSummaryRow(record.key)) {
                rowClassName += ' custom-summe-row';
              }
              return rowClassName;
            }}
          >
            <Column
              {...getColumnsAttr('bezeichnung', translationKey)}
              className="first-heading"
              render={(_: any, record: AufwandTableDatarow, index) => {
                return (
                  <div className="text-cell text">
                    {summaryRows[record.key] || dynamicDataOnFly[index]?.bezeichnung}
                  </div>
                );
              }}
            />
            <Column
              {...getColumnsAttr('bemerkungen', translationKey)}
              render={(_: any, record: AufwandTableDatarow, index) => {
                if (isSummaryRow(record.key)) {
                  return <div className="hinweis-cell"></div>;
                }
                return getCellTextarea(['aufwandstabellenZeilen', index, 'bemerkungen'], updateHandler);
              }}
            />
            <Column
              {...getColumnsAttr('einheit', translationKey)}
              render={(_: any, record: AufwandTableDatarow, index) => {
                if (isSummaryRow(record.key)) {
                  return <div className="hinweis-cell"></div>;
                }
                return getCellSelect(
                  'einheit',
                  index,
                  updateHandler,
                  `${translationKey}.einheitSelect`,
                  Object.values(EgfaErfuellungItemAufwandEinheitType),
                );
              }}
            />
            {!props.isEinmalig && (
              <ColumnGroup
                title={t(`${translationKey}.tableHeaders.anzahlGroup`).toString()}
                key="anzahlGroup"
                className="grouping"
              >
                {columnWithInput('anzahlBisher', updateHandler)}
                {columnWithInput('anzahlNeu', updateHandler)}
                {columnWithInput('anzahlDifferenz', updateHandler, true)}
              </ColumnGroup>
            )}

            {props.isEinmalig && columnWithInput('anzahlDifferenz', updateHandler, true, true)}
            <Column
              {...getColumnsAttr('vollzugsebene', translationKey)}
              render={(_: any, record: AufwandTableDatarow, index) => {
                if (summaryRows[record.key]) {
                  return <div className="cell-holder"></div>;
                }
                return getCellSelect(
                  'vollzugsebene',
                  index,
                  updateHandler,
                  `${translationKey}.vollzugsebeneSelect`,
                  Object.keys(t(`${translationKey}.vollzugsebeneSelect`, { returnObjects: true })),
                );
              }}
            />
            <Column
              {...getColumnsAttr('lohnkosten', translationKey)}
              shouldCellUpdate={() => false}
              render={(_: any, record: AufwandTableDatarow, index) => {
                if (isSummaryRow(record.key)) {
                  return <div className="cell-holder"></div>;
                }
                return formCellRender({
                  recordKey: record.key,
                  index,
                  localVals: dynamicDataOnFly,
                  fieldName: 'lohnkosten',
                  currencyLabel: '€/Std.',
                  updateTableValues: updateHandler,
                  hasDecimal: true,
                  allowNegative: false,
                  oneDigitAfterComma: false,
                });
              }}
            />
            {additionalColumns}
          </Table>
        );
      }}
    />
  );
}
