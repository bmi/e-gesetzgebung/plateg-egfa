// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { SACHAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN } from './EINZELNE_STANDARDAKTIVITAETEN/component.react';
export const SACHAUFWAND_EINMALIG = {
  EINZELNE_STANDARDAKTIVITAETEN: SACHAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN,
};
