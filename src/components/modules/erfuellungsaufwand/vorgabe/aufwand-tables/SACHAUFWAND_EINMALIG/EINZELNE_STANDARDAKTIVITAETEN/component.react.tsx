// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Table } from 'antd';
import Column from 'antd/lib/table/Column';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as uuidv4 } from 'uuid';

import { EgfaErfuellungItemBerechnungsartType } from '@plateg/rest-api';
import { DeleteOutlined, PlusOutlined, Restore } from '@plateg/theme';

import { AufwandTableDatarow, AufwandTableProps } from '../..';
import {
  convertFirstCellToHeader,
  ErfuellungsaufwandTableItem,
  formCellRender,
  getCellTextarea,
} from '../../../controller';
import { AufwandTableWrapperComponent } from '../../aufwand-table-wrapper/component.react';

export function SACHAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN(props: AufwandTableProps): React.ReactElement {
  const { t } = useTranslation();
  const translationKey =
    'egfa.modules.erfuellungsaufwand.vorgabePage.SACHAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN';
  const [dynamicDataOnFly, setDynamicDataOnFly] = useState<ErfuellungsaufwandTableItem[]>([]);
  const patternItem: ErfuellungsaufwandTableItem = {
    id: uuidv4(),
    summe: false,
    bezeichnung: '',
    bemerkungen: '',
    sachaufwandProFall: undefined,
    berechnungsart: EgfaErfuellungItemBerechnungsartType.GesamtaufwandProFall,
    orderIndex: 0,
  };
  const checkinEmptyFields = {
    bezeichnung: '',
    bemerkungen: '',
    sachaufwandProFall: undefined,
  };

  const recalculateDynamicRowDataOnFly = (
    rowItemsList: ErfuellungsaufwandTableItem[],
  ): ErfuellungsaufwandTableItem[] => {
    return rowItemsList;
  };

  const intialDataKeys = [
    'aufwandbeschaffung',
    'anschaffungKosten',
    'aufwandAnlagen',
    'wartungsAufwand',
    'aufwandInanspruchnahme',
    'fortbildungen',
    'sachmittel',
    'wegesachkosten',
    'sonstigeSachkosten',
  ];
  const staticData = intialDataKeys.map((key) => ({
    ...patternItem,
    bezeichnung: t(`${translationKey}.zeileKategorien.${key}`),
    id: uuidv4(),
  }));
  const bezeichnungIsNotStandard = (item: string): boolean => {
    return intialDataKeys.find((key) => t(`${translationKey}.zeileKategorien.${key}`) === item) ? false : true;
  };

  useEffect(() => {
    convertFirstCellToHeader();
  }, []);

  return (
    <AufwandTableWrapperComponent
      aufwandstabellenZeilen={props.aufwandstabellenZeilen}
      setDynamicDataOnFly={setDynamicDataOnFly}
      recalculateDynamicRowDataOnFly={recalculateDynamicRowDataOnFly}
      patternItem={patternItem}
      updateAufwandsTableZeilen={props.updateAufwandsTableZeilen}
      staticInitialData={staticData}
      sumAtStart
      renderTable={(
        data: AufwandTableDatarow[],
        updateHandler,
        deleteRow?: (itemRowKey: string) => void,
        addNewRow?: () => void,
        restoreDefaultRows?: () => void,
        deleteAllEmptyRows?: (pattern: { [s: string]: string | undefined }, bezeichnungen?: string[]) => void,
      ) => {
        const generateButton = (id: string, icon: React.ReactElement, content: string, onClick?: () => void) => {
          return (
            <Button id={id} size="small" icon={icon} type="link" onClick={onClick} className="blue-text-button">
              {content}
            </Button>
          );
        };
        return (
          <>
            <Table
              dataSource={data}
              pagination={false}
              className="aufwand-table"
              rowClassName={(record) => {
                if (record.key.indexOf('summeRow') !== -1) {
                  return 'summe-row';
                }
                return '';
              }}
              style={{ maxWidth: 800 }}
            >
              <Column
                title={t(`${translationKey}.tableHeaders.bezeichnung`).toString()}
                dataIndex="bezeichnung"
                key="bezeichnung"
                className="first-heading"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key.indexOf('hidden-row-') !== -1) {
                    return undefined;
                  }
                  if (
                    bezeichnungIsNotStandard(dynamicDataOnFly[index - 1]?.bezeichnung) &&
                    record.key !== 'summeRow' &&
                    record.key !== 'summeRowStart'
                  ) {
                    return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bezeichnung'], updateHandler);
                  }
                  return (
                    <div className="text-cell text">
                      {record.key !== 'summeRow' && record.key !== 'summeRowStart'
                        ? dynamicDataOnFly[index - 1].bezeichnung
                        : 'Summe'}
                    </div>
                  );
                }}
              />
              <Column
                title={t(`${translationKey}.tableHeaders.bemerkungen`).toString()}
                dataIndex="bemerkungen"
                key="bemerkungen"
                render={(_: any, record: AufwandTableDatarow, index) => {
                  if (record.key === 'summeRow' || record.key === 'summeRowStart') {
                    return <div className="hinweis-cell"></div>;
                  }
                  return getCellTextarea(['aufwandstabellenZeilen', index - 1, 'bemerkungen'], updateHandler);
                }}
              />

              <Column
                title={t(`${translationKey}.tableHeaders.sachAufwandProFall`).toString()}
                dataIndex="sachaufwandProFall"
                key="sachaufwandProFall"
                className="selected sachaufwand-jahrlich"
                shouldCellUpdate={(record: AufwandTableDatarow) => {
                  return record.key === 'summeRow' || record.key === 'summeRowStart';
                }}
                render={(_: any, record: AufwandTableDatarow, index) => {
                  return formCellRender({
                    recordKey: record.key,
                    index: index - 1,
                    localVals: dynamicDataOnFly,
                    fieldName: 'sachaufwandProFall',
                    currencyLabel: '€',
                    updateTableValues: updateHandler,
                    hasDecimal: true,
                    allowNegative: true,
                    oneDigitAfterComma: false,
                  });
                }}
              />
              <Column
                title=""
                dataIndex=""
                key=""
                render={(_: any, record: AufwandTableDatarow) => {
                  return (
                    <div className="button-delete-cell">
                      {record.key !== 'summeRow' && record.key !== 'summeRowStart' && (
                        <Button
                          id={`button-remove-row-${record.key}`}
                          onClick={() => deleteRow?.(record.key)}
                          aria-label="Löschen"
                        >
                          <DeleteOutlined />
                        </Button>
                      )}
                    </div>
                  );
                }}
              />
            </Table>
            <div className="buttons-holder">
              {generateButton(
                'button-add-row',
                <PlusOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnAddNewItemMaterial`),
                () => addNewRow?.(),
              )}
              {generateButton(
                'button-restore-default-rows',
                <Restore />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnRestoreDefaultDataMaterial`),
                () => restoreDefaultRows?.(),
              )}
              {generateButton(
                'button-delete-all-empty-rows',
                <DeleteOutlined />,
                t(`egfa.modules.erfuellungsaufwand.vorgabePage.buttons.btnDeleteEmptyRows`),
                () =>
                  deleteAllEmptyRows?.(
                    checkinEmptyFields,
                    staticData.map((item) => item.bezeichnung),
                  ),
              )}
            </div>
          </>
        );
      }}
    />
  );
}
