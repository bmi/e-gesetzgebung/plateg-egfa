// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form, InputNumber, Select, TreeSelect } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { ColumnsType } from 'antd/lib/table';
import i18n, { t } from 'i18next';
import React, { ReactElement } from 'react';
import { v4 as uuidv4 } from 'uuid';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
} from '@plateg/rest-api';
import { SelectWrapper, TableComponentProps } from '@plateg/theme';

import { TreeSelectItem } from '../gesamtsummenNormadressaten/section-expert-table/component.react';

export interface DecimalInfo {
  firstRender: boolean;
  lastRender: boolean;
  oneDigitAfterComma: boolean;
}

export interface ErfuellungsaufwandTableItem
  extends EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO {
  [s: string]: number | string | boolean | undefined;
}
export const currencyParser = (
  val: string | undefined,
  hasDecimal = false,
  allowNegative = false,
  decimalInfo: DecimalInfo = { firstRender: true, lastRender: false, oneDigitAfterComma: false },
): string | number => {
  // for when the input gets clears
  if (typeof val === 'string' && !val.length) {
    val = '';
  }

  !val && (val = '');

  // detecting and parsing between comma and dot
  const group = new Intl.NumberFormat('de-DE').format(1111).replace(/1/g, '');
  let reversedVal: string | number = val.replace(new RegExp('\\' + group, 'g'), '');
  if (hasDecimal) {
    const decimal = new Intl.NumberFormat('de-DE').format(1.1).replace(/1/g, '');
    reversedVal = reversedVal.replace(new RegExp('\\' + decimal, 'g'), '.');
  }

  // removing everything except the digits , dote and - if the number was negative
  const negativeMark = allowNegative && reversedVal.startsWith('-') ? '-' : '';
  reversedVal = negativeMark + reversedVal.replace(/[^0-9.]/g, '');

  // appending digits properly
  const digitsAfterDecimalCount = (reversedVal.split('.')[1] || []).length;
  const commaIndicator = decimalInfo.oneDigitAfterComma ? 1 : 2;
  const needsDigitsAppended = digitsAfterDecimalCount > commaIndicator;

  if (needsDigitsAppended) {
    reversedVal = (reversedVal as any) * Math.pow(10, digitsAfterDecimalCount - commaIndicator);
  }

  if (decimalInfo.firstRender) {
    decimalInfo.firstRender = false;
  }

  return Number.isNaN(reversedVal) ? '' : reversedVal;
};
export const convertFirstCellToHeader = () => {
  const table = document.querySelector('.aufwand-table tbody');
  if (table) {
    const observer = new MutationObserver(() => {
      replaceTdWithTh();
    });
    observer.observe(table, { childList: true, subtree: true });
    return () => {
      observer.disconnect();
    };
  }
};
const replaceTdWithTh = () => {
  document.querySelectorAll('td.ant-table-cell.first-heading').forEach((cell) => {
    const th = document.createElement('th');
    const cellChildren = Array.from(cell.children);
    th.className = cell.className;
    for (const element of cellChildren) {
      th.append(element);
    }

    cell.replaceWith(th);
  });
};
export const currencyFormatter = (
  value: string | number | undefined,
  hasDecimal = false,
  decimalInfo: DecimalInfo = { firstRender: true, lastRender: false, oneDigitAfterComma: false },
) => {
  if (typeof value === 'string' && !value.length) {
    return '';
  }

  let result = new Intl.NumberFormat('de-DE').format(value as number);

  if (hasDecimal && (decimalInfo.lastRender || decimalInfo.firstRender)) {
    result = completeWithZeros(result, decimalInfo.oneDigitAfterComma);
  }

  return result;
};
const completeWithZeros = (val: string, oneDigitAfterComma: boolean): string => {
  if (!oneDigitAfterComma) {
    if (!val.includes(',')) {
      return val + ',00';
    } else {
      if (val.split(',')[1].length === 1) {
        return val + '0';
      }
      return val;
    }
  } else {
    if (!val.includes(',')) {
      return val + ',0';
    } else {
      if (val.split(',')[1].length === 0) {
        return val + '0';
      }
      return val;
    }
  }
};
export interface FallzahlTableItemInterface {
  id: string;
  veraenderungAnzahl?: number;
  veraenderungHaeufigkeit?: number;
  veraenderungFallzahl?: number;
  veraenderungHinweise?: string;
}
const handleFallzahlTableCondition = (
  record: FallzahlTableItemInterface,
  prevRecord: FallzahlTableItemInterface,
  updateKey: keyof FallzahlTableItemInterface,
) => {
  const getValue = (record: FallzahlTableItemInterface) => record?.[updateKey];

  return getValue(record) !== getValue(prevRecord);
};
export const getFallzahlTableVals = (
  content: FallzahlTableItemInterface[],
  updateMethod: () => void,
  gesamtsummeType: EgfaErfuellungItemAufwandGesamtsummeType,
): TableComponentProps<FallzahlTableItemInterface> => {
  const veraenderungAnzahlColumn = {
    title: i18n.t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.fallzahlTable.veraenderungAnzahl`),
    key: 'veraenderungAnzahl',
    shouldCellUpdate: () => false,
    render: (): ReactElement => {
      return <>{getCellInputNumber(['veraenderungAnzahl'], null, updateMethod, false, true)}</>;
    },
  };
  const veraenderungHaeufigkeitColumn = {
    title: i18n.t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.fallzahlTable.veraenderungHaeufigkeit`),
    key: 'veraenderungHaeufigkeit',
    shouldCellUpdate: () => false,
    render: (): ReactElement => {
      return <>{getCellInputNumber(['veraenderungHaeufigkeit'], null, updateMethod, true, true)}</>;
    },
  };
  const veraenderungFallzahlColumn = {
    title: i18n.t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.fallzahlTable.veraenderungFallzahl`),
    key: 'veraenderungFallzahl',
    className: 'veraenderungFallzahl',
    shouldCellUpdate: (record: FallzahlTableItemInterface, prevRecord: FallzahlTableItemInterface) => {
      const isVeraenderungAnzahlChanged = handleFallzahlTableCondition(record, prevRecord, 'veraenderungAnzahl');

      const isVeraenderungHaeufigkeitChanged = handleFallzahlTableCondition(
        record,
        prevRecord,
        'veraenderungHaeufigkeit',
      );

      return isVeraenderungAnzahlChanged || isVeraenderungHaeufigkeitChanged;
    },
    render: (): ReactElement => {
      return <>{getCellInputNumber(['veraenderungFallzahl'], null, () => {}, true, true)}</>;
    },
  };
  const veraenderungHinweiseColumn = {
    title: i18n.t(`egfa.modules.erfuellungsaufwand.vorgabePage.tablesSection.fallzahlTable.veraenderungHinweise`),
    key: 'veraenderungHinweise',
    render: (): ReactElement => {
      return (
        <div className="hinweis-cell">
          <Form.Item name={['veraenderungHinweise']}>
            <TextArea autoSize autoComplete="off" placeholder="" />
          </Form.Item>
        </div>
      );
    },
  };

  let columns: ColumnsType<FallzahlTableItemInterface>;
  if (
    gesamtsummeType === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig ||
    gesamtsummeType === EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig
  ) {
    columns = [veraenderungFallzahlColumn, veraenderungHinweiseColumn];
  } else {
    columns = [
      veraenderungAnzahlColumn,
      veraenderungHaeufigkeitColumn,
      veraenderungFallzahlColumn,
      veraenderungHinweiseColumn,
    ];
  }
  return {
    expandable: false,
    columns,
    content,
    id: '',
  };
};

export const getCellInputNumber = (
  inputFormName: (string | number)[],
  label: string | null,
  onUpdate: () => void,
  hasDecimal = false,
  allowNegative = false,
  oneDigitAfterComma = false,
  checkTrigger = false,
): React.ReactElement => {
  const decimalInfo: DecimalInfo = {
    firstRender: true,
    lastRender: checkTrigger,
    oneDigitAfterComma,
  };
  return (
    <div className="cell-holder">
      <Form.Item name={inputFormName}>
        <InputNumber
          onChange={onUpdate}
          defaultValue=""
          parser={(displayValue: string | undefined) =>
            currencyParser(displayValue, hasDecimal, allowNegative, decimalInfo)
          }
          formatter={(val) => currencyFormatter(val, hasDecimal, decimalInfo)}
          controls={false}
          onBlur={() => {
            decimalInfo.lastRender = true;
          }}
          onKeyDown={() => {
            decimalInfo.firstRender = false;
            decimalInfo.lastRender = false;
          }}
        />
      </Form.Item>

      {label && <div className="currency-label">{label}</div>}
    </div>
  );
};

export const getCellTextarea = (
  inputFormName: (string | number)[],
  onUpdate: () => void,
  isRequired = false,
): React.ReactElement => {
  return (
    <div className="hinweis-cell">
      <Form.Item
        name={inputFormName}
        rules={[{ required: isRequired, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
      >
        <TextArea autoSize autoComplete="off" placeholder="" onChange={onUpdate} />
      </Form.Item>
    </div>
  );
};

export const getCellSelect = (
  fieldName: string,
  fieldIndex: number,
  updateHandler: () => void,
  translationKey: string,
  optionsList: string[],
): React.ReactElement => {
  return (
    <div className="hinweis-cell">
      <Form.Item name={['aufwandstabellenZeilen', fieldIndex, fieldName]}>
        <SelectWrapper
          id={`aufwandstabellenZeilen_${fieldName}_${fieldIndex}`}
          onChange={() => updateHandler()}
          dropdownClassName="dropdown-menu aufwand-table-dropdown"
        >
          {optionsList.map((element) => {
            return (
              <Select.Option
                key={`aufwandstabellenZeilen_${fieldName}_${fieldIndex}-${element}`}
                value={element}
                aria-label={element}
              >
                {i18n.t(`${translationKey}.${element}`).toString()}
              </Select.Option>
            );
          })}
        </SelectWrapper>
      </Form.Item>
    </div>
  );
};

export const getTreeSelect = (
  inputFormName: (string | number)[],
  updateHandler: () => void,
  treeSelectData: any[],
): React.ReactElement => {
  return (
    <div className="hinweis-cell">
      <Form.Item name={inputFormName}>
        <TreeSelect
          // dropdownClassName is deprecated, but popupClassName is not recognized in cockpit
          dropdownClassName="custom-select-dropdown dropdownClassName"
          treeData={treeSelectData}
          onChange={updateHandler}
          placeholder={i18n
            .t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item3.placeholder')
            .toString()}
          multiple
        />
      </Form.Item>
    </div>
  );
};

export const prepareFilteredTreeData = (
  treeData: TreeSelectItem[],
  sectionKey: EgfaErfuellungItemNormadressat,
  recordId: string,
): TreeSelectItem[] => {
  const selectedCategory = treeData.find((category) => category.value.indexOf(sectionKey) != -1);
  const reducedChildren = selectedCategory?.children.filter((kid) => kid.value !== recordId) || [];
  return treeData.map((category) => {
    if (category.value.indexOf(sectionKey) == -1) {
      return category;
    }
    const preparedCategory = { ...category };
    preparedCategory.children = reducedChildren;
    return preparedCategory;
  });
};

export const getColumnsAttr = (fieldName: string, translationKey: string): { [s: string]: string } => {
  const title = i18n.t(`${translationKey}.tableHeaders.${fieldName}`, { defaultValue: '' });
  return { title, dataIndex: fieldName, key: fieldName };
};

export const averageProYear = (param1: number | undefined, param2: number | undefined): number | undefined => {
  if (!param1 || !param2) {
    return undefined;
  }
  return Math.round(((param1 * param2) / 60 + Number.EPSILON) * 100) / 100;
};
export const zeitaufwandDifferenz = (
  absolutNeu: number | undefined,
  bisher: number | undefined,
): number | undefined => {
  if (!absolutNeu) {
    absolutNeu = 0;
  }
  if (!bisher) {
    bisher = 0;
  }
  return absolutNeu - bisher;
};
/**
 * Calculate data for summe rows
 * go through all elements and calculate summe of all values from fieldName
 * @param list
 * @param fieldName
 * @returns
 */
export const getSumme = (
  list: ErfuellungsaufwandTableItem[],
  fieldName: string,
  hasDecimal = false,
  averageSum = false,
): string => {
  let result = getSummeAsNumber(list, fieldName);
  const toDivideOn = list.filter((item) => item[fieldName]).length;
  if (averageSum && toDivideOn) {
    result = result / toDivideOn;
  }

  if (hasDecimal) {
    return forceDecimalFraction(result);
  }
  return new Intl.NumberFormat('de-DE').format(result);
};

export const getSummeAsNumber = (list: ErfuellungsaufwandTableItem[], fieldName: string): number => {
  return list
    .map((item) => item[fieldName] as number)
    .reduce((a, b) => {
      if (!b) {
        return a;
      }
      return a + b;
    }, 0);
};

type FormCellRenderArguments = {
  recordKey: string;
  index: number;
  localVals: ErfuellungsaufwandTableItem[];
  fieldName: string;
  currencyLabel: string;
  updateTableValues: () => void;
  hasDecimal?: boolean;
  allowNegative?: boolean;
  oneDigitAfterComma?: boolean;
  triggerKey?: string;
  averageSum?: boolean;
};

export const formCellRender = (argument: FormCellRenderArguments) => {
  const {
    recordKey,
    index,
    localVals,
    fieldName,
    currencyLabel,
    updateTableValues,
    hasDecimal,
    allowNegative,
    oneDigitAfterComma,
    triggerKey,
    averageSum,
  } = argument;
  // Render content for summe row
  if (recordKey === 'summeRow' || recordKey === 'summeRowStart') {
    return (
      <div className="text-cell static-text">
        {getSumme(localVals, fieldName, hasDecimal, averageSum) || '--'}
        {currencyLabel}
      </div>
    );
  }

  // Render content for regular table row
  return getCellInputNumber(
    ['aufwandstabellenZeilen', index, fieldName],
    currencyLabel,
    updateTableValues,
    hasDecimal,
    allowNegative,
    oneDigitAfterComma,
    triggerKey ? triggerKey !== fieldName : false,
  );
};

/**
 * Render cell with dynamic content updated on the fly
 * @param recordKey
 * @param index
 * @param localVals
 * @param fieldName
 * @param hasDecimal
 * @returns React.ReactElement
 */
export const dynamicCellRender = (
  recordKey: string,
  index: number,
  localVals: ErfuellungsaufwandTableItem[],
  fieldName: string,
  currencyLabel: string,
) => {
  // Render content for summe row
  if (recordKey === 'summeRow' || recordKey === 'summeRowStart') {
    return (
      <div className="text-cell static-text">
        {getSumme(localVals, fieldName, true) || '--'}
        {currencyLabel}
      </div>
    );
  }
  // Render content for regular table row
  const formatedData =
    localVals[index]?.[fieldName] !== undefined
      ? forceDecimalFraction(localVals[index]?.[fieldName] as number)
      : '0,00';
  return (
    <div className="text-cell static-text">
      {formatedData}
      {currencyLabel}
    </div>
  );
};
export const forceDecimalFraction = (value: number, digits = 2): string => {
  return value
    .toFixed(digits)
    .replace(/\B(?=(\d{3})+(?!\d))/g, 's')
    .replace('.', ',')
    .replace(/s/g, '.');
};

export const generateTableButton = (id: string, icon: React.ReactElement, content: string, onClick?: () => void) => {
  return (
    <div role={'listitem'}>
      <Button
        id={id}
        size="small"
        icon={icon}
        type="link"
        onClick={onClick}
        className="blue-text-button"
        aria-label={content}
      >
        {content}
      </Button>
    </div>
  );
};

export const generateStandardActivities = (
  normadressat: EgfaErfuellungItemNormadressat,
  patternItem: ErfuellungsaufwandTableItem,
  sectionType: 'einmalig' | 'informationspflicht' | 'vorgabe' = 'vorgabe',
): ErfuellungsaufwandTableItem[] => {
  return Object.keys(
    t(`egfa.modules.erfuellungsaufwand.vorgabePage.${normadressat.toLowerCase()}StandardActivities.${sectionType}`, {
      returnObjects: true,
    }),
  ).map(
    (item) =>
      ({
        ...patternItem,
        bezeichnung: t(
          `egfa.modules.erfuellungsaufwand.vorgabePage.${normadressat.toLowerCase()}StandardActivities.${sectionType}.${item}.bezeichnung`,
        ),
        id: uuidv4(),
        zeitwerte: t(
          `egfa.modules.erfuellungsaufwand.vorgabePage.${normadressat.toLowerCase()}StandardActivities.${sectionType}.${item}.zeitwerte`,
          {
            defaultValue: '',
          },
        ),
        translationKey: item,
      }) as ErfuellungsaufwandTableItem,
  );
};

export const sumZeilen = (
  zeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO[],
  sectionName: EgfaErfuellungItemAufwandGesamtsummeType,
  typ: 'Buerger' | '' = '',
): { sum: number; label: string } => {
  let sumKey:
    | 'personalaufwandDifferenz'
    | 'personalaufwandProFall'
    | 'sachaufwandDifferenz'
    | 'zeitaufwandDifferenz'
    | 'sachaufwandProFall' = 'personalaufwandDifferenz';
  let label = '€';
  switch (sectionName) {
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich:
      if (typ === 'Buerger') {
        sumKey = 'zeitaufwandDifferenz';
        label = 'Std.';
      }
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig:
      if (typ === 'Buerger') {
        sumKey = 'zeitaufwandDifferenz';
        label = 'Std.';
      } else {
        sumKey = 'personalaufwandProFall';
      }
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich:
      sumKey = 'sachaufwandDifferenz';
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig:
      sumKey = 'sachaufwandProFall';
      break;
    default:
      sumKey = 'personalaufwandDifferenz';
      break;
  }
  const aufwandstabelle = zeilen.find((item) => item.gesamtsummeType === sectionName);
  if (aufwandstabelle) {
    const sum =
      aufwandstabelle?.aufwandstabellenZeilen
        .filter((item) => item.berechnungsart === aufwandstabelle.berechnungsart && !item.summe) //item.summer is reserved for Expert mode
        .reduce((a: number, b: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO) => {
          return a + (b[sumKey] || 0);
        }, 0) * (aufwandstabelle.veraenderungFallzahl || 0);
    if (sum) {
      return label === 'Std.' ? { sum: sum / 60, label } : { sum, label };
    }
  }
  return { sum: 0, label };
};

export const sumZeilenExpert = (
  zeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO[],
  sectionName: EgfaErfuellungItemAufwandGesamtsummeType,
  typ: 'Buerger' | '' = '',
): { sum: number; label: string } => {
  const sumKey = 'personalaufwandSumme';
  let label = '€';
  switch (sectionName) {
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich:
      if (typ === 'Buerger') {
        label = 'Std.';
      }
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig:
      label = typ === 'Buerger' ? 'Std.' : '€';
      break;
    default:
      break;
  }
  const aufwandstabelle = zeilen.find((item) => item.gesamtsummeType === sectionName);
  if (aufwandstabelle) {
    const sum = aufwandstabelle?.aufwandstabellenZeilen
      .filter((item) => item.summe) //item.summe is reserved for Expert mode
      .reduce((a: number, b: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO) => {
        return a + (b[sumKey] || 0);
      }, 0);
    if (sum) {
      return { sum, label };
    }
  }
  return { sum: 0, label };
};
export const sumZeilenFormatter = (val: { sum: number; label: string }, zeroVal?: string) => {
  if (val.sum) {
    return `${forceDecimalFraction(val.sum, 0)} ${val.label}`;
  }
  return `${zeroVal ?? '--'} ${val.label}`;
};
