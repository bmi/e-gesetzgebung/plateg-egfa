// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ExpensesData } from '../reducers';
export enum VorgabeActionType {
  setExpensesData = 'SET_EXPENSES_DATA',
}
export type ExpensesAction = {
  type: VorgabeActionType;
  payload: ExpensesData;
};

export type VorgabeActions = ExpensesAction; // we can add many action types here

export const setExpensesAction = (items: ExpensesData): ExpensesAction => {
  return {
    type: VorgabeActionType.setExpensesData,
    payload: items,
  };
};
