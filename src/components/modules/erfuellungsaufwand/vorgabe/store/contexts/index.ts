// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { VorgabeActions } from '../actions';
import { InitialVorgabeState, VorgabeState } from '../reducers';

const storeContext = {
  state: InitialVorgabeState,
  dispatch: () => null,
};

export const StoreContext = React.createContext<{ state: VorgabeState; dispatch: React.Dispatch<VorgabeActions> }>(
  storeContext,
);

export const useStore = (): { state: VorgabeState; dispatch: React.Dispatch<VorgabeActions> } =>
  React.useContext(StoreContext);
