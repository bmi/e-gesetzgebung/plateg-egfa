// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { ExpensesReducer, InitialVorgabeState } from './reducers';
import { StoreContext } from './contexts';
type Props = {
  children: React.ReactNode;
};

export const StoreProvider = (props: Props) => {
  const [state, dispatch] = React.useReducer(ExpensesReducer, InitialVorgabeState);

  return <StoreContext.Provider value={{ state, dispatch }}>{props.children}</StoreContext.Provider>;
};
