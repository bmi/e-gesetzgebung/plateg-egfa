// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ExpenseType } from '../../expenses-drawer/component.react';
import { ExpensesAction, VorgabeActionType } from '../actions';

export type ExpensesData = {
  wirtschaftData: ExpenseType[];
  verwaltungData: ExpenseType[];
  wegezeitenData: ExpenseType[];
};
export type VorgabeState = {
  expensesData: ExpensesData;
};

export const InitialVorgabeState: VorgabeState = {
  expensesData: {
    wirtschaftData: [],
    verwaltungData: [],
    wegezeitenData: [],
  },
};

export const ExpensesReducer = (state: VorgabeState, action: ExpensesAction): VorgabeState => {
  if (action.type === VorgabeActionType.setExpensesData) return { ...state, expensesData: action.payload };
  return state;
};
