// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './fallzahl-table.less';

import { FormInstance, Table } from 'antd';
import React, { useEffect, useState } from 'react';

import {
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
} from '@plateg/rest-api';
import { TableComponentProps } from '@plateg/theme';

import { FallzahlTableItemInterface, getFallzahlTableVals } from '../controller';

interface FallzahlTableProps {
  aufwandsTabelle: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO;
  form: FormInstance;
  normadressat: EgfaErfuellungItemNormadressat;
  berechnungsart: EgfaErfuellungItemBerechnungsartType;
}

export function FallzahlTable(props: FallzahlTableProps): React.ReactElement {
  const [fallzahlTableVals, setFallzahlTableVals] = useState<TableComponentProps<FallzahlTableItemInterface>>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
    id: '',
  });

  // Prepare data for 1.Fallzahl pro Jahr berechnen table
  useEffect(() => {
    const fallzahlProJahrTableItem = {
      id: '1',
      key: '1',
      veraenderungAnzahl: props.aufwandsTabelle.veraenderungAnzahl,
      veraenderungHaeufigkeit: props.aufwandsTabelle.veraenderungHaeufigkeit,
      veraenderungFallzahl: props.aufwandsTabelle.veraenderungFallzahl,
      veraenderungHinweise: props.aufwandsTabelle.veraenderungHinweise,
    };
    const arr = [];
    arr.push(fallzahlProJahrTableItem);
    setFallzahlTableVals(getFallzahlTableVals(arr, updateVeraenderungFallzahl, props.aufwandsTabelle.gesamtsummeType));
  }, [props.aufwandsTabelle, props.normadressat, props.berechnungsart]);

  const updateVeraenderungFallzahl = () => {
    const veraenderungHaeufigkeit = props.form.getFieldValue('veraenderungHaeufigkeit') as string;
    const veraenderungAnzahl = props.form.getFieldValue('veraenderungAnzahl') as string;
    if (veraenderungHaeufigkeit && veraenderungAnzahl) {
      props.form.setFieldsValue({
        veraenderungFallzahl:
          Math.round((Number(veraenderungAnzahl) * Number(veraenderungHaeufigkeit) + Number.EPSILON) * 100) / 100,
      });
    }
  };

  return (
    <div className="fallzahl-table-holder">
      <Table
        columns={fallzahlTableVals.columns}
        dataSource={fallzahlTableVals.content}
        pagination={false}
        className="fallzahl-table"
      />
    </div>
  );
}
