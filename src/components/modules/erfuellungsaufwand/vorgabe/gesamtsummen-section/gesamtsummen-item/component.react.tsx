// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';

import { sumZeilenExpert, sumZeilenFormatter } from '../../controller';

interface GesamtsummenSectionComponentProps {
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  setSelectedSection: (sectionName: EgfaErfuellungItemAufwandGesamtsummeType | null) => void;
  sectionName: EgfaErfuellungItemAufwandGesamtsummeType;
  selectedSection: string | null;
  applyChanges: () => void;
}
export function GesamtsummenItemComponent(props: GesamtsummenSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const typBuerger =
    props.vorgabe.normadressat === EgfaErfuellungItemNormadressat.Buerger &&
    (props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich ||
      props.sectionName === EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig)
      ? 'Buerger'
      : '';
  const [isSectionSelected, setIsSectionSelected] = useState(false);
  useEffect(() => {
    setIsSectionSelected(props.selectedSection === props.sectionName);
  }, [props.selectedSection, props.sectionName]);

  const [amount, setAmount] = useState<string | number>('--');
  useEffect(() => {
    const amountSum = sumZeilenFormatter(
      sumZeilenExpert(props.vorgabe.aufwandstabellen, props.sectionName, typBuerger),
      '--',
    );
    setAmount(amountSum);
  }, [props.vorgabe]);

  return (
    <>
      <div className={`summen-holder-col ${isSectionSelected ? 'selected' : ''}`} role="tab">
        <div className="summe-item" role="tabpanel">
          <strong>
            {t(`egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.${props.sectionName}${typBuerger}`)}
          </strong>

          <div>
            <span className="amount">{amount}</span>
            <span className="amount-line"></span>
            {!isSectionSelected && (
              <Button
                id={`egfa-erfuellungsaufwand-vorgabe-gesamtsummen-editBtn-${props.sectionName}`}
                type="default"
                size="middle"
                onClick={() => props.setSelectedSection(props.sectionName)}
                role="button"
                aria-label={t('egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.btnEdit')}
              >
                {t('egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.btnEdit')}
              </Button>
            )}
            {isSectionSelected && (
              <Button
                id={`egfa-erfuellungsaufwand-vorgabe-gesamtsummen-saveBtn-${props.sectionName}`}
                type="default"
                size="middle"
                onClick={() => {
                  props.applyChanges();
                  props.setSelectedSection(null);
                }}
                className="btn-save"
                role="button"
                aria-label={t('egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.btnSave')}
              >
                {t('egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.btnSave')}
              </Button>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
