// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './gesamtsummen-section.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemAufwandGesamtsummeType, EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';

import { GesamtsummenItemComponent } from './gesamtsummen-item/component.react';

interface GesamtsummenSectionComponentProps {
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  setSelectedSection: (sectionName: EgfaErfuellungItemAufwandGesamtsummeType | null) => void;
  selectedSection: string | null;
  applyChanges: () => void;
}
export function GesamtsummenSectionComponent(props: GesamtsummenSectionComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const gesamtsummenItemProps = {
    vorgabe: props.vorgabe,
    setSelectedSection: props.setSelectedSection,
    selectedSection: props.selectedSection,
    applyChanges: props.applyChanges,
  };
  return (
    <>
      <div className="vorgabe-gesamtsummen-section" role="tablist">
        <Title level={2}>{t(`egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.title`)}</Title>
        <div className="summen-holder">
          <GesamtsummenItemComponent
            sectionName={EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich}
            {...gesamtsummenItemProps}
          />
          <GesamtsummenItemComponent
            sectionName={EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich}
            {...gesamtsummenItemProps}
          />
          <GesamtsummenItemComponent
            sectionName={EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig}
            {...gesamtsummenItemProps}
          />
          <GesamtsummenItemComponent
            sectionName={EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig}
            {...gesamtsummenItemProps}
          />
        </div>
      </div>
    </>
  );
}
