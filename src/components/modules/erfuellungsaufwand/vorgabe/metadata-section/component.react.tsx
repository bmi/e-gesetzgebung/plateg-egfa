// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './metadata-section.less';

import { Button } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { VorgabeItems } from '../../controller';
import { calculateArbeitszeitInStunden } from '../aufwand-tables/PERSONALAUFWAND_JAEHRLICH/PERSONEN_TAGE_MONATE/component.react';
import { forceDecimalFraction, sumZeilenFormatter } from '../controller';

interface MetadataSectionProps {
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  setIsVorgabeModalVisible: (status: boolean) => void;
  itemsList: VorgabeItems[];
  egfaId: string;
  isVisible: boolean;
}
const getSum = (
  zeilen: EgfaModuleErfuellungsaufwandItemEntityAufwandstabelleEntityDTO[],
  sectionName: EgfaErfuellungItemAufwandGesamtsummeType,
  typ: 'Buerger' | '' = '',
): { sum: number; label: string } => {
  let sumKey:
    | 'sachaufwandDifferenz'
    | 'zeitaufwandDifferenz'
    | 'sachaufwandProFall'
    | 'zeitaufwandProFall'
    | 'anzahlDifferenz' = 'zeitaufwandDifferenz';
  let label = '€';

  switch (sectionName) {
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich:
      sumKey = 'zeitaufwandDifferenz';
      label = 'Min.';
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig:
      sumKey = typ === 'Buerger' ? 'zeitaufwandDifferenz' : 'zeitaufwandProFall';
      label = 'Min.';
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich:
      sumKey = 'sachaufwandDifferenz';
      break;
    case EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig:
      sumKey = 'sachaufwandProFall';
      break;
  }
  const aufwandstabelle = zeilen.find((item) => item.gesamtsummeType === sectionName);
  if (aufwandstabelle) {
    const sum = aufwandstabelle?.aufwandstabellenZeilen
      .filter((item) => item.berechnungsart === aufwandstabelle.berechnungsart)
      .reduce((a: number, b: EgfaModuleErfuellungsaufwandItemEntityAufwandstabellenZeileEntityDTO) => {
        if (aufwandstabelle.berechnungsart !== EgfaErfuellungItemBerechnungsartType.PersonenTageMonate) {
          return a + (b[sumKey] || 0);
        } else {
          return a + (calculateArbeitszeitInStunden(b.anzahlDifferenz, b.einheit) || 0) * 60;
        }
      }, 0);
    if (sum) {
      return { sum, label };
    }
  }
  return { sum: 0, label };
};
export function MetadataSection(props: MetadataSectionProps): React.ReactElement {
  const { t } = useTranslation();

  const spiegelvorgabeList = props.itemsList.filter((item) =>
    props.vorgabe.spiegelvorgabenIds.includes(item.id as string),
  );

  const noAnswer = <i>keine Angabe</i>;
  const findFahlZahl = (
    spiegelvorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO,
    gesamtsummeType: EgfaErfuellungItemAufwandGesamtsummeType,
  ): number => {
    return (
      spiegelvorgabe.aufwandstabellen.find((item) => item.gesamtsummeType === gesamtsummeType)?.veraenderungFallzahl ||
      0
    );
  };
  const btnEdit = useRef<HTMLButtonElement>(null);
  const [isFocus, setFocus] = useState(false);
  useEffect(() => {
    if (!props.isVisible && isFocus) {
      btnEdit.current?.focus();
      btnEdit.current?.blur();
    }
  }, [props.isVisible]);

  return (
    <>
      <div className="vorgabe-metadata-section">
        <dl>
          <div className="review-row">
            <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.fallgruppe')}</dt>
            <dd>{props.vorgabe.fallgruppe?.bezeichnung || noAnswer}</dd>
          </div>
          <div className="review-row">
            <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.paragrafUndRechtsnorm')}</dt>
            <dd>{props.vorgabe.paragrafUndRechtsnorm || noAnswer}</dd>
          </div>
          {props.vorgabe.normadressat === EgfaErfuellungItemNormadressat.Verwaltung && (
            <div className="review-row">
              <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.vollzugsebene')}</dt>
              <dd>
                {t(
                  `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.ebeneType.${
                    props.vorgabe.vollzugsebene || ''
                  }`,
                )}
              </dd>
            </div>
          )}

          <div className="review-row">
            <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabe')}</dt>
            <dd>
              {spiegelvorgabeList.length === 0 && noAnswer}
              {spiegelvorgabeList.map((spiegelvorgabe, index) => {
                const buergerTyp =
                  spiegelvorgabe.normadressat === EgfaErfuellungItemNormadressat.Buerger ? 'Buerger' : '';
                const jaehrlichenPersonalaufwands = getSum(
                  spiegelvorgabe.aufwandstabellen,
                  EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
                  buergerTyp,
                );
                const jaehrlichenPersonalaufwandsText = sumZeilenFormatter(jaehrlichenPersonalaufwands, '--');
                const jaehrlichenSachaufwands = getSum(
                  spiegelvorgabe.aufwandstabellen,
                  EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
                  buergerTyp,
                );
                const jaehrlichenSachaufwandsText = sumZeilenFormatter(jaehrlichenSachaufwands, '--');
                const einmaligerPersonalaufwand = getSum(
                  spiegelvorgabe.aufwandstabellen,
                  EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
                  buergerTyp,
                );
                const einmaligerPersonalaufwandText = sumZeilenFormatter(einmaligerPersonalaufwand, '--');
                const einmaligerSachaufwand = getSum(
                  spiegelvorgabe.aufwandstabellen,
                  EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
                  buergerTyp,
                );
                const einmaligerSachaufwandText = sumZeilenFormatter(einmaligerSachaufwand, '--');

                const personalaufwandJaehrlichFahlzahl = findFahlZahl(
                  spiegelvorgabe,
                  EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich,
                );
                const sachaufwandJaehrlichFahlzahl = findFahlZahl(
                  spiegelvorgabe,
                  EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich,
                );
                const personalaufwandEinmaligFahlzahl = findFahlZahl(
                  spiegelvorgabe,
                  EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig,
                );
                const sachaufwandEinmaligFahlzahl = findFahlZahl(
                  spiegelvorgabe,
                  EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig,
                );
                return (
                  <div key={index}>
                    "{spiegelvorgabe.bezeichnung}" (NA{' '}
                    {t(
                      `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${spiegelvorgabe.normadressat}.textTitle`,
                    )}
                    ){' '}
                    <InfoComponent
                      title={t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.title', {
                        spiegelvorgabeName: spiegelvorgabe.bezeichnung,
                      })}
                      titleWithoutPrefix={true}
                    >
                      <div>
                        {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.normadressat')}{' '}
                        {t(
                          `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${spiegelvorgabe.normadressat}.textTitle`,
                        )}
                      </div>
                      <div>
                        {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.paragraf')}{' '}
                        {spiegelvorgabe.paragrafUndRechtsnorm || noAnswer}
                      </div>
                      <br />
                      <strong>
                        {t(
                          `egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.jaehrlicherPersonalaufwand${buergerTyp}`,
                        )}
                      </strong>
                      <dl className="spiegelvorgabe-drawer">
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.fallzahlenTitle',
                          )}
                        </dt>
                        <dd>
                          {personalaufwandJaehrlichFahlzahl
                            ? forceDecimalFraction(personalaufwandJaehrlichFahlzahl, 0)
                            : '--'}
                        </dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.zeitaufwandProFall',
                          )}
                        </dt>
                        <dd>{jaehrlichenPersonalaufwandsText}</dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.summeZeitaufwand',
                          )}
                        </dt>
                        <dd>
                          {sumZeilenFormatter(
                            {
                              ...jaehrlichenPersonalaufwands,
                              sum: jaehrlichenPersonalaufwands.sum * personalaufwandJaehrlichFahlzahl,
                            },
                            '--',
                          )}
                        </dd>
                      </dl>
                      <strong>
                        {t(
                          'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.jaehrlicherSachaufwand',
                        )}
                      </strong>
                      <dl className="spiegelvorgabe-drawer">
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.fallzahlenTitle',
                          )}
                        </dt>
                        <dd>
                          {sachaufwandJaehrlichFahlzahl ? forceDecimalFraction(sachaufwandJaehrlichFahlzahl, 0) : '--'}
                        </dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.sachaufwandProFall',
                          )}
                        </dt>
                        <dd>{jaehrlichenSachaufwandsText}</dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.summeSchaufwand',
                          )}
                        </dt>
                        <dd>
                          {sumZeilenFormatter(
                            {
                              ...jaehrlichenSachaufwands,
                              sum: jaehrlichenSachaufwands.sum * sachaufwandJaehrlichFahlzahl,
                            },
                            '--',
                          )}
                        </dd>
                      </dl>
                      <strong>
                        {t(
                          `egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.einmaligerPersonalaufwand${buergerTyp}`,
                        )}
                      </strong>
                      <dl className="spiegelvorgabe-drawer">
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.fallzahlenTitle',
                          )}
                        </dt>
                        <dd>
                          {personalaufwandEinmaligFahlzahl
                            ? forceDecimalFraction(personalaufwandEinmaligFahlzahl, 0)
                            : '--'}
                        </dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.zeitaufwandProFall',
                          )}
                        </dt>
                        <dd>{einmaligerPersonalaufwandText}</dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.summeZeitaufwand',
                          )}
                        </dt>
                        <dd>
                          {sumZeilenFormatter(
                            {
                              ...einmaligerPersonalaufwand,
                              sum: einmaligerPersonalaufwand.sum * personalaufwandEinmaligFahlzahl,
                            },
                            '--',
                          )}
                        </dd>
                      </dl>
                      <strong>
                        {t(
                          'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.einmaligerSachaufwand',
                        )}
                      </strong>
                      <dl className="spiegelvorgabe-drawer">
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.fallzahlenTitle',
                          )}
                        </dt>
                        <dd>
                          {sachaufwandEinmaligFahlzahl ? forceDecimalFraction(sachaufwandEinmaligFahlzahl, 0) : '--'}
                        </dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.sachaufwandProFall',
                          )}
                        </dt>
                        <dd>{einmaligerSachaufwandText}</dd>
                        <dt>
                          {t(
                            'egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.summeSchaufwand',
                          )}
                        </dt>
                        <dd>
                          {sumZeilenFormatter(
                            {
                              ...einmaligerSachaufwand,
                              sum: einmaligerSachaufwand.sum * sachaufwandEinmaligFahlzahl,
                            },
                            '--',
                          )}
                        </dd>
                      </dl>
                      <Link
                        to={`/egfa/${props.egfaId}/${routes.MODULE}/${
                          routes.ERFUELLUNGSAUFWAND
                        }/${spiegelvorgabe.normadressat.toLocaleLowerCase()}/${spiegelvorgabe.id as string}`}
                      >
                        {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.spiegelvorgabeDrawer.lintToVorgabe')}
                      </Link>
                    </InfoComponent>
                  </div>
                );
              })}
            </dd>
          </div>
        </dl>
        <div className="button-holder">
          <Button
            id="egfa-erfuellungsaufwand-vorgabe-edit-btn"
            type="text"
            size="large"
            className="blue-text-button"
            onClick={() => {
              props.setIsVorgabeModalVisible(true);
              setFocus(true);
            }}
            ref={btnEdit}
          >
            {t('egfa.modules.erfuellungsaufwand.vorgabePage.preview.btnEdit')}
          </Button>
        </div>
      </div>
    </>
  );
}
