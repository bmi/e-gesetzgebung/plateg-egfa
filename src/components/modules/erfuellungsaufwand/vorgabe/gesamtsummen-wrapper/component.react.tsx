// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemAufwandGesamtsummeType, EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';
import { GeneralFormWrapper } from '@plateg/theme';

import { GesamtsummenSectionComponent } from '../gesamtsummen-section/component.react';
import { TablesSection } from '../tables-section/component.react';

interface GesamtsummenWrapperComponentProps {
  vorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  handleFormChange: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
  saveUpdatedVorgabe: (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
}

export function GesamtsummenWrapperComponent(props: GesamtsummenWrapperComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [selectedSection, setSelectedSection] = useState<EgfaErfuellungItemAufwandGesamtsummeType | null>(null);
  const [updatedVorgabe, setUpdatedVorgabe] = useState<EgfaModuleErfuellungsaufwandItemEntityDTO>(props.vorgabe);

  const [showCategory, setShowCategory] = useState<boolean>();
  const [formAufwandsTable] = Form.useForm();

  // Update vorgabe
  const applyChanges = (
    e?: React.MouseEvent<HTMLAnchorElement, MouseEvent> | React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => {
    if (updatedVorgabe.aufwandstabellen[updatedVorgabe.aufwandstabellen.length - 1].kategorie || !showCategory) {
      if (e) {
        // removes warning "form is disconnected"
        e.preventDefault();
      }
      setSelectedSection(null);
      props.saveUpdatedVorgabe(updatedVorgabe);
    }
  };

  // Reset selected section for another vorgabe
  useEffect(() => {
    setSelectedSection(null);
  }, [props.vorgabe.id]);

  const tablesSectionProps = {
    vorgabe: props.vorgabe,
    setUpdatedVorgabe: setUpdatedVorgabe,
    selectedSection: selectedSection,
    handleFormChange: props.handleFormChange,
  };

  return (
    <div style={{ marginBottom: 120 }}>
      <GesamtsummenSectionComponent
        vorgabe={props.vorgabe}
        setSelectedSection={setSelectedSection}
        selectedSection={selectedSection}
        applyChanges={applyChanges}
      />
      {selectedSection && (
        <>
          <GeneralFormWrapper
            form={formAufwandsTable}
            layout="vertical"
            scrollToFirstError={true}
            noValidate={false}
            className="gesamtsummen-form"
          >
            {
              // Dont render view for inactive section
              selectedSection !== tablesSectionProps.selectedSection ? (
                <></>
              ) : (
                <TablesSection
                  formAufwandsTable={formAufwandsTable}
                  setShowCategory={setShowCategory}
                  sectionName={selectedSection}
                  {...tablesSectionProps}
                />
              )
            }
            <Form.Item>
              <Button
                htmlType="submit"
                id={`egfa-erfuellungsaufwand-vorgabe-gesamtsummen-saveBtn-${selectedSection}-bottom`}
                type="primary"
                size="middle"
                onClick={(
                  e: React.MouseEvent<HTMLAnchorElement, MouseEvent> | React.MouseEvent<HTMLButtonElement, MouseEvent>,
                ) => {
                  applyChanges(e);
                }}
                className="btn-save"
              >
                {t('egfa.modules.erfuellungsaufwand.vorgabePage.gesamtsummen.btnSave')}
              </Button>
            </Form.Item>
          </GeneralFormWrapper>
        </>
      )}
    </div>
  );
}
