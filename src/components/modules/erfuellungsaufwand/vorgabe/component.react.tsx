// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import {
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { displayMessage, FormWrapper, GeneralFormWrapper, GlobalDI } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { VorgabeItems } from '../controller';
import { VorgabeModalComponent } from '../vorgabe-modal/component.react';
import { CommentSectionComponent } from './comment-section/component.react';
import { ExpenseItem, ExpenseType } from './expenses-drawer/component.react';
import { GesamtsummenWrapperComponent } from './gesamtsummen-wrapper/component.react';
import { MetadataSection } from './metadata-section/component.react';
import { setExpensesAction } from './store/actions';
import { useStore } from './store/contexts';

export interface VorgabeComponentComponentProps<P> extends ContentPageProps<P> {
  moduleName: string;
  sectionName: string;
  handleFormChange?: () => void;
}
export function VorgabeComponent(
  props: VorgabeComponentComponentProps<EgfaModuleErfuellungsaufwandEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const [vorgabe, setVorgabe] = useState<EgfaModuleErfuellungsaufwandItemEntityDTO>();
  const [isVorgabeModalVisible, setIsVorgabeModalVisible] = useState(false);
  const [itemsList, setItemsList] = useState<VorgabeItems[]>();
  const [isFirstPageLoad, setIsFirstPageLoad] = useState<boolean>(true);
  const [isDirty, setIsDirty] = useState(false);
  const { dispatch } = useStore();

  const routeMatcherVorbereitung = useRouteMatch<{ vorgabeId: string }>(
    `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/:type/:vorgabeId`,
  );

  const initData = () => {
    if (props.formData && props.formData?.items) {
      form.setFieldsValue({
        ...props.formData,
      });
      const selectedItem = [...props.formData.items].filter(
        (item) => item.id === routeMatcherVorbereitung?.params?.vorgabeId,
      )[0];

      setVorgabe(selectedItem);
      setIsFirstPageLoad(false);
      setItemsList(props.formData?.items || []);
    }
  };

  useEffect(() => {
    if (isFirstPageLoad) {
      initData();
    }
    setIsDirty(false);
  }, [props.formData]);

  useEffect(() => {
    initData();

    const egfaErfuellungsaufwandControllerApi = GlobalDI.get<EgfaModuleErfuellungsaufwandControllerApi>(
      'egfaErfuellungsaufwandControllerApi',
    );

    egfaErfuellungsaufwandControllerApi.getEgfaErfuellungstabellen().subscribe({
      next: (data) => {
        if (data) {
          const wirtschaftExpenses = data.wirtschaftsabschnitte.map((item) => {
            return {
              name: `${item.abschnitt} ${item.bezeichnung}`,
              items: [
                {
                  name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.niedrig'),
                  amount:
                    item.niedrig.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.unit'),
                },
                {
                  name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.mittel'),
                  amount:
                    item.mittel.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.unit'),
                },
                {
                  name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.hoch'),
                  amount:
                    item.hoch.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.unit'),
                },
                {
                  name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.durchschnitt'),
                  amount:
                    item.durchschnitt.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.unit'),
                },
              ],
            } as ExpenseType;
          });

          const verwaltungExpenses = [
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.bund'),
              items: data.lohnkosten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.bund.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.unit'),
                } as ExpenseItem;
              }),
            },
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.laender'),
              items: data.lohnkosten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.laender.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.unit'),
                } as ExpenseItem;
              }),
            },
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.kommunen'),
              items: data.lohnkosten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.kommunen.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.unit'),
                } as ExpenseItem;
              }),
            },
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.sozialversicherung'),
              items: data.lohnkosten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.sozialversicherung.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.unit'),
                } as ExpenseItem;
              }),
            },
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.durchschnitt'),
              items: data.lohnkosten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.durchschnitt.toLocaleString(t('langCode'), {
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2,
                    }) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.unit'),
                } as ExpenseItem;
              }),
            },
          ];

          const wegezeitenExpenses = [
            {
              name: t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.name'),
              items: data.wegezeiten.map((item) => {
                return {
                  name: item.bezeichnung,
                  amount:
                    item.wegezeit.toLocaleString(t('langCode')) +
                    ' ' +
                    t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.unit'),
                } as ExpenseItem;
              }),
            },
          ];

          dispatch(
            setExpensesAction({
              wirtschaftData: wirtschaftExpenses,
              verwaltungData: verwaltungExpenses,
              wegezeitenData: wegezeitenExpenses,
            }),
          );
        }
      },
      error: (error: string) => {
        displayMessage(
          t(`egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.loadingError`) + ' ' + error,
          error,
        );
      },
    });
  }, [routeMatcherVorbereitung?.params?.vorgabeId]);

  const handleFormChange = () => {
    props.handleFormChange?.();
    setIsDirty(true);
  };
  const saveUpdatedVorgabe = (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
    updateLocalVorgabe(updatedVorgabe);
    props.onSave(form, EgfaModuleStatusType.InBearbeitung, false)();
  };

  const updateLocalVorgabe = (updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
    setVorgabe(updatedVorgabe);
    const categoryItems = (form.getFieldValue('items') as VorgabeItems[]).filter(
      (item) => item.id !== updatedVorgabe.id,
    );
    updatedVorgabe.aufwandstabellen.forEach((aufwandstabelle) => {
      aufwandstabelle.aufwandstabellenZeilen.forEach((aufwandstabellenZeile: { [key: string]: any }) => {
        delete aufwandstabellenZeile['zeitwerte'];
        delete aufwandstabellenZeile['translationKey'];
      });
    });
    form.setFieldsValue({ items: [...categoryItems, updatedVorgabe] });
  };

  return (
    <>
      {vorgabe && (
        <>
          <div style={{ display: 'none' }}>
            <FormWrapper
              projectName="eGFA"
              title={<></>}
              previousPage={`/egfa/`}
              nextPage={`/egfa/`}
              saveDraft={props.onSave(form)}
              isDirty={() => isDirty}
              form={form}
              formInitialValue={{}}
              setFormInstance={props.setFormInstance}
              showMandatoryFieldInfo={true}
              handleFormChanges={handleFormChange}
            >
              <></>
            </FormWrapper>
          </div>
          <Title level={1}>
            <span
              dangerouslySetInnerHTML={{
                __html: t('egfa.modules.erfuellungsaufwand.vorgabePage.title', {
                  artType: t(`egfa.modules.erfuellungsaufwand.vorgabePage.artType.${vorgabe.art}`),
                  vorgabeBezeichnung: vorgabe.bezeichnung,
                }),
              }}
            ></span>
          </Title>
          <GeneralFormWrapper
            form={form}
            layout="vertical"
            scrollToFirstError={true}
            className="vorgaben-form"
            noValidate
          >
            <MetadataSection
              vorgabe={vorgabe}
              itemsList={itemsList || []}
              isVisible={isVorgabeModalVisible}
              setIsVorgabeModalVisible={setIsVorgabeModalVisible}
              egfaId={props.egfaId}
            />
          </GeneralFormWrapper>
          <VorgabeModalComponent
            isVisible={isVorgabeModalVisible}
            setIsVisible={setIsVorgabeModalVisible}
            sectionKey={t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${vorgabe.normadressat}.textTitle`,
            )}
            isWirtschaft={vorgabe.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft}
            itemsList={itemsList}
            vorgabe={vorgabe}
            normadressat={vorgabe.normadressat}
            successCallback={(updatedVorgabe) => {
              saveUpdatedVorgabe(updatedVorgabe);
              setIsVorgabeModalVisible(false);
            }}
            isEditMode={true}
          />
          <CommentSectionComponent
            vorgabe={vorgabe}
            saveUpdatedVorgabe={saveUpdatedVorgabe}
            handleFormChange={(updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
              handleFormChange();
              updateLocalVorgabe(updatedVorgabe);
            }}
          />
          <GesamtsummenWrapperComponent
            vorgabe={vorgabe}
            handleFormChange={(updatedVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => {
              handleFormChange();
              updateLocalVorgabe(updatedVorgabe);
            }}
            saveUpdatedVorgabe={saveUpdatedVorgabe}
          />
        </>
      )}
    </>
  );
}
