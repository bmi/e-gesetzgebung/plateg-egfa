// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './expenses-drawer.less';

import { Select, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { useStore } from '../store/contexts';

export enum ExpensesDrawerType {
  wirtschaft = 'WIRTSCHAFT',
  verwaltung = 'VERWALTUNG',
  wegezeiten = 'WEGEZEITEN',
}
export interface ExpensesDrawerProps {
  displayType: ExpensesDrawerType;
}
export interface ExpenseItem {
  name: string;
  amount: string;
}
export interface ExpenseType {
  name: string;
  items: ExpenseItem[];
}
type LinkPageNumber = '65' | '59' | '60' | '1';

export function ExpensesDrawer(props: ExpensesDrawerProps): React.ReactElement {
  const { Title } = Typography;
  const [expensesInfo, setExpensesInfo] = useState<ExpenseType[]>();
  const [selectedExpenses, setSelectedExpenses] = useState<ExpenseType>();
  const [selectLabel, setSelectLabel] = useState<string>();
  const [linkPageNumber, setLinkPageNumber] = useState<LinkPageNumber>('1');
  const [showSelect, setShowSelect] = useState<boolean>(false);
  const selectContainerRef = useRef<HTMLDivElement>(null);

  const { t } = useTranslation();

  const { state } = useStore();
  useEffect(() => {
    switch (props.displayType) {
      case ExpensesDrawerType.wirtschaft: {
        setSelectLabel(
          t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.wirtschaftSelectLabel'),
        );
        const wirtschaftExpenses = state.expensesData.wirtschaftData;
        setExpensesInfo(wirtschaftExpenses);

        setSelectedExpenses(wirtschaftExpenses?.[0]);

        setLinkPageNumber('59');
        setShowSelect(true);
        break;
      }
      case ExpensesDrawerType.verwaltung: {
        setSelectLabel(
          t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.verwaltung.wirtschaftSelectLabel'),
        );
        const verwaltungExpenses = state.expensesData.verwaltungData;
        setExpensesInfo(verwaltungExpenses);
        setSelectedExpenses(verwaltungExpenses?.[0]);
        setLinkPageNumber('65');
        setShowSelect(true);
        break;
      }
      case ExpensesDrawerType.wegezeiten: {
        const wegezeitenExpenses = state.expensesData.wegezeitenData;
        setSelectedExpenses(wegezeitenExpenses?.[0]);
        setLinkPageNumber('60');
        setShowSelect(false);
        break;
      }

      default:
        break;
    }
  }, [state]);
  const expensesSelectOnChange = (value: string) => {
    if (expensesInfo) {
      setSelectedExpenses(expensesInfo.find((x) => x.name === value));
    }
  };
  return (
    <div className="expenses-info">
      {showSelect && (
        <div className="select-container" ref={selectContainerRef}>
          <label>{selectLabel}</label>
          <Select
            value={selectedExpenses?.name || ''}
            onChange={expensesSelectOnChange}
            size="small"
            getPopupContainer={() => selectContainerRef.current as HTMLElement}
          >
            {expensesInfo &&
              expensesInfo.map((item) => (
                <Select.Option key={item.name} value={item.name} className="expenses-option">
                  {item.name}
                </Select.Option>
              ))}
          </Select>
        </div>
      )}
      {selectedExpenses && (
        <div className="expenses-table">
          <Title level={2}>{selectedExpenses.name}</Title>
          {props.displayType === ExpensesDrawerType.wegezeiten && (
            <>
              <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.description')}</p>
              <dl className="drawer-table-title">
                <dt>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.verwaltungsebene')}</dt>
                <dd>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.wegezeitenMin')}</dd>
              </dl>
            </>
          )}
          {selectedExpenses.items.map((item) => (
            <dl key={item.name}>
              <dt>{item.name}</dt>
              <dd>{item.amount}</dd>
            </dl>
          ))}
        </div>
      )}
      <a target="_blank" href={`/egesetzgebung-platform-backend/arbeitshilfen/download/29#page=${linkPageNumber}`}>
        {t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.pdfTableLink')}
      </a>
      {props.displayType === ExpensesDrawerType.wirtschaft && (
        <div className="wirtschaft-content">
          <Title level={3}>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.hinweise')}</Title>
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.standDate')}</p>
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.lowQualificationLevel')}</p>
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.mediumQualificationLevel')}</p>
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.highQualificationLevel')}</p>
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wirtschaft.traineesCost')}</p>
        </div>
      )}
      {props.displayType === ExpensesDrawerType.wegezeiten && (
        <div className="wirtschaft-content">
          <p>{t('egfa.modules.erfuellungsaufwand.vorgabePage.expensesDrawer.wegezeiten.standDate')}</p>
        </div>
      )}
    </div>
  );
}
