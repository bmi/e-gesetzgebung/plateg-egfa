// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { ErfuellungsaufwandController } from './controller';
import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandVerwaltungItemEntityDTO,
} from '@plateg/rest-api';

const buergerData: EgfaModuleErfuellungsaufwandVerwaltungItemEntityDTO[] = [
  {
    normadressat: 'BUERGER' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
  {
    normadressat: 'BUERGER' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
  {
    normadressat: 'BUERGER' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
];
const wirtschaftData: EgfaModuleErfuellungsaufwandVerwaltungItemEntityDTO[] = [
  {
    normadressat: 'WIRTSCHAFT' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
  {
    normadressat: 'WIRTSCHAFT' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
];
const verwaltungData: EgfaModuleErfuellungsaufwandVerwaltungItemEntityDTO[] = [
  {
    normadressat: 'VERWALTUNG' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
  {
    normadressat: 'VERWALTUNG' as EgfaErfuellungItemNormadressat,
    spiegelvorgabenIds: ['1', '2'],
    bezeichnung: 'Test',
    art: 'VORGABE' as EgfaErfuellungItemArtType,
    berechnungsart: 'GESAMTAUFWAND_PRO_FALL' as EgfaErfuellungItemBerechnungsartType,
    aufwandstabellen: [],
  },
];

const testData = [...buergerData, ...wirtschaftData, ...verwaltungData];
describe(`Test: sort by EgfaErfuellungItemNormadressat`, () => {
  const controller = new ErfuellungsaufwandController();
  const result = controller.prepareItems(testData);
  it(`result.BUERGER to equal buergerData`, () => {
    expect(result.BUERGER).to.deep.equal(buergerData);
  });
  it(`result.WIRTSCHAFT to equal wirtschaftData`, () => {
    expect(result.WIRTSCHAFT).to.deep.equal(wirtschaftData);
  });
  it(`result.VERWALTUNG to equal verwaltungData`, () => {
    expect(result.VERWALTUNG).to.deep.equal(verwaltungData);
  });
});
