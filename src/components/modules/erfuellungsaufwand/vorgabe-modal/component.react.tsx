// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vorgabe-modal.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaErfuellungItemArtType,
  EgfaErfuellungItemAufwandGesamtsummeType,
  EgfaErfuellungItemBerechnungsartType,
  EgfaErfuellungItemNormadressat,
  EgfaModuleErfuellungsaufwandItemEntityDTO,
} from '@plateg/rest-api';
import { MultiPageModalComponent } from '@plateg/theme';

import { VorgabeItems } from '../controller';
import { createPatternAufwandstabelle } from '../gesamtsummenNormadressaten/section-expert-table/controller';
import { Page1ArtUndBezeichnungComponent } from './pages/page-1-art-bezeichnung/component.react';
import { Page2RechtsgrundlageUndFallgruppeComponent } from './pages/page-2-rechtsgrundlage-fallgruppe/component.react';
import { Page3NewVorgabeReviewComponent } from './pages/page-3-review/component.react';

export interface VorgabeModalComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  sectionKey: string;
  isWirtschaft?: boolean;
  normadressat: EgfaErfuellungItemNormadressat;
  vorgabe?: EgfaModuleErfuellungsaufwandItemEntityDTO;
  successCallback: (newVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO) => void;
  itemsList?: VorgabeItems[];
  isEditMode?: boolean;
}

export const vollzugsebeneTypes = {
  bund: 'BUND',
  land: 'LAND',
};

export function VorgabeModalComponent(props: VorgabeModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const emptyVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO = {
    spiegelvorgabenIds: [],
    fallgruppe: undefined,
    bezeichnung: '',
    art: EgfaErfuellungItemArtType.Vorgabe,
    vollzugsebene: vollzugsebeneTypes.bund,
    paragrafUndRechtsnorm: '',
    aufwandstabellen: [
      {
        ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandJaehrlich),
      },
      {
        ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandJaehrlich),
      },
      {
        ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.PersonalaufwandEinmalig),
      },
      {
        ...createPatternAufwandstabelle(EgfaErfuellungItemAufwandGesamtsummeType.SachaufwandEinmalig),
      },
    ],
    berechnungsart: EgfaErfuellungItemBerechnungsartType.EinzelneStandardaktivitaeten,
    normadressat: props.normadressat,
  };
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [newVorgabe, setNewVorgabe] = useState<EgfaModuleErfuellungsaufwandItemEntityDTO>(
    props.vorgabe || emptyVorgabe,
  );
  const [selectedArt, setSelectedArt] = useState(props.vorgabe?.art || EgfaErfuellungItemArtType.Vorgabe);
  const [selectedBezeichnung, setSelectedBezeichnung] = useState<string>('');
  const [selectedParagrafUndRechtsnorm, setSelectedParagrafUndRechtsnorm] = useState<string>();
  const [selectedSpiegelvorgabenIds, setSelectedSpiegelvorgabenIds] = useState<string[]>([]);
  const [selectedEbene, setSelectedEbene] = useState(props.vorgabe?.vollzugsebene || vollzugsebeneTypes.bund);
  const [isDraftSubmitted, setIsDraftSubmitted] = useState<boolean>(false);

  useEffect(() => {
    setActivePageIndex(0);
    setNewVorgabe(props.vorgabe || emptyVorgabe);

    if (!props.isVisible) {
      setSelectedArt(props.vorgabe?.art || EgfaErfuellungItemArtType.Vorgabe);
      setSelectedBezeichnung(props.vorgabe?.bezeichnung || '');
      setSelectedParagrafUndRechtsnorm(props.vorgabe?.paragrafUndRechtsnorm || '');
      setSelectedSpiegelvorgabenIds(props.vorgabe?.spiegelvorgabenIds || []);
      setSelectedEbene(props.vorgabe?.vollzugsebene || vollzugsebeneTypes.bund);
      setIsDraftSubmitted(false);
    }
  }, [props.isVisible]);

  useEffect(() => {
    if (props.vorgabe) {
      setNewVorgabe(props.vorgabe);
      setSelectedArt(props.vorgabe?.art);
      setSelectedBezeichnung(props.vorgabe?.bezeichnung);
      setSelectedParagrafUndRechtsnorm(props.vorgabe?.paragrafUndRechtsnorm);
      setSelectedSpiegelvorgabenIds(props.vorgabe?.spiegelvorgabenIds);
      setSelectedEbene(props.vorgabe?.vollzugsebene || '');
    }
  }, [props.vorgabe]);

  useEffect(() => {
    setNewVorgabe({
      ...newVorgabe,
      art: selectedArt,
      bezeichnung: selectedBezeichnung,
      spiegelvorgabenIds: selectedSpiegelvorgabenIds,
      paragrafUndRechtsnorm: selectedParagrafUndRechtsnorm,
      vollzugsebene: selectedEbene,
    });
  }, [selectedArt, selectedBezeichnung, selectedSpiegelvorgabenIds, selectedParagrafUndRechtsnorm, selectedEbene]);

  const editLabel = props.isEditMode ? 'Edit' : '';
  const title = props.isWirtschaft
    ? t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.mainTitleWirtschaft${editLabel}`)
    : t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.mainTitle${editLabel}`, {
        sectionKey: props.sectionKey,
      });

  return (
    <>
      <MultiPageModalComponent
        className="new-vorgabe-modal"
        key={`new-vorgabe-${props.isVisible.toString()}`}
        isVisible={props.isVisible}
        setIsVisible={props.setIsVisible}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={title}
        cancelBtnText={t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.btnCancel')}
        nextBtnText={t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.btnNext')}
        prevBtnText={t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.btnPrev')}
        componentReferenceContext={`new-vorgabe-${props.isVisible.toString()}`}
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1ArtUndBezeichnungComponent
                name="page1"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                selectedArt={selectedArt}
                setSelectedArt={setSelectedArt}
                selectedBezeichnung={selectedBezeichnung}
                setSelectedBezeichnung={setSelectedBezeichnung}
                selectedSpiegelvorgabenIds={selectedSpiegelvorgabenIds}
                setSelectedSpiegelvorgabenIds={setSelectedSpiegelvorgabenIds}
                selectedEbene={selectedEbene}
                setSelectedEbene={setSelectedEbene}
                isWirtschaft={props.isWirtschaft}
                itemsList={props.itemsList || []}
                normadressat={props.normadressat}
              />
            ),
          },
          {
            formName: 'page2',
            content: (
              <Page2RechtsgrundlageUndFallgruppeComponent
                name="page2"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                selectedParagrafUndRechtsnorm={selectedParagrafUndRechtsnorm}
                setSelectedParagrafUndRechtsnorm={setSelectedParagrafUndRechtsnorm}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page2.btnNext'),
              shouldNavToNextPage: true,
            },
          },
          {
            content: <Page3NewVorgabeReviewComponent newVorgabe={newVorgabe} itemsList={props.itemsList || []} />,
            primaryInsteadNextBtn: {
              buttonText: t(
                `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.btnNext${editLabel}`,
              ),
              disabled: isDraftSubmitted,
            },
            nextOnClick: () => {
              props.successCallback(newVorgabe);
              props.setIsVisible(false);
              setIsDraftSubmitted(true);
            },
          },
        ]}
      />
    </>
  );
}
