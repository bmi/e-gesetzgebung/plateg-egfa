// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './page-3-review.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemNormadressat, EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';

import { VorgabeItems } from '../../../controller';

export interface Page3NewVorgabeReviewComponentProps {
  newVorgabe: EgfaModuleErfuellungsaufwandItemEntityDTO;
  itemsList: VorgabeItems[];
}

export function Page3NewVorgabeReviewComponent(props: Page3NewVorgabeReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = <i>keine Angabe</i>;

  const getSelectedSpiegelvorgaben = (ids: string[]) => {
    if (!ids.length) {
      return false;
    }
    return props.itemsList
      .filter((item) => ids.includes(item.id as string))
      .map((item, index, list) => {
        const paragrafText = item.paragrafUndRechtsnorm ? `(${item.paragrafUndRechtsnorm})` : '';
        return (
          <span key={index}>
            {item.bezeichnung} {paragrafText}
            {index < list.length - 1 ? ', ' : ''}
          </span>
        );
      });
  };

  const labelKey = props.newVorgabe.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft ? 'Wirtschaft' : '';

  return (
    <div className="new-vorgabe-metadata-section">
      <Title level={2}>
        {t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.title')}
      </Title>
      <dl>
        <div className="review-row">
          <dt>
            {t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.art${labelKey}`,
            )}
          </dt>
          <dd>
            {t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.artType.${
                props.newVorgabe.art || ''
              }`,
            )}
          </dd>
        </div>
        {props.newVorgabe.normadressat === EgfaErfuellungItemNormadressat.Verwaltung && (
          <div className="review-row">
            <dt>
              {t(
                `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.ebene`,
              )}
            </dt>
            <dd>
              {t(
                `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.ebeneType.${
                  props.newVorgabe.vollzugsebene || ''
                }`,
              )}
            </dd>
          </div>
        )}
        <div className="review-row">
          <dt>
            {t(
              `egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.bezeichnung${labelKey}`,
            )}
          </dt>
          <dd>{props.newVorgabe.bezeichnung}</dd>
        </div>
        <div className="review-row">
          <dt>
            {t(
              'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.spiegelvorgaben',
            )}
          </dt>
          <dd>{getSelectedSpiegelvorgaben(props.newVorgabe.spiegelvorgabenIds) || noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>
            {t(
              'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.paragrafUndRechtsnorm',
            )}
          </dt>
          <dd>{props.newVorgabe.paragrafUndRechtsnorm || noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>
            {t(
              'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page3.definitionTitles.fallgruppen',
            )}
          </dt>
          <dd>{props.newVorgabe.fallgruppe || noAnswer}</dd>
        </div>
      </dl>
    </div>
  );
}
