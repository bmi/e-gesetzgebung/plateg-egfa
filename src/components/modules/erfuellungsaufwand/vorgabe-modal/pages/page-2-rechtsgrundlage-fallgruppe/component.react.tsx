// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo } from '@plateg/theme';

import { EgfaFormComponent } from '../../../../../egfa/main/neue-gesetzesfolgenabschaetzung/egfa-form-component/component.react';

export interface Page2RechtsgrundlageUndFallgruppeComponentProps {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  setSelectedParagrafUndRechtsnorm: (paragrafUndRechtsnorm: string) => void;
  selectedParagrafUndRechtsnorm?: string;
}

export function Page2RechtsgrundlageUndFallgruppeComponent(
  props: Page2RechtsgrundlageUndFallgruppeComponentProps,
): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <EgfaFormComponent
        form={Form.useForm()[0]}
        name={props.name}
        title={t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page2.title')}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        values={{
          paragrafUndRechtsnorm: props.selectedParagrafUndRechtsnorm,
        }}
        setValues={(value: { paragrafUndRechtsnorm: string }) => {
          props.setSelectedParagrafUndRechtsnorm(value.paragrafUndRechtsnorm);
        }}
      >
        <FormItemWithInfo
          label={
            <span>
              {t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page2.item1.label')}
            </span>
          }
          name="paragrafUndRechtsnorm"
        >
          <Input type="text" />
        </FormItemWithInfo>
      </EgfaFormComponent>
    </>
  );
}
