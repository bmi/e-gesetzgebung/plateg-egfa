// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './page-1-art-bezeichnung.less';

import { Checkbox, Form, Input, Radio, TreeSelect } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaErfuellungItemArtType, EgfaErfuellungItemNormadressat } from '@plateg/rest-api';
import { FormItemWithInfo, GlobalDI } from '@plateg/theme';

import { EgfaFormComponent } from '../../../../../egfa/main/neue-gesetzesfolgenabschaetzung/egfa-form-component/component.react';
import { ErfuellungsaufwandController, VorgabeItems } from '../../../controller';
import { vollzugsebeneTypes } from '../../component.react';

export interface Page1ArtUndBezeichnungComponentProps {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  selectedArt: EgfaErfuellungItemArtType;
  setSelectedArt: (art: EgfaErfuellungItemArtType) => void;
  selectedBezeichnung: string;
  setSelectedBezeichnung: (bezeichnung: string) => void;
  selectedSpiegelvorgabenIds: string[];
  setSelectedSpiegelvorgabenIds: (spiegelvorgaben: string[]) => void;
  selectedEbene: string;
  setSelectedEbene: (ebene: string) => void;
  isWirtschaft?: boolean;
  itemsList: VorgabeItems[];
  normadressat: EgfaErfuellungItemNormadressat;
}

export function Page1ArtUndBezeichnungComponent(props: Page1ArtUndBezeichnungComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isSpiegelvorgaben, setIsSpiegelvorgaben] = useState(false);
  const [isVorgabe, setIsVorgabe] = useState<boolean>(true);
  const ctrl = GlobalDI.getOrRegister('erfuellungsaufwandController', () => new ErfuellungsaufwandController());
  useEffect(() => {
    if (props.selectedSpiegelvorgabenIds.length) {
      setIsSpiegelvorgaben(true);
    }
  }, []);

  useEffect(() => {
    if (props.selectedArt === EgfaErfuellungItemArtType.Informationspflicht) {
      setIsVorgabe(false);
    }
  }, [props.selectedArt]);

  const getTreeSelectData = (itemsList: VorgabeItems[]): any[] => {
    const dataList: any[] = [];
    const preparedItems = ctrl.prepareItems(itemsList);

    Object.keys(preparedItems).forEach((key) => {
      if (preparedItems[key].length) {
        dataList.push({
          title: t(`egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.${key}.textTitle`),
          value: `${key}Value`,
          selectable: false,
          children: getCategoryChildren(preparedItems[key]),
        });
      }
    });

    return dataList;
  };

  const getCategoryChildren = (list: VorgabeItems[]): any[] => {
    return list.map((item) => {
      const paragrafText = item.paragrafUndRechtsnorm ? `(${item.paragrafUndRechtsnorm})` : '';
      const artType = t(`egfa.modules.erfuellungsaufwand.vorgabePage.artType.${item.art}`);
      return {
        title: `${artType}: "${item.bezeichnung}" ${paragrafText}`,
        value: item.id,
      };
    });
  };

  const title = props.isWirtschaft
    ? t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.titleWirtschaft')
    : t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.title');

  return (
    <>
      <EgfaFormComponent
        name={props.name}
        form={form}
        title={title}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        values={{
          art: props.selectedArt,
          bezeichnung: props.selectedBezeichnung,
          spiegelvorgabenIds: props.selectedSpiegelvorgabenIds,
          vollzugsebene: props.selectedEbene,
        }}
        setValues={(value: {
          art: EgfaErfuellungItemArtType;
          bezeichnung: string;
          spiegelvorgabenIds: string[];
          vollzugsebene?: string;
        }) => {
          props.setSelectedArt(value.art || props.selectedArt);
          props.setSelectedBezeichnung(value.bezeichnung);
          props.setSelectedSpiegelvorgabenIds(value.spiegelvorgabenIds || []);
          props.setSelectedEbene(value.vollzugsebene || props.selectedEbene);
        }}
      >
        <>
          {props.isWirtschaft && (
            <FormItemWithInfo
              label={
                <span>
                  {t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item1.label')}
                </span>
              }
              rules={[
                {
                  required: true,
                  message: t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item1.requiredMsg',
                  ),
                },
              ]}
              name="art"
            >
              <Radio.Group
                className="horizontal-radios"
                name="art"
                onChange={(event) => {
                  if (event?.target?.value === EgfaErfuellungItemArtType.Informationspflicht) {
                    setIsVorgabe(false);
                  } else {
                    setIsVorgabe(true);
                  }
                }}
              >
                <Radio
                  id={`egfa-${props.name}Vorgabe-radio`}
                  className="horizontal-radios"
                  value={EgfaErfuellungItemArtType.Vorgabe}
                >
                  {t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item1.vorgabeLabel',
                  )}
                </Radio>
                <Radio
                  id={`egfa-${props.name}Informationspflicht-radio`}
                  className="horizontal-radios"
                  value={EgfaErfuellungItemArtType.Informationspflicht}
                >
                  {t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item1.informationspflichtLabel',
                  )}
                </Radio>
              </Radio.Group>
            </FormItemWithInfo>
          )}

          {props.normadressat === EgfaErfuellungItemNormadressat.Verwaltung && (
            <FormItemWithInfo
              label={
                <span>
                  {t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.itemEbene.label',
                  )}
                </span>
              }
              rules={[
                {
                  required: true,
                  message: t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.itemEbene.requiredMsg',
                  ),
                },
              ]}
              name="vollzugsebene"
            >
              <Radio.Group className="horizontal-radios" name="vollzugsebene">
                <Radio
                  id={`egfa-${props.name}bund-radio`}
                  className="horizontal-radios"
                  value={vollzugsebeneTypes.bund}
                >
                  {t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.itemEbene.bundLabel',
                  )}
                </Radio>
                <Radio
                  id={`egfa-${props.name}land-radio`}
                  className="horizontal-radios"
                  value={vollzugsebeneTypes.land}
                >
                  {t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.itemEbene.landLabel',
                  )}
                </Radio>
              </Radio.Group>
            </FormItemWithInfo>
          )}
          <FormItemWithInfo
            label={
              <span>
                {t(
                  'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item2.labelVorgabe',
                  { artVorgabe: isVorgabe ? 'Vorgabe' : 'Informationspflicht' },
                )}
              </span>
            }
            name="bezeichnung"
            rules={[
              {
                whitespace: true,
                required: true,
                message: t(
                  'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item2.requiredMsg',
                ),
              },
            ]}
          >
            <Input />
          </FormItemWithInfo>
          <div className="spiegelvorgaben-checkbox-holder">
            <Checkbox
              checked={isSpiegelvorgaben}
              id="new-vorgabe-spiegelvorgabeCheckbox"
              aria-label={t(
                'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.spiegelvorgabeCheckboxLabel',
                { artVorgabe: isVorgabe ? 'Vorgabe' : 'Informationspflicht' },
              )}
              onChange={(e) => {
                setIsSpiegelvorgaben(e.target.checked);
              }}
            >
              {t(
                'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.spiegelvorgabeCheckboxLabel',
                { artVorgabe: isVorgabe ? 'Vorgabe' : 'Informationspflicht' },
              )}
            </Checkbox>
          </div>
          {isSpiegelvorgaben && (
            <FormItemWithInfo
              label={
                <span>
                  {t('egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item3.label')}
                </span>
              }
              name="spiegelvorgabenIds"
              rules={[
                {
                  required: true,
                  message: t(
                    'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item3.requiredMsg',
                  ),
                },
              ]}
            >
              <TreeSelect
                // dropdownClassName is deprecated, but popupClassName is not recognized in cockpit
                dropdownClassName="custom-select-dropdown dropdownClassName"
                treeData={getTreeSelectData(props.itemsList)}
                placeholder={t(
                  'egfa.modules.erfuellungsaufwand.gesamtsummenNormadressaten.newVorgabeModal.page1.item3.placeholder',
                )}
                multiple
              />
            </FormItemWithInfo>
          )}
        </>
      </EgfaFormComponent>
    </>
  );
}
