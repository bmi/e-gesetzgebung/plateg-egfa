// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EgfaErfuellungItemNormadressat, EgfaModuleErfuellungsaufwandItemEntityDTO } from '@plateg/rest-api';

export type VorgabeItems = EgfaModuleErfuellungsaufwandItemEntityDTO;

export interface PreparedItemsInterface {
  [s: string]: VorgabeItems[];
}
export class ErfuellungsaufwandController {
  /**
   * Prepare items by category
   */

  public prepareItems(itemsList: VorgabeItems[]): PreparedItemsInterface {
    return {
      [EgfaErfuellungItemNormadressat.Buerger]: itemsList.filter(
        (item) => item.normadressat === EgfaErfuellungItemNormadressat.Buerger,
      ),
      [EgfaErfuellungItemNormadressat.Wirtschaft]: itemsList.filter(
        (item) => item.normadressat === EgfaErfuellungItemNormadressat.Wirtschaft,
      ),
      [EgfaErfuellungItemNormadressat.Verwaltung]: itemsList.filter(
        (item) => item.normadressat === EgfaErfuellungItemNormadressat.Verwaltung,
      ),
    };
  }
}
