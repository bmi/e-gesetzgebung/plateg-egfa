// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleErfuellungsaufwandEntityDTO,
  EgfaModuleErfuellungsaufwandEntityResponseDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { ErfuellungsaufwandController, PreparedItemsInterface } from './controller';
import { EinleitungErfuellungsaufwand } from './einleitung/component.react';
import { FertigstellungErfuellungsaufwandComponent } from './fertigstellung/component.react';
import { ErfuellungsaufwandExportJsonController } from './fertigstellung/controller';
import { GesamtsummenNormadressatenComponent } from './gesamtsummenNormadressaten/component.react';
import { VorgabeComponent } from './vorgabe/component.react';
import { StoreProvider } from './vorgabe/store';
import { ZusammenfassungAlgemeineangaben } from './zusammenfassung/allgemeine-angaben/component.react';
import { ZusammenfassungBegruendungsteilComponent } from './zusammenfassung/begruendungsteil/component.react';

export function Erfuellungsaufwand(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [erfuellungsaufwandData, setErfuellungsaufwandData] = useState<EgfaModuleErfuellungsaufwandEntityDTO>();
  const egfaErfuellungsaufwandControllerApi = GlobalDI.get<EgfaModuleErfuellungsaufwandControllerApi>(
    'egfaErfuellungsaufwandControllerApi',
  );
  const [isExpertMode, setIsExpertMode] = useState<boolean>(false);
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const { t } = useTranslation();

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaErfuellungsaufwandControllerApi.getEgfaModule9({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleErfuellungsaufwandEntityDTO) => {
    return egfaErfuellungsaufwandControllerApi.modifyEgfaModule9({
      egfaId: egfaId,
      egfaModuleErfuellungsaufwandEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.ERFUELLUNGSAUFWAND);
  }, []);

  useEffect(() => {
    setIsExpertMode(erfuellungsaufwandData?.expertenModus || false);
  }, [erfuellungsaufwandData]);

  const PageRoutingEA = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    updateMenuItems?: (items: PreparedItemsInterface, isModuleClosed?: boolean) => void,
  ) => {
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: erfuellungsaufwandData,
      onSave,
      handleFormChange,
    };
    const ctrl = GlobalDI.getOrRegister('erfuellungsaufwandController', () => new ErfuellungsaufwandController());
    const preparedItems = ctrl.prepareItems(erfuellungsaufwandData?.items || []);
    useEffect(() => {
      if (updateItemDisableStatus && erfuellungsaufwandData) {
        updateItemDisableStatus(
          [routes.ZUSAMMENFASSUNG],
          erfuellungsaufwandData.status === EgfaModuleStatusType.VollstaendigBearbeitet,
        );
      }
      if (updateMenuItems && erfuellungsaufwandData && !isExpertMode) {
        updateMenuItems(preparedItems, erfuellungsaufwandData.status === EgfaModuleStatusType.VollstaendigBearbeitet);
      }
      if (isExpertMode) {
        updateItemDisableStatus?.([routes.BUERGER, routes.WIRTSCHAFT, routes.VERWALTUNG], isExpertMode);
      }
      if (!erfuellungsaufwandData?.aktionen?.includes(AktionType.Schreiben)) {
        updateItemDisableStatus?.([routes.BUERGER, routes.WIRTSCHAFT, routes.VERWALTUNG, routes.ZUSAMMENFASSUNG], true);
      }
    }, [erfuellungsaufwandData, isExpertMode]);
    const ctrlPdf = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
    const PDFExportBtn = ctrlPdf.getPDFExportAction(
      erfuellungsaufwandData?.egfaId as string,
      moduleName,
      erfuellungsaufwandData?.status,
    );

    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.FERTIGSTELLUNG}`]}>
          <FertigstellungErfuellungsaufwandComponent
            {...contentPageProps}
            moduleName={routes.ERFUELLUNGSAUFWAND}
            previousLinkName={routes.BEGRUENDUNGSTEIL}
            additionalActions={PDFExportBtn}
          />
        </Route>
        {(erfuellungsaufwandData?.aktionen?.includes(AktionType.Schreiben) === false ||
          erfuellungsaufwandData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}

        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={routes.ERFUELLUNGSAUFWAND}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.GESAMTSUMMEN_NORMADRESSATEN}
            customTitle={<Title level={1}>{t(`egfa.modules.erfuellungsaufwand.einleitung.title`)}</Title>}
            customContent={<EinleitungErfuellungsaufwand />}
            customClassName={'erfuellungsaufwand-einleitung-holder'}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.GESAMTSUMMEN_NORMADRESSATEN}`]}
        >
          <GesamtsummenNormadressatenComponent
            {...contentPageProps}
            moduleName={routes.ERFUELLUNGSAUFWAND}
            sectionName={routes.GESAMTSUMMEN_NORMADRESSATEN}
            isExpertMode={isExpertMode}
            setIsExpertMode={setIsExpertMode}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${routes.ALGEMEINEANGABEN}`,
          ]}
        >
          <ZusammenfassungAlgemeineangaben
            {...contentPageProps}
            moduleName={routes.ERFUELLUNGSAUFWAND}
            sectionName={routes.ALGEMEINEANGABEN}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.ZUSAMMENFASSUNG}/${routes.BEGRUENDUNGSTEIL}`,
          ]}
        >
          <ZusammenfassungBegruendungsteilComponent
            sectionName={routes.BEGRUENDUNGSTEIL}
            previousLinkName={routes.ALGEMEINEANGABEN}
            nextLinkName={routes.FERTIGSTELLUNG}
            {...contentPageProps}
            moduleName={routes.ERFUELLUNGSAUFWAND}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.BUERGER}/:vorgabeId`,
            `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.WIRTSCHAFT}/:vorgabeId`,
            `/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.VERWALTUNG}/:vorgabeId`,
          ]}
          render={({ match }: RouteComponentProps<{ id: string }>) => {
            return !isExpertMode ? (
              <VorgabeComponent
                {...contentPageProps}
                moduleName={routes.ERFUELLUNGSAUFWAND}
                sectionName={routes.WIRTSCHAFT}
              />
            ) : (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.GESAMTSUMMEN_NORMADRESSATEN}`}
              />
            );
          }}
        ></Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <StoreProvider>
      <ModuleWrapper<EgfaModuleErfuellungsaufwandEntityDTO, EgfaModuleErfuellungsaufwandEntityResponseDTO>
        setFormData={setErfuellungsaufwandData}
        getData={getData}
        putData={putData}
        moduleIdentifier={moduleName}
        egfaId={egfaId}
        form={formInstance}
        pageRouting={PageRoutingEA}
        pageName={currentPageName}
        isFullscreen={true}
        generateJson={(data) => {
          const erfuellungsaufwandExportJsonController = new ErfuellungsaufwandExportJsonController();
          erfuellungsaufwandExportJsonController.erfuellungData = data;
          return erfuellungsaufwandExportJsonController.createErfuellungsaufwandJSON();
        }}
      />
    </StoreProvider>
  );
}
