// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './einleitung.less';

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router-dom';

import { BASE_PATH } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';

export function EinleitungErfuellungsaufwand(): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>(`/egfa/:id/${routes.MODULE}/:moduleName/:pageName`);
  const MODULE_LINK = `#/egfa/${routeMatcherVorbereitung?.params?.id as string}/module`;
  const BIB_LINK = `${BASE_PATH}/arbeitshilfen/download`;
  return (
    <div className="erfuellungsaufwand-einleitung-content">
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph1.content', {
            link: `${BIB_LINK}/34#page=35`,
            interpolation: { escapeValue: false },
          }),
        }}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph2.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph3.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph4.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph5.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <p
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.erfuellungsaufwand.einleitung.content.paragraph6.content', {
            interpolation: { escapeValue: false },
            link1: `${MODULE_LINK}/kmu`,
            link2: `${BIB_LINK}/27`,
            link3: `${MODULE_LINK}/evaluierung`,
          }),
        }}
      ></p>
    </div>
  );
}
