// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './experimentierklauseln.less';

import { FormInstance, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router';

import {
  AktionType,
  EgfaModuleExperimentierklauselControllerApi,
  EgfaModuleExperimentierklauselEntityDTO,
  EgfaModuleExperimentierklauselEntityResponseDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { ErprobungszweckComponent } from './absarz1/erprobungszweck/component.react';
import { AbsatzZweiZusammenfassungComponent } from './absatz2/absatzZweiZusammenfassung/component.react';
import { AenderungReallaborgestaltungComponent } from './absatz2/aenderungReallaborgestaltung/component.react';
import { BefristungErprobungComponent } from './absatz2/befristungErprobung/component.react';
import { BegleitendePflichtenComponent } from './absatz2/begleitendePflichten/component.react';
import { EntscheidungsinhaltComponent } from './absatz2/entscheidungsinhalt/component.react';
import { ErmaechtigungBehoerdeComponent } from './absatz2/ermaechtigungBehoerde/component.react';
import { ErprobungsgegenstandComponent } from './absatz2/erprobungsgegenstand/component.react';
import { MaterielleBegrenzungComponent } from './absatz2/materielleBegrenzung/component.react';
import { MoeglichkeitAufhebungComponent } from './absatz2/moeglichkeitAufhebung/component.react';
import { UmfangErprobungComponent } from './absatz2/umfangErprobung/component.react';
import { VerfahrensvorgabenComponent } from './absatz2/verfahrensvorgaben/component.react';
import { VerhaeltnisAufschiebendeWirkungComponent } from './absatz2/verhaeltnisAufschiebendeWirkung/component.react';
import { VerlaengerungsmoeglichkeitComponent } from './absatz2/verlaengerungsmoeglichkeit/component.react';
import { WeitereNebenbestimmungenComponent } from './absatz2/weitereNebenbestimmungen/component.react';
import { ZustaendigkeitComponent } from './absatz2/zustaendigkeit/component.react';
import { AbsatzDreiZusammenfassungComponent } from './absatz3/absatzDreiZusammenfassung/component.react';
import { BefristungExperimentierklauselComponent } from './absatz3/befristungExperimentierklausel/component.react';
import { EvaluationUndTransferComponent } from './absatz3/evaluationUndTransfer/component.react';
import { VerordnungsermaechtigungComponent } from './absatz4/verordnungsermaechtigung/component.react';
import { generateExperimentierklauselnJSON } from './controller';
import { ExperimentierklauselnContentComponent } from './einleitung/component.react';
import { ExperimentierklauselnFormWrapperComponent } from './experimentierklauseln-form-wrapper/component.react';
import { ExperimentierklauselnIntroComponent } from './intro/component.react';
import { StandortUndTitelComponent } from './standort-und-titel/component.react';
import { ZusammenfassungExperimentierklauselnComponent } from './zusammenfassung/component.react';

export function Experimentierklauseln(props: ModuleProps) {
  const { t } = useTranslation();
  const { Title } = Typography;
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [experimentierklauselnData, setExperimentierklauselnData] = useState<EgfaModuleExperimentierklauselEntityDTO>();
  const egfaModuleExperimentierklauselnController = GlobalDI.get<EgfaModuleExperimentierklauselControllerApi>(
    'egfaModuleExperimentierklauselControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaModuleExperimentierklauselnController.getEgfaModule7({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleExperimentierklauselEntityDTO) =>
    egfaModuleExperimentierklauselnController.modifyEgfaModule7({
      egfaId: egfaId,
      egfaModuleExperimentierklauselEntityDTO: localData,
    });

  useEffect(() => {
    props.setEgfaName(routes.Experimentierklauseln);
  }, []);

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());

  const PageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
  ) => {
    useEffect(() => {
      if (updateItemDisableStatus) {
        const fertigstellungIsDisabled =
          ((!experimentierklauselnData?.influenceSummary || experimentierklauselnData?.influenceSummary === '') &&
            experimentierklauselnData?.aktionen?.includes(AktionType.Schreiben)) ||
          false;
        updateItemDisableStatus([routes.FERTIGSTELLUNG], fertigstellungIsDisabled);
      }
    }, [experimentierklauselnData]);
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: experimentierklauselnData,
      onSave,
      handleFormChange,
    };
    const PDFExportBtn = ctrl.getPDFExportAction(
      experimentierklauselnData?.egfaId as string,
      moduleName,
      experimentierklauselnData?.status,
    );
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            previousLinkName={routes.ZUSAMMENFASSUNG}
            customEditLink={routes.ZUSAMMENFASSUNG}
            additionalActions={PDFExportBtn}
            isTitleStretched={true}
          />
        </Route>
        {(experimentierklauselnData?.allowedToModify === false ||
          experimentierklauselnData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.INTRO}`]}>
          <ExperimentierklauselnIntroComponent moduleName={`${routes.EXPERIMENTIERKLAUSELN}`} />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.STANDORT_UND_TITEL}
            customContent={<ExperimentierklauselnContentComponent />}
            customTitle={
              <div className="module-page-heading">
                <Title level={1}>{t(`egfa.modules.experimentierklauseln.einleitung.title`)}</Title>
              </div>
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.STANDORT_UND_TITEL}`]}>
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ1}/${routes.ERPROBUNGSZWECK}`}
            previousLinkName={routes.EINLEITUNG}
            stepName={`${routes.STANDORT_UND_TITEL}`}
          >
            <StandortUndTitelComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ1}/${routes.ERPROBUNGSZWECK}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.ZUSTAENDIGKEIT}`}
            previousLinkName={routes.STANDORT_UND_TITEL}
            stepName={`${routes.ABSATZ1}.${routes.ERPROBUNGSZWECK}`}
          >
            <ErprobungszweckComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ZUSTAENDIGKEIT}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.ERMAECHTIGUNGBEHOERDE}`}
            previousLinkName={`${routes.ABSATZ1}/${routes.ERPROBUNGSZWECK}`}
            stepName={`${routes.ABSATZ2}.${routes.ZUSTAENDIGKEIT}`}
          >
            <ZustaendigkeitComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ERMAECHTIGUNGBEHOERDE}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.ENTSCHEIDUNGSINHALT}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.ZUSTAENDIGKEIT}`}
            stepName={`${routes.ABSATZ2}.${routes.ERMAECHTIGUNGBEHOERDE}`}
          >
            <ErmaechtigungBehoerdeComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ENTSCHEIDUNGSINHALT}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.ERPROBUNGSGEGENSTAND}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.ERMAECHTIGUNGBEHOERDE}`}
            stepName={`${routes.ABSATZ2}.${routes.ENTSCHEIDUNGSINHALT}`}
          >
            <EntscheidungsinhaltComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ERPROBUNGSGEGENSTAND}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.MATERIELLEBEGRENZUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.ENTSCHEIDUNGSINHALT}`}
            stepName={`${routes.ABSATZ2}.${routes.ERPROBUNGSGEGENSTAND}`}
          >
            <ErprobungsgegenstandComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.MATERIELLEBEGRENZUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.VERFAHRENSVORGABEN}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.ERPROBUNGSGEGENSTAND}`}
            stepName={`${routes.ABSATZ2}.${routes.MATERIELLEBEGRENZUNG}`}
          >
            <MaterielleBegrenzungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.VERFAHRENSVORGABEN}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.UMFANGERPROBUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.MATERIELLEBEGRENZUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.VERFAHRENSVORGABEN}`}
          >
            <VerfahrensvorgabenComponent formData={experimentierklauselnData} />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.UMFANGERPROBUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.BEGLEITENDEPFLICHTEN}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.VERFAHRENSVORGABEN}`}
            stepName={`${routes.ABSATZ2}.${routes.UMFANGERPROBUNG}`}
          >
            <UmfangErprobungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.BEGLEITENDEPFLICHTEN}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.BEFRISTUNGERPROBUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.UMFANGERPROBUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.BEGLEITENDEPFLICHTEN}`}
          >
            <BegleitendePflichtenComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.BEFRISTUNGERPROBUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.VERLAENGERUNGSMOEGLICHKEIT}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.BEGLEITENDEPFLICHTEN}`}
            stepName={`${routes.ABSATZ2}.${routes.BEFRISTUNGERPROBUNG}`}
          >
            <BefristungErprobungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.VERLAENGERUNGSMOEGLICHKEIT}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.VERHAELTNISAUFSCHIEBENDEWIRKUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.BEFRISTUNGERPROBUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.VERLAENGERUNGSMOEGLICHKEIT}`}
          >
            <VerlaengerungsmoeglichkeitComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.VERHAELTNISAUFSCHIEBENDEWIRKUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.WEITERENEBENBESTIMMUNGEN}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.VERLAENGERUNGSMOEGLICHKEIT}`}
            stepName={`${routes.ABSATZ2}.${routes.VERHAELTNISAUFSCHIEBENDEWIRKUNG}`}
          >
            <VerhaeltnisAufschiebendeWirkungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.WEITERENEBENBESTIMMUNGEN}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.AENDERUNGREALLABORGESTALTUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.VERHAELTNISAUFSCHIEBENDEWIRKUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.WEITERENEBENBESTIMMUNGEN}`}
          >
            <WeitereNebenbestimmungenComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.AENDERUNGREALLABORGESTALTUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.MOEGLICHKEITAUFHEBUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.WEITERENEBENBESTIMMUNGEN}`}
            stepName={`${routes.ABSATZ2}.${routes.AENDERUNGREALLABORGESTALTUNG}`}
          >
            <AenderungReallaborgestaltungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.MOEGLICHKEITAUFHEBUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ2}/${routes.ABSATZZWEIZUSAMMENFASSUNG}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.AENDERUNGREALLABORGESTALTUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.MOEGLICHKEITAUFHEBUNG}`}
          >
            <MoeglichkeitAufhebungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ABSATZZWEIZUSAMMENFASSUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ3}/${routes.EVALUATIONUNDTRANSFER}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.MOEGLICHKEITAUFHEBUNG}`}
            stepName={`${routes.ABSATZ2}.${routes.ABSATZZWEIZUSAMMENFASSUNG}`}
          >
            <AbsatzZweiZusammenfassungComponent
              egfaId={egfaId}
              formData={experimentierklauselnData}
              form={formInstance}
            />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ3}/${routes.EVALUATIONUNDTRANSFER}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ3}/${routes.BEFRISTUNGEXPERIMENTIERKLAUSEL}`}
            previousLinkName={`${routes.ABSATZ2}/${routes.ABSATZZWEIZUSAMMENFASSUNG}`}
            stepName={`${routes.ABSATZ3}.${routes.EVALUATIONUNDTRANSFER}`}
          >
            <EvaluationUndTransferComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ3}/${routes.BEFRISTUNGEXPERIMENTIERKLAUSEL}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ3}/${routes.ABSATZDREIZUSAMMENFASSUNG}`}
            previousLinkName={`${routes.ABSATZ3}/${routes.EVALUATIONUNDTRANSFER}`}
            stepName={`${routes.ABSATZ3}.${routes.BEFRISTUNGEXPERIMENTIERKLAUSEL}`}
          >
            <BefristungExperimentierklauselComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ3}/${routes.ABSATZDREIZUSAMMENFASSUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ABSATZ4}/${routes.VERORDNUNGSERMAECHTIGUNG}`}
            previousLinkName={`${routes.ABSATZ3}/${routes.BEFRISTUNGEXPERIMENTIERKLAUSEL}`}
            stepName={`${routes.ABSATZ3}.${routes.ABSATZDREIZUSAMMENFASSUNG}`}
          >
            <AbsatzDreiZusammenfassungComponent
              egfaId={egfaId}
              formData={experimentierklauselnData}
              form={formInstance}
            />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ4}/${routes.VERORDNUNGSERMAECHTIGUNG}`,
          ]}
        >
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.ZUSAMMENFASSUNG}`}
            previousLinkName={`${routes.ABSATZ3}/${routes.ABSATZDREIZUSAMMENFASSUNG}`}
            stepName={`${routes.ABSATZ4}.${routes.VERORDNUNGSERMAECHTIGUNG}`}
          >
            <VerordnungsermaechtigungComponent />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ZUSAMMENFASSUNG}`]}>
          <ExperimentierklauselnFormWrapperComponent
            {...contentPageProps}
            moduleName={`${routes.EXPERIMENTIERKLAUSELN}`}
            nextLinkName={`${routes.FERTIGSTELLUNG}`}
            previousLinkName={`${routes.ABSATZ4}/${routes.VERORDNUNGSERMAECHTIGUNG}`}
            stepName={`${routes.ZUSAMMENFASSUNG}`}
            showMandatoryFieldInfo
          >
            <ZusammenfassungExperimentierklauselnComponent
              egfaId={egfaId}
              formData={experimentierklauselnData}
              form={formInstance}
            />
          </ExperimentierklauselnFormWrapperComponent>
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.INTRO}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleExperimentierklauselEntityDTO, EgfaModuleExperimentierklauselEntityResponseDTO>
      setFormData={setExperimentierklauselnData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRouting}
      pageName={currentPageName}
      generateJson={generateExperimentierklauselnJSON}
      customResultPage={routes.FERTIGSTELLUNG}
    />
  );
}
