// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

export function ExperimentierklauselnContentComponent(): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  return (
    <>
      <Title level={2}>{t(`egfa.modules.experimentierklauseln.einleitung.title2`)}</Title>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.experimentierklauseln.einleitung.text1`),
        }}
      />
      <Image
        loading="lazy"
        preview={false}
        src={(require(`../../../../media/experimentierklauseln/exklauseln_muster.svg`) as ImageModule).default}
        alt={t(`egfa.modules.experimentierklauseln.einleitung.imgAltText`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.experimentierklauseln.einleitung.text2`),
        }}
      />
    </>
  );
}
