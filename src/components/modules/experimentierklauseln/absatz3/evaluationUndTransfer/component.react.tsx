// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function EvaluationUndTransferComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ3}.${routes.EVALUATIONUNDTRANSFER}`;
  const { TextArea } = Input;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      ></div>
      <ol>
        <li>
          <span
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1List.item1`),
            }}
          ></span>
        </li>
        <li>
          <span
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1List.item2`),
            }}
          ></span>
        </li>
        <li>
          <span
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1List.item3`),
            }}
          ></span>
          <InfoComponent title={t(`${translationKey}.text1List.item3Drawer.title`)}>
            <div
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text1List.item3Drawer.text`, {
                  link1: `https://www.bmwk.de/Redaktion/DE/Downloads/J-L/konzeption-zur-evaluierung-neuer-regelungsvorhaben-2013.pdf?__blob=publicationFile`,
                  link2: `https://www.bundesregierung.de/resource/blob/974430/1698788/784303d11758802127d37fc38f49dc8a/2019-11-27-beschluss-evaluierung-data.pdf?download=1`,
                }),
              }}
            ></div>
          </InfoComponent>
        </li>
      </ol>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      ></div>

      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      ></div>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection2.title`)}
        text={t(`${translationKey}.hiddenSection2.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text4`),
        }}
      ></div>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection3.title`)}
        text={t(`${translationKey}.hiddenSection3.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text5`),
        }}
      ></div>
      <FormItemWithInfo
        name={routes.EVALUATIONUNDTRANSFER}
        label={<span>{t(`${translationKey}.formItem.label`)}</span>}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
