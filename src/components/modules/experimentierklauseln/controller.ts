// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleExperimentierklauselControllerApi,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportExperimentierklauselnToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleExperimentierklauselController = GlobalDI.get<EgfaModuleExperimentierklauselControllerApi>(
    'egfaModuleExperimentierklauselControllerApi',
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleExperimentierklauselController.getEgfaModule7({ egfaId }).subscribe({
    next: (data) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Experimentierklausel,
          egfaDatenuebernahmeDTO: generateExperimentierklauselnJSON(data.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Experimentierklausel data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Experimentierklausel data', error);
    },
  });
};

export const generateExperimentierklauselnJSON = (
  experimentierklauselnData: EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const experimentierklauselnJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'experimentierklauselErgebnisdokumentation',
    title: i18n.t(`egfa.modules.experimentierklauseln.fertigstellung.subtitle`),
    content: [],
    children: [],
  };

  if (experimentierklauselnData.influenceExpected && experimentierklauselnData.influenceSummary) {
    experimentierklauselnJSONExport.content.push({
      id: 'experimentierklauseSummary',
      type: EgfaItemType.Text,
      data: experimentierklauselnData.influenceSummary,
    });
  } else {
    experimentierklauselnJSONExport.content.push({
      id: 'experimentierklauseSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.experimentierklauseln.fertigstellung.noImpact`).toString(),
    });
  }
  return experimentierklauselnJSONExport;
};
