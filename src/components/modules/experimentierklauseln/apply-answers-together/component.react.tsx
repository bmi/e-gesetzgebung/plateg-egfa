// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './applyAnswersTogether.less';

import { Button, FormInstance } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { OverviewListItem } from '../absatz2/absatzZweiZusammenfassung/absatzOverviewLIst/component.react';

interface ApplyAnswersTogetherInterface {
  translationKey: string;
  form: FormInstance | undefined;
  summaryFieldName: string;
}
export function ApplyAnswersTogetherComponent(props: ApplyAnswersTogetherInterface): React.ReactElement {
  const { t } = useTranslation();

  const applyAnswersTogether = () => {
    const list = (
      t(`${props.translationKey}.overviewList`, {
        returnObjects: true,
      }) as OverviewListItem[]
    )
      .map((item) => {
        return props.form?.getFieldValue([item.key]);
      })
      .filter((item) => item && item.trim())
      .join('\n');

    props.form?.setFieldsValue({ [props.summaryFieldName]: list });
  };

  return (
    <Button type="link" onClick={applyAnswersTogether} className="btn-apply-answers-together">
      {t(`${props.translationKey}.formItem.btnApplyText`)}
    </Button>
  );
}
