// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function MaterielleBegrenzungComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.MATERIELLEBEGRENZUNG}`;
  const { TextArea } = Input;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      ></div>

      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />

      <ul>
        <li>
          <span
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text2`),
            }}
          ></span>
          <InfoComponent title={t(`${translationKey}.text2drawer.title`)}>
            <div
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text2drawer.text`),
              }}
            ></div>
          </InfoComponent>
        </li>
      </ul>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection2.title`)}
        text={t(`${translationKey}.hiddenSection2.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      ></div>
      <FormItemWithInfo name={routes.MATERIELLEBEGRENZUNG} label={<span>{t(`${translationKey}.formItem.label`)}</span>}>
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
