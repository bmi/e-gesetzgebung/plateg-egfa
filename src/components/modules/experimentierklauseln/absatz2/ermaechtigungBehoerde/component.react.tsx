// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function ErmaechtigungBehoerdeComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.ERMAECHTIGUNGBEHOERDE}`;
  const { TextArea } = Input;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      />
      <ul>
        <li>
          {t(`${translationKey}.text1ListItem1`)}
          <InfoComponent title={t(`${translationKey}.drawerListItem1.title`)}>
            <>{t(`${translationKey}.drawerListItem1.text`)}</>
          </InfoComponent>
        </li>
        <li>
          {t(`${translationKey}.text1ListItem2`)}
          <InfoComponent title={t(`${translationKey}.drawerListItem2.title`)}>
            <>{t(`${translationKey}.drawerListItem2.text`)}</>
          </InfoComponent>
        </li>
        <li>{t(`${translationKey}.text1ListItem3`)}</li>
      </ul>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      />
      <FormItemWithInfo
        name={routes.ERMAECHTIGUNGBEHOERDE}
        label={<span>{t(`${translationKey}.formItem.label`)}</span>}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
