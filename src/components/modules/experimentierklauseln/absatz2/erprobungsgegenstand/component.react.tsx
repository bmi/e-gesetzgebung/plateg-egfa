// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

interface ErprobungsgegenstandListItemInterface {
  text: string;
  drawer?: { title: string; text: string };
}

export function ErprobungsgegenstandComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.ERPROBUNGSGEGENSTAND}`;
  const { TextArea } = Input;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      />
      <ul>
        {(
          t(`${translationKey}.text1List`, {
            returnObjects: true,
          }) as []
        ).map((item: ErprobungsgegenstandListItemInterface) => {
          return (
            <li key={crypto.randomUUID()}>
              <strong>{item.text}</strong>
              {item.drawer && (
                <InfoComponent title={item.drawer.title}>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: item.drawer.text,
                    }}
                  />
                </InfoComponent>
              )}
            </li>
          );
        })}
      </ul>

      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      />
      <FormItemWithInfo name={routes.ERPROBUNGSGEGENSTAND} label={<span>{t(`${translationKey}.formItem.label`)}</span>}>
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
