// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Radio } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleExperimentierklauselEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function VerfahrensvorgabenComponent(props: {
  formData: EgfaModuleExperimentierklauselEntityDTO | undefined;
}): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.VERFAHRENSVORGABEN}`;
  const { TextArea } = Input;
  const [answer, setAnswer] = useState<boolean>();

  useEffect(() => {
    if (props.formData) {
      setAnswer(props.formData.verfahrensvorgabenBestehenRegeln);
    }
  }, [props.formData]);

  return (
    <>
      <div className="like-p">
        {t(`${translationKey}.text1`)}
        <InfoComponent title={t(`${translationKey}.text1drawer.title`)}>
          <div
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1drawer.text`),
            }}
          />
        </InfoComponent>
      </div>

      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1List`),
        }}
      />

      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />

      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      />
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t(`${translationKey}.angabe`)}</legend>
        <Form.Item
          name={'verfahrensvorgabenBestehenRegeln'}
          label={t(`${translationKey}.angabe`)}
          rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
        >
          <Radio.Group
            className="horizontal-radios"
            onChange={(event) => setAnswer(event?.target?.value as boolean)}
            name={'verfahrensvorgabenBestehenRegeln'}
          >
            <Radio id="egfa-verfahrensvorgabenBestehenRegelnJa-radio" className="horizontal-radios" value={true}>
              Ja
            </Radio>
            <Radio id="egfa-verfahrensvorgabenBestehenRegelnNein-radio" className="horizontal-radios" value={false}>
              Nein
            </Radio>
          </Radio.Group>
        </Form.Item>
      </fieldset>
      {answer && <p>{t(`${translationKey}.verfahrensvorgabenBestehenRegelnJa`)}</p>}
      {answer === false && (
        <>
          <p>{t(`${translationKey}.verfahrensvorgabenBestehenRegelnNein`)}</p>
          <FormItemWithInfo
            name={routes.VERFAHRENSVORGABEN}
            label={<span>{t(`${translationKey}.formItem.label`)}</span>}
          >
            <TextArea rows={8} />
          </FormItemWithInfo>
        </>
      )}
    </>
  );
}
