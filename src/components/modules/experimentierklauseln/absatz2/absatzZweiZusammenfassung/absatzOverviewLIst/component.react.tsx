// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './overview-list.less';

import { Button, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleExperimentierklauselEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';

export interface OverviewListItem {
  key: string;
  label: string;
  isMandatory: boolean;
}
interface OverviewListProps {
  translationKey: string;
  linkToModule: string;
  formData: EgfaModuleExperimentierklauselEntityDTO | undefined;
}
export function AbsatzOverviewListComponent(props: OverviewListProps): React.ReactElement {
  const { t } = useTranslation();
  const { TextArea } = Input;

  return (
    <div className="absatz-overview-list">
      <fieldset>
        <legend>{t(`${props.translationKey}.overviewListLegend`)}</legend>
        <ul className="absatz-overview-list">
          {(
            t(`${props.translationKey}.overviewList`, {
              returnObjects: true,
            }) as OverviewListItem[]
          ).map((item) => {
            const isTextareaHidden =
              (item.key === routes.VERFAHRENSVORGABEN && props.formData?.verfahrensvorgabenBestehenRegeln === true) ||
              false;
            return (
              <li key={item.key}>
                <FormItemWithInfo
                  name={item.key}
                  label={<span>{item.label}</span>}
                  rules={
                    item.isMandatory
                      ? [
                          {
                            required: true,
                            whitespace: true,
                            message: t('egfa.modules.validationMessages.defaultRequiredField'),
                          },
                        ]
                      : undefined
                  }
                >
                  {!isTextareaHidden && <TextArea rows={4} />}
                </FormItemWithInfo>
                <Button
                  type="link"
                  onClick={() => {
                    window.location.href = `${props.linkToModule}/${item.key}`;
                  }}
                >
                  {t(`${props.translationKey}.overviewListItemLink`)}
                </Button>
              </li>
            );
          })}
        </ul>
      </fieldset>
    </div>
  );
}
