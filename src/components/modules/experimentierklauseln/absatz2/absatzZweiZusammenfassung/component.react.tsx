// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleExperimentierklauselEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, HinweisComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ApplyAnswersTogetherComponent } from '../../apply-answers-together/component.react';
import { ZusammenfassungErprobungszweckPart } from '../../zusammenfassung/sharedParts/component.react';
import { AbsatzOverviewListComponent } from './absatzOverviewLIst/component.react';

interface AbsatzZweiZusammenfassungInterface {
  egfaId: string;
  formData: EgfaModuleExperimentierklauselEntityDTO | undefined;
  form: FormInstance | undefined;
}
export function AbsatzZweiZusammenfassungComponent(props: AbsatzZweiZusammenfassungInterface): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.ABSATZZWEIZUSAMMENFASSUNG}`;
  const { TextArea } = Input;

  return (
    <>
      <p>{t(`${translationKey}.text1`)}</p>

      <ZusammenfassungErprobungszweckPart egfaId={props.egfaId} />

      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.textLabelAbsatz2`),
        }}
      ></div>
      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis2.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis2.content`, {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        }
      />
      <AbsatzOverviewListComponent
        translationKey={translationKey}
        linkToModule={`#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}`}
        formData={props.formData}
      />

      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      ></div>
      <FormItemWithInfo
        name={routes.ABSATZZWEIZUSAMMENFASSUNG}
        label={
          <span>
            {t(`${translationKey}.formItem.label`)}
            <ApplyAnswersTogetherComponent
              translationKey={translationKey}
              form={props.form}
              summaryFieldName={routes.ABSATZZWEIZUSAMMENFASSUNG}
            />
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
