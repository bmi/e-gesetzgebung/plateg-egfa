// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function WeitereNebenbestimmungenComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.WEITERENEBENBESTIMMUNGEN}`;
  const { TextArea } = Input;

  return (
    <>
      <div className="like-p">
        <span
          dangerouslySetInnerHTML={{
            __html: t(`${translationKey}.text1`),
          }}
        />
        <InfoComponent title={t(`${translationKey}.text1drawer.title`)}>
          <div
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1drawer.text`),
            }}
          />
        </InfoComponent>
      </div>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`)}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      />
      <FormItemWithInfo
        name={routes.WEITERENEBENBESTIMMUNGEN}
        label={<span>{t(`${translationKey}.formItem.label`)}</span>}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
