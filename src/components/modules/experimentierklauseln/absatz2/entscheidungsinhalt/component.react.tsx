// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function EntscheidungsinhaltComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.ENTSCHEIDUNGSINHALT}`;
  const { TextArea } = Input;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      />
      <ol>
        <li>
          {t(`${translationKey}.text1ListItem1`)}
          <InfoComponent title={t(`${translationKey}.drawerListItem1.title`)}>
            <div
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.drawerListItem1.text`),
              }}
            />
          </InfoComponent>

          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.text1ListItem11`),
            }}
          />
        </li>
        <li>
          <p>{t(`${translationKey}.text1ListItem2`)}</p>
          <ul>
            <li
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text1ListItem2ListItem1`),
              }}
            />
            <li
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text1ListItem2ListItem2`),
              }}
            />
          </ul>
        </li>
        <li>
          <p>{t(`${translationKey}.text1ListItem3`)}</p>
          <ul>
            <li
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text1ListItem3ListItem1`),
              }}
            />
            <li
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.text1ListItem3ListItem2`),
              }}
            />
          </ul>
        </li>
      </ol>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      />
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection2.title`)}
        text={t(`${translationKey}.hiddenSection2.text`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      />
      <FormItemWithInfo name={routes.ENTSCHEIDUNGSINHALT} label={<span>{t(`${translationKey}.formItem.label`)}</span>}>
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
