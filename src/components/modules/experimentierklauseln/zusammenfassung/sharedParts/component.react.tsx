// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HinweisComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';

interface SharedPartsInterface {
  egfaId: string;
}
export function ZusammenfassungErprobungszweckPart(props: SharedPartsInterface): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ZUSAMMENFASSUNG}`;
  const { TextArea } = Input;

  return (
    <>
      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis1.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis1.content`, {
                interpolation: { escapeValue: false },
                linkHinweis1: `#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ1}/${routes.ERPROBUNGSZWECK}`,
              }),
            }}
          />
        }
      />
      <FormItemWithInfo
        name={routes.ERPROBUNGSZWECK}
        label={
          <span>
            {t(
              `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ1}.${routes.ERPROBUNGSZWECK}.overviewListLegend`,
            )}
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </>
  );
}
