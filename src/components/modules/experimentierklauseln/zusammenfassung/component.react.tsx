// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleExperimentierklauselEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, HinweisComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ApplyAnswersTogetherComponent } from '../apply-answers-together/component.react';
import { ZusammenfassungErprobungszweckPart } from './sharedParts/component.react';

interface AbsatzDreiZusammenfassungInterface {
  egfaId: string;
  formData: EgfaModuleExperimentierklauselEntityDTO | undefined;
  form: FormInstance | undefined;
}
export function ZusammenfassungExperimentierklauselnComponent(
  props: AbsatzDreiZusammenfassungInterface,
): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ZUSAMMENFASSUNG}`;
  const { TextArea } = Input;

  return (
    <>
      <p>{t(`${translationKey}.text1`)}</p>
      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis1.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis1.content`, {
                interpolation: { escapeValue: false },
                linkHinweis1: `#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.STANDORT_UND_TITEL}`,
              }),
            }}
          />
        }
      />
      <FormItemWithInfo
        name={`titleExperimentierklausel`}
        label={
          <span>
            {t(
              `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.STANDORT_UND_TITEL}.titleExperimentierklausel.label`,
            )}
            :
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>

      <ZusammenfassungErprobungszweckPart egfaId={props.egfaId} />

      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis1.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis1.content`, {
                interpolation: { escapeValue: false },
                linkHinweis1: `#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ2}/${routes.ABSATZZWEIZUSAMMENFASSUNG}`,
              }),
            }}
          />
        }
      />
      <FormItemWithInfo
        name={routes.ABSATZZWEIZUSAMMENFASSUNG}
        label={
          <span>
            {t(
              `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ2}.${routes.ABSATZZWEIZUSAMMENFASSUNG}.overviewListLegend`,
            )}
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>

      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis1.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis1.content`, {
                interpolation: { escapeValue: false },
                linkHinweis1: `#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ3}/${routes.ABSATZDREIZUSAMMENFASSUNG}`,
              }),
            }}
          />
        }
      />
      <FormItemWithInfo
        name={routes.ABSATZDREIZUSAMMENFASSUNG}
        label={
          <span>
            {t(
              `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ3}.${routes.ABSATZDREIZUSAMMENFASSUNG}.overviewListLegend`,
            )}
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>

      <HinweisComponent
        mode="warning"
        title={t(`${translationKey}.hinweis1.title`)}
        content={
          <p
            dangerouslySetInnerHTML={{
              __html: t(`${translationKey}.hinweis1.content`, {
                interpolation: { escapeValue: false },
                linkHinweis1: `#/egfa/${props.egfaId}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.ABSATZ4}/${routes.VERORDNUNGSERMAECHTIGUNG}`,
              }),
            }}
          />
        }
      />
      <FormItemWithInfo
        name={routes.VERORDNUNGSERMAECHTIGUNG}
        label={
          <span>
            {t(
              `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.ABSATZ4}.${routes.VERORDNUNGSERMAECHTIGUNG}.overviewListLegend`,
            )}
          </span>
        }
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      />
      <FormItemWithInfo
        name={`influenceSummary`}
        label={
          <span>
            {t(`${translationKey}.formItem.label`)}
            <ApplyAnswersTogetherComponent
              translationKey={translationKey}
              form={props.form}
              summaryFieldName={'influenceSummary'}
            />
          </span>
        }
        rules={[
          { required: true, whitespace: true, message: t('egfa.modules.validationMessages.defaultRequiredField') },
        ]}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      ></div>
    </>
  );
}
