// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import { BASE_PATH } from '@plateg/rest-api';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';

interface ExperimentierklauselnIntroInterface {
  moduleName: string;
}
export function ExperimentierklauselnIntroComponent(props: ExperimentierklauselnIntroInterface): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const translationKey = `egfa.modules.${props.moduleName}.intro`;
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>(`/egfa/:id/${routes.MODULE}/:moduleName/:pageName`);

  return (
    <>
      <div className="module-page-heading">
        <Title level={1}>{t(`${translationKey}.title`)}</Title>
      </div>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text1`),
        }}
      ></p>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection1.title`)}
        text={t(`${translationKey}.hiddenSection1.text`, {
          link1: 'https://data.consilium.europa.eu/doc/document/ST-13026-2020-INIT/de/pdf',
        })}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text2`),
        }}
      ></p>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection2.title`)}
        text={t(`${translationKey}.hiddenSection2.text`, {
          link1: 'https://data.consilium.europa.eu/doc/document/ST-13026-2020-INIT/de/pdf',
        })}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text3`),
        }}
      ></p>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection3.title`)}
        text={t(`${translationKey}.hiddenSection3.text`)}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text4`),
        }}
      ></p>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text5`),
        }}
      ></p>
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`${translationKey}.hiddenSection4.title`)}
        text={t(`${translationKey}.hiddenSection4.text`)}
      />
      <p
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text6`, {
            link1: `${BASE_PATH}/arbeitshilfen/download/4a2bb916-33ee-460d-bf99-8c656e557825`,
            link2: `https://www.bmwk.de/Redaktion/DE/Downloads/G/gutachten-experimentierklausel-reallabore.pdf?__blob=publicationFile&v=1`,
            link3: `https://www.bmwk.de/Redaktion/DE/Publikationen/Innovation/240506-gutachten-noerr-reallabore.pdf?__blob=publicationFile&v=6`,
          }),
        }}
      ></p>
      <ul className="links-list">
        <li>
          <Button type="link" disabled size="large">
            {t(`${translationKey}.text7.link1`)} &nbsp;
            <RightDirectionArrow />
          </Button>
        </li>
        <li>
          <a
            href={`#/egfa/${routeMatcherVorbereitung?.params.id as string}/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}/${routes.EINLEITUNG}`}
          >
            {t(`${translationKey}.text7.link2`)} &nbsp;
            <RightDirectionArrow />
          </a>
        </li>
      </ul>
    </>
  );
}
