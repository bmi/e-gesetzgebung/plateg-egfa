// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaModuleEntityRequestDTO,
  EgfaModuleExperimentierklauselEntityDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
  stepName: string;
  children: React.ReactElement;
  showMandatoryFieldInfo?: boolean;
}

export function ExperimentierklauselnFormWrapperComponent(
  props: SimpleModuleProps<EgfaModuleEntityRequestDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleExperimentierklauselEntityDTO;
      form.setFieldsValue(formDataValue);
      setIsDirty(false);
    }
  }, [props.formData]);

  const routeKey = `${props.moduleName.toUpperCase()}`;
  const moduleNameRoute: string = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.${props.stepName}`;

  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };

  const onSave = (formInstance: FormInstance<any>) => {
    if (props.stepName === routes.ZUSAMMENFASSUNG) {
      formInstance.setFieldsValue({ influenceExpected: true });
    }
    return props.onSave(formInstance);
  };
  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`${translationKey}.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={props.showMandatoryFieldInfo}
        handleFormChanges={handleFormChange}
        isTitleStretched={true}
      >
        {props.children}
      </FormWrapper>
    </div>
  );
}
