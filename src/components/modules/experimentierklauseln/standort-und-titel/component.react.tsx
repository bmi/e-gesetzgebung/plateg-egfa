// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo } from '@plateg/theme';

import { routes } from '../../../../shares/routes';

export function StandortUndTitelComponent(): React.ReactElement {
  const { t } = useTranslation();
  const translationKey = `egfa.modules.${routes.EXPERIMENTIERKLAUSELN}.${routes.STANDORT_UND_TITEL}`;

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`${translationKey}.text`),
        }}
      ></div>
      <FormItemWithInfo
        name={'titleExperimentierklausel'}
        label={<span>{t(`${translationKey}.titleExperimentierklausel.label`)}</span>}
      >
        <Input />
      </FormItemWithInfo>
    </>
  );
}
