// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './modules.less';

import React, { lazy, Suspense, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { EgfaControllerApi, GetEgfa1Request } from '@plateg/rest-api';
import { ErrorPage, GlobalDI, SuccessPage } from '@plateg/theme';

import { routes } from '../../shares/routes';

const DemografieCheck = lazy(() =>
  import('./auswirkungen-demografie/component.react').then((module) => {
    return { default: module.DemografieCheck };
  }),
);
const AuswirkungenEinnahmenAusgabenComponent = lazy(() =>
  import('./auswirkungen-einnahmen-ausgaben/component.react').then((module) => {
    return { default: module.AuswirkungenEinnahmenAusgabenComponent };
  }),
);
const AuswirkungenMittelstaendischeUnternehmen = lazy(() =>
  import('./auswirkungen-mittelstaendische-unternehmen/component.react').then((module) => {
    return { default: module.AuswirkungenMittelstaendischeUnternehmen };
  }),
);
const AuswirkungenPreise = lazy(() =>
  import('./auswirkungen-preise/component.react').then((module) => {
    return { default: module.AuswirkungenPreise };
  }),
);
const AuswirkungenVerbraucher = lazy(() =>
  import('./auswirkungen-verbraucher/component.react').then((module) => {
    return { default: module.AuswirkungenVerbraucher };
  }),
);
const AuswirkungenWeitere = lazy(() =>
  import('./auswirkungen-weitere/component.react').then((module) => {
    return { default: module.AuswirkungenWeitere };
  }),
);
const DisabilityMainstreaming = lazy(() =>
  import('./disability/component.react').then((module) => {
    return { default: module.DisabilityMainstreaming };
  }),
);
const Enap = lazy(() =>
  import('./enap/component.react').then((module) => {
    return { default: module.Enap };
  }),
);
const Erfuellungsaufwand = lazy(() =>
  import('./erfuellungsaufwand/component.react').then((module) => {
    return { default: module.Erfuellungsaufwand };
  }),
);
const Evaluierung = lazy(() =>
  import('./evaluierung/component.react').then((module) => {
    return { default: module.Evaluierung };
  }),
);
const Gleichstellung = lazy(() =>
  import('./gleichstellung/component.react').then((module) => {
    return { default: module.Gleichstellung };
  }),
);
const GleichwertigkeitsCheck = lazy(() =>
  import('./gleichwertigkeits-check/component.react').then((module) => {
    return { default: module.GleichwertigkeitsCheck };
  }),
);

const SonstigeKosten = lazy(() =>
  import('./sonstige-kosten/component.react').then((module) => {
    return { default: module.SonstigeKosten };
  }),
);
const Experimentierklauseln = lazy(() =>
  import('./experimentierklauseln/component.react').then((module) => {
    return { default: module.Experimentierklauseln };
  }),
);
export function ModulesRoutes(): React.ReactElement {
  const { t } = useTranslation();

  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName`,
  );
  const egfaId = routeMatcherVorbereitung?.params?.id;
  const moduleName = routeMatcherVorbereitung?.params?.moduleName;
  const [rvInfo, setRvInfo] = useState('');
  const [egfaName, setEgfaName] = useState('');

  const moduleProps = { egfaId: egfaId || '', moduleName: moduleName || '' };
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');

  useEffect(() => {
    const sub = egfaController
      .getEgfa1({ id: routeMatcherVorbereitung?.params?.id } as GetEgfa1Request)
      .subscribe((data) => {
        setRvInfo(data.dto.title || '');
      });
    return () => {
      sub.unsubscribe();
    };
  }, [routeMatcherVorbereitung?.params?.moduleName]);
  if (!egfaId || !moduleName) {
    return <Redirect to="/egfa" />;
  }
  return (
    <Suspense>
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ABSCHLUSS_ERFOLGREICH}`]}>
          <SuccessPage
            title={t('egfa.modules.general.successTitle', { egfaModuleName: t(`egfa.modules.${egfaName}.name`) })}
            text={t('egfa.modules.general.successText')}
            link={`/egfa/${egfaId}/${routes.MODUL_UEBERSICHT}`}
            linkText={t('egfa.modules.general.successLink', { rvInfo })}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ABSCHLUSS_FEHLER}`]}>
          <ErrorPage
            title={t('egfa.modules.general.errorTitle')}
            text={t('egfa.modules.general.errorText')}
            link={`/egfa/${egfaId}/${routes.MODUL_UEBERSICHT}`}
            linkText={t('egfa.modules.general.errorLink')}
          />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/verbraucher`]}>
          <AuswirkungenVerbraucher moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/preise`]}>
          <AuswirkungenPreise moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/weitere`]}>
          <AuswirkungenWeitere moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`]}>
          <AuswirkungenMittelstaendischeUnternehmen moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}`]}>
          <GleichwertigkeitsCheck moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`]}>
          <AuswirkungenEinnahmenAusgabenComponent moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}`]}>
          <DemografieCheck moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHSTELLUNG}`]}>
          <Gleichstellung moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/sonstigeKosten`]}>
          <SonstigeKosten moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}`]}>
          <DisabilityMainstreaming moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}`]}>
          <Enap moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.ERFUELLUNGSAUFWAND}`]}>
          <Erfuellungsaufwand moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}`]}>
          <Evaluierung moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.EXPERIMENTIERKLAUSELN}`]}>
          <Experimentierklauseln moduleProps={moduleProps} setEgfaName={setEgfaName} />
        </Route>
      </Switch>
    </Suspense>
  );
}
