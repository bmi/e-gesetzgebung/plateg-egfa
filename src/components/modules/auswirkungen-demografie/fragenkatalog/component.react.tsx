// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './fragenkatalog.less';

import { Checkbox, Form, Input, Typography } from 'antd';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDemografieCategoryEntityDTO, EgfaModuleDemografieEntityDTOAllOf } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { categoriesList } from '../controller';

export interface ModuleFragenkatalogProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange: () => void;
}

export function ModuleFragenkatalog(
  props: ModuleFragenkatalogProps<EgfaModuleDemografieEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [nextLinkEnabled, setNextLinkEnabled] = useState<boolean>(false);

  const [nextLink, setNextLink] = useState<string>(props.nextLinkName || '');
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      setCategoriesName();
      checkSelectedCategories();
      setIsDirty(false);
    }
  }, [props.formData]);

  useEffect(() => {
    if (props.updateNextLink && props.nextLinkName) {
      if (nextLinkEnabled) {
        props.updateNextLink(props.nextLinkName, false);
        setNextLink(props.nextLinkName);
      } else {
        props.updateNextLink(props.nextLinkName, true);
        setNextLink(routes.ZUSAMMENFASSUNG);
      }
    }
  }, [nextLinkEnabled]);

  const prepareCheckboxesList = (checkboxesNames: string[], sectionName: string, categoryName: string) => {
    return checkboxesNames.map((itemName: string) => {
      return (
        <Checkbox
          id={`egfa-${props.moduleName}-${sectionName}-${categoryName}-${itemName}-chk`}
          value={itemName}
          aria-label={t(
            `egfa.modules.${props.moduleName}.fragenkatalog.${sectionName}.${categoryName}.items.${itemName}.label`,
          )}
          key={itemName}
        >
          <>
            <span className="nonBreakingArea">
              <span className="normalBreakingArea">
                {t(
                  `egfa.modules.${props.moduleName}.fragenkatalog.${sectionName}.${categoryName}.items.${itemName}.label`,
                )}
              </span>
            </span>
          </>
        </Checkbox>
      );
    });
  };

  const checkSelectedCategories = () => {
    const formData: EgfaModuleDemografieEntityDTOAllOf = form.getFieldsValue(
      true,
    ) as EgfaModuleDemografieEntityDTOAllOf;
    const fieldsData: EgfaModuleDemografieCategoryEntityDTO[] =
      formData.egfaModuleDemografieCategoryEntity as EgfaModuleDemografieCategoryEntityDTO[];
    const hasSelectedCategories = fieldsData
      .map((category) => {
        const isAuswirkungenEmpty = !!category.auswirkungen && !!category.auswirkungen?.length;
        const isBeruecksichtigungenEmpty = !!category.beruecksichtigungen && !!category.beruecksichtigungen?.length;

        return isAuswirkungenEmpty || isBeruecksichtigungenEmpty;
      }, false)
      .some((item) => item === true);

    setNextLinkEnabled(hasSelectedCategories);
    form.setFieldsValue({ ...formData, influenceExpected: hasSelectedCategories });
  };

  const setCategoriesName = () => {
    const formData: EgfaModuleDemografieEntityDTOAllOf = form.getFieldsValue(
      true,
    ) as EgfaModuleDemografieEntityDTOAllOf;
    const categoriesData: EgfaModuleDemografieCategoryEntityDTO[] =
      formData.egfaModuleDemografieCategoryEntity as EgfaModuleDemografieCategoryEntityDTO[];
    if (!categoriesData.length) {
      Object.entries(categoriesList).forEach(([key, value]) => {
        categoriesData.push({ categoryName: value.categoryName });
      });
      form.setFieldsValue({ egfaModuleDemografieCategoryEntity: categoriesData });
    }
  };

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.fragenkatalog.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange();
        }}
      >
        <div className="fragen-katalog-holder">
          {Object.entries(categoriesList).map(([key, value]) => {
            const formData: EgfaModuleDemografieEntityDTOAllOf = form.getFieldsValue(
              true,
            ) as EgfaModuleDemografieEntityDTOAllOf;
            const ind = formData?.egfaModuleDemografieCategoryEntity?.findIndex(
              (item) => item.categoryName === value.categoryName,
            ) as number;
            return (
              <div key={key}>
                <Form.Item
                  name={['egfaModuleDemografieCategoryEntity', ind, 'categoryName']}
                  initialValue={value.categoryName}
                  hidden={true}
                  style={{ display: 'none' }}
                >
                  <Input type="hidden" />
                </Form.Item>
                <Title level={2}>
                  {t(`egfa.modules.${props.moduleName}.fragenkatalog.${value.categoryName}.categoryTitle`)}
                </Title>

                {value.auswirkungen && (
                  <>
                    <fieldset className="fieldset-form-items">
                      {i18n.exists(
                        `egfa.modules.${props.moduleName}.fragenkatalog.${value.categoryName}.auswirkungen.title`,
                      ) && (
                        <legend>
                          {t(`egfa.modules.${props.moduleName}.fragenkatalog.${value.categoryName}.auswirkungen.title`)}
                        </legend>
                      )}
                      <Form.Item name={['egfaModuleDemografieCategoryEntity', ind, 'auswirkungen']}>
                        <Checkbox.Group name="auswirkungen" onChange={() => checkSelectedCategories()}>
                          {prepareCheckboxesList(value.auswirkungen, value.categoryName, 'auswirkungen')}
                        </Checkbox.Group>
                      </Form.Item>
                    </fieldset>
                  </>
                )}

                {value.beruecksichtigungen && (
                  <>
                    <fieldset className="fieldset-form-items">
                      {i18n.exists(
                        `egfa.modules.${props.moduleName}.fragenkatalog.${value.categoryName}.beruecksichtigungen.title`,
                      ) && (
                        <legend>
                          {t(
                            `egfa.modules.${props.moduleName}.fragenkatalog.${value.categoryName}.beruecksichtigungen.title`,
                          )}
                        </legend>
                      )}
                      <Form.Item name={['egfaModuleDemografieCategoryEntity', ind, 'beruecksichtigungen']}>
                        <Checkbox.Group name="beruecksichtigungen" onChange={() => checkSelectedCategories()}>
                          {prepareCheckboxesList(value.beruecksichtigungen, value.categoryName, 'beruecksichtigungen')}
                        </Checkbox.Group>
                      </Form.Item>
                    </fieldset>
                  </>
                )}
              </div>
            );
          })}
        </div>
      </FormWrapper>
    </div>
  );
}
