// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleDemografieControllerApi,
  EgfaModuleDemografieEntityDTO,
  EgfaModuleDemografieEntityResponseDTO,
  EgfaModuleStatusType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import {
  DisabilityReferenzenTypes,
  EnapReferenzenTypes,
  GleichstellungReferenzenTypes,
  GleichwertigkeitReferenzenTypes,
  SonstigeKostenReferenzenTypes,
} from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { ModuleDemografischeEntwicklung } from './demographische-entwicklung/component.react';
import { DemografieModuleFertigstellung } from './fertigstellung/component.react';
import { generateDemografieCheckJSON } from './fertigstellung/controller';
import { DemografieModuleZusammenfassung } from './zusammenfassung/component.react';

export function DemografieCheck(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [demografieData, SetDemografieDataData] = useState<EgfaModuleDemografieEntityDTO & ExtendedModuleData>();
  const egfaModuleDemografieControllerApi = GlobalDI.get<EgfaModuleDemografieControllerApi>(
    'egfaModuleDemografieControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaModuleDemografieControllerApi.getEgfaModule13({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleDemografieEntityDTO) => {
    return egfaModuleDemografieControllerApi.modifyEgfaModule13({
      egfaId: egfaId,
      egfaModuleDemografieEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.DEMOGRAFIE_CHECK);
  }, []);

  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (demografieData && demografieData.referenzenData) {
      setReferenzenData(demografieData.referenzenData);
    }
  }, [demografieData]);
  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string, isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ) => {
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: demografieData,
      onSave,
      handleFormChange,
    };

    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.FERTIGSTELLUNG}`]}>
          <DemografieModuleFertigstellung {...contentPageProps} moduleName={routes.DEMOGRAFIE_CHECK} />
        </Route>
        {(demografieData?.aktionen?.includes(AktionType.Schreiben) === false ||
          demografieData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                push
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_EINLEITUNG}
            nextLinkName={`${routes.DEMOGRAFIE_DEMO_ENTWICKLUNG}`}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_DEMO_ENTWICKLUNG}`]}
        >
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_DEMO_ENTWICKLUNG}
            nextLinkName={`${routes.DEMOGRAFIE_FAMILIE}`}
            previousLinkName={routes.DEMOGRAFIE_EINLEITUNG}
            specificCategory="EgfaDemografieEntwicklung"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_FAMILIE}`]}>
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_FAMILIE}
            previousLinkName={`${routes.DEMOGRAFIE_DEMO_ENTWICKLUNG}`}
            nextLinkName={`${routes.DEMOGRAFIE_ARBEIT}`}
            specificCategory="EgfaFamilieKinderJugendliche"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg4],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.demografieGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedCheckboxArbeit,
                      GleichstellungReferenzenTypes.hasSelectedCheckboxZeit,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_ARBEIT}`]}>
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_ARBEIT}
            previousLinkName={`${routes.DEMOGRAFIE_FAMILIE}`}
            nextLinkName={`${routes.DEMOGRAFIE_ALTER}`}
            specificCategory="EgfaArbeitBerufWirtschaft"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg4,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg85,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg101,
                    ],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.demografieGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedCheckboxArbeit,
                      GleichstellungReferenzenTypes.hasSelectedCheckboxWissen,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.demografieDisabilityeferenzen,
                    conditionsList: [DisabilityReferenzenTypes.hasSelectedCheckboxTeilhabe],
                    moduleName: routes.DISABILITY,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_ALTER}`]}>
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_ALTER}
            previousLinkName={`${routes.DEMOGRAFIE_ARBEIT}`}
            nextLinkName={`${routes.DEMOGRAFIE_REGIONEN}`}
            specificCategory="EgfaAlterWohnenPflege"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg113],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.demografieGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge,
                      GleichwertigkeitReferenzenTypes.hasSelectedWohnraum,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_REGIONEN}`]}>
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_REGIONEN}
            previousLinkName={`${routes.DEMOGRAFIE_ALTER}`}
            nextLinkName={`${routes.DEMOGRAFIE_GENERATIONEN}`}
            specificCategory="EgfaRegionen"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_GENERATIONEN}`]}>
          <ModuleDemografischeEntwicklung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            sectionName={routes.DEMOGRAFIE_GENERATIONEN}
            previousLinkName={`${routes.DEMOGRAFIE_REGIONEN}`}
            nextLinkName={`${routes.ZUSAMMENFASSUNG}`}
            updateNextLink={updateItemDisableStatus}
            specificCategory="EgfaZusammenhatlGenerationenZivilgesellschaftlichesEngagement"
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.demografieEaOehhReferenzen,
                    moduleName: routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN,
                  },
                  {
                    referenzenData: referenzenData?.demografieSonstigeKostenReferenzen,
                    conditionsList: [SonstigeKostenReferenzenTypes.hasSelectedInfluenceExpected],
                    moduleName: routes.SONSTIGE_KOSTEN,
                  },
                  {
                    referenzenData: referenzenData?.demografieEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg82],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.demografieGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedEngagement],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.ZUSAMMENFASSUNG}`]}>
          <DemografieModuleZusammenfassung
            {...contentPageProps}
            moduleName={`${routes.DEMOGRAFIE_CHECK}`}
            nextLinkName={`${routes.FERTIGSTELLUNG}`}
            previousLinkName={`${routes.DEMOGRAFIE_GENERATIONEN}`}
            sectionName={routes.ZUSAMMENFASSUNG}
            updateNextLink={updateItemDisableStatus}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.DEMOGRAFIE_CHECK}/${routes.DEMOGRAFIE_EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleDemografieEntityDTO, EgfaModuleDemografieEntityResponseDTO>
      setFormData={SetDemografieDataData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      customResultPage="fertigstellung"
      generateJson={generateDemografieCheckJSON}
    />
  );
}
