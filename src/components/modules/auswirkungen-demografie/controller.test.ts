// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { EgfaModuleDemografieCategoryEntityDTO } from '@plateg/rest-api';

import { getSelectedItems, retrieveItemsCategory } from './controller';

const emptyCategory = { categoryName: '', type: '' };
const demografischeEntwicklungAuswirkungen = { categoryName: 'demografischeEntwicklung', type: 'auswirkungen' };
const demografischeEntwicklungBeruecksichtigungen = {
  categoryName: 'demografischeEntwicklung',
  type: 'beruecksichtigungen',
};
describe(`Test: retrieveItemsCategory`, () => {
  it(`return empty category`, () => {
    const result = retrieveItemsCategory('');
    expect(result).to.eql(emptyCategory);
  });

  it(`return empty category if unknown item`, () => {
    const result = retrieveItemsCategory('UNKNOWN');
    expect(result).eql(emptyCategory);
  });

  it(`return category Auswirkungen`, () => {
    const result = retrieveItemsCategory('ZU_UND_ABWANDERUNG');
    expect(result).eql(demografischeEntwicklungAuswirkungen);
  });

  it(`return category beruecksichtigungen`, () => {
    const result = retrieveItemsCategory('AENDERUNG_ALTER_STRUKTUR');
    expect(result).eql(demografischeEntwicklungBeruecksichtigungen);
  });
});

const testData: EgfaModuleDemografieCategoryEntityDTO[] = [
  {
    categoryName: 'demografischeEntwicklung',
    auswirkungen: ['GEBURTEN_ENTWICKLUNG', 'ZU_UND_ABWANDERUNG'],
    beruecksichtigungen: ['AENDERUNG_ALTER_STRUKTUR'],
  },
  {
    categoryName: 'familieKinderJugendliche',
    auswirkungen: ['KINDER_UND_FAMILIE_FREUNDLICHKEIT'],
    beruecksichtigungen: [],
  },
];

const auswirkungenList = ['GEBURTEN_ENTWICKLUNG', 'ZU_UND_ABWANDERUNG', 'KINDER_UND_FAMILIE_FREUNDLICHKEIT'];
const beruecksichtigungenList = ['AENDERUNG_ALTER_STRUKTUR'];
describe(`Test: getSelectedItems`, () => {
  it(`return empty`, () => {
    const result = getSelectedItems([]);
    expect(result).to.eql([[], []]);
  });
  it(`return category`, () => {
    const result = getSelectedItems(testData);
    expect(result).to.eql([auswirkungenList, beruecksichtigungenList]);
  });
});
