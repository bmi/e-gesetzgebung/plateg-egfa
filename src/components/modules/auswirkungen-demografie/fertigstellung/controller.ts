// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleDemografieControllerApi,
  EgfaModuleDemografieEntityDTOAllOf,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportDemografieCheckToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleDemografieControllerApi>('egfaModuleDemografieControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule13({ egfaId }).subscribe({
    next: (demografieCheckData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Demografie,
          egfaDatenuebernahmeDTO: generateDemografieCheckJSON(demografieCheckData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Demografie-Check data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Demografie-Check data', error);
    },
  });
};
export const generateDemografieCheckJSON = (
  demografieCheckData: EgfaModuleDemografieEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const demografieCheckJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'demografieCheckErgebnisdokumentation',
    title: i18n.t(`egfa.modules.demografie.fertigstellung.title`),
    content: [],
    children: [],
  };
  if (demografieCheckData.influenceExpected && demografieCheckData.influenceSummary) {
    demografieCheckJSONExport.content.push({
      id: 'demografieCheckDataSummary',
      type: EgfaItemType.Text,
      data: demografieCheckData.influenceSummary,
    });
  } else {
    demografieCheckJSONExport.content.push({
      id: 'demografieCheckDataSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.demografie.fertigstellung.noImpact`).toString(),
    });
  }
  return demografieCheckJSONExport;
};
