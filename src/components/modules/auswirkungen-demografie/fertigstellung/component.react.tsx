// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleDemografieEntityDTOAllOf, EgfaModuleStatusType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ModulesListController } from '../../../egfa/main/modules-list/controller';
import { SimpleModuleProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';
import { getSelectedItems } from '../controller';

interface DemografieModuleFertigstellungState {
  bearbeitungLink: string;
  summaryText: string;
  noSelections: boolean;
}
export function DemografieModuleFertigstellung(
  props: SimpleModuleProps<EgfaModuleDemografieEntityDTOAllOf>,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const moduleNameRoute: string = routes.DEMOGRAFIE_CHECK;
  const [state, setState] = useState<DemografieModuleFertigstellungState>({
    bearbeitungLink: '',
    summaryText: '',
    noSelections: true,
  });
  const [auswirkungenList, beruecksichtigungenList] = getSelectedItems(
    props.formData?.egfaModuleDemografieCategoryEntity || [],
  );

  useEffect(() => {
    if (auswirkungenList.length > 0 || beruecksichtigungenList.length > 0) {
      setState({
        ...state,
        bearbeitungLink: routes.ZUSAMMENFASSUNG,
        summaryText: props.formData?.influenceSummary || '',
        noSelections: false,
      });
    } else {
      setState({
        ...state,
        bearbeitungLink: routes.ZUSAMMENFASSUNG,
        summaryText: '',
        noSelections: true,
      });
    }
  }, [props.formData]);
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const PDFExportBtn = ctrl.getPDFExportAction(props.egfaId, props.moduleName, props.formData?.status);
  return (
    <ZusammenfassungWrapper
      egfaId={props.egfaId}
      prevPage={moduleNameRoute + '/' + state.bearbeitungLink}
      onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
      moduleData={props.formData}
      moduleNameRoute={props.moduleName}
      additionalActions={PDFExportBtn}
    >
      <>
        <ZusammenfassungItem
          title={t(`egfa.modules.${props.moduleName}.zusammenfassung.title`)}
          bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${state.bearbeitungLink}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          egfaId={props.egfaId}
          allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
        >
          <>
            {state.noSelections ? (
              t(`egfa.modules.${props.moduleName}.fertigstellung.noImpact`)
            ) : (
              <>
                <Title level={3}>{t(`egfa.modules.${props.moduleName}.fertigstellung.impact`)}</Title>
                <p className="ant-typography p-no-style">{state.summaryText}</p>
              </>
            )}
          </>
        </ZusammenfassungItem>
      </>
    </ZusammenfassungWrapper>
  );
}
