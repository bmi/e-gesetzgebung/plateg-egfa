// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EgfaModuleDemografieCategoryEntityDTO } from '@plateg/rest-api';

export interface CategoriesListInterface {
  [s: string]: CategoriesListItemInterface;
}
export interface CategoriesListItemInterface {
  categoryName: string;
  auswirkungen: string[];
  beruecksichtigungen?: string[];
}

interface CategoryItemInterface {
  categoryName: string;
  type: string;
}
export const categoriesList: CategoriesListInterface = {
  EgfaDemografieEntwicklung: {
    categoryName: 'demografischeEntwicklung',
    auswirkungen: [
      'GEBURTEN_ENTWICKLUNG',
      'ZU_UND_ABWANDERUNG',
      'ALTER_STRUKTUR_DER_BEVOELKERUNG',
      'REGIONAL_VERTEILUNG_DER_BEVOLKERUNG',
    ],
    beruecksichtigungen: ['AENDERUNG_ALTER_STRUKTUR', 'STEIGENDER_ANTEIL_DER_MIGRANTEN'],
  },

  EgfaFamilieKinderJugendliche: {
    categoryName: 'familieKinderJugendliche',
    auswirkungen: [
      'KINDER_UND_FAMILIE_FREUNDLICHKEIT',
      'VEREINBARKEIT_VON_FAMILIE_UND_BERUF',
      'BETREUUNG_UND_AUSBILDUNG_VON_KINDER_UND_JUGENDLICHEN',
    ],
  },

  EgfaArbeitBerufWirtschaft: {
    categoryName: 'arbeitBerufWirtschaft',
    auswirkungen: [
      'CHANCEN_QUALIFIZIERUNG_UND_WEITERBILDUNG',
      'KULTUR_LAENGEREN_ARBEITEN',
      'ALTERSGERECHTE_ARBEITSWELT_UND_ARBEITS_GESTALTUNG',
      'GESUNDHEITSFOEDERUNG_UND_KRANKHEITSPRAEVENTION_AM_ARBEITSPLATZ',
      'SICHERUNG_FACHKRAEFTEBASIS',
      'INTEGRATION_ZUWANDERER',
    ],
    beruecksichtigungen: ['VERAENDERUNGEN_NACHFRAGEVERHALTEN_UND_NACHFRAGEVERSCHIEBUNGEN'],
  },

  EgfaAlterWohnenPflege: {
    categoryName: 'alterWohnenPflege',
    auswirkungen: [
      'SITUATION_VON_HILFE_UND_PFLEGEBEDUERFTIGEN',
      'SELBSTBESTIMMTES_WOHNEN_UND_MOBILITAET',
      'SITUATION_PFLENGENDER_UND_AUSSCHOEPFUNG_DES_PFLEGEPOTENZIAL',
      'WOHNORTNAHE_UND_BARRIEREARME_VERSORGUNG_DASEINVERSORGE',
    ],
  },

  EgfaRegionen: {
    categoryName: 'regionen',
    auswirkungen: ['GLEICHWERTIGE_LEBENSVERHAELTNISSE'],
  },

  EgfaZusammenhatlGenerationenZivilgesellschaftlichesEngagement: {
    categoryName: 'zusammenhaltDerGenerationenZivilgesellschaftlichesEngagement',
    auswirkungen: [
      'FINANZIELLE_BELASTUNG_KUENFTIGE_GENERATIONEN',
      'ZUSAMMENLEBEN_UND_UNTERSTUETZUNG_DER_GENERATIONEN',
      'ZIVILGESELLSCHAFTLICHES_ENGAGEMENT',
    ],
  },
};

export const retrieveItemsCategory = (item: string): CategoryItemInterface => {
  if (!item || item.length < 0) {
    return { categoryName: '', type: '' };
  }
  for (const entry in categoriesList) {
    if (categoriesList[entry].auswirkungen.includes(item)) {
      return { categoryName: categoriesList[entry].categoryName, type: 'auswirkungen' };
    } else if (categoriesList[entry].beruecksichtigungen && categoriesList[entry].beruecksichtigungen?.includes(item)) {
      return { categoryName: categoriesList[entry].categoryName, type: 'beruecksichtigungen' };
    }
  }
  return { categoryName: '', type: '' };
};

export const getSelectedItems = (list: EgfaModuleDemografieCategoryEntityDTO[]): string[][] => {
  let auswirkungen = [];
  let beruecksichtigungen = [];
  return list.reduce(
    (acc: string[][], category: EgfaModuleDemografieCategoryEntityDTO): string[][] => {
      auswirkungen = [...acc[0], ...(category.auswirkungen || [])];
      beruecksichtigungen = [...acc[1], ...(category.beruecksichtigungen || [])];

      return [auswirkungen, beruecksichtigungen];
    },
    [[], []],
  );
};
