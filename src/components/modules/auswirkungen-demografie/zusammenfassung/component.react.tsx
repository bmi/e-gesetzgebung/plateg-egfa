// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDemografieEntityDTOAllOf } from '@plateg/rest-api';
import { CheckOutlined, FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { getSelectedItems, retrieveItemsCategory } from '../controller';

export interface DemografieModuleZusammenfassungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  sectionName: string;
  previousLinkName?: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
}
export function DemografieModuleZusammenfassung(
  props: DemografieModuleZusammenfassungProps<EgfaModuleDemografieEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [auswirkungen, setAuswirkungen] = useState<string[]>([]);
  const [beruecksichtigungen, setBeruecksichtigungen] = useState<string[]>([]);
  const [summaryLabel, setSummaryLabel] = useState<string>('');
  const [nextLink] = useState<string>(props.nextLinkName || '');
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      const formData = { ...props.formData };
      const [auswirkungenList, beruecksichtigungenList] = getSelectedItems(
        formData.egfaModuleDemografieCategoryEntity || [],
      );
      auswirkungenList.sort();
      beruecksichtigungenList.sort();
      setAuswirkungen(auswirkungenList);
      setBeruecksichtigungen(beruecksichtigungenList);
      form.setFieldsValue(formData);
      setIsDirty(false);
    }
  }, [props.formData]);

  const createList = (elements: string[]) => {
    return (
      <ul className="egfa-checked-list" style={{ listStyle: 'none' }}>
        {elements.map((item, index) => (
          <li key={index}>
            <strong>
              <CheckOutlined />
              &nbsp;
              {t(
                `egfa.modules.demografie.fragenkatalog.${retrieveItemsCategory(item).categoryName}.${
                  retrieveItemsCategory(item).type
                }.items.${item}.shortDescription`,
              )}
            </strong>
          </li>
        ))}
      </ul>
    );
  };

  useEffect(() => {
    if (auswirkungen.length >= 1 && beruecksichtigungen.length >= 1) {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.textAreaBothSeveralSelectionsLabel`));
    } else if (auswirkungen.length === 1 && beruecksichtigungen.length < 1) {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.auswirkungen.textAreaSingularSelectionLabel`));
    } else if (auswirkungen.length > 1 && beruecksichtigungen.length < 1) {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.auswirkungen.textAreaSeveralSelectionsLabel`));
    } else if (beruecksichtigungen.length === 1 && auswirkungen.length < 1) {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.beruecksichtigungen.textAreaSingularSelectionLabel`));
    } else if (beruecksichtigungen.length > 1 && auswirkungen.length < 1) {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.beruecksichtigungen.textAreaSeveralSelectionsLabel`));
    } else {
      setSummaryLabel(t(`egfa.modules.demografie.zusammenfassung.auswirkungen.textAreaDefaultLabel`));
    }
  }, [auswirkungen, beruecksichtigungen]);

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.zusammenfassung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={auswirkungen?.length > 0}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        {auswirkungen?.length === 0 && <p>{t(`egfa.modules.demografie.fertigstellung.noImpact`)}</p>}
        {auswirkungen?.length === 1 && (
          <p>{t(`egfa.modules.demografie.zusammenfassung.auswirkungen.singularAreaSelectionLabel`)}</p>
        )}
        {auswirkungen?.length > 1 && (
          <p>{t(`egfa.modules.demografie.zusammenfassung.auswirkungen.severalAreasSelectionsLabel`)}</p>
        )}
        {auswirkungen.length ? createList(auswirkungen) : null}
        {beruecksichtigungen?.length === 1 && (
          <p>
            <br />
            {t(`egfa.modules.demografie.zusammenfassung.beruecksichtigungen.singularAreaSelectionLabel`)}
          </p>
        )}
        {beruecksichtigungen?.length > 1 && (
          <p>
            <br />
            {t(`egfa.modules.demografie.zusammenfassung.beruecksichtigungen.severalAreasSelectionsLabel`)}
          </p>
        )}
        {beruecksichtigungen.length > 0 && createList(beruecksichtigungen)}
        <br /> <br />
        {auswirkungen?.length > 0 && (
          <Form.Item
            name="influenceSummary"
            label={<span>{summaryLabel}</span>}
            rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
          >
            <TextArea rows={8} />
          </Form.Item>
        )}
      </FormWrapper>
    </div>
  );
}
