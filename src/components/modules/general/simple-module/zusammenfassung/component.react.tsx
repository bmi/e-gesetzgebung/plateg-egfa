// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleEntityRequestDTO, EgfaModuleStatusType } from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { ZusammenfassungWrapper } from '../../../general/zusammenfassung-wrapper/component.react';
import { ZusammenfassungItem } from '../../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';
import { SimpleModuleProps } from '../auswirkungen/component.react';

interface SimpleModuleZusammenfassungProps extends SimpleModuleProps<EgfaModuleEntityRequestDTO> {
  additionalActions?: React.ReactElement[];
  isLight?: boolean;
  previousLinkName?: string;
  customEditLink?: string;
  customNoImpactText?: string;
  isTitleStretched?: boolean;
}
export function SimpleModuleZusammenfassung(props: SimpleModuleZusammenfassungProps): React.ReactElement {
  const { t } = useTranslation();
  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute: string = routes[routeKey];
  return (
    <ZusammenfassungWrapper
      egfaId={props.egfaId}
      prevPage={
        props.moduleName +
        '/' +
        (props.previousLinkName ??
          (moduleNameRoute === routes.ENAP ? routes.ZUSAMMENFASSUNG : routes.WESENTLICHE_AUSWIRKUNGEN))
      }
      onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
      moduleData={props.formData}
      additionalActions={props.additionalActions}
      moduleNameRoute={moduleNameRoute}
      isLight={props.isLight}
      isTitleStretched={props.isTitleStretched}
    >
      <ZusammenfassungItem
        title={t(`egfa.modules.${props.moduleName}.fertigstellung.subtitle`)}
        bearbeitungsLink={
          props.customEditLink ??
          `/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${
            moduleNameRoute === routes.ENAP ? routes.ZUSAMMENFASSUNG : routes.WESENTLICHE_AUSWIRKUNGEN
          }`
        }
        onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
        egfaId={props.egfaId}
        isLight={props.isLight}
        allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
      >
        <div>
          {props.formData?.influenceExpected === false && (
            <p>{props.customNoImpactText ?? t(`egfa.modules.${props.moduleName}.fertigstellung.noImpact`)}</p>
          )}
          {props.formData?.influenceExpected && (
            <div className="preview-summary">
              <p>{props.formData.influenceSummary}</p>
            </div>
          )}
        </div>
      </ZusammenfassungItem>
    </ZusammenfassungWrapper>
  );
}
