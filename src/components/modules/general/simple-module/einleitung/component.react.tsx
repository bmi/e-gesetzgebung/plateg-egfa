// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH } from '@plateg/rest-api';

import { SimpleModuleEinleitungButtons } from './button-section/component.react';

interface SimpleModuleEinleitungProps {
  moduleName: string;
  nextLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  customTitle?: React.ReactElement;
  customContent?: React.ReactElement;
  customClassName?: string;
}
export function SimpleModuleEinleitung(props: SimpleModuleEinleitungProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const title = props.customTitle ?? (
    <Title level={1}>{t(`egfa.modules.${props.moduleName}.${props.sectionName}.title`)}</Title>
  );
  const content = props.customContent ?? (
    <div
      dangerouslySetInnerHTML={{
        __html: t(`egfa.modules.${props.moduleName}.${props.sectionName}.text`, {
          basePath: BASE_PATH,
        }),
      }}
    />
  );

  // Implement this logic for enabling menu items according to conditions described in your task
  useEffect(() => {
    if (props.updateNextLink && props.nextLinkName) {
      props.updateNextLink(props.nextLinkName, false);
    }
  }, [props.sectionName]);

  return (
    <div className={props.customClassName}>
      {title}
      {content}
      <SimpleModuleEinleitungButtons {...props} />
    </div>
  );
}
