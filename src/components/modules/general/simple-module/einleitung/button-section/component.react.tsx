// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { routes } from '../../../../../../shares/routes';

interface SimpleModuleEinleitungButtonsProps {
  moduleName: string;
  nextLinkName?: string;
}
export function SimpleModuleEinleitungButtons(props: SimpleModuleEinleitungButtonsProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>('/egfa/:id');
  return (
    <div className="form-control-buttons">
      {props.nextLinkName ? (
        <>
          <Button
            id={`egfa-einleitung-${props.nextLinkName}-btn${props.moduleName}`}
            type="primary"
            size={'large'}
            htmlType="button"
            onClick={() => {
              history.push(
                `/egfa/${routeMatcherVorbereitung?.params.id as string}/${routes.MODULE}/${props.moduleName}/${
                  props.nextLinkName as string
                }`,
              );
            }}
          >
            {t(`egfa.modules.general.moduleStarten`)}
          </Button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <Button
            id={`egfa-einleitung-${props.nextLinkName}-cancel-btn`}
            type="default"
            size={'large'}
            htmlType="button"
            onClick={() => {
              history.push(`/egfa/${routeMatcherVorbereitung?.params.id as string}/${routes.MODUL_UEBERSICHT}`);
            }}
          >
            {t(`egfa.modules.general.moduleAbbrechen`)}
          </Button>
        </>
      ) : null}
    </div>
  );
}
