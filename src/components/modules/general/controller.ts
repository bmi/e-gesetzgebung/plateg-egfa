// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import { Observable, of, Subscription, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import {
  DemografieDisabilityReferenzenDTO,
  DemografieEaOehhReferenzenDTO,
  DemografieEnapReferenzenDTO,
  DemografieGleichstellungReferenzenDTO,
  DemografieGleichwertigkeitReferenzenDTO,
  DemografieSonstigeKostenReferenzenDTO,
  DisabilityDemografieReferenzenDTO,
  DisabilityEnapReferenzenDTO,
  DisabilityGleichstellungReferenzenDTO,
  DisabilityGleichwertigkeitReferenzenDTO,
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleStatusType,
  EgfaModuleType,
  EnapDemografieReferenzenDTO,
  EnapDisabilityReferenzenDTO,
  EnapEaOehhReferenzenDTO,
  EnapGleichstellungReferenzenDTO,
  EnapGleichwertigkeitReferenzenDTO,
  EvaluierungEaOehhReferenzenDTO,
  EvaluierungErfuellungsaufwandReferenzenDTO,
  EvaluierungSonstigeKostenReferenzenDTO,
  GleichstellungDemografieReferenzenDTO,
  GleichstellungDisabilityReferenzenDTO,
  GleichstellungEnapReferenzenDTO,
  GleichstellungGleichwertigkeitReferenzenDTO,
  GleichwertigkeitDemografieReferenzenDTO,
  GleichwertigkeitDisabilityReferenzenDTO,
  GleichwertigkeitEaOehhReferenzenDTO,
  GleichwertigkeitEnapReferenzenDTO,
  GleichwertigkeitErfuellungsaufwandReferenzenDTO,
  GleichwertigkeitGleichstellungReferenzenDTO,
  GleichwertigkeitSonstigeKostenReferenzenDTO,
  KmuErfuellungsaufwandReferenzenDTO,
  KmuSonstigeKostenReferenzenDTO,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  VerbraucherEnapReferenzenDTO,
} from '@plateg/rest-api';
import { displayMessage, LoadingStatusController, MessageController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

export interface ModifyModuleDtoResponse<T> extends ReferenzenTypes {
  dto: T;
  regelungsvorhaben: RegelungsvorhabenEntityWithDetailsResponseDTO;
}
export interface ExtendedModuleData {
  referenzenData?: ReferenzenTypes;
}
enum ReferenzenDataEnum {
  enapReferenzen = 'enapReferenzen',

  demografieEnapReferenzen = 'demografieEnapReferenzen',
  demografieGleichstellungReferenzen = 'demografieGleichstellungReferenzen',
  demografieDisabilityeferenzen = 'demografieDisabilityeferenzen',
  demografieGleichwertigkeitReferenzen = 'demografieGleichwertigkeitReferenzen',
  demografieSonstigeKostenReferenzen = 'demografieSonstigeKostenReferenzen',
  demografieEaOehhReferenzen = 'demografieEaOehhReferenzen',

  disabilityDemografieReferenzen = 'disabilityDemografieReferenzen',
  disabilityEnapReferenzen = 'disabilityEnapReferenzen',
  disabilityGleichstellungReferenzen = 'disabilityGleichstellungReferenzen',
  disabilityGleichwertigkeitReferenzen = 'disabilityGleichwertigkeitReferenzen',

  gleichstellungDemografieReferenzen = 'gleichstellungDemografieReferenzen',
  gleichstellungDisabilityReferenzen = 'gleichstellungDisabilityReferenzen',
  gleichstellungEnapReferenzen = 'gleichstellungEnapReferenzen',
  gleichstellungGleichwertigkeitReferenzen = 'gleichstellungGleichwertigkeitReferenzen',

  gleichwertigkeitDemografieReferenzen = 'gleichwertigkeitDemografieReferenzen',
  gleichwertigkeitDisabilityReferenzen = 'gleichwertigkeitDisabilityReferenzen',
  gleichwertigkeitEaOehhReferenzen = 'gleichwertigkeitEaOehhReferenzen',
  gleichwertigkeitEnapReferenzen = 'gleichwertigkeitEnapReferenzen',
  gleichwertigkeitErfuellungsaufwandReferenzen = 'gleichwertigkeitErfuellungsaufwandReferenzen',
  gleichwertigkeitGleichstellungReferenzen = 'gleichwertigkeitGleichstellungReferenzen',
  gleichwertigkeitSonstigeKostenReferenzen = 'gleichwertigkeitSonstigeKostenReferenzen',

  enapDemografieReferenzen = 'enapDemografieReferenzen',
  enapGleichstellungReferenzen = 'enapGleichstellungReferenzen',
  enapGleichwertigkeitReferenzen = 'enapGleichwertigkeitReferenzen',
  enapDisabilityReferenzen = 'enapDisabilityReferenzen',
  enapEaOehhReferenzen = 'enapEaOehhReferenzen',

  kmuErfuellungsaufwandReferenzen = 'kmuErfuellungsaufwandReferenzen',
  kmuSonstigeKostenReferenzen = 'kmuSonstigeKostenReferenzen',

  evaluierungEaOehhReferenzen = 'evaluierungEaOehhReferenzen',
  evaluierungErfuellungsaufwandReferenzen = 'evaluierungErfuellungsaufwandReferenzen',
  evaluierungSonstigeKostenReferenzen = 'evaluierungSonstigeKostenReferenzen',
}
export type ReferenzenDTOTypes =
  | VerbraucherEnapReferenzenDTO
  | DemografieEnapReferenzenDTO
  | DemografieGleichstellungReferenzenDTO
  | DemografieDisabilityReferenzenDTO
  | DemografieGleichwertigkeitReferenzenDTO
  | DemografieSonstigeKostenReferenzenDTO
  | DemografieEaOehhReferenzenDTO
  | DisabilityDemografieReferenzenDTO
  | DisabilityEnapReferenzenDTO
  | DisabilityGleichstellungReferenzenDTO
  | DisabilityGleichwertigkeitReferenzenDTO
  | GleichstellungDemografieReferenzenDTO
  | GleichstellungDisabilityReferenzenDTO
  | GleichstellungEnapReferenzenDTO
  | GleichstellungGleichwertigkeitReferenzenDTO
  | GleichwertigkeitDemografieReferenzenDTO
  | GleichwertigkeitDisabilityReferenzenDTO
  | GleichwertigkeitEaOehhReferenzenDTO
  | GleichwertigkeitEnapReferenzenDTO
  | GleichwertigkeitErfuellungsaufwandReferenzenDTO
  | GleichwertigkeitGleichstellungReferenzenDTO
  | GleichwertigkeitSonstigeKostenReferenzenDTO
  | EnapDemografieReferenzenDTO
  | EnapGleichstellungReferenzenDTO
  | EnapGleichwertigkeitReferenzenDTO
  | EnapDisabilityReferenzenDTO
  | EnapEaOehhReferenzenDTO
  | KmuErfuellungsaufwandReferenzenDTO
  | KmuSonstigeKostenReferenzenDTO
  | EvaluierungEaOehhReferenzenDTO
  | EvaluierungErfuellungsaufwandReferenzenDTO
  | EvaluierungSonstigeKostenReferenzenDTO;

export type ReferenzenTypes = {
  [key in ReferenzenDataEnum]?: ReferenzenDTOTypes;
};

export class EGFAModuleController<T extends EgfaModuleEntityRequestDTO, P extends ModifyModuleDtoResponse<T>> {
  private readonly getData: () => Observable<P>;
  private readonly putData: (data: T) => Observable<P>;
  private readonly setData: (newData: T & ExtendedModuleData) => void;
  private readonly generateJson: (data: T) => EgfaDatenuebernahmeDTO;
  private readonly setRvFull: (rvFull: RegelungsvorhabenEntityWithDetailsResponseDTO) => void;
  private readonly setIsModuleClosed: (isClosed: boolean) => void;
  private readonly shouldUpdate: React.MutableRefObject<boolean>;
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private readonly messageController = GlobalDI.getOrRegister('messageController', () => new MessageController());
  private readonly egfaController = GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
  private readonly isLight?: boolean;
  public constructor(
    getData: () => Observable<P>,
    putData: (data: T) => Observable<P>,
    setData: (newData: T & ExtendedModuleData) => void,
    setRvFull: (rvFull: RegelungsvorhabenEntityWithDetailsResponseDTO) => void,
    setIsModuleClosed: (isClosed: boolean) => void,
    shouldUpdate: React.MutableRefObject<boolean>,
    generateJson: (data: T) => EgfaDatenuebernahmeDTO,
    isLight?: boolean,
  ) {
    this.getData = getData;
    this.putData = putData;
    this.setData = setData;
    this.setRvFull = setRvFull;
    this.setIsModuleClosed = setIsModuleClosed;
    this.shouldUpdate = shouldUpdate;
    this.generateJson = generateJson;
    this.isLight = isLight;
  }

  public getInitialData(): Subscription {
    this.loadingStatusController.setLoadingStatus(true);
    return this.getData().subscribe({
      next: (data) => {
        if (this.shouldUpdate?.current) {
          this.setRvFull(data.regelungsvorhaben);
          this.setIsModuleClosed(data.dto?.status === EgfaModuleStatusType.VollstaendigBearbeitet || false);
          this.setData({
            ...data.dto,
            referenzenData: this.prepareReferenzenData(data),
          });
        }
        this.loadingStatusController.setLoadingStatus(false);
      },
      error: (error: string) => {
        this.loadingStatusController.setLoadingStatus(false);
        this.messageController.displayMessage(`Daten konnten nicht vom Server geladen werden: ${error}`, 'error');
      },
    });
  }

  public updateData(
    localData: T,
    isDraftSaving = false,
    setIsSaveButtonDisabled?: (status: boolean) => void,
    successCallback?: () => void,
  ): void {
    if (!isDraftSaving) {
      this.loadingStatusController.setLoadingStatus(true);
    }
    this.getData().subscribe({
      next: (serverData: P) =>
        this.saveMergedData(localData, serverData.dto, isDraftSaving, setIsSaveButtonDisabled, successCallback),
      error: (error: string) => {
        this.loadingStatusController.setLoadingStatus(false);
        this.messageController.displayMessage(`Daten konnten nicht vom Server geladen werden: ${error}`, 'error');
      },
    });
  }

  public onSave = (
    form?: FormInstance,
    status?: EgfaModuleStatusType,
    isDraftSaving = false,
    setIsSaveButtonDisabled?: (status: boolean) => void,
  ): (() => void) => {
    return (successCallback?: () => void) => {
      const fieldValues = form ? (form.getFieldsValue(true) as T) : ({} as T);
      fieldValues.status = status || EgfaModuleStatusType.InBearbeitung;
      this.updateData(fieldValues, isDraftSaving, setIsSaveButtonDisabled, successCallback);
    };
  };
  /**
   * Modifies the data using the provided observable and sends the modified data to the editor.
   *
   * @param modifyObservable - The observable representing the data modification operation.
   * @param egfaId - The identifier associated with the data.
   * @param moduleType - The type of the EGFA module.
   * @param jsonGenerator - A function that generates the JSON payload for sending to the editor based on the modified data.
   * @returns An observable that completes after attempting to send the modified data to the editor.
   *          If an error occurs during the sending process, it provides an opportunity to customize
   *          error messages to distinguish between errors during the data saving and sending to the editor.
   */
  private modifyAndSendToEditor(
    modifyObservable: Observable<P>,
    egfaId: string,
    moduleType: EgfaModuleType,
    jsonGenerator: (data: T) => EgfaDatenuebernahmeDTO,
  ) {
    return modifyObservable.pipe(
      switchMap((data) =>
        this.egfaController
          .sendEgfaDatenuebernahme({
            id: egfaId,
            moduleType: moduleType,
            egfaDatenuebernahmeDTO: jsonGenerator(data.dto),
          })
          .pipe(
            catchError((error: Error) => {
              // This section is designed for customizing error messages to distinguish between errors
              // occurring during the data saving process and those arising during the data sending
              // to the editor. It helps provide more specific error feedback based on the operation
              // that encountered an issue - whether it's the local data modification or the subsequent
              // transfer to the editor.
              return of(data);
            }),
          ),
      ),
      catchError((error: Error) => {
        return throwError(() => error);
      }),
    );
  }
  private saveMergedData(
    localData: T,
    serverData: T,
    isDraftSaving = false,
    setIsSaveButtonDisabled?: (status: boolean) => void,
    successCallback?: () => void,
  ): void {
    const mergedData = { ...serverData, ...localData };
    if (this.shouldUpdate?.current) {
      this.setData(mergedData);
      this.setIsModuleClosed(mergedData.status === EgfaModuleStatusType.VollstaendigBearbeitet || false);
    }
    let observer = this.putData(mergedData);
    if (mergedData.status === EgfaModuleStatusType.VollstaendigBearbeitet && mergedData.egfaId && !this.isLight) {
      observer = this.modifyAndSendToEditor(observer, mergedData.egfaId, mergedData.typ, this.generateJson);
    }
    observer.subscribe({
      next: (data) => {
        this.loadingStatusController.setLoadingStatus(false);
        if (isDraftSaving && setIsSaveButtonDisabled) {
          this.messageController.displayMessage('Das Modul wurde erfolgreich gespeichert.', 'success');
          setIsSaveButtonDisabled(true);
        }
        if (this.shouldUpdate?.current && data) {
          this.setData(data.dto);
        }
        if (typeof successCallback === 'function') {
          successCallback();
        }
      },
      error: (error: string) => {
        this.loadingStatusController.setLoadingStatus(false);
        displayMessage(`Eingaben konnten nicht gespeichert werden: ${error}`, error);
      },
    });
  }
  private prepareReferenzenData(data: P): ReferenzenTypes {
    const referenzenData: ReferenzenTypes = {};
    Object.keys(data).forEach((key) => {
      if (Object.keys(ReferenzenDataEnum).some((el) => key == el)) {
        referenzenData[key as ReferenzenDataEnum] = data[key as ReferenzenDataEnum];
      }
    });
    return referenzenData;
  }
}
