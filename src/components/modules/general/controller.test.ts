// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import { EgfaModuleStatusType } from '@plateg/rest-api';

import { loadingStatusSetStub, messageControllerDisplayStub } from '../../../general.test';
import { EGFAModuleController } from './controller';

describe('eGFAModuleController testen', () => {
  describe(`Erfolgsfälle mit shouldupdate=true`, () => {
    const getDataStub = sinon.stub().returns(
      of({
        dto: { influenceExpected: false },
        regelungsvorhaben: { dto: { abkuerzung: '123' } },
        enapReferenzen: {
          rolleType: 'KEINE',
          status: null,
          sdg12AnderweitigRelevantRv: null,
        },
        demografieEnapReferenzen: {
          rolleType: 'KEINE',
          status: null,
          hasSelectedCheckboxSdg4: null,
        },
        demografieGleichstellungReferenzen: {
          rolleType: 'KEINE',
          status: null,
          hasSelectedCheckboxArbeit: false,
          hasSelectedCheckboxZeit: false,
        },
        demografieGleichwertigkeitReferenzen: {
          rolleType: 'KEINE',
          status: null,
          hasSelectedCheckbox: false,
        },
        gleichwertigkeitDemografieReferenzen: {
          rolleType: 'KEINE',
          hasSelectedCheckbox: false,
          status: null,
        },
        gleichwertigkeitEaOehhReferenzen: {
          rolleType: 'KEINE',
          status: null,
        },
        gleichwertigkeitErfuellungsaufwandReferenzen: {
          rolleType: 'KEINE',
          status: null,
        },
        gleichwertigkeitSonstigeKostenReferenzen: {
          hasSelectedInfluenceExpected: false,
          rolleType: 'KEINE',
          status: null,
        },
      }),
    );
    const putDataStub = sinon.stub().returns(of({}));
    const setDataStub = sinon.stub();
    const setRvFull = sinon.stub();
    const setIsModuleClosedStub = sinon.stub();
    const shouldUpdateStub: React.MutableRefObject<boolean> = { current: true } as React.MutableRefObject<boolean>;
    const generateJsonStub = sinon.stub();
    const ctrl = new EGFAModuleController(
      getDataStub,
      putDataStub,
      setDataStub,
      setRvFull,
      setIsModuleClosedStub,
      shouldUpdateStub,
      generateJsonStub,
    );
    afterEach(function () {
      loadingStatusSetStub.resetHistory();
      setDataStub.resetHistory();
      setRvFull.resetHistory();
      putDataStub.resetHistory();
      setIsModuleClosedStub.resetHistory();
      generateJsonStub.resetHistory();
    });
    it('getInitialData testen', function (done) {
      ctrl.getInitialData();
      setTimeout(() => {
        sinon.assert.called(loadingStatusSetStub);
        sinon.assert.calledOnce(setDataStub);
        sinon.assert.calledWith(setDataStub, {
          influenceExpected: false,
          referenzenData: {
            enapReferenzen: {
              rolleType: 'KEINE',
              status: null,
              sdg12AnderweitigRelevantRv: null,
            },
            demografieEnapReferenzen: {
              rolleType: 'KEINE',
              status: null,
              hasSelectedCheckboxSdg4: null,
            },
            demografieGleichstellungReferenzen: {
              rolleType: 'KEINE',
              status: null,
              hasSelectedCheckboxArbeit: false,
              hasSelectedCheckboxZeit: false,
            },
            demografieGleichwertigkeitReferenzen: {
              rolleType: 'KEINE',
              status: null,
              hasSelectedCheckbox: false,
            },
            gleichwertigkeitDemografieReferenzen: {
              rolleType: 'KEINE',
              hasSelectedCheckbox: false,
              status: null,
            },
            gleichwertigkeitEaOehhReferenzen: {
              rolleType: 'KEINE',
              status: null,
            },
            gleichwertigkeitErfuellungsaufwandReferenzen: {
              rolleType: 'KEINE',
              status: null,
            },
            gleichwertigkeitSonstigeKostenReferenzen: {
              hasSelectedInfluenceExpected: false,
              rolleType: 'KEINE',
              status: null,
            },
          },
        });
        sinon.assert.calledOnce(setRvFull);
        sinon.assert.calledOnce(setIsModuleClosedStub);
        sinon.assert.calledWith(setIsModuleClosedStub, false);
        done();
      }, 20);
    });
    it('updateData testen', function (done) {
      const localData = { influenceExpected: true };
      ctrl.updateData(localData);
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.calledTwice(setDataStub);
        sinon.assert.calledWith(setDataStub, { influenceExpected: true });
        sinon.assert.calledOnce(setIsModuleClosedStub);
        sinon.assert.calledWith(setIsModuleClosedStub, false);
        sinon.assert.calledOnce(putDataStub);
        sinon.assert.calledWith(putDataStub, { influenceExpected: true });

        done();
      }, 20);
    });
    it('onSave testen', function (done) {
      // onSave returns functions that calls updateData
      ctrl.onSave(undefined, EgfaModuleStatusType.NichtBearbeitet)();
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.calledTwice(setDataStub);
        sinon.assert.calledWith(setDataStub, {
          influenceExpected: false,
          status: EgfaModuleStatusType.NichtBearbeitet,
        });
        sinon.assert.calledOnce(setIsModuleClosedStub);
        sinon.assert.calledWith(setIsModuleClosedStub, false);
        sinon.assert.calledOnce(putDataStub);
        sinon.assert.calledWith(putDataStub, {
          influenceExpected: false,
          status: EgfaModuleStatusType.NichtBearbeitet,
        });
        done();
      }, 20);
    });
    it('onSave testen mit undefined Input', function (done) {
      // onSave returns functions that calls updateData
      ctrl.onSave(undefined, undefined)();
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.calledTwice(setDataStub);
        sinon.assert.calledWith(setDataStub, { influenceExpected: false, status: EgfaModuleStatusType.InBearbeitung });
        sinon.assert.calledOnce(setIsModuleClosedStub);
        sinon.assert.calledWith(setIsModuleClosedStub, false);
        sinon.assert.calledOnce(putDataStub);
        sinon.assert.calledWith(putDataStub, { influenceExpected: false, status: EgfaModuleStatusType.InBearbeitung });
        done();
      }, 20);
    });
  });

  describe(`Erfolgsfälle mit shouldupdate=false`, () => {
    const getDataStub = sinon
      .stub()
      .returns(of({ dto: { influenceExpected: false }, regelungsvorhaben: { dto: { abkuerzung: '123' } } }));
    const putDataStub = sinon.stub().returns(of({}));
    const setDataStub = sinon.stub();
    const setRvNameStub = sinon.stub();
    const setIsModuleClosedStub = sinon.stub();
    const shouldUpdateStub: React.MutableRefObject<boolean> = { current: false } as React.MutableRefObject<boolean>;
    const generateJsonStub = sinon.stub();
    const ctrl = new EGFAModuleController(
      getDataStub,
      putDataStub,
      setDataStub,
      setRvNameStub,
      setIsModuleClosedStub,
      shouldUpdateStub,
      generateJsonStub,
    );
    afterEach(() => {
      loadingStatusSetStub.resetHistory();
      setDataStub.resetHistory();
      setRvNameStub.resetHistory();
      putDataStub.resetHistory();
      setIsModuleClosedStub.resetHistory();
      generateJsonStub.resetHistory();
    });
    it('getInitialData testen', function (done) {
      ctrl.getInitialData();
      setTimeout(() => {
        sinon.assert.called(loadingStatusSetStub);
        sinon.assert.notCalled(setDataStub);
        sinon.assert.notCalled(setRvNameStub);
        sinon.assert.notCalled(setIsModuleClosedStub);
        done();
      }, 20);
    });
    it('updateData testen', function (done) {
      const localData = { influenceExpected: true };
      ctrl.updateData(localData);
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.notCalled(setDataStub);
        sinon.assert.notCalled(setIsModuleClosedStub);
        sinon.assert.calledOnce(putDataStub);
        sinon.assert.calledWith(putDataStub, { influenceExpected: true });
        done();
      }, 20);
    });
  });

  describe('Fehler in getData', () => {
    const getDataStub = sinon.stub().returns(throwError({ status: 404 }));
    const putDataStub = sinon.stub();
    const setDataStub = sinon.stub();
    const setRvNameStub = sinon.stub();
    const setIsModuleClosedStub = sinon.stub();
    const shouldUpdateStub: React.MutableRefObject<boolean> = {} as React.MutableRefObject<boolean>;
    const generateJsonStub = sinon.stub();
    const ctrl = new EGFAModuleController(
      getDataStub,
      putDataStub,
      setDataStub,
      setRvNameStub,
      setIsModuleClosedStub,
      shouldUpdateStub,
      generateJsonStub,
    );
    const setIsSaveButtonDisabledStub = sinon.stub();
    afterEach(() => {
      messageControllerDisplayStub.resetHistory();
      loadingStatusSetStub.resetHistory();
      getDataStub.resetHistory();
      putDataStub.resetHistory();
      setDataStub.resetHistory();
      setRvNameStub.resetHistory();
      setIsModuleClosedStub.resetHistory();
      setIsSaveButtonDisabledStub.resetHistory();
      generateJsonStub.resetHistory();
    });
    it('getInitialData testen', function (done) {
      ctrl.getInitialData();
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.notCalled(setDataStub);
        sinon.assert.notCalled(setRvNameStub);
        sinon.assert.notCalled(setIsModuleClosedStub);
        sinon.assert.calledOnce(messageControllerDisplayStub);
        done();
      }, 20);
    });
    it('updateData testen', function (done) {
      const localData = { influenceExpected: true };
      ctrl.updateData(localData);
      setTimeout(() => {
        sinon.assert.calledTwice(loadingStatusSetStub);
        sinon.assert.notCalled(setDataStub);
        sinon.assert.notCalled(setIsModuleClosedStub);
        sinon.assert.notCalled(putDataStub);
        sinon.assert.notCalled(setIsSaveButtonDisabledStub);
        done();
      }, 20);
    });
  });

  describe('Fehler in putData', () => {
    const getDataStub = sinon
      .stub()
      .returns(of({ dto: { influenceExpected: false }, regelungsvorhaben: { dto: { abkuerzung: '123' } } }));
    const testData = {};
    const putDataStub = sinon.stub().returns(of(testData));
    const setDataStub = sinon.stub();
    const setRvNameStub = sinon.stub();
    const setIsModuleClosedStub = sinon.stub();
    const shouldUpdateStub: React.MutableRefObject<boolean> = {} as React.MutableRefObject<boolean>;
    const generateJsonStub = sinon.stub();
    const ctrl = new EGFAModuleController(
      getDataStub,
      putDataStub,
      setDataStub,
      setRvNameStub,
      setIsModuleClosedStub,
      shouldUpdateStub,
      generateJsonStub,
    );
    const setIsSaveButtonDisabledStub = sinon.stub();

    afterEach(() => {
      loadingStatusSetStub.resetHistory();
      getDataStub.resetHistory();
      putDataStub.resetHistory();
      setDataStub.resetHistory();
      setRvNameStub.resetHistory();
      setIsModuleClosedStub.resetHistory();
      setIsSaveButtonDisabledStub.resetHistory();
      messageControllerDisplayStub.resetHistory();
      generateJsonStub.resetHistory();
    });
    it('updateData testen', function (done) {
      const localData = { influenceExpected: false };
      const isDraftSavingTest = true;
      ctrl.updateData(localData, isDraftSavingTest, setIsSaveButtonDisabledStub);
      setTimeout(() => {
        sinon.assert.calledOnce(loadingStatusSetStub);
        sinon.assert.notCalled(setDataStub);
        sinon.assert.notCalled(setIsModuleClosedStub);
        sinon.assert.calledOnce(putDataStub);
        sinon.assert.calledWith(putDataStub, { influenceExpected: false });
        sinon.assert.calledWith(setIsSaveButtonDisabledStub, true);
        sinon.assert.calledOnce(messageControllerDisplayStub);
        done();
      }, 20);
    });
  });
});
