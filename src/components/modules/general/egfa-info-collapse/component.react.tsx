// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './egfa-info-collapse.less';

import { Collapse, CollapseProps } from 'antd';
import React from 'react';

import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';

export interface EgfaInfoCollapseProps {
  headerText: string;
  text: string | React.ReactElement;
  panelKey: string;
  collapseKey?: string;
}
export function EgfaInfoCollapse(props: EgfaInfoCollapseProps): React.ReactElement {
  const content =
    typeof props.text === 'string' ? (
      <div
        dangerouslySetInnerHTML={{
          __html: props.text,
        }}
      />
    ) : (
      props.text
    );

  const itemsEgfaCollapse: CollapseProps['items'] = [
    {
      key: props.panelKey,
      label: props.headerText,
      children: content,
    },
  ];

  return (
    <Collapse
      items={itemsEgfaCollapse}
      key={props.collapseKey}
      bordered={false}
      className="egfa-info-collapse"
      expandIcon={({ isActive }) => (
        <DirectionRightOutlinedNew
          style={{ width: 22, height: 22, marginTop: -3 }}
          className={isActive ? 'svg-transition svg-rotate-down' : 'svg-transition'}
        />
      )}
    />
  );
}
