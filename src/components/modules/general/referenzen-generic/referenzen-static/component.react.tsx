// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

interface ReferenzenStaticComponentProps {
  link: string;
  moduleName: string;
  translationKey: string;
  link2?: string;
}
export function ReferenzenStaticComponent({
  link,
  moduleName,
  translationKey,
  link2,
}: ReferenzenStaticComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.referenzenText.${moduleName}.${translationKey}`, {
            link: link,
            link2: link2 || '',
          }),
        }}
      />
    </>
  );
}
