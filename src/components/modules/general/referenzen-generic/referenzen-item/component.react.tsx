// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  DemografieDisabilityReferenzenDTO,
  DemografieEaOehhReferenzenDTO,
  DemografieEnapReferenzenDTO,
  DemografieGleichstellungReferenzenDTO,
  DemografieGleichwertigkeitReferenzenDTO,
  DemografieSonstigeKostenReferenzenDTO,
  DisabilityDemografieReferenzenDTO,
  DisabilityEnapReferenzenDTO,
  DisabilityGleichstellungReferenzenDTO,
  DisabilityGleichwertigkeitReferenzenDTO,
  EgfaModuleStatusType,
  EnapDemografieReferenzenDTO,
  GleichstellungDemografieReferenzenDTO,
  GleichstellungDisabilityReferenzenDTO,
  GleichstellungEnapReferenzenDTO,
  GleichstellungGleichwertigkeitReferenzenDTO,
  GleichwertigkeitDemografieReferenzenDTO,
  GleichwertigkeitDisabilityReferenzenDTO,
  GleichwertigkeitEaOehhReferenzenDTO,
  GleichwertigkeitEnapReferenzenDTO,
  GleichwertigkeitErfuellungsaufwandReferenzenDTO,
  GleichwertigkeitGleichstellungReferenzenDTO,
  GleichwertigkeitSonstigeKostenReferenzenDTO,
  KmuErfuellungsaufwandReferenzenDTO,
  KmuSonstigeKostenReferenzenDTO,
  RolleLokalType,
  UserEntityDTO,
  VerbraucherEnapReferenzenDTO,
} from '@plateg/rest-api';

export type ReferenzenDataTypes = DemografieGleichwertigkeitReferenzenDTO &
  DemografieEnapReferenzenDTO &
  DemografieGleichstellungReferenzenDTO &
  DemografieSonstigeKostenReferenzenDTO &
  DemografieDisabilityReferenzenDTO &
  VerbraucherEnapReferenzenDTO &
  DemografieEaOehhReferenzenDTO &
  DisabilityDemografieReferenzenDTO &
  DisabilityEnapReferenzenDTO &
  DisabilityGleichstellungReferenzenDTO &
  DisabilityGleichwertigkeitReferenzenDTO &
  GleichstellungDemografieReferenzenDTO &
  GleichstellungDisabilityReferenzenDTO &
  GleichstellungEnapReferenzenDTO &
  GleichstellungGleichwertigkeitReferenzenDTO &
  GleichwertigkeitDemografieReferenzenDTO &
  GleichwertigkeitDisabilityReferenzenDTO &
  GleichwertigkeitEaOehhReferenzenDTO &
  GleichwertigkeitEnapReferenzenDTO &
  GleichwertigkeitErfuellungsaufwandReferenzenDTO &
  GleichwertigkeitGleichstellungReferenzenDTO &
  GleichwertigkeitSonstigeKostenReferenzenDTO &
  EnapDemografieReferenzenDTO &
  KmuErfuellungsaufwandReferenzenDTO &
  KmuSonstigeKostenReferenzenDTO;

export enum EnapReferenzenTypes {
  hasSelectedCheckboxSdg1 = 'hasSelectedCheckboxSdg1',
  hasSelectedCheckboxSdg31 = 'hasSelectedCheckboxSdg31',
  hasSelectedCheckboxSdg32 = 'hasSelectedCheckboxSdg32',
  hasSelectedCheckboxSdg4 = 'hasSelectedCheckboxSdg4',
  hasSelectedCheckboxSdg41 = 'hasSelectedCheckboxSdg41',
  hasSelectedCheckboxSdg42 = 'hasSelectedCheckboxSdg42',
  hasSelectedCheckboxSdg5 = 'hasSelectedCheckboxSdg5',
  hasSelectedCheckboxSdg82 = 'hasSelectedCheckboxSdg82',
  hasSelectedCheckboxSdg83 = 'hasSelectedCheckboxSdg83',
  hasSelectedCheckboxSdg85 = 'hasSelectedCheckboxSdg85',
  hasSelectedCheckboxSdg9 = 'hasSelectedCheckboxSdg9',
  hasSelectedCheckboxSdg91b = 'hasSelectedCheckboxSdg91b',
  hasSelectedCheckboxSdg10 = 'hasSelectedCheckboxSdg10',
  hasSelectedCheckboxSdg11 = 'hasSelectedCheckboxSdg11',
  hasSelectedCheckboxSdg101 = 'hasSelectedCheckboxSdg101',
  hasSelectedCheckboxSdg111 = 'hasSelectedCheckboxSdg111',
  hasSelectedCheckboxSdg112 = 'hasSelectedCheckboxSdg112',
  hasSelectedCheckboxSdg113 = 'hasSelectedCheckboxSdg113',
  hasSelectedCheckboxSdg141 = 'hasSelectedCheckboxSdg141',
  hasSelectedCheckboxSdg161 = 'hasSelectedCheckboxSdg161',
  hasSelectedCheckboxSdg16 = 'hasSelectedCheckboxSdg16',
}
export enum GleichwertigkeitReferenzenTypes {
  hasSelectedCheckbox = 'hasSelectedCheckbox',
  hasSelectedEngagement = 'hasSelectedEngagement',
  hasSelectedWohnraum = 'hasSelectedWohnraum',
  hasSelectedDaseinsvorsoge = 'hasSelectedDaseinsvorsoge',
  hasSelectedDaseinsvorsorge = 'hasSelectedDaseinsvorsorge',
  hasSelectedMobilitaet = 'hasSelectedMobilitaet',
  hasSelectedWirtschaft = 'hasSelectedWirtschaft',
  hasSelectedLebensgrundlage = 'hasSelectedLebensgrundlage',
}

export enum GleichstellungReferenzenTypes {
  hasSelectedCheckboxArbeit = 'hasSelectedCheckboxArbeit',
  hasSelectedCheckboxWissen = 'hasSelectedCheckboxWissen',
  hasSelectedCheckboxZeit = 'hasSelectedCheckboxZeit',
  hasSelectedExplicitGoal = 'hasSelectedExplicitGoal',
  hasSelectedMenAndWomanInfluenced = 'hasSelectedMenAndWomanInfluenced',
  hasSelectedZeit = 'hasSelectedZeit',
  hasSelectedGeld = 'hasSelectedGeld',
  hasSelectedArbeit = 'hasSelectedArbeit',
  hasSelectedGesundheit = 'hasSelectedGesundheit',
  hasSelectedEntscheidungsmacht = 'hasSelectedEntscheidungsmacht',
  hasSelectedGewalt = 'hasSelectedGewalt',
}
export enum SonstigeKostenReferenzenTypes {
  hasSelectedInfluenceExpected = 'hasSelectedInfluenceExpected',
}

export enum DisabilityReferenzenTypes {
  hasSelectedCheckboxTeilhabe = 'hasSelectedCheckboxTeilhabe',
  hasSelectedCheckboxRelevanz = 'hasSelectedCheckboxRelevanz',
  hasSelectedCheckboxHauptpruefung2 = 'hasSelectedCheckboxHauptpruefung2',
  hasSelectedCheckboxHauptpruefung3 = 'hasSelectedCheckboxHauptpruefung3',
  hasSelectedCheckbox = 'hasSelectedCheckbox',
  hasSelectedCheckboxZugang = 'hasSelectedCheckboxZugang',
}

export enum VerbraucherEnapReferenzenTypes {
  hasSelectedCheckboxSdg121 = 'hasSelectedCheckboxSdg121',
}

export enum DemografieReferenzenTypes {
  hasSelectedArbeit = 'hasSelectedArbeit',
  hasSelectedCheckbox = 'hasSelectedCheckbox',
  hasSelectedFamilie = 'hasSelectedFamilie',
  hasSelectedZusammenhalt = 'hasSelectedZusammenhalt',
  hasSelectedAlter = 'hasSelectedAlter',
}

export enum KmuReferenzenTypes {
  moreThanOneMioEurWirtschaft = 'moreThanOneMioEurWirtschaft',
  moreThanHundredThousandHrsZeitaufwandBuerger = 'moreThanHundredThousandHrsZeitaufwandBuerger',
  moreThanOneMioEurSachaufwandBuerger = 'moreThanOneMioEurSachaufwandBuerger',
  moreThanOneMioEurVerwaltung = 'moreThanOneMioEurVerwaltung',
}

export type ExtendedConditionsTypes =
  | EnapReferenzenTypes
  | GleichwertigkeitReferenzenTypes
  | GleichstellungReferenzenTypes
  | SonstigeKostenReferenzenTypes
  | DisabilityReferenzenTypes
  | VerbraucherEnapReferenzenTypes
  | DemografieReferenzenTypes
  | KmuReferenzenTypes;

export interface ReferenzenTextComponentProps {
  referenzenData?: ReferenzenDataTypes;
  conditionsList?: ExtendedConditionsTypes[];
  egfaLink: string;
  moduleName: string;
  egfaErsteller?: UserEntityDTO;
  extendedInfo?: boolean;
}
export function ReferenzenItemComponent({
  referenzenData,
  conditionsList,
  egfaLink,
  moduleName,
  egfaErsteller,
  extendedInfo,
}: ReferenzenTextComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const [hasRights, setHasRights] = useState(false);
  const [mainCondition, setMainCondition] = useState(false);
  const [conditionsValues, setConditionsValues] = useState<ExtendedConditionsTypes[]>([]);
  const moduleLink = `${egfaLink}/${moduleName}`;
  useEffect(() => {
    if (referenzenData) {
      // Prepare checking reading rights
      setHasRights(referenzenData?.rolleType !== RolleLokalType.Keine);
      // Checking main conditions to show or now referenzen info section
      setMainCondition(referenzenData?.status === EgfaModuleStatusType.VollstaendigBearbeitet);
      if (conditionsList !== undefined) {
        setMainCondition(
          (previousMainCondition) =>
            // If it has list of addititional condtitions -> check it and combine with mainCondition
            previousMainCondition && conditionsList.some((condition) => referenzenData[condition]),
        );
        setConditionsValues(conditionsList.filter((condition) => referenzenData[condition]));
      }
    }
  }, [referenzenData]);
  return (
    <>
      {mainCondition && (
        <>
          {extendedInfo && (
            <>
              <p
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.referenzenText.extendedInfoText`, {
                    moduleName: t(`egfa.modules.referenzenText.${moduleName}.moduleTitle`),
                  }),
                }}
              />
              {conditionsValues && (
                <ul>
                  {conditionsValues.map((item, index) => (
                    <li key={index}>{t(`egfa.modules.referenzenText.${moduleName}.${item as string}`)}</li>
                  ))}
                </ul>
              )}
            </>
          )}
          <p
            dangerouslySetInnerHTML={{
              __html: t(
                `egfa.modules.referenzenText.${extendedInfo ? 'extendedText.' : ''}${
                  hasRights ? 'hasRights' : 'noRights'
                }`,
                {
                  moduleLink: moduleLink,
                  moduleName: t(`egfa.modules.referenzenText.${moduleName}.moduleTitle`),
                  verantwortlichenPersonEmail: egfaErsteller?.email,
                  verantwortlichenPersonName: egfaErsteller?.name,
                },
              ),
            }}
          />
        </>
      )}
    </>
  );
}
