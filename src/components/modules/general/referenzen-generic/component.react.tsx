// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { Fragment } from 'react';

import { UserEntityResponseDTO } from '@plateg/rest-api';

import { ReferenzenItemCustomComponent } from './referenzen-item-custom/component.react';
import {
  ExtendedConditionsTypes,
  ReferenzenDataTypes,
  ReferenzenItemComponent,
} from './referenzen-item/component.react';
import { ReferenzenNotFertiggestelltComponent } from './referenzen-not-fertiggestellt/component.react';

interface ReferenzenGenericComponentProps {
  egfaLink: string;
  egfaErsteller?: UserEntityResponseDTO;
  referenzenDataConfig: ReferenzenDataConfigInterface[];
}
interface ReferenzenDataConfigInterface {
  referenzenData?: ReferenzenDataTypes;
  conditionsList?: ExtendedConditionsTypes[];
  moduleName: string;
  extendedInfo?: boolean;
  checkIsNotFertiggestellt?: boolean;
  currentModuleName?: string;
  checkTwoCasesCondition?: boolean;
}
export function ReferenzenGenericComponent({
  egfaLink,
  egfaErsteller,
  referenzenDataConfig,
}: ReferenzenGenericComponentProps): React.ReactElement {
  return (
    <>
      {referenzenDataConfig.map((configItem, index) => {
        return (
          <Fragment key={index}>
            {configItem.checkTwoCasesCondition ? (
              <ReferenzenItemCustomComponent egfaLink={egfaLink} egfaErsteller={egfaErsteller?.dto} {...configItem} />
            ) : (
              <ReferenzenItemComponent egfaLink={egfaLink} egfaErsteller={egfaErsteller?.dto} {...configItem} />
            )}

            {configItem.checkIsNotFertiggestellt && (
              <ReferenzenNotFertiggestelltComponent
                egfaLink={egfaLink}
                egfaErsteller={egfaErsteller?.dto}
                {...configItem}
              />
            )}
          </Fragment>
        );
      })}
    </>
  );
}
