// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleStatusType, RolleLokalType } from '@plateg/rest-api';

import { ReferenzenTextComponentProps } from '../referenzen-item/component.react';

interface ReferenzenTextCustomComponentProps extends ReferenzenTextComponentProps {
  currentModuleName?: string;
}

export function ReferenzenItemCustomComponent({
  referenzenData,
  conditionsList,
  egfaLink,
  moduleName,
  currentModuleName,
  egfaErsteller,
}: ReferenzenTextCustomComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const [hasRights, setHasRights] = useState(false);
  const [mainCondition, setMainCondition] = useState(false);
  const [isConditionSatisfies, setIsConditionSatisfies] = useState(false);
  const moduleLink = `${egfaLink}/${moduleName}`;
  useEffect(() => {
    if (referenzenData && conditionsList) {
      // Prepare checking reading rights
      setHasRights(referenzenData?.rolleType !== RolleLokalType.Keine);
      // Checking main conditions to show or now referenzen info section
      setMainCondition(referenzenData?.status === EgfaModuleStatusType.VollstaendigBearbeitet);
      setIsConditionSatisfies(conditionsList.some((condition) => referenzenData[condition]));
    }
  }, [referenzenData]);

  return (
    <>
      {mainCondition && currentModuleName && (
        <>
          {conditionsList &&
            [conditionsList.join('')].map((item) => (
              <p
                dangerouslySetInnerHTML={{
                  __html: t(
                    `egfa.modules.referenzenText.${currentModuleName}.${moduleName}.${item}.${
                      isConditionSatisfies ? 'satisfies' : 'notsatisfies'
                    }.${hasRights ? 'hasRights' : 'noRights'}`,
                    {
                      moduleLink: moduleLink,
                      moduleName: t(`egfa.modules.referenzenText.${moduleName}.moduleTitle`),
                      currentModuleName: t(`egfa.modules.referenzenText.${currentModuleName}.moduleTitle`),
                    },
                  ),
                }}
              />
            ))}
          {!hasRights && (
            <p
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.referenzenText.requestRights`, {
                  verantwortlichenPersonEmail: egfaErsteller?.email,
                  verantwortlichenPersonName: egfaErsteller?.name,
                }),
              }}
            />
          )}
        </>
      )}
    </>
  );
}
