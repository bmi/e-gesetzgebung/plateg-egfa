// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleStatusType, RolleLokalType } from '@plateg/rest-api';

import { ReferenzenTextComponentProps } from '../referenzen-item/component.react';

interface ReferenzenNotFertiggestelltComponentProps extends ReferenzenTextComponentProps {
  currentModuleName?: string;
}
export function ReferenzenNotFertiggestelltComponent({
  referenzenData,
  egfaLink,
  moduleName,
  egfaErsteller,
  currentModuleName,
  conditionsList,
}: ReferenzenNotFertiggestelltComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const [hasRights, setHasRights] = useState(false);
  const [isNotFertiggestellt, setIsNotFertiggestellt] = useState(false);
  const [selectedCondition, setSelectedCondition] = useState('');
  const moduleLink = `${egfaLink}/${moduleName}`;

  useEffect(() => {
    if (referenzenData) {
      // Prepare checking reading rights
      setHasRights(referenzenData?.rolleType !== RolleLokalType.Keine);
      // Checking main conditions to show or now referenzen info section
      setIsNotFertiggestellt(referenzenData?.status !== EgfaModuleStatusType.VollstaendigBearbeitet);
    }
    // For now we have case only with one condition.
    if (conditionsList?.length) {
      setSelectedCondition([conditionsList.join('')][0]);
    }
  }, [referenzenData, conditionsList]);

  return (
    <>
      {/* For cases where need to show message for NotFertiggestellt modules */}
      {isNotFertiggestellt && currentModuleName && (
        <>
          <p
            dangerouslySetInnerHTML={{
              __html: t(
                `egfa.modules.referenzenText.${currentModuleName}.${moduleName}.notFertiggestelltText.${
                  selectedCondition ? selectedCondition + '.' : ''
                }${hasRights ? 'hasRights' : 'noRights'}`,
                {
                  moduleLink: moduleLink,
                  moduleName: t(`egfa.modules.referenzenText.${moduleName}.moduleTitle`),
                  currentModuleName: t(`egfa.modules.referenzenText.${currentModuleName}.moduleTitle`),
                },
              ),
            }}
          />
          {!hasRights && (
            <p
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.referenzenText.requestRights`, {
                  verantwortlichenPersonEmail: egfaErsteller?.email,
                  verantwortlichenPersonName: egfaErsteller?.name,
                }),
              }}
            />
          )}
        </>
      )}
    </>
  );
}
