// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { GlobalDI, LoadingStatusController } from '@plateg/theme';

import { ModulesListController } from '../../../../egfa/main/modules-list/controller';

interface ZusammenfassungItemProps {
  bearbeitungsLink: string;
  title: string;
  children?: React.ReactElement;
  egfaId: string;
  onSave: (successCallback: () => void) => void;
  isLight?: boolean;
  allowedToModify?: boolean;
}
export function ZusammenfassungItem(props: ZusammenfassungItemProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const loadingStatusController = GlobalDI.getOrRegister<LoadingStatusController>(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const [isRVArchived, setIsRVArchived] = useState<boolean>(false);

  useEffect(() => {
    if (!props.isLight) {
      loadingStatusController.setLoadingStatus(true);
      const sub = ctrl.getIsRVArchived(props.egfaId).subscribe({
        next: (data: boolean) => {
          loadingStatusController.setLoadingStatus(false);
          setIsRVArchived(data);
        },
        error: (error) => {
          loadingStatusController.setLoadingStatus(false);
          console.error(error);
        },
      });
      return () => {
        sub.unsubscribe();
      };
    }
  }, []);

  return (
    <div>
      <div className="title-div">
        {!isRVArchived && props.allowedToModify && (
          <Link
            className="blue-text-button"
            id="egfa-summaryBearbeitung-link"
            to={props.bearbeitungsLink}
            onClick={() => props.onSave(() => history.push(props.bearbeitungsLink))}
          >
            {t('egfa.modules.general.summaryBearbeitung')}
          </Link>
        )}
        <Title level={2}>{props.title}</Title>
      </div>
      {props.children}
      <div className="divider-div" />
    </div>
  );
}
