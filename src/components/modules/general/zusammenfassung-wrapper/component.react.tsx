// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Button, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import {
  AktionType,
  EgfaModuleKMUEntityDTOAllOf,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
} from '@plateg/rest-api';
import { GlobalDI, LeftOutlined, LoadingStatusController, RightOutlined } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ModulesListController } from '../../../egfa/main/modules-list/controller';

interface ZusammenfassungWrapperProps {
  children: React.ReactElement;
  egfaId: string;
  prevPage: string;
  moduleData: EgfaModuleVerbraucherEntityDTO | EgfaModuleKMUEntityDTOAllOf | undefined;
  onSave: (successCallback: () => void) => void;
  additionalActions?: React.ReactElement[];
  moduleNameRoute?: string;
  isLight?: boolean;
  saveBtnDisabled?: boolean;
  isTitleStretched?: boolean;
}
export function ZusammenfassungWrapper(props: ZusammenfassungWrapperProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const loadingStatusController = GlobalDI.getOrRegister<LoadingStatusController>(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const [isRVArchived, setIsRVArchived] = useState<boolean>(false);

  useEffect(() => {
    if (!props.isLight) {
      loadingStatusController.setLoadingStatus(true);
      const sub = ctrl.getIsRVArchived(props.egfaId).subscribe({
        next: (data: boolean) => {
          loadingStatusController.setLoadingStatus(false);
          setIsRVArchived(data);
        },
        error: (error) => {
          loadingStatusController.setLoadingStatus(false);
          console.error(error);
        },
      });
      return () => {
        sub.unsubscribe();
      };
    }
  }, []);

  const onSubmit = () => {
    const url = !props.isLight ? `${props.egfaId}/${routes.MODULE}/${routes.ABSCHLUSS_ERFOLGREICH}` : '';
    props.onSave(() => history.push(`/egfa/${url}`));
  };

  const onPrevious = () => {
    history.push(`/egfa/${props.egfaId}/${routes.MODULE}/${props.prevPage}`);
  };

  const showButton =
    !isRVArchived &&
    props.moduleData &&
    props.moduleData.status !== EgfaModuleStatusType.VollstaendigBearbeitet &&
    props.moduleData.aktionen?.includes(AktionType.Schreiben);

  return (
    <div className="zusammenfassung-div">
      <div className={props.isTitleStretched ? 'form-heading-stretched' : ''}>
        <Title level={1}>{t('egfa.modules.general.summaryTitle')}</Title>
      </div>
      <p>
        {props.moduleNameRoute
          ? t(`egfa.modules.${props.moduleNameRoute}.fertigstellung.infoParagraph`)
          : t('egfa.modules.general.summaryExplanation')}
      </p>
      <div className="divider-div" />
      {props.children}
      {showButton && (
        <div className="form-control-buttons">
          <Button
            id={`egfa-zusammenfassung-prev-btn-${props.egfaId}`}
            onClick={onPrevious}
            type="text"
            size={'large'}
            className="btn-prev"
          >
            <LeftOutlined />
            {t('egfa.neueGesetzesfolgenabschaetzung.btnPrev')}
          </Button>
          <Button
            id="egfa-zusammenfassung-submit-btn"
            type="primary"
            onClick={onSubmit}
            className="btn-next"
            size={'large'}
            disabled={!!props.saveBtnDisabled}
          >
            {t('egfa.modules.general.summarySubmit')}
          </Button>
        </div>
      )}
      {props.additionalActions && props.additionalActions.length > 0 && (
        <div className="additional-buttons">
          {props.additionalActions}
          {!props.isLight && (
            <Button
              id={`egfa-zusammenfassung-modul-uebersicht-${props.egfaId}`}
              onClick={() => {
                history.push(`/egfa/${props.egfaId}/${routes.MODUL_UEBERSICHT}`);
              }}
              type="text"
              size={'large'}
            >
              {t('egfa.modules.general.successLink')}
              <RightOutlined />
            </Button>
          )}
        </div>
      )}
    </div>
  );
}
