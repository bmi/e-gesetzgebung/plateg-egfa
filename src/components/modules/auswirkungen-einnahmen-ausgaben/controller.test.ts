// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { Observable, of, throwError } from 'rxjs';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
  OperationOpts,
  SendEgfaDatenuebernahmeRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import {
  AuswirkungenEinnahmenAusgabenController,
  verwaltungsUndSonstigeRegelungKeysMap,
  WidgetRegelungKeysMapInterface,
  zweckausgabenRegelungKeysMap,
} from './controller';

const regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] = [
  {
    regelungKey: 'OtherRegelung_3_0',
    bezeichnung: 'Regelung 0',
    regelungstabellen: [
      {
        tabellenKey: 'OtherRegelung_3',
        bezeichnung: 'OtherTable 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'OtherRegelung_3_zeile_0',
            bezeichnung: 'OtherTable 3 plan 0',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'OtherRegelung_3_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 10 },
              { jahr: 2022, betrag: 10 },
              { jahr: 2023, betrag: 10 },
              { jahr: 2024, betrag: 10 },
              { jahr: 2025, betrag: 10 },
            ],
          },
        ],
      },

      {
        tabellenKey: 'ZweckausgabenBundRegelung_0',
        bezeichnung: 'Table 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: 'Table 0 plan 0',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 10 },
              { jahr: 2022, betrag: 10 },
              { jahr: 2023, betrag: 10 },
              { jahr: 2024, betrag: 10 },
              { jahr: 2025, betrag: 10 },
            ],
          },
        ],
      },
    ],
  },

  {
    regelungKey: 'ZweckausgabenBundRegelung_2',
    bezeichnung: 'Regelung 2',
    regelungstabellen: [
      {
        tabellenKey: 'Tabelle_0',
        bezeichnung: 'Table 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: '',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 10 },
              { jahr: 2022, betrag: 10 },
              { jahr: 2023, betrag: 10 },
              { jahr: 2024, betrag: 10 },
              { jahr: 2025, betrag: 10 },
            ],
          },
        ],
      },
    ],
  },
  {
    regelungKey: 'ZweckausgabenBundRegelung_3',
    bezeichnung: 'Regelung 3',
    regelungstabellen: [
      {
        tabellenKey: 'Tabelle_0',
        bezeichnung: 'Table 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: '',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 10 },
              { jahr: 2022, betrag: 10 },
              { jahr: 2023, betrag: 10 },
              { jahr: 2024, betrag: 10 },
              { jahr: 2025, betrag: 10 },
            ],
          },
        ],
      },
    ],
  },
];

const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = zweckausgabenRegelungKeysMap;

const yearsRange = [2021, 2022, 2023, 2024, 2025];

const getDefaultYearsRange = (maxYear?: number): number[] => {
  const startYear = new Date().getFullYear();
  const yearsLength = maxYear ? maxYear - startYear + 1 : 5;
  const yearsRange = [];
  for (let i = 0; i < yearsLength; i++) {
    yearsRange.push(startYear + i);
  }
  return yearsRange;
};

describe('AuswirkungenEinnahmenAusgabenController', () => {
  const ctrl = new AuswirkungenEinnahmenAusgabenController();
  const updateRegelungsListSpy = Sinon.spy((list: EgfaModuleEaOehhRegelungEntityDTO[]) => {});

  const setRegelungsListSpy = Sinon.spy((list: EgfaModuleEaOehhRegelungEntityDTO[]) => {});

  beforeEach(() => {
    updateRegelungsListSpy.resetHistory();
    setRegelungsListSpy.resetHistory();
  });

  after(() => {});

  it('prepareYearsRange with regelung list', () => {
    const result = ctrl.prepareYearsRange(regelungsList, widgetRegelungKeysMap);
    expect(result).to.eql(yearsRange);
  });

  it('prepareYearsRange with empty list', () => {
    const result = ctrl.prepareYearsRange([], widgetRegelungKeysMap);
    expect(result).to.eql(getDefaultYearsRange());
  });

  it('prepareYearsRange with undefined list', () => {
    const result = ctrl.prepareYearsRange(undefined, widgetRegelungKeysMap);
    expect(result).to.eql(getDefaultYearsRange());
  });

  it('prepareYearsRange with undefined requestedList', () => {
    const result = ctrl.prepareYearsRange(regelungsList, verwaltungsUndSonstigeRegelungKeysMap);
    expect(result).to.eql(getDefaultYearsRange(2025));
  });

  it('getRequestedRegelungs', () => {
    const result = ctrl.getRequestedRegelungs(regelungsList, widgetRegelungKeysMap);
    const example = regelungsList.slice(1);
    expect(result).to.eql(example);
  });

  it('getRequestedRegelungs return empty list', () => {
    const result = ctrl.getRequestedRegelungs(undefined, widgetRegelungKeysMap);
    expect(result).to.eql([]);
  });

  it('modifyRegelungsToYearsRange', () => {
    const result = ctrl.modifyRegelungsToYearsRange(regelungsList, widgetRegelungKeysMap, [2020, ...yearsRange]);

    expect(result[0].regelungstabellen[0].regelungstabellenZeilen[0].regelungstabellenZeileWerte.length).to.eql(5);
    expect(result[1].regelungstabellen[0].regelungstabellenZeilen[0].regelungstabellenZeileWerte.length).to.eql(6);
  });
  it('modifyRegelungsToYearsRange return empty list', () => {
    const result = ctrl.modifyRegelungsToYearsRange(undefined, widgetRegelungKeysMap, [2020, ...yearsRange]);
    expect(result).to.eql([]);
  });

  it('hasDataInRegelungs', () => {
    const result = ctrl.hasDataInRegelungs(ctrl.getRequestedRegelungs(regelungsList, widgetRegelungKeysMap));
    expect(result).to.be.true;
  });

  it('getSortedRegelungs', () => {
    const sortedList: EgfaModuleEaOehhRegelungEntityDTO[] = [
      {
        regelungKey: 'VerwaltungsvollzugBundRegelung_0',
        bezeichnung: 'Bund 1',
        regelungstabellen: [
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
          {
            tabellenKey: 'table_1',
            bezeichnung: 'Tabelle 2',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
      {
        bezeichnung: 'Bund 2',
        regelungKey: 'VerwaltungsvollzugBundRegelung_1',
        regelungstabellen: [
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
      {
        regelungKey: 'VerwaltungsvollzugBundRegelung_2',
        bezeichnung: 'Bund 3',
        regelungstabellen: [
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
    ];

    // add correct regelung type
    const unsortedList: EgfaModuleEaOehhRegelungEntityDTO[] = [
      {
        bezeichnung: 'Bund 2',
        regelungKey: 'VerwaltungsvollzugBundRegelung_1',
        regelungstabellen: [
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
      {
        bezeichnung: 'Bund 1',
        regelungKey: 'VerwaltungsvollzugBundRegelung_0',
        regelungstabellen: [
          {
            tabellenKey: 'table_1',
            bezeichnung: 'Tabelle 2',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
      {
        bezeichnung: 'Bund 3',
        regelungKey: 'VerwaltungsvollzugBundRegelung_2',
        regelungstabellen: [
          {
            tabellenKey: 'table_0',
            bezeichnung: 'Tabelle 1',
            regelungstabellenZeilen: [
              {
                zeileKey: 'zeile_1',
                bezeichnung: 'zeile 2',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_0',
                bezeichnung: 'zeile 1',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_summe',
                bezeichnung: 'Summe',
                regelungstabellenZeileWerte: [],
              },
              {
                zeileKey: 'zeile_2',
                bezeichnung: 'zeile 3',
                regelungstabellenZeileWerte: [],
              },
            ],
          },
        ],
      },
    ];
    const result = ctrl.getSortedRegelungs(unsortedList);
    expect(result).to.eql(sortedList);
  });
});

describe(`Test: exportAuswirkungenEinnahmenAusgabenToEditor`, () => {
  const auswirkungenEinnahmenAusgabenController = new AuswirkungenEinnahmenAusgabenController();
  let egfaControllerStub: Sinon.SinonStub<
    [SendEgfaDatenuebernahmeRequest, (OperationOpts | undefined)?],
    Observable<void | AjaxResponse<void>>
  >;
  let egfaModuleControllerStub: Sinon.SinonStub;
  let i18nStub: Sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    GlobalDI.getOrRegister('egfaModuleEaOehhControllerApi', () => new EgfaModuleEaOehhControllerApi());
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleEaOehhControllerApi.prototype, 'getEgfaModule11');

    i18nStub = Sinon.stub(i18n, 't');
  });
  const auswirkungenEinnahmenAusgabenData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = {
    base: {
      id: '87c40451-d8bc-441a-8134-bec475457322',
      erstelltAm: '2023-08-02T10:05:21.808813Z',
      bearbeitetAm: '2023-08-10T09:49:29.950382Z',
    },
    regelungsvorhaben: {
      base: {
        id: 'ac403b22-e3e4-416b-afb9-48229f7b8a64',
        erstelltAm: '2023-08-02T10:03:27.169384Z',
        bearbeitetAm: '2023-08-08T10:00:14.150806Z',
      },
      dto: {
        abkuerzung: 'VkBkmG',
        kurzbezeichnung: 'Gesetz über die Verkündung von Rechtsverordnungen und Bekanntmachungen',
        status: 'IN_BEARBEITUNG',
        vorhabenart: 'GESETZ',
        archiviertAm: null,
        erstelltAm: null,
        erstelltVon: {
          base: {
            id: 'a9fbc760-5aa4-4e65-a927-f2d935f3114b',
            erstelltAm: '2023-02-06T10:48:46.047685Z',
            bearbeitetAm: '2023-02-06T10:48:46.047685Z',
          },
          dto: {
            name: 'Example User',
            email: 'example1@example.com',
            telefon: null,
            abteilung: 'D',
            fachreferat: null,
            emailVerteilerReferat: null,
            titel: null,
            ressort: {
              id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
              kurzbezeichnung: 'BMI',
              bezeichnung: 'Bundesministerium des Innern und für Heimat',
              aktiv: true,
            },
            anrede: 'Herr',
            gid: 'someRandomId',
          },
        },
        bearbeitetAm: null,
        pkpGesendetAm: '2023-08-02T10:03:27.169384Z',
        pkpStatus: 'ERSTELLT',
        farbe: null,
        langtitel: 'Gesetz über die Verkündung von Rechtsverordnungen und Bekanntmachungen',
        kurzbeschreibung: 'Gesetz über die Verkündung von Rechtsverordnungen und Bekanntmachungen',
        technischesFfRessort: {
          id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
          kurzbezeichnung: 'BMI',
          bezeichnung: 'Bundesministerium des Innern und für Heimat',
          aktiv: true,
        },
        technischeFfAbteilung: {
          id: '14a67ec6-7077-4366-8e40-a58db2907274',
          bezeichnung: 'Z',
          unterabteilungen: [
            {
              id: '8d3dccdb-4003-4880-bdf5-0a80b108135d',
              bezeichnung: 'Z II',
              unterabteilungen: [],
              referate: [
                {
                  id: '0d488328-8009-4ae1-8a89-aaca9505e08d',
                  bezeichnung: 'Z II 1',
                },
                {
                  id: '3d6ce95a-0af4-414c-aef4-69b9d36378fd',
                  bezeichnung: 'Z II 2',
                },
              ],
            },
            {
              id: 'be04e225-bd50-44d1-b107-7a8b92598ecc',
              bezeichnung: 'Z I',
              unterabteilungen: [],
              referate: [],
            },
            {
              id: 'e0a8e60d-4405-4064-a80c-e284df3793b2',
              bezeichnung: 'Z III',
              unterabteilungen: [],
              referate: [],
            },
          ],
          referate: [],
        },
        technischesFfReferat: {
          id: '3d6ce95a-0af4-414c-aef4-69b9d36378fd',
          bezeichnung: 'Z II 2',
        },
        bearbeitetVon: {
          base: {
            id: 'a9fbc760-5aa4-4e65-a927-f2d935f3114b',
            erstelltAm: '2023-02-06T10:48:46.047685Z',
            bearbeitetAm: '2023-02-06T10:48:46.047685Z',
          },
          dto: {
            name: 'Example User',
            email: 'example@example.com',
            telefon: null,
            abteilung: 'D',
            fachreferat: null,
            emailVerteilerReferat: null,
            titel: null,
            ressort: {
              id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
              kurzbezeichnung: 'BMI',
              bezeichnung: 'Bundesministerium des Innern und für Heimat',
              aktiv: true,
            },
            anrede: 'Herr',
            gid: 'someRandomId',
          },
        },
      },
    },
    dto: {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'test',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: null,
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: null,
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: null,
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: null,
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: null,
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: null,
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: null,
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: null,
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: null,
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: null,
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: null,
      influenceVolleJahreswirkungSummary: null,
      influenceGeplanteKompensationDerKostenExpected: 'NEIN',
      influenceGeplanteKompensationDerKostenSummary: null,
      regelungen: [],
      typ: 'EA_OEHH',
    },
  };
  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
    i18nStub.resetHistory();
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function
    egfaModuleControllerStub.returns(of(auswirkungenEinnahmenAusgabenData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      const expectedOutput = {
        id: 'auswirkungenEinnahmenAusgabenErgebnisdokumentation',
        content: [],
        children: [
          {
            id: 'vorblatt',
            title: 'Vorblatt',
            content: [],
            children: [
              {
                id: 'vorblattD',
                title: 'D. Haushaltsausgaben ohne Erfüllungsaufwand',
                content: [
                  {
                    id: 'textVorblattD',
                    data: 'test',
                    type: 'TEXT',
                  },
                ],
              },
              {
                id: 'ausgaben',
                title: 'Ausgaben',
                content: [],
                children: [
                  {
                    id: 'vorblattausgabenbund',
                    content: [
                      {
                        id: 'vorblattausgabenbundData',
                        type: 'TABLE',
                        data: [['Bund', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblattausgabenlaender',
                    content: [
                      {
                        id: 'vorblattausgabenlaenderData',
                        type: 'TABLE',
                        data: [['Länder', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblattausgabengemeinden',
                    content: [
                      {
                        id: 'vorblattausgabengemeindenData',
                        type: 'TABLE',
                        data: [['Gemeinden/Gemeindeverbände', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblattausgabengesamtsumme',
                    content: [
                      {
                        id: 'vorblattausgabengesamtsummeData',
                        type: 'TABLE',
                        data: [['Gesamtsumme', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                ],
              },
              {
                id: 'einnahmen',
                title: 'Einnahmen',
                content: [],
                children: [
                  {
                    id: 'vorblatteinnahmenbund',
                    content: [
                      {
                        id: 'vorblatteinnahmenbundData',
                        type: 'TABLE',
                        data: [['Bund', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblatteinnahmenlaender',
                    content: [
                      {
                        id: 'vorblatteinnahmenlaenderData',
                        type: 'TABLE',
                        data: [['Länder', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblatteinnahmengemeinden',
                    content: [
                      {
                        id: 'vorblatteinnahmengemeindenData',
                        type: 'TABLE',
                        data: [['Gemeinden/Gemeindeverbände', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                  {
                    id: 'vorblatteinnahmengesamtsumme',
                    content: [
                      {
                        id: 'vorblatteinnahmengesamtsummeData',
                        type: 'TABLE',
                        data: [['Gesamtsumme', '2023', '2024', '2025', '2026', '2027'], []],
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            id: 'vorblatt',
            title: 'Begründung - Allgemeiner Teil',
            content: [],
            children: [
              {
                id: 'auswirkungenEinzelnenBereicheTitle',
                content: [],
                title: 'A. VI. 3 Haushaltsausgaben ohne Erfüllungsaufwand',
              },
              {
                id: 'zweckausgaben_patternSummary',
                children: [
                  {
                    id: 'zweckausgaben_patternSummarytitle',
                    title: 'Zweckausgaben',
                    content: [
                      {
                        id: 'zweckausgaben_patternSummary_text',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
                content: [],
              },
              {
                id: 'vollzugsaufwand_patternSummary',
                children: [
                  {
                    id: 'vollzugsaufwand_patternSummarytitle',
                    title: 'Ausgaben für den Verwaltungsvollzug',
                    content: [
                      {
                        id: 'vollzugsaufwand_patternSummary_text',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
                content: [],
              },
              {
                id: 'normadressat_Summary',
                content: [],
                children: [
                  {
                    id: 'normadressat_Summary_Title',
                    title: 'Ausgaben für die Verwaltung als Normadressat',
                    content: [
                      {
                        id: 'normadressat_Summary_text1',
                        data: '',
                        type: 'TEXT',
                      },
                      {
                        id: 'normadressat_Summary_text2',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'verwaltungsUndSonstige_patternSummary',
                children: [
                  {
                    id: 'verwaltungsUndSonstige_patternSummarytitle',
                    title: 'Verwaltungseinnahmen und sonstige Einnahmen',
                    content: [
                      {
                        id: 'verwaltungsUndSonstige_patternSummary_text',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
                content: [],
              },
              {
                id: 'steuereinnahmen_patternSummary',
                children: [
                  {
                    id: 'steuereinnahmen_patternSummarytitle',
                    title: 'Steuereinnahmen',
                    content: [
                      {
                        id: 'steuereinnahmen_patternSummary_text',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
                content: [],
              },
              {
                id: 'sonstigerBereiche_Summary',
                content: [],
                children: [
                  {
                    id: 'sonstigerBereiche_Title',
                    title: 'Ausgaben und Einnahmen weiterer Bereiche des Staates',
                    content: [
                      {
                        id: 'sonstigerBereiche_text1',
                        data: '',
                        type: 'TEXT',
                      },
                      {
                        id: 'sonstigerBereiche_text2',
                        data: 'Keine Auswirkungen',
                        type: 'TEXT',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'kompensationKosten',
                title: 'Geplante Kompensation der Kosten',
                content: [
                  {
                    id: 'kompensationKosten',
                    data: '',
                    type: 'TEXT',
                  },
                ],
                children: [],
              },
            ],
          },
        ],
      };
      // Assert the result

      egfaControllerStub.calledWith({
        id: '123',
        moduleType: EgfaModuleType.EaOehh,
        egfaDatenuebernahmeDTO: expectedOutput,
      });

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });

  it(` getting AuswirkungenEinnahmenAusgaben error`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(throwError(new Error('error getting eGFA')));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');
    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      Sinon.assert.notCalled(egfaControllerStub);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`error on sending to editor`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(of(auswirkungenEinnahmenAusgabenData));
    egfaControllerStub.returns(throwError(new Error('error getting EaOehh')));
    i18nStub.returns('sometext');
    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceAusgabenExpected expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: true,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary1",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceZweckausgaben expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: true,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary12",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceAusgabenVerwaltungsvollzug expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: true,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary10",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceEinnahmen Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: true,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary4",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceEinnahmen Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: true,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary6",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceWeitereEinnahmen Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: true,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary8",')).to.be.true;
      expect(x.includes('"data":"TestSummary9",')).to.be.true;
      expect(x.includes('"id":"sonstigerBereicheTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheGesamtTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenGesamtTable"')).to.be.false;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceVerwaltungAlsNormadressat Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: true,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);
      expect(x.includes('"data":"TestSummary2",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceGeplanteKompensationDerKostenSummary Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: 'TestSummary14',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);

      expect(x.includes('"data":"TestSummary14",')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, influenceGeplanteKompensationDerKostenSummary not Expected`, function (done) {
    // Add the 'done' parameter to the test function
    const newInputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = JSON.parse(
      JSON.stringify(auswirkungenEinnahmenAusgabenData),
    ) as EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO;
    newInputData.dto = {
      egfaId: '26dab19a-e202-42d1-b3ef-8a94dc3d9ede',
      title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      influenceExpected: false,
      deleted: false,
      influenceSummary: 'TestSummary1',
      status: 'VOLLSTAENDIG_BEARBEITET',
      allowedToModify: true,
      influenceAusgabenExpected: false,
      influenceVerwaltungsvollzugExpected: false,
      influenceVerwaltungAlsNormadressatExpected: false,
      influenceVerwaltungAlsNormadressatSummary: 'TestSummary2',
      influenceVerwaltungAlsNormadressatStellenplaene: false,
      influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'TestSummary3',
      influenceZweckausgabenExpected: false,
      influenceEinnahmenExpected: false,
      influenceEinnahmenSummary: 'TestSummary4',
      influenceEinnahmenVolleJahreswirkungExpected: false,
      influenceEinnahmenVolleJahreswirkungSummary: 'TestSummary5',
      influenceSteuereinnahmenExpected: false,
      influenceSteuereinnahmenSummary: 'TestSummary6',
      influenceSteuereinnahmenVolleJahreswirkungExpected: false,
      influenceSteuereinnahmenVolleJahreswirkungSummary: 'TestSummary7',
      influenceWeitereEinnahmenExpected: false,
      influenceWeitereEinnahmenSummary: 'TestSummary8',
      influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
      influenceWeitereEinnahmenVolleJahreswirkungSummary: 'TestSummary9',
      influenceAusgabenSonstigeBereicheExpected: false,
      influenceEinnahmenSonstigeBereicheExpected: false,
      influenceAusgabenVerwaltungsvollzug: false,
      influenceAusgabenVerwaltungsvollzugSummary: 'TestSummary10',
      influenceVollzugsaufwandStellenplaene: false,
      influenceVollzugsaufwandStellenplaeneSummary: 'TestSummary11',
      influenceZweckausgaben: false,
      influenceZweckausgabenSummary: 'TestSummary12',
      influenceVolleJahreswirkungSummary: 'TestSummary13',
      influenceGeplanteKompensationDerKostenExpected: false,
      influenceGeplanteKompensationDerKostenSummary: '',
      regelungen: [],
      typ: 'EA_OEHH',
    };
    egfaModuleControllerStub.returns(of(newInputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);

      expect(x.includes('{"id":"kompensationKostenText","data":"sometext","type":"TEXT"}')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, no Regelungen`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(of(auswirkungenEinnahmenAusgabenData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);

      expect(x.includes('"id":"sonstigerBereicheTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheGesamtTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenTable"')).to.be.false;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenGesamtTable"')).to.be.false;

      expect(x.includes('"id":"normadressatAusgabenDurchTable"')).to.be.false;
      expect(x.includes('"id":"normadressatAusgabenGesamtTable"')).to.be.false;

      expect(x.includes('"id":"vorblattAusgaben"')).to.be.false;
      expect(x.includes('"id":"vorblattAusgabenBund"')).to.be.false;
      expect(x.includes('"id":"vorblattAusgabenLaender"')).to.be.false;
      expect(x.includes('"id":"vorblattAusgabenGemeinden"')).to.be.false;
      expect(x.includes('"id":"vorblattAusgabenGesamtsumme"')).to.be.false;
      expect(x.includes('"id":"vorblattEinnahmenLaender"')).to.be.false;
      expect(x.includes('"id":"vorblattEinnahmenBund"')).to.be.false;
      expect(x.includes('"id":"vorblattEinnahmenGemeinden"')).to.be.false;
      expect(x.includes('"id":"vorblattEinnahmenGesamtsumme"')).to.be.false;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct AuswirkungenEinnahmenAusgaben data, with Regelungen`, function (done) {
    // Add the 'done' parameter to the test function
    const inputData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = {
      base: {
        id: 'e53db09e-8122-4e46-b5d4-272e56649b23',
        erstelltAm: '2023-07-07T09:31:13.551420Z',
        bearbeitetAm: '2023-08-09T17:58:08.638731Z',
      },
      regelungsvorhaben: {
        base: {
          id: '4592d979-9cd6-417f-a7c5-ba3ef11ce65d',
          erstelltAm: '2023-05-26T10:52:03.064804Z',
          bearbeitetAm: '2023-06-05T10:05:56.401182Z',
        },
        dto: {
          abkuerzung: 'HALLO WELT!',
          kurzbezeichnung: 'HALLO WELT!',
          status: 'IN_BEARBEITUNG',
          vorhabenart: 'GESETZ',
          archiviertAm: null,
          erstelltAm: null,
          erstelltVon: {
            base: {
              id: '8ab98ca8-a683-4ed8-beb9-c2be63abea2e',
              erstelltAm: '2023-02-06T12:42:54.141444Z',
              bearbeitetAm: '2023-02-06T12:42:54.141444Z',
            },
            dto: {
              name: 'Example User',
              email: 'example@example.com',
              telefon: '32 16 8',
              abteilung: null,
              fachreferat: null,
              emailVerteilerReferat: null,
              titel: 'Dr. rer. nat.',
              ressort: {
                id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
                kurzbezeichnung: 'BMI',
                bezeichnung: 'Bundesministerium des Innern und für Heimat',
                aktiv: true,
              },
              anrede: 'Frau',
              gid: 'pkp-3',
            },
          },
          bearbeitetAm: null,
          pkpGesendetAm: null,
          pkpStatus: 'OFFEN',
          farbe: null,
          langtitel: 'HALLO WELT!',
          kurzbeschreibung: null,
          technischesFfRessort: {
            id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
            kurzbezeichnung: 'BMI',
            bezeichnung: 'Bundesministerium des Innern und für Heimat',
            aktiv: true,
          },
          technischeFfAbteilung: null,
          technischesFfReferat: null,
          bearbeitetVon: {
            base: {
              id: '8ab98ca8-a683-4ed8-beb9-c2be63abea2e',
              erstelltAm: '2023-02-06T12:42:54.141444Z',
              bearbeitetAm: '2023-02-06T12:42:54.141444Z',
            },
            dto: {
              name: 'Example User',
              email: 'example@example.com',
              telefon: '32 16 8',
              abteilung: null,
              fachreferat: null,
              emailVerteilerReferat: null,
              titel: 'Dr. rer. nat.',
              ressort: {
                id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
                kurzbezeichnung: 'BMI',
                bezeichnung: 'Bundesministerium des Innern und für Heimat',
                aktiv: true,
              },
              anrede: 'Frau',
              gid: 'pkp-3',
            },
          },
        },
      },
      dto: {
        egfaId: 'ba2f89f2-533a-4f83-8af3-531f367c1f3e',
        title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
        influenceExpected: false,
        deleted: false,
        influenceSummary: 'Ausgaben der öffentlichen Haushalte',
        status: 'VOLLSTAENDIG_BEARBEITET',
        allowedToModify: true,
        influenceAusgabenExpected: false,
        influenceVerwaltungsvollzugExpected: false,
        influenceVerwaltungAlsNormadressatExpected: true,
        influenceVerwaltungAlsNormadressatSummary:
          'Bitte beschreiben Sie die erwarteten Auswirkungen durch Veränderungen bei den Ausgaben für die Verwaltung als Normadressat.',
        influenceVerwaltungAlsNormadressatStellenplaene: true,
        influenceVerwaltungAlsNormadressatStellenplaeneSummary: 'errtty',
        influenceZweckausgabenExpected: true,
        influenceEinnahmenExpected: true,
        influenceEinnahmenSummary: 'errt',
        influenceEinnahmenVolleJahreswirkungExpected: false,
        influenceEinnahmenVolleJahreswirkungSummary: 'fgfg',
        influenceSteuereinnahmenExpected: true,
        influenceSteuereinnahmenSummary: 'tryhgfh',
        influenceSteuereinnahmenVolleJahreswirkungExpected: false,
        influenceSteuereinnahmenVolleJahreswirkungSummary: 'fgh',
        influenceWeitereEinnahmenExpected: true,
        influenceWeitereEinnahmenSummary: 'sdfdsf',
        influenceWeitereEinnahmenVolleJahreswirkungExpected: false,
        influenceWeitereEinnahmenVolleJahreswirkungSummary: 'sdfdsf',
        influenceAusgabenSonstigeBereicheExpected: false,
        influenceEinnahmenSonstigeBereicheExpected: false,
        influenceAusgabenVerwaltungsvollzug: true,
        influenceAusgabenVerwaltungsvollzugSummary: 'test',
        influenceVollzugsaufwandStellenplaene: true,
        influenceVollzugsaufwandStellenplaeneSummary: 'test',
        influenceZweckausgaben: false,
        influenceZweckausgabenSummary: 'asdaasd',
        influenceVolleJahreswirkungSummary: null,
        influenceGeplanteKompensationDerKostenExpected: 'NEIN',
        influenceGeplanteKompensationDerKostenSummary: 'ioioio',
        regelungen: [
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 1,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: 1,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: 1,
                      },
                      {
                        jahr: 2031,
                        betrag: 3,
                      },
                      {
                        jahr: 2032,
                        betrag: 5,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'fgh',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 1,
                      },
                      {
                        jahr: 2027,
                        betrag: 1,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                      {
                        jahr: 2029,
                        betrag: 1,
                      },
                      {
                        jahr: 2030,
                        betrag: 1,
                      },
                      {
                        jahr: 2031,
                        betrag: 2,
                      },
                      {
                        jahr: 2032,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'AenderungenSteuereinnahmenBundRegelung_26',
            bezeichnung: 'gfh',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 5,
                      },
                      {
                        jahr: 2028,
                        betrag: 6,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: null,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'AenderungenSteuereinnahmenGemeindenRegelung_28',
            bezeichnung: 'sdf',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 1,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: null,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Bund gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 222,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: null,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Länder gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 3333,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: null,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: 444,
                      },
                      {
                        jahr: 2029,
                        betrag: 555,
                      },
                      {
                        jahr: 2030,
                        betrag: 555,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'AenderungenSteuereinnahmenGesamtsummeRegelung_29',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 1,
                      },
                      {
                        jahr: 2026,
                        betrag: 2,
                      },
                      {
                        jahr: 2027,
                        betrag: 3,
                      },
                      {
                        jahr: 2028,
                        betrag: 4,
                      },
                      {
                        jahr: 2029,
                        betrag: null,
                      },
                      {
                        jahr: 2030,
                        betrag: null,
                      },
                      {
                        jahr: 2031,
                        betrag: null,
                      },
                      {
                        jahr: 2032,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'AenderungenSteuereinnahmenLaenderRegelung_27',
            bezeichnung: 'fgfgfgddddddddddddddd',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 5,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 6,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 7,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: 8,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst gesamt',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'NormadressatGesamtStellenplaeneRegelung_25',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 6,
                      },
                      {
                        jahr: 2026,
                        betrag: 7,
                      },
                      {
                        jahr: 2027,
                        betrag: 8,
                      },
                      {
                        jahr: 2028,
                        betrag: 9,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'NormadressatGesamtsummeRegelung_23',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 5,
                      },
                      {
                        jahr: 2026,
                        betrag: 6,
                      },
                      {
                        jahr: 2027,
                        betrag: 7,
                      },
                      {
                        jahr: 2028,
                        betrag: 8,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: '13dd',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 12,
                      },
                      {
                        jahr: 2026,
                        betrag: 23,
                      },
                      {
                        jahr: 2027,
                        betrag: 34,
                      },
                      {
                        jahr: 2028,
                        betrag: 45,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'ee',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 2,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: 4,
                      },
                      {
                        jahr: 2028,
                        betrag: 5,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'NormadressatRegelungRegelung_22',
            bezeichnung: 'ddff',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 11,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 12,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: '1dfdf',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 12,
                      },
                      {
                        jahr: 2026,
                        betrag: 12,
                      },
                      {
                        jahr: 2027,
                        betrag: 12,
                      },
                      {
                        jahr: 2028,
                        betrag: 12,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'eee',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 12,
                      },
                      {
                        jahr: 2026,
                        betrag: 11,
                      },
                      {
                        jahr: 2027,
                        betrag: 11,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'NormadressatRegelungRegelung_44',
            bezeichnung: 'erere',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 1,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 2,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 3,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: 'dfgfhhj',
              },
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: 12,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 12,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 12,
                      },
                      {
                        jahr: 2028,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                      {
                        jahr: 2028,
                        betrag: 12,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst',
                  },
                ],
                tabellenKey: 'table_1',
                bezeichnung: 'dfsgdf',
              },
            ],
            regelungKey: 'NormadressatStellenplaeneRegelung_24',
            bezeichnung: 'tyyuui',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1000,
                      },
                      {
                        jahr: 2024,
                        betrag: 2000,
                      },
                      {
                        jahr: 2025,
                        betrag: 3000,
                      },
                      {
                        jahr: 2026,
                        betrag: 522,
                      },
                      {
                        jahr: 2027,
                        betrag: 7899,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'SonstigerBereicheStaatesAusgabenRegelung_30',
            bezeichnung: 'retgfgdfg fgdfg',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: 11,
                      },
                      {
                        jahr: 2025,
                        betrag: 33,
                      },
                      {
                        jahr: 2026,
                        betrag: 44,
                      },
                      {
                        jahr: 2027,
                        betrag: 55,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'SonstigerBereicheStaatesEinnahmenRegelung_32',
            bezeichnung: 'fghgfh',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'gg',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'SonstigerBereicheStaatesEinnahmenRegelung_45',
            bezeichnung: 'fgfgfggf',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 23,
                      },
                      {
                        jahr: 2024,
                        betrag: 23,
                      },
                      {
                        jahr: 2025,
                        betrag: 45,
                      },
                      {
                        jahr: 2026,
                        betrag: 56,
                      },
                      {
                        jahr: 2027,
                        betrag: 76,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'SonstigerBereicheStaatesGesamtsummeAusgabenRegelung_31',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 34,
                      },
                      {
                        jahr: 2024,
                        betrag: 45,
                      },
                      {
                        jahr: 2025,
                        betrag: 4455,
                      },
                      {
                        jahr: 2026,
                        betrag: 55,
                      },
                      {
                        jahr: 2027,
                        betrag: 66,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'SonstigerBereicheStaatesGesamtsummeEinnahmenRegelung_33',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1,
                      },
                      {
                        jahr: 2024,
                        betrag: 2,
                      },
                      {
                        jahr: 2025,
                        betrag: 33,
                      },
                      {
                        jahr: 2026,
                        betrag: 1,
                      },
                      {
                        jahr: 2027,
                        betrag: 2,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsUndSonstigeBundRegelung_4',
            bezeichnung: 'fgfg',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 55,
                      },
                      {
                        jahr: 2025,
                        betrag: 66,
                      },
                      {
                        jahr: 2026,
                        betrag: 77,
                      },
                      {
                        jahr: 2027,
                        betrag: 88,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsUndSonstigeGemeindenRegelung_6',
            bezeichnung: 'uiuioio',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 22,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 1,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Bund gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: 33,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 2,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Länder gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 44,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 55,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsUndSonstigeGesamtsummeRegelung_7',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 56,
                      },
                      {
                        jahr: 2024,
                        betrag: 67,
                      },
                      {
                        jahr: 2025,
                        betrag: 78,
                      },
                      {
                        jahr: 2026,
                        betrag: 89,
                      },
                      {
                        jahr: 2027,
                        betrag: 90,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsUndSonstigeLaenderRegelung_34',
            bezeichnung: 'ghggggggg',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 5,
                      },
                      {
                        jahr: 2025,
                        betrag: 6,
                      },
                      {
                        jahr: 2026,
                        betrag: 1,
                      },
                      {
                        jahr: 2027,
                        betrag: 2,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsUndSonstigeLaenderRegelung_5',
            bezeichnung: 'dffggh',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1,
                      },
                      {
                        jahr: 2024,
                        betrag: 2,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 5,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'something',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 6,
                      },
                      {
                        jahr: 2024,
                        betrag: 7,
                      },
                      {
                        jahr: 2025,
                        betrag: 8,
                      },
                      {
                        jahr: 2026,
                        betrag: 9,
                      },
                      {
                        jahr: 2027,
                        betrag: 10,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'somthing1',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 7,
                      },
                      {
                        jahr: 2024,
                        betrag: 9,
                      },
                      {
                        jahr: 2025,
                        betrag: 11,
                      },
                      {
                        jahr: 2026,
                        betrag: 13,
                      },
                      {
                        jahr: 2027,
                        betrag: 15,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugBundRegelung_16',
            bezeichnung: 'ausgabe bund',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 56,
                      },
                      {
                        jahr: 2024,
                        betrag: 5,
                      },
                      {
                        jahr: 2025,
                        betrag: 5,
                      },
                      {
                        jahr: 2026,
                        betrag: 6,
                      },
                      {
                        jahr: 2027,
                        betrag: 66,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: '4445fgh',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 4,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'ee',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 55,
                      },
                      {
                        jahr: 2024,
                        betrag: 66,
                      },
                      {
                        jahr: 2025,
                        betrag: 6,
                      },
                      {
                        jahr: 2026,
                        betrag: 7,
                      },
                      {
                        jahr: 2027,
                        betrag: 8,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugBundRegelung_40',
            bezeichnung: 'fgfgghjhj',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 34,
                      },
                      {
                        jahr: 2024,
                        betrag: 45,
                      },
                      {
                        jahr: 2025,
                        betrag: 56,
                      },
                      {
                        jahr: 2026,
                        betrag: 67,
                      },
                      {
                        jahr: 2027,
                        betrag: 78,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugGemeindenRegelung_18',
            bezeichnung: 'Gemeinden/Gemeindeverbände ddd',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 12,
                      },
                      {
                        jahr: 2024,
                        betrag: 23,
                      },
                      {
                        jahr: 2025,
                        betrag: 34,
                      },
                      {
                        jahr: 2026,
                        betrag: 45,
                      },
                      {
                        jahr: 2027,
                        betrag: 56,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugGemeindenRegelung_41',
            bezeichnung: 'rr',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 56,
                      },
                      {
                        jahr: 2024,
                        betrag: 67,
                      },
                      {
                        jahr: 2025,
                        betrag: 78,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugGemeindenRegelung_42',
            bezeichnung: 'ttttttttttttttttttttttttttttttttttttt',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 5,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 5,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 6,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst gesamt',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugGesamtStellenplaeneRegelung_21',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 12,
                      },
                      {
                        jahr: 2024,
                        betrag: 67,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Bund gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: 12,
                      },
                      {
                        jahr: 2025,
                        betrag: 45,
                      },
                      {
                        jahr: 2026,
                        betrag: 34,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Länder gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 34,
                      },
                      {
                        jahr: 2024,
                        betrag: 45,
                      },
                      {
                        jahr: 2025,
                        betrag: 12,
                      },
                      {
                        jahr: 2026,
                        betrag: 23,
                      },
                      {
                        jahr: 2027,
                        betrag: 12,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 45,
                      },
                      {
                        jahr: 2024,
                        betrag: 56,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 12,
                      },
                      {
                        jahr: 2027,
                        betrag: 12,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugGesamtsummeRegelung_19',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 66,
                      },
                      {
                        jahr: 2024,
                        betrag: 77,
                      },
                      {
                        jahr: 2025,
                        betrag: 88,
                      },
                      {
                        jahr: 2026,
                        betrag: 99,
                      },
                      {
                        jahr: 2027,
                        betrag: 0,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VerwaltungsvollzugLaenderRegelung_17',
            bezeichnung: 'summe lander ',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1000,
                      },
                      {
                        jahr: 2024,
                        betrag: 2000,
                      },
                      {
                        jahr: 2025,
                        betrag: 3000,
                      },
                      {
                        jahr: 2026,
                        betrag: 522,
                      },
                      {
                        jahr: 2027,
                        betrag: 7899,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: 'testttttt gfgg',
              },
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 44,
                      },
                      {
                        jahr: 2024,
                        betrag: 44,
                      },
                      {
                        jahr: 2025,
                        betrag: 44,
                      },
                      {
                        jahr: 2026,
                        betrag: 44,
                      },
                      {
                        jahr: 2027,
                        betrag: 44,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 33,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 2,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 22,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst',
                  },
                ],
                tabellenKey: 'table_1',
                bezeichnung: '4434342dsd',
              },
            ],
            regelungKey: 'VerwaltungsvollzugStellenplaeneRegelung_20',
            bezeichnung: 'Bezeichnung der Regelung',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 2,
                      },
                      {
                        jahr: 2024,
                        betrag: 2,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Höherer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: 2,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Gehobener Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: 2,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Mittlerer Dienst',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: 2,
                      },
                    ],
                    zeileKey: 'zeile_3',
                    bezeichnung: 'Einfacher Dienst',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '22',
              },
            ],
            regelungKey: 'VerwaltungsvollzugStellenplaeneRegelung_43',
            bezeichnung: 'fgfg',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattAusgabenBundRegelung_8',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1000,
                      },
                      {
                        jahr: 2024,
                        betrag: 2000,
                      },
                      {
                        jahr: 2025,
                        betrag: 3000,
                      },
                      {
                        jahr: 2026,
                        betrag: 522,
                      },
                      {
                        jahr: 2027,
                        betrag: 7899,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattAusgabenGemeindenRegelung_10',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 3,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattAusgabenGesamtsummeRegelung_11',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: null,
                      },
                      {
                        jahr: 2025,
                        betrag: null,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattAusgabenLaenderRegelung_9',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 3,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattEinnahmenBundRegelung_12',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 3,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattEinnahmenGemeindenRegelung_14',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: 3,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: null,
                      },
                      {
                        jahr: 2027,
                        betrag: null,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattEinnahmenGesamtsummeRegelung_15',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 3,
                      },
                      {
                        jahr: 2024,
                        betrag: 3,
                      },
                      {
                        jahr: 2025,
                        betrag: 3,
                      },
                      {
                        jahr: 2026,
                        betrag: 3,
                      },
                      {
                        jahr: 2027,
                        betrag: 3,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'VorblattEinnahmenLaenderRegelung_13',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 55,
                      },
                      {
                        jahr: 2024,
                        betrag: 55,
                      },
                      {
                        jahr: 2025,
                        betrag: 55,
                      },
                      {
                        jahr: 2026,
                        betrag: 55,
                      },
                      {
                        jahr: 2027,
                        betrag: 55,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'testttttt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 1000,
                      },
                      {
                        jahr: 2024,
                        betrag: 2000,
                      },
                      {
                        jahr: 2025,
                        betrag: 3000,
                      },
                      {
                        jahr: 2026,
                        betrag: 522,
                      },
                      {
                        jahr: 2027,
                        betrag: 7899,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenBundRegelung_0',
            bezeichnung: 'Bezeichnung  1',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 22,
                      },
                      {
                        jahr: 2024,
                        betrag: 33,
                      },
                      {
                        jahr: 2025,
                        betrag: 44,
                      },
                      {
                        jahr: 2026,
                        betrag: 55,
                      },
                      {
                        jahr: 2027,
                        betrag: 66,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'we',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 33,
                      },
                      {
                        jahr: 2024,
                        betrag: 33,
                      },
                      {
                        jahr: 2025,
                        betrag: 33,
                      },
                      {
                        jahr: 2026,
                        betrag: 33,
                      },
                      {
                        jahr: 2027,
                        betrag: 33,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'rtrtrtrtrt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 33,
                      },
                      {
                        jahr: 2024,
                        betrag: 33,
                      },
                      {
                        jahr: 2025,
                        betrag: 33,
                      },
                      {
                        jahr: 2026,
                        betrag: 33,
                      },
                      {
                        jahr: 2027,
                        betrag: 33,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenBundRegelung_35',
            bezeichnung: 'ffffffffffffff',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 124,
                      },
                      {
                        jahr: 2024,
                        betrag: 4,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'dfsd',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: null,
                      },
                      {
                        jahr: 2024,
                        betrag: 4,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'fggh',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 4,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 4,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenBundRegelung_36',
            bezeichnung: 'fgghhj',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 12354,
                      },
                      {
                        jahr: 2024,
                        betrag: 4511,
                      },
                      {
                        jahr: 2025,
                        betrag: 6659,
                      },
                      {
                        jahr: 2026,
                        betrag: 8487,
                      },
                      {
                        jahr: 2027,
                        betrag: 4545,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenGemeindenRegelung_2',
            bezeichnung: 'Gemeinden/Gemeindeverbändefffff',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 5,
                      },
                      {
                        jahr: 2025,
                        betrag: 6,
                      },
                      {
                        jahr: 2026,
                        betrag: 7,
                      },
                      {
                        jahr: 2027,
                        betrag: 8,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenGemeindenRegelung_39',
            bezeichnung: 'ddddd',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 12,
                      },
                      {
                        jahr: 2024,
                        betrag: 123,
                      },
                      {
                        jahr: 2025,
                        betrag: 123,
                      },
                      {
                        jahr: 2026,
                        betrag: 1122,
                      },
                      {
                        jahr: 2027,
                        betrag: 312,
                      },
                    ],
                    zeileKey: 'zeile_0',
                    bezeichnung: 'Bund gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 312,
                      },
                      {
                        jahr: 2024,
                        betrag: 312,
                      },
                      {
                        jahr: 2025,
                        betrag: 312,
                      },
                      {
                        jahr: 2026,
                        betrag: 3312,
                      },
                      {
                        jahr: 2027,
                        betrag: 1254,
                      },
                    ],
                    zeileKey: 'zeile_1',
                    bezeichnung: 'Länder gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 5456,
                      },
                      {
                        jahr: 2024,
                        betrag: 4654,
                      },
                      {
                        jahr: 2025,
                        betrag: 65454,
                      },
                      {
                        jahr: 2026,
                        betrag: 65454,
                      },
                      {
                        jahr: 2027,
                        betrag: 654,
                      },
                    ],
                    zeileKey: 'zeile_2',
                    bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
                  },
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 645,
                      },
                      {
                        jahr: 2024,
                        betrag: 645,
                      },
                      {
                        jahr: 2025,
                        betrag: 645,
                      },
                      {
                        jahr: 2026,
                        betrag: 645,
                      },
                      {
                        jahr: 2027,
                        betrag: 645,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Gesamtsumme',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenGesamtsummeRegelung_3',
            bezeichnung: '',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 465,
                      },
                      {
                        jahr: 2024,
                        betrag: 21321,
                      },
                      {
                        jahr: 2025,
                        betrag: 78798,
                      },
                      {
                        jahr: 2026,
                        betrag: 3545,
                      },
                      {
                        jahr: 2027,
                        betrag: 1554,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenLaenderRegelung_1',
            bezeichnung: 'Länder',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 4,
                      },
                      {
                        jahr: 2024,
                        betrag: 4,
                      },
                      {
                        jahr: 2025,
                        betrag: 4,
                      },
                      {
                        jahr: 2026,
                        betrag: 4,
                      },
                      {
                        jahr: 2027,
                        betrag: 5,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenLaenderRegelung_37',
            bezeichnung: 'fgfg',
          },
          {
            regelungstabellen: [
              {
                regelungstabellenZeilen: [
                  {
                    regelungstabellenZeileWerte: [
                      {
                        jahr: 2023,
                        betrag: 5,
                      },
                      {
                        jahr: 2024,
                        betrag: 6,
                      },
                      {
                        jahr: 2025,
                        betrag: 7,
                      },
                      {
                        jahr: 2026,
                        betrag: 77,
                      },
                      {
                        jahr: 2027,
                        betrag: 88,
                      },
                    ],
                    zeileKey: 'zeile_summe',
                    bezeichnung: 'Summe',
                  },
                ],
                tabellenKey: 'table_0',
                bezeichnung: '',
              },
            ],
            regelungKey: 'ZweckausgabenLaenderRegelung_38',
            bezeichnung: 'ghgh',
          },
        ],
        typ: 'EA_OEHH',
      },
    };
    egfaModuleControllerStub.returns(of(inputData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    auswirkungenEinnahmenAusgabenController.exportAuswirkungenEinnahmenAusgabenToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = JSON.stringify(egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO);

      expect(x.includes('"id":"sonstigerBereicheTable"')).to.be.true;
      expect(x.includes('"id":"sonstigerBereicheGesamtTable"')).to.be.true;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenTable"')).to.be.true;
      expect(x.includes('"id":"sonstigerBereicheEinnahmenGesamtTable"')).to.be.true;

      expect(x.includes('"id":"normadressatAusgabenDurchTable"')).to.be.true;
      expect(x.includes('"id":"normadressatAusgabenGesamtTable"')).to.be.true;

      expect(x.includes('"id":"vorblattAusgaben"')).to.be.true;
      expect(x.includes('"id":"vorblattAusgabenBundTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattAusgabenLaenderTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattAusgabenGemeindenTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattAusgabenGesamtsummeTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattEinnahmenLaenderTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattEinnahmenBundTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattEinnahmenGemeindenTable0"')).to.be.true;
      expect(x.includes('"id":"vorblattEinnahmenGesamtsummeTable0"')).to.be.true;

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
