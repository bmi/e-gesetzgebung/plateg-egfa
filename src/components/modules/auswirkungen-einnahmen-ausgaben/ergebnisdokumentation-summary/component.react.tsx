// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEaOehhEntityDTOAllOf } from '@plateg/rest-api';

import {
  aenderungenSteuereinnahmenRegelungKeysMap,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  verwaltungsUndSonstigeRegelungKeysMap,
  verwaltungsvollzugKeysMap,
  zweckausgabenRegelungKeysMap,
} from '../controller';
import { KompensationKostenSummaryComponent } from '../zusammenfassung/kompensation-kosten-summary/component.react';
import { NormadressatSummaryComponent } from '../zusammenfassung/normadressat-summary/component.react';
import { PatternSummaryComponent } from '../zusammenfassung/pattern-summary/component.react';
import { SonstigerBereicheStaatesSummaryComponent } from '../zusammenfassung/sonstigerBereicheStaates-summary/component.react';
import { VorblattSummaryComponent } from '../zusammenfassung/vorblatt-summary/component.react';

interface ErgebnisdokumentationSummaryProps {
  formData: EgfaModuleEaOehhEntityDTOAllOf;
}
export function ErgebnisdokumentationSummaryComponent(props: ErgebnisdokumentationSummaryProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const translationKey = `egfa.modules.einnahmenAusgaben`;
  return (
    <>
      <Title level={2}>{t(`${translationKey}.fertigstellung.generalTitlePart`)}</Title>
      <Title level={3}>{t(`${translationKey}.vorblatt.haushaltsausgabenTitle`)}</Title>
      <VorblattSummaryComponent formData={props.formData} translationKey={translationKey} />
      <Title level={2}>{t(`${translationKey}.zusammenfassung.auswirkungenEinzelnenBereicheTitle`)}</Title>
      <Title level={3}>{t(`${translationKey}.fertigstellung.haushaltsausgabenTitle`)}</Title>
      <PatternSummaryComponent
        formData={props.formData}
        translationKey={`${translationKey}.zweckausgaben`}
        widgetRegelungKeys={zweckausgabenRegelungKeysMap}
        influenceExpectedKey={InfluenceExpectedEnum.Zweckausgaben}
        influenceSummaryKey={InfluenceSummaryEnum.Zweckausgaben}
        influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Zweckausgaben}
        regelungKeysList={Object.values(zweckausgabenRegelungKeysMap)}
      />
      <PatternSummaryComponent
        formData={props.formData}
        translationKey={`${translationKey}.vollzugsaufwand`}
        widgetRegelungKeys={verwaltungsvollzugKeysMap}
        influenceExpectedKey={InfluenceExpectedEnum.AusgabenVerwaltungsvollzug}
        influenceSummaryKey={InfluenceSummaryEnum.AusgabenVerwaltungsvollzug}
        influenceStellenplaeneKey={InfluenceStellenplaeneEnum.AusgabenVerwaltungsvollzug}
        influenceStellenplaeneSummaryKey={InfluenceStellenplaeneSummaryEnum.AusgabenVerwaltungsvollzug}
        regelungKeysList={Object.values(verwaltungsvollzugKeysMap)}
      />
      <NormadressatSummaryComponent formData={props.formData} translationKey={translationKey} />
      <PatternSummaryComponent
        formData={props.formData}
        translationKey={`${translationKey}.verwaltungsUndSonstige`}
        widgetRegelungKeys={verwaltungsUndSonstigeRegelungKeysMap}
        influenceExpectedKey={InfluenceExpectedEnum.Einnahmen}
        influenceSummaryKey={InfluenceSummaryEnum.Einnahmen}
        influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Einnahmen}
        regelungKeysList={Object.values(verwaltungsUndSonstigeRegelungKeysMap)}
      />
      <PatternSummaryComponent
        formData={props.formData}
        translationKey={`${translationKey}.aenderungenSteuereinnahmen`}
        widgetRegelungKeys={aenderungenSteuereinnahmenRegelungKeysMap}
        influenceExpectedKey={InfluenceExpectedEnum.Steuereinnahmen}
        influenceSummaryKey={InfluenceSummaryEnum.Steuereinnahmen}
        influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Steuereinnahmen}
        regelungKeysList={Object.values(aenderungenSteuereinnahmenRegelungKeysMap)}
      />
      <SonstigerBereicheStaatesSummaryComponent formData={props.formData} translationKey={translationKey} />
      {props.formData.influenceGeplanteKompensationDerKostenSummary && (
        <KompensationKostenSummaryComponent formData={props.formData} translationKey={translationKey} />
      )}
    </>
  );
}
