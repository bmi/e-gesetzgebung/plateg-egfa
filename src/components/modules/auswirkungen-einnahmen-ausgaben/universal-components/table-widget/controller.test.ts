// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai from 'chai';
import sinon from 'sinon';

import { EgfaModuleEaOehhRegelungEntityDTO, EgfaModuleEaOehhRegelungstabelleEntityDTO } from '@plateg/rest-api';

import { WidgetRegelungKeysMapInterface, zweckausgabenRegelungKeysMap } from '../../controller';
import { TableWidgetController } from './controller';

const regList_AddTable: EgfaModuleEaOehhRegelungEntityDTO[] = [
  {
    regelungKey: `VerwaltungsvollzugStellenplaeneRegelung_0`,
    bezeichnung: '',
    regelungstabellen: [
      {
        tabellenKey: 'table_0',
        bezeichnung: '',
        regelungstabellenZeilen: [
          {
            zeileKey: `zeile_0`,
            bezeichnung: 'Bund gesamt',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
          {
            zeileKey: `zeile_1`,
            bezeichnung: 'Länder gesamt',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
          {
            zeileKey: `zeile_2`,
            bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
          {
            zeileKey: `zeile_summe`,
            bezeichnung: 'Gesamtsumme',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
        ],
      },
    ],
  },
];

const regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] = [
  {
    regelungKey: 'ZweckausgabenBundRegelung_0',
    bezeichnung: 'Bund 1',
    regelungstabellen: [
      {
        tabellenKey: 'table_0',
        bezeichnung: 'Table 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: 'Table 0 plan 0',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 0,
              },
              {
                jahr: 2022,
                betrag: 0,
              },
              {
                jahr: 2023,
                betrag: 0,
              },
              {
                jahr: 2024,
                betrag: 0,
              },
              {
                jahr: 2025,
                betrag: 0,
              },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    bezeichnung: 'Bund 2',
    regelungKey: 'ZweckausgabenBundRegelung_1',
    regelungstabellen: [
      {
        tabellenKey: 'Tabelle_1',
        bezeichnung: 'Table 1',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: 'Table 1 plan 0',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 0,
              },
              {
                jahr: 2022,
                betrag: 0,
              },
              {
                jahr: 2023,
                betrag: 0,
              },
              {
                jahr: 2024,
                betrag: 0,
              },
              {
                jahr: 2025,
                betrag: 0,
              },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              {
                jahr: 2021,
                betrag: 10,
              },
              {
                jahr: 2022,
                betrag: 10,
              },
              {
                jahr: 2023,
                betrag: 10,
              },
              {
                jahr: 2024,
                betrag: 10,
              },
              {
                jahr: 2025,
                betrag: 10,
              },
            ],
          },
        ],
      },
    ],
  },
];
const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = zweckausgabenRegelungKeysMap;

const yearsRange = [2021, 2022, 2023, 2024, 2025];
const emptyRowData = yearsRange.map((year) => {
  return {
    jahr: year,
    betrag: undefined,
  };
});

describe('TableWidgetController', () => {
  const ctrl = new TableWidgetController();
  const updateRegelungsListSpy = sinon.spy((list: EgfaModuleEaOehhRegelungEntityDTO[]) => {});
  const getActualRegelungsListMock = () => {
    return regelungsList;
  };
  const getActualRegelungsListMockAddTable = () => {
    return regList_AddTable;
  };

  const setRegelungsListSpy = sinon.spy((list: EgfaModuleEaOehhRegelungEntityDTO[]) => {});
  const handleFormChangeSpy = sinon.spy(() => {});

  beforeEach(() => {
    updateRegelungsListSpy.resetHistory();
    setRegelungsListSpy.resetHistory();
    handleFormChangeSpy.resetHistory();
  });

  after(() => {});
  it('deleteRegelung ', () => {
    ctrl.deleteRegelung(
      'ZweckausgabenBundRegelung_1',
      getActualRegelungsListMock,
      updateRegelungsListSpy,
      setRegelungsListSpy,
      handleFormChangeSpy,
    );
    sinon.assert.calledOnce(updateRegelungsListSpy);
    sinon.assert.calledWithExactly(updateRegelungsListSpy, [regelungsList[0]]);
    sinon.assert.calledOnce(setRegelungsListSpy);
    sinon.assert.calledWithExactly(setRegelungsListSpy, [regelungsList[0]]);
    sinon.assert.calledOnce(handleFormChangeSpy);
  });
  it('deleteRegelung without handleFormChange', () => {
    ctrl.deleteRegelung(
      'ZweckausgabenBundRegelung_1',
      getActualRegelungsListMock,
      updateRegelungsListSpy,
      setRegelungsListSpy,
    );
    sinon.assert.notCalled(handleFormChangeSpy);
  });

  it('addRegelung ', () => {
    const newRegelung: EgfaModuleEaOehhRegelungEntityDTO = {
      bezeichnung: '',
      regelungKey: `ZweckausgabenBundRegelung_${regelungsList.length}`,
      regelungstabellen: [
        {
          tabellenKey: 'table_0',
          bezeichnung: '',
          regelungstabellenZeilen: [
            {
              zeileKey: `zeile_0`,
              bezeichnung: '',
              regelungstabellenZeileWerte: emptyRowData,
            },
            {
              zeileKey: `zeile_summe`,
              bezeichnung: 'Summe',
              regelungstabellenZeileWerte: emptyRowData,
            },
          ],
        },
      ],
    };
    ctrl.addRegelung({
      regelungsListLenght: regelungsList.length,
      yearsRange,
      getActualRegelungsList: getActualRegelungsListMock,
      updateRegelungsList: updateRegelungsListSpy,
      setRegelungsList: setRegelungsListSpy,
      widgetRegelungKey: 'ZweckausgabenBund',
      hasOnlySumme: false,
      handeFormChange: handleFormChangeSpy,
    });
    const result = [...regelungsList, newRegelung];
    sinon.assert.calledOnce(updateRegelungsListSpy);
    sinon.assert.calledWithExactly(updateRegelungsListSpy, result);
    sinon.assert.calledOnce(setRegelungsListSpy);
    sinon.assert.calledWithExactly(setRegelungsListSpy, result);
    sinon.assert.calledOnce(handleFormChangeSpy);
  });
  it('addRegelung without handleFormChange', () => {
    const newRegelungOnlySumme: EgfaModuleEaOehhRegelungEntityDTO = {
      bezeichnung: '',
      regelungKey: `ZweckausgabenBundRegelung_${regelungsList.length}`,
      regelungstabellen: [
        {
          tabellenKey: `table_0`,
          bezeichnung: '',
          regelungstabellenZeilen: [
            {
              zeileKey: `zeile_summe`,
              bezeichnung: 'Summe',
              regelungstabellenZeileWerte: emptyRowData,
            },
          ],
        },
      ],
    };
    ctrl.addRegelung({
      regelungsListLenght: regelungsList.length,
      yearsRange,
      getActualRegelungsList: getActualRegelungsListMock,
      updateRegelungsList: updateRegelungsListSpy,
      setRegelungsList: setRegelungsListSpy,
      widgetRegelungKey: 'ZweckausgabenBund',
      hasOnlySumme: true,
    });
    const result = [...regelungsList, newRegelungOnlySumme];

    sinon.assert.calledOnce(updateRegelungsListSpy);
    sinon.assert.calledWithExactly(updateRegelungsListSpy, result);
    sinon.assert.calledOnce(setRegelungsListSpy);
    sinon.assert.calledWithExactly(setRegelungsListSpy, result);
    sinon.assert.notCalled(handleFormChangeSpy);
  });

  it('addTableWithStaticRows', () => {
    const newTable: EgfaModuleEaOehhRegelungEntityDTO = {
      regelungKey: `ZweckausgabenGesamtsummeRegelung_${regelungsList.length}`,
      bezeichnung: '',
      regelungstabellen: [
        {
          tabellenKey: 'table_0',
          bezeichnung: '',
          regelungstabellenZeilen: [
            {
              zeileKey: `zeile_0`,
              bezeichnung: 'Bund gesamt',
              regelungstabellenZeileWerte: emptyRowData,
            },
            {
              zeileKey: `zeile_1`,
              bezeichnung: 'Länder gesamt',
              regelungstabellenZeileWerte: emptyRowData,
            },
            {
              zeileKey: `zeile_2`,
              bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
              regelungstabellenZeileWerte: emptyRowData,
            },
            {
              zeileKey: `zeile_summe`,
              bezeichnung: 'Gesamtsumme',
              regelungstabellenZeileWerte: emptyRowData,
            },
          ],
        },
      ],
    };
    ctrl.addTableWithStaticRows(
      regelungsList.length,
      yearsRange,
      getActualRegelungsListMock,
      updateRegelungsListSpy,
      setRegelungsListSpy,
      'ZweckausgabenGesamtsumme',
      [
        { title: 'Bund gesamt', isSumme: false },
        { title: 'Länder gesamt', isSumme: false },
        { title: 'Gemeinden/Gemeindeverbände gesamt', isSumme: false },
        { title: 'Gesamtsumme', isSumme: true },
      ],
    );
    const result = [...regelungsList, newTable];
    sinon.assert.calledOnce(updateRegelungsListSpy);
    sinon.assert.calledWithExactly(updateRegelungsListSpy, result);
    sinon.assert.calledOnce(setRegelungsListSpy);
    sinon.assert.calledWithExactly(setRegelungsListSpy, result);
  });

  it('addTable', () => {
    const regIndex = 0;
    const tableListLength = regList_AddTable[regIndex].regelungstabellen.length;

    const newTable: EgfaModuleEaOehhRegelungstabelleEntityDTO = {
      tabellenKey: `table_${tableListLength}`,
      bezeichnung: '',
      regelungstabellenZeilen: [
        {
          zeileKey: `zeile_0`,
          bezeichnung: 'Bund gesamt',
          regelungstabellenZeileWerte: emptyRowData,
        },
        {
          zeileKey: `zeile_1`,
          bezeichnung: 'Länder gesamt',
          regelungstabellenZeileWerte: emptyRowData,
        },
        {
          zeileKey: `zeile_2`,
          bezeichnung: 'Gemeinden/Gemeindeverbände gesamt',
          regelungstabellenZeileWerte: emptyRowData,
        },
        {
          zeileKey: `zeile_summe`,
          bezeichnung: 'Gesamtsumme',
          regelungstabellenZeileWerte: emptyRowData,
        },
      ],
    };

    ctrl.addTable({
      regelungsIndex: regIndex,
      tableListLength,
      rowsLabels: [
        { title: 'Bund gesamt', isSumme: false },
        { title: 'Länder gesamt', isSumme: false },
        { title: 'Gemeinden/Gemeindeverbände gesamt', isSumme: false },
        { title: 'Gesamtsumme', isSumme: true },
      ],
      yearsRange,
      getActualRegelungsList: getActualRegelungsListMockAddTable,
      updateRegelungsList: updateRegelungsListSpy,
      setRegelungsList: setRegelungsListSpy,
      handeFormChange: handleFormChangeSpy,
    });

    regList_AddTable[regIndex].regelungstabellen.push(newTable);

    sinon.assert.calledOnce(updateRegelungsListSpy);
    sinon.assert.calledWithExactly(updateRegelungsListSpy, regList_AddTable);
    sinon.assert.calledOnce(setRegelungsListSpy);
    sinon.assert.calledWithExactly(setRegelungsListSpy, regList_AddTable);
  });
});
