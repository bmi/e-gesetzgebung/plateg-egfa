// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEaOehhRegelungstabelleEntityDTO,
  EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO,
} from '@plateg/rest-api';
export interface RowLabelsInterface {
  title: string;
  isSumme: boolean;
}

interface AddRegelungArgsType {
  regelungsListLenght: number;
  yearsRange: number[];
  getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[];
  updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  widgetRegelungKey: string;
  hasOnlySumme?: boolean;
  handeFormChange?: () => void;
}

interface AddTableArgsType {
  regelungsIndex: number;
  tableListLength: number;
  rowsLabels: RowLabelsInterface[];
  yearsRange: number[];
  getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[];
  updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  handeFormChange?: () => void;
}

export class TableWidgetController {
  /**
   * Delete regelung
   * @param regelungKey e.g. "ZweckausgabenLaenderRegelung_5"
   */
  public deleteRegelung(
    regelungKey: string,
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    handeFormChange?: () => void,
  ): void {
    const formData = getActualRegelungsList();
    const filteredList = formData
      .filter((regelung) => regelung.regelungKey !== regelungKey)
      .map((regelung, index) => {
        // recalculate regelungKey
        const keyArray = regelung.regelungKey.split('_');
        keyArray[1] = index.toString();
        regelung.regelungKey = keyArray.join('_');

        return regelung;
      });
    updateRegelungsList(filteredList);
    setRegelungsList(filteredList);
    if (handeFormChange) {
      handeFormChange();
    }
  }
  public deleteTable(
    regelungIndex: number,
    tableIndex: number,
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    handeFormChange?: () => void,
  ) {
    const formData = getActualRegelungsList();

    const filtered = formData.map((regelung) => {
      if (regelung.regelungKey.includes(regelungIndex.toString())) {
        regelung.regelungstabellen = regelung.regelungstabellen
          .filter((table) => !table.tabellenKey.includes(tableIndex.toString()))
          .map((table, index) => {
            const keyArray = table.tabellenKey.split('_');
            keyArray[1] = index.toString();
            table.tabellenKey = keyArray.join('_');
            return table;
          });
      }
      return regelung;
    });

    updateRegelungsList(filtered);
    setRegelungsList(filtered);
    if (handeFormChange) {
      handeFormChange();
    }
  }

  public addTable(argument: AddTableArgsType) {
    const {
      getActualRegelungsList,
      regelungsIndex,
      rowsLabels,
      setRegelungsList,
      tableListLength: tablesListLength,
      updateRegelungsList,
      yearsRange,
      handeFormChange,
    } = argument;
    const regelungstabellenZeilen = rowsLabels.map((label, index) => {
      let key = `zeile_${index}`;
      if (label.isSumme) {
        key = 'zeile_summe';
      }

      return {
        zeileKey: `${key}`,
        bezeichnung: label.title,
        regelungstabellenZeileWerte: this.prepareEmptyTableRowDate(yearsRange),
      };
    });

    const newTable: EgfaModuleEaOehhRegelungstabelleEntityDTO = {
      bezeichnung: '',
      regelungstabellenZeilen: regelungstabellenZeilen,
      tabellenKey: `table_${tablesListLength}`,
    };
    const formData = getActualRegelungsList();
    formData[regelungsIndex].regelungstabellen.push(newTable);

    updateRegelungsList(formData);
    setRegelungsList(formData);
    if (handeFormChange) {
      handeFormChange();
    }
  }

  public addRegelung(argument: AddRegelungArgsType): void {
    const {
      getActualRegelungsList,
      regelungsListLenght,
      setRegelungsList,
      updateRegelungsList,
      widgetRegelungKey,
      yearsRange,
      handeFormChange,
      hasOnlySumme,
    } = argument;

    const keyPrefix = `${widgetRegelungKey}Regelung_${regelungsListLenght}`;

    let regelungstabellenZeilen = [
      {
        zeileKey: `zeile_0`,
        bezeichnung: '',
        regelungstabellenZeileWerte: this.prepareEmptyTableRowDate(yearsRange),
      },
      {
        zeileKey: `zeile_summe`,
        bezeichnung: 'Summe',
        regelungstabellenZeileWerte: this.prepareEmptyTableRowDate(yearsRange),
      },
    ];
    // If table has only summe row reduce first empty row from list
    if (hasOnlySumme) {
      regelungstabellenZeilen = regelungstabellenZeilen.slice(1);
    }
    const regelungstabellen: EgfaModuleEaOehhRegelungstabelleEntityDTO[] = [
      {
        bezeichnung: '',
        regelungstabellenZeilen: regelungstabellenZeilen,
        tabellenKey: 'table_0',
      },
    ];

    const newRegelung = {
      regelungKey: `${keyPrefix}`,
      bezeichnung: '',
      regelungstabellen,
    } as EgfaModuleEaOehhRegelungEntityDTO;
    const formData = getActualRegelungsList();
    updateRegelungsList([...formData, newRegelung]);
    setRegelungsList([...formData, newRegelung]);
    if (handeFormChange) {
      handeFormChange();
    }
  }

  /**
   * Add table with static rows
   * @param regelungsListLenght
   * @param yearsRange
   * @param getActualRegelungsList
   * @param updateRegelungsList
   * @param setRegelungsList
   * @param widgetRegelungKey
   * @param rowsLabels
   */
  public addTableWithStaticRows(
    regelungsListLenght: number,
    yearsRange: number[],
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
    widgetRegelungKey: string,
    rowsLabels: RowLabelsInterface[],
  ) {
    const keyPrefix = `${widgetRegelungKey}Regelung_${regelungsListLenght}`;
    const regelungstabellenZeilen = rowsLabels.map((label, index) => {
      let key = `zeile_${index}`;
      if (label.isSumme) {
        key = 'zeile_summe';
      }

      return {
        zeileKey: `${key}`,
        bezeichnung: label.title,
        regelungstabellenZeileWerte: this.prepareEmptyTableRowDate(yearsRange),
      };
    });

    const regelungstabellen: EgfaModuleEaOehhRegelungstabelleEntityDTO[] = [
      {
        bezeichnung: '',
        regelungstabellenZeilen: regelungstabellenZeilen,
        tabellenKey: 'table_0',
      },
    ];
    const newRegelung = {
      regelungKey: `${keyPrefix}`,
      bezeichnung: '',
      regelungstabellen,
    } as EgfaModuleEaOehhRegelungEntityDTO;
    const formData = getActualRegelungsList();
    updateRegelungsList([...formData, newRegelung]);
    setRegelungsList([...formData, newRegelung]);
  }

  /**
   * Prepare data for empty row
   */
  private prepareEmptyTableRowDate(yearsRange: number[]): EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[] {
    return yearsRange.map((year) => {
      return {
        jahr: year,
        betrag: undefined,
      };
    });
  }
}
