// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React from 'react';

export interface RegelungTitleInterface {
  elementIndex: number;
  isEditable: boolean;
  regelungName: string;
  labelText: string;
  formElementName: (string | number)[];
  formElementNameKey: (string | number)[];
  widgetRegelungKey: string;
}

export function RegelungTitleComponent(props: RegelungTitleInterface): React.ReactElement {
  return (
    <>
      <div className="regelung-title-holder">
        {props.isEditable ? (
          <>
            <Form.Item name={props.formElementName} label={props.labelText} style={{ margin: 0 }}>
              <Input autoComplete="off" />
            </Form.Item>
            {/* Hidden form element for tabellenKey */}
            <Form.Item
              name={props.formElementNameKey}
              hidden={true}
              style={{ margin: 0 }}
              initialValue={`${props.widgetRegelungKey}_regelung_${props.elementIndex + 1}`}
            >
              <Input />
            </Form.Item>
          </>
        ) : (
          <strong style={{ fontSize: '16px', fontWeight: 500 }}>{props.regelungName}</strong>
        )}
      </div>
    </>
  );
}
