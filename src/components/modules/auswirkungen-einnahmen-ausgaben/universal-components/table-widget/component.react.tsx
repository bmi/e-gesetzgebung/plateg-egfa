// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './table-widget.less';

import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { DeleteOutlined, GlobalDI, PlusOutlined } from '@plateg/theme';

import { widgetRegelungKeysNormadressatMap } from '../../controller';
import { RowLabelsInterface, TableWidgetController } from './controller';
import { RegelungTableComponent } from './regelung-table/component.react';
import { RegelungTitleComponent } from './regelung-title/component.react';

export interface TableWidgetComponentInterface {
  yearsRange: number[];
  regelungsList: EgfaModuleEaOehhRegelungEntityDTO[];
  isEditable: boolean;
  updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[];
  hasCurrency: boolean;
  hasOnlySumme?: boolean;
  tableHeaderLabel: string;
  addRowLabel?: string;
  removeRowLabel?: string;
  widgetRegelungKey: string;
  handeFormChange?: () => void;
  isStaticRows?: boolean;
  hasTitle?: boolean;
  rowsLabels?: RowLabelsInterface[];
  noMoreRegelungs?: boolean;
  defaultIndex: number;
}
export interface Datarow {
  key: string;
  data: string | React.ReactElement;
  [s: number]: number | React.ReactElement;
}

export function TableWidgetComponent(props: TableWidgetComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const tableWidgetCtrl = GlobalDI.getOrRegister('tableWidgetController', () => new TableWidgetController());
  const [regelungsList, setRegelungsList] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>(props.regelungsList);
  const [regelungLabel, setRegelungLabel] = useState<string>('');

  let localRegelungIndex = 0;

  const {
    yearsRange,
    hasOnlySumme,
    handeFormChange,
    widgetRegelungKey,
    getActualRegelungsList,
    updateRegelungsList,
    rowsLabels,
  } = props;

  useEffect(() => {
    setRegelungsList(props.regelungsList);
  }, [props.regelungsList]);

  useEffect(() => {
    if (regelungsList) {
      const items = regelungsList.filter((regelung) => regelung.regelungKey.indexOf(props.widgetRegelungKey) !== -1);
      // Added empty table if it has no tables on BE
      if (items.length === 0) {
        handleAddNewRegelung(props.defaultIndex);
      }
    }
  }, [regelungsList]);

  useEffect(() => {
    const labelKey = props.hasOnlySumme ? 'regelungLabel' : 'regelungUndEinzelplans';
    setRegelungLabel(t(`egfa.modules.einnahmenAusgaben.tableWidget.${labelKey}`));
  }, [props.hasOnlySumme]);

  const handleAddNewRegelung = (defaultIndex?: number) => {
    const regelungIndex = defaultIndex ? regelungsList.length + props.defaultIndex : regelungsList.length;
    if (props.rowsLabels) {
      tableWidgetCtrl.addTableWithStaticRows(
        regelungIndex,
        props.yearsRange,
        props.getActualRegelungsList,
        props.updateRegelungsList,
        setRegelungsList,
        props.widgetRegelungKey,
        props.rowsLabels,
      );
    } else {
      tableWidgetCtrl.addRegelung({
        regelungsListLenght: regelungIndex,
        yearsRange,
        getActualRegelungsList,
        updateRegelungsList,
        setRegelungsList,
        widgetRegelungKey,
        hasOnlySumme,
        handeFormChange,
      });
    }
  };

  const handleAddNewTable = (regIndex: number, tableListLength: number) => {
    if (rowsLabels) {
      tableWidgetCtrl.addTable({
        regelungsIndex: regIndex,
        tableListLength,
        rowsLabels,
        yearsRange,
        getActualRegelungsList,
        updateRegelungsList,
        setRegelungsList,
        handeFormChange,
      });
    }
  };

  return (
    <div className="table-widget">
      {regelungsList.map((regelung, regelungIndex) => {
        if (regelung.regelungKey.indexOf(props.widgetRegelungKey) === -1) {
          return null;
        }
        localRegelungIndex++;
        return (
          <div style={{ marginTop: '12px' }} key={`${props.widgetRegelungKey}-${regelungIndex}`}>
            {props.hasTitle && (
              <RegelungTitleComponent
                key={regelungIndex}
                elementIndex={regelungIndex}
                isEditable={props.isEditable}
                regelungName={regelung.bezeichnung}
                labelText={regelungLabel}
                formElementName={['regelungen', regelungIndex, 'bezeichnung']}
                formElementNameKey={['regelungen', regelungIndex, 'regelungKey']}
                widgetRegelungKey={props.widgetRegelungKey}
              />
            )}

            {regelung.regelungstabellen.map((regTable, tableIndex) => {
              return (
                <RegelungTableComponent
                  isEditable={props.isEditable}
                  yearsRange={props.yearsRange}
                  formElementName={[
                    'regelungen',
                    regelungIndex,
                    'regelungstabellen',
                    tableIndex,
                    'regelungstabellenZeilen',
                  ]}
                  regList={regelungsList}
                  regelungIndex={regelungIndex}
                  tableIndex={tableIndex}
                  regTable={regTable}
                  updateRegelungsList={props.updateRegelungsList}
                  getActualRegelungsList={props.getActualRegelungsList}
                  hasCurrency={props.hasCurrency}
                  hasOnlySumme={props.hasOnlySumme}
                  tableHeaderLabel={props.tableHeaderLabel}
                  addRowLabel={props.addRowLabel}
                  removeRowLabel={props.removeRowLabel}
                  widgetRegelungKey={props.widgetRegelungKey}
                  handeFormChange={props.handeFormChange}
                  isStaticRows={props.isStaticRows}
                  setRegelungsList={setRegelungsList}
                />
              );
            })}
            {props.isEditable && localRegelungIndex > 1 && (
              <Button
                id={`remove-regelung-${props.widgetRegelungKey}-${regelungIndex}`}
                size="small"
                icon={<DeleteOutlined />}
                type="link"
                style={{ float: 'right', marginTop: '3px' }}
                onClick={() =>
                  tableWidgetCtrl.deleteRegelung(
                    regelung.regelungKey,
                    props.getActualRegelungsList,
                    props.updateRegelungsList,
                    setRegelungsList,
                    props.handeFormChange,
                  )
                }
                className="blue-text-button"
              >
                Regelung entfernen
              </Button>
            )}

            {props.isEditable &&
              (props.widgetRegelungKey === widgetRegelungKeysNormadressatMap['NormadressatStellenplaene'] ||
                props.widgetRegelungKey == 'VerwaltungsvollzugStellenplaene') && (
                <>
                  <Button
                    style={{ marginBottom: '5px' }}
                    className="blue-text-button"
                    type="text"
                    icon={<PlusOutlined />}
                    onClick={() => handleAddNewTable(regelungIndex, regelung.regelungstabellen.length)}
                  >
                    Einzelplan hinzufügen
                  </Button>
                </>
              )}
          </div>
        );
      })}

      {props.isEditable && !props.noMoreRegelungs && (
        <Button
          id={`add-new-regelung-${props.widgetRegelungKey}`}
          icon={<PlusOutlined />}
          type="text"
          onClick={() => handleAddNewRegelung()}
          className="blue-text-button"
        >
          {'Weitere Regelung hinzufügen'}
        </Button>
      )}
    </div>
  );
}
