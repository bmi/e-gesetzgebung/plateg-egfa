// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai from 'chai';
import sinon from 'sinon';

import { EgfaModuleEaOehhRegelungEntityDTO, EgfaModuleEaOehhRegelungstabellenZeileEntityDTO } from '@plateg/rest-api';

import { RegelungTableController } from './controller';
const regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] = [
  {
    regelungKey: '',
    bezeichnung: '',
    regelungstabellen: [
      {
        tabellenKey: 'ZweckausgabenBundRegelung_0',
        bezeichnung: 'Table 0',
        regelungstabellenZeilen: [
          {
            zeileKey: 'zeile_0',
            bezeichnung: 'Table 0 plan 0',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'zeile_1',
            bezeichnung: 'Table 0 plan 1',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 0 },
              { jahr: 2022, betrag: 0 },
              { jahr: 2023, betrag: 0 },
              { jahr: 2024, betrag: 0 },
              { jahr: 2025, betrag: 0 },
            ],
          },
          {
            zeileKey: 'zeile_summe',
            bezeichnung: 'Summe',
            regelungstabellenZeileWerte: [
              { jahr: 2021, betrag: 10 },
              { jahr: 2022, betrag: 10 },
              { jahr: 2023, betrag: 10 },
              { jahr: 2024, betrag: 10 },
              { jahr: 2025, betrag: 10 },
            ],
          },
        ],
      },
    ],
  },
];

const yearsRange = [2021, 2022, 2023, 2024, 2025];

describe('RegelungTableController', () => {
  const ctrl = new RegelungTableController();
  const updateDataSpy = sinon.spy(
    (
      formValue: EgfaModuleEaOehhRegelungEntityDTO[],
      regelungsList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    ) => {},
  );
  const getActualRegelungsListMock = () => {
    return regelungsList;
  };

  beforeEach(() => {
    updateDataSpy.resetHistory();
  });

  after(() => {});
  it('deleteRow ', () => {
    const reducedList = [
      {
        zeileKey: 'zeile_0',
        bezeichnung: 'Table 0 plan 0',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: 0 },
          { jahr: 2022, betrag: 0 },
          { jahr: 2023, betrag: 0 },
          { jahr: 2024, betrag: 0 },
          { jahr: 2025, betrag: 0 },
        ],
      },
      {
        zeileKey: 'zeile_summe',
        bezeichnung: 'Summe',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: 10 },
          { jahr: 2022, betrag: 10 },
          { jahr: 2023, betrag: 10 },
          { jahr: 2024, betrag: 10 },
          { jahr: 2025, betrag: 10 },
        ],
      },
    ];
    ctrl.deleteRow(
      'zeile_1',
      regelungsList[0].regelungstabellen[0].regelungstabellenZeilen,

      getActualRegelungsListMock,
      updateDataSpy,
    );
    sinon.assert.calledOnce(updateDataSpy);
    sinon.assert.calledWithExactly(updateDataSpy, regelungsList, reducedList);
  });

  it('addRow ', () => {
    const updatedList = [
      {
        zeileKey: 'zeile_0',
        bezeichnung: 'Table 0 plan 0',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: 0 },
          { jahr: 2022, betrag: 0 },
          { jahr: 2023, betrag: 0 },
          { jahr: 2024, betrag: 0 },
          { jahr: 2025, betrag: 0 },
        ],
      },
      {
        zeileKey: 'zeile_1',
        bezeichnung: 'Table 0 plan 1',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: 0 },
          { jahr: 2022, betrag: 0 },
          { jahr: 2023, betrag: 0 },
          { jahr: 2024, betrag: 0 },
          { jahr: 2025, betrag: 0 },
        ],
      },
      {
        zeileKey: 'zeile_2',
        bezeichnung: '',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: undefined },
          { jahr: 2022, betrag: undefined },
          { jahr: 2023, betrag: undefined },
          { jahr: 2024, betrag: undefined },
          { jahr: 2025, betrag: undefined },
        ],
      },
      {
        zeileKey: 'zeile_summe',
        bezeichnung: 'Summe',
        regelungstabellenZeileWerte: [
          { jahr: 2021, betrag: 10 },
          { jahr: 2022, betrag: 10 },
          { jahr: 2023, betrag: 10 },
          { jahr: 2024, betrag: 10 },
          { jahr: 2025, betrag: 10 },
        ],
      },
    ];
    ctrl.addRow(0, yearsRange, getActualRegelungsListMock, updateDataSpy);
    sinon.assert.calledOnce(updateDataSpy);
    sinon.assert.calledWithExactly(updateDataSpy, regelungsList, updatedList);
  });
});
