// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EgfaModuleEaOehhRegelungEntityDTO, EgfaModuleEaOehhRegelungstabellenZeileEntityDTO } from '@plateg/rest-api';
export class RegelungTableController {
  /**
   * Delete row
   * Delete row from table and recalculate zeileKey to keep it in correct order(avoid Summe row)
   * @param zeileKey: string
   */
  public deleteRow(
    zeileKey: string,
    tableData: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateData: (
      formValue: EgfaModuleEaOehhRegelungEntityDTO[],
      zeilenList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    ) => void,
  ): void {
    const filteredList = tableData
      .filter((zeil) => !zeil.zeileKey.includes(zeileKey))
      .map((zeil, index) => {
        if (zeil.bezeichnung === 'Summe') {
          return zeil;
        }
        // recalculate zeileKey
        zeil.zeileKey = `zeile_${index}`;
        return zeil;
      });

    const formValue = getActualRegelungsList();
    updateData(formValue, filteredList);
  }

  /**
   * Add new row to the table and update form instance
   */
  public addRow(
    regelungIndex: number,
    yearsRange: number[],
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateData: (
      formValue: EgfaModuleEaOehhRegelungEntityDTO[],
      zeilenList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    ) => void,
  ): void {
    const formValue = getActualRegelungsList();
    const rowsList = formValue[regelungIndex].regelungstabellen[0].regelungstabellenZeilen;
    // Take Summe row to keep it in correct place always on the last position
    const summeRow = rowsList.pop();
    const newRow = {
      zeileKey: `zeile_${rowsList.length}`,
      bezeichnung: '',
      regelungstabellenZeileWerte: yearsRange.map((year) => {
        return {
          jahr: year,
          betrag: undefined,
        };
      }),
    };
    rowsList.push(newRow);
    // Put Summe on correct place to the last position
    rowsList.push(summeRow as EgfaModuleEaOehhRegelungstabellenZeileEntityDTO);
    // Update local data and form instance
    updateData(formValue, rowsList);
  }
}
