// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './regelung-table.less';

import { Button, Form, Input, InputNumber, Table } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import {
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEaOehhRegelungstabelleEntityDTO,
  EgfaModuleEaOehhRegelungstabellenZeileEntityDTO,
  EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO,
} from '@plateg/rest-api';
import { DeleteOutlined, GlobalDI, PlusOutlined, TableScroll } from '@plateg/theme';

import { widgetRegelungKeysNormadressatMap } from '../../../controller';
import { Datarow } from '../component.react';
import { TableWidgetController } from '../controller';
import { RegelungTableController } from './controller';

export interface RegelungTableComponentInterface {
  isEditable: boolean;
  yearsRange: number[];
  formElementName: (string | number)[];
  regelungIndex: number;
  regTable: EgfaModuleEaOehhRegelungstabelleEntityDTO;
  updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[];
  hasCurrency: boolean;
  hasOnlySumme?: boolean;
  tableHeaderLabel: string;
  addRowLabel?: string;
  removeRowLabel?: string;
  widgetRegelungKey: string;
  handeFormChange?: () => void;
  isStaticRows?: boolean;
  tableIndex: number;
  setRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => void;
  regList: EgfaModuleEaOehhRegelungEntityDTO[];
}

type YearsValuesTypeUnion = number | null | React.ReactElement;
type ColumsType = {
  title: string | React.ReactElement;
  dataIndex: string;
  fixed: boolean;
  rowScope?: 'row';
}[];

export function RegelungTableComponent(props: RegelungTableComponentInterface): React.ReactElement {
  const regelungTableCtrl = GlobalDI.getOrRegister('regelungTableController', () => new RegelungTableController());
  const tableWidgetCtrl = GlobalDI.getOrRegister('tableWidgetController', () => new TableWidgetController());
  const [rowData, setRowData] = useState(props.regTable.regelungstabellenZeilen);
  const [data, setData] = useState<Datarow[]>();

  useEffect(() => {
    const rowDataLocal = props.regTable.regelungstabellenZeilen;
    setRowData(rowDataLocal);
    const updatedData: Datarow[] = [];
    if (!props.hasOnlySumme) {
      updatedData.push(...getZeilenRow(rowDataLocal, props.isEditable, props.isStaticRows));
    }
    if (props.isEditable && !props.hasOnlySumme && !props.isStaticRows) {
      updatedData.push({
        key: uuidv4() + '-add-row',
        data: (
          <Button
            id={`egfa-addZeil-row-${props.regelungIndex}-${props.tableIndex}`}
            type="link"
            onClick={() =>
              regelungTableCtrl.addRow(
                props.regelungIndex,
                props.yearsRange,
                props.getActualRegelungsList,
                updateDataRow,
              )
            }
            className="blue-text-button"
          >
            <PlusOutlined />
            {props.addRowLabel || `Einzelplan hinzufügen`}
          </Button>
        ),
      });
    }
    updatedData.push(...getSummeRow(rowDataLocal, props.isEditable));
    setData(updatedData);
  }, [props.regList]);

  // Prepare columns based on selected years range
  let columns: ColumsType = props.yearsRange.map((year) => {
    return { title: year.toString(), dataIndex: year.toString(), fixed: false };
  });
  let tableHeader: string | React.ReactElement = props.tableHeaderLabel;
  if (
    props.widgetRegelungKey === widgetRegelungKeysNormadressatMap['NormadressatStellenplaene'] ||
    props.widgetRegelungKey == 'VerwaltungsvollzugStellenplaene'
  ) {
    const formElementNameCopy = [...props.formElementName];
    formElementNameCopy[formElementNameCopy.length - 1] = 'bezeichnung';
    tableHeader = props.isEditable ? (
      <Form.Item name={[...formElementNameCopy]}>
        <Input height={'auto'} size="small" />
      </Form.Item>
    ) : (
      <div className="bezeichnung-cell text">{props.regTable.bezeichnung}</div>
    );
  }

  columns = [{ title: tableHeader, dataIndex: 'data', rowScope: 'row', fixed: true }, ...columns];
  if (props.isEditable) {
    columns = [...columns];
  }

  /**
   * Get data for title depends on isEditable mode
   * true - return form input
   * false - just a corresponding value for this cell
   * @param bezeichnung
   * @param zeilIndex
   * @param isEditable
   * @returns
   */
  const getBezeichnungCell = (
    bezeichnung: string,
    zeileKey: string,
    zeilIndex: number,
    isEditable: boolean,
    isStaticRows = false,
  ) => {
    let cellContent: string | null | React.ReactElement = <div className="bezeichnung-cell text">{bezeichnung}</div>;
    if (isEditable && !isStaticRows) {
      cellContent = (
        <div className="bezeichnung-cell">
          <Form.Item name={[...props.formElementName, zeilIndex, 'bezeichnung']}>
            <TextArea autoSize autoComplete="off" />
          </Form.Item>
          {zeilIndex > 0 && (
            <Button
              className="delete-einzelplan blue-text-button"
              type="link"
              onClick={() =>
                regelungTableCtrl.deleteRow(zeileKey, rowData, props.getActualRegelungsList, updateDataRow)
              }
            >
              <DeleteOutlined />
              {props.removeRowLabel ?? 'Einzelplan entfernen'}
            </Button>
          )}
        </div>
      );
    }
    return cellContent;
  };
  /**
   * Get data for all rows avoid Summe
   * @param zeilList
   * @param isEditable
   * @returns
   */
  const getZeilenRow = (
    zeilList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    isEditable: boolean,
    isStaticRows = false,
  ): Datarow[] => {
    return zeilList
      .filter((ziel) => ziel.bezeichnung !== 'Summe')
      .map((ziel: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO, index: number) => {
        return {
          key: ziel.zeileKey,
          data: getBezeichnungCell(ziel.bezeichnung, ziel.zeileKey, index, isEditable, isStaticRows),
          ...getYearsValues(ziel.regelungstabellenZeileWerte, index, isEditable),
        };
      });
  };

  /**
   * Get data only for Summe row
   * @param zeilList
   * @param isEditable
   * @returns
   */
  const getSummeRow = (zeilList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[], isEditable: boolean): Datarow[] => {
    return zeilList
      .filter((ziel) => ziel.bezeichnung === 'Summe')
      .map((ziel: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO) => {
        return {
          key: ziel.zeileKey,
          data: <div className="bezeichnung-cell summe">{ziel.bezeichnung}</div>,
          ...getYearsValues(ziel.regelungstabellenZeileWerte, zeilList.length - 1, isEditable),
        };
      });
  };

  const currencyParser = (val: string | undefined): string | number => {
    // for when the input gets clears
    if (typeof val === 'string' && !val.length) {
      val = '';
    }

    if (!val) {
      val = '';
    }

    // detecting and parsing between comma and dot
    const group = new Intl.NumberFormat('de-DE').format(1111).replace(/1/g, '');
    const decimal = new Intl.NumberFormat('de-DE').format(1.1).replace(/1/g, '');
    let reversedVal: string | number = val.replace(new RegExp('\\' + group, 'g'), '');
    reversedVal = reversedVal.replace(new RegExp('\\' + decimal, 'g'), '.');

    // removing everything except the digits and dot
    reversedVal = reversedVal.replace(/[^0-9.]/g, '');

    // appending digits properly
    const digitsAfterDecimalCount = (reversedVal.split('.')[1] || []).length;
    const needsDigitsAppended = digitsAfterDecimalCount > 2;

    if (needsDigitsAppended) {
      reversedVal = (reversedVal as any) * Math.pow(10, digitsAfterDecimalCount - 2);
    }
    return Number.isNaN(reversedVal) ? '' : reversedVal;
  };

  const currencyFormatter = (value: string | number | undefined) => {
    if (typeof value === 'string' && !value.length) {
      return '';
    }
    return new Intl.NumberFormat('de-DE').format(value as number);
  };

  /**
   * Prepare content for years cells depends on if isEditable mode true or false
   * true - return form input
   * false - just a corresponding value for this cell
   * @param values
   * @param zeilIndex
   * @param isEditable
   * @returns
   */
  const getYearsValues = (
    values: EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[],
    zeilIndex: number,
    isEditable: boolean,
  ): { [s: number]: YearsValuesTypeUnion } => {
    const valuesObj: { [s: number]: YearsValuesTypeUnion } = {};
    values.forEach((value, valueIndex) => {
      // set text for cell content
      let cellContent: YearsValuesTypeUnion = (
        <div className="year-cell" role="cell">
          <div className="value">{value.betrag && value.betrag.toLocaleString('de-DE')}</div>
          {props.hasCurrency && <div className="currency-label">T€</div>}
        </div>
      );
      // if is Editable mode true - replace it with form element
      if (isEditable) {
        cellContent = (
          <div className="year-cell" role="cell">
            <Form.Item
              // initialValue={''}
              name={[...props.formElementName, zeilIndex, 'regelungstabellenZeileWerte', valueIndex, 'betrag']}
            >
              <InputNumber
                defaultValue={''}
                parser={currencyParser}
                formatter={(val) => currencyFormatter(val)}
                controls={false}
              />
            </Form.Item>
            <Form.Item
              name={[...props.formElementName, zeilIndex, 'regelungstabellenZeileWerte', valueIndex, 'jahr']}
              hidden={true}
            >
              <Input />
            </Form.Item>
            {props.hasCurrency && (
              <label
                htmlFor={[
                  ...props.formElementName,
                  zeilIndex,
                  'regelungstabellenZeileWerte',
                  valueIndex,
                  'betrag',
                ].join('_')}
                className="currency-label"
                aria-label="T€"
              >
                T€
              </label>
            )}
          </div>
        );
      }
      valuesObj[value.jahr] = cellContent;
    });
    return valuesObj;
  };

  /**
   * Update data in form instance and set actual data to local table state
   * @param formValue
   * @param tabellenZeilenList
   */
  const updateDataRow = (
    formValue: EgfaModuleEaOehhRegelungEntityDTO[],
    zeilenList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
  ) => {
    const newFormVal = formValue.map((item) => {
      if (item.regelungKey.includes(props.regelungIndex.toString())) {
        // regelungstabellen[0] for data row should be ok
        item.regelungstabellen[0].regelungstabellenZeilen = zeilenList;
      }
      return item;
    });
    props.updateRegelungsList(newFormVal);

    setRowData([...zeilenList]);
    if (props.handeFormChange) {
      props.handeFormChange();
    }
  };
  const handleRemoveTable = (
    regIndex: number,
    tableIndex: number,
    getActualRegelungsList: () => EgfaModuleEaOehhRegelungEntityDTO[],
    updateRegelungsList: (regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]) => void,
  ) => {
    tableWidgetCtrl.deleteTable(
      regIndex,
      tableIndex,
      getActualRegelungsList,
      updateRegelungsList,
      props.setRegelungsList,
      props.handeFormChange,
    );
  };

  const showAddTableButton = (props.widgetRegelungKey ===
    widgetRegelungKeysNormadressatMap['NormadressatStellenplaene'] ||
    props.widgetRegelungKey == 'VerwaltungsvollzugStellenplaene') &&
    props.tableIndex != 0 &&
    props.isEditable && (
      <Button
        id={`remove-table-${props.widgetRegelungKey}-${props.regelungIndex}-${props.tableIndex}`}
        type="text"
        style={{
          marginTop: '10px',
          float: 'right',
        }}
        size="small"
        className="blue-text-button"
        icon={<DeleteOutlined />}
        onClick={() =>
          handleRemoveTable(
            props.regelungIndex,
            props.tableIndex,
            props.getActualRegelungsList,
            props.updateRegelungsList,
          )
        }
      >
        Einzelplan entfernen
      </Button>
    );

  return (
    <div>
      <div className="regelung-table-holder">
        <TableScroll>
          <Table
            columns={columns}
            dataSource={data}
            pagination={false}
            className="regelung-table"
            rowClassName={(record) => {
              if (record.key.indexOf('_summe') !== -1) {
                return 'summe-row';
              }
              return '';
            }}
          />
        </TableScroll>
        {showAddTableButton}
      </div>
    </div>
  );
}
