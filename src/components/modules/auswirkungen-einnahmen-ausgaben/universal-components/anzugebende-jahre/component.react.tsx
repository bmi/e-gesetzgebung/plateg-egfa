// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './anzugebende-jahre.less';

import { Row, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

import { JahrComponent } from './jahr-element/component.react';

export interface AnzugebendeJahreComponentInterface {
  nameStart: string;
  nameEnd: string;
  moduleName: string;
  hasData: boolean;
  form: FormInstance;
  setYearsRange: (yearsRange: number[]) => void;
  yearStart: number;
  yearEnd: number;
}
export function AnzugebendeJahreComponent(props: AnzugebendeJahreComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const [yearStart, setYearStart] = useState<number>(props.yearStart);
  const [yearEnd, setYearEnd] = useState<number>(props.yearEnd);

  useEffect(() => {
    let yearsDiff = yearEnd - yearStart;
    if (yearsDiff >= 3 && yearsDiff <= 7) {
      yearsDiff++;
      const yearsList = [...Array(yearsDiff).keys()].map((num) => yearStart + num);
      props.setYearsRange(yearsList);
    }
  }, [yearStart, yearEnd]);

  return (
    <>
      <Title level={2}>
        {t(`egfa.modules.${props.moduleName}.anzugebendeJahre.title`)}
        <InfoComponent title={t(`egfa.modules.${props.moduleName}.anzugebendeJahre.title`)}>
          <div
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.${props.moduleName}.anzugebendeJahre.drawerTitleText`),
            }}
          ></div>
        </InfoComponent>
      </Title>
      <div className="period-date-picker-container">
        <Row gutter={8} className="date-time-holder">
          <JahrComponent
            formElementName={props.nameStart}
            itemNameYearToCompare={props.nameEnd}
            moduleName={props.moduleName}
            hasData={props.hasData}
            form={props.form}
            isEndField={false}
            onValidDate={setYearStart}
          />
          <JahrComponent
            formElementName={props.nameEnd}
            itemNameYearToCompare={props.nameStart}
            moduleName={props.moduleName}
            hasData={props.hasData}
            form={props.form}
            isEndField={true}
            onValidDate={setYearEnd}
          />
        </Row>
      </div>
    </>
  );
}
