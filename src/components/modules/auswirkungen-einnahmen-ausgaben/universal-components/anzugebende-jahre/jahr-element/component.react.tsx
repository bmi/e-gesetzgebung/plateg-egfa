// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, Form, Input, InputRef, Modal } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ExclamationMarkRed } from '@plateg/theme';

export interface JahrComponentInterface {
  formElementName: string;
  itemNameYearToCompare: string;
  moduleName: string;
  hasData: boolean;
  form: FormInstance;
  isEndField: boolean;
  onValidDate: (year: number) => void;
}
export function JahrComponent(props: JahrComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const inputRef = useRef<InputRef>(null);
  const [isEditRequestAccepted, setIsEditRequestAccepted] = useState<boolean | undefined>(undefined);

  const isDateOutOfRange = (selectedYear: number) => {
    const yearToCompare = props.form.getFieldValue(props.itemNameYearToCompare) as number;
    const condition = props.isEndField ? selectedYear < yearToCompare : selectedYear > yearToCompare;
    return condition;
  };
  const validMinRange = (selectedYear: number, isEndYear: boolean) => {
    const yearToCompare = props.form.getFieldValue(props.itemNameYearToCompare) as number;
    const condition = isEndYear ? selectedYear - yearToCompare : yearToCompare - selectedYear;
    return condition < 3;
  };
  const validMaxRange = (selectedYear: number, isEndYear: boolean) => {
    const yearToCompare = props.form.getFieldValue(props.itemNameYearToCompare) as number;
    const condition = isEndYear ? selectedYear - yearToCompare : yearToCompare - selectedYear;
    return condition > 7;
  };

  useEffect(() => {
    const year = props.form.getFieldValue(props.formElementName) as string;

    // If recived Date object take only years
    if (Object.prototype.toString.call(year) === '[object Date]') {
      props.form.setFieldsValue({ [props.formElementName]: `${(year as unknown as Date).getFullYear()}` });
    } else {
      props.form.setFieldsValue({ [props.formElementName]: year });
    }
  }, [props.form.getFieldValue(props.formElementName)]);

  useEffect(() => {
    if (isEditRequestAccepted) {
      inputRef.current?.focus();
    }
  }, [isEditRequestAccepted]);

  return (
    <>
      <Col span={12}>
        <Form.Item
          className="period-date-picker"
          name={props.formElementName}
          label={t(`egfa.modules.${props.moduleName}.anzugebendeJahre.${props.formElementName}.label`)}
          rules={[
            {
              validator: (_rule, value: string) => {
                if (!value) {
                  return Promise.resolve();
                }
                if (!Number(value) || value.length > 4) {
                  return Promise.reject(t(`egfa.modules.${props.moduleName}.anzugebendeJahre.errorShouldBeNumber`));
                } else {
                  return Promise.resolve();
                }
              },
            },
            {
              validator: (_rule, value: string) => {
                if (!value || value.length != 4) {
                  return Promise.resolve();
                }
                const selectedYear = Number(value);
                if (isDateOutOfRange(selectedYear)) {
                  return Promise.reject(
                    t(
                      `egfa.modules.${props.moduleName}.anzugebendeJahre.errorDateOutOfRange${
                        props.isEndField ? 'End' : 'Start'
                      }`,
                    ),
                  );
                } else if (validMinRange(selectedYear, props.isEndField)) {
                  return Promise.reject(t(`egfa.modules.${props.moduleName}.anzugebendeJahre.errorMinRange`));
                } else if (validMaxRange(selectedYear, props.isEndField)) {
                  return Promise.reject(t(`egfa.modules.${props.moduleName}.anzugebendeJahre.errorMaxRange`));
                } else {
                  return Promise.resolve();
                }
              },
            },
          ]}
        >
          <Input
            ref={inputRef}
            type="text"
            onChange={(value) => {
              if (value.target.value.length < 4 || isDateOutOfRange(Number(value.target.value))) {
                return false;
              }
              void props.form.validateFields([props.itemNameYearToCompare]);
              props.onValidDate(Number(value.target.value));
            }}
            onFocus={() => {
              if (props.hasData && isEditRequestAccepted === undefined) {
                Modal.confirm({
                  className: 'confirm-years-change',
                  title: (
                    <h3 style={{ fontSize: '1.8rem', fontWeight: 500 }}>
                      {t(`egfa.modules.${props.moduleName}.anzugebendeJahre.confirm.label`)}
                    </h3>
                  ),
                  icon: <ExclamationMarkRed style={{ float: 'left', margin: '23px 15px 0 0' }} />,
                  okText: t(`egfa.modules.${props.moduleName}.anzugebendeJahre.confirm.btnOk`),
                  cancelText: t(`egfa.modules.${props.moduleName}.anzugebendeJahre.confirm.btnCancel`),
                  onOk: () => {
                    setIsEditRequestAccepted(true);
                  },
                  onCancel: () => {
                    setIsEditRequestAccepted(false);
                  },
                });
              }
            }}
            onBlur={() => {
              if (props.hasData && isEditRequestAccepted === false) {
                setIsEditRequestAccepted(undefined);
              }
            }}
          />
        </Form.Item>
      </Col>
    </>
  );
}
