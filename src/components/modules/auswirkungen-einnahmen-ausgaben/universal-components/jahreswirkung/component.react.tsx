// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './jahreswirkung.less';

import { Form, FormInstance, Input, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

export interface JahreswirkungComponentInterface {
  moduleName: string;
  translationKey: string;
  formElementName: string;
  form: FormInstance;
  setIsDirty: () => void;
}
export function JahreswirkungComponent(props: JahreswirkungComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const { TextArea } = Input;
  const [isChecked, setIsChecked] = useState(false);
  const [textareaData, setTextareaData] = useState('');
  useEffect(() => {
    const textareaValue = props.form.getFieldValue(props.formElementName) as string;
    if (textareaValue) {
      setIsChecked(true);
      setTextareaData(textareaValue);
    }
  }, []);

  const onChange = (val: boolean) => {
    setIsChecked(val);
    const obj: { [s: string]: string } = {};
    obj[props.formElementName] = val ? textareaData : '';
    props.form.setFieldsValue(obj);
    props.setIsDirty();
    window.dispatchEvent(new Event('scroll'));
  };
  return (
    <>
      <div className="jahreswirkung-switch">
        <Switch
          className="switch-component"
          checked={isChecked}
          onChange={onChange}
          aria-label={t(`${props.translationKey}.jahreswirkung.switchLabel`)}
        />
        <span>{t(`${props.translationKey}.jahreswirkung.switchLabel`)}</span>
        <InfoComponent isContactPerson={false} title={t(`${props.translationKey}.jahreswirkung.switchLabel`)}>
          {t(`${props.translationKey}.jahreswirkung.drawerSwitchLabel`)}
        </InfoComponent>
      </div>
      {isChecked && (
        <Form.Item
          name={props.formElementName}
          label={t(`${props.translationKey}.jahreswirkung.jahreswirkungSummary.label`)}
        >
          <TextArea rows={8} onChange={(e) => setTextareaData(e.currentTarget.value)} />
        </Form.Item>
      )}
    </>
  );
}
