// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Radio, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

interface InfluenceExpectedComponentInterface {
  name: string;
  translationKey: string;
  onChange: (value: boolean) => void;
}
export function InfluenceExpectedComponent(props: InfluenceExpectedComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;

  return (
    <>
      <Title level={2}>{t(`${props.translationKey}.influenceExpected.title`)}</Title>

      <FormItemWithInfo
        htmlFor={`id-${props.name}`}
        name={props.name}
        customName={`id-${props.name}`}
        label={
          <>
            <span
              dangerouslySetInnerHTML={{
                __html: t(`${props.translationKey}.influenceExpected.question`, { newLine: `<br />` }),
              }}
            />
            <InfoComponent
              title={t(`${props.translationKey}.influenceExpected.question`, { newLine: `` })}
              withLabelRequired
            >
              <div
                dangerouslySetInnerHTML={{
                  __html: t(`${props.translationKey}.influenceExpected.questionDrawer`),
                }}
              ></div>
            </InfoComponent>
          </>
        }
        rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
      >
        <Radio.Group
          id={`id-${props.name}`}
          className="horizontal-radios"
          onChange={(event) => props.onChange(event?.target?.value as boolean)}
          name={props.name}
        >
          <Radio id={`egfa-${props.name}Ja-radio`} className="horizontal-radios" value={true}>
            Ja
          </Radio>
          <Radio id={`egfa-${props.name}Nein-radio`} className="horizontal-radios" value={false}>
            Nein
          </Radio>
        </Radio.Group>
      </FormItemWithInfo>
    </>
  );
}
