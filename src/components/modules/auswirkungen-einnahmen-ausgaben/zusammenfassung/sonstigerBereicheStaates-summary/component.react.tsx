// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { AktionType, EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { AuswirkungenEinnahmenAusgabenController, widgetRegelungKeysSonstigerBereicheMap } from '../../controller';
import { TableWidgetComponent } from '../../universal-components/table-widget/component.react';

export interface SonstigerBereicheStaatesSummaryInterface {
  formData?: EgfaModuleEaOehhEntityDTOAllOf;
  translationKey: string;
  editLink?: string;
  isOnlyGesamtsummenTabels?: boolean;
  onSave?: (successCallback: () => void) => void;
}

export function SonstigerBereicheStaatesSummaryComponent(
  props: SonstigerBereicheStaatesSummaryInterface,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const [regelungListData, setRegelungListData] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRangeData, setYearsRangeData] = useState<number[]>([]);
  const [influenceExpected, setInfluenceExpected] = useState<boolean>(false);
  const [influenceExpectedSummary, setInfluenceExpectedSummary] = useState<string>('');
  const [influenceVolleJahreswirkungSummary, setInfluenceVolleJahreswirkungSummary] = useState<string>('');
  const [isEditAllowed, setIsEditAllowed] = useState<boolean>(false);

  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );

  useEffect(() => {
    // Data only for peresentation
    if (props.formData?.regelungen) {
      const sortedRegelungs = ctrl.getSortedRegelungs(props.formData.regelungen);
      const regelungsWithData = ctrl.regelungsWithData(sortedRegelungs);
      setRegelungListData(regelungsWithData);
      const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysSonstigerBereicheMap);
      setYearsRangeData(years);
      setInfluenceExpected(props.formData['influenceWeitereEinnahmenExpected'] as boolean);
      setInfluenceExpectedSummary(props.formData['influenceWeitereEinnahmenSummary'] as string);
      setInfluenceVolleJahreswirkungSummary(
        props.formData['influenceWeitereEinnahmenVolleJahreswirkungSummary'] as string,
      );
    }
    setIsEditAllowed(props.formData?.aktionen?.includes(AktionType.Schreiben) == true);
  }, [props.formData]);

  const translationKey = `${props.translationKey}.sonstigerBereicheStaates`;
  const handleFormChangeMethod = undefined;
  const updateRegelungsListMethod = (_list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    /** This is intentional */
  };
  const getActualRegelungsListMethod = () => [];
  const onClickHandler = () => {
    if (props.onSave && props.editLink) {
      props.onSave(() => history.push(props.editLink as string));
    }
  };

  const tableProps = {
    yearsRange: yearsRangeData,
    regelungsList: regelungListData,
    isEditable: false,
    updateRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => updateRegelungsListMethod(list),
    getActualRegelungsList: () => getActualRegelungsListMethod(),
    hasCurrency: true,
    tableHeaderLabel: '',
    handeFormChange: handleFormChangeMethod,
    hasTitle: true,
  };

  return (
    <div>
      <div className="section-widget">
        <div className="title">
          <span>{t(`${translationKey}.linkName`)}</span>
          {props.editLink && isEditAllowed && (
            <Link to={props.editLink} onClick={onClickHandler}>
              {t(`egfa.modules.general.summaryBearbeitung`)}
            </Link>
          )}
        </div>
        <p className="summary-text">
          {!influenceExpected && <>{t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`)}</>}
          {influenceExpected && influenceExpectedSummary && <>{influenceExpectedSummary}</>}
        </p>
        {influenceExpected && influenceVolleJahreswirkungSummary && (
          <>
            <div className="title jahreswirkung">
              <span>{t(`${translationKey}.jahreswirkung.title`)}</span>
            </div>
            <p className="summary-text">{influenceVolleJahreswirkungSummary}</p>
          </>
        )}
      </div>
      {influenceExpected && regelungListData.length > 0 && (
        <>
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.ausgaben.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                tableHeaderLabel={t(`${translationKey}.ausgaben.headerLabel`)}
                widgetRegelungKey={widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesAusgaben']}
                defaultIndex={0}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.gesamtsummeAusgaben.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            {...tableProps}
            widgetRegelungKey={widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesGesamtsummeAusgaben']}
            isStaticRows={true}
            noMoreRegelungs={true}
            rowsLabels={[
              { title: t(`${translationKey}.gesamtsummeAusgaben.tableRowTitles.gesamtsumme`), isSumme: true },
            ]}
            hasTitle={false}
            defaultIndex={1}
          />
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.einnahmen.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                tableHeaderLabel={t(`${translationKey}.einnahmen.headerLabel`)}
                widgetRegelungKey={widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesEinnahmen']}
                defaultIndex={2}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.gesamtsummeEinnahmen.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>

          <TableWidgetComponent
            {...tableProps}
            widgetRegelungKey={widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesGesamtsummeEinnahmen']}
            isStaticRows={true}
            noMoreRegelungs={true}
            rowsLabels={[
              { title: t(`${translationKey}.gesamtsummeEinnahmen.tableRowTitles.gesamtsumme`), isSumme: true },
            ]}
            hasTitle={false}
            defaultIndex={3}
          />
        </>
      )}
    </div>
  );
}
