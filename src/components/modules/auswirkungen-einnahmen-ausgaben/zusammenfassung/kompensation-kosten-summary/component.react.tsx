// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { AktionType, EgfaModuleEaOehhEntityDTOAllOf } from '@plateg/rest-api';

export interface KompensationKostenSummaryInterface {
  formData?: EgfaModuleEaOehhEntityDTOAllOf;
  translationKey: string;
  editLink?: string;
  isOnlyGesamtsummenTabels?: boolean;
  onSave?: (successCallback: () => void) => void;
}

export function KompensationKostenSummaryComponent(props: KompensationKostenSummaryInterface): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();

  const [influenceExpectedSummary, setInfluenceExpectedSummary] = useState<string>('');

  useEffect(() => {
    // Data only for peresentation
    if (props.formData) {
      setInfluenceExpectedSummary(props.formData['influenceGeplanteKompensationDerKostenSummary'] as string);
    }
  }, [props.formData]);

  const translationKey = `${props.translationKey}.kompensationKosten`;
  const onClickHandler = () => {
    if (props.onSave && props.editLink) {
      props.onSave(() => history.push(props.editLink as string));
    }
  };

  return (
    <div>
      <div className="section-widget">
        <div className="title">
          <span>{t(`${translationKey}.linkName`)}</span>
          {props.editLink && props.formData?.aktionen?.includes(AktionType.Schreiben) && (
            <Link to={props.editLink} onClick={onClickHandler}>
              {t(`egfa.modules.general.summaryBearbeitung`)}
            </Link>
          )}
        </div>
        <p className="summary-text">{influenceExpectedSummary && <>{influenceExpectedSummary}</>}</p>
      </div>
    </div>
  );
}
