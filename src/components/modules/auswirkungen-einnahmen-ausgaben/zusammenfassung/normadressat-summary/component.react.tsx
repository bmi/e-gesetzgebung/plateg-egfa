// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { AktionType, EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { AuswirkungenEinnahmenAusgabenController, widgetRegelungKeysNormadressatMap } from '../../controller';
import { TableWidgetComponent } from '../../universal-components/table-widget/component.react';

export interface NormadressatSummaryInterface {
  formData?: EgfaModuleEaOehhEntityDTOAllOf;
  translationKey: string;
  editLink?: string;
  isOnlyGesamtsummenTabels?: boolean;
  onSave?: (successCallback: () => void) => void;
}

export function NormadressatSummaryComponent(props: NormadressatSummaryInterface): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const [regelungListData, setRegelungListData] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRangeData, setYearsRangeData] = useState<number[]>([]);
  const [influenceExpected, setInfluenceExpected] = useState<boolean>(false);
  const [influenceExpectedSummary, setInfluenceExpectedSummary] = useState<string>('');
  const [influenceStellenplanen, setInfluenceStellenplanen] = useState<boolean>(false);
  const [influenceStellenplanenSummary, setInfluenceStellenplanenSummary] = useState<string>('');
  const [isEditAllowed, setIsEditAllowed] = useState<boolean>(false);

  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );

  useEffect(() => {
    // Data only for peresentation
    if (props.formData?.regelungen) {
      const sortedRegelungs = ctrl.getSortedRegelungs(props.formData?.regelungen);
      const regelungsWithData = ctrl.regelungsWithData(sortedRegelungs);
      setRegelungListData(regelungsWithData);
      const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysNormadressatMap);
      setYearsRangeData(years);
      setInfluenceExpected(props.formData['influenceVerwaltungAlsNormadressatExpected'] as boolean);
      setInfluenceExpectedSummary(props.formData['influenceVerwaltungAlsNormadressatSummary'] as string);
      setInfluenceStellenplanen(props.formData['influenceVerwaltungAlsNormadressatStellenplaene'] as boolean);
      setInfluenceStellenplanenSummary(
        props.formData['influenceVerwaltungAlsNormadressatStellenplaeneSummary'] as string,
      );
    }
    setIsEditAllowed(props.formData?.aktionen?.includes(AktionType.Schreiben) == true);
  }, [props.formData]);

  const translationKey = `${props.translationKey}.normadressat`;
  const handleFormChangeMethod = undefined;
  const updateRegelungsListMethod = (_list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    /** This is intentional */
  };
  const getActualRegelungsListMethod = () => [];
  const onClickHandler = () => {
    if (props.onSave && props.editLink) {
      props.onSave(() => history.push(props.editLink as string));
    }
  };

  return (
    <div>
      <div className="section-widget">
        <div className="title">
          <span>{t(`${translationKey}.linkName`)}</span>
          {props.editLink && isEditAllowed && (
            <Link to={props.editLink}>{t(`egfa.modules.general.summaryBearbeitung`)}</Link>
          )}
        </div>
        <p className="summary-text">
          {!influenceExpected && <>{t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`)}</>}
          {influenceExpected && influenceExpectedSummary && <>{influenceExpectedSummary}</>}
        </p>
      </div>
      {influenceExpected && regelungListData.length > 0 && (
        <>
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.ausgabenDurch.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                yearsRange={yearsRangeData}
                regelungsList={regelungListData}
                isEditable={false}
                updateRegelungsList={(list) => updateRegelungsListMethod(list)}
                getActualRegelungsList={() => getActualRegelungsListMethod()}
                hasCurrency={true}
                tableHeaderLabel={t(`${translationKey}.normadressatRegelung.headerLabel`)}
                widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatRegelung']}
                handeFormChange={handleFormChangeMethod}
                hasTitle={true}
                defaultIndex={0}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.gesamtsumme.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.gesamtsumme.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatGesamtsumme']}
            handeFormChange={handleFormChangeMethod}
            isStaticRows={true}
            noMoreRegelungs={true}
            rowsLabels={[{ title: t(`${translationKey}.gesamtsumme.tableRowTitles.gesamtsumme`), isSumme: true }]}
            defaultIndex={1}
          />
        </>
      )}
      {influenceExpected && influenceStellenplanen && influenceStellenplanenSummary && (
        <div className="section-widget">
          <p className="summary-text">{influenceStellenplanenSummary}</p>
        </div>
      )}
      {influenceExpected && influenceStellenplanen && regelungListData.length > 0 && (
        <>
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.planstellen.influenceExpected.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                yearsRange={yearsRangeData}
                regelungsList={regelungListData}
                isEditable={false}
                updateRegelungsList={(list) => updateRegelungsListMethod(list)}
                getActualRegelungsList={() => getActualRegelungsListMethod()}
                hasCurrency={false}
                tableHeaderLabel={''}
                widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatStellenplaene']}
                handeFormChange={handleFormChangeMethod}
                isStaticRows={true}
                rowsLabels={[
                  { title: t(`${translationKey}.planstellen.tableRowTitles.hoehererDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.gehobenerDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.mittlererDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.einfacherDienst`), isSumme: false },
                ]}
                defaultIndex={0}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.planstellen.gesamtTable.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={false}
            tableHeaderLabel={t(`${translationKey}.planstellen.gesamtTable.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatGesamtStellenplaene']}
            handeFormChange={handleFormChangeMethod}
            noMoreRegelungs={true}
            isStaticRows={true}
            rowsLabels={[
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.hoehererDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.gehobenerDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.mittlererDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.einfacherDienst`),
                isSumme: false,
              },
            ]}
            defaultIndex={1}
          />
        </>
      )}
    </div>
  );
}
