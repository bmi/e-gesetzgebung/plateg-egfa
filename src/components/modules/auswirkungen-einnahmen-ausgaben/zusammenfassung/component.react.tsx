// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Form, Switch, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEntityRequestDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { FormWrapper, GlobalDI, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import {
  aenderungenSteuereinnahmenRegelungKeysMap,
  AuswirkungenEinnahmenAusgabenController,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  verwaltungsUndSonstigeRegelungKeysMap,
  verwaltungsvollzugKeysMap,
  zweckausgabenRegelungKeysMap,
} from '../controller';
import { KompensationKostenSummaryComponent } from './kompensation-kosten-summary/component.react';
import { NormadressatSummaryComponent } from './normadressat-summary/component.react';
import { PatternSummaryComponent } from './pattern-summary/component.react';
import { SonstigerBereicheStaatesSummaryComponent } from './sonstigerBereicheStaates-summary/component.react';
import { VorblattComponent } from './vorblatt/component.react';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}

export function ZusammenfassungComponent(props: SimpleModuleProps<EgfaModuleEntityRequestDTO>): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isOnlyGesamtsummenTabels, setIsOnlyGesamtsummenTabels] = useState<boolean>(false);
  const [isDirty, setIsDirty] = useState(false);
  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );

  useEffect(() => {
    if (props.formData) {
      const formDataValue = { ...(props.formData as EgfaModuleEaOehhEntityDTOAllOf) };
      const sortedRegelungs = ctrl.getSortedRegelungs(formDataValue.regelungen || []);
      form.setFieldsValue({ ...formDataValue, regelungen: sortedRegelungs });
      setIsDirty(false);
    }
  }, [props.formData]);

  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute: string = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.zusammenfassung`;

  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };
  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`${translationKey}.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={handleFormChange}
      >
        <p>{t(`${translationKey}.text`)}</p>
        <Title level={2}>{t(`${translationKey}.auswirkungenEinzelnenBereicheTitle`)}</Title>
        <div className="only-gesamtsummen-tabels-switch">
          <Switch
            className="switch-component"
            checked={isOnlyGesamtsummenTabels}
            onChange={setIsOnlyGesamtsummenTabels}
          />
          <span>
            {t(`${translationKey}.onlyGesamtsummenTabels.switchLabel`)}
            <InfoComponent isContactPerson={false} title={t(`${translationKey}.onlyGesamtsummenTabels.switchLabel`)}>
              {t(`${translationKey}.onlyGesamtsummenTabels.drawerSwitchLabel`)}
            </InfoComponent>
          </span>
        </div>
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.zweckausgaben`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.ZWECKAUSGABEN}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
          widgetRegelungKeys={zweckausgabenRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Zweckausgaben}
          influenceSummaryKey={InfluenceSummaryEnum.Zweckausgaben}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Zweckausgaben}
          regelungKeysList={Object.values(zweckausgabenRegelungKeysMap)}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.vollzugsaufwand`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.VOLLZUGSAUFWAND}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
          widgetRegelungKeys={verwaltungsvollzugKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.AusgabenVerwaltungsvollzug}
          influenceSummaryKey={InfluenceSummaryEnum.AusgabenVerwaltungsvollzug}
          influenceStellenplaeneKey={InfluenceStellenplaeneEnum.AusgabenVerwaltungsvollzug}
          influenceStellenplaeneSummaryKey={InfluenceStellenplaeneSummaryEnum.AusgabenVerwaltungsvollzug}
          regelungKeysList={Object.values(verwaltungsvollzugKeysMap)}
        />
        <NormadressatSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.NORMADRESSAT}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.verwaltungsUndSonstige`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.VERWALTUNGSUNDSONSTIGE}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
          widgetRegelungKeys={verwaltungsUndSonstigeRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Einnahmen}
          influenceSummaryKey={InfluenceSummaryEnum.Einnahmen}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Einnahmen}
          regelungKeysList={Object.values(verwaltungsUndSonstigeRegelungKeysMap)}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.aenderungenSteuereinnahmen`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.AENDERUNGENSTEUEREINNAHMEN}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
          widgetRegelungKeys={aenderungenSteuereinnahmenRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Steuereinnahmen}
          influenceSummaryKey={InfluenceSummaryEnum.Steuereinnahmen}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Steuereinnahmen}
          regelungKeysList={Object.values(aenderungenSteuereinnahmenRegelungKeysMap)}
        />
        <SonstigerBereicheStaatesSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.SONSTIGERBEREICHESTAATES}`}
          isOnlyGesamtsummenTabels={isOnlyGesamtsummenTabels}
        />
        <KompensationKostenSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.KOMPENSATIONKOSTEN}`}
        />
        <VorblattComponent
          handleFormChange={handleFormChange}
          formData={props.formData as EgfaModuleEaOehhEntityDTOAllOf}
          moduleName={props.moduleName}
          form={form}
        />
      </FormWrapper>
    </div>
  );
}
