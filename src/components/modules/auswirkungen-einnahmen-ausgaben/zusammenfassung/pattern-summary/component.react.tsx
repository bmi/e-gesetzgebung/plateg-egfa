// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { AktionType, EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import {
  AuswirkungenEinnahmenAusgabenController,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  WidgetRegelungKeysMapInterface,
} from '../../controller';
import { TableWidgetComponent } from '../../universal-components/table-widget/component.react';

export interface PatternSummaryInterface {
  formData?: EgfaModuleEaOehhEntityDTOAllOf;
  translationKey: string;
  editLink?: string;
  isOnlyGesamtsummenTabels?: boolean;
  onSave?: (successCallback: () => void) => void;
  widgetRegelungKeys: WidgetRegelungKeysMapInterface;
  influenceExpectedKey: InfluenceExpectedEnum;
  influenceSummaryKey: InfluenceSummaryEnum;
  influenceVolleJahreswirkungSummaryKey?: InfluenceVolleJahreswirkungSummaryEnum;
  influenceStellenplaeneKey?: InfluenceStellenplaeneEnum;
  influenceStellenplaeneSummaryKey?: InfluenceStellenplaeneSummaryEnum;
  regelungKeysList: string[];
}

export function PatternSummaryComponent(props: PatternSummaryInterface): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const [regelungListData, setRegelungListData] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRangeData, setYearsRangeData] = useState<number[]>([]);
  const [influenceExpected, setInfluenceExpected] = useState<boolean>(false);
  const [influenceExpectedSummary, setInfluenceExpectedSummary] = useState<string>('');
  const [influenceVolleJahreswirkungSummary, setInfluenceVolleJahreswirkungSummary] = useState<string | null>(null);
  const [influenceStellenplanen, setInfluenceStellenplanen] = useState<boolean>(false);
  const [influenceStellenplanenSummary, setInfluenceStellenplanenSummary] = useState<string | null>(null);
  const [isEditAllowed, setIsEditAllowed] = useState<boolean>(false);

  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );
  const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = props.widgetRegelungKeys;

  useEffect(() => {
    // Data only for peresentation
    if (props.formData?.regelungen) {
      const sortedRegelungs = ctrl.getSortedRegelungs(props.formData.regelungen);
      const regelungsWithData = ctrl.regelungsWithData(sortedRegelungs);
      setRegelungListData(regelungsWithData);
      const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysMap);
      setYearsRangeData(years);
      setInfluenceExpected(props.formData[props.influenceExpectedKey] as boolean);
      setInfluenceExpectedSummary(props.formData[props.influenceSummaryKey] as string);
      if (props.influenceVolleJahreswirkungSummaryKey) {
        setInfluenceVolleJahreswirkungSummary(props.formData[props.influenceVolleJahreswirkungSummaryKey] as string);
      }
      if (props.influenceStellenplaeneKey && props.influenceStellenplaeneSummaryKey) {
        setInfluenceStellenplanen(props.formData[props.influenceStellenplaeneKey] as boolean);
        setInfluenceStellenplanenSummary(props.formData[props.influenceStellenplaeneSummaryKey] as string);
      }
    }
    setIsEditAllowed(props.formData?.aktionen?.includes(AktionType.Schreiben) == true);
  }, [props.formData]);

  const translationKey = `${props.translationKey}`;
  const handleFormChangeMethod = undefined;
  const updateRegelungsListMethod = (_list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    /** This is intentional */
  };
  const getActualRegelungsListMethod = () => [];
  const onClickHandler = () => {
    if (props.onSave && props.editLink) {
      props.onSave(() => history.push(props.editLink as string));
    }
  };
  const tableProps = {
    yearsRange: yearsRangeData,
    regelungsList: regelungListData,
    isEditable: false,
    updateRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => updateRegelungsListMethod(list),
    getActualRegelungsList: () => getActualRegelungsListMethod(),
    hasCurrency: true,
    tableHeaderLabel: '',
    handeFormChange: handleFormChangeMethod,
    hasTitle: true,
  };

  return (
    <div>
      <div className="section-widget">
        <div className="title">
          <span>{t(`${translationKey}.linkName`)}</span>
          {props.editLink && isEditAllowed && (
            <Link to={props.editLink} onClick={onClickHandler}>
              {t(`egfa.modules.general.summaryBearbeitung`)}
            </Link>
          )}
        </div>
        <p className="summary-text">
          {!influenceExpected && <>{t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`)}</>}
          {influenceExpected && influenceExpectedSummary && <>{influenceExpectedSummary}</>}
        </p>
        {influenceExpected && influenceVolleJahreswirkungSummary && (
          <>
            <div className="title jahreswirkung">
              <span>{t(`${translationKey}.jahreswirkung.title`)}</span>
            </div>
            <p className="summary-text">{influenceVolleJahreswirkungSummary}</p>
          </>
        )}
      </div>
      {influenceExpected && regelungListData.length > 0 && (
        <>
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.bund.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                tableHeaderLabel={t(`${translationKey}.bund.headerLabel`)}
                widgetRegelungKey={props.regelungKeysList[0]}
                defaultIndex={0}
              />
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.laender.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                hasOnlySumme={true}
                widgetRegelungKey={props.regelungKeysList[1]}
                defaultIndex={1}
              />
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.gemeinden.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                hasOnlySumme={true}
                widgetRegelungKey={props.regelungKeysList[2]}
                defaultIndex={2}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.gesamtsumme.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            {...tableProps}
            tableHeaderLabel={t(`${translationKey}.gesamtsumme.headerLabel`)}
            widgetRegelungKey={props.regelungKeysList[3]}
            isStaticRows={true}
            noMoreRegelungs={true}
            rowsLabels={[
              { title: t(`${translationKey}.gesamtsumme.tableRowTitles.bund`), isSumme: false },
              { title: t(`${translationKey}.gesamtsumme.tableRowTitles.laender`), isSumme: false },
              { title: t(`${translationKey}.gesamtsumme.tableRowTitles.gemeinden`), isSumme: false },
              { title: t(`${translationKey}.gesamtsumme.tableRowTitles.gesamtsumme`), isSumme: true },
            ]}
            hasTitle={false}
            defaultIndex={3}
          />
        </>
      )}
      {influenceExpected && influenceStellenplanen && influenceStellenplanenSummary && (
        <div className="section-widget">
          <p className="summary-text">{influenceStellenplanenSummary}</p>
        </div>
      )}
      {influenceExpected && influenceStellenplanen && regelungListData.length > 0 && (
        <>
          {!props.isOnlyGesamtsummenTabels && (
            <>
              <div className="widget-table-title">
                <Title level={3}>{t(`${translationKey}.planstellen.influenceExpected.title`)}</Title>
                {props.editLink && isEditAllowed && (
                  <Link to={props.editLink} onClick={onClickHandler}>
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                )}
              </div>
              <TableWidgetComponent
                {...tableProps}
                hasCurrency={false}
                widgetRegelungKey={props.regelungKeysList[4]}
                isStaticRows={true}
                rowsLabels={[
                  { title: t(`${translationKey}.planstellen.tableRowTitles.hoehererDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.gehobenerDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.mittlererDienst`), isSumme: false },
                  { title: t(`${translationKey}.planstellen.tableRowTitles.einfacherDienst`), isSumme: false },
                ]}
                hasTitle={false}
                defaultIndex={0}
              />
            </>
          )}
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.planstellen.gesamtTable.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            {...tableProps}
            hasCurrency={false}
            tableHeaderLabel={t(`${translationKey}.planstellen.gesamtTable.headerLabel`)}
            widgetRegelungKey={props.regelungKeysList[5]}
            noMoreRegelungs={true}
            isStaticRows={true}
            rowsLabels={[
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.hoehererDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.gehobenerDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.mittlererDienst`),
                isSumme: false,
              },
              {
                title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.einfacherDienst`),
                isSumme: false,
              },
            ]}
            hasTitle={false}
            defaultIndex={1}
          />
        </>
      )}
    </div>
  );
}
