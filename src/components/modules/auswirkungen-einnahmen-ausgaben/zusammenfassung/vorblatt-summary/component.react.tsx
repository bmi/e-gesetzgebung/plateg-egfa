// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';

import { AktionType, EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { AuswirkungenEinnahmenAusgabenController, widgetRegelungKeysVorblattMap } from '../../controller';
import { TableWidgetComponent } from '../../universal-components/table-widget/component.react';

export interface VorblattSummaryInterface {
  formData?: EgfaModuleEaOehhEntityDTOAllOf;
  translationKey: string;
  editLink?: string;
  isOnlyGesamtsummenTabels?: boolean;
  onSave?: (successCallback: () => void) => void;
}

export function VorblattSummaryComponent(props: VorblattSummaryInterface): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();

  const [regelungListData, setRegelungListData] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRangeData, setYearsRangeData] = useState<number[]>([]);
  const [influenceExpectedSummary, setInfluenceExpectedSummary] = useState<string>('');
  const [isEditAllowed, setIsEditAllowed] = useState<boolean>(false);

  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );

  useEffect(() => {
    // Data only for peresentation
    if (props.formData?.regelungen) {
      const sortedRegelungs = ctrl.getSortedRegelungs(props.formData.regelungen);
      const regelungsWithData = ctrl.regelungsWithData(sortedRegelungs);
      setRegelungListData(regelungsWithData);
      const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysVorblattMap);
      setYearsRangeData(years);
      setInfluenceExpectedSummary(props.formData['influenceSummary'] as string);
    }
    setIsEditAllowed(props.formData?.aktionen?.includes(AktionType.Schreiben) == true);
  }, [props.formData]);

  const translationKey = `${props.translationKey}.vorblatt`;
  const handleFormChangeMethod = undefined;
  const updateRegelungsListMethod = (_list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    /** This is intentional */
  };
  const getActualRegelungsListMethod = () => [];

  const onClickHandler = () => {
    if (props.onSave && props.editLink) {
      props.onSave(() => history.push(props.editLink as string));
    }
  };
  return (
    <div>
      <div className="section-widget">
        <div className="summary-box">
          {props.editLink && isEditAllowed && (
            <Link to={props.editLink} onClick={onClickHandler}>
              {t(`egfa.modules.general.summaryBearbeitung`)}
            </Link>
          )}
          <p className="summary-text">{influenceExpectedSummary && <>{influenceExpectedSummary}</>}</p>
        </div>
      </div>
      {influenceExpectedSummary && regelungListData.length > 0 && (
        <>
          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.ausgaben.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.ausgaben.bund.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattAusgabenBund']}
            handeFormChange={handleFormChangeMethod}
            hasTitle={true}
            defaultIndex={0}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.ausgaben.laender.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattAusgabenLaender']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={1}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.ausgaben.gemeinden.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattAusgabenGemeinden']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={2}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.ausgaben.gesamtsumme.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattAusgabenGesamtsumme']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={3}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />

          <div className="widget-table-title">
            <Title level={3}>{t(`${translationKey}.einnahmen.title`)}</Title>
            {props.editLink && isEditAllowed && (
              <Link to={props.editLink} onClick={onClickHandler}>
                {t(`egfa.modules.general.summaryBearbeitung`)}
              </Link>
            )}
          </div>
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.einnahmen.bund.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattEinnahmenBund']}
            handeFormChange={handleFormChangeMethod}
            hasTitle={true}
            defaultIndex={4}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.einnahmen.laender.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattEinnahmenLaender']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={5}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.einnahmen.gemeinden.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattEinnahmenGemeinden']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={6}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />
          <TableWidgetComponent
            yearsRange={yearsRangeData}
            regelungsList={regelungListData}
            isEditable={false}
            updateRegelungsList={(list) => updateRegelungsListMethod(list)}
            getActualRegelungsList={() => getActualRegelungsListMethod()}
            hasCurrency={true}
            tableHeaderLabel={t(`${translationKey}.einnahmen.gesamtsumme.headerLabel`)}
            widgetRegelungKey={widgetRegelungKeysVorblattMap['VorblattEinnahmenGesamtsumme']}
            handeFormChange={handleFormChangeMethod}
            defaultIndex={7}
            hasOnlySumme={true}
            noMoreRegelungs={true}
          />
        </>
      )}
    </div>
  );
}
