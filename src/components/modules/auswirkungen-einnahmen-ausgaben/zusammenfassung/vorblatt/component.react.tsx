// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEaOehhRegelungEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, GlobalDI, InfoComponent } from '@plateg/theme';

import { AuswirkungenEinnahmenAusgabenController, WidgetRegelungKeysMapInterface } from '../../controller';
import { TableWidgetComponent } from '../../universal-components/table-widget/component.react';

export interface VorblattInterface {
  moduleName: string;
  handleFormChange?: () => void;
  formData: EgfaModuleEaOehhEntityDTOAllOf;
  form: FormInstance;
}

export function VorblattComponent(props: VorblattInterface): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [regelungsList, setRegelungsList] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRange, setYearsRange] = useState<number[]>([]);
  const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = {
    VorblattAusgabenBund: 'VorblattAusgabenBund',
    VorblattAusgabenLaender: 'VorblattAusgabenLaender',
    VorblattAusgabenGemeinden: 'VorblattAusgabenGemeinden',
    VorblattAusgabenGesamtsumme: 'VorblattAusgabenGesamtsumme',
    VorblattEinnahmenBund: 'VorblattEinnahmenBund',
    VorblattEinnahmenLaender: 'VorblattEinnahmenLaender',
    VorblattEinnahmenGemeinden: 'VorblattEinnahmenGemeinden',
    VorblattEinnahmenGesamtsumme: 'VorblattEinnahmenGesamtsumme',
  };

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData;
      if (formDataValue.regelungen) {
        const sortedRegelungs = ctrl.getSortedRegelungs(formDataValue.regelungen);
        setRegelungsList(sortedRegelungs);
        const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysMap);
        setYearsRange(years);
      }
    }
  }, [props.formData]);

  const translationKey = `egfa.modules.${props.moduleName}.vorblatt`;

  const updateRegelungsList = (list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    props.form.setFieldsValue({ regelungen: list });
    setRegelungsList(list);
  };

  const getActualRegelungsList = () => {
    return props.form.getFieldValue('regelungen') as EgfaModuleEaOehhRegelungEntityDTO[];
  };

  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
  };
  return (
    <div>
      <>
        <div className="vorblatt-divider"></div>
        <Title level={2}>{t(`${translationKey}.title`)}</Title>
        <FormItemWithInfo
          name={'influenceSummary'}
          label={
            <span>
              {t(`${translationKey}.influenceSummary.label`)}
              <InfoComponent title={t(`${translationKey}.influenceSummary.label`)} withLabelRequired>
                <div
                  dangerouslySetInnerHTML={{
                    __html: t(`${translationKey}.influenceSummary.labelDrawer`),
                  }}
                ></div>
              </InfoComponent>
            </span>
          }
          rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
        >
          <TextArea rows={8} />
        </FormItemWithInfo>

        {regelungsList.length > 0 && (
          <>
            <Title level={3} className="vorblatt-title">
              {t(`${translationKey}.ausgaben.title`)}
            </Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.ausgaben.bund.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattAusgabenBund']}
              handeFormChange={handleFormChange}
              defaultIndex={0}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.ausgaben.laender.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattAusgabenLaender']}
              handeFormChange={handleFormChange}
              defaultIndex={1}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.ausgaben.gemeinden.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattAusgabenGemeinden']}
              handeFormChange={handleFormChange}
              defaultIndex={2}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.ausgaben.gesamtsumme.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattAusgabenGesamtsumme']}
              handeFormChange={handleFormChange}
              defaultIndex={3}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />

            <Title level={3} className="vorblatt-title">
              {t(`${translationKey}.einnahmen.title`)}
            </Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.einnahmen.bund.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattEinnahmenBund']}
              handeFormChange={handleFormChange}
              defaultIndex={4}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.einnahmen.laender.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattEinnahmenLaender']}
              handeFormChange={handleFormChange}
              defaultIndex={5}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.einnahmen.gemeinden.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattEinnahmenGemeinden']}
              handeFormChange={handleFormChange}
              defaultIndex={6}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.einnahmen.gesamtsumme.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['VorblattEinnahmenGesamtsumme']}
              handeFormChange={handleFormChange}
              defaultIndex={7}
              hasOnlySumme={true}
              noMoreRegelungs={true}
            />
          </>
        )}
      </>
    </div>
  );
}
