// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import setYear from 'date-fns/setYear';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, GlobalDI, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { AuswirkungenEinnahmenAusgabenController, WidgetRegelungKeysMapInterface } from '../controller';
import { AnzugebendeJahreComponent } from '../universal-components/anzugebende-jahre/component.react';
import { InfluenceExpectedComponent } from '../universal-components/influence-expected/component.react';
import { JahreswirkungComponent } from '../universal-components/jahreswirkung/component.react';
import { TableWidgetComponent } from '../universal-components/table-widget/component.react';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}

export function SonstigerBereicheStaatesComponent(
  props: SimpleModuleProps<EgfaModuleEntityRequestDTO>,
): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [answer, setAnswer] = useState<boolean>();
  const [isDirty, setIsDirty] = useState(false);
  const [regelungsList, setRegelungsList] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);

  const [yearsRange, setYearsRange] = useState<number[]>([]);
  const [hasData, setHasData] = useState<boolean>(false);
  const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = {
    SonstigerBereicheStaatesAusgaben: 'SonstigerBereicheStaatesAusgaben',
    SonstigerBereicheStaatesGesamtsummeAusgaben: 'SonstigerBereicheStaatesGesamtsummeAusgaben',
    SonstigerBereicheStaatesEinnahmen: 'SonstigerBereicheStaatesEinnahmen',
    SonstigerBereicheStaatesGesamtsummeEinnahmen: 'SonstigerBereicheStaatesGesamtsummeEinnahmen',
  };

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleEaOehhEntityDTOAllOf;
      form.setFieldsValue(formDataValue);
      setAnswer(formDataValue.influenceWeitereEinnahmenExpected as boolean);
      setIsDirty(false);
      if (formDataValue.regelungen) {
        const sortedRegelungs = ctrl.getSortedRegelungs(formDataValue.regelungen);

        setRegelungsList(sortedRegelungs);
        const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysMap);
        form.setFieldsValue({
          jahrVon: setYear(new Date(), years[0]),
          jahrBis: setYear(new Date(), years[years.length - 1]),
        });
        setYearsRange(years);
      }
    }
  }, [props.formData]);

  useEffect(() => {
    const data = ctrl.hasDataInRegelungs(ctrl.getRequestedRegelungs(regelungsList, widgetRegelungKeysMap));
    setHasData(data);
  }, [regelungsList]);

  useEffect(() => {
    if (yearsRange.length) {
      const updatedData = ctrl.modifyRegelungsToYearsRange(regelungsList, widgetRegelungKeysMap, yearsRange);
      updateRegelungsList(updatedData);
    }
  }, [yearsRange]);

  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.sonstigerBereicheStaates`;

  const updateRegelungsList = (list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    form.setFieldsValue({ regelungen: list });
    setRegelungsList(list);
  };

  const getActualRegelungsList = () => {
    return form.getFieldValue('regelungen') as EgfaModuleEaOehhRegelungEntityDTO[];
  };
  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };
  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={
          <Title level={1}>
            {t(`${translationKey}.title`)}
            <InfoComponent title={t(`${translationKey}.title`)}>
              <div
                dangerouslySetInnerHTML={{
                  __html: t(`${translationKey}.drawerTitleText`),
                }}
              ></div>
            </InfoComponent>
          </Title>
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={handleFormChange}
      >
        <InfluenceExpectedComponent
          name={'influenceWeitereEinnahmenExpected'}
          translationKey={translationKey}
          onChange={setAnswer}
        />
        {answer && (
          <>
            <FormItemWithInfo
              name={'influenceWeitereEinnahmenSummary'}
              label={
                <span>
                  {t(`${translationKey}.influenceSummary.label`)}
                  <InfoComponent title={t(`${translationKey}.influenceSummary.label`)} withLabelRequired>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: t(`${translationKey}.influenceSummary.labelDrawer`),
                      }}
                    ></div>
                  </InfoComponent>
                </span>
              }
              rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
            >
              <TextArea rows={8} />
            </FormItemWithInfo>
            <AnzugebendeJahreComponent
              nameStart={'jahrVon'}
              nameEnd={'jahrBis'}
              moduleName={props.moduleName}
              hasData={hasData}
              form={form}
              setYearsRange={setYearsRange}
              yearStart={yearsRange[0]}
              yearEnd={yearsRange[yearsRange.length - 1]}
            />
            {/* Ausgaben Durch section */}
            <Title level={2}>{t(`${translationKey}.ausgabenDurch.title`)}</Title>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.ausgabenDurch.text`),
              }}
            ></p>
            <JahreswirkungComponent
              formElementName="influenceWeitereEinnahmenVolleJahreswirkungSummary"
              moduleName={props.moduleName}
              translationKey={translationKey}
              form={form}
              setIsDirty={handleFormChange}
            />
            <Title level={3}>{t(`${translationKey}.ausgaben.title`)}</Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.ausgaben.headerLabel`)}
              addRowLabel={t(`${translationKey}.ausgaben.addRowLabel`)}
              removeRowLabel={t(`${translationKey}.ausgaben.removeRowLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['SonstigerBereicheStaatesAusgaben']}
              handeFormChange={handleFormChange}
              hasTitle={true}
              defaultIndex={0}
            />
            <Title level={3}>{t(`${translationKey}.gesamtsummeAusgaben.title`)}</Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={''}
              widgetRegelungKey={widgetRegelungKeysMap['SonstigerBereicheStaatesGesamtsummeAusgaben']}
              handeFormChange={handleFormChange}
              isStaticRows={true}
              noMoreRegelungs={true}
              rowsLabels={[
                { title: t(`${translationKey}.gesamtsummeAusgaben.tableRowTitles.gesamtsumme`), isSumme: true },
              ]}
              defaultIndex={1}
            />
            <Title level={3}>{t(`${translationKey}.einnahmen.title`)}</Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.einnahmen.headerLabel`)}
              addRowLabel={t(`${translationKey}.einnahmen.addRowLabel`)}
              removeRowLabel={t(`${translationKey}.einnahmen.removeRowLabel`)}
              widgetRegelungKey={widgetRegelungKeysMap['SonstigerBereicheStaatesEinnahmen']}
              handeFormChange={handleFormChange}
              hasTitle={true}
              defaultIndex={2}
            />
            <Title level={3}>{t(`${translationKey}.gesamtsummeEinnahmen.title`)}</Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={''}
              widgetRegelungKey={widgetRegelungKeysMap['SonstigerBereicheStaatesGesamtsummeEinnahmen']}
              handeFormChange={handleFormChange}
              isStaticRows={true}
              noMoreRegelungs={true}
              rowsLabels={[
                { title: t(`${translationKey}.gesamtsummeEinnahmen.tableRowTitles.gesamtsumme`), isSumme: true },
              ]}
              defaultIndex={3}
            />
          </>
        )}
      </FormWrapper>
    </div>
  );
}
