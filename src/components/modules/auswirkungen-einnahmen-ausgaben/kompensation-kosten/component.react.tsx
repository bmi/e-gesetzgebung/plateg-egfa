// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEaOehhEntityDTOAllOf, EgfaModuleEntityRequestDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}

export function KompensationKostenComponent(props: SimpleModuleProps<EgfaModuleEntityRequestDTO>): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleEaOehhEntityDTOAllOf;
      form.setFieldsValue(formDataValue);
      setIsDirty(false);
    }
  }, [props.formData]);

  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute: string = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.kompensationKosten`;

  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`${translationKey}.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={handleFormChange}
      >
        {/* Ausgaben Durch section */}
        <Title level={2}>{t(`${translationKey}.ausgabenDurch.title`)}</Title>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`${translationKey}.ausgabenDurch.text`),
          }}
        ></p>
        <FormItemWithInfo
          name={'influenceGeplanteKompensationDerKostenSummary'}
          label={
            <span>
              {t(`${translationKey}.influenceSummary.label`)}
              <InfoComponent title={t(`${translationKey}.influenceSummary.label`)} withLabelRequired>
                <div
                  dangerouslySetInnerHTML={{
                    __html: t(`${translationKey}.influenceSummary.labelDrawer`),
                  }}
                ></div>
              </InfoComponent>
            </span>
          }
          rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
        >
          <TextArea rows={8} />
        </FormItemWithInfo>
      </FormWrapper>
    </div>
  );
}
