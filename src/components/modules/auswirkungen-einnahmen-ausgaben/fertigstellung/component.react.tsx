// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEntityRequestDTO, EgfaModuleStatusType } from '@plateg/rest-api';

import { routes } from '../../../../shares/routes';
import { SimpleModuleProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import {
  aenderungenSteuereinnahmenRegelungKeysMap,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  verwaltungsUndSonstigeRegelungKeysMap,
  verwaltungsvollzugKeysMap,
  zweckausgabenRegelungKeysMap,
} from '../controller';
import { KompensationKostenSummaryComponent } from '../zusammenfassung/kompensation-kosten-summary/component.react';
import { NormadressatSummaryComponent } from '../zusammenfassung/normadressat-summary/component.react';
import { PatternSummaryComponent } from '../zusammenfassung/pattern-summary/component.react';
import { SonstigerBereicheStaatesSummaryComponent } from '../zusammenfassung/sonstigerBereicheStaates-summary/component.react';
import { VorblattSummaryComponent } from '../zusammenfassung/vorblatt-summary/component.react';

interface SimpleModuleFertigstellungProps extends SimpleModuleProps<EgfaModuleEntityRequestDTO> {
  additionalActions?: React.ReactElement[];
  isLight?: boolean;
  previousLinkName?: string;
  customEditLink?: string;
}
export function FertigstellungComponent(props: SimpleModuleFertigstellungProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute: string = routes[routeKey];
  return (
    <ZusammenfassungWrapper
      egfaId={props.egfaId}
      prevPage={
        props.moduleName +
        '/' +
        ((props.previousLinkName ?? moduleNameRoute === routes.ENAP)
          ? routes.ZUSAMMENFASSUNG
          : routes.WESENTLICHE_AUSWIRKUNGEN)
      }
      onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
      moduleData={props.formData}
      additionalActions={props.additionalActions}
      moduleNameRoute={moduleNameRoute}
      isLight={props.isLight}
    >
      <>
        <Title level={2}>{t(`egfa.modules.${props.moduleName}.fertigstellung.generalTitlePart`)}</Title>
        <Title level={3}>{t(`egfa.modules.${props.moduleName}.vorblatt.haushaltsausgabenTitle`)}</Title>
        <VorblattSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.ZUSAMMENFASSUNG}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
        />
        <Title level={2}>
          {t(`egfa.modules.${props.moduleName}.zusammenfassung.auswirkungenEinzelnenBereicheTitle`)}
        </Title>
        <Title level={3}>{t(`egfa.modules.${props.moduleName}.fertigstellung.haushaltsausgabenTitle`)}</Title>
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.zweckausgaben`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.ZWECKAUSGABEN}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          widgetRegelungKeys={zweckausgabenRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Zweckausgaben}
          influenceSummaryKey={InfluenceSummaryEnum.Zweckausgaben}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Zweckausgaben}
          regelungKeysList={Object.values(zweckausgabenRegelungKeysMap)}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.vollzugsaufwand`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.VOLLZUGSAUFWAND}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          widgetRegelungKeys={verwaltungsvollzugKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.AusgabenVerwaltungsvollzug}
          influenceSummaryKey={InfluenceSummaryEnum.AusgabenVerwaltungsvollzug}
          influenceStellenplaeneKey={InfluenceStellenplaeneEnum.AusgabenVerwaltungsvollzug}
          influenceStellenplaeneSummaryKey={InfluenceStellenplaeneSummaryEnum.AusgabenVerwaltungsvollzug}
          regelungKeysList={Object.values(verwaltungsvollzugKeysMap)}
        />
        <NormadressatSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.NORMADRESSAT}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.verwaltungsUndSonstige`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.VERWALTUNGSUNDSONSTIGE}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          widgetRegelungKeys={verwaltungsUndSonstigeRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Einnahmen}
          influenceSummaryKey={InfluenceSummaryEnum.Einnahmen}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Einnahmen}
          regelungKeysList={Object.values(verwaltungsUndSonstigeRegelungKeysMap)}
        />
        <PatternSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}.aenderungenSteuereinnahmen`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.AENDERUNGENSTEUEREINNAHMEN}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          widgetRegelungKeys={aenderungenSteuereinnahmenRegelungKeysMap}
          influenceExpectedKey={InfluenceExpectedEnum.Steuereinnahmen}
          influenceSummaryKey={InfluenceSummaryEnum.Steuereinnahmen}
          influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Steuereinnahmen}
          regelungKeysList={Object.values(aenderungenSteuereinnahmenRegelungKeysMap)}
        />
        <SonstigerBereicheStaatesSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.SONSTIGERBEREICHESTAATES}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
        />
        <KompensationKostenSummaryComponent
          formData={props.formData}
          translationKey={`egfa.modules.${props.moduleName}`}
          editLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.KOMPENSATIONKOSTEN}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
        />
      </>
    </ZusammenfassungWrapper>
  );
}
