// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { map, switchMap } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaDatenuebernahmeDTOContentInner,
  EgfaItemType,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEaOehhRegelungstabelleEntityDTO,
  EgfaModuleEaOehhRegelungstabellenZeileEntityDTO,
  EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export interface WidgetRegelungKeysMapInterface {
  [s: string]: string;
}
export class AuswirkungenEinnahmenAusgabenController {
  public aeaData: EgfaModuleEaOehhEntityDTOAllOf & EgfaModuleEntityRequestDTO = {};

  /**
   *
   * @param egfaId string
   * @param onFinish void function with boolean parameter success true on success false on Failur
   */
  public exportAuswirkungenEinnahmenAusgabenToEditor(
    egfaId: string,
    onFinish: (success: boolean, error?: AjaxError) => void,
  ): void {
    const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
    const egfaModuleEaOehhControllerApi = GlobalDI.get<EgfaModuleEaOehhControllerApi>('egfaModuleEaOehhControllerApi');
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    loadingStatusController.setLoadingStatus(true);
    egfaModuleEaOehhControllerApi
      .getEgfaModule11({ egfaId: egfaId })
      .pipe(
        map((aeaData) => {
          this.aeaData = aeaData.dto;
          const aeaJSONExport = this.createAeaJSON();
          return aeaJSONExport;
        }),
        switchMap((aeaJSONExportData) => {
          return egfaController.sendEgfaDatenuebernahme({
            id: egfaId,
            moduleType: EgfaModuleType.EaOehh,
            egfaDatenuebernahmeDTO: aeaJSONExportData,
          });
        }),
      )
      .subscribe({
        next: () => {
          onFinish(true);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error) => {
          onFinish(false, error as AjaxError);
          loadingStatusController.setLoadingStatus(false);
          console.error('AuswirkungenEinnahmenAusgaben data export to Editor failed', error);
        },
      });
  }
  public createAeaJSON(): EgfaDatenuebernahmeDTO {
    const aeaJSONExport: EgfaDatenuebernahmeDTO = {
      id: 'auswirkungenEinnahmenAusgabenErgebnisdokumentation',
      content: [],
      children: [],
    };
    aeaJSONExport?.children?.push(this.prepareVorblatt());
    aeaJSONExport?.children?.push(this.prepareBegruendung());
    return aeaJSONExport;
  }
  private prepareBegruendung(): EgfaDatenuebernahmeDTO {
    const result: EgfaDatenuebernahmeDTO = {
      id: 'begruendung',
      title: i18n.t('egfa.modules.einnahmenAusgaben.zusammenfassung.auswirkungenEinzelnenBereicheTitle'),
      content: [],
      children: [],
    };
    result.children?.push({
      id: 'auswirkungenEinzelnenBereicheTitle',
      content: [],
      title: i18n.t('egfa.modules.einnahmenAusgaben.fertigstellung.haushaltsausgabenTitle'),
    });
    result.children?.push(
      this.getPatternSummary(
        {
          translationKey: `egfa.modules.einnahmenAusgaben.zweckausgaben`,
          widgetRegelungKeys: zweckausgabenRegelungKeysMap,
          influenceExpectedKey: InfluenceExpectedEnum.Zweckausgaben,
          influenceSummaryKey: InfluenceSummaryEnum.Zweckausgaben,
          regelungKeysList: Object.values(zweckausgabenRegelungKeysMap),
          id: 'zweckausgaben',
        },

        InfluenceVolleJahreswirkungSummaryEnum.Zweckausgaben,
      ),
    );
    result.children?.push(
      this.getPatternSummary(
        {
          translationKey: `egfa.modules.einnahmenAusgaben.vollzugsaufwand`,
          widgetRegelungKeys: verwaltungsvollzugKeysMap,
          influenceExpectedKey: InfluenceExpectedEnum.AusgabenVerwaltungsvollzug,
          influenceSummaryKey: InfluenceSummaryEnum.AusgabenVerwaltungsvollzug,
          regelungKeysList: Object.values(verwaltungsvollzugKeysMap),
          id: 'vollzugsaufwand',
        },
        undefined,
        InfluenceStellenplaeneEnum.AusgabenVerwaltungsvollzug,
        InfluenceStellenplaeneSummaryEnum.AusgabenVerwaltungsvollzug,
      ),
    );
    result.children?.push(this.getNormadressatSummary('egfa.modules.einnahmenAusgaben'));
    result.children?.push(
      this.getPatternSummary(
        {
          translationKey: `egfa.modules.einnahmenAusgaben.verwaltungsUndSonstige`,
          widgetRegelungKeys: verwaltungsUndSonstigeRegelungKeysMap,
          influenceExpectedKey: InfluenceExpectedEnum.Einnahmen,
          influenceSummaryKey: InfluenceSummaryEnum.Einnahmen,
          regelungKeysList: Object.values(verwaltungsUndSonstigeRegelungKeysMap),
          id: 'verwaltungsUndSonstige',
        },
        InfluenceVolleJahreswirkungSummaryEnum.Einnahmen,
      ),
    );
    result.children?.push(
      this.getPatternSummary(
        {
          translationKey: `egfa.modules.einnahmenAusgaben.aenderungenSteuereinnahmen`,
          widgetRegelungKeys: aenderungenSteuereinnahmenRegelungKeysMap,
          influenceExpectedKey: InfluenceExpectedEnum.Steuereinnahmen,
          influenceSummaryKey: InfluenceSummaryEnum.Steuereinnahmen,
          regelungKeysList: Object.values(aenderungenSteuereinnahmenRegelungKeysMap),
          id: 'steuereinnahmen',
        },
        InfluenceVolleJahreswirkungSummaryEnum.Steuereinnahmen,
      ),
    );
    result.children?.push(this.getSonstigerBereiche('egfa.modules.einnahmenAusgaben'));
    result.children?.push(this.getKompensationKosten('egfa.modules.einnahmenAusgaben'));
    return result;
  }
  private getKompensationKosten(translatKey: string): EgfaDatenuebernahmeDTO {
    const translationKey = `${translatKey}.kompensationKosten`;
    const influenceExpectedSummary = this.aeaData.influenceGeplanteKompensationDerKostenSummary;
    const result: EgfaDatenuebernahmeDTO = {
      id: 'kompensationKosten',
      title: i18n.t(`${translationKey}.linkName`),
      content: [
        {
          id: 'kompensationKostenText',
          data: influenceExpectedSummary
            ? influenceExpectedSummary
            : i18n.t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`).toString(),
          type: EgfaItemType.Text,
        },
      ],
      children: [],
    };
    return result;
  }
  private getSonstigerBereiche(translatKey: string): EgfaDatenuebernahmeDTO {
    const { regelungsWithData, yearsRange } = this.getYearsRegelungswithData(widgetRegelungKeysSonstigerBereicheMap);
    const influenceExpected = this.aeaData.influenceWeitereEinnahmenExpected;
    const influenceExpectedSummary = this.aeaData.influenceWeitereEinnahmenSummary;
    const influenceVolleJahreswirkungSummary = this.aeaData.influenceWeitereEinnahmenVolleJahreswirkungSummary;
    const translationKey = `${translatKey}.sonstigerBereicheStaates`;
    const result: EgfaDatenuebernahmeDTO = {
      id: 'sonstigerBereicheSummary',
      content: [],
      children: [],
    };
    const sonstigerBereicheNode: EgfaDatenuebernahmeDTO = {
      id: 'sonstigerBereicheTitle',
      title: i18n.t(`${translationKey}.linkName`),
      content: [],
    };
    if (!influenceExpected) {
      sonstigerBereicheNode.content.push({
        id: `sonstigerBereicheText`,
        data: i18n.t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`).toString(),
        type: EgfaItemType.Text,
      });
    } else {
      if (influenceExpectedSummary) {
        sonstigerBereicheNode.content.push({
          id: `sonstigerBereicheText`,
          data: influenceExpectedSummary,
          type: EgfaItemType.Text,
        });
      }
    }
    result.children?.push(sonstigerBereicheNode);
    if (influenceExpected && influenceVolleJahreswirkungSummary) {
      result.children?.push({
        id: 'sonstigerBereicheVolleJahreswirkungSummary',
        title: i18n.t(`${translationKey}.jahreswirkung.title`),
        content: [
          {
            id: 'sonstigerBereicheVolleJahreswirkungSummaryText',
            data: influenceVolleJahreswirkungSummary,
            type: EgfaItemType.Text,
          },
        ],
      });
    }
    if (influenceExpected && regelungsWithData.length > 0) {
      result.children?.push({
        id: `sonstigerBereicheTable`,
        title: i18n.t(`${translationKey}.ausgaben.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `sonstigerBereiche`,
          tableHeaderLabel: i18n.t(`${translationKey}.ausgaben.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesAusgaben'],
          regelungsWithData: regelungsWithData,
          yearsRange: yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `sonstigerBereicheGesamtTable`,
        title: i18n.t(`${translationKey}.gesamtsummeAusgaben.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `sonstigerBereicheGesamt`,
          tableHeaderLabel: '',
          widgetRegelungKey: widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesGesamtsummeAusgaben'],
          regelungsWithData: regelungsWithData,
          yearsRange: yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: false,
        }),
      });
      result.children?.push({
        id: `sonstigerBereicheEinnahmenTable`,
        title: i18n.t(`${translationKey}.einnahmen.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `sonstigerBereicheEinnahmen`,
          tableHeaderLabel: i18n.t(`${translationKey}.einnahmen.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesEinnahmen'],
          regelungsWithData: regelungsWithData,
          yearsRange: yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `sonstigerBereicheEinnahmenGesamtTable`,
        title: i18n.t(`${translationKey}.gesamtsummeEinnahmen.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `sonstigerBereicheEinnahmenGesamt`,
          tableHeaderLabel: '',
          widgetRegelungKey: widgetRegelungKeysSonstigerBereicheMap['SonstigerBereicheStaatesGesamtsummeEinnahmen'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
    }

    return result;
  }
  private getNormadressatSummary(translatKey: string): EgfaDatenuebernahmeDTO {
    const { regelungsWithData, yearsRange } = this.getYearsRegelungswithData(widgetRegelungKeysNormadressatMap);
    const influenceExpected = this.aeaData.influenceVerwaltungAlsNormadressatExpected;
    const influenceExpectedSummary = this.aeaData.influenceVerwaltungAlsNormadressatSummary;
    const influenceStellenplanen = this.aeaData.influenceVerwaltungAlsNormadressatStellenplaene;
    const influenceStellenplanenSummary = this.aeaData.influenceVerwaltungAlsNormadressatStellenplaeneSummary;
    const translationKey = `${translatKey}.normadressat`;
    const result: EgfaDatenuebernahmeDTO = {
      id: 'normadressatSummary',
      content: [],
      children: [],
    };
    const normadressatSummaryNode: EgfaDatenuebernahmeDTO = {
      id: 'normadressatSummaryTitle',
      title: i18n.t(`${translationKey}.linkName`),
      content: [],
    };
    if (!influenceExpected) {
      normadressatSummaryNode.content?.push({
        id: `normadressatSummaryText`,
        data: i18n.t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`).toString(),
        type: EgfaItemType.Text,
      });
    } else {
      if (influenceExpected && influenceExpectedSummary) {
        normadressatSummaryNode.content.push({
          id: `normadressatSummaryText`,
          data: influenceExpectedSummary,
          type: EgfaItemType.Text,
        });
      }
    }
    result.children?.push(normadressatSummaryNode);
    if (influenceExpected && regelungsWithData.length > 0) {
      result.children?.push({
        id: `normadressatAusgabenDurchTable`,
        title: i18n.t(`${translationKey}.ausgabenDurch.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `normadressatAusgabenDurch`,
          tableHeaderLabel: i18n.t(`${translationKey}.normadressatRegelung.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysNormadressatMap['NormadressatRegelung'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `normadressatAusgabenGesamtTable`,
        title: i18n.t(`${translationKey}.gesamtsumme.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `normadressatAusgabenGesamt`,
          tableHeaderLabel: i18n.t(`${translationKey}.gesamtsumme.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysNormadressatMap['NormadressatGesamtsumme'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: false,
        }),
      });
    }
    if (influenceExpected && influenceStellenplanen && influenceStellenplanenSummary) {
      result.children?.push({
        id: 'stellenplanenSummary',
        content: [
          {
            id: 'stellenplanenSummaryText',
            data: influenceStellenplanenSummary,
            type: EgfaItemType.Text,
          },
        ],
      });
    }
    if (influenceExpected && influenceStellenplanen && regelungsWithData.length > 0) {
      result.children?.push({
        id: `normadressatPlanstellenTable`,
        title: i18n.t(`${translationKey}.planstellen.influenceExpected.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `normadressatPlanstellen`,
          tableHeaderLabel: '',
          widgetRegelungKey: widgetRegelungKeysNormadressatMap['NormadressatStellenplaene'],
          regelungsWithData,
          yearsRange,
          hasCurrency: false,
          hasOnlySumme: false,
          hasTitle: false,
        }),
      });
      result.children?.push({
        id: `normadressatPlanstellenGesamtTable`,
        title: i18n.t(`${translationKey}.planstellen.gesamtTable.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `normadressatPlanstellenGesamt`,
          tableHeaderLabel: i18n.t(`${translationKey}.planstellen.gesamtTable.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysNormadressatMap['NormadressatGesamtStellenplaene'],
          regelungsWithData,
          yearsRange,
          hasCurrency: false,
          hasOnlySumme: false,
          hasTitle: false,
        }),
      });
    }
    return result;
  }
  private getPatternSummary(
    patternData: {
      translationKey: string;
      widgetRegelungKeys: WidgetRegelungKeysMapInterface;
      influenceExpectedKey: InfluenceExpectedEnum;
      influenceSummaryKey: InfluenceSummaryEnum;
      regelungKeysList: string[];
      id: string;
    },
    influenceVolleJahreswirkungSummaryKey?: InfluenceVolleJahreswirkungSummaryEnum,
    influenceStellenplaeneKey?: InfluenceStellenplaeneEnum,
    influenceStellenplaeneSummaryKey?: InfluenceStellenplaeneSummaryEnum,
  ): EgfaDatenuebernahmeDTO {
    const { regelungsWithData, yearsRange } = this.getYearsRegelungswithData(patternData.widgetRegelungKeys);
    const influenceExpected = this.aeaData[patternData.influenceExpectedKey];
    const influenceExpectedSummary = this.aeaData[patternData.influenceSummaryKey];
    const influenceVolleJahreswirkungSummary = influenceVolleJahreswirkungSummaryKey
      ? this.aeaData[influenceVolleJahreswirkungSummaryKey]
      : null;
    let influenceStellenplanen = false;
    let influenceStellenplanenSummary = null;
    if (influenceStellenplaeneKey && influenceStellenplaeneSummaryKey) {
      influenceStellenplanen = this.aeaData[influenceStellenplaeneKey] as boolean;
      influenceStellenplanenSummary = this.aeaData[influenceStellenplaeneSummaryKey] as string;
    }
    const patternSummaryChild: EgfaDatenuebernahmeDTO = {
      id: `${patternData.id}PatternSummaryTitle`,
      title: i18n.t(`${patternData.translationKey}.linkName`).toString(),
      content: [],
    };
    const result: EgfaDatenuebernahmeDTO = {
      id: `${patternData.id}PatternSummary`,
      children: [patternSummaryChild],
      content: [],
    };
    if (!influenceExpected) {
      patternSummaryChild.content.push({
        id: `${patternData.id}PatternSummaryText`,
        data: i18n.t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`).toString(),
        type: EgfaItemType.Text,
      });
    } else {
      if (influenceExpected && influenceExpectedSummary) {
        patternSummaryChild.content.push({
          id: `${patternData.id}PatternSummaryText`,
          data: influenceExpectedSummary,
          type: EgfaItemType.Text,
        });
      }
      if (influenceExpected && influenceVolleJahreswirkungSummary) {
        result.children?.push({
          id: `${patternData.id}PatternSummaryJahreswirkungTitle`,
          title: i18n.t(`${patternData.translationKey}.jahreswirkung.title`).toString(),
          content: [
            {
              id: `${patternData.id}PatternSummaryJahreswirkungText`,
              data: influenceVolleJahreswirkungSummary,
              type: EgfaItemType.Text,
            },
          ],
        });
      }
    }
    if (influenceExpected && regelungsWithData.length > 0) {
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetBundTable`,
        title: i18n.t(`${patternData.translationKey}.bund.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetBund`,
          tableHeaderLabel: i18n.t(`${patternData.translationKey}.bund.headerLabel`),
          widgetRegelungKey: patternData.regelungKeysList[0],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetLaenderTable`,
        title: i18n.t(`${patternData.translationKey}.laender.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetLaender`,
          tableHeaderLabel: '',
          widgetRegelungKey: patternData.regelungKeysList[1],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetGemeindenTable`,
        title: i18n.t(`${patternData.translationKey}.gemeinden.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetGemeinden`,
          tableHeaderLabel: '',
          widgetRegelungKey: patternData.regelungKeysList[2],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetGesamtsummeTable`,
        title: i18n.t(`${patternData.translationKey}.gesamtsumme.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetGesamtSumme`,
          tableHeaderLabel: i18n.t(`${patternData.translationKey}.gesamtsumme.headerLabel`),
          widgetRegelungKey: patternData.regelungKeysList[3],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
    }
    if (influenceExpected && influenceStellenplanen && influenceStellenplanenSummary) {
      result.children?.push({
        id: `${patternData.id}InfluenceSummary`,
        title: '',
        content: [
          {
            id: `${patternData.id}InfluenceSummaryText`,
            data: influenceStellenplanenSummary,
            type: EgfaItemType.Text,
          },
        ],
      });
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetPlanstellenTable`,
        title: i18n.t(`${patternData.translationKey}.planstellen.influenceExpected.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetPlanstellen`,
          tableHeaderLabel: '',
          widgetRegelungKey: patternData.regelungKeysList[4],
          regelungsWithData,
          yearsRange,
          hasCurrency: false,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
      result.children?.push({
        id: `${patternData.id}PatternSummaryWidgetPlanstellGesamtenTable`,
        title: i18n.t(`${patternData.translationKey}.planstellen.gesamtTable.title`).toString(),
        content: [],
        children: this.getTableWidget({
          id: `${patternData.id}WidgetPlanstellGesamt`,
          tableHeaderLabel: i18n.t(`${patternData.translationKey}.planstellen.gesamtTable.headerLabel`),
          widgetRegelungKey: patternData.regelungKeysList[5],
          regelungsWithData,
          yearsRange,
          hasCurrency: false,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      });
    }
    return result;
  }
  private getTableWidget(tableWidgetData: {
    id: string;
    tableHeaderLabel: string;
    widgetRegelungKey: string;
    regelungsWithData: EgfaModuleEaOehhRegelungEntityDTO[];
    yearsRange: number[];
    hasCurrency: boolean;
    hasOnlySumme: boolean;
    hasTitle: boolean;
  }): EgfaDatenuebernahmeDTO[] {
    return tableWidgetData.regelungsWithData
      .filter((regelung) => regelung.regelungKey.indexOf(tableWidgetData.widgetRegelungKey) !== -1)
      .map((item, innerIdx) => {
        const tableContent: EgfaDatenuebernahmeDTO = {
          id: `${tableWidgetData.id}Table${innerIdx}`,
          title: tableWidgetData.hasTitle ? item.bezeichnung : '',
          content: [],
        };
        tableContent.content.push(
          ...item.regelungstabellen.map((regelungstabel, idx) => {
            return this.getRegelungTable(
              `${tableWidgetData.id}Content${innerIdx}${idx}`,
              regelungstabel,
              tableWidgetData.yearsRange,
              tableWidgetData.tableHeaderLabel,
              tableWidgetData.widgetRegelungKey,
              tableWidgetData.hasCurrency,
              tableWidgetData.hasOnlySumme,
            );
          }),
        );
        return tableContent;
      });
  }
  private getRegelungTable(
    id: string,
    regTable: EgfaModuleEaOehhRegelungstabelleEntityDTO,
    yearsRange: number[],
    tableHeaderLabel: string,
    widgetRegelungKey: string,
    hasCurrency: boolean,
    hasOnlySumme: boolean,
  ): EgfaDatenuebernahmeDTOContentInner {
    let tableHeader: string = tableHeaderLabel;
    if (
      widgetRegelungKey === widgetRegelungKeysNormadressatMap['NormadressatStellenplaene'] ||
      widgetRegelungKey == 'VerwaltungsvollzugStellenplaene'
    ) {
      tableHeader = regTable.bezeichnung;
    }
    const result: EgfaDatenuebernahmeDTOContentInner = {
      id: `${id}RegelungTable`,
      type: EgfaItemType.Table,
    };
    if (hasOnlySumme) {
      result.data = this.getSummeRow(
        regTable.regelungstabellenZeilen,
        tableHeader,
        hasCurrency ? 'T€' : '',
        yearsRange,
      );
    } else {
      result.data = this.getZeilenRow(
        regTable.regelungstabellenZeilen,
        tableHeader,
        hasCurrency ? 'T€' : '',
        yearsRange,
      );
    }
    return result;
  }

  private prepareVorblatt(): EgfaDatenuebernahmeDTO {
    const translationKey = `egfa.modules.einnahmenAusgaben.vorblatt`;
    const { regelungsWithData, yearsRange } = this.getYearsRegelungswithData(widgetRegelungKeysVorblattMap);
    const influenceExpectedSummary = this.aeaData.influenceSummary;
    const result: EgfaDatenuebernahmeDTO = {
      id: 'vorblatt',
      title: i18n.t(`${translationKey}.title`),
      content: [],
      children: [
        {
          id: 'vorblattD',
          title: i18n.t(`egfa.modules.einnahmenAusgaben.vorblatt.haushaltsausgabenTitle`),
          content: [
            {
              id: 'textVorblattD',
              data: this.aeaData.influenceSummary,
              type: EgfaItemType.Text,
            },
          ],
        },
      ],
    };
    if (influenceExpectedSummary && regelungsWithData.length > 0) {
      result.children?.push({
        id: 'vorblattAusgaben',
        title: i18n.t(`${translationKey}.ausgaben.title`),
        content: [],
      });
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattAusgabenBund',
          tableHeaderLabel: i18n.t(`${translationKey}.ausgaben.bund.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattAusgabenBund'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattAusgabenLaender',
          tableHeaderLabel: i18n.t(`${translationKey}.ausgaben.laender.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattAusgabenLaender'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattAusgabenGemeinden',
          tableHeaderLabel: i18n.t(`${translationKey}.ausgaben.gemeinden.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattAusgabenGemeinden'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattAusgabenGesamtsumme',
          tableHeaderLabel: i18n.t(`${translationKey}.ausgaben.gesamtsumme.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattAusgabenGesamtsumme'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
      result.children?.push({
        id: 'vorblattEinnahmen',
        title: i18n.t(`${translationKey}.einnahmen.title`),
        content: [],
      });
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattEinnahmenBund',
          tableHeaderLabel: i18n.t(`${translationKey}.einnahmen.bund.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattEinnahmenBund'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: false,
          hasTitle: true,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattEinnahmenLaender',
          tableHeaderLabel: i18n.t(`${translationKey}.einnahmen.laender.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattEinnahmenLaender'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattEinnahmenGemeinden',
          tableHeaderLabel: i18n.t(`${translationKey}.einnahmen.gemeinden.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattEinnahmenGemeinden'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
      result.children?.push(
        ...this.getTableWidget({
          id: 'vorblattEinnahmenGesamtsumme',
          tableHeaderLabel: i18n.t(`${translationKey}.einnahmen.gesamtsumme.headerLabel`),
          widgetRegelungKey: widgetRegelungKeysVorblattMap['VorblattEinnahmenGesamtsumme'],
          regelungsWithData,
          yearsRange,
          hasCurrency: true,
          hasOnlySumme: true,
          hasTitle: false,
        }),
      );
    }
    return result;
  }

  private getYearsRegelungswithData(widgetRegelungKeysMap: WidgetRegelungKeysMapInterface) {
    const sortedRegelungs = this.getSortedRegelungs(this.aeaData.regelungen || []);
    const regelungsWithData = this.regelungsWithData(sortedRegelungs);
    const yearsRange = this.prepareYearsRange(sortedRegelungs, widgetRegelungKeysMap);
    return { yearsRange, regelungsWithData };
  }

  private getZeilenRow(
    zeilList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    dataLabel: string,
    unitSymbol: string,
    yearsRange: number[],
  ) {
    let unit = ` ${unitSymbol}`;
    if (unitSymbol === '') {
      unit = '';
    }
    const result = [[dataLabel, ...yearsRange.map((item) => item.toString())]];
    zeilList.forEach((item) => {
      result.push([item.bezeichnung, ...this.getYearsValues(item.regelungstabellenZeileWerte, unit)]);
    });

    return result;
  }
  private getSummeRow(
    zeilList: EgfaModuleEaOehhRegelungstabellenZeileEntityDTO[],
    dataLabel: string,
    unitSymbol: string,
    yearsRange: number[],
  ) {
    let unit = ` ${unitSymbol}`;
    if (unitSymbol === '') {
      unit = '';
    }
    const result = zeilList.find((ziel) => ziel.bezeichnung === 'Summe');
    if (result) {
      return [
        [dataLabel, ...yearsRange.map((item) => item.toString())],
        [result.bezeichnung, ...this.getYearsValues(result.regelungstabellenZeileWerte, unit)],
      ];
    }
    return [[dataLabel, ...yearsRange.map((item) => item.toString())], []];
  }
  private getYearsValues(values: EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[], unit: string) {
    return values.map((value) => {
      return `${value.betrag ? value.betrag.toLocaleString('de-DE') : ''}${unit}`;
    });
  }
  /**
   * Prepare years range according to already existed regelungs
   * @param regelungsList
   * @param keysMap
   * @returns
   */
  public prepareYearsRange(
    regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] | undefined,
    keysMap: WidgetRegelungKeysMapInterface,
  ): number[] {
    if (!regelungsList) {
      return this.getDefaultYearsRange();
    }
    const requestedList = this.getRequestedRegelungs(regelungsList, keysMap);

    if (!requestedList.length) {
      const yearsList: number[] = regelungsList
        .flatMap((regelung) => regelung.regelungstabellen)
        .flatMap((table) => table.regelungstabellenZeilen)
        .flatMap((zeil) => zeil.regelungstabellenZeileWerte)
        .map((werte) => werte.jahr);
      const maxYear = yearsList.length ? Math.max(...yearsList) : undefined;
      return this.getDefaultYearsRange(maxYear);
    }
    return requestedList[0].regelungstabellen[0].regelungstabellenZeilen[0].regelungstabellenZeileWerte.map(
      (zeil) => zeil.jahr,
    );
  }

  /**
   * Get default years range
   * @returns
   */
  private getDefaultYearsRange(maxYear?: number): number[] {
    const startYear = new Date().getFullYear();
    const yearsLength = maxYear ? maxYear - startYear + 1 : 5;
    const yearsRange = [];
    for (let i = 0; i < yearsLength; i++) {
      yearsRange.push(startYear + i);
    }
    return yearsRange;
  }

  /**
   * Get list only with regelungs applied to the map
   * @param regelungsList
   * @param keysMap
   * @returns
   */
  public getRequestedRegelungs(
    regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] | undefined,
    keysMap: WidgetRegelungKeysMapInterface,
  ) {
    if (!regelungsList) {
      return [];
    }
    const keysList = Object.values(keysMap);
    // Get only regulungs what has key from keyMap

    return regelungsList.filter((regelung) => {
      const regelungKeyPrefix = regelung.regelungKey.split('_');
      return keysList.some((key) => regelungKeyPrefix[0].indexOf(key) !== -1);
    });
  }

  /**
   * Modify regelungs according to years range
   * @param regelungsList
   * @param keysMap
   * @param yearsRange
   * @returns
   */
  public modifyRegelungsToYearsRange(
    regelungsList: EgfaModuleEaOehhRegelungEntityDTO[] | undefined,
    keysMap: WidgetRegelungKeysMapInterface,
    yearsRange: number[],
  ): EgfaModuleEaOehhRegelungEntityDTO[] {
    if (!regelungsList) {
      return [];
    }
    const keysList = Object.values(keysMap);

    return regelungsList.map((regelung) => {
      const regelungKeyPrefix = regelung.regelungKey.split('_');
      const regelungKey = regelungKeyPrefix[0];
      if (keysList.some((key) => regelungKey.indexOf(key) !== -1)) {
        regelung.regelungstabellen.forEach((regTab) => {
          regTab.regelungstabellenZeilen.forEach((ziel) => {
            ziel.regelungstabellenZeileWerte = this.updateZeilValues(ziel.regelungstabellenZeileWerte, yearsRange);
          });
        });
      }
      return regelung;
    });
  }
  /**
   * When changing columns quantity update form data and table content
   */
  private updateZeilValues(
    valuesList: EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[],
    yearsRange: number[],
  ): EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[] {
    const newValuesList: EgfaModuleEaOehhRegelungstabellenZeileWertEntityDTO[] = [];
    yearsRange.forEach((year) => {
      const newValue = valuesList.filter((value) => value.jahr === year);
      if (newValue.length) {
        newValuesList.push(newValue[0]);
      } else {
        newValuesList.push({
          jahr: year,
          betrag: undefined,
        });
      }
    });

    return newValuesList;
  }

  public hasDataInRegelungs(regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]): boolean {
    return regelungsList
      .flatMap((regelung) => {
        return regelung.regelungstabellen[0].regelungstabellenZeilen;
      })
      .flatMap((zeil) => {
        return zeil.regelungstabellenZeileWerte;
      })
      .some((item) => item.betrag);
  }
  /**
   * Sort regelungs and zeilen inside regelungs in correct order.
   * @param regelungsList
   * @returns
   */
  public getSortedRegelungs(regelungsList: EgfaModuleEaOehhRegelungEntityDTO[]): EgfaModuleEaOehhRegelungEntityDTO[] {
    return regelungsList
      .map((regelung) => {
        regelung.regelungstabellen.sort((regTabelle1, regTabelle2) => {
          const indexTabelle1 = +regTabelle1.tabellenKey.split('_').slice(-1)[0];
          const indexTabelle2 = +regTabelle2.tabellenKey.split('_').slice(-1)[0];
          return indexTabelle1 - indexTabelle2;
        });
        regelung.regelungstabellen.forEach((regTabelle) => {
          // Store summe zeil row separatly and add later as last item
          const zeilSumm = regTabelle.regelungstabellenZeilen.filter(
            (zeil) => zeil.zeileKey.indexOf('_summe') !== -1,
          )[0];
          // Exclude summe zeil row from sorting
          const zeilen = regTabelle.regelungstabellenZeilen.filter((zeil) => zeil.zeileKey.indexOf('_summe') === -1);
          // Sort zeilen
          zeilen.sort((zeil1, zeil2) => {
            // Take zeil index
            const indexZeil1 = +zeil1.zeileKey.split('_').slice(-1)[0];
            const indexZeil2 = +zeil2.zeileKey.split('_').slice(-1)[0];
            return indexZeil1 - indexZeil2;
          });
          // If summe row exist push it as last item
          if (zeilSumm) {
            zeilen.push(zeilSumm);
          }
          regTabelle.regelungstabellenZeilen = [...zeilen];
        });

        return regelung;
      })
      .sort((regelung1, regelung2) => {
        // Take regelung index
        const indexRegelung1 = +regelung1.regelungKey.split('_').slice(-1)[0];
        const indexRegelung2 = +regelung2.regelungKey.split('_').slice(-1)[0];
        return indexRegelung1 - indexRegelung2;
      });
  }

  public regelungsWithData(regelungen: EgfaModuleEaOehhRegelungEntityDTO[]): EgfaModuleEaOehhRegelungEntityDTO[] {
    return regelungen.map((regelung) => {
      const filteredZeilen = regelung.regelungstabellen[0].regelungstabellenZeilen.filter(
        (zeile) => zeile.bezeichnung !== '',
      );
      // check later regelungstabellen[0]
      regelung.regelungstabellen[0].regelungstabellenZeilen = filteredZeilen;
      return regelung;
    });
  }
}
export const widgetRegelungKeysSonstigerBereicheMap: WidgetRegelungKeysMapInterface = {
  SonstigerBereicheStaatesAusgaben: 'SonstigerBereicheStaatesAusgaben',
  SonstigerBereicheStaatesGesamtsummeAusgaben: 'SonstigerBereicheStaatesGesamtsummeAusgaben',
  SonstigerBereicheStaatesEinnahmen: 'SonstigerBereicheStaatesEinnahmen',
  SonstigerBereicheStaatesGesamtsummeEinnahmen: 'SonstigerBereicheStaatesGesamtsummeEinnahmen',
};
export const widgetRegelungKeysNormadressatMap: WidgetRegelungKeysMapInterface = {
  NormadressatRegelung: 'NormadressatRegelung',
  NormadressatGesamtsumme: 'NormadressatGesamtsumme',
  NormadressatStellenplaene: 'NormadressatStellenplaene',
  NormadressatGesamtStellenplaene: 'NormadressatGesamtStellenplaene',
};
export const widgetRegelungKeysVorblattMap: WidgetRegelungKeysMapInterface = {
  VorblattAusgabenBund: 'VorblattAusgabenBund',
  VorblattAusgabenLaender: 'VorblattAusgabenLaender',
  VorblattAusgabenGemeinden: 'VorblattAusgabenGemeinden',
  VorblattAusgabenGesamtsumme: 'VorblattAusgabenGesamtsumme',
  VorblattEinnahmenBund: 'VorblattEinnahmenBund',
  VorblattEinnahmenLaender: 'VorblattEinnahmenLaender',
  VorblattEinnahmenGemeinden: 'VorblattEinnahmenGemeinden',
  VorblattEinnahmenGesamtsumme: 'VorblattEinnahmenGesamtsumme',
};
export const zweckausgabenRegelungKeysMap: WidgetRegelungKeysMapInterface = {
  ZweckausgabenBund: 'ZweckausgabenBund',
  ZweckausgabenLaender: 'ZweckausgabenLaender',
  ZweckausgabenGemeinden: 'ZweckausgabenGemeinden',
  ZweckausgabenGesamtsumme: 'ZweckausgabenGesamtsumme',
};

export const verwaltungsUndSonstigeRegelungKeysMap: WidgetRegelungKeysMapInterface = {
  VerwaltungsUndSonstigeBund: 'VerwaltungsUndSonstigeBund',
  VerwaltungsUndSonstigeLaender: 'VerwaltungsUndSonstigeLaender',
  VerwaltungsUndSonstigeGemeinden: 'VerwaltungsUndSonstigeGemeinden',
  VerwaltungsUndSonstigeGesamtsumme: 'VerwaltungsUndSonstigeGesamtsumme',
};

export const aenderungenSteuereinnahmenRegelungKeysMap: WidgetRegelungKeysMapInterface = {
  AenderungenSteuereinnahmenBund: 'AenderungenSteuereinnahmenBund',
  AenderungenSteuereinnahmenLaender: 'AenderungenSteuereinnahmenLaender',
  AenderungenSteuereinnahmenGemeinden: 'AenderungenSteuereinnahmenGemeinden',
  AenderungenSteuereinnahmenGesamtsumme: 'AenderungenSteuereinnahmenGesamtsumme',
};

export const verwaltungsvollzugKeysMap: WidgetRegelungKeysMapInterface = {
  VerwaltungsvollzugBund: 'VerwaltungsvollzugBund',
  VerwaltungsvollzugLaender: 'VerwaltungsvollzugLaender',
  VerwaltungsvollzugGemeinden: 'VerwaltungsvollzugGemeinden',
  VerwaltungsvollzugGesamtsumme: 'VerwaltungsvollzugGesamtsumme',
  VerwaltungsvollzugStellenplaene: 'VerwaltungsvollzugStellenplaene',
  VerwaltungsvollzugGesamtStellenplaene: 'VerwaltungsvollzugGesamtStellenplaene',
};

export enum InfluenceExpectedEnum {
  Zweckausgaben = 'influenceZweckausgabenExpected',
  AusgabenVerwaltungsvollzug = 'influenceAusgabenVerwaltungsvollzug',
  Einnahmen = 'influenceEinnahmenExpected',
  Steuereinnahmen = 'influenceSteuereinnahmenExpected',
}
export enum InfluenceSummaryEnum {
  Zweckausgaben = 'influenceZweckausgabenSummary',
  AusgabenVerwaltungsvollzug = 'influenceAusgabenVerwaltungsvollzugSummary',
  Einnahmen = 'influenceEinnahmenSummary',
  Steuereinnahmen = 'influenceSteuereinnahmenSummary',
}

export enum InfluenceVolleJahreswirkungSummaryEnum {
  Zweckausgaben = 'influenceVolleJahreswirkungSummary',
  Einnahmen = 'influenceEinnahmenVolleJahreswirkungSummary',
  Steuereinnahmen = 'influenceSteuereinnahmenVolleJahreswirkungSummary',
}

export enum InfluenceStellenplaeneEnum {
  AusgabenVerwaltungsvollzug = 'influenceVollzugsaufwandStellenplaene',
}
export enum InfluenceStellenplaeneSummaryEnum {
  AusgabenVerwaltungsvollzug = 'influenceVollzugsaufwandStellenplaeneSummary',
}
