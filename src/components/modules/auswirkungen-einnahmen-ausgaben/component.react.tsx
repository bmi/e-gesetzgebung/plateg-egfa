// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEaOehhEntityDTO,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { EinnahmenAusgabenCommonComponent } from './aea-common/component.react';
import {
  aenderungenSteuereinnahmenRegelungKeysMap,
  AuswirkungenEinnahmenAusgabenController,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  verwaltungsUndSonstigeRegelungKeysMap,
  verwaltungsvollzugKeysMap,
  zweckausgabenRegelungKeysMap,
} from './controller';
import { AuswirkungenEinnahmenAusgabenEinleitungContentComponent } from './einleitung/component.react';
import { FertigstellungComponent } from './fertigstellung/component.react';
import { KompensationKostenComponent } from './kompensation-kosten/component.react';
import { NormadressatComponent } from './normadressat/component.react';
import { SonstigerBereicheStaatesComponent } from './sonstigerBereicheStaates/component.react';
import { ZusammenfassungComponent } from './zusammenfassung/component.react';

export function AuswirkungenEinnahmenAusgabenComponent(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [auswirkungenEinnahmenAusgabenCheckData, setAuswirkungenEinnahmenAusgabenCheckData] =
    useState<EgfaModuleEaOehhEntityDTO>();
  const egfaModuleEaOehhControllerApi = GlobalDI.get<EgfaModuleEaOehhControllerApi>('egfaModuleEaOehhControllerApi');

  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaModuleEaOehhControllerApi.getEgfaModule11({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleEaOehhEntityDTO) => {
    return egfaModuleEaOehhControllerApi.modifyEgfaModule11({
      egfaId: egfaId,
      egfaModuleEaOehhEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN);
  }, []);

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());

  const PageRoutingCompAEAC = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
  ): React.ReactElement => {
    useEffect(() => {
      if (updateItemDisableStatus) {
        const fertigstellungIsDisabled =
          ((!auswirkungenEinnahmenAusgabenCheckData?.influenceSummary ||
            auswirkungenEinnahmenAusgabenCheckData?.influenceSummary === '') &&
            auswirkungenEinnahmenAusgabenCheckData?.aktionen?.includes(AktionType.Schreiben)) ||
          false;
        updateItemDisableStatus([routes.FERTIGSTELLUNG], fertigstellungIsDisabled);
      }
    }, [auswirkungenEinnahmenAusgabenCheckData]);
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: auswirkungenEinnahmenAusgabenCheckData,
      onSave,
      handleFormChange,
    };
    const PDFExportBtn = ctrl.getPDFExportAction(egfaId, moduleName, auswirkungenEinnahmenAusgabenCheckData?.status);
    return (
      <Switch>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.FERTIGSTELLUNG}`]}
        >
          <FertigstellungComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            additionalActions={PDFExportBtn}
            previousLinkName={routes.ZUSAMMENFASSUNG}
          />
        </Route>
        {(auswirkungenEinnahmenAusgabenCheckData?.aktionen?.includes(AktionType.Schreiben) === false ||
          auswirkungenEinnahmenAusgabenCheckData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.EINLEITUNG}`]}
        >
          <SimpleModuleEinleitung
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.ZWECKAUSGABEN}
            customContent={<AuswirkungenEinnahmenAusgabenEinleitungContentComponent />}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.ZWECKAUSGABEN}`]}
        >
          <EinnahmenAusgabenCommonComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.VOLLZUGSAUFWAND}
            previousLinkName={routes.EINLEITUNG}
            regelungKeysMap={zweckausgabenRegelungKeysMap}
            regelungKeysList={Object.values(zweckausgabenRegelungKeysMap)}
            translationKey="zweckausgaben"
            influenceExpectedKey={InfluenceExpectedEnum.Zweckausgaben}
            influenceSummaryKey={InfluenceSummaryEnum.Zweckausgaben}
            influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Zweckausgaben}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.VOLLZUGSAUFWAND}`]}
        >
          <EinnahmenAusgabenCommonComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.NORMADRESSAT}
            previousLinkName={routes.ZWECKAUSGABEN}
            regelungKeysMap={verwaltungsvollzugKeysMap}
            regelungKeysList={Object.values(verwaltungsvollzugKeysMap)}
            translationKey="vollzugsaufwand"
            influenceStellenplaeneKey={InfluenceStellenplaeneEnum.AusgabenVerwaltungsvollzug}
            influenceStellenplaeneSummaryKey={InfluenceStellenplaeneSummaryEnum.AusgabenVerwaltungsvollzug}
            influenceExpectedKey={InfluenceExpectedEnum.AusgabenVerwaltungsvollzug}
            influenceSummaryKey={InfluenceSummaryEnum.AusgabenVerwaltungsvollzug}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.NORMADRESSAT}`]}
        >
          <NormadressatComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.VERWALTUNGSUNDSONSTIGE}
            previousLinkName={routes.VOLLZUGSAUFWAND}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.VERWALTUNGSUNDSONSTIGE}`,
          ]}
        >
          <EinnahmenAusgabenCommonComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.AENDERUNGENSTEUEREINNAHMEN}
            previousLinkName={routes.NORMADRESSAT}
            regelungKeysMap={verwaltungsUndSonstigeRegelungKeysMap}
            regelungKeysList={Object.values(verwaltungsUndSonstigeRegelungKeysMap)}
            translationKey="verwaltungsUndSonstige"
            influenceExpectedKey={InfluenceExpectedEnum.Einnahmen}
            influenceSummaryKey={InfluenceSummaryEnum.Einnahmen}
            influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Einnahmen}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.AENDERUNGENSTEUEREINNAHMEN}`,
          ]}
        >
          <EinnahmenAusgabenCommonComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.SONSTIGERBEREICHESTAATES}
            previousLinkName={routes.VERWALTUNGSUNDSONSTIGE}
            regelungKeysMap={aenderungenSteuereinnahmenRegelungKeysMap}
            regelungKeysList={Object.values(aenderungenSteuereinnahmenRegelungKeysMap)}
            translationKey="aenderungenSteuereinnahmen"
            influenceExpectedKey={InfluenceExpectedEnum.Steuereinnahmen}
            influenceSummaryKey={InfluenceSummaryEnum.Steuereinnahmen}
            influenceVolleJahreswirkungSummaryKey={InfluenceVolleJahreswirkungSummaryEnum.Steuereinnahmen}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.SONSTIGERBEREICHESTAATES}`,
          ]}
        >
          <SonstigerBereicheStaatesComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.KOMPENSATIONKOSTEN}
            previousLinkName={routes.AENDERUNGENSTEUEREINNAHMEN}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.KOMPENSATIONKOSTEN}`]}
        >
          <KompensationKostenComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            previousLinkName={routes.SONSTIGERBEREICHESTAATES}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.ZUSAMMENFASSUNG}`]}
        >
          <ZusammenfassungComponent
            {...contentPageProps}
            moduleName={`${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`}
            nextLinkName={routes.FERTIGSTELLUNG}
            previousLinkName={routes.KOMPENSATIONKOSTEN}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={setAuswirkungenEinnahmenAusgabenCheckData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRoutingCompAEAC}
      pageName={currentPageName}
      customResultPage="fertigstellung"
      generateJson={(data: EgfaModuleVerbraucherEntityDTO) => {
        const auswirkungenEinnahmenAusgabenController = new AuswirkungenEinnahmenAusgabenController();
        auswirkungenEinnahmenAusgabenController.aeaData = data;
        return auswirkungenEinnahmenAusgabenController.createAeaJSON();
      }}
    />
  );
}
