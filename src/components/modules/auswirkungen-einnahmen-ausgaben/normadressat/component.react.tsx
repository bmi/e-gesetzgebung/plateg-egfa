// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import setYear from 'date-fns/setYear';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, GlobalDI, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { AuswirkungenEinnahmenAusgabenController, widgetRegelungKeysNormadressatMap } from '../controller';
import { AnzugebendeJahreComponent } from '../universal-components/anzugebende-jahre/component.react';
import { InfluenceExpectedComponent } from '../universal-components/influence-expected/component.react';
import { TableWidgetComponent } from '../universal-components/table-widget/component.react';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}

export function NormadressatComponent(props: SimpleModuleProps<EgfaModuleEntityRequestDTO>): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [answer, setAnswer] = useState<boolean>();
  const [answerStellenplaene, setAnswerStellenplaene] = useState<boolean>();
  const [isDirty, setIsDirty] = useState(false);
  const [regelungsList, setRegelungsList] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRange, setYearsRange] = useState<number[]>([]);
  const [hasData, setHasData] = useState<boolean>(false);

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleEaOehhEntityDTOAllOf;
      form.setFieldsValue(formDataValue);
      setAnswer(formDataValue.influenceVerwaltungAlsNormadressatExpected as boolean);
      setAnswerStellenplaene(formDataValue.influenceVerwaltungAlsNormadressatStellenplaene as boolean);
      setIsDirty(false);
      if (formDataValue.regelungen) {
        const sortedRegelungs = ctrl.getSortedRegelungs(formDataValue.regelungen);
        setRegelungsList(sortedRegelungs);
        const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysNormadressatMap);
        form.setFieldsValue({
          jahrVon: setYear(new Date(), years[0]),
          jahrBis: setYear(new Date(), years[years.length - 1]),
        });
        setYearsRange(years);
      }
    }
  }, [props.formData]);

  useEffect(() => {
    const data = ctrl.hasDataInRegelungs(ctrl.getRequestedRegelungs(regelungsList, widgetRegelungKeysNormadressatMap));
    setHasData(data);
  }, [regelungsList]);

  useEffect(() => {
    if (yearsRange.length) {
      const updatedData = ctrl.modifyRegelungsToYearsRange(
        regelungsList,
        widgetRegelungKeysNormadressatMap,
        yearsRange,
      );
      updateRegelungsList(updatedData);
    }
  }, [yearsRange]);

  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.normadressat`;

  const updateRegelungsList = (list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    form.setFieldsValue({ regelungen: list });
    setRegelungsList(list);
  };

  const getActualRegelungsList = () => {
    return form.getFieldValue('regelungen') as EgfaModuleEaOehhRegelungEntityDTO[];
  };
  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };
  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={
          <Title level={1}>
            {t(`${translationKey}.title`)}
            <InfoComponent title={t(`${translationKey}.title`)}>
              <div
                dangerouslySetInnerHTML={{
                  __html: t(`${translationKey}.drawerTitleText`),
                }}
              ></div>
            </InfoComponent>
          </Title>
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={handleFormChange}
      >
        <InfluenceExpectedComponent
          name={'influenceVerwaltungAlsNormadressatExpected'}
          translationKey={translationKey}
          onChange={setAnswer}
        />
        {answer && (
          <>
            <FormItemWithInfo
              name={'influenceVerwaltungAlsNormadressatSummary'}
              label={
                <span>
                  {t(`${translationKey}.influenceSummary.label`)}
                  <InfoComponent title={t(`${translationKey}.influenceSummary.label`)} withLabelRequired>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: t(`${translationKey}.influenceSummary.labelDrawer`),
                      }}
                    ></div>
                  </InfoComponent>
                </span>
              }
              rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
            >
              <TextArea rows={8} />
            </FormItemWithInfo>
            <AnzugebendeJahreComponent
              nameStart={'jahrVon'}
              nameEnd={'jahrBis'}
              moduleName={props.moduleName}
              hasData={hasData}
              form={form}
              setYearsRange={setYearsRange}
              yearStart={yearsRange[0]}
              yearEnd={yearsRange[yearsRange.length - 1]}
            />
            {/* Ausgaben Durch section */}
            <Title level={2}>{t(`${translationKey}.ausgabenDurch.title`)}</Title>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.ausgabenDurch.text`),
              }}
            ></p>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.normadressatRegelung.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatRegelung']}
              handeFormChange={handleFormChange}
              hasTitle={true}
              defaultIndex={0}
            />
            <Title level={3}>{t(`${translationKey}.gesamtsumme.title`)}</Title>
            <TableWidgetComponent
              yearsRange={yearsRange}
              regelungsList={regelungsList}
              isEditable={true}
              updateRegelungsList={(list) => updateRegelungsList(list)}
              getActualRegelungsList={() => getActualRegelungsList()}
              hasCurrency={true}
              tableHeaderLabel={t(`${translationKey}.gesamtsumme.headerLabel`)}
              widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatGesamtsumme']}
              handeFormChange={handleFormChange}
              isStaticRows={true}
              noMoreRegelungs={true}
              rowsLabels={[{ title: t(`${translationKey}.gesamtsumme.tableRowTitles.gesamtsumme`), isSumme: true }]}
              defaultIndex={1}
            />
            <InfluenceExpectedComponent
              name={'influenceVerwaltungAlsNormadressatStellenplaene'}
              translationKey={`${translationKey}.planstellen`}
              onChange={setAnswerStellenplaene}
            />
            {answerStellenplaene && (
              <>
                <FormItemWithInfo
                  name={'influenceVerwaltungAlsNormadressatStellenplaeneSummary'}
                  label={
                    <span>
                      {t(`${translationKey}.planstellen.influenceSummary.label`)}
                      <InfoComponent
                        withLabelRequired
                        title={t(`${translationKey}.planstellen.influenceSummary.label`)}
                      >
                        <div
                          dangerouslySetInnerHTML={{
                            __html: t(`${translationKey}.planstellen.influenceSummary.labelDrawer`),
                          }}
                        ></div>
                      </InfoComponent>
                    </span>
                  }
                  rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
                >
                  <TextArea rows={8} />
                </FormItemWithInfo>
                <div style={{ marginBottom: '50px' }}>
                  <Title level={3}>{t(`${translationKey}.planstellen.ausgabenDurch.title`)}</Title>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(`${translationKey}.planstellen.ausgabenDurch.text`),
                    }}
                  ></p>
                </div>
                <TableWidgetComponent
                  yearsRange={yearsRange}
                  hasTitle={true}
                  regelungsList={regelungsList}
                  isEditable={true}
                  updateRegelungsList={(list) => updateRegelungsList(list)}
                  getActualRegelungsList={() => getActualRegelungsList()}
                  hasCurrency={false}
                  tableHeaderLabel={'Label Neu'}
                  widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatStellenplaene']}
                  handeFormChange={handleFormChange}
                  isStaticRows={true}
                  rowsLabels={[
                    { title: t(`${translationKey}.planstellen.tableRowTitles.hoehererDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.gehobenerDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.mittlererDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.einfacherDienst`), isSumme: false },
                  ]}
                  defaultIndex={0}
                />
                <Title level={3}>{t(`${translationKey}.planstellen.gesamtTable.title`)}</Title>
                <TableWidgetComponent
                  yearsRange={yearsRange}
                  regelungsList={regelungsList}
                  isEditable={true}
                  updateRegelungsList={(list) => updateRegelungsList(list)}
                  getActualRegelungsList={() => getActualRegelungsList()}
                  hasCurrency={false}
                  tableHeaderLabel={t(`${translationKey}.planstellen.gesamtTable.headerLabel`)}
                  widgetRegelungKey={widgetRegelungKeysNormadressatMap['NormadressatGesamtStellenplaene']}
                  handeFormChange={handleFormChange}
                  noMoreRegelungs={true}
                  isStaticRows={true}
                  rowsLabels={[
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.hoehererDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.gehobenerDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.mittlererDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.einfacherDienst`),
                      isSumme: false,
                    },
                  ]}
                  defaultIndex={1}
                />
              </>
            )}
          </>
        )}
      </FormWrapper>
    </div>
  );
}
