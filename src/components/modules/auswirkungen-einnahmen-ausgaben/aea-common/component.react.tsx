// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import setYear from 'date-fns/setYear';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';

import {
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEaOehhRegelungEntityDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleStatusType,
} from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, GlobalDI, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import {
  AuswirkungenEinnahmenAusgabenController,
  InfluenceExpectedEnum,
  InfluenceStellenplaeneEnum,
  InfluenceStellenplaeneSummaryEnum,
  InfluenceSummaryEnum,
  InfluenceVolleJahreswirkungSummaryEnum,
  WidgetRegelungKeysMapInterface,
} from '../controller';
import { AnzugebendeJahreComponent } from '../universal-components/anzugebende-jahre/component.react';
import { InfluenceExpectedComponent } from '../universal-components/influence-expected/component.react';
import { JahreswirkungComponent } from '../universal-components/jahreswirkung/component.react';
import { TableWidgetComponent } from '../universal-components/table-widget/component.react';

export interface ContentPageProps<P> {
  egfaId: string;
  setFormInstance: (form: FormInstance | undefined) => void;
  formData: P | undefined;
  onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void;
}

export interface SimpleModuleProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
  regelungKeysMap: WidgetRegelungKeysMapInterface;
  regelungKeysList: string[];
  translationKey: string;
  influenceExpectedKey: InfluenceExpectedEnum;
  influenceSummaryKey: InfluenceSummaryEnum;
  influenceVolleJahreswirkungSummaryKey?: InfluenceVolleJahreswirkungSummaryEnum;
  influenceStellenplaeneKey?: InfluenceStellenplaeneEnum;
  influenceStellenplaeneSummaryKey?: InfluenceStellenplaeneSummaryEnum;
}

export function EinnahmenAusgabenCommonComponent(
  props: SimpleModuleProps<EgfaModuleEntityRequestDTO>,
): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister(
    'auswirkungenEinnahmenAusgabenController',
    () => new AuswirkungenEinnahmenAusgabenController(),
  );
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [answer, setAnswer] = useState<boolean>();
  const [isDirty, setIsDirty] = useState(false);
  const [regelungsList, setRegelungsList] = useState<EgfaModuleEaOehhRegelungEntityDTO[]>([]);
  const [yearsRange, setYearsRange] = useState<number[]>([]);
  const [hasData, setHasData] = useState<boolean>(false);
  const [answerStellenplaene, setAnswerStellenplaene] = useState<boolean>();
  const widgetRegelungKeysMap: WidgetRegelungKeysMapInterface = props.regelungKeysMap;

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleEaOehhEntityDTOAllOf;
      form.setFieldsValue(formDataValue);

      setIsDirty(false);
      if (formDataValue.regelungen) {
        const sortedRegelungs = ctrl.getSortedRegelungs(formDataValue.regelungen);
        setRegelungsList(sortedRegelungs);
        const years = ctrl.prepareYearsRange(sortedRegelungs, widgetRegelungKeysMap);
        form.setFieldsValue({
          jahrVon: setYear(new Date(), years[0]),
          jahrBis: setYear(new Date(), years[years.length - 1]),
        });
        setYearsRange(years);
      }
      if (props.influenceStellenplaeneKey) {
        setAnswerStellenplaene(formDataValue[props.influenceStellenplaeneKey]);
      } else {
        setAnswerStellenplaene(false);
      }
    }
  }, [props.formData, props.influenceStellenplaeneKey]);

  useEffect(() => {
    if (props.formData) {
      const formDataValue = props.formData as EgfaModuleEaOehhEntityDTOAllOf;
      setAnswer(formDataValue[props.influenceExpectedKey] as boolean);
    }
  }, [props.influenceExpectedKey]);

  useEffect(() => {
    const data = ctrl.hasDataInRegelungs(ctrl.getRequestedRegelungs(regelungsList, widgetRegelungKeysMap));
    setHasData(data);
  }, [regelungsList]);

  useEffect(() => {
    if (yearsRange.length) {
      const updatedData = ctrl.modifyRegelungsToYearsRange(regelungsList, widgetRegelungKeysMap, yearsRange);
      updateRegelungsList(updatedData);
    }
  }, [yearsRange]);

  const routeKey = `AUSWIRKUNGEN_${props.moduleName.toUpperCase()}`;
  const moduleNameRoute = routes[routeKey];
  const translationKey = `egfa.modules.${props.moduleName}.${props.translationKey}`;

  const updateRegelungsList = (list: EgfaModuleEaOehhRegelungEntityDTO[]) => {
    form.setFieldsValue({ regelungen: list });
    setRegelungsList(list);
  };

  const getActualRegelungsList = () => {
    return form.getFieldValue('regelungen') as EgfaModuleEaOehhRegelungEntityDTO[];
  };
  const handleFormChange = () => {
    if (props.handleFormChange) {
      props.handleFormChange();
    }
    setIsDirty(true);
  };

  const tableProps = {
    yearsRange: yearsRange,
    regelungsList: regelungsList,
    isEditable: true,
    updateRegelungsList: (list: EgfaModuleEaOehhRegelungEntityDTO[]) => updateRegelungsList(list),
    getActualRegelungsList: () => getActualRegelungsList(),
    hasCurrency: true,
    handeFormChange: handleFormChange,
    tableHeaderLabel: '',
    hasTitle: true,
  };

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={
          <Title level={1}>
            {t(`${translationKey}.title`)}
            <InfoComponent title={t(`${translationKey}.title`)}>
              <div
                dangerouslySetInnerHTML={{
                  __html: t(`${translationKey}.drawerTitleText`),
                }}
              ></div>
            </InfoComponent>
          </Title>
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={handleFormChange}
      >
        <InfluenceExpectedComponent
          name={props.influenceExpectedKey}
          translationKey={translationKey}
          onChange={setAnswer}
        />
        {answer && (
          <>
            <FormItemWithInfo
              name={props.influenceSummaryKey}
              label={
                <span>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: t(`${translationKey}.influenceSummary.label`, { newLine: `<br/>` }),
                    }}
                  />
                  <InfoComponent
                    title={t(`${translationKey}.influenceSummary.label`, { newLine: `` })}
                    withLabelRequired
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html: t(`${translationKey}.influenceSummary.labelDrawer`),
                      }}
                    ></div>
                  </InfoComponent>
                </span>
              }
              rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
            >
              <TextArea rows={8} />
            </FormItemWithInfo>

            <AnzugebendeJahreComponent
              nameStart={'jahrVon'}
              nameEnd={'jahrBis'}
              moduleName={props.moduleName}
              hasData={hasData}
              form={form}
              setYearsRange={setYearsRange}
              yearStart={yearsRange[0]}
              yearEnd={yearsRange[yearsRange.length - 1]}
            />
            {/* Ausgaben Durch section */}
            <Title level={2}>{t(`${translationKey}.ausgabenDurch.title`)}</Title>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`${translationKey}.ausgabenDurch.text`),
              }}
            ></p>
            {props.influenceVolleJahreswirkungSummaryKey && (
              <JahreswirkungComponent
                formElementName={props.influenceVolleJahreswirkungSummaryKey as string}
                moduleName={props.moduleName}
                translationKey={translationKey}
                form={form}
                setIsDirty={handleFormChange}
              />
            )}

            <Title level={3}>{t(`${translationKey}.bund.title`)}</Title>
            <TableWidgetComponent
              {...tableProps}
              tableHeaderLabel={t(`${translationKey}.bund.headerLabel`)}
              widgetRegelungKey={props.regelungKeysList[0]}
              defaultIndex={0}
            />
            <Title level={3}>{t(`${translationKey}.laender.title`)}</Title>
            <TableWidgetComponent
              {...tableProps}
              hasOnlySumme={true}
              widgetRegelungKey={props.regelungKeysList[1]}
              defaultIndex={1}
            />

            <Title level={3}>{t(`${translationKey}.gemeinden.title`)}</Title>
            <TableWidgetComponent
              {...tableProps}
              hasOnlySumme={true}
              widgetRegelungKey={props.regelungKeysList[2]}
              defaultIndex={2}
            />
            <Title level={3}>{t(`${translationKey}.gesamtsumme.title`)}</Title>
            <TableWidgetComponent
              {...tableProps}
              tableHeaderLabel={t(`${translationKey}.gesamtsumme.headerLabel`)}
              widgetRegelungKey={props.regelungKeysList[3]}
              isStaticRows={true}
              noMoreRegelungs={true}
              rowsLabels={[
                { title: t(`${translationKey}.gesamtsumme.tableRowTitles.bund`), isSumme: false },
                { title: t(`${translationKey}.gesamtsumme.tableRowTitles.laender`), isSumme: false },
                { title: t(`${translationKey}.gesamtsumme.tableRowTitles.gemeinden`), isSumme: false },
                { title: t(`${translationKey}.gesamtsumme.tableRowTitles.gesamtsumme`), isSumme: true },
              ]}
              hasTitle={false}
              defaultIndex={3}
            />

            {props.influenceStellenplaeneKey && (
              <InfluenceExpectedComponent
                name={props.influenceStellenplaeneKey}
                translationKey={`${translationKey}.planstellen`}
                onChange={setAnswerStellenplaene}
              />
            )}

            {answerStellenplaene && pageName !== routes.ZWECKAUSGABEN && (
              <>
                <FormItemWithInfo
                  name={props.influenceStellenplaeneSummaryKey}
                  label={
                    <span>
                      {t(`${translationKey}.planstellen.influenceSummary.label`)}
                      <InfoComponent
                        withLabel
                        withLabelRequired
                        title={t(`${translationKey}.planstellen.influenceSummary.label`)}
                      >
                        <div
                          dangerouslySetInnerHTML={{
                            __html: t(`${translationKey}.planstellen.influenceSummary.labelDrawer`),
                          }}
                        ></div>
                      </InfoComponent>
                    </span>
                  }
                  rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
                >
                  <TextArea rows={8} />
                </FormItemWithInfo>
                <div style={{ marginBottom: '50px' }}>
                  <Title level={3}>{t(`${translationKey}.planstellen.ausgabenDurch.title`)}</Title>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(`${translationKey}.planstellen.ausgabenDurch.text`),
                    }}
                  ></p>
                </div>
                <TableWidgetComponent
                  {...tableProps}
                  hasCurrency={false}
                  widgetRegelungKey={props.regelungKeysList[4]}
                  isStaticRows={true}
                  rowsLabels={[
                    { title: t(`${translationKey}.planstellen.tableRowTitles.hoehererDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.gehobenerDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.mittlererDienst`), isSumme: false },
                    { title: t(`${translationKey}.planstellen.tableRowTitles.einfacherDienst`), isSumme: false },
                  ]}
                  defaultIndex={0}
                />
                <Title level={3}>{t(`${translationKey}.planstellen.gesamtTable.title`)}</Title>
                <TableWidgetComponent
                  {...tableProps}
                  hasCurrency={false}
                  tableHeaderLabel={t(`${translationKey}.planstellen.gesamtTable.headerLabel`)}
                  widgetRegelungKey={props.regelungKeysList[5]}
                  noMoreRegelungs={true}
                  isStaticRows={true}
                  rowsLabels={[
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.hoehererDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.gehobenerDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.mittlererDienst`),
                      isSumme: false,
                    },
                    {
                      title: t(`${translationKey}.planstellen.gesamtTable.tableRowTitles.einfacherDienst`),
                      isSumme: false,
                    },
                  ]}
                  hasTitle={false}
                  defaultIndex={1}
                />
              </>
            )}
          </>
        )}
      </FormWrapper>
    </div>
  );
}
