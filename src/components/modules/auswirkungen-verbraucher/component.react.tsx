// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherControllerApi,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import { VerbraucherEnapReferenzenTypes } from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleAuswirkungen } from '../general/simple-module/auswirkungen/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateVerbraucherJSON } from './controller';

export interface ModuleProps {
  moduleProps: {
    egfaId: string;
    moduleName: string;
  };
  setEgfaName: (name: string) => void;
}

export function AuswirkungenVerbraucher(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [verbraucherData, setVerbraucherData] = useState<EgfaModuleVerbraucherEntityDTO & ExtendedModuleData>();

  const egfaVerbraucherController = GlobalDI.get<EgfaModuleVerbraucherControllerApi>(
    'egfaModuleVerbraucherControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaVerbraucherController.getEgfaModule1({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleVerbraucherEntityDTO) =>
    egfaVerbraucherController.modifyEgfaModule1({
      egfaId: egfaId,
      egfaModuleVerbraucherEntityDTO: localData,
    });

  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (verbraucherData && verbraucherData.referenzenData) {
      setReferenzenData(verbraucherData.referenzenData);
    }
  }, [verbraucherData]);

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ) => {
    const contentPageProps = { egfaId, setFormInstance, formData: verbraucherData, onSave, handleFormChange };
    const PDFExportBtn = ctrl.getPDFExportAction(egfaId, moduleName, verbraucherData?.status as EgfaModuleStatusType);
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName="verbraucher"
            additionalActions={PDFExportBtn}
          />
        </Route>
        {(verbraucherData?.aktionen?.includes(AktionType.Schreiben) === false ||
          verbraucherData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.AUSWIRKUNGEN_VERBRAUCHER}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.WESENTLICHE_AUSWIRKUNGEN}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.WESENTLICHE_AUSWIRKUNGEN}`]}
        >
          <SimpleModuleAuswirkungen
            {...contentPageProps}
            moduleName="verbraucher"
            referenzen={
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapReferenzen,
                    conditionsList: [VerbraucherEnapReferenzenTypes.hasSelectedCheckboxSdg121],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.ZUSAMMENFASSUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.FERTIGSTELLUNG}`}
            />
          )}
        />
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_VERBRAUCHER}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={setVerbraucherData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      generateJson={generateVerbraucherJSON}
    />
  );
}
