// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
  EgfaModuleVerbraucherControllerApi,
  EgfaModuleWeitereEntityDTOAllOf,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportVerbraucherToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleVerbraucherControllerApi>('egfaModuleVerbraucherControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule1({ egfaId }).subscribe({
    next: (verbraucherData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Verbraucher,
          egfaDatenuebernahmeDTO: generateVerbraucherJSON(verbraucherData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Verbraucher data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Verbraucher data', error);
    },
  });
};
export const generateVerbraucherJSON = (
  verbraucherData: EgfaModuleWeitereEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const verbraucherJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'verbraucherErgebnisdokumentation',
    title: i18n.t(`egfa.modules.verbraucher.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  if (verbraucherData.influenceExpected && verbraucherData.influenceSummary) {
    verbraucherJSONExport.content.push({
      id: 'verbraucherSummary',
      type: EgfaItemType.Text,
      data: verbraucherData.influenceSummary,
    });
  } else {
    verbraucherJSONExport.content.push({
      id: 'verbraucherSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.verbraucher.fertigstellung.noImpact`).toString(),
    });
  }
  return verbraucherJSONExport;
};
