// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  BASE_PATH,
  EgfaModuleEnapControllerApi,
  EgfaModuleEnapEntityDTO,
  EgfaModuleEnapEntityResponseDTO,
  EgfaModuleStatusType,
  EgfaModuleType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import {
  DemografieReferenzenTypes,
  DisabilityReferenzenTypes,
  GleichstellungReferenzenTypes,
  GleichwertigkeitReferenzenTypes,
} from '../general/referenzen-generic/referenzen-item/component.react';
import { ReferenzenStaticComponent } from '../general/referenzen-generic/referenzen-static/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateEnapJSON } from './controller';
import { EinleitungEnap } from './einleitung/component.react';
import { PrinzipienComponent } from './prinzipien/component.react';
import { SDG10UngleichheitenComponent } from './SDG10Ungleichheiten/component.react';
import { SDG11NachhaltigeStaedteGemeindenComponent } from './SDG11NachhaltigeStaedteGemeinden/component.react';
import { SDG12NachhaltigKonsumProduktionComponent } from './SDG12NachhaltigKonsumProduktion/component.react';
import { SDG13KlimaschutzComponent } from './SDG13Klimaschutz/component.react';
import { SDG14LebenUnterWasserComponent } from './SDG14LebenUnterWasser/component.react';
import { SDG15LebenAnLandComponent } from './SDG15LebenAnLand/component.react';
import { SDG16FriedenGerechtigkeitComponent } from './SDG16FriedenGerechtigkeit/component.react';
import { SDG17PartnerschaftenZurErreichungDerZieleComponent } from './SDG17PartnerschaftenZurErreichungDerZiele/component.react';
import { SDG1KeineArmutComponent } from './SDG1KeineArmut/component.react';
import { SDG2KeinHungerComponent } from './SDG2KeinHunger/component.react';
import { SDG3GesundheitComponent } from './SDG3Gesundheit/component.react';
import { SDG4HochwertigeBildungComponent } from './SDG4HochwertigeBildung/component.react';
import { SDG5GeschlechtergleichstellungComponent } from './SDG5Geschlechtergleichstellung/component.react';
import { SDG6SauberesWasserComponent } from './SDG6SauberesWasser/component.react';
import { SDG7EnergieComponent } from './SDG7Energie/component.react';
import { SDG8ArbeitWirtschaftComponent } from './SDG8ArbeitWirtschaft/component.react';
import { SDG9IndustrieComponent } from './SDG9Industrie/component.react';
import { EnapZusammenfassung } from './zusammenfassung/component.react';

export function Enap(props: ModuleProps): React.ReactElement {
  const { t } = useTranslation();
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [enapData, setEnapData] = useState<EgfaModuleEnapEntityDTO & ExtendedModuleData>();
  const egfaModuleEnapController = GlobalDI.get<EgfaModuleEnapControllerApi>('egfaModuleEnapControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const BIB_LINK = `${BASE_PATH}/arbeitshilfen/download`;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaModuleEnapController.getEgfaModule10({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleEnapEntityDTO) => {
    return egfaModuleEnapController.modifyEgfaModule10({
      egfaId: egfaId,
      egfaModuleEnapEntityDTO: { ...localData, typ: EgfaModuleType.Enap },
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.AUSWIRKUNGEN_WEITERE);
  }, []);

  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (enapData && enapData.referenzenData) {
      setReferenzenData(enapData.referenzenData);
    }
  }, [enapData]);

  const PageRoutingEnap = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ): React.ReactElement => {
    useEffect(() => {
      if (updateItemDisableStatus) {
        const fertigstellungIsDisabled =
          ((!enapData?.influenceSummary || enapData?.influenceSummary === '') &&
            enapData?.aktionen?.includes(AktionType.Schreiben)) ||
          false;
        updateItemDisableStatus([routes.FERTIGSTELLUNG], fertigstellungIsDisabled);
      }
    }, [enapData]);
    const contentPageProps = { egfaId, setFormInstance, formData: enapData, onSave, handleFormChange };
    const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
    const PDFExportBtn = ctrl.getPDFExportAction(enapData?.egfaId as string, moduleName, enapData?.status);
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung {...contentPageProps} moduleName="enap" additionalActions={PDFExportBtn} />
        </Route>
        {(enapData?.aktionen?.includes(AktionType.Schreiben) === false ||
          enapData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.ENAP}/${routes.FERTIGSTELLUNG}`} />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.EINLEITUNG}`]}>
          <EinleitungEnap />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG1}`]}>
          <SDG1KeineArmutComponent
            {...contentPageProps}
            sectionName={routes.SDG1}
            nextLinkName={routes.SDG2}
            previousLinkName={routes.EINLEITUNG}
            referenzenTop={
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [GleichstellungReferenzenTypes.hasSelectedGeld],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.enapDisabilityReferenzen,
                    conditionsList: [DisabilityReferenzenTypes.hasSelectedCheckboxTeilhabe],
                    moduleName: routes.DISABILITY,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG2}`]}>
          <SDG2KeinHungerComponent
            {...contentPageProps}
            sectionName={routes.SDG2}
            nextLinkName={routes.SDG3}
            previousLinkName={routes.SDG1}
            referenzenList={[
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=36`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG2'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG3}`]}>
          <SDG3GesundheitComponent
            {...contentPageProps}
            sectionName={routes.SDG3}
            nextLinkName={routes.SDG4}
            previousLinkName={routes.SDG2}
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [GleichstellungReferenzenTypes.hasSelectedGesundheit],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=31`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG3'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG4}`]}>
          <SDG4HochwertigeBildungComponent
            {...contentPageProps}
            sectionName={routes.SDG4}
            nextLinkName={routes.SDG5}
            previousLinkName={routes.SDG3}
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [
                      DemografieReferenzenTypes.hasSelectedFamilie,
                      DemografieReferenzenTypes.hasSelectedArbeit,
                    ],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsorge],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedFamilie],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedArbeit,
                      GleichstellungReferenzenTypes.hasSelectedGeld,
                      GleichstellungReferenzenTypes.hasSelectedZeit,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG5}`]}>
          <SDG5GeschlechtergleichstellungComponent
            {...contentPageProps}
            sectionName={routes.SDG5}
            nextLinkName={routes.SDG6}
            previousLinkName={routes.SDG4}
            referenzenTop={
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedExplicitGoal,
                      GleichstellungReferenzenTypes.hasSelectedMenAndWomanInfluenced,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG6}`]}>
          <SDG6SauberesWasserComponent
            {...contentPageProps}
            sectionName={routes.SDG6}
            nextLinkName={routes.SDG7}
            previousLinkName={routes.SDG5}
            referenzenList={[
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=36`}
                link2={`${BIB_LINK}/45#page=40`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG6'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG7}`]}>
          <SDG7EnergieComponent
            {...contentPageProps}
            sectionName={routes.SDG7}
            nextLinkName={routes.SDG8}
            previousLinkName={routes.SDG6}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG8}`]}>
          <SDG8ArbeitWirtschaftComponent
            {...contentPageProps}
            sectionName={routes.SDG8}
            nextLinkName={routes.SDG9}
            previousLinkName={routes.SDG7}
            referenzenList={[
              null,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapEaOehhReferenzen,
                    moduleName: routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN,
                  },
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedZusammenhalt],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedWirtschaft],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
              null,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedArbeit],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG9}`]}>
          <SDG9IndustrieComponent
            {...contentPageProps}
            sectionName={routes.SDG9}
            nextLinkName={routes.SDG10}
            previousLinkName={routes.SDG8}
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapEaOehhReferenzen,
                    moduleName: routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN,
                  },
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedWirtschaft,
                      GleichwertigkeitReferenzenTypes.hasSelectedMobilitaet,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG10}`]}>
          <SDG10UngleichheitenComponent
            {...contentPageProps}
            sectionName={routes.SDG10}
            nextLinkName={routes.SDG11}
            previousLinkName={routes.SDG9}
            referenzenTop={
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                  },
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [GleichstellungReferenzenTypes.hasSelectedGeld],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.enapDisabilityReferenzen,
                    conditionsList: [DisabilityReferenzenTypes.hasSelectedCheckboxTeilhabe],
                    moduleName: routes.DISABILITY,
                    extendedInfo: true,
                  },
                ]}
              />
            }
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedArbeit],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG11}`]}>
          <SDG11NachhaltigeStaedteGemeindenComponent
            {...contentPageProps}
            sectionName={routes.SDG11}
            nextLinkName={routes.SDG12}
            previousLinkName={routes.SDG10}
            referenzenTop={
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedExplicitGoal,
                      GleichstellungReferenzenTypes.hasSelectedMenAndWomanInfluenced,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                  {
                    referenzenData: referenzenData?.enapDisabilityReferenzen,
                    conditionsList: [
                      DisabilityReferenzenTypes.hasSelectedCheckboxTeilhabe,
                      DisabilityReferenzenTypes.hasSelectedCheckboxZugang,
                    ],
                    moduleName: routes.DISABILITY,
                    extendedInfo: true,
                  },
                ]}
              />
            }
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedWohnraum,
                      GleichwertigkeitReferenzenTypes.hasSelectedLebensgrundlage,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedMobilitaet],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                    conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedWohnraum],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG12}`]}>
          <SDG12NachhaltigKonsumProduktionComponent
            {...contentPageProps}
            sectionName={routes.SDG12}
            nextLinkName={routes.SDG13}
            previousLinkName={routes.SDG11}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG13}`]}>
          <SDG13KlimaschutzComponent
            {...contentPageProps}
            sectionName={routes.SDG13}
            nextLinkName={routes.SDG14}
            previousLinkName={routes.SDG12}
            referenzenList={[
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=33`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG13'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG14}`]}>
          <SDG14LebenUnterWasserComponent
            {...contentPageProps}
            sectionName={routes.SDG14}
            nextLinkName={routes.SDG15}
            previousLinkName={routes.SDG13}
            referenzenList={[
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=36`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG2'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG15}`]}>
          <SDG15LebenAnLandComponent
            {...contentPageProps}
            sectionName={routes.SDG15}
            nextLinkName={routes.SDG16}
            previousLinkName={routes.SDG14}
            referenzenTop={
              <>
                <ReferenzenGenericComponent
                  egfaLink={EGFA_LINK}
                  egfaErsteller={egfaErsteller}
                  referenzenDataConfig={[
                    {
                      referenzenData: referenzenData?.enapGleichwertigkeitReferenzen,
                      conditionsList: [GleichwertigkeitReferenzenTypes.hasSelectedLebensgrundlage],
                      moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                      extendedInfo: true,
                    },
                  ]}
                />
                <ReferenzenStaticComponent
                  link={`${BIB_LINK}/45`}
                  moduleName={routes.ENAP}
                  translationKey={'staticTextSDG15'}
                />
              </>
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG16}`]}>
          <SDG16FriedenGerechtigkeitComponent
            {...contentPageProps}
            sectionName={routes.SDG16}
            nextLinkName={routes.SDG17}
            previousLinkName={routes.SDG15}
            referenzenList={[
              <ReferenzenGenericComponent
                egfaLink={EGFA_LINK}
                egfaErsteller={egfaErsteller}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.enapGleichstellungReferenzen,
                    conditionsList: [GleichstellungReferenzenTypes.hasSelectedGewalt],
                    moduleName: routes.GLEICHSTELLUNG,
                    extendedInfo: true,
                  },
                ]}
              />,
              <ReferenzenStaticComponent
                link={`${BIB_LINK}/45#page=31`}
                moduleName={routes.ENAP}
                translationKey={'staticTextSDG3'}
              />,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG17}`]}>
          <SDG17PartnerschaftenZurErreichungDerZieleComponent
            {...contentPageProps}
            sectionName={routes.SDG17}
            nextLinkName={routes.prinzipien}
            previousLinkName={routes.SDG16}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.prinzipien}`]}>
          <PrinzipienComponent
            {...contentPageProps}
            sectionName={routes.prinzipien}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            previousLinkName={routes.SDG17}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.ZUSAMMENFASSUNG}`]}>
          <EnapZusammenfassung
            {...contentPageProps}
            sectionName={routes.ZUSAMMENFASSUNG}
            nextLinkName={routes.FERTIGSTELLUNG}
            previousLinkName={routes.prinzipien}
            updateNextLink={updateItemDisableStatus}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung {...contentPageProps} moduleName="enap" additionalActions={PDFExportBtn} />
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.ENAP}/${routes.EINLEITUNG}`} />
          )}
        />
      </Switch>
    );
  };

  const contactDrawer = (
    <InfoComponent
      isContactPerson={false}
      title={t('egfa.modules.enap.drawerTitle')}
      buttonText={t('egfa.modules.general.contactPerson')}
      key="egfa-header-contact-person"
      titleWithoutPrefix={true}
    >
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.enap.contactPerson.content`),
        }}
      />
    </InfoComponent>
  );

  return (
    <ModuleWrapper<EgfaModuleEnapEntityDTO, EgfaModuleEnapEntityResponseDTO>
      setFormData={setEnapData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRoutingEnap}
      pageName={currentPageName}
      customContactDrawer={contactDrawer}
      customResultPage="fertigstellung"
      generateJson={generateEnapJSON}
    />
  );
}
