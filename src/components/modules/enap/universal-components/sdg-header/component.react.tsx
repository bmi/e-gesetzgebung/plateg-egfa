// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './sdg-header.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule, InfoComponent } from '@plateg/theme';

interface SDGHeaderProps {
  sectionName: string;
  fileName?: string;
  infoIsHidden?: boolean;
}

export function SDGHeader(props: SDGHeaderProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  return (
    <div className="sdg-heading">
      <div className="sdg-heading-holder">
        {props.fileName && (
          <Image
            loading="lazy"
            preview={false}
            src={(require(`../../../../../media/enap/${props.fileName}`) as ImageModule).default}
            alt={`Logo ${props.sectionName}`}
          />
        )}
        <div className="sdg-heading-text">
          <Title level={1}>
            {t(`egfa.modules.enap.${props.sectionName}.title`)}{' '}
            {props.infoIsHidden !== true && (
              <InfoComponent title={t(`egfa.modules.enap.${props.sectionName}.titleDrawer`)}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: t(`egfa.modules.enap.${props.sectionName}.titleDrawerText`),
                  }}
                ></div>
              </InfoComponent>
            )}
          </Title>
          <p>{t(`egfa.modules.enap.${props.sectionName}.subtitle`)}</p>
        </div>
      </div>
    </div>
  );
}
