// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  BegruendungFormElementInterface,
  getConditionalItem,
  SdgIndikatorMessageItemInterface,
} from '../../controller';
import {
  ConditionalSection,
  getAnderweitigRelevantProps,
  getRelevantProps,
} from '../conditional-section/component.react';
import { RelevantTitle } from '../relevant-title/component.react';
import { WeitereAuswirkungen } from '../weitere-auswirkungen/component.react';

interface StepHolderComponentProps {
  indikatorenStructure: SdgIndikatorMessageItemInterface[];
  sectionName: string;
  indikatorsVisibility: boolean[];
  anderweitigRelevantRv?: boolean;
  dontShowAnderweitigRelevantRv?: boolean;
  referenzenList?: (React.ReactElement | null)[];
  referenzenTop?: React.ReactElement;
}

export function StepHolderComponent(props: StepHolderComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      {props.referenzenTop && <div className="referenzenTop-holder">{props.referenzenTop}</div>}
      {props.indikatorenStructure.map((indikator, indikatorIndex) => {
        const begruendungen = indikator.begruendungen.map((begruendung, begruendungIndex) => {
          return getConditionalItem(begruendung, props.sectionName, indikatorIndex, begruendungIndex);
        });
        if (indikator.anderweitigRelevantLabel) {
          const anderweitigRelevant = getAnderweitigRelevantProps(indikator.anderweitigRelevantLabel, [
            props.sectionName,
            'indikatoren',
            indikatorIndex,
            'anderweitigRelevant',
          ]) as BegruendungFormElementInterface;

          begruendungen.push(anderweitigRelevant);
        }

        return (
          <div key={indikatorIndex}>
            <RelevantTitle indikator={indikator} />
            <ConditionalSection
              isVisible={props.indikatorsVisibility[indikatorIndex]}
              subtitle={t(`egfa.modules.enap.relevant.relevantAreaTitle`)}
              condition={getRelevantProps(
                [props.sectionName, 'indikatoren', indikatorIndex, 'relevant'],
                props.sectionName === 'prinzipien' ? 'prinzipien' : undefined,
              )}
              conditionalItems={begruendungen}
              sdgStep={props.sectionName}
            />
            {props.referenzenList && props.referenzenList[indikatorIndex]}
            <div className="sdg-seperator"></div>
          </div>
        );
      })}
      {!props.dontShowAnderweitigRelevantRv && (
        <WeitereAuswirkungen
          isVisible={props.anderweitigRelevantRv}
          title={t(`egfa.modules.enap.${props.sectionName}.title`)}
          sdgStep={props.sectionName}
        />
      )}
    </>
  );
}
