// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';

import { InfoComponent } from '@plateg/theme';

import { SdgIndikatorMessageItemInterface } from '../../controller';

interface RelevantTitleProps {
  indikator: SdgIndikatorMessageItemInterface;
}
export function RelevantTitle(props: RelevantTitleProps): React.ReactElement {
  const { Title } = Typography;

  return (
    <>
      <Title level={2}>
        {props.indikator.relevantTitle}{' '}
        <InfoComponent title={props.indikator.relevantTitle}>
          <div
            dangerouslySetInnerHTML={{
              __html: props.indikator.relevantTitleDrawerText,
            }}
          ></div>
        </InfoComponent>
      </Title>
    </>
  );
}
