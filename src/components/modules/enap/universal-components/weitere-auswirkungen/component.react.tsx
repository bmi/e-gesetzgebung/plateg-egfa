// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

import { ConditionalSection } from '../conditional-section/component.react';

interface WeitereAuswirkungenProps {
  isVisible?: boolean;
  title: string;
  sdgStep: string;
}

export function WeitereAuswirkungen(props: WeitereAuswirkungenProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  return (
    <div className="sdg-weitere-content">
      <Title level={2}>
        {t(`egfa.modules.enap.weitereInformationen.anderweitigRelevantRvTitle`, { title: props.title })}
        <InfoComponent
          title={t(`egfa.modules.enap.weitereInformationen.anderweitigRelevantRvTitle`, { title: props.title })}
        >
          <div
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.weitereInformationen.anderweitigRelevantRvTitleDrawerText`),
            }}
          ></div>
        </InfoComponent>
      </Title>
      <ConditionalSection
        isVisible={props.isVisible}
        condition={{
          name: [props.sdgStep, 'anderweitigRelevantRv'],
          label: t(`egfa.modules.enap.weitereInformationen.anderweitigRelevantRvLabel`),
          trueOptionLabel: 'Ja',
          falseOptionLabel: 'Nein',
        }}
        conditionalItems={[
          {
            label: t(`egfa.modules.enap.weitereInformationen.anderweitigRelevantRvSummaryLabel`, {
              title: props.title,
            }),
            name: [props.sdgStep, 'anderweitigRelevantRvSummary'],
          },
        ]}
        sdgStep={props.sdgStep}
      />
    </div>
  );
}
