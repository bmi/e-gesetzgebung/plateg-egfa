// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { useEffect, useState } from 'react';

import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';
import { SDGHeader } from '../sdg-header/component.react';

export interface SDGComponentProps<P> extends ContentPageProps<P> {
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
  isLight?: boolean;
  referenzenList?: (React.ReactElement | null)[];
  referenzenTop?: React.ReactElement;
}

interface ENAPFormWrapperProps<P> extends SDGComponentProps<P> {
  imageFileName?: string;
  children: React.ReactNode;
  sdgStep: string;
  initialSdgStructure: FormDataInterface;
  infoIsHidden?: boolean;
}

interface FormDataInterface {
  [s: string]: any;
}
export function ENAPFormWrapper(props: ENAPFormWrapperProps<any>): React.ReactElement {
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const data: FormDataInterface = props.formData;
      if (data[props.sdgStep] === null) {
        data[props.sdgStep] = props.initialSdgStructure;
      }
      form.setFieldsValue(data);
      setIsDirty(false);
    }
  }, [props.formData]);

  return (
    <FormWrapper
      projectName={props.isLight ? 'eNAP' : 'eGFA'}
      title={
        <SDGHeader sectionName={props.sectionName} fileName={props.imageFileName} infoIsHidden={props.infoIsHidden} />
      }
      previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/enap/${props.previousLinkName as string}`}
      nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/enap/${props.nextLinkName as string}`}
      saveDraft={props.onSave(form)}
      isDirty={() => isDirty}
      form={form}
      formInitialValue={{}}
      setFormInstance={props.setFormInstance}
      handleFormChanges={() => {
        setIsDirty(true);
        props.handleFormChange?.();
      }}
    >
      {props.children}
    </FormWrapper>
  );
}
