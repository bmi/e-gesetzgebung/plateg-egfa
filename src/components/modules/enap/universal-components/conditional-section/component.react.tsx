// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './conditional-selection.less';

import { Form, Input, Radio, Typography } from 'antd';
import i18next, { t } from 'i18next';
import React, { useEffect, useState } from 'react';

import { InfoComponent } from '@plateg/theme';

interface ConditionalSectionProps {
  condition: ConditionalSelectCondition;
  conditionalItems: ConditionalSectionItems[];
  isVisible?: boolean;
  subtitle?: string;
  sdgStep: string;
}

interface ConditionalSelectCondition {
  name: (string | number)[];
  label: string;
  trueOptionLabel: string;
  falseOptionLabel: string;
}

interface ConditionalSectionItems {
  title?: string;
  drawerText?: string;
  drawerLink?: string;
  subtitle?: string;
  name: (string | number)[];
  label: string;
  showInfo: boolean;
  begruendungDrawerContent?: string;
}

export const getAnderweitigRelevantProps = (bereich: string, name: (string | number)[]): ConditionalSectionItems => {
  return {
    title: i18next.t(`egfa.modules.enap.anderweitigRelevant.title`, { bereich: bereich }),
    subtitle: i18next.t(`egfa.modules.enap.anderweitigRelevant.text`),
    label: i18next.t(`egfa.modules.enap.anderweitigRelevant.label`),
    showInfo: false,
    name: name,
  };
};

export const getRelevantProps = (name: (string | number)[], customLabel?: string): ConditionalSelectCondition => {
  return {
    name: name,
    label: i18next.t(`egfa.modules.enap.relevant.${customLabel ? customLabel : 'label'}`),
    trueOptionLabel: 'Ja',
    falseOptionLabel: 'Nein',
  };
};

export function ConditionalSection(props: ConditionalSectionProps): React.ReactElement {
  const { TextArea } = Input;
  const { Title } = Typography;

  const [condionalSectionIsVisible, setCondionalSectionIsVisible] = useState<boolean>(props.isVisible || false);
  const elementNameAsString = props.condition.name.join('_').toString();

  useEffect(() => {
    setCondionalSectionIsVisible(props.isVisible || false);
  }, [props.isVisible]);

  const conditionalItems = props.conditionalItems.map((item, index) => {
    const isPrinzipien = item.name.indexOf('prinzipien') !== -1;

    return (
      <div key={index}>
        <div className={'sdg-additional-info'}>
          {item.title && (
            <Title level={3}>
              {item.title}{' '}
              {item.showInfo && !isPrinzipien && (
                <InfoComponent title={item.title}>
                  <p>{i18next.t(`egfa.modules.enap.sdgGeneral.drawerTextIndikatoren`)}</p>
                  <a target="_blank" href={item.drawerLink}>
                    {item.drawerLink}
                  </a>
                </InfoComponent>
              )}
            </Title>
          )}
          {item.subtitle && (
            <p
              dangerouslySetInnerHTML={{
                __html: item.subtitle,
              }}
            ></p>
          )}
        </div>
        {condionalSectionIsVisible && (
          <Form.Item
            name={item.name}
            label={
              <span>
                {item.label}
                {item.begruendungDrawerContent && (
                  <InfoComponent title={item.label}>
                    <p>{i18next.t(`egfa.modules.enap.sdgGeneral.begruendungDrawerIntro`)}</p>
                    <p>{item.begruendungDrawerContent}</p>
                  </InfoComponent>
                )}
              </span>
            }
          >
            <TextArea rows={8} />
          </Form.Item>
        )}
      </div>
    );
  });

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend>{props.condition.label}</legend>
        <Form.Item name={props.condition.name}>
          <Radio.Group
            className="horizontal-radios"
            onChange={(event) => setCondionalSectionIsVisible(event?.target?.value as boolean)}
            name={elementNameAsString}
          >
            <Radio
              id={`egfa-relevanzpruefung-${elementNameAsString}-ja-radio`}
              className="horizontal-radios"
              value={true}
            >
              {props.condition.trueOptionLabel}
            </Radio>
            <Radio
              id={`egfa-relevanzpruefung-${elementNameAsString}-nein-radio`}
              className="horizontal-radios"
              value={false}
            >
              {props.condition.falseOptionLabel}
            </Radio>
          </Radio.Group>
        </Form.Item>
      </fieldset>
      <div hidden={!props.subtitle?.length || props.sdgStep === 'prinzipien'} className={'sdg-additional-info'}>
        <Title level={3}>{props.subtitle} </Title>
      </div>
      {conditionalItems}
    </>
  );
}
