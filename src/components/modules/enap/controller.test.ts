// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { Observable, of, throwError } from 'rxjs';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaModuleEnapControllerApi,
  EgfaModuleEnapEntityDTO,
  EgfaModuleEnapEntityResponseDTO,
  EgfaModuleStatusType,
  EgfaModuleType,
  ExportControllerApi,
  OperationOpts,
  SendEgfaDatenuebernahmeRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { loadingStatusSetStub } from '../../../general.test';
import {
  createInitialSdgStructure,
  exportEnap,
  exportEnapToEditor,
  getConditionalItem,
  prepareBegruendungenKeys,
} from './controller';

const indikatorenStructure = [
  {
    relevantTitle: 'title',
    relevantTitleDrawerText: 'text',
    begruendungen: [
      {
        title: 'subtitle 0',
        drawerLink: 'link 0',
        text: 'text 0',
        summaryLabel: 'summary 0',
        begruendungDrawerContent: 'content 0',
      },
      {
        title: 'subtitle 1',
        drawerLink: 'link 1',
        text: 'text 1',
        summaryLabel: 'summary 1',
        begruendungDrawerContent: 'content 1',
      },
    ],
    anderweitigRelevantLabel: 'Anderweitig',
  },
];
const initSdgStructure = {
  indikatoren: [
    {
      indikatorKey: 'sdg1_indikatoren_0_indikatorKey',
      begruendungen: [
        { begruendungKey: 'sdg1_indikatoren_0_begruendungen_0_begruendungKey' },
        { begruendungKey: 'sdg1_indikatoren_0_begruendungen_1_begruendungKey' },
      ],
    },
  ],
};
describe(`Test: createInitialSdgStructure `, () => {
  it(`Create Initial Sdg Structure`, () => {
    const result = createInitialSdgStructure(indikatorenStructure, 'sdg1');
    expect(result).to.eql(initSdgStructure);
  });
});

describe(`Test: prepareBegruendungenKeys `, () => {
  it(`Prepare Begruendungen Keys`, () => {
    const result = prepareBegruendungenKeys(indikatorenStructure[0].begruendungen, 0, 'sdg1');
    expect(result).to.eql(initSdgStructure.indikatoren[0].begruendungen);
  });
});

describe(`Test: getConditionalItem `, () => {
  const resultPattern = {
    drawerLink: 'link 0',
    title: 'subtitle 0',
    showInfo: true,
    subtitle: 'text 0',
    label: 'summary 0',
    name: ['sdg1', 'indikatoren', 0, 'begruendungen', 0, 'begruendungText'],
    begruendungDrawerContent: 'content 0',
  };
  it(`Get Conditional Item`, () => {
    const result = getConditionalItem(indikatorenStructure[0].begruendungen[0], 'sdg1', 0, 0);
    expect(result).to.eql(resultPattern);
  });
});

describe(`Test: exportEnap `, () => {
  const exportController = GlobalDI.getOrRegister('exportControllerApi', () => new ExportControllerApi());
  const exportStub = Sinon.stub(exportController, 'createEnapPdfSummary');
  window.URL.createObjectURL = Sinon.stub();
  const enapData: EgfaModuleEnapEntityDTO = {
    typ: EgfaModuleType.Enap,
    deleted: false,
    influenceExpected: true,
    status: EgfaModuleStatusType.VollstaendigBearbeitet,
  };
  beforeEach(() => {
    loadingStatusSetStub.resetHistory();
    exportStub.resetHistory();
  });
  after(() => {
    exportStub.reset();
  });
  it(`no enapData, nothing called`, () => {
    exportEnap();
    Sinon.assert.notCalled(loadingStatusSetStub);
    Sinon.assert.notCalled(exportStub);
  });
  it(`endpoint and setLoasdingStatus called`, (done) => {
    exportStub.callsFake(() => {
      return new Observable<AjaxResponse<Blob>>((observer) => {
        const blob = new Blob(['Test'], { type: 'application/pdf' });
        observer.next({
          originalEvent: new Event('Test'),
          xhr: new XMLHttpRequest(),
          request: {},
          status: 200,
          response: blob,
          responseText: 'Test',
          responseType: 'blob',
        });
      });
    });
    exportEnap(enapData);
    setTimeout(() => {
      Sinon.assert.calledTwice(loadingStatusSetStub);
      Sinon.assert.calledOnce(exportStub);
      done();
    }, 20);
  });
});

describe(`Test: exportEnapToEditor`, () => {
  let egfaControllerStub: Sinon.SinonStub<
    [SendEgfaDatenuebernahmeRequest, (OperationOpts | undefined)?],
    Observable<void | AjaxResponse<void>>
  >;
  let egfaModuleControllerStub: sinon.SinonStub;
  let i18nStub: sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleEnapControllerApi.prototype, 'getEgfaModule10');

    i18nStub = Sinon.stub(i18n, 't');
  });
  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
    i18nStub.resetHistory();
  });
  it(`correct enap data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleEnapEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');

    // Call the function under test (exportEnapToEditor)
    let result: boolean;
    exportEnapToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('hello World');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct enap data, influence not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleEnapEntityResponseDTO = {
      dto: {
        influenceExpected: false,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportEnapToEditor)
    let result: boolean;
    exportEnapToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('some text');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct enap data, influenceSummary  not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleEnapEntityResponseDTO = {
      dto: {
        influenceExpected: true,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportEnapToEditor)
    let result: boolean;
    exportEnapToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('some text');
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(` getting eGFA error`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(throwError(new Error('error getting eGFA')));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('some text');
    // Call the function under test (exportEnapToEditor)
    let result: boolean;
    exportEnapToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      Sinon.assert.notCalled(egfaControllerStub);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`error on sending to editor`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleEnapEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(throwError(new Error('error getting eGFA')));
    i18nStub.returns('some text');
    // Call the function under test (exportEnapToEditor)
    let result: boolean;
    exportEnapToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall?.(0)?.args?.[0]?.egfaDatenuebernahmeDTO?.content?.[0].data;
      expect(x).to.equal('hello World');
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
