// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './einleitung.less';

import { Button, Form, Image, Input, InputRef, Tooltip, Typography } from 'antd';
import React, { useLayoutEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { BASE_PATH } from '@plateg/rest-api';
import { AriaController, GeneralFormWrapper, GlobalDI, ImageModule, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EnapLightController } from '../../../egfa-light/main/controller';
import { RecentDraftsView } from '../../../egfa-light/main/home/recentDraftsView/component.react';
import { SimpleModuleEinleitungButtons } from '../../general/simple-module/einleitung/button-section/component.react';

interface EinleitungEnapProps {
  isLight?: boolean;
}

export function EinleitungEnap(props: EinleitungEnapProps): React.ReactElement {
  const enapLightCtrl = GlobalDI.getOrRegister('enapLightController', () => new EnapLightController());

  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const history = useHistory();
  const inputRef = useRef<InputRef>(null);

  const [showLoadComponent, setShowLoadComponent] = useState(true);

  useLayoutEffect(() => AriaController.setAriaAttrByClassName('ant-image-img', 'disabled', 'true'), []);

  const onFinish = () => {
    const id = enapLightCtrl.createNewRegelungsentwurf(form.getFieldValue('draftName') as string);
    history.push(`/egfa/${id}/${routes.MODULE}/${routes.ENAP}/${routes.SDG1}`);
  };

  const onAbort = () => {
    form.setFieldsValue({ draftName: '' });
    setShowLoadComponent(true);
  };

  const loadComponent = (
    <div className="evor-load-component">
      <Button id="evor-newProject-btn" type="primary" size={'large'} onClick={() => setShowLoadComponent(false)}>
        {t('egfa.modules.enap.einleitung.btnNewProject')}
      </Button>
    </div>
  );

  const startNewComponent = (
    <div>
      <Title level={3}>{t('egfa.modules.enap.einleitung.newDraftTitle')}</Title>

      <GeneralFormWrapper form={form} layout="vertical" onFinish={onFinish} noValidate>
        <Form.Item
          name="draftName"
          label={<span>{t('egfa.modules.enap.einleitung.newDraftSubtitle')}</span>}
          rules={[{ required: true, whitespace: true, message: 'Bitte geben Sie einen Namen für den Prüfung ein!' }]}
        >
          <Input autoFocus required ref={inputRef} />
        </Form.Item>
        <Form.Item>
          <Button
            id="evor-startProject-btn"
            type="primary"
            size={'large'}
            onClick={() => {
              inputRef.current?.focus();
            }}
            htmlType="submit"
          >
            {t('egfa.modules.enap.einleitung.btnStartProject')}
          </Button>
          <Button
            id="evor-abortStartProject-btn"
            type="default"
            size={'large'}
            style={{ marginLeft: '24px' }}
            onClick={onAbort}
          >
            {t('egfa.modules.enap.einleitung.btnAbort')}
          </Button>
        </Form.Item>
      </GeneralFormWrapper>
    </div>
  );

  return (
    <div className="enap-einleitung">
      <section className="enap-einleitung-headsection">
        <Title level={1}>
          {props.isLight ? t(`egfa.modules.enap.light.einleitung.title`) : t(`egfa.modules.enap.einleitung.title`)}
        </Title>
        <Image
          loading="lazy"
          preview={false}
          src={(require(`../../../../media/enap/Nachhaltigkeitsstrategie-Deutschland-Logo.svg`) as ImageModule).default}
          alt="Logo Nachhaltigkeitsstrategie Deutschland"
        />
      </section>
      {props.isLight ? <RecentDraftsView /> : null}
      <section>
        <Title level={2}>{t(`egfa.modules.enap.einleitung.section1.title`)}</Title>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.enap.einleitung.section1.text`, { basePath: BASE_PATH }),
          }}
        />
      </section>
      <section>{props.isLight && (showLoadComponent ? loadComponent : startNewComponent)}</section>
      <div className="seperator" />
      <section>
        <Title level={3}>
          <span
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section2.title`),
            }}
          />
        </Title>
        <ul className="enap-einleitung-sdg-preview">
          {Array(18)
            .fill(undefined, 0)
            .map((_, i) => {
              const sdgNumber = i < 10 ? '0' : '';
              const title = t(`egfa.modules.enap.sdg${i}.title`);
              return (
                <li key={crypto.randomUUID()}>
                  <Tooltip
                    title={title}
                    trigger={['hover', 'focus']}
                    getPopupContainer={(triggerNode) => triggerNode.parentNode as HTMLElement}
                    key={title}
                  >
                    <Image
                      loading="lazy"
                      key={i}
                      preview={false}
                      src={
                        i === 0
                          ? (require(`../../../../media/enap/SDG-Logo-Wortmarke.svg`) as ImageModule).default
                          : (require(`../../../../media/enap/SDG-icon-DE-${sdgNumber}${i}.svg`) as ImageModule).default
                      }
                      alt={title}
                    />
                  </Tooltip>
                </li>
              );
            })}
        </ul>
      </section>
      <div className="seperator" />
      <section className="enap-einleitung-link-list">
        <Title level={3}>{t(`egfa.modules.enap.einleitung.section3.title`)}</Title>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.enap.einleitung.section3.text`),
          }}
        />
        <ul>
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.nachhaltigkeitsstrategie`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.indikatorenbericht`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.grundsatzbeschluss_nachhaltigkeitsstrategie`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.nhk_ziele`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.empfehlung_zur_staerkung_der_verbindlichkeit`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.best_practice_nachhaltigkeitspruefung`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.einleitung.section3.links.informationen`, {
                basePath: BASE_PATH,
              }),
            }}
          />
          <li>
            <InfoComponent
              key="enap-zentrale-begriffe-drawer"
              title={t('egfa.modules.enap.einleitung.section3.zentraleBegriffe.title')}
              buttonText={t(`egfa.modules.enap.einleitung.section3.links.zentraleBegriffe`)}
              titleWithoutPrefix={true}
            >
              <div
                className="rv-info-drawer"
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.enap.einleitung.section3.zentraleBegriffe.content`, {
                    basePath: BASE_PATH,
                  }),
                }}
              />
            </InfoComponent>
          </li>
        </ul>
      </section>
      {!props.isLight && <SimpleModuleEinleitungButtons moduleName={routes.ENAP} nextLinkName={routes.SDG1} />}
      {props.isLight && (
        <section className="light-contact-person">
          <Title level={3}>{t('egfa.modules.general.contactPerson')}</Title>
          <div
            dangerouslySetInnerHTML={{
              __html: t(`egfa.modules.enap.contactPerson.content`),
            }}
          />
        </section>
      )}
    </div>
  );
}
