// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { saveAs } from 'file-saver';
import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEnapControllerApi,
  EgfaModuleEnapEntityDTO,
  EgfaModuleEnapEntityDTOAllOf,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
  ExportControllerApi,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

interface SdgInitialStructureInterface {
  indikatoren: SdgIndikatorInterface[];
}
export interface SdgIndikatorMessageItemInterface {
  relevantTitle: string;
  relevantTitleDrawerText: string;
  begruendungen: SdgBegruendungMessageItemInterface[];
  anderweitigRelevantLabel: string;
}
export interface SdgBegruendungMessageItemInterface {
  [s: string]: string;
}
interface SdgIndikatorInterface {
  indikatorKey: string;
  begruendungen: SdgBegruendungInterface[];
}
interface SdgBegruendungInterface {
  begruendungKey: string;
}
export interface BegruendungFormElementInterface {
  title: string;
  drawerLink: string;
  subtitle: string;
  showInfo: boolean;
  label: string;
  name: (string | number)[];
  begruendungDrawerContent?: string;
}
export const createInitialSdgStructure = (
  indikatorenStructure: SdgIndikatorMessageItemInterface[],
  sdgStep: string,
): SdgInitialStructureInterface => {
  return {
    indikatoren: indikatorenStructure.map(
      (indikator: SdgIndikatorMessageItemInterface, index: number): SdgIndikatorInterface => {
        return {
          indikatorKey: `${sdgStep}_indikatoren_${index}_indikatorKey`,
          begruendungen: prepareBegruendungenKeys(indikator?.begruendungen, index, sdgStep),
        };
      },
    ),
  };
};

export const prepareBegruendungenKeys = (
  begruendungen: SdgBegruendungMessageItemInterface[],
  indikatorIndex: number,
  sdgStep: string,
): SdgBegruendungInterface[] => {
  return begruendungen.map((_begruendung, index) => {
    return { begruendungKey: `${sdgStep}_indikatoren_${indikatorIndex}_begruendungen_${index}_begruendungKey` };
  });
};

export const getConditionalItem = (
  begruendung: SdgBegruendungMessageItemInterface,
  sectionName: string,
  indikatorIndex: number,
  begruendungIndex: number,
): BegruendungFormElementInterface => {
  return {
    title: begruendung.title,
    drawerLink: begruendung.drawerLink,
    subtitle: begruendung.text,
    label: begruendung.summaryLabel,
    showInfo: true,
    name: [sectionName, 'indikatoren', indikatorIndex, 'begruendungen', begruendungIndex, 'begruendungText'],
    begruendungDrawerContent: begruendung.begruendungDrawerContent || undefined,
  };
};

export const exportEnap = (enapData?: EgfaModuleEnapEntityDTO): void => {
  if (enapData) {
    enapData.typ = EgfaModuleType.Enap;
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    loadingStatusController.setLoadingStatus(true);
    const exportController = GlobalDI.get<ExportControllerApi>('exportControllerApi');
    exportController.createEnapPdfSummary({ egfaModuleEnapEntityDTO: enapData }).subscribe({
      next: (data) => {
        saveAs(data, `${enapData.title || 'enap'}-${enapData.egfaId || 'x'}.pdf`);
        loadingStatusController.setLoadingStatus(false);
      },
      error: () => {
        loadingStatusController.setLoadingStatus(false);
      },
    });
  }
};

export const exportEnapToEditor = (egfaId: string, onFinish: (success: boolean, error?: AjaxError) => void): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleEnapController = GlobalDI.get<EgfaModuleEnapControllerApi>('egfaModuleEnapControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleEnapController.getEgfaModule10({ egfaId }).subscribe({
    next: (enapData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Enap,
          egfaDatenuebernahmeDTO: generateEnapJSON(enapData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('eNAP data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load eNAP data', error);
    },
  });
};
export const generateEnapJSON = (
  enapData: EgfaModuleEnapEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const enapJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'enapErgebnisdokumentation',
    title: i18n.t(`egfa.modules.enap.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  if (enapData.influenceExpected && enapData.influenceSummary) {
    enapJSONExport.content.push({
      id: 'enapSummary',
      type: EgfaItemType.Text,
      data: enapData.influenceSummary,
    });
  } else {
    enapJSONExport.content.push({
      id: 'enapSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.enap.fertigstellung.noImpact`).toString(),
    });
  }
  return enapJSONExport;
};
