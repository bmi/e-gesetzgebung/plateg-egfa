// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../enap.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEnapEntityDTO, EgfaModuleEnapSdg8EntityDTO } from '@plateg/rest-api';

import { createInitialSdgStructure, SdgIndikatorMessageItemInterface } from '../controller';
import { ENAPFormWrapper, SDGComponentProps } from '../universal-components/enap-form-wrapper/component.react';
import { StepHolderComponent } from '../universal-components/step-holder/component.react';

export function SDG8ArbeitWirtschaftComponent(props: SDGComponentProps<EgfaModuleEnapEntityDTO>): React.ReactElement {
  const { t } = useTranslation();
  const [localFormData, setLocalFormData] = useState<EgfaModuleEnapSdg8EntityDTO>();
  const indikatorenStructure: SdgIndikatorMessageItemInterface[] = t(
    `egfa.modules.enap.${props.sectionName}.indikatoren`,
    {
      returnObjects: true,
    },
  );
  // Hide all additional fields by default
  const [indikatorsVisibility, setIndikatorsIsVisibility] = useState<boolean[]>(
    Array.from(indikatorenStructure, () => false),
  );

  // Prepare initial structure for correct submit sdg for first time
  const initSdgStructure = createInitialSdgStructure(indikatorenStructure, props.sectionName);

  const sdgStep = 'sdg8';
  useEffect(() => {
    if (props.formData) {
      setLocalFormData(props.formData[sdgStep]);
      // Set text fields visible depends on indicator relevant
      if ((props.formData[sdgStep] as EgfaModuleEnapSdg8EntityDTO).indikatoren.length) {
        const indikatorsFilings =
          (props.formData[sdgStep] as EgfaModuleEnapSdg8EntityDTO).indikatoren.map((item) => item.relevant || false) ||
          [];

        setIndikatorsIsVisibility(indikatorsFilings);
      }
    }
  }, [props.formData]);

  return (
    <ENAPFormWrapper
      initialSdgStructure={initSdgStructure}
      imageFileName="SDG-icon-DE-08.svg"
      {...props}
      sdgStep={props.sectionName}
      isLight={props.isLight}
    >
      <StepHolderComponent
        indikatorenStructure={indikatorenStructure}
        sectionName={props.sectionName}
        indikatorsVisibility={indikatorsVisibility}
        anderweitigRelevantRv={localFormData?.anderweitigRelevantRv}
        referenzenList={props.referenzenList}
      />
    </ENAPFormWrapper>
  );
}
