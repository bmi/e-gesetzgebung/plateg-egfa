// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Form, FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEnapEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { SDGComponentProps } from '../universal-components/enap-form-wrapper/component.react';
import { AuswirkungenComponent } from './auswirkungen/component.react';
import { IndikatorReasonsComponent } from './indikator-reasons/component.react';
import { UnfinishedSDGsComponent } from './unfinished-sdgs/component.react';

export function EnapZusammenfassung(props: SDGComponentProps<EgfaModuleEnapEntityDTO>): React.ReactElement {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    form.setFieldsValue(props.formData);
    setIsDirty(false);
  }, [props.formData]);

  const onSave = (formInstance: FormInstance<any>) => {
    formInstance.setFieldsValue({ influenceExpected: true });
    return props.onSave(formInstance);
  };

  return (
    <FormWrapper
      projectName={props.isLight ? 'eNAP' : 'eGFA'}
      title={<Title level={1}>{t(`egfa.modules.enap.zusammenfassung.title`)}</Title>}
      previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/enap/${props.previousLinkName as string}`}
      nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/enap/${props.nextLinkName as string}`}
      saveDraft={onSave(form)}
      isDirty={() => isDirty}
      form={form}
      formInitialValue={{}}
      setFormInstance={props.setFormInstance}
      handleFormChanges={() => {
        setIsDirty(true);
        props.handleFormChange();
      }}
      showMandatoryFieldInfo={true}
    >
      <div className="enap-zusammenfassung">
        <p className="ant-typography p-no-style">{t(`egfa.modules.enap.zusammenfassung.intro1`)}</p>
        <p
          className="ant-typography p-no-style"
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.enap.zusammenfassung.intro2`),
          }}
        />
        <UnfinishedSDGsComponent entity={props.formData} />
        <IndikatorReasonsComponent entity={props.formData} />
        <AuswirkungenComponent />
      </div>
    </FormWrapper>
  );
}
