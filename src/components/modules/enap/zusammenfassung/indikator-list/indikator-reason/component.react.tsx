// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

interface IndikatorReasonComponentProps {
  title: string;
  content?: string;
}

export function IndikatorReasonComponent(props: IndikatorReasonComponentProps): React.ReactElement {
  const keineAngabe = <i>keine Angabe</i>;
  return (
    <span className="reason">
      <span className="reason-title">{props.title}</span>
      <span className="reason-content">{props.content || keineAngabe}</span>
    </span>
  );
}
