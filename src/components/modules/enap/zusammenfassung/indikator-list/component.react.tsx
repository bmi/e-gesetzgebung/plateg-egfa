// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './indikator-list.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  EgfaModuleEnapEntityDTO,
  EgfaModuleEnapPrinzipienEntityDTOAllOf,
  EgfaModuleEnapSdgEntityDTO,
} from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { SdgIndikatorMessageItemInterface } from '../../controller';
import { IndikatorReasonComponent } from './indikator-reason/component.react';

interface IndikatorReasonsListComponentProps {
  entity?: EgfaModuleEnapEntityDTO;
  onlyReasonsAreVisible: boolean;
  customIncludedSdgs?: string[];
}

export function IndikatorReasonsListComponent(props: IndikatorReasonsListComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const keineAngabe = <i>keine Angabe</i>;
  const [sdgsWithIndikatorenAndReasons, setSdgsWithIndikatorenAndReasons] =
    useState<{ name: string; sdg: EgfaModuleEnapSdgEntityDTO }[]>();

  useEffect(() => {
    if (props.entity) {
      let sdgs: { name: string; sdg: EgfaModuleEnapSdgEntityDTO }[] = [];
      for (let i = 1; i < 19; i++) {
        let sdgName;
        let sdg = null;
        if (i < 18) {
          sdgName = `sdg${i}`;
        } else {
          sdgName = 'prinzipien';
        }
        sdg = props?.entity?.[sdgName as keyof EgfaModuleEnapEntityDTO] as
          | (EgfaModuleEnapPrinzipienEntityDTOAllOf & EgfaModuleEnapSdgEntityDTO)
          | null;

        const finishedIndikator = sdg?.indikatoren.findIndex((indikator) => {
          return indikator.relevant;
        });
        if (sdg && ((finishedIndikator !== undefined && finishedIndikator >= 0) || sdg.anderweitigRelevantRv)) {
          sdgs.push({ name: sdgName, sdg });
        }
      }
      if (props.customIncludedSdgs) {
        sdgs = sdgs.filter((sdg) => {
          return props.customIncludedSdgs?.includes(sdg.name);
        });
      }
      setSdgsWithIndikatorenAndReasons(sdgs);
    }
  }, [props.entity]);

  if (props.onlyReasonsAreVisible) {
    const allReasons: string[] = [];

    sdgsWithIndikatorenAndReasons?.forEach((sdg) => {
      sdg.sdg.indikatoren
        .filter((indikator) => {
          return indikator.relevant === true;
        })
        .forEach((indikator) => {
          indikator.begruendungen.forEach((begruendung) => {
            if (begruendung.begruendungText) {
              allReasons.push(begruendung.begruendungText);
            }
          });
          if (indikator.anderweitigRelevant) {
            allReasons.push(indikator.anderweitigRelevant);
          }
        });

      if (sdg.sdg.anderweitigRelevantRv && sdg.sdg.anderweitigRelevantRvSummary) {
        allReasons.push(sdg.sdg.anderweitigRelevantRvSummary);
      }
    });

    return (
      <div className="reasons-only">
        {allReasons.map((reason) => {
          return <span>{reason}</span>;
        })}
      </div>
    );
  }

  return (
    <>
      {sdgsWithIndikatorenAndReasons?.map((sdg) => {
        return (
          <div className="sdg-with-indikatoren">
            <div className="sdg-name">
              <span>{t(`egfa.modules.enap.${sdg.name}.title`)}</span>
              <Link
                className="blue-text-button"
                to={`/egfa/${props.entity?.egfaId || ''}/${routes.MODULE}/enap/${sdg.name}`}
              >
                {t(`egfa.modules.enap.zusammenfassung.unfinishedSdgs.zurBearbeitungBtn`)}
              </Link>
            </div>
            {sdg.sdg.indikatoren
              .filter((indikator) => {
                return indikator.relevant === true;
              })
              .map((indikator) => {
                const translation = t(`egfa.modules.enap.${sdg.name}.indikatoren`, {
                  returnObjects: true,
                })[
                  sdg.sdg.indikatoren.findIndex((item) => {
                    return item.indikatorKey === indikator.indikatorKey;
                  })
                ] as unknown as SdgIndikatorMessageItemInterface;
                return (
                  <div className={`indikator-with-reasons ${sdg.name === 'prinzipien' ? 'indikator-prinzipien' : ''}`}>
                    <span className="indikator-title">{translation.relevantTitle}</span>
                    <div className="reasons">
                      {translation.begruendungen.map((begruendung, i) => {
                        return (
                          <IndikatorReasonComponent
                            title={sdg.name !== 'prinzipien' ? begruendung.title : ''}
                            content={indikator.begruendungen[i]?.begruendungText}
                          />
                        );
                      })}
                      {sdg.name !== 'prinzipien' && (
                        <IndikatorReasonComponent
                          title={t(`egfa.modules.enap.zusammenfassung.reasons.anderweitig`, {
                            bereich: translation.relevantTitle,
                          })}
                          content={indikator.anderweitigRelevant}
                        />
                      )}
                    </div>
                  </div>
                );
              })}
            {sdg.name !== 'prinzipien' && (
              <div className="additonal-reason">
                <span className="indikator-title">
                  {t(`egfa.modules.enap.zusammenfassung.reasons.anderweitig`, {
                    bereich: t(`egfa.modules.enap.${sdg.name}.title`),
                  })}
                </span>
                <span className="additonal-reason-content">{sdg.sdg.anderweitigRelevantRvSummary || keineAngabe}</span>
              </div>
            )}
          </div>
        );
      })}
    </>
  );
}
