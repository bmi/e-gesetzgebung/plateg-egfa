// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './auswirkungen.less';

import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH } from '@plateg/rest-api';
import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

export function AuswirkungenComponent(): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className="enap-auswirkungen-text">
      <Title level={2}>{t(`egfa.modules.enap.zusammenfassung.auswirkungen.title`)}</Title>
      <FormItemWithInfo
        name="influenceSummary"
        label={
          <>
            {t('egfa.modules.enap.zusammenfassung.auswirkungen.title')}{' '}
            <InfoComponent
              title={t(`egfa.modules.enap.zusammenfassung.auswirkungen.drawers.zusammenfassung.title`)}
              withLabelRequired
            >
              <p
                dangerouslySetInnerHTML={{
                  __html: t('egfa.modules.enap.zusammenfassung.auswirkungen.drawers.zusammenfassung.content', {
                    basePath: BASE_PATH,
                  }),
                }}
              />
            </InfoComponent>
          </>
        }
        rules={[
          {
            required: true,
            whitespace: true,
            message: t('egfa.modules.enap.zusammenfassung.auswirkungen.requiredMsg'),
          },
        ]}
      >
        <TextArea rows={8} />
      </FormItemWithInfo>
    </div>
  );
}
