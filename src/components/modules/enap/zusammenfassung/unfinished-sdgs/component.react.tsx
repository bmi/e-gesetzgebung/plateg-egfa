// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './unfinished-sdgs.less';

import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import {
  EgfaModuleEnapEntityDTO,
  EgfaModuleEnapPrinzipienEntityDTOAllOf,
  EgfaModuleEnapSdgEntityDTO,
} from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';

type CheckSdgArgType = {
  sdg: (EgfaModuleEnapPrinzipienEntityDTOAllOf & EgfaModuleEnapSdgEntityDTO) | null;
  sdgName: string;
  sdgs: string[];
  unfinishedIndikator: number | undefined;
};

export function UnfinishedSDGsComponent(props: { entity?: EgfaModuleEnapEntityDTO }): React.ReactElement {
  const { t } = useTranslation();
  const [unfinishedSdgs, setUnfinishedSdgs] = useState<string[]>([]);

  useEffect(() => {
    if (props.entity) {
      const sdgs: string[] = [];
      for (let i = 1; i < 19; i++) {
        const sdgName = i < 18 ? `sdg${i}` : 'prinzipien';
        const sdg = props?.entity?.[sdgName as keyof EgfaModuleEnapEntityDTO] as
          | (EgfaModuleEnapPrinzipienEntityDTOAllOf & EgfaModuleEnapSdgEntityDTO)
          | null;

        const unfinishedIndikator = sdg?.indikatoren.findIndex((indikator) => {
          return indikator.relevant === undefined || indikator.relevant === null;
        });
        checkSdg({ sdg, sdgName, sdgs, unfinishedIndikator });
      }
      setUnfinishedSdgs(sdgs);
    }
  }, [props.entity]);

  // helper func to reduce complexity
  const checkSdg = (argument: CheckSdgArgType) => {
    const { sdg, sdgName, sdgs, unfinishedIndikator } = argument;
    if (
      !sdg ||
      (unfinishedIndikator !== undefined && unfinishedIndikator >= 0) ||
      (sdg.anderweitigRelevantRv === undefined && sdgName !== 'prinzipien') ||
      (sdg.anderweitigRelevantRv === null && sdgName !== 'prinzipien')
    ) {
      sdgs.push(sdgName);
    }
  };

  return (
    <div className="unfinished-sdgs">
      <Title level={2}>{t(`egfa.modules.enap.zusammenfassung.unfinishedSdgs.title`)}</Title>
      {unfinishedSdgs?.length === 0 && <span>{t(`egfa.modules.enap.zusammenfassung.unfinishedSdgs.allFinished`)}</span>}
      <ul>
        {unfinishedSdgs?.map((sdg) => {
          return (
            <li className="unfinished-sdg">
              <span>{t(`egfa.modules.enap.${sdg}.title`)}</span>
              <Link
                className="blue-text-button"
                to={`/egfa/${props.entity?.egfaId || ''}/${routes.MODULE}/enap/${sdg}`}
              >
                {t(`egfa.modules.enap.zusammenfassung.unfinishedSdgs.zurBearbeitungBtn`)}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
