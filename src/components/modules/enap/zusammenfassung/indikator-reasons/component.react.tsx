// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './indikator-reasons.less';

import { Switch } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEnapEntityDTO } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';

import { IndikatorReasonsListComponent } from '../indikator-list/component.react';

export function IndikatorReasonsComponent(props: { entity?: EgfaModuleEnapEntityDTO }): React.ReactElement {
  const { t } = useTranslation();
  const [onlyReasonsAreVisible, setOnlyReasonsAreVisible] = useState(false);

  return (
    <div className="indikator-reasons">
      <Title level={2}>{t(`egfa.modules.enap.zusammenfassung.reasons.title`)}</Title>
      <div className="indikator-reasons-switch">
        <div>
          <Switch onChange={(val) => setOnlyReasonsAreVisible(val)} />
        </div>
        <span>
          {t(`egfa.modules.enap.zusammenfassung.reasons.switchLabel`)}
          <InfoComponent
            isContactPerson={false}
            title={t('egfa.modules.enap.zusammenfassung.reasons.drawers.begruessung.title')}
          >
            {t('egfa.modules.enap.zusammenfassung.reasons.drawers.begruessung.content')}
          </InfoComponent>
        </span>
      </div>
      <IndikatorReasonsListComponent onlyReasonsAreVisible={onlyReasonsAreVisible} entity={props.entity} />
    </div>
  );
}
