// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './besondere-belastungen.less';

import { Form, Input, Typography } from 'antd';
import i18n from 'i18next';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';

import { EgfaModuleKMUEntityDTOAllOf } from '@plateg/rest-api';
import { CheckboxGroupWithInfo, FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface ModuleBesondereBelastungenProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
  referenzen2?: React.ReactElement;
}

interface CategoriesListInterface {
  [s: string]: string[];
}

export function ModuleBesondereBelastungen(
  props: ModuleBesondereBelastungenProps<EgfaModuleKMUEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [rvUmsetzungSummary, setRvUmsetzungSummary] = useState<string>();
  const [rvBelastungSummary, setRvBelastungSummary] = useState<string>();
  const [umsetzungChecked, setUmsetzungChecked] = useState(false);
  const [belastungChecked, setBelastungChecked] = useState(false);
  const [isDirty, setIsDirty] = useState(false);
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName || '');
  const { hash } = useLocation();

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      setRvUmsetzungSummary(props.formData.rvUmsetzungSummary);
      setRvBelastungSummary(props.formData.rvBelastungSummary);
      setIsDirty(false);
    }
    if (hash) {
      const ref = document.body.querySelector(hash);
      setTimeout(() => {
        window.scroll({
          top: ref?.getBoundingClientRect().top - 110,
          behavior: 'instant',
        });
      }, 10);
    }
  }, [props.formData]);

  // Enable next link in sidebar and clear summary fields if active
  useEffect(() => {
    const linksList = [props.sectionName];
    if (props.updateNextLink && props.nextLinkName) {
      if (rvUmsetzungSummary || rvBelastungSummary) {
        linksList.push(props.nextLinkName);
        setNextLink(props.nextLinkName);
      } else {
        setNextLink(routes.ZUSAMMENFASSUNG);
      }
      props.updateNextLink(linksList, false);
    }
  }, [rvUmsetzungSummary, rvBelastungSummary]);

  const categoriesList: CategoriesListInterface = {
    EgfaKMUUmsetzungType: [
      'RV_UMSETZUNG_NICHT_ELEKTRONISCH',
      'RV_UMSETZUNG_ROUTINE_FEHLT',
      'RV_UMSEZUNG_SPEZIALISTEN_FEHLEN',
      'RV_UMSETZUNG_INVESTITIONEN',
      'RV_UMSETZUNG_ANDERE',
    ],
    EgfaKMUBelastungType: [
      'RV_BELASTUNG_MARKTANTEILE',
      'RV_BELASTUNG_MARKTEINTRITTSBARRIERREN',
      'RV_BELASTUNG_PRODUKTION',
      'RV_BELASTUNG_FORSCHUNG',
      'RV_BELASTUNG_INTERNATIONALER_WETTBEWERB',
      'RV_BELASTUNG_UMSETZUNG_FREIWILLIG',
      'RV_BELASTUNG_GEWINNUNG_VON_FACHKRAEFTEN',
      'RV_BELASTUNG_AUSLAGEN',
      'RV_BELASTUNG_ANDERE',
    ],
  };

  const prepareCheckboxesList = (nameCategory: string) => {
    return categoriesList[nameCategory].map((itemName: string) => {
      return {
        label: (
          <span className="nonBreakingArea">
            <span className="normalBreakingArea">
              {t(`egfa.modules.${props.moduleName}.besondereBelastungen.${nameCategory}.items.${itemName}.label`)}
            </span>
            &nbsp;
            {i18n.exists(
              `egfa.modules.${props.moduleName}.besondereBelastungen.${nameCategory}.items.${itemName}.tooltip`,
            ) && (
              <InfoComponent
                title={t(
                  `egfa.modules.${props.moduleName}.besondereBelastungen.${nameCategory}.items.${itemName}.label`,
                )}
              >
                <p>
                  {t(`egfa.modules.${props.moduleName}.besondereBelastungen.${nameCategory}.items.${itemName}.tooltip`)}
                </p>
              </InfoComponent>
            )}
          </span>
        ),
        value: itemName,
        title: t(`egfa.modules.${props.moduleName}.besondereBelastungen.${nameCategory}.items.${itemName}.label`),
      };
    });
  };

  const handleCheckboxChange = useCallback(
    (setChecked: (checked: boolean) => void, setTextSummary: (summary: string) => void, checkboxName: string) =>
      (value: (string | number)[]) => {
        const isChecked = value && value.length > 0;
        setChecked(isChecked);
        if (!isChecked) {
          setTextSummary('');
          form.setFieldsValue({ [checkboxName]: '' });
        }
      },
    [],
  );

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      setRvUmsetzungSummary(props.formData.rvUmsetzungSummary);
      setRvBelastungSummary(props.formData.rvBelastungSummary);
      setUmsetzungChecked((props.formData.optionenUmsetzung?.length ?? 0) > 0);
      setBelastungChecked((props.formData.optionenBelastung?.length ?? 0) > 0);
    }
  }, [props.formData]);

  const prepareSummaryTextarea = (areaName: string, setStateMethod: (value: string) => void) => {
    return (
      <FormItemWithInfo
        name={areaName}
        label={
          <span>
            {t(`egfa.modules.${props.moduleName}.besondereBelastungen.${areaName}.label`)}
            <InfoComponent
              title={t(`egfa.modules.${props.moduleName}.besondereBelastungen.${areaName}.label`)}
              withLabelRequired
            >
              <p>{t(`egfa.modules.${props.moduleName}.besondereBelastungen.${areaName}.tooltip`)}</p>
            </InfoComponent>
          </span>
        }
        rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
      >
        <TextArea rows={8} onChange={(event) => setStateMethod(event?.target?.value)} />
      </FormItemWithInfo>
    );
  };

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.besondereBelastungen.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <Title level={3}>{t(`egfa.modules.${props.moduleName}.besondereBelastungen.title2`)}</Title>
        <div className="besondere-belastungen-holder">
          <fieldset className="fieldset-form-items" id="optionenUmsetzung-section">
            <legend>{t(`egfa.modules.${props.moduleName}.besondereBelastungen.EgfaKMUUmsetzungType.title`)}</legend>
            <Form.Item name="optionenUmsetzung">
              <CheckboxGroupWithInfo
                name="optionenUmsetzung"
                id="optionenUmsetzung"
                items={prepareCheckboxesList('EgfaKMUUmsetzungType')}
                onChange={handleCheckboxChange(setUmsetzungChecked, setRvUmsetzungSummary, 'rvUmsetzungSummary')}
              />
            </Form.Item>
          </fieldset>
          {umsetzungChecked && prepareSummaryTextarea('rvUmsetzungSummary', setRvUmsetzungSummary)}
          {props.referenzen}
          <fieldset className="fieldset-form-items" id="optionenBelastung-section">
            <legend>{t(`egfa.modules.${props.moduleName}.besondereBelastungen.EgfaKMUBelastungType.title`)}</legend>
            <Form.Item name="optionenBelastung">
              <CheckboxGroupWithInfo
                name="optionenBelastung"
                id="optionenBelastung"
                items={prepareCheckboxesList('EgfaKMUBelastungType')}
                onChange={handleCheckboxChange(setBelastungChecked, setRvBelastungSummary, 'rvBelastungSummary')}
              />
            </Form.Item>
          </fieldset>
          {belastungChecked && prepareSummaryTextarea('rvBelastungSummary', setRvBelastungSummary)}
          {props.referenzen2}
        </div>
      </FormWrapper>
    </div>
  );
}
