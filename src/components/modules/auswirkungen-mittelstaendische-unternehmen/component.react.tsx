// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleKmuControllerApi,
  EgfaModuleKMUEntityDTO,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import { KmuReferenzenTypes } from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { ModuleBesondereBelastungen } from './besondere-belastungen/component.react';
import { KMUModuleFertigstellung } from './fertigstellung/component.react';
import { generatekmuJSON } from './fertigstellung/controller';
import { KMUModuleZusammenfassung } from './kmu-zusammenfassung/component.react';
import { ModuleRegelungsalternativen } from './regelungsalternativen/component.react';
import { ModuleRelevanzpruefung } from './relevanzpruefung/component.react';

export function AuswirkungenMittelstaendischeUnternehmen(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [mittelstaendischeUnternehmenData, setMittelstaendischeUnternehmenData] = useState<
    EgfaModuleKMUEntityDTO & ExtendedModuleData
  >();
  const egfaModuleKmuController = GlobalDI.get<EgfaModuleKmuControllerApi>('egfaModuleKmuControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaModuleKmuController.getEgfaModule4({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleKMUEntityDTO) => {
    if (!localData.influenceExpected || !localData.rvHoherErfuellungsaufwand) {
      if (!localData.influenceExpected) {
        localData.rvHoherErfuellungsaufwand = false;
      }
      localData.rvUmsetzungSummary = undefined;
      localData.rvBelastungSummary = undefined;
      localData.rvUnterstuetzungsmassnahmenSummary = undefined;
      localData.optionenBelastung = [];
      localData.optionenUmsetzung = [];
    }
    return egfaModuleKmuController.modifyEgfaModule4({
      egfaId: egfaId,
      egfaModuleKMUEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.MITTELSTAENDISCHE_UNTERNEHMEN);
  }, []);

  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (mittelstaendischeUnternehmenData?.referenzenData) {
      setReferenzenData(mittelstaendischeUnternehmenData.referenzenData);
    }
  }, [mittelstaendischeUnternehmenData]);
  const PageRoutingCompAMU = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ): React.ReactElement => {
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: mittelstaendischeUnternehmenData,
      onSave,
      handleFormChange,
    };
    useEffect(() => {
      const formData = mittelstaendischeUnternehmenData;

      if (
        updateItemDisableStatus &&
        formData &&
        formData.status === EgfaModuleStatusType.InBearbeitung &&
        formData.influenceSummary
      ) {
        const linksList = [];

        if (formData.rvHoherErfuellungsaufwand) {
          linksList.push(routes.BESONDERE_BELASTUNGEN);
        } else {
          linksList.push(routes.FERTIGSTELLUNG);
        }

        if (formData.rvUmsetzungSummary || formData.rvBelastungSummary) {
          linksList.push(routes.REGELUNGSALTERNATIVEN);
        }

        updateItemDisableStatus(linksList, false);
      }
    }, [mittelstaendischeUnternehmenData]);

    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };
    return (
      <Switch>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.FERTIGSTELLUNG}`]}
        >
          <KMUModuleFertigstellung {...contentPageProps} moduleName={routes.MITTELSTAENDISCHE_UNTERNEHMEN} />
        </Route>
        {(mittelstaendischeUnternehmenData?.aktionen?.includes(AktionType.Schreiben) === false ||
          mittelstaendischeUnternehmenData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.RELEVANZPRUEFUNG}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.RELEVANZPRUEFUNG}`]}
        >
          <ModuleRelevanzpruefung
            {...contentPageProps}
            moduleName={`${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`}
            sectionName={routes.RELEVANZPRUEFUNG}
            previousLinkName={routes.EINLEITUNG}
            nextLinkName={routes.BESONDERE_BELASTUNGEN}
            updateNextLink={updateItemDisableStatus}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.kmuErfuellungsaufwandReferenzen,
                    conditionsList: [KmuReferenzenTypes.moreThanOneMioEurWirtschaft],
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                    checkTwoCasesCondition: true,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.MITTELSTAENDISCHE_UNTERNEHMEN,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.BESONDERE_BELASTUNGEN}`]}
        >
          <ModuleBesondereBelastungen
            {...contentPageProps}
            moduleName={`${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`}
            sectionName={routes.BESONDERE_BELASTUNGEN}
            previousLinkName={routes.RELEVANZPRUEFUNG}
            nextLinkName={routes.REGELUNGSALTERNATIVEN}
            updateNextLink={updateItemDisableStatus}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.kmuErfuellungsaufwandReferenzen,
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.MITTELSTAENDISCHE_UNTERNEHMEN,
                  },
                ]}
              />
            }
            referenzen2={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.kmuSonstigeKostenReferenzen,
                    moduleName: routes.SONSTIGE_KOSTEN,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.MITTELSTAENDISCHE_UNTERNEHMEN,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.REGELUNGSALTERNATIVEN}`]}
        >
          <ModuleRegelungsalternativen
            {...contentPageProps}
            moduleName={`${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`}
            sectionName={routes.REGELUNGSALTERNATIVEN}
            previousLinkName={routes.BESONDERE_BELASTUNGEN}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            updateNextLink={updateItemDisableStatus}
          />
        </Route>

        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.ZUSAMMENFASSUNG}`]}
        >
          <KMUModuleZusammenfassung
            {...contentPageProps}
            moduleName={routes.MITTELSTAENDISCHE_UNTERNEHMEN}
            previousLinkName={routes.REGELUNGSALTERNATIVEN}
            sectionName={routes.ZUSAMMENFASSUNG}
            nextLinkName={routes.FERTIGSTELLUNG}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.MITTELSTAENDISCHE_UNTERNEHMEN}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={setMittelstaendischeUnternehmenData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRoutingCompAMU}
      pageName={currentPageName}
      customResultPage="fertigstellung"
      generateJson={generatekmuJSON}
    />
  );
}
