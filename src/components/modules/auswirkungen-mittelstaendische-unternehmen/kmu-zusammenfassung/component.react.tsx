// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { AktionType, EgfaModuleKMUEntityDTOAllOf, EgfaModuleStatusType } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';

export interface KMUModuleZusammenfassungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
}

export function KMUModuleZusammenfassung(
  props: KMUModuleZusammenfassungProps<EgfaModuleKMUEntityDTOAllOf>,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const { TextArea } = Input;
  const moduleNameRoute: string = routes.MITTELSTAENDISCHE_UNTERNEHMEN;

  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);
  const [affectedMessage, setAffectedMessage] = useState('');

  useEffect(() => {
    if (props.formData) {
      const formData = { ...props.formData };

      // Set form data
      form.setFieldsValue(formData);
      setIsDirty(false);

      let affectedKey = 'affected';
      if (!props.formData?.influenceExpected) {
        affectedKey = 'notAffected';
      } else if (props.formData?.influenceExpected && !props.formData?.rvHoherErfuellungsaufwand) {
        affectedKey = 'notRelevantAffected';
      }
      setAffectedMessage(t(`egfa.modules.${props.moduleName}.zusammenfassung.${affectedKey}`));
    }
  }, [props.formData]);

  return (
    <FormWrapper
      projectName="eGFA"
      title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.zusammenfassung.title`)}</Title>}
      previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
      nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName as string}`}
      saveDraft={props.onSave(form)}
      isDirty={() => isDirty}
      form={form}
      formInitialValue={{}}
      setFormInstance={props.setFormInstance}
      showMandatoryFieldInfo={props.formData?.rvHoherErfuellungsaufwand}
      handleFormChanges={() => {
        setIsDirty(true);
        props.handleFormChange?.();
      }}
    >
      <div className="zusammenfassung-div">
        <p>{t(`egfa.modules.${props.moduleName}.zusammenfassung.infoParagraph`)}</p>
        <ZusammenfassungItem
          title={t(`egfa.modules.${props.moduleName}.relevanzpruefung.title`)}
          bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.RELEVANZPRUEFUNG}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          egfaId={props.egfaId}
          allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
        >
          <div>{affectedMessage}</div>
        </ZusammenfassungItem>
        {props.formData?.influenceExpected && props.formData?.rvHoherErfuellungsaufwand && (
          <>
            <ZusammenfassungItem
              title={t(`egfa.modules.${props.moduleName}.besondereBelastungen.title`)}
              bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.BESONDERE_BELASTUNGEN}`}
              onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
              egfaId={props.egfaId}
              allowedToModify={false}
            >
              <div>
                <div className="title-div">
                  <Link
                    className="blue-text-button"
                    id="egfa-summaryBearbeitung-link-optionenUmsetzung"
                    to={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.BESONDERE_BELASTUNGEN}#optionenUmsetzung-section`}
                    onClick={() => props.onSave()}
                  >
                    {t('egfa.modules.general.summaryBearbeitung')}
                  </Link>
                  <p style={{ fontWeight: 500 }}>
                    {t(`egfa.modules.${props.moduleName}.zusammenfassung.impactHoherErfuellungsaufwand`)}
                  </p>
                </div>

                {props.formData?.rvUmsetzungSummary ? (
                  <p>{props.formData?.rvUmsetzungSummary}</p>
                ) : (
                  <p>{t(`egfa.modules.${props.moduleName}.zusammenfassung.unaffected`)}</p>
                )}

                <div className="title-div">
                  <Link
                    className="blue-text-button"
                    id="egfa-summaryBearbeitung-link-optionenBelastung-section"
                    to={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.BESONDERE_BELASTUNGEN}#optionenBelastung-section`}
                    onClick={() => props.onSave()}
                  >
                    {t('egfa.modules.general.summaryBearbeitung')}
                  </Link>
                  <p style={{ fontWeight: 500 }}>
                    {t(`egfa.modules.${props.moduleName}.zusammenfassung.impactSonstigeKosten`)}
                  </p>
                </div>

                {props.formData?.rvBelastungSummary ? (
                  <p>{props.formData?.rvBelastungSummary}</p>
                ) : (
                  <p>{t(`egfa.modules.${props.moduleName}.zusammenfassung.unaffected`)}</p>
                )}
              </div>
            </ZusammenfassungItem>

            <ZusammenfassungItem
              title={t(`egfa.modules.${props.moduleName}.regelungsalternativen.title`)}
              bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.REGELUNGSALTERNATIVEN}`}
              onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
              egfaId={props.egfaId}
              allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
            >
              <div>
                <p style={{ fontWeight: 500 }}>
                  {t(`egfa.modules.${props.moduleName}.zusammenfassung.impactMassnahmen`)}
                </p>

                <p>{props.formData?.rvUnterstuetzungsmassnahmenSummary}</p>
              </div>
            </ZusammenfassungItem>
            <div className="enap-auswirkungen-text">
              <Title level={2}>{t(`egfa.modules.${props.moduleName}.zusammenfassung.ergebnisse.title`)}</Title>
              <FormItemWithInfo
                name="influenceSummary"
                label={<>{t(`egfa.modules.${props.moduleName}.zusammenfassung.ergebnisse.label`)}</>}
                rules={[
                  {
                    required: true,
                    whitespace: true,
                    message: t(`egfa.modules.${props.moduleName}.zusammenfassung.ergebnisse.requiredMsg`),
                  },
                ]}
              >
                <TextArea rows={8} />
              </FormItemWithInfo>
            </div>
          </>
        )}
      </div>
    </FormWrapper>
  );
}
