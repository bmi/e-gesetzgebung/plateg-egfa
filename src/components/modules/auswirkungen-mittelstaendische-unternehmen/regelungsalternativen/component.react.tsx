// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleKMUEntityDTOAllOf } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface ModuleRegelungsalternativenProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
}
export function ModuleRegelungsalternativen(
  props: ModuleRegelungsalternativenProps<EgfaModuleKMUEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      const formData = { ...props.formData };

      // Set form data
      form.setFieldsValue(formData);
      setIsDirty(false);
    }
  }, [props.formData]);

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.regelungsalternativen.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName as string}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <Title level={3}>{t(`egfa.modules.${props.moduleName}.regelungsalternativen.additionalTitle`)}</Title>

        <p
          className="ant-typography p-no-style"
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.infobox`),
          }}
        ></p>

        <EgfaInfoCollapse
          panelKey="1"
          headerText={t(`egfa.modules.${props.moduleName}.regelungsalternativen.list1.title`)}
          text={
            <ol>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list1.item1`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list1.item2`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list1.item3`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list1.item4`),
                }}
              ></li>
            </ol>
          }
        />
        <EgfaInfoCollapse
          panelKey="2"
          headerText={t(`egfa.modules.${props.moduleName}.regelungsalternativen.list2.title`)}
          text={
            <ol className="ant-typography">
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list2.item1`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list2.item2`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list2.item3`),
                }}
              ></li>
              <li
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.${props.moduleName}.regelungsalternativen.list2.item4`),
                }}
              ></li>
            </ol>
          }
        />
        <Form.Item
          name={'rvUnterstuetzungsmassnahmenSummary'}
          label={<span>{t(`egfa.modules.${props.moduleName}.regelungsalternativen.textareaLabel`)}</span>}
          rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
        >
          <TextArea rows={8} />
        </Form.Item>
      </FormWrapper>
    </div>
  );
}
