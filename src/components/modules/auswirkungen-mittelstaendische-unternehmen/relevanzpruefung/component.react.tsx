// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleKMUEntityDTOAllOf } from '@plateg/rest-api';
import { FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface ModuleRelevanzpruefungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string | string[], isDisabled: boolean) => void;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}
export function ModuleRelevanzpruefung(
  props: ModuleRelevanzpruefungProps<EgfaModuleKMUEntityDTOAllOf>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [influenceExpected, setInfluenceExpected] = useState<boolean>();
  const [rvHoherErfuellungsaufwand, setRvHoherErfuellungsaufwand] = useState<boolean>();
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName || '');
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      // Set form data
      form.setFieldsValue(props.formData);
      setInfluenceExpected(props.formData.influenceExpected);
      setIsDirty(false);
    }
  }, [props.formData]);

  // Enable next link in sidebar and clear summary fields if active
  useEffect(() => {
    const fromFields = form.getFieldsValue(true) as EgfaModuleKMUEntityDTOAllOf;
    const linksList = [];
    if (props.updateNextLink && props.nextLinkName && fromFields) {
      if (influenceExpected && rvHoherErfuellungsaufwand) {
        linksList.push(props.nextLinkName);
        setNextLink(props.nextLinkName);
      } else {
        setNextLink(routes.ZUSAMMENFASSUNG);
      }
      props.updateNextLink(linksList, false);
    }
  }, [influenceExpected, rvHoherErfuellungsaufwand]);

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.relevanzpruefung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <fieldset className="fieldset-form-items">
          <legend className="seo">{t(`egfa.modules.${props.moduleName}.relevanzpruefung.question1`)}</legend>
          <Form.Item
            name={'influenceExpected'}
            label={t(`egfa.modules.${props.moduleName}.relevanzpruefung.question1`)}
            rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
          >
            <Radio.Group
              className="horizontal-radios"
              onChange={(event) => setInfluenceExpected(event?.target?.value as boolean)}
              name={'influenceExpected'}
            >
              <Radio id="egfa-relevanzpruefung-influenceExpectedJa-radio" className="horizontal-radios" value={true}>
                Ja
              </Radio>
              <Radio id="egfa-relevanzpruefung-influenceExpectedNein-radio" className="horizontal-radios" value={false}>
                Nein
              </Radio>
            </Radio.Group>
          </Form.Item>
        </fieldset>
        {influenceExpected && (
          <>
            <fieldset className="fieldset-form-items">
              <legend className="seo">{t(`egfa.modules.${props.moduleName}.relevanzpruefung.question2`)}</legend>
              <Form.Item
                name={'rvHoherErfuellungsaufwand'}
                label={
                  <span>
                    {t(`egfa.modules.${props.moduleName}.relevanzpruefung.question2`)}
                    <span style={{ position: 'relative', marginLeft: '10px', marginRight: '-40px' }}>
                      <InfoComponent title={t(`egfa.modules.${props.moduleName}.relevanzpruefung.drawerTitle`)}>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung.drawerText`),
                          }}
                        ></div>
                      </InfoComponent>
                    </span>
                  </span>
                }
                rules={[{ required: true, message: t('egfa.modules.validationMessages.radioGroup') }]}
              >
                <Radio.Group
                  className="horizontal-radios"
                  onChange={(event) => setRvHoherErfuellungsaufwand(event?.target?.value as boolean)}
                  name={'rvHoherErfuellungsaufwand'}
                >
                  <Radio
                    id="egfa-relevanzpruefung-rvHoherErfuellungsaufwandJa-radio"
                    className="horizontal-radios"
                    value={true}
                  >
                    Ja
                  </Radio>
                  <Radio
                    id="egfa-relevanzpruefung-rvHoherErfuellungsaufwandNein-radio"
                    className="horizontal-radios"
                    value={false}
                  >
                    Nein
                  </Radio>
                </Radio.Group>
              </Form.Item>
            </fieldset>
          </>
        )}
        {props.referenzen}
      </FormWrapper>
    </div>
  );
}
