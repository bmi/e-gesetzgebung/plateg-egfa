// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleKMUEntityDTOAllOf, EgfaModuleStatusType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ModulesListController } from '../../../egfa/main/modules-list/controller';
import { SimpleModuleProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';

export function KMUModuleFertigstellung(props: SimpleModuleProps<EgfaModuleKMUEntityDTOAllOf>): React.ReactElement {
  const { t } = useTranslation();
  const moduleNameRoute: string = routes.MITTELSTAENDISCHE_UNTERNEHMEN;
  const [affectedMessage, setAffectedMessage] = useState('');

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const PDFExportBtn = ctrl.getPDFExportAction(props.egfaId, props.moduleName, props.formData?.status);

  useEffect(() => {
    if (props.formData) {
      let affectedKey = 'affected';
      if (!props.formData?.influenceExpected) {
        affectedKey = 'notAffected';
      } else if (props.formData?.influenceExpected && !props.formData?.rvHoherErfuellungsaufwand) {
        affectedKey = 'notRelevantAffected';
      }
      setAffectedMessage(t(`egfa.modules.${props.moduleName}.zusammenfassung.${affectedKey}`));
    }
  }, [props.formData]);

  return (
    <ZusammenfassungWrapper
      egfaId={props.egfaId}
      prevPage={moduleNameRoute + '/' + routes.ZUSAMMENFASSUNG}
      onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
      moduleData={props.formData}
      moduleNameRoute={props.moduleName}
      additionalActions={PDFExportBtn}
    >
      <>
        <ZusammenfassungItem
          title={t(`egfa.modules.${props.moduleName}.fertigstellung.subtitle`)}
          bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.ZUSAMMENFASSUNG}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          egfaId={props.egfaId}
          allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
        >
          <div>
            <p>{affectedMessage}</p>
            {props.formData?.influenceExpected && props.formData?.rvHoherErfuellungsaufwand && (
              <p>{props.formData?.influenceSummary}</p>
            )}
          </div>
        </ZusammenfassungItem>
      </>
    </ZusammenfassungWrapper>
  );
}
