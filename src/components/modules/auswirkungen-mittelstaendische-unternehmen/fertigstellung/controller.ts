// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleKmuControllerApi,
  EgfaModuleKMUEntityDTOAllOf,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportKmuTestToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleKmuControllerApi>('egfaModuleKmuControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule4({ egfaId }).subscribe({
    next: (kmuData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Kmu,
          egfaDatenuebernahmeDTO: generatekmuJSON(kmuData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('KMU data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load KMU data', error);
    },
  });
};
export const generatekmuJSON = (
  kmuData: EgfaModuleKMUEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const kmuJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'kmuErgebnisdokumentation',
    title: i18n.t(`egfa.modules.kmu.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  let affectedKey = 'affected';
  if (!kmuData.influenceExpected) {
    affectedKey = 'notAffected';
  } else if (kmuData.influenceExpected && !kmuData.rvHoherErfuellungsaufwand) {
    affectedKey = 'notRelevantAffected';
  }
  kmuJSONExport.content.push({
    id: 'kmuAffectedText',
    type: EgfaItemType.Text,
    data: i18n.t(`egfa.modules.kmu.zusammenfassung.${affectedKey}`).toString(),
  });
  if (kmuData.influenceExpected && kmuData.rvHoherErfuellungsaufwand) {
    kmuJSONExport.content.push({
      id: 'kmuSummary',
      type: EgfaItemType.Text,
      data: kmuData.influenceSummary,
    });
  }
  return kmuJSONExport;
};
