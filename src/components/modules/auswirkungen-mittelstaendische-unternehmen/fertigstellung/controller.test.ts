// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { Observable, of, throwError } from 'rxjs';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaModuleKmuControllerApi,
  EgfaModuleKMUEntityResponseDTO,
  OperationOpts,
  SendEgfaDatenuebernahmeRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { exportKmuTestToEditor } from './controller';

describe(`Test: exportKmuTestToEditor`, () => {
  let egfaControllerStub: Sinon.SinonStub<
    [SendEgfaDatenuebernahmeRequest, (OperationOpts | undefined)?],
    Observable<void | AjaxResponse<void>>
  >;
  let egfaModuleControllerStub: sinon.SinonStub;
  let i18nStub: sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleKmuControllerApi.prototype, 'getEgfaModule4');

    i18nStub = Sinon.stub(i18n, 't').returns('');
  });
  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
  });
  it(`correct KmuTest data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleKMUEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
        rvHoherErfuellungsaufwand: true,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));

    // Call the function under test (exportKmuTestToEditor)
    let result: boolean;
    exportKmuTestToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result
      const x = egfaControllerStub?.getCall(0)?.args[0]?.egfaDatenuebernahmeDTO?.content?.length;
      expect(x).to.equal(2);
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct KmuTest data, influence not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleKMUEntityResponseDTO = {
      dto: {
        influenceExpected: false,
        influenceSummary: 'hello World',
        rvHoherErfuellungsaufwand: true,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));

    // Call the function under test (exportKmuTestToEditor)
    let result: boolean;
    exportKmuTestToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall(0)?.args[0]?.egfaDatenuebernahmeDTO?.content?.length;
      expect(x).to.equal(1);
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`correct KmuTest data, rv Hoher Erfuellungsaufwand  not expected`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleKMUEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
        rvHoherErfuellungsaufwand: false,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(of(void 0));

    // Call the function under test (exportKmuTestToEditor)
    let result: boolean;
    exportKmuTestToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall(0)?.args[0]?.egfaDatenuebernahmeDTO?.content?.length;
      expect(x).to.equal(1);
      expect(result).to.equal(true);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(` getting eGFA error`, function (done) {
    // Add the 'done' parameter to the test function

    egfaModuleControllerStub.returns(throwError(new Error('error getting eGFA')));
    egfaControllerStub.returns(of(void 0));

    // Call the function under test (exportKmuTestToEditor)
    let result: boolean;
    exportKmuTestToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      Sinon.assert.notCalled(egfaControllerStub);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
  it(`error on sending to editor`, function (done) {
    // Add the 'done' parameter to the test function

    const moduleData: EgfaModuleKMUEntityResponseDTO = {
      dto: {
        influenceExpected: true,
        influenceSummary: 'hello World',
        rvHoherErfuellungsaufwand: true,
      },
    };
    egfaModuleControllerStub.returns(of(moduleData));
    egfaControllerStub.returns(throwError(new Error('error getting eGFA')));

    // Call the function under test (exportKmuTestToEditor)
    let result: boolean;
    exportKmuTestToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      // Assert the result

      const x = egfaControllerStub?.getCall(0)?.args[0]?.egfaDatenuebernahmeDTO?.content?.length;
      expect(x).to.equal(2);
      expect(result).to.equal(false);

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
