// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleGleichwertigkeitControllerApi,
  EgfaModuleGleichwertigkeitEntityDTO,
  EgfaModuleKMUEntityDTO,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import {
  DemografieReferenzenTypes,
  DisabilityReferenzenTypes,
  EnapReferenzenTypes,
  GleichstellungReferenzenTypes,
  SonstigeKostenReferenzenTypes,
} from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { clearUnselectedFactors, getSelectedFactors } from './controller';
import { EinleitungGleichwertigkeitsCheck } from './einleitung/component.react';
import { FactorGleichwertigkeitsCheck } from './factor/component.react';
import { GleichwertigkeitsCheckFertigstellung } from './fertigstellung/component.react';
import { generateGleichwertigkeitsCheckJSON } from './fertigstellung/controller';
import { GleichwertigkeitsCheckZusammenfassung } from './zusammenfassung/component.react';

export function GleichwertigkeitsCheck(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [gleichwertigkeitsCheckData, SetGleichwertigkeitsCheckData] = useState<
    EgfaModuleKMUEntityDTO & ExtendedModuleData
  >();
  const egfaModuleGleichwertigkeitControllerApi = GlobalDI.get<EgfaModuleGleichwertigkeitControllerApi>(
    'egfaModuleGleichwertigkeitControllerApi',
  );
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaModuleGleichwertigkeitControllerApi.getEgfaModule5({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleGleichwertigkeitEntityDTO) => {
    const selectedFactors = getSelectedFactors(localData);
    localData = clearUnselectedFactors(localData) as EgfaModuleGleichwertigkeitEntityDTO;

    if (selectedFactors.length) {
      localData.influenceExpected = true;
    } else {
      localData.influenceSummary = undefined;
      localData.influenceExpected = false;
    }
    return egfaModuleGleichwertigkeitControllerApi.modifyEgfaModule5({
      egfaId: egfaId,
      egfaModuleGleichwertigkeitEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.GLEICHWERTIGKEITS_CHECK);
  }, []);

  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (gleichwertigkeitsCheckData && gleichwertigkeitsCheckData.referenzenData) {
      setReferenzenData(gleichwertigkeitsCheckData.referenzenData);
    }
  }, [gleichwertigkeitsCheckData]);

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string, isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ) => {
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: gleichwertigkeitsCheckData,
      onSave,
      handleFormChange,
    };
    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FERTIGSTELLUNG}`]}>
          <GleichwertigkeitsCheckFertigstellung {...contentPageProps} moduleName={routes.GLEICHWERTIGKEITS_CHECK} />
        </Route>
        {(gleichwertigkeitsCheckData?.aktionen?.includes(AktionType.Schreiben) === false ||
          gleichwertigkeitsCheckData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.GLEICHWERTIGKEITS_CHECK}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.FAKTOR_FINANZSITUATION}
            customContent={<EinleitungGleichwertigkeitsCheck egfaId={egfaId} />}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_FINANZSITUATION}`]}
        >
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_FINANZSITUATION}
            nextLinkName={routes.FAKTOR_WIRTSCHAFT}
            previousLinkName={routes.EINLEITUNG}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEaOehhReferenzen,
                    moduleName: routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitErfuellungsaufwandReferenzen,
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitSonstigeKostenReferenzen,
                    conditionsList: [SonstigeKostenReferenzenTypes.hasSelectedInfluenceExpected],
                    moduleName: routes.SONSTIGE_KOSTEN,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_WIRTSCHAFT}`]}
        >
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_WIRTSCHAFT}
            nextLinkName={routes.FAKTOR_MOBILITAET}
            previousLinkName={routes.FAKTOR_FINANZSITUATION}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg83,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg9,
                    ],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_MOBILITAET}`]}
        >
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_MOBILITAET}
            nextLinkName={routes.FAKTOR_DASEINSVORSORGE}
            previousLinkName={routes.FAKTOR_WIRTSCHAFT}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg91b,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg112,
                    ],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitGleichstellungReferenzen,
                    conditionsList: [GleichstellungReferenzenTypes.hasSelectedZeit],
                    extendedInfo: true,
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDisabilityReferenzen,
                    conditionsList: [
                      DisabilityReferenzenTypes.hasSelectedCheckboxHauptpruefung2,
                      DisabilityReferenzenTypes.hasSelectedCheckboxHauptpruefung3,
                    ],
                    extendedInfo: true,
                    moduleName: routes.DISABILITY,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_DASEINSVORSORGE}`]}
        >
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_DASEINSVORSORGE}
            nextLinkName={routes.FAKTOR_ENGAGEMENT}
            previousLinkName={routes.FAKTOR_MOBILITAET}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg42],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedCheckbox],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedGeld,
                      GleichstellungReferenzenTypes.hasSelectedZeit,
                      GleichstellungReferenzenTypes.hasSelectedGesundheit,
                    ],
                    extendedInfo: true,
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDisabilityReferenzen,
                    conditionsList: [
                      DisabilityReferenzenTypes.hasSelectedCheckboxHauptpruefung2,
                      DisabilityReferenzenTypes.hasSelectedCheckboxHauptpruefung3,
                    ],
                    extendedInfo: true,
                    moduleName: routes.DISABILITY,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_ENGAGEMENT}`]}
        >
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_ENGAGEMENT}
            nextLinkName={routes.FAKTOR_WOHNRAUM}
            previousLinkName={routes.FAKTOR_DASEINSVORSORGE}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedZusammenhalt],
                    extendedInfo: true,
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedZeit,
                      GleichstellungReferenzenTypes.hasSelectedEntscheidungsmacht,
                    ],
                    extendedInfo: true,
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDisabilityReferenzen,
                    conditionsList: [DisabilityReferenzenTypes.hasSelectedCheckboxHauptpruefung2],
                    extendedInfo: true,
                    moduleName: routes.DISABILITY,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_WOHNRAUM}`]}>
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_WOHNRAUM}
            nextLinkName={routes.FAKTOR_NATUR}
            previousLinkName={routes.FAKTOR_ENGAGEMENT}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg111,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg113,
                    ],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                  {
                    referenzenData: referenzenData?.gleichwertigkeitDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedAlter],
                    extendedInfo: true,
                    moduleName: routes.DEMOGRAFIE_CHECK,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.FAKTOR_NATUR}`]}>
          <FactorGleichwertigkeitsCheck
            {...contentPageProps}
            sectionName={routes.FAKTOR_NATUR}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            previousLinkName={routes.FAKTOR_WOHNRAUM}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.gleichwertigkeitEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg32,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg111,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg141,
                    ],
                    extendedInfo: true,
                    moduleName: routes.ENAP,
                  },
                ]}
              />
            }
          />
        </Route>

        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.ZUSAMMENFASSUNG}`]}>
          <GleichwertigkeitsCheckZusammenfassung
            {...contentPageProps}
            sectionName={routes.ZUSAMMENFASSUNG}
            nextLinkName={routes.FERTIGSTELLUNG}
            previousLinkName={routes.FAKTOR_NATUR}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.GLEICHWERTIGKEITS_CHECK}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={SetGleichwertigkeitsCheckData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      customResultPage={routes.FERTIGSTELLUNG}
      generateJson={generateGleichwertigkeitsCheckJSON}
    />
  );
}
