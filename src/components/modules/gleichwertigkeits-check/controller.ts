// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

interface FormDataInterface {
  [s: string]: any;
}

export const getSelectedFactors = (data: FormDataInterface): string[] => {
  return Object.keys(data).reduce((acc: string[], item: string): string[] => {
    const name = item.startsWith('faktor') && item.indexOf('Summary') === -1 ? item : '';
    const replacedName = name.replace('Secondary', '');
    if (name && data[name] && !acc.includes(replacedName)) {
      acc.push(replacedName);
    }
    return acc;
  }, []);
};
export const getFactorsBySummary = (data: FormDataInterface): string[] => {
  return Object.keys(data).reduce((acc: string[], item: string): string[] => {
    const name = item.indexOf('Summary') !== -1 && item.indexOf('influenceSummary') === -1 ? item : '';
    const replacedName = name.replace('Summary', '');
    if (name && data[name] && !acc.includes(replacedName)) {
      acc.push(replacedName);
    }
    return acc;
  }, []);
};

export const clearUnselectedFactors = (data: FormDataInterface): FormDataInterface => {
  getFactorsBySummary(data).forEach((summary) => {
    const isFactorSelected = getSelectedFactors(data).some((factor) => factor.indexOf(summary) !== -1);
    if (!isFactorSelected) {
      data[`${summary}Summary`] = undefined;
    }
  });

  return data;
};
