// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { clearUnselectedFactors, getFactorsBySummary, getSelectedFactors } from './controller';
const data = {
  egfaId: '4a6d1b3a-a9fa-42fa-b1bf-6486dcd78e5e',
  title: 'Gleichwertigkeits-Check',
  influenceExpected: false,
  deleted: false,
  influenceSummary: 'Summary',
  status: 'IN_BEARBEITUNG',
  typ: 'GLEICHWERTIGKEIT',
  faktorFinanzsituation: false,
  faktorFinanzsituationSummary: 'Finanzsituation summary',
  faktorWirtschaft: false,
  faktorWirtschaftSummary: null,
  faktorMobilitaetRegionen: false,
  faktorMobilitaetInfrastrukturen: false,
  faktorMobilitaetDienstleistungen: false,
  faktorMobilitaetSummary: null,
  faktorDaseinsvorsorge: false,
  faktorDaseinsvorsorgeSummary: null,
  faktorEngagementZusammenhalt: true,
  faktorEngagementTeilhabe: false,
  faktorEngagementEinbeziehung: false,
  faktorEngagementSummary: null,
  faktorWohnraumStruktur: false,
  faktorWohnraumInnenAussen: false,
  faktorWohnraumAttraktivitaet: false,
  faktorWohnraumSummary: null,
  faktorNaturMenschen: false,
  faktorNaturTiere: false,
  faktorNaturKlima: false,
  faktorNaturFlaeche: true,
  faktorNaturQualitaet: false,
  faktorNaturSummary: 'ioioio',
};

describe(`Test: getFactorsBySummary`, () => {
  it(`return empty`, () => {
    const result = getFactorsBySummary(data);
    expect(result).to.eql(['faktorFinanzsituation', 'faktorNatur']);
  });
});

describe(`Test: getSelectedFactors`, () => {
  it(`return empty`, () => {
    const result = getSelectedFactors(data);
    expect(result).to.eql(['faktorEngagementZusammenhalt', 'faktorNaturFlaeche']);
  });
});

describe(`Test: clearUnselectedFactors`, () => {
  it(`return empty`, () => {
    const result = clearUnselectedFactors(data);
    expect(result.faktorFinanzsituationSummary).to.be.undefined;
  });
});
