// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './factor.less';

import { Checkbox, Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router-dom';

import { EgfaModuleGleichwertigkeitEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

interface FactorGleichwertigkeitsCheckProps<P> extends ContentPageProps<P> {
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}

interface FormDataInterface {
  [s: string]: any;
}
export function FactorGleichwertigkeitsCheck(
  props: FactorGleichwertigkeitsCheckProps<EgfaModuleGleichwertigkeitEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [answer, setAnswer] = useState<boolean>(false);
  const [isDirty, setIsDirty] = useState(false);
  const multipleAnswersExists = i18n.exists(`egfa.modules.gleichwertigkeit.${props.sectionName}.question.radios`);
  const radios = t(`egfa.modules.gleichwertigkeit.${props.sectionName}.question.radios`, { returnObjects: true }) as [
    { name: string; content: string; separate?: boolean },
  ];
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; step: string }>(
    '/egfa/:id/module/:moduleName/:step',
  );
  const step = routeMatcherVorbereitung?.params?.step != null ? routeMatcherVorbereitung?.params?.step : '';

  useEffect(() => {
    if (props.formData) {
      const formData: FormDataInterface = props.formData;
      form.setFieldsValue(props.formData);
      setAnswer(formData[props.sectionName] as boolean);
      if (multipleAnswersExists) {
        setSummaryVisibleForMultipleAnswers();
      }
      setIsDirty(false);
    }
  }, [props.formData, step]);

  const setSummaryVisibleForMultipleAnswers = () => {
    const values = Object.values(
      form.getFieldsValue(
        radios?.map((radio) => {
          return radio.name;
        }),
      ) as boolean[],
    );
    setAnswer(values.includes(true));
  };

  return (
    <FormWrapper
      projectName="eGFA"
      key={`gw-check-page-${props.sectionName}`}
      title={<Title level={1}>{t(`egfa.modules.gleichwertigkeit.${props.sectionName}.title`)}</Title>}
      previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/gleichwertigkeit/${props.previousLinkName as string}`}
      nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/gleichwertigkeit/${props.nextLinkName as string}`}
      saveDraft={props.onSave(form)}
      isDirty={() => isDirty}
      form={form}
      formInitialValue={{}}
      setFormInstance={props.setFormInstance}
      showMandatoryFieldInfo={true}
      handleFormChanges={() => {
        setIsDirty(true);
        props.handleFormChange?.();
      }}
    >
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.gleichwertigkeit.${props.sectionName}.text`),
        }}
      ></div>
      <EgfaInfoCollapse
        panelKey="1"
        collapseKey={`more-info-collapse-${step}`}
        headerText={t(`egfa.modules.gleichwertigkeit.${props.sectionName}.additionalInfoTitle`)}
        text={t(`egfa.modules.gleichwertigkeit.${props.sectionName}.additionalInfo`)}
      />

      {multipleAnswersExists && (
        <>
          <fieldset className="fieldset-form-items">
            <legend
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.gleichwertigkeit.${props.sectionName}.question.label`),
              }}
            ></legend>
            <ul className="factors-checkboxes">
              {radios.map((radio) => {
                return (
                  <li key={crypto.randomUUID()}>
                    <Form.Item
                      style={{ marginTop: radio.separate ? '50px' : 'initial' }}
                      name={radio.name}
                      valuePropName="checked"
                    >
                      <Checkbox
                        onChange={() => setSummaryVisibleForMultipleAnswers()}
                        id={`egfa-gleichwertigkeit-${props.sectionName}-${radio.name}`}
                      >
                        {radio.content}
                      </Checkbox>
                    </Form.Item>
                  </li>
                );
              })}
            </ul>
          </fieldset>
        </>
      )}

      {answer && (
        <>
          <FormItemWithInfo
            name={`${props.sectionName}Summary`}
            label={
              <span>
                {t(`egfa.modules.gleichwertigkeit.${props.sectionName}.summary.label`)}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <InfoComponent
                    withLabelRequired
                    titleWithoutPrefix={true}
                    title={
                      t('egfa.modules.gleichwertigkeit.auswirkungenDrawer.title') +
                      t(`egfa.modules.gleichwertigkeit.${props.sectionName}.summary.label`)
                    }
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html:
                          t('egfa.modules.gleichwertigkeit.auswirkungenDrawer.intro') +
                          t(`egfa.modules.gleichwertigkeit.${props.sectionName}.summary.text`),
                      }}
                    />
                  </InfoComponent>
                </span>
              </span>
            }
            rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
          >
            <TextArea rows={8} />
          </FormItemWithInfo>
        </>
      )}
      {props.referenzen}
    </FormWrapper>
  );
}
