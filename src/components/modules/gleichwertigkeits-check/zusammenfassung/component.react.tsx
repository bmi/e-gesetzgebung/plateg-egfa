// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { EgfaModuleGleichwertigkeitEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { getFactorsBySummary } from '../controller';

interface FactorGleichwertigkeitsCheckProps<P> extends ContentPageProps<P> {
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateNextLink?: (itemName: string, isDisabled: boolean) => void;
  handleFormChange?: () => void;
}

export interface FormDataInterface {
  [s: string]: any;
}
export function GleichwertigkeitsCheckZusammenfassung(
  props: FactorGleichwertigkeitsCheckProps<EgfaModuleGleichwertigkeitEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [selectedFactors, setSelectedFactors] = useState<string[]>([]);
  const [formData, setFormData] = useState<FormDataInterface>();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    if (props.formData) {
      const data: FormDataInterface = props.formData;
      form.setFieldsValue(data);

      setFormData(data);
      setSelectedFactors(getFactorsBySummary(data));
      setIsDirty(false);
    }
  }, [props.formData]);

  return (
    <FormWrapper
      projectName="eGFA"
      title={<Title level={1}>{t(`egfa.modules.gleichwertigkeit.${props.sectionName}.title`)}</Title>}
      previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/gleichwertigkeit/${props.previousLinkName as string}`}
      nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/gleichwertigkeit/${props.nextLinkName as string}`}
      saveDraft={props.onSave(form)}
      isDirty={() => isDirty}
      form={form}
      formInitialValue={{}}
      setFormInstance={props.setFormInstance}
      showMandatoryFieldInfo={!!selectedFactors.length}
      handleFormChanges={() => {
        setIsDirty(true);
        props.handleFormChange?.();
      }}
    >
      {!selectedFactors.length && <p>{t(`egfa.modules.gleichwertigkeit.fertigstellung.noImpact`)}</p>}

      {selectedFactors.map((item, index) => {
        return (
          <div key={index} className="faktor-answer">
            <div className="faktor-holder">
              <strong>{t(`egfa.modules.gleichwertigkeit.${item}.title`)}:</strong>
              <Link className="blue-text-button" to={`/egfa/${props.egfaId}/${routes.MODULE}/gleichwertigkeit/${item}`}>
                {t(`egfa.modules.enap.zusammenfassung.unfinishedSdgs.zurBearbeitungBtn`)}
              </Link>
            </div>
            {formData && formData[`${item}Summary`]}
          </div>
        );
      })}

      {selectedFactors.length > 0 && (
        <Form.Item
          name="influenceSummary"
          label={t(`egfa.modules.gleichwertigkeit.zusammenfassung.summary`)}
          rules={[{ required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') }]}
        >
          <TextArea id="egfa-gleichwertigkeitAbschliessendeBewertung-txtArea" rows={8} />
        </Form.Item>
      )}
    </FormWrapper>
  );
}
