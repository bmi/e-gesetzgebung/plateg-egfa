// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  BASE_PATH,
  EgfaControllerApi,
  EgfaModuleTableDTO,
  EgfaModuleType,
  EgfaWithModulesTableResponseDTO,
} from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { computeLink } from '../../../egfa/main/modules-list/controller';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';

interface EinleitungGleichwertigkeitsCheckProps {
  egfaId: string;
}

export function EinleitungGleichwertigkeitsCheck(props: EinleitungGleichwertigkeitsCheckProps): React.ReactElement {
  const { t } = useTranslation();
  const [egfa, setEgfa] = useState<EgfaWithModulesTableResponseDTO>();
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.get<ErrorController>('errorController');

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const sub = egfaController.getEgfaWithModules({ id: props.egfaId }).subscribe({
      next: (data) => {
        setEgfa(data);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => {
      sub.unsubscribe();
    };
  }, []);

  const getRelatedModuleLink = (moduleType: EgfaModuleType): string | undefined => {
    const egfaModuleBaseURL = `#/egfa/${props.egfaId}/${routes.MODULE}`;
    const module = egfa?.modules?.find((elem: EgfaModuleTableDTO) => {
      return elem.typ === moduleType;
    });
    if (!module?.title) {
      return;
    }
    return `${egfaModuleBaseURL}/${computeLink(module?.title || '', module?.status, !module.aktionen?.includes(AktionType.Schreiben))}`;
  };

  return (
    <>
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.gleichwertigkeit.einleitung.text1`, {
            basePath: BASE_PATH,
          }),
        }}
      />
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`egfa.modules.gleichwertigkeit.einleitung.additionalInfoTitle`)}
        text={t(`egfa.modules.gleichwertigkeit.einleitung.text2`)}
      />
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.gleichwertigkeit.einleitung.text3`, {
            demoCheckLink: getRelatedModuleLink(EgfaModuleType.Demografie),
            enapLink: getRelatedModuleLink(EgfaModuleType.Enap),
            gfaLink: getRelatedModuleLink(EgfaModuleType.Gleichstellung),
            disabilityLink: getRelatedModuleLink(EgfaModuleType.Disability),
          }),
        }}
      />
    </>
  );
}
