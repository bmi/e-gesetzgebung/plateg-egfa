// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleGleichwertigkeitControllerApi,
  EgfaModuleGleichwertigkeitEntityDTOAllOf,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportGleichwertigkeitsCheckToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleGleichwertigkeitControllerApi>(
    'egfaModuleGleichwertigkeitControllerApi',
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule5({ egfaId }).subscribe({
    next: (gleichwertigkeitsCheckData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Gleichwertigkeit,
          egfaDatenuebernahmeDTO: generateGleichwertigkeitsCheckJSON(gleichwertigkeitsCheckData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Gleichwertigkeits-Check data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Gleichwertigkeits-Check data', error);
    },
  });
};
export const generateGleichwertigkeitsCheckJSON = (
  gleichwertigkeitsCheckData: EgfaModuleEntityRequestDTO & EgfaModuleGleichwertigkeitEntityDTOAllOf,
): EgfaDatenuebernahmeDTO => {
  const gleichwertigkeitsCheckJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'gleichwertigkeitsCheckErgebnisdokumentation',
    title: i18n.t(`egfa.modules.gleichwertigkeit.name`),
    content: [],
    children: [],
  };
  if (gleichwertigkeitsCheckData.influenceExpected && gleichwertigkeitsCheckData.influenceSummary) {
    gleichwertigkeitsCheckJSONExport.content.push({
      id: 'gleichwertigkeitsCheckSummary',
      type: EgfaItemType.Text,
      data: gleichwertigkeitsCheckData.influenceSummary,
    });
  } else {
    gleichwertigkeitsCheckJSONExport.content.push({
      id: 'gleichwertigkeitsCheckSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.gleichwertigkeit.fertigstellung.noImpact`).toString(),
    });
  }
  return gleichwertigkeitsCheckJSONExport;
};
