// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleGleichwertigkeitEntityDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ModulesListController } from '../../../egfa/main/modules-list/controller';
import { SimpleModuleProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';
import { getFactorsBySummary } from '../controller';
import { FormDataInterface } from '../zusammenfassung/component.react';

export function GleichwertigkeitsCheckFertigstellung(
  props: SimpleModuleProps<EgfaModuleGleichwertigkeitEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const moduleNameRoute: string = routes.GLEICHWERTIGKEITS_CHECK;
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const PDFExportBtn = ctrl.getPDFExportAction(
    props.formData?.egfaId as string,
    props.moduleName,
    props.formData?.status,
  );
  const [saveBtnDisabled, setSaveBtnDisabled] = useState<boolean>(false);
  useEffect(() => {
    if (props.formData) {
      const data: FormDataInterface = props.formData;

      const factors = getFactorsBySummary(data);
      if (!data['influenceSummary'] && factors.length) {
        setSaveBtnDisabled(true);
      } else {
        setSaveBtnDisabled(false);
      }
    }
  }, [props.formData]);
  return (
    <ZusammenfassungWrapper
      saveBtnDisabled={saveBtnDisabled}
      egfaId={props.egfaId}
      prevPage={moduleNameRoute + '/' + routes.ZUSAMMENFASSUNG}
      onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
      moduleData={props.formData}
      moduleNameRoute={props.moduleName}
      additionalActions={PDFExportBtn}
    >
      <>
        <ZusammenfassungItem
          title={t(`egfa.modules.${props.moduleName}.fertigstellung.subtitle`)}
          bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${moduleNameRoute}/${routes.ZUSAMMENFASSUNG}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          egfaId={props.egfaId}
          allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
        >
          <>
            <div>
              {props.formData?.influenceSummary ? (
                <>
                  <Title level={3}>{t(`egfa.modules.gleichwertigkeit.fertigstellung.impact`)}</Title>
                  <p>{props.formData?.influenceSummary}</p>
                </>
              ) : (
                t(`egfa.modules.gleichwertigkeit.fertigstellung.noImpact`)
              )}
            </div>
          </>
        </ZusammenfassungItem>
      </>
    </ZusammenfassungWrapper>
  );
}
