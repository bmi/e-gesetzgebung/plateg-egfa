// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { routes } from '../../../../shares/routes';
import { getNavBarsItems } from './controller';

describe(`Test: getNavbaritems`, () => {
  it(`Simple module nav`, () => {
    const result = getNavBarsItems('Hello World');
    expect(result[0]?.menuKeys).has.length(3);
  });
  it(`KMU module nav`, () => {
    const result = getNavBarsItems(routes.MITTELSTAENDISCHE_UNTERNEHMEN);
    expect(result[0]?.menuKeys).has.length(6);
  });
  it(`GleichwertigkeitsCheck module nav`, () => {
    const result = getNavBarsItems(routes.GLEICHWERTIGKEITS_CHECK);
    expect(result[0]?.menuKeys).has.length(10);
  });
  it(`AuswirkungenEinnahmenAusgaben module nav`, () => {
    const result = getNavBarsItems(routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN);
    expect(result[0]?.menuKeys).has.length(10);
  });
  it(`demografieCheck module nav`, () => {
    const result = getNavBarsItems(routes.DEMOGRAFIE_CHECK);
    expect(result[0]?.menuKeys).has.length(9);
  });
  it(`gleichstellung module nav`, () => {
    const result = getNavBarsItems(routes.GLEICHSTELLUNG);
    expect(result[0]?.menuKeys).has.length(7);
  });
  it(`erfuellungsaufwand module nav`, () => {
    const result = getNavBarsItems(routes.ERFUELLUNGSAUFWAND);
    expect(result[0]?.menuKeys).has.length(7);
  });
  it(`evaluierung module nav`, () => {
    const result = getNavBarsItems(routes.EVALUIERUNG);
    expect(result[0]?.menuKeys).has.length(6);
  });

  it(`EXPERIMENTIERKLAUSELN module nav`, () => {
    const result = getNavBarsItems(routes.EXPERIMENTIERKLAUSELN);
    expect(result[0]?.menuKeys).has.length(9);
  });
});
