// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { NavBarItem } from '@plateg/theme';

import { routes } from '../../../../shares/routes';

export const navBarItems: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'wesentlicheAuswirkungen', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItems2: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'relevanzpruefung', isDisabled: false },
      { key: 'besondereBelastungen', isDisabled: true },
      { key: 'regelungsalternativen', isDisabled: true },
      { key: routes.ZUSAMMENFASSUNG, isDisabled: false },
      { key: routes.FERTIGSTELLUNG, isDisabled: false },
    ],
  },
];

export const navBarItemsgleichwertigkeitsCheck: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'faktorFinanzsituation', isDisabled: false },
      { key: 'faktorWirtschaft', isDisabled: false },
      { key: 'faktorMobilitaet', isDisabled: false },
      { key: 'faktorDaseinsvorsorge', isDisabled: false },
      { key: 'faktorEngagement', isDisabled: false },
      { key: 'faktorWohnraum', isDisabled: false },
      { key: 'faktorNatur', isDisabled: false },
      { key: routes.ZUSAMMENFASSUNG, isDisabled: false },
      { key: routes.FERTIGSTELLUNG, isDisabled: false },
    ],
  },
];

export const navBarItemsAuswirkungenEinnahmenAusgaben: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: routes.ZWECKAUSGABEN, isDisabled: false },
      { key: routes.VOLLZUGSAUFWAND, isDisabled: false },
      { key: routes.NORMADRESSAT, isDisabled: false },
      { key: routes.VERWALTUNGSUNDSONSTIGE, isDisabled: false },
      { key: routes.AENDERUNGENSTEUEREINNAHMEN, isDisabled: false },
      { key: routes.SONSTIGERBEREICHESTAATES, isDisabled: false },
      { key: routes.KOMPENSATIONKOSTEN, isDisabled: false },
      { key: routes.ZUSAMMENFASSUNG, isDisabled: false },
      { key: routes.FERTIGSTELLUNG, isDisabled: true },
    ],
  },
];

export const navBarItems3: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'demografischeEntwicklung', isDisabled: false },
      { key: 'familieKinderJugendliche', isDisabled: false },
      { key: 'arbeitBerufWirtschaft', isDisabled: false },
      { key: 'alterWohnenPflege', isDisabled: false },
      { key: 'regionen', isDisabled: false },
      { key: 'generationen', isDisabled: false },
      { key: 'zusammenfassung', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsSonstigeKosten: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'pruefungSonstigeKosten', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsEnap: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'sdg1', isDisabled: false },
      { key: 'sdg2', isDisabled: false },
      { key: 'sdg3', isDisabled: false },
      { key: 'sdg4', isDisabled: false },
      { key: 'sdg5', isDisabled: false },
      { key: 'sdg6', isDisabled: false },
      { key: 'sdg7', isDisabled: false },
      { key: 'sdg8', isDisabled: false },
      { key: 'sdg9', isDisabled: false },
      { key: 'sdg10', isDisabled: false },
      { key: 'sdg11', isDisabled: false },
      { key: 'sdg12', isDisabled: false },
      { key: 'sdg13', isDisabled: false },
      { key: 'sdg14', isDisabled: false },
      { key: 'sdg15', isDisabled: false },
      { key: 'sdg16', isDisabled: false },
      { key: 'sdg17', isDisabled: false },
      { key: 'prinzipien', isDisabled: false },
      { key: 'zusammenfassung', isDisabled: false },
      { key: 'fertigstellung', isDisabled: true },
    ],
  },
];

export const navBarItemsDisability: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'relevanzpruefung', isDisabled: false },
      { key: 'hauptpruefung', isDisabled: true },
      { key: 'folgenabschaetzung', isDisabled: true },
      { key: 'zusammenfassung', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsGleichstellung: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'relevanzpruefung1', isDisabled: false },
      { key: 'relevanzpruefung2', isDisabled: false },
      {
        submenuEntry: {
          key: 'hauptpruefung',
          isDisabled: true,
          isLink: true,
        },
        menuKeys: [
          { key: 'hauptpruefung.arbeit', isDisabled: true },
          { key: 'hauptpruefung.geld', isDisabled: true },
          { key: 'hauptpruefung.wissen', isDisabled: true },
          { key: 'hauptpruefung.zeit', isDisabled: true },
          { key: 'hauptpruefung.entscheidungsmacht', isDisabled: true },
          { key: 'hauptpruefung.gesundheit', isDisabled: true },
          { key: 'hauptpruefung.gewalt', isDisabled: true },
          { key: 'hauptpruefung.auswirkungen', isDisabled: true },
        ],
      },
      { key: 'geschlechtergerechtigkeit', isDisabled: true },
      { key: 'gleichstellungZusammenfassung', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsErfuellungsaufwand: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      { key: 'gesamtsummenNormadressaten', isDisabled: false },
      {
        submenuEntry: {
          key: 'buerger',
          isDisabled: true,
          isLink: false,
        },
        menuKeys: [],
      },
      {
        submenuEntry: {
          key: 'wirtschaft',
          isDisabled: true,
          isLink: false,
        },
        menuKeys: [],
      },
      {
        submenuEntry: {
          key: 'verwaltung',
          isDisabled: true,
          isLink: false,
        },
        menuKeys: [],
      },
      {
        submenuEntry: {
          key: 'zusammenfassung',
          isDisabled: false,
          isLink: false,
        },
        menuKeys: [
          { key: 'zusammenfassung.algemeineangaben', isDisabled: false },
          { key: 'zusammenfassung.begruendungsteil', isDisabled: false },
        ],
      },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsEvaluierung: NavBarItem[] = [
  {
    menuKeys: [
      { key: 'einleitung', isDisabled: false },
      {
        submenuEntry: {
          key: 'pruefungErforderlichkeit',
          isDisabled: false,
          isLink: true,
        },
        menuKeys: [
          { key: 'pruefungErforderlichkeit.schwellenWerte', isDisabled: false },
          { key: 'pruefungErforderlichkeit.weitereEvaluierungsanlaesse', isDisabled: false },
        ],
      },
      { key: 'gruendeFuerVerzicht', isDisabled: false },
      {
        submenuEntry: {
          key: 'ausgestaltungDerEvaluierung',
          isDisabled: false,
          isLink: true,
        },
        menuKeys: [
          { key: 'ausgestaltungDerEvaluierung.was', isDisabled: false },
          { key: 'ausgestaltungDerEvaluierung.wie', isDisabled: false },
          { key: 'ausgestaltungDerEvaluierung.wann', isDisabled: false },
        ],
      },
      { key: 'zusammenfassung', isDisabled: false },
      { key: 'fertigstellung', isDisabled: false },
    ],
  },
];

export const navBarItemsExperimentierklauseln: NavBarItem[] = [
  {
    menuKeys: [
      { key: routes.INTRO, isDisabled: false },
      { key: routes.EINLEITUNG, isDisabled: false },
      { key: routes.STANDORT_UND_TITEL, isDisabled: false },
      {
        submenuEntry: {
          key: routes.ABSATZ1,
          isDisabled: false,
          isLink: false,
        },
        menuKeys: [{ key: `${routes.ABSATZ1}.${routes.ERPROBUNGSZWECK}`, isDisabled: false }],
      },
      {
        submenuEntry: {
          key: routes.ABSATZ2,
          isDisabled: false,
          isLink: false,
        },
        menuKeys: [
          { key: `${routes.ABSATZ2}.${routes.ZUSTAENDIGKEIT}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.ERMAECHTIGUNGBEHOERDE}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.ENTSCHEIDUNGSINHALT}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.ERPROBUNGSGEGENSTAND}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.MATERIELLEBEGRENZUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.VERFAHRENSVORGABEN}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.UMFANGERPROBUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.BEGLEITENDEPFLICHTEN}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.BEFRISTUNGERPROBUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.VERLAENGERUNGSMOEGLICHKEIT}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.VERHAELTNISAUFSCHIEBENDEWIRKUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.WEITERENEBENBESTIMMUNGEN}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.AENDERUNGREALLABORGESTALTUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.MOEGLICHKEITAUFHEBUNG}`, isDisabled: false },
          { key: `${routes.ABSATZ2}.${routes.ABSATZZWEIZUSAMMENFASSUNG}`, isDisabled: false },
        ],
      },
      {
        submenuEntry: {
          key: routes.ABSATZ3,
          isDisabled: false,
          isLink: false,
        },
        menuKeys: [
          { key: `${routes.ABSATZ3}.${routes.EVALUATIONUNDTRANSFER}`, isDisabled: false },
          { key: `${routes.ABSATZ3}.${routes.BEFRISTUNGEXPERIMENTIERKLAUSEL}`, isDisabled: false },
          { key: `${routes.ABSATZ3}.${routes.ABSATZDREIZUSAMMENFASSUNG}`, isDisabled: false },
        ],
      },
      {
        submenuEntry: {
          key: routes.ABSATZ4,
          isDisabled: false,
          isLink: false,
        },
        menuKeys: [{ key: `${routes.ABSATZ4}.${routes.VERORDNUNGSERMAECHTIGUNG}`, isDisabled: false }],
      },
      { key: `${routes.ZUSAMMENFASSUNG}`, isDisabled: false },
      { key: `${routes.FERTIGSTELLUNG}`, isDisabled: false },
    ],
  },
];

export const getNavBarsItems = (moduleName: string): NavBarItem[] => {
  switch (moduleName) {
    case routes.MITTELSTAENDISCHE_UNTERNEHMEN:
      return navBarItems2;
    case routes.GLEICHWERTIGKEITS_CHECK:
      return navBarItemsgleichwertigkeitsCheck;
    case routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN:
      return navBarItemsAuswirkungenEinnahmenAusgaben;
    case routes.DEMOGRAFIE_CHECK:
      return navBarItems3;
    case routes.DISABILITY:
      return navBarItemsDisability;
    case routes.GLEICHSTELLUNG:
      return navBarItemsGleichstellung;
    case routes.ENAP:
      return navBarItemsEnap;
    case routes.ERFUELLUNGSAUFWAND:
      return navBarItemsErfuellungsaufwand;
    case routes.EVALUIERUNG:
      return navBarItemsEvaluierung;
    case routes.SONSTIGE_KOSTEN:
      return navBarItemsSonstigeKosten;
    case routes.EXPERIMENTIERKLAUSELN:
      return navBarItemsExperimentierklauseln;
    default:
      return navBarItems;
  }
};
