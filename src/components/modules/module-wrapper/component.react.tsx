// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Affix, Col, Layout, Row, Spin } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useRef, useState } from 'react';
import { Autosave } from 'react-autosave';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import { Observable, Subscription } from 'rxjs';

import {
  AktionType,
  EgfaDatenuebernahmeDTO,
  EgfaModuleEntityRequestDTO,
  EgfaModuleStatusType,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  UserEntityResponseDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import {
  HeaderController,
  LoadingStatusController,
  MenuCollapseController,
  MenuItem,
  NavbarComponent,
  NavBarItem,
  SiderWrapper,
} from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { ContinueLaterComponent } from '../../../shares/continue-later/component.react';
import { routes } from '../../../shares/routes';
import { computeeGFAType, ModulesListController } from '../../egfa/main/modules-list/controller';
import { FreigabeModalEgfa } from '../../egfa/main/modules-list/freigabe-modal/component.react';
import { EgfaWithType } from '../../egfa/main/modules-list/freigabe-modal/controller';
import { PreparedItemsInterface } from '../erfuellungsaufwand/controller';
import { EGFAModuleController, ModifyModuleDtoResponse } from '../general/controller';
import { ModulesHeader } from './header/component.react';
import { getNavBarsItems } from './nav/controller';

type UpdateItemDisableStatusHelperArgType = {
  newItem: NavBarItem | MenuItem;
  itemsNameList:
    | string[]
    | {
        name: string;
        isDisabled: boolean;
      }[];
  hasSubMenu: boolean;
  isDisabled: boolean | undefined;
};

interface ModuleWrapperProps<T, P extends ModifyModuleDtoResponse<T>> {
  moduleIdentifier: string;
  egfaId: string;
  form?: FormInstance;
  getData: () => Observable<P>;
  putData: (data: T) => Observable<P>;
  generateJson: (data: T) => EgfaDatenuebernahmeDTO;
  setFormData: (data: T) => void;
  pageRouting: (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ) => React.ReactElement;
  pageName?: string;
  customContactDrawer?: React.ReactElement;
  customResultPage?: string;
  headerRightItems?: React.ReactElement[];
  isLight?: boolean;
  pruefenName?: string;
  isFullscreen?: boolean;
  isStaticPage?: boolean;
}

const AUTOSAVE_INTERVAL = 2 * 60 * 1000; // 2 minutes autosave interval

export function ModuleWrapper<T extends EgfaModuleEntityRequestDTO, P extends ModifyModuleDtoResponse<T>>(
  props: ModuleWrapperProps<T, P>,
): React.ReactElement {
  const { t } = useTranslation();
  const moduleName = t(`egfa.modules.${props.moduleIdentifier}.name`);
  const { Header, Content } = Layout;
  const [rvFull, setRvFull] = useState<RegelungsvorhabenEntityWithDetailsResponseDTO>();
  const [isModuleClosed, setIsModuleClosed] = useState<boolean>(false);
  const [isRVArchived, setIsRVArchived] = useState<boolean>(false);
  const [isAllowedToModify, setIsAllowedToModify] = useState<boolean | undefined>(true);
  const [sideMenu, setSideMenu] = useState<NavBarItem[]>(getNavBarsItems(props.moduleIdentifier));
  const [isEGFAArchived, setIsEGFAArchived] = useState<boolean>(false);
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const menuCollapseController = new MenuCollapseController(isCollapsed, setIsCollapsed);
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);
  const [menuOffsetTop, setMenuOffsetTop] = useState<number>(0);
  const [hasSaveBtn, setHasSaveBtn] = useState<boolean>(false);
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);
  const [selectedEgfa, setSelectedEgfa] = useState<EgfaWithType | undefined>();
  const [egfaErst, setEgfaErst] = useState<UserEntityResponseDTO>();
  const shouldUpdate = useRef(false);
  const ctrlEGFA = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  // As sideMenu async sate we have to use local variable to be sure what we have the latest state of side menu
  // it is required for methods updateItemDisableStatus and updateMenuItems which passed to pageRouting
  let localSideMenu: NavBarItem[] = getNavBarsItems(props.moduleIdentifier);

  // Used to avoid memory leak in controller (this component's state is only updated in
  // the asynchronous backend call if this component did not already unmount)
  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    let sub: Subscription;
    if (!props.isLight) {
      sub = ctrlEGFA.getEGFAItemCall(props.egfaId).subscribe({
        next: (data) => {
          loadingStatusController.setLoadingStatus(false);
          setIsEGFAArchived(data.isArchived);
          setSelectedEgfa({
            egfa: data,
            type: computeeGFAType(moduleName),
            rolleTyp: data.modules.find((module) => module.name === moduleName)?.rolleTyp,
            moduleId: data.modules.find((module) => module.name === moduleName)?.id as string,
            aktionen: data.modules.find((module) => module.name === moduleName)?.aktionen || [],
          });
          setEgfaErst(data.ersteller);
        },
        error: (error) => {
          loadingStatusController.setLoadingStatus(false);
          console.error(error);
        },
      });
    }
    shouldUpdate.current = true;
    return () => {
      shouldUpdate.current = false;
      if (!props.isLight) {
        sub.unsubscribe();
      }
    };
  }, []);

  useEffect(() => {
    setIsRVArchived(rvFull?.dto.status === VorhabenStatusType.Archiviert || isEGFAArchived);
  }, [rvFull, isEGFAArchived]);

  // helper func to reduce complexity
  const setMenuItemHelper = (item: NavBarItem | MenuItem, result: string) => {
    const newItem = { ...item }; // clone item
    if (item.key !== result) {
      newItem.isDisabled = isModuleClosed || isRVArchived || !isAllowedToModify;
    }
    if ((item as NavBarItem).submenuEntry) {
      // Fix for wrong enabling/disabling submenus. Use deep clone of submenuEntry
      (newItem as NavBarItem).submenuEntry = { ...(newItem as NavBarItem).submenuEntry };
      (newItem as NavBarItem).submenuEntry.isDisabled = isModuleClosed || isRVArchived || !isAllowedToModify;
    }
    return newItem;
  };

  useEffect(() => {
    // Enable or disable all nav bar items except result page
    if (isModuleClosed || isRVArchived || !isAllowedToModify) {
      const resultPageKey = props.customResultPage ?? 'zusammenfassung';
      localSideMenu = localSideMenu.map((navBarItem) => {
        const result = navBarItem.menuKeys.find((item) => {
          return item.key === resultPageKey;
        })
          ? resultPageKey
          : 'fertigstellung';
        return {
          menuKeys: navBarItem.menuKeys.map((item) => setMenuItemHelper(item, result)),
        };
      });
      setSideMenu(localSideMenu);
    } else {
      // Set initial state of the side menu
      localSideMenu = getNavBarsItems(props.moduleIdentifier);
      setSideMenu(localSideMenu);
    }
  }, [isModuleClosed, isRVArchived, isAllowedToModify]);

  const egfaModuleController = new EGFAModuleController(
    props.getData,
    props.putData,
    (data) => {
      props.setFormData(data);
      props.isLight
        ? setIsAllowedToModify(data.allowedToModify)
        : setIsAllowedToModify(data.aktionen?.includes(AktionType.Schreiben));
    },
    setRvFull,
    setIsModuleClosed,
    shouldUpdate,
    props.generateJson,
    props.isLight,
  );

  useEffect(() => {
    const sub = egfaModuleController.getInitialData();
    return () => {
      sub.unsubscribe();
    };
  }, [props.egfaId]);

  useEffect(() => {
    headerController.setHeaderProps({
      setMenuOffset: setMenuOffsetTop,
    });
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error) => {
        console.log('Error sub', error);
      },
    });
    loadingStatusController.initLoadingStatus();
    return () => subs.unsubscribe();
  }, []);

  useEffect(() => {
    menuCollapseController.registerListener();
  }, []);
  useEffect(() => {
    menuCollapseController.updateButtonPosition();
  });
  useEffect(() => {
    menuCollapseController.configureCollapseButton();
  }, [isCollapsed]);

  useEffect(() => {
    const saveForbiddeen = [routes.EINLEITUNG, routes.FERTIGSTELLUNG, routes.INTRO];
    if (
      props.moduleIdentifier !== routes.ENAP &&
      props.moduleIdentifier !== routes.DISABILITY &&
      props.moduleIdentifier !== routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN &&
      props.moduleIdentifier !== routes.ERFUELLUNGSAUFWAND &&
      props.moduleIdentifier !== routes.EXPERIMENTIERKLAUSELN
    ) {
      saveForbiddeen.push(routes.ZUSAMMENFASSUNG);
    }
    setHasSaveBtn(!saveForbiddeen.includes(props.pageName || ''));
    setIsSaveButtonDisabled(true);
  }, [props.pageName]);

  const [isSaveButtonDisabled, setIsSaveButtonDisabled] = useState<boolean>(true);
  const [formValueChanges, setFormValueChanges] = useState<string>(JSON.stringify(props.form?.getFieldsValue()));
  const handleFormChange = (): void => {
    // As we have trigger for handleFormChange also on page leave guard,
    // so we updating formValueChanges only when we have real fields changes,
    // to prevent wrong double API call
    if (!isSaveButtonDisabled) {
      setFormValueChanges(JSON.stringify(props.form?.getFieldsValue()));
    }
    setIsSaveButtonDisabled(false);
  };

  const customTranslation = (name: string, dynamicName?: string) => {
    return dynamicName ? dynamicName : t(`egfa.modules.${props.moduleIdentifier}.${name}.linkName`);
  };

  const contactPerson = {
    name: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.name`, ''),
    anrede: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.anrede`, ''),
    email: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.email`, ''),
    fachreferat: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.fachreferat`, ''),
    abteilung: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.abteilung`, ''),
    titel: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.titel`, ''),
    additionalInfo: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.additionalInfo`, ''),
    adresse: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.adresse`, ''),
    telefon: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.telefon`, ''),
    ressort: { kurzbezeichnung: t(`egfa.modules.${props.moduleIdentifier}.contactPerson.ressort`, '') },
  };

  // helper func to reduce complexity
  const updateItemDisableStatusHelper = (argument: UpdateItemDisableStatusHelperArgType) => {
    const { hasSubMenu, isDisabled, itemsNameList, newItem } = argument;
    if (
      itemsNameList.some((name) =>
        hasSubMenu ? (newItem as NavBarItem).submenuEntry?.key === name : (newItem as MenuItem).key === name,
      )
    ) {
      if (hasSubMenu) {
        (newItem as NavBarItem).submenuEntry.isDisabled = isDisabled;
        (newItem as NavBarItem).menuKeys.forEach((key) => {
          (key as MenuItem).isDisabled = isDisabled;
        });
      } else {
        (newItem as MenuItem).isDisabled = isDisabled;
      }
    }
  };

  const updateItemDisableStatus = (
    itemName: string | string[] | { name: string; isDisabled: boolean }[],
    isDisabled?: boolean,
  ): void => {
    localSideMenu = localSideMenu.map((navBarItem) => {
      return {
        menuKeys: navBarItem.menuKeys.map((item) => {
          const hasSubMenu = item.hasOwnProperty('submenuEntry');
          const newItem = { ...item }; // clone item
          const itemsNameList = typeof itemName === 'string' ? [itemName] : itemName;
          if (typeof itemsNameList?.[0] === 'string') {
            updateItemDisableStatusHelper({ hasSubMenu, isDisabled, itemsNameList, newItem });
          } else {
            const disabledStatus = (itemsNameList as { name: string; isDisabled: string }[]).find((entry) => {
              return entry.name === item.key;
            })?.isDisabled as boolean | undefined;
            if (disabledStatus !== undefined) {
              newItem.isDisabled = disabledStatus;
            }
          }
          return newItem;
        }),
      };
    });
    setSideMenu(localSideMenu);
  };

  const updateMenuItems = (preparedItems: PreparedItemsInterface, isModuleDisabled = true) => {
    localSideMenu = localSideMenu.map((navBarItem) => {
      return {
        menuKeys: navBarItem.menuKeys.map((item) => {
          const hasSubMenu = item.hasOwnProperty('submenuEntry');
          // clone item
          const newItem = { ...item };
          Object.keys(preparedItems).forEach((category) => {
            const categoryLowerCase = category.toLocaleLowerCase();
            if (hasSubMenu && (item as NavBarItem).submenuEntry?.key === categoryLowerCase) {
              newItem.menuKeys = preparedItems[category].map((categoryItem) => {
                return {
                  key: `${categoryLowerCase}.${categoryItem.id as string}`,
                  isDisabled: false,
                  dynamicName: categoryItem.bezeichnung,
                };
              });
              // Fix for wrong enabling/disabling submenus. Use deep clone of submenuEntry
              (newItem as NavBarItem).submenuEntry = { ...(newItem as NavBarItem).submenuEntry };
              (newItem as NavBarItem).submenuEntry.isDisabled = isModuleDisabled;
            }
          });

          return newItem;
        }),
      };
    });
    setSideMenu(localSideMenu);
  };

  const [columnSettings, setColumnSettings] = useState({
    xs: { span: 22, offset: 1 },
    md: { span: 14, offset: 2 },
    lg: { span: 16, offset: 3 },
    xl: { span: 12, offset: 3 },
    xxl: { span: 10, offset: 4 },
  });

  useEffect(() => {
    if (props.isFullscreen) {
      setColumnSettings({
        xs: { span: 22, offset: 1 },
        md: { span: 22, offset: 1 },
        lg: { span: 22, offset: 1 },
        xl: { span: 22, offset: 1 },
        xxl: { span: 22, offset: 1 },
      });
    }
  }, [props.isFullscreen]);
  useEffect(() => {
    if (props.isStaticPage) {
      setColumnSettings({
        xs: { span: 22, offset: 1 },
        md: { span: 14, offset: 2 },
        lg: { span: 16, offset: 3 },
        xl: { span: 12, offset: 3 },
        xxl: { span: 10, offset: 7 },
      });
    }
  }, [props.isStaticPage]);

  const location = useLocation();
  const mainFormContentRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (!loadingStatus) {
      mainFormContentRef.current?.focus();
      window.scrollTo(0, 0);
    }
  }, [location.pathname, loadingStatus]);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{ height: '100%', display: loadingStatus === true ? 'none' : 'flex' }}
        className="site-layout-background"
      >
        <Header className="header-component site-layout-background">
          <ModulesHeader
            moduleName={moduleName}
            rvFull={rvFull}
            isRVArchived={isRVArchived}
            egfaId={props.egfaId}
            form={props.form}
            contactPerson={contactPerson}
            hasSaveBtn={hasSaveBtn}
            saveMethod={() => {
              egfaModuleController.onSave(props.form, undefined, true, setIsSaveButtonDisabled)();
              setIsSaveButtonDisabled(true);
            }}
            isSaveButtonDisabled={isSaveButtonDisabled}
            customContactDrawer={props.customContactDrawer}
            headerRightItems={props.headerRightItems}
            isLight={props.isLight}
            pageName={props.pageName}
            pruefenName={props.pruefenName}
            setIsFreigebenModalVisible={setIsFreigebenModalVisible}
            aktionen={selectedEgfa?.aktionen || []}
          />
        </Header>
        <FreigabeModalEgfa
          isVisible={isFreigebenModalVisible}
          setIsVisible={setIsFreigebenModalVisible}
          selectedEgfa={selectedEgfa}
          disabled={!selectedEgfa?.aktionen?.includes(AktionType.BerechtigungenAendern)}
          allowChanges={selectedEgfa?.aktionen?.includes(AktionType.BerechtigungenAendern)}
        />
        <Layout className="ant-layout-has-sider has-drawer">
          {!props.isStaticPage ? (
            <SiderWrapper
              breakpoint="lg"
              collapsedWidth="30"
              onCollapse={() => setIsCollapsed(!isCollapsed)}
              collapsible
              width={280}
              collapsed={isCollapsed}
              className="navbar-component egfa-navbar"
            >
              {!isCollapsed && (
                <Affix offsetTop={menuOffsetTop} style={{ position: 'fixed' }} className="egfa-nav">
                  <span>
                    <NavbarComponent form={props.form} navBarItems={sideMenu} trans={customTranslation} />
                  </span>
                </Affix>
              )}
            </SiderWrapper>
          ) : null}

          <Layout className="main-content-area site-layout-background">
            <Content className={`${props.moduleIdentifier}-holder`} style={{ padding: '24px 24px 120px' }}>
              <Row>
                <Col {...columnSettings} ref={mainFormContentRef} tabIndex={-1} className="module-form-wrapper">
                  {props.pageRouting(
                    egfaModuleController.onSave,
                    updateItemDisableStatus,
                    handleFormChange,
                    updateMenuItems,
                    egfaErst,
                  )}
                  {!props.isStaticPage ? (
                    <>
                      {hasSaveBtn && (
                        <ContinueLaterComponent
                          saveCurrentState={() => {
                            egfaModuleController.onSave(props.form, undefined, true, setIsSaveButtonDisabled)();
                            setIsSaveButtonDisabled(true);
                          }}
                          isDisabled={isSaveButtonDisabled}
                        />
                      )}

                      <Autosave
                        data={formValueChanges}
                        onSave={egfaModuleController.onSave(props.form, undefined, true, setIsSaveButtonDisabled)}
                        interval={AUTOSAVE_INTERVAL}
                        saveOnUnmount={false}
                      />
                    </>
                  ) : null}
                </Col>
              </Row>
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </Spin>
  );
}
