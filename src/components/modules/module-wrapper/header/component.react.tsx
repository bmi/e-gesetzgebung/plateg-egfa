// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { AktionType, RegelungsvorhabenEntityWithDetailsResponseDTO, UserEntityDTO } from '@plateg/rest-api';
import {
  BerechtigungenOutlined,
  BreadcrumbComponent,
  ContactPerson,
  DropdownMenu,
  HeaderComponent,
  HeaderController,
  InfoComponent,
  RVInfoComponent,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { ContinueLaterButton } from '../../../../shares/continue-later/component.react';
import { routes } from '../../../../shares/routes';

interface HeaderProps {
  moduleName: string;
  isRVArchived: boolean;
  rvFull?: RegelungsvorhabenEntityWithDetailsResponseDTO;
  egfaId: string;
  form?: FormInstance;
  contactPerson: UserEntityDTO;
  hasSaveBtn: boolean;
  saveMethod: () => void;
  isSaveButtonDisabled: boolean;
  customContactDrawer?: React.ReactElement;
  headerRightItems?: React.ReactElement[];
  isLight?: boolean;
  pageName?: string;
  pruefenName?: string;
  setIsFreigebenModalVisible: (val: boolean) => void;
  aktionen: AktionType[];
}

export function ModulesHeader(props: HeaderProps): React.ReactElement {
  const { t } = useTranslation();
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');
  const [breadcrumbItems, setBreadcrumbItems] = useState<React.ReactElement[]>();

  useEffect(() => {
    const items: React.ReactElement[] = [];
    const homeKey = !props.isLight ? 'linkHome' : 'linkHomeLight';
    let homeLink = (
      <Link id="egfa-startSeite-link" to="/egfa/" key="egfa-home">
        {t(`egfa.header.${homeKey}`)}
      </Link>
    );
    if (!props.isLight) {
      homeLink = (
        <Link
          id="egfa-startSeite-link"
          key="egfa-home"
          to={`/egfa/${props.isRVArchived ? routes.ARCHIV : routes.MEINE_EGFAS}`}
        >
          {t(`egfa.header.breadcrumbs.projectName`)} -{' '}
          {t(
            `egfa.home.gesetzesfolgenabschatzung.${
              props.isRVArchived ? 'archiv' : 'meineGesetzesfolgeabschaetzungen'
            }.title`,
          )}
        </Link>
      );
    }
    items.push(homeLink);

    if (!props.isLight) {
      const archivedLabel = props.isRVArchived ? t('egfa.header.breadcrumbs.archived') : '';
      items.push(
        <Link id="egfa-modulUebersicht-link" key="egfa-module" to={`/egfa/${props.egfaId}/${routes.MODUL_UEBERSICHT}`}>
          {archivedLabel}
          {props.rvFull?.dto.abkuerzung} {t('egfa.header.breadcrumbs.allModules')}
        </Link>,
      );
      items.push(<span key="egfa-module-name">{props.moduleName}</span>);
    }
    if (props.isLight && props.pageName && props.pruefenName) {
      items.push(
        <span key="egfa-module-name">
          {`${props.pruefenName} - `}
          {t(`egfa.modules.enap.${props.pageName}.linkName`)}
        </span>,
      );
    }

    setBreadcrumbItems(items);
  }, [
    props.egfaId,
    props.rvFull?.dto.abkuerzung,
    props.isRVArchived,
    props.moduleName,
    props.pageName,
    props.aktionen,
  ]);

  useEffect(() => {
    setBreadcrumb();
  }, [
    breadcrumbItems,
    props.hasSaveBtn,
    props.isSaveButtonDisabled,
    props.headerRightItems,
    props.form,
    props.aktionen,
  ]);

  const setBreadcrumb = () => {
    const rvDrawer =
      !props.isLight && props.aktionen.includes(AktionType.Lesen) ? (
        <InfoComponent
          isContactPerson={false}
          title={t('egfa.modules.general.infoRvTitle')}
          buttonText={t('egfa.modules.general.infoRv')}
          key="egfa-header-rv-info"
          titleWithoutPrefix={true}
        >
          <RVInfoComponent regelungsvorhaben={props.rvFull} isCreator={props.aktionen.includes(AktionType.Lesen)} />
        </InfoComponent>
      ) : null;
    const contactDrawer = props.customContactDrawer ?? (
      <InfoComponent
        isContactPerson={true}
        title={t('egfa.modules.general.contactPerson')}
        titleWithoutPrefix={true}
        buttonText={t('egfa.modules.general.contactPerson')}
        key="egfa-header-contact-person"
      >
        <ContactPerson user={props.contactPerson} />
      </InfoComponent>
    );

    const freigabeHeaderButton =
      !props.isLight && props.aktionen.includes(AktionType.BerechtigungenLesen) ? (
        <Button
          key="egfa-freigabe-btn"
          id="egfa-freigabe-btn"
          className="no-wrap blue-text-button"
          type="text"
          onClick={() => {
            props.setIsFreigebenModalVisible(true);
          }}
        >
          <BerechtigungenOutlined /> {t('egfa.viewEGFA.table.freigeben')}
        </Button>
      ) : null;

    const headerRight = props.headerRightItems ?? [];
    const headerMenuItems: DropdownMenuItem[] = [];
    if (props.hasSaveBtn) {
      headerRight.push(
        <ContinueLaterButton
          saveCurrentState={props.saveMethod}
          isDisabled={props.isSaveButtonDisabled}
          key="zwischenspeichern-button"
        />,
      );
      headerMenuItems.push({
        element: t('egfa.header.btnContinueLater.btnText'),
        onClick: props.saveMethod,
        disabled: () => props.isSaveButtonDisabled,
      });
    }
    if (rvDrawer) {
      headerRight.push(rvDrawer);
      headerMenuItems.push({
        element: rvDrawer,
      });
    }
    headerRight.push(contactDrawer);
    headerMenuItems.push({
      element: contactDrawer,
    });
    if (freigabeHeaderButton) {
      headerRight.push(freigabeHeaderButton);
      headerMenuItems.push({
        element: freigabeHeaderButton,
      });
    }
    headerRight.push(
      <DropdownMenu
        key={'egfa-module-header'}
        items={headerMenuItems}
        elementId={'headerRightAlternative'}
        overlayClass={'headerRightAlternative-overlay'}
      />,
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={breadcrumbItems} />],
      headerRight: headerRight,
    });
  };
  return <HeaderComponent ctrl={headerController} />;
}
