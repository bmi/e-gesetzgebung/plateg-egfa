// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModulePreiseControllerApi,
  EgfaModuleType,
  EgfaModuleWeitereEntityDTOAllOf,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportPreisToEditor = (egfaId: string, onFinish: (success: boolean, error?: AjaxError) => void): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaPreiseController = GlobalDI.get<EgfaModulePreiseControllerApi>('egfaModulePreiseControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaPreiseController.getEgfaModule3({ egfaId }).subscribe({
    next: (preisData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Preise,
          egfaDatenuebernahmeDTO: generatePreisJSON(preisData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Auswirkungen auf Einzelpreise und Preisniveau data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Auswirkungen auf Einzelpreise und Preisniveau data', error);
    },
  });
};
export const generatePreisJSON = (
  preisData: EgfaModuleWeitereEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const preisJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'preisErgebnisdokumentation',
    title: i18n.t(`egfa.modules.preise.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  if (preisData.influenceExpected && preisData.influenceSummary) {
    preisJSONExport.content.push({
      id: 'preisSummary',
      type: EgfaItemType.Text,
      data: preisData.influenceSummary,
    });
  } else {
    preisJSONExport.content.push({
      id: 'preisSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.preise.fertigstellung.noImpact`).toString(),
    });
  }
  return preisJSONExport;
};
