// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModulePreiseControllerApi,
  EgfaModulePreiseEntityDTO,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleAuswirkungen } from '../general/simple-module/auswirkungen/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generatePreisJSON } from './controller';

export function AuswirkungenPreise(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [preiseData, setPreiseData] = useState<EgfaModulePreiseEntityDTO>();
  const egfaPreiseController = GlobalDI.get<EgfaModulePreiseControllerApi>('egfaModulePreiseControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaPreiseController.getEgfaModule3({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModulePreiseEntityDTO) =>
    egfaPreiseController.modifyEgfaModule3({
      egfaId: egfaId,
      egfaModulePreiseEntityDTO: localData,
    });

  useEffect(() => {
    props.setEgfaName(routes.AUSWIRKUNGEN_PREISE);
  }, []);

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    _egfaErsteller?: UserEntityResponseDTO,
  ) => {
    const contentPageProps = { egfaId, setFormInstance, formData: preiseData, onSave, handleFormChange };
    const PDFExportBtn = ctrl.getPDFExportAction(preiseData?.egfaId as string, moduleName, preiseData?.status);
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung {...contentPageProps} moduleName="preise" additionalActions={PDFExportBtn} />
        </Route>
        {(preiseData?.aktionen?.includes(AktionType.Schreiben) === false ||
          preiseData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.AUSWIRKUNGEN_PREISE}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.WESENTLICHE_AUSWIRKUNGEN}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.WESENTLICHE_AUSWIRKUNGEN}`]}
        >
          <SimpleModuleAuswirkungen {...contentPageProps} moduleName="preise" />
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.ZUSAMMENFASSUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.FERTIGSTELLUNG}`}
            />
          )}
        />
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_PREISE}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={setPreiseData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      generateJson={generatePreisJSON}
    />
  );
}
