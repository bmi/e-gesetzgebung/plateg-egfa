// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Form, FormInstance } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { AktionType, EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { checkPruefungErforderlichkeitContainTrueAnswer } from '../pruefung-erforderlichkeit/component.react';

export interface ZusammenfassungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}
interface ZusammenfassungState {
  isDirty: boolean;
  pruefungErforderlichkeitContainTrueAnswer: boolean;
  previousLink: string;
  pruefungErforderlichkeitWeitereEvaluierungsanlaesseTrueAnswer?: boolean;
  prupruefungErforderlichkeitSchwellenwerrteTrueAnswer?: boolean;
}
export const keineEvaluierungElement = (
  evaluierungEntity: EgfaModuleEvaluierungEntityDTO | undefined,
  translator: TFunction<'translation', undefined>,
): React.ReactElement => {
  const keineAuswirkungen = translator(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`);
  if (evaluierungEntity === undefined) {
    return <p className="summary-text">{keineAuswirkungen}</p>;
  } else {
    return (
      <>
        <Title level={3}>
          {translator(`egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitDurchgefuehrt`)}
        </Title>
        <ul>
          {Object.keys(
            translator('egfa.modules.evaluierung.zusammenfassung.keineEvaluierungOptions', { returnObjects: true }),
          )
            .filter((key) => {
              return evaluierungEntity[key as keyof EgfaModuleEvaluierungEntityDTO];
            })
            .map((key) => {
              return <li>{translator(`egfa.modules.evaluierung.zusammenfassung.keineEvaluierungOptions.${key}`)}</li>;
            })}
        </ul>
      </>
    );
  }
};
export function Zusammenfassung(props: ZusammenfassungProps<EgfaModuleEvaluierungEntityDTO>): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [state, setState] = useState<ZusammenfassungState>({
    isDirty: false,
    pruefungErforderlichkeitContainTrueAnswer: false,
    previousLink: props.previousLinkName,
  });
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };
  const onClickHandler = () => {
    props.onSave();
  };

  const verzichtAufEvaluirungIsTrue = form.getFieldValue('keineEvaluierung')
    ? routes.GRUENDE_VERZICHT
    : props.previousLinkName;

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      const pruefungErforderlichkeitContainTrueAnswer = checkPruefungErforderlichkeitContainTrueAnswer(form, t);
      const pruefungErforderlichkeitWeitereEvaluierungsanlaesseTrueAnswer =
        checkPruefungErforderlichkeitContainTrueAnswer(form, t, 'weitereEvaluierungsanlaesse');
      const prupruefungErforderlichkeitSchwellenwerrteTrueAnswer = checkPruefungErforderlichkeitContainTrueAnswer(
        form,
        t,
        'schwellenWerte',
      );
      setState({
        ...state,
        pruefungErforderlichkeitContainTrueAnswer: pruefungErforderlichkeitContainTrueAnswer,
        previousLink: pruefungErforderlichkeitContainTrueAnswer
          ? verzichtAufEvaluirungIsTrue
          : routes.PRUEFUNG_ERFORDERLICHKEIT,
        pruefungErforderlichkeitWeitereEvaluierungsanlaesseTrueAnswer:
          pruefungErforderlichkeitWeitereEvaluierungsanlaesseTrueAnswer,
        prupruefungErforderlichkeitSchwellenwerrteTrueAnswer: prupruefungErforderlichkeitSchwellenwerrteTrueAnswer,
      });
    }
  }, [props.formData]);
  const noInputElement = t(`egfa.modules.einnahmenAusgaben.zusammenfassung.keineAuswirkungen`);
  const noEvaluation = t('egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtVorgesehen');
  return (
    <div className="evaluierung-ausgestaltung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.evaluierung.zusammenfassung.linkName`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${state.previousLink}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => state.isDirty}
        showMandatoryFieldInfo
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setState({ ...state, isDirty: true });
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        <>
          <div className="section-evaluierung">
            <div className="title">
              <span>{t(`egfa.modules.evaluierung.pruefungErforderlichkeit.linkName`)}</span>
              {props.formData?.egfaId && props.formData?.aktionen?.includes(AktionType.Schreiben) && (
                <span className="zur_bearbeitung">
                  <Link
                    to={`/egfa/${props.formData.egfaId}/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.PRUEFUNG_ERFORDERLICHKEIT}`}
                    onClick={onClickHandler}
                  >
                    {t(`egfa.modules.general.summaryBearbeitung`)}
                  </Link>
                </span>
              )}
            </div>
            <p className="summary-text">
              {state.pruefungErforderlichkeitContainTrueAnswer && keineEvaluierungElement(props.formData, t)}
              {props.formData?.gruendeEvaluierungNachJahren &&
              props.formData?.weitereGruendeFuerDieEvaluierung?.length ? (
                <>
                  <Title level={3}>{t(`egfa.modules.evaluierung.zusammenfassung.summaryWeitereGruende`)}</Title>

                  {props.formData?.weitereGruendeFuerDieEvaluierung}
                </>
              ) : (
                <></>
              )}
            </p>
            {!state.pruefungErforderlichkeitContainTrueAnswer && (
              <>
                <p
                  className="summary-text"
                  dangerouslySetInnerHTML={{
                    __html: `${t(
                      `egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtDurchgefuehrt`,
                    )}`,
                  }}
                />
                {!state.pruefungErforderlichkeitWeitereEvaluierungsanlaesseTrueAnswer ?? (
                  <p className="summary-text">{props.formData?.influenceSummary}</p>
                )}
              </>
            )}
          </div>
          {state.pruefungErforderlichkeitContainTrueAnswer && (
            <div className="section-evaluierung">
              <div className="title">
                <span>
                  {state.pruefungErforderlichkeitContainTrueAnswer && !form.getFieldValue('keineEvaluierung')
                    ? t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.linkName`)
                    : t(`egfa.modules.evaluierung.zusammenfassung.gruendeFuerDenVerzicht`)}
                </span>
                {props.formData?.egfaId && props.formData?.aktionen?.includes(AktionType.Schreiben) && (
                  <span className="zur_bearbeitung">
                    <Link
                      to={`/egfa/${props.formData.egfaId}/${routes.MODULE}/${routes.EVALUIERUNG}/${
                        form.getFieldValue('keineEvaluierung')
                          ? routes.GRUENDE_VERZICHT
                          : routes.AUSGESTALTUNG_DER_EVALUIERUNG
                      }`}
                      onClick={onClickHandler}
                    >
                      {t(`egfa.modules.general.summaryBearbeitung`)}
                    </Link>
                  </span>
                )}
              </div>
              {props.formData?.keineEvaluierung ? (
                <>
                  <Title level={3}>
                    {t(`egfa.modules.evaluierung.zusammenfassung.evaluierungAusgestaltungDurchgefuehrt`)}
                  </Title>

                  <p className="summary-text">{props.formData.keineEvaluierungBegruendung || noEvaluation}</p>
                </>
              ) : (
                <>
                  <div className="title">
                    <span>{t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.wasWirdEvaluiertLabel`)}</span>
                  </div>
                  <p className="summary-text">{props.formData?.wasWirdEvaluiert || noInputElement}</p>
                  <div className="title">
                    <span>{t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.wieWirdEvaluiertLabel`)}</span>
                  </div>
                  <p className="summary-text">{props.formData?.wieWirdEvaluiert || noInputElement}</p>
                  <div className="title">
                    <span>{t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.wannWirdEvaluiertLabel`)}</span>
                  </div>
                  <p className="summary-text">{props.formData?.wannWirdEvaluiert || noInputElement}</p>
                  <div className="section-evaluierung">
                    <p className="summary-text input">
                      {t(`egfa.modules.evaluierung.zusammenfassung.keineEvaluierung`)}
                    </p>
                    <FormItemWithInfo
                      name="zusammenfassung"
                      label={
                        <span>
                          {t(`egfa.modules.evaluierung.zusammenfassung.summary`)}
                          <InfoComponent
                            id={`info-drawer-ev-zusammenfassung`}
                            title={t(`egfa.modules.evaluierung.zusammenfassung.summaryInfoDrawer.title`)}
                            titleWithoutPrefix
                            withLabelRequired
                          >
                            <div
                              dangerouslySetInnerHTML={{
                                __html: t(`egfa.modules.evaluierung.zusammenfassung.summaryInfoDrawer.content`),
                              }}
                            />
                          </InfoComponent>
                        </span>
                      }
                      rules={[
                        {
                          required: true,
                          whitespace: true,
                          message: t('egfa.modules.evaluierung.zusammenfassung.summaryError'),
                        },
                      ]}
                    >
                      <TextArea rows={8} />
                    </FormItemWithInfo>
                  </div>
                </>
              )}
            </div>
          )}
        </>
      </FormWrapper>
    </div>
  );
}
