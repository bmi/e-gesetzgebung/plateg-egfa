// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router';

import {
  AktionType,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleEvaluierungEntityDTO,
  EgfaModuleEvaluierungEntityResponseDTO,
  EgfaModuleStatusType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../shares/routes';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import { KmuReferenzenTypes } from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { EvaluierungController } from './controller';
import { AusgestaltungQuestionPage } from './evaluierung-ausgestaltung/ausgestaltung-question-page/component.react';
import { EvaluierungAusgestaltung } from './evaluierung-ausgestaltung/component.react';
import { Fertigstellung } from './fertigstellung/component.react';
import { GruendeFuerVerzichtEineEvaluierung } from './greunde-fuer-verzicht-auf-evaluierung/component.react';
import { PruefungErforderlichkeit } from './pruefung-erforderlichkeit/component.react';
import { PruefungErforderlichkeitSimplePage } from './pruefung-erforderlichkeit/pruefung-erforderlichkeit-simple-page/component.react';
import { Zusammenfassung } from './zusammenfassung/component.react';

export function Evaluierung(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [evaluierungData, setEvaluierungData] = useState<EgfaModuleEvaluierungEntityDTO & ExtendedModuleData>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const egfaEvaluierungControllerApi = GlobalDI.get<EgfaModuleEvaluierungControllerApi>('egfaEvaluierungControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const { t } = useTranslation();
  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);
  const getData = () => {
    return egfaEvaluierungControllerApi.getEgfaModule8({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleEvaluierungEntityDTO) => {
    return egfaEvaluierungControllerApi.modifyEgfaModule8({
      egfaId: egfaId,
      egfaModuleEvaluierungEntityDTO: localData,
    });
  };
  useEffect(() => {
    props.setEgfaName(routes.EVALUIERUNG);
  }, []);
  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (evaluierungData && evaluierungData.referenzenData) {
      setReferenzenData(evaluierungData.referenzenData);
    }
  }, [evaluierungData]);

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string, isDisabled: boolean) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ) => {
    const contentPageProps = {
      egfaId,
      setFormInstance,
      formData: evaluierungData,
      onSave,
      handleFormChange,
    };
    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.FERTIGSTELLUNG}`]}>
          <Fertigstellung
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            previousLinkName={routes.ZUSAMMENFASSUNG}
          />
        </Route>
        {(evaluierungData?.aktionen?.includes(AktionType.Schreiben) === false ||
          evaluierungData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.PRUEFUNG_ERFORDERLICHKEIT}
            customContent={
              <>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('egfa.modules.evaluierung.einleitung.content.paragraph1.content'),
                  }}
                />
                <p>{t('egfa.modules.evaluierung.einleitung.content.paragraph2.content')}</p>
              </>
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.PRUEFUNG_ERFORDERLICHKEIT}`]}>
          <PruefungErforderlichkeitSimplePage
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={`${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.SCHWELLENWERTE}`}
            previousLinkName={routes.EINLEITUNG}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.SCHWELLENWERTE}`,
          ]}
        >
          <PruefungErforderlichkeit
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={`${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.WEITERE_EVALUIERUNGSANLAESSE}`}
            previousLinkName={routes.PRUEFUNG_ERFORDERLICHKEIT}
            optionGroup="schwellenWerte"
            referenzenList={[
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.evaluierungErfuellungsaufwandReferenzen,
                    conditionsList: [
                      KmuReferenzenTypes.moreThanOneMioEurSachaufwandBuerger,
                      KmuReferenzenTypes.moreThanHundredThousandHrsZeitaufwandBuerger,
                    ],
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                    checkTwoCasesCondition: true,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.EVALUIERUNG,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.evaluierungErfuellungsaufwandReferenzen,
                    conditionsList: [KmuReferenzenTypes.moreThanOneMioEurWirtschaft],
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                    checkTwoCasesCondition: true,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.EVALUIERUNG,
                  },
                ]}
              />,
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.evaluierungErfuellungsaufwandReferenzen,
                    conditionsList: [KmuReferenzenTypes.moreThanOneMioEurVerwaltung],
                    moduleName: routes.ERFUELLUNGSAUFWAND,
                    checkTwoCasesCondition: true,
                    checkIsNotFertiggestellt: true,
                    currentModuleName: routes.EVALUIERUNG,
                  },
                ]}
              />,
            ]}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.WEITERE_EVALUIERUNGSANLAESSE}`,
          ]}
        >
          <PruefungErforderlichkeit
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={routes.GRUENDE_VERZICHT}
            previousLinkName={`${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.SCHWELLENWERTE}`}
            optionGroup="weitereEvaluierungsanlaesse"
            referenzenList={[
              <>
                <ReferenzenGenericComponent
                  {...referenzenDataProps}
                  referenzenDataConfig={[
                    {
                      referenzenData: referenzenData?.evaluierungEaOehhReferenzen,
                      moduleName: routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN,
                      checkIsNotFertiggestellt: true,
                      currentModuleName: routes.EVALUIERUNG,
                    },
                  ]}
                />
                <ReferenzenGenericComponent
                  {...referenzenDataProps}
                  referenzenDataConfig={[
                    {
                      referenzenData: referenzenData?.evaluierungSonstigeKostenReferenzen,
                      moduleName: routes.SONSTIGE_KOSTEN,
                      checkIsNotFertiggestellt: true,
                      currentModuleName: routes.EVALUIERUNG,
                    },
                  ]}
                />
              </>,
            ]}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.GRUENDE_VERZICHT}`]}>
          <GruendeFuerVerzichtEineEvaluierung
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={routes.AUSGESTALTUNG_DER_EVALUIERUNG}
            previousLinkName={`${routes.PRUEFUNG_ERFORDERLICHKEIT}/${routes.WEITERE_EVALUIERUNGSANLAESSE}`}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.AUSGESTALTUNG_DER_EVALUIERUNG}`]}
        >
          <EvaluierungAusgestaltung
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={`${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WAS}`}
            previousLinkName={routes.GRUENDE_VERZICHT}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WAS}`,
          ]}
        >
          <AusgestaltungQuestionPage
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={`${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WIE}`}
            previousLinkName={routes.AUSGESTALTUNG_DER_EVALUIERUNG}
            questionKey={routes.WAS}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WIE}`,
          ]}
        >
          <AusgestaltungQuestionPage
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={`${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WANN}`}
            previousLinkName={`${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WAS}`}
            questionKey={routes.WIE}
          />
        </Route>
        <Route
          exact
          path={[
            `/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WANN}`,
          ]}
        >
          <AusgestaltungQuestionPage
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            previousLinkName={`${routes.AUSGESTALTUNG_DER_EVALUIERUNG}/${routes.WIE}`}
            questionKey={routes.WANN}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.ZUSAMMENFASSUNG}`]}>
          <Zusammenfassung
            {...contentPageProps}
            moduleName={routes.EVALUIERUNG}
            nextLinkName={routes.FERTIGSTELLUNG}
            previousLinkName={routes.AUSGESTALTUNG_DER_EVALUIERUNG}
          />
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.EVALUIERUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.EINLEITUNG}`} />
          )}
        />
      </Switch>
    );
  };
  return (
    <ModuleWrapper<EgfaModuleEvaluierungEntityDTO, EgfaModuleEvaluierungEntityResponseDTO>
      setFormData={setEvaluierungData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      customResultPage={routes.FERTIGSTELLUNG}
      generateJson={(data) => {
        const evaluierungController = new EvaluierungController();
        evaluierungController.evaluierungData = data;
        return evaluierungController.createEvaluierungJSON();
      }}
    />
  );
}
