// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { Observable, of } from 'rxjs';
import { AjaxResponse } from 'rxjs/internal/ajax/AjaxResponse';
import Sinon from 'sinon';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleEvaluierungEntityDTOAllOf,
  EgfaModuleType,
  OperationOpts,
  SendEgfaDatenuebernahmeRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { EvaluierungController } from './controller';

export const evaluierungTestData: EgfaModuleEvaluierungEntityDTOAllOf & EgfaModuleEntityRequestDTO = {
  base: {
    id: '8ee5d72e-9ff7-4a8f-998b-7b435bcc4e70',
    erstelltAm: '2023-08-21T15:46:36Z',
    bearbeitetAm: '2023-09-12T07:07:53Z',
  },
  regelungsvorhaben: {
    base: {
      id: 'f9ca854b-b158-44e5-9d44-0c698b0aee45',
      erstelltAm: '2023-06-07T14:33:41.081793Z',
      bearbeitetAm: '2023-08-24T09:39:32.351717Z',
    },
    dto: {
      abkuerzung: 'rv kab 1',
      kurzbezeichnung: 'rv kab 1',
      status: 'IN_BEARBEITUNG',
      vorhabenart: 'GESETZ',
      archiviertAm: null,
      erstelltAm: null,
      erstelltVon: {
        base: {
          id: 'a9fbc760-5aa4-4e65-a927-f2d935f3114b',
          erstelltAm: '2023-02-06T10:48:46.047685Z',
          bearbeitetAm: '2023-02-06T10:48:46.047685Z',
        },
        dto: {
          name: 'Robert1 Rechtsetzungsreferent1',
          email: 'robert1.rechtsetzungsreferent1@egesetz.de',
          telefon: null,
          abteilung: 'D',
          fachreferat: null,
          emailVerteilerReferat: null,
          titel: null,
          ressort: {
            id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
            kurzbezeichnung: 'BMI',
            bezeichnung: 'Bundesministerium des Innern und für Heimat',
            aktiv: true,
          },
          anrede: 'Herr',
          gid: 'someRandomId',
        },
      },
      bearbeitetAm: null,
      pkpGesendetAm: '2023-08-22T11:35:08.414979Z',
      pkpStatus: 'AKTUALISIERT',
      pkpWorkflowStatus: null,
      farbe: null,
      langtitel: 'rv kab 1',
      kurzbeschreibung: null,
      technischesFfRessort: {
        id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
        kurzbezeichnung: 'BMI',
        bezeichnung: 'Bundesministerium des Innern und für Heimat',
        aktiv: true,
      },
      technischeFfAbteilung: {
        id: '14a67ec6-7077-4366-8e40-a58db2907274',
        bezeichnung: 'Z',
        unterabteilungen: [
          {
            id: '8d3dccdb-4003-4880-bdf5-0a80b108135d',
            bezeichnung: 'Z II',
            unterabteilungen: [],
            referate: [
              {
                id: '0d488328-8009-4ae1-8a89-aaca9505e08d',
                bezeichnung: 'Z II 1',
              },
              {
                id: '3d6ce95a-0af4-414c-aef4-69b9d36378fd',
                bezeichnung: 'Z II 2',
              },
            ],
          },
          {
            id: 'be04e225-bd50-44d1-b107-7a8b92598ecc',
            bezeichnung: 'Z I',
            unterabteilungen: [],
            referate: [],
          },
          {
            id: 'e0a8e60d-4405-4064-a80c-e284df3793b2',
            bezeichnung: 'Z III',
            unterabteilungen: [],
            referate: [],
          },
        ],
        referate: [],
      },
      technischesFfReferat: {
        id: '0d488328-8009-4ae1-8a89-aaca9505e08d',
        bezeichnung: 'Z II 1',
      },
      bearbeitetVon: {
        base: {
          id: 'a9fbc760-5aa4-4e65-a927-f2d935f3114b',
          erstelltAm: '2023-02-06T10:48:46.047685Z',
          bearbeitetAm: '2023-02-06T10:48:46.047685Z',
        },
        dto: {
          name: 'Robert1 Rechtsetzungsreferent1',
          email: 'robert1.rechtsetzungsreferent1@egesetz.de',
          telefon: null,
          abteilung: 'D',
          fachreferat: null,
          emailVerteilerReferat: null,
          titel: null,
          ressort: {
            id: 'cd0952e8-7b68-485b-b16a-d97fb9f1467a',
            kurzbezeichnung: 'BMI',
            bezeichnung: 'Bundesministerium des Innern und für Heimat',
            aktiv: true,
          },
          anrede: 'Herr',
          gid: 'someRandomId',
        },
      },
    },
  },
  dto: {
    egfaId: 'd1e68b66-f742-4880-9728-a6e41e978121',
    title: 'Evaluierung',
    influenceExpected: false,
    deleted: false,
    influenceSummary: '',
    status: 'VOLLSTAENDIG_BEARBEITET',
    allowedToModify: true,
    keineEvaluierung: true,
    keineEvaluierungBegruendung:
      'Begründen Sie Ihre Entscheidung, warum Sie von einer Evaluierung absehen wollen, obwohl die Voraussetzungen hierfür vorliegen',
    wasWirdEvaluiert: null,
    wieWirdEvaluiert: null,
    wannWirdEvaluiert: null,
    weitereGruendeFuerDieEvaluierung:
      'Gibt es weitere Gründe, die für eine Evaluierung des Regelungsentwurfs nach drei bis fünf Jahren sprechen?',
    minErfuellungsaufwandFuerBuerger: true,
    minErfuellungsaufwandFuerWirtschaft: true,
    minErfuellungsaufwandFuerVerwaltung: false,
    hoherFinanziellerGesamtaufwand: false,
    grosseUnsicherheiten: false,
    besonderePolitischeBedeutung: false,
    gruendeEvaluierungNachJahren: true,
    zusammenfassung: null,
    typ: 'EVALUIERUNG',
  },
  evaluierungSonstigeKostenReferenzen: {
    rolleType: 'ERSTELLER',
    status: 'IN_BEARBEITUNG',
  },
  evaluierungErfuellungsaufwandReferenzen: {
    rolleType: 'ERSTELLER',
    status: 'IN_BEARBEITUNG',
    moreThanOneMioEurSachaufwandBuerger: false,
    moreThanHundredThousandHrsZeitaufwandBuerger: false,
    moreThanOneMioEurWirtschaft: false,
    moreThanOneMioEurVerwaltung: false,
  },
  evaluierungEaOehhReferenzen: {
    rolleType: 'ERSTELLER',
    status: 'IN_BEARBEITUNG',
  },
};
describe(`Test: exportEvaluierungToEditor`, () => {
  const evaluierungController = new EvaluierungController();
  let egfaControllerStub: Sinon.SinonStub<
    [SendEgfaDatenuebernahmeRequest, (OperationOpts | undefined)?],
    Observable<void | AjaxResponse<void>>
  >;
  let egfaModuleControllerStub: Sinon.SinonStub;
  let i18nStub: Sinon.SinonStub;
  before(() => {
    // Stub the observables to return synchronous data (using `of`)
    GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi());
    GlobalDI.getOrRegister('egfaEvaluierungControllerApi', () => new EgfaModuleEvaluierungControllerApi());
    egfaControllerStub = Sinon.stub(EgfaControllerApi.prototype, 'sendEgfaDatenuebernahme');
    egfaModuleControllerStub = Sinon.stub(EgfaModuleEvaluierungControllerApi.prototype, 'getEgfaModule8');

    i18nStub = Sinon.stub(i18n, 't');
  });

  after(() => {
    egfaModuleControllerStub.restore();
    egfaControllerStub.restore();
    i18nStub.restore();
  });
  afterEach(() => {
    egfaModuleControllerStub.resetHistory();
    egfaControllerStub.resetHistory();
    i18nStub.resetHistory();
  });
  it(`correct Evaluierung data, influence expected`, function (done) {
    // Add the 'done' parameter to the test function
    egfaModuleControllerStub.returns(of(evaluierungTestData));
    egfaControllerStub.returns(of(void 0));
    i18nStub.returns('sometext');

    // Call the function under test (auswirkungenEinnahmenAusgabenExportJsonCtrl.exportauswirkungenEinnahmenAusgabenToEditor)
    let result: boolean;
    evaluierungController.exportEvaluierungToEditor('123', (success) => {
      result = success;
    });

    // Use setTimeout to introduce a slight delay to allow the asynchronous behavior to complete
    setTimeout(() => {
      const expectedOutput: EgfaDatenuebernahmeDTO = {
        id: 'evaluierungErgebnisdokumentation',
        title: 'Zusammenfassung',
        content: [],
        children: [
          {
            id: 'evaluierungNichtDurchfuehrung',
            title: 'Aussagen zur Nicht-Durchführung einer Evaluierung',
            content: [
              {
                id: 'evaluierungNichtDurchfuehrungText',
                type: EgfaItemType.Text,
                data: 'Begründen Sie Ihre Entscheidung, warum Sie von einer Evaluierung absehen wollen, obwohl die Voraussetzungen hierfür vorliegen',
              },
            ],
          },
        ],
      };
      // Assert the result

      egfaControllerStub.calledWith({
        id: '123',
        moduleType: EgfaModuleType.Evaluierung,
        egfaDatenuebernahmeDTO: expectedOutput,
      });

      // Call 'done' to notify the test framework that the test has finished
      done();
    }, 50); // Adjust the delay time as needed
  });
});
