// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';
import { FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';

export interface AusgestaltungQuestionPageProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
  questionKey: string;
}
interface EvaluierungAusgestaltungState {
  isDirty: boolean;
}
export function AusgestaltungQuestionPage(
  props: AusgestaltungQuestionPageProps<EgfaModuleEvaluierungEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [state, setState] = useState<EvaluierungAusgestaltungState>({
    isDirty: false,
  });
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
    }
  }, [props.formData]);

  const translationKey = `egfa.modules.evaluierung.ausgestaltungDerEvaluierung.questionPages.${props.questionKey}`;

  const prepareHtmlUl = (translation: string) => {
    const list: string[] = t(translation, { returnObjects: true });
    return (
      <ul>
        {list.map((item, ind) => (
          <li key={ind}>{item}</li>
        ))}
      </ul>
    );
  };

  const prepareInfoDrawer = (translation: string, drawerId: string) => {
    return (
      <InfoComponent
        title={t(`${translation}.title`)}
        id={`info-drawer-ausgestaltungDerEvaluierung-${props.questionKey}-${drawerId}`}
        titleWithoutPrefix
      >
        <div
          dangerouslySetInnerHTML={{
            __html: t(`${translation}.content`),
          }}
        />
      </InfoComponent>
    );
  };

  return (
    <div className="evaluierung-ausgestaltung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`${translationKey}.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => state.isDirty}
        showMandatoryFieldInfo={false}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setState({ ...state, isDirty: true });
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        <div>
          {t(`${translationKey}.intro`)}
          {i18n.exists(`${translationKey}.introInfoDrawer1`) &&
            prepareInfoDrawer(`${translationKey}.introInfoDrawer1`, 'introInfoDrawer1')}
          {i18n.exists(`${translationKey}.introPart2`) && t(`${translationKey}.introPart2`)}
        </div>
        {i18n.exists(`${translationKey}.introList`) && prepareHtmlUl(`${translationKey}.introList`)}
        {i18n.exists(`${translationKey}.intro2`) && (
          <div>
            {t(`${translationKey}.intro2`)}
            {i18n.exists(`${translationKey}.intro2InfoDrawer1`) &&
              prepareInfoDrawer(`${translationKey}.intro2InfoDrawer1`, 'intro2InfoDrawer1')}
            {i18n.exists(`${translationKey}.intro2Part2`) && t(`${translationKey}.intro2Part2`)}
          </div>
        )}
        {i18n.exists(`${translationKey}.intro2List`) && prepareHtmlUl(`${translationKey}.intro2List`)}
        {i18n.exists(`${translationKey}.intro3`) && <p>{t(`${translationKey}.intro3`)}</p>}
        <Form.Item name={t(`${translationKey}.fieldName`)} label={t(`${translationKey}.labelTextarea`)}>
          <TextArea rows={8} />
        </Form.Item>
      </FormWrapper>
    </div>
  );
}
