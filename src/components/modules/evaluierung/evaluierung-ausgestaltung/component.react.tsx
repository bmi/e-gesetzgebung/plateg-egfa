// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './evaluierung-ausgestaltung.less';

import { Form, FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';
import { FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface EvaluierungAusgestaltungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}
export function EvaluierungAusgestaltung(
  props: EvaluierungAusgestaltungProps<EgfaModuleEvaluierungEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
    }
  }, [props.formData]);

  return (
    <div className="evaluierung-ausgestaltung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.linkName`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => false}
        showMandatoryFieldInfo={false}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        <div>
          {t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.part1`)}
          <InfoComponent
            title={t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.introInfoDrawer1.title`)}
            id={`info-drawer-ausgestaltungDerEvaluierung-introDrawer1`}
            titleWithoutPrefix
          >
            <div
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.introInfoDrawer1.content`),
              }}
            />
          </InfoComponent>{' '}
          {t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.part2`)}
          <InfoComponent
            title={t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.introInfoDrawer2.title`)}
            id={`info-drawer-ausgestaltungDerEvaluierung-introDrawer2`}
            titleWithoutPrefix
          >
            <div
              dangerouslySetInnerHTML={{
                __html: t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.introInfoDrawer2.content`),
              }}
            />
          </InfoComponent>{' '}
          {t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro.part3`)}
        </div>
        <p>{t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro2`)}</p>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro3`, {
              email: t(`egfa.modules.evaluierung.ausgestaltungDerEvaluierung.intro3_email`),
            }),
          }}
        ></p>
      </FormWrapper>
    </div>
  );
}
