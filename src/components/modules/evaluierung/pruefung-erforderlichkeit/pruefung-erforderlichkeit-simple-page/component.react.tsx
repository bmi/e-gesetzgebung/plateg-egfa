// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../pruefung-erforderlichkeit.less';

import { Form, FormInstance } from 'antd';

import Title from 'antd/lib/typography/Title';
import React, { useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';

import { EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ContentPageProps } from '../../../general/simple-module/auswirkungen/component.react';

export interface PruefungErforderlichkeitMainProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
  referenzenList?: (React.ReactElement | null)[];
}
export const checkPruefungErforderlichkeitContainTrueAnswer = (
  pruefungErforderlichkeitForm: FormInstance<any>,
  translator: TFunction,
): boolean => {
  const formData: EgfaModuleEvaluierungEntityDTO = pruefungErforderlichkeitForm.getFieldsValue(
    true,
  ) as EgfaModuleEvaluierungEntityDTO;
  return Object.keys(
    translator('egfa.modules.evaluierung.pruefungErforderlichkeit.content.options', { returnObjects: true }),
  )
    .map((key) => {
      return formData[key as keyof EgfaModuleEvaluierungEntityDTO] === true;
    }, false)
    .some((item) => item === true);
};
export function PruefungErforderlichkeitSimplePage(
  props: PruefungErforderlichkeitMainProps<EgfaModuleEvaluierungEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };
  return (
    <div className="pruefung-erforderlichkeit">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.evaluierung.pruefungErforderlichkeit.linkName`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        showMandatoryFieldInfo
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.paragraph1`),
          }}
        />
        <p>{t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.paragraph2`)}</p>
        <p>{t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.paragraph3`)}</p>
      </FormWrapper>
    </div>
  );
}
