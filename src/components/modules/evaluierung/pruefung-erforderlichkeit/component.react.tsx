// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './pruefung-erforderlichkeit.less';

import { Form, FormInstance, Radio } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router-dom';

import { EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';
import { FormItemWithInfo, FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface PruefungErforderlichkeitProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
  referenzenList?: (React.ReactElement | null)[];
  optionGroup?: string;
}
export const checkPruefungErforderlichkeitContainTrueAnswer = (
  pruefungErforderlichkeitForm: FormInstance<any>,
  translator: TFunction,
  specificOptionGroup?: string,
): boolean => {
  const formData: EgfaModuleEvaluierungEntityDTO = pruefungErforderlichkeitForm.getFieldsValue(
    true,
  ) as EgfaModuleEvaluierungEntityDTO;
  return Object.keys(
    translator(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.options${specificOptionGroup ?? ''}`, {
      returnObjects: true,
    }),
  )
    .map((key) => {
      return formData[key as keyof EgfaModuleEvaluierungEntityDTO] === true;
    }, false)
    .some((item) => item === true);
};
export function PruefungErforderlichkeit(
  props: PruefungErforderlichkeitProps<EgfaModuleEvaluierungEntityDTO>,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName);
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };
  const routeMatcherVorbereitung = useRouteMatch<{ subPageName: string }>(
    `/egfa/:id/module/:moduleName/:pageName/:subPageName`,
  );
  const isWietereEvaluirungsanlaesse = routeMatcherVorbereitung?.params.subPageName === 'weitereEvaluierungsanlaesse';
  const subPageName = routeMatcherVorbereitung?.params?.subPageName;
  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      checkTrueAnswerExist();
    }
  }, [props.formData, subPageName]);

  const checkTrueAnswerExist = () => {
    const hasTruevalues = checkPruefungErforderlichkeitContainTrueAnswer(form, t);
    !hasTruevalues && isWietereEvaluirungsanlaesse
      ? setNextLink(routes.ZUSAMMENFASSUNG)
      : setNextLink(props.nextLinkName);
  };
  const options = Object.keys(
    t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.options${props.optionGroup ?? ''}`, {
      returnObjects: true,
    }),
  ).map((key, optionIndex) => {
    return (
      <React.Fragment key={crypto.randomUUID()}>
        <FormItemWithInfo
          name={key}
          required
          label={
            <>
              <span
                className="radio-label"
                dangerouslySetInnerHTML={{
                  __html: t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.options.${key}`),
                }}
              />
              {key === 'grosseUnsicherheiten' && (
                <InfoComponent
                  title={t(
                    'egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionsweitereEvaluierungsanlaesseInfoDrawerTitle5',
                  )}
                  withLabelRequired
                >
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(
                        'egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionsweitereEvaluierungsanlaesseInfoDrawerContent5',
                        {
                          interpolation: { escapeValue: false },
                        },
                      ),
                    }}
                  ></p>
                </InfoComponent>
              )}
              {key === 'besonderePolitischeBedeutung' && (
                <InfoComponent
                  title={t(
                    'egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionsweitereEvaluierungsanlaesseInfoDrawerTitle6',
                  )}
                  withLabelRequired
                >
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t(
                        'egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionsweitereEvaluierungsanlaesseInfoDrawerContent6',
                        {
                          interpolation: { escapeValue: false },
                        },
                      ),
                    }}
                  ></p>
                </InfoComponent>
              )}
            </>
          }
        >
          <Radio.Group
            defaultValue={props.formData?.[key as keyof EgfaModuleEvaluierungEntityDTO]}
            className="horizontal-radios"
            onChange={() => checkTrueAnswerExist()}
          >
            <Radio
              id={`pruefungErforderlichkeit-${key}`}
              aria-label={t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionYes`)}
              key={`option-yes-${key}`}
              value={true}
              className="horizontal-radios"
            >
              {key === 'gruendeEvaluierungNachJahren'
                ? t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionYesUndZwar`)
                : t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionYes`)}
            </Radio>
            {key === 'gruendeEvaluierungNachJahren' && form.getFieldValue('gruendeEvaluierungNachJahren') === true && (
              <Form.Item name={['weitereGruendeFuerDieEvaluierung']}>
                <TextArea rows={8} />
              </Form.Item>
            )}

            <Radio
              id={`pruefungErforderlichkeit-${key}`}
              aria-label={t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionNo`)}
              key={`option-no-${key}`}
              value={false}
              className="horizontal-radios"
            >
              {t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.optionNo`)}
            </Radio>
          </Radio.Group>
        </FormItemWithInfo>
        {props.referenzenList && props.referenzenList[optionIndex]}
      </React.Fragment>
    );
  });
  return (
    <div className="pruefung-erforderlichkeit">
      <FormWrapper
        projectName="eGFA"
        title={
          <Title level={1}>
            {props.optionGroup
              ? t(`egfa.modules.evaluierung.pruefungErforderlichkeit.${props.optionGroup}.linkName`)
              : t(`egfa.modules.evaluierung.pruefungErforderlichkeit.linkName`)}
          </Title>
        }
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        showMandatoryFieldInfo
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        {options}
      </FormWrapper>
    </div>
  );
}
