// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { map, switchMap } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaDatenuebernahmeDTOContentInner,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleEvaluierungEntityDTO,
  EgfaModuleEvaluierungEntityDTOAllOf,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export class EvaluierungController {
  public evaluierungData: EgfaModuleEvaluierungEntityDTOAllOf & EgfaModuleEntityRequestDTO = {};

  /**
   *
   * @param egfaId string
   * @param onFinish void function with boolean parameter success true on success false on Failur
   */
  public exportEvaluierungToEditor(egfaId: string, onFinish: (success: boolean, error?: AjaxError) => void): void {
    const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
    const egfaEvaluierungControllerApi =
      GlobalDI.get<EgfaModuleEvaluierungControllerApi>('egfaEvaluierungControllerApi');
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    loadingStatusController.setLoadingStatus(true);
    egfaEvaluierungControllerApi
      .getEgfaModule8({ egfaId: egfaId })
      .pipe(
        map((evaluierungData) => {
          this.evaluierungData = evaluierungData.dto;
          const evaluierungJSONExport = this.createEvaluierungJSON();
          return evaluierungJSONExport;
        }),
        switchMap((evaluierungJSONExportData) => {
          return egfaController.sendEgfaDatenuebernahme({
            id: egfaId,
            moduleType: EgfaModuleType.Evaluierung,
            egfaDatenuebernahmeDTO: evaluierungJSONExportData,
          });
        }),
      )
      .subscribe({
        next: () => {
          onFinish(true);
          loadingStatusController.setLoadingStatus(false);
        },
        error: (error) => {
          onFinish(false, error as AjaxError);
          loadingStatusController.setLoadingStatus(false);
          console.error('Evaluierung data export to Editor failed', error);
        },
      });
  }
  public createEvaluierungJSON(): EgfaDatenuebernahmeDTO {
    const noEvaluation = i18n.t(
      'egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtDurchgefuehrt',
    );
    const nichtVorgesehen = i18n.t(
      'egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtVorgesehen',
    );
    const pruefungErforderlichkeitContainTrueAnswer = this.checkPruefungErforderlichkeitContainTrueAnswer();
    const pruefung1to3Questions = this.checkPruefungErforderlichkeitContainTrueAnswer('schwellenWerte');
    const evaluierungJSONExport: EgfaDatenuebernahmeDTO = {
      id: 'evaluierungErgebnisdokumentation',
      title: i18n.t(`egfa.modules.evaluierung.zusammenfassung.linkName`),
      content: [],
      children: [],
    };
    const content: EgfaDatenuebernahmeDTOContentInner[] = [];
    if (!pruefungErforderlichkeitContainTrueAnswer) {
      content.push({
        id: 'noEvaluierung',
        type: EgfaItemType.Text,
        data: noEvaluation,
      });
    }
    const children: EgfaDatenuebernahmeDTO[] = [];
    if (pruefungErforderlichkeitContainTrueAnswer && !this.evaluierungData.keineEvaluierung) {
      children.push({
        id: 'evaluierungDurchfuehrung',
        title: i18n.t(`egfa.modules.evaluierung.fertigstellung.aussagenZurDurchfuehrung`).toString(),
        content: [
          {
            id: 'evaluierungDurchfuehrungText',
            type: EgfaItemType.Text,
            data: this.evaluierungData.zusammenfassung,
          },
        ],
      });
    }
    if (pruefungErforderlichkeitContainTrueAnswer && pruefung1to3Questions && this.evaluierungData.keineEvaluierung) {
      children.push({
        id: 'evaluierungDurchfuehrung',
        title: i18n.t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`).toString(),
        content: [
          {
            id: 'evaluierungDurchfuehrungText',
            type: EgfaItemType.Text,
            data: this.evaluierungData.keineEvaluierungBegruendung,
          },
        ],
      });
    }
    if (
      pruefungErforderlichkeitContainTrueAnswer &&
      !pruefung1to3Questions &&
      this.evaluierungData.keineEvaluierung &&
      this.evaluierungData.keineEvaluierungBegruendung &&
      this.evaluierungData.keineEvaluierungBegruendung?.length > 0
    ) {
      children.push({
        id: 'evaluierungDurchfuehrung',
        title: i18n.t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`).toString(),
        content: [
          {
            id: 'evaluierungDurchfuehrungText',
            type: EgfaItemType.Text,
            data: this.evaluierungData.keineEvaluierungBegruendung,
          },
        ],
      });
    }
    if (
      pruefungErforderlichkeitContainTrueAnswer &&
      !pruefung1to3Questions &&
      this.evaluierungData.keineEvaluierung &&
      !this.evaluierungData.keineEvaluierungBegruendung
    ) {
      children.push({
        id: 'evaluierungDurchfuehrung',
        title: i18n.t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`).toString(),
        content: [
          {
            id: 'evaluierungDurchfuehrungText',
            type: EgfaItemType.Text,
            data: nichtVorgesehen,
          },
        ],
      });
    }
    evaluierungJSONExport.content = content;
    evaluierungJSONExport.children = children;
    return evaluierungJSONExport;
  }
  private checkPruefungErforderlichkeitContainTrueAnswer(specificOptionGroup?: string): boolean {
    return Object.keys(
      i18n.t(`egfa.modules.evaluierung.pruefungErforderlichkeit.content.options${specificOptionGroup ?? ''}`, {
        returnObjects: true,
      }),
    )
      .map((key) => {
        return this.evaluierungData[key as keyof EgfaModuleEvaluierungEntityDTO] === true;
      }, false)
      .some((item) => item === true);
  }
}
