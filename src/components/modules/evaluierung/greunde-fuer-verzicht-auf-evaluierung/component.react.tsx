// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input, Radio, Typography } from 'antd';
import { FormInstance } from 'antd/es/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleEntityRequestDTO } from '@plateg/rest-api';
import { FormWrapper, InfoComponent } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';

export interface GruendeFuerVerzichtEineEvaluierungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  nextLinkName: string;
  previousLinkName: string;
}

export function GruendeFuerVerzichtEineEvaluierung(
  props: GruendeFuerVerzichtEineEvaluierungProps<EgfaModuleEntityRequestDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { TextArea } = Input;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName);
  const onSave = (formInstance: FormInstance<any>) => {
    return props.onSave(formInstance);
  };

  const keineEvaluierung = Form.useWatch('keineEvaluierung', form) as boolean;

  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      setIsDirty(false);
    }
  }, [props.formData]);

  useEffect(() => {
    if (
      (question1To3AnsweredYes && form.getFieldValue('keineEvaluierung') === true) ||
      (questions4To7 && form.getFieldValue('keineEvaluierung') === true)
    ) {
      setNextLink(routes.ZUSAMMENFASSUNG);
    }
  }, [keineEvaluierung, form.getFieldValue('keineEvaluierung')]);

  const question1To3AnsweredYes =
    form.getFieldValue('minErfuellungsaufwandFuerBuerger') === true ||
    form.getFieldValue('minErfuellungsaufwandFuerWirtschaft') === true ||
    form.getFieldValue('minErfuellungsaufwandFuerVerwaltung') === true;

  const questions4To7 =
    form.getFieldValue('hoherFinanziellerGesamtaufwand') === true ||
    form.getFieldValue('grosseUnsicherheiten') === true ||
    form.getFieldValue('besonderePolitischeBedeutung') === true ||
    form.getFieldValue('gruendeEvaluierungNachJahren') === true;

  return (
    <div>
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.evaluierung.gruendeFuerVerzicht.linkName`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={onSave(form)}
        isDirty={() => isDirty}
        showMandatoryFieldInfo
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        handleFormChanges={() => {
          setIsDirty(true);
          if (props.handleFormChange) props.handleFormChange();
        }}
      >
        <div>
          {t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.paragraph1`)}
          <InfoComponent
            title={t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.infoDrawerTitle`)}
            children={
              <p
                dangerouslySetInnerHTML={{
                  __html: t('egfa.modules.evaluierung.gruendeFuerVerzicht.content.infoDrawerContent', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              ></p>
            }
          />
        </div>
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.paragraph2`),
          }}
        />
        <Form.Item
          name="keineEvaluierung"
          label={<span>{t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.optionFrage`)}</span>}
        >
          <Radio.Group className="horizontal-radios">
            <Radio
              aria-label={t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.optionYes`)}
              value={true}
              className="horizontal-radios"
            >
              {t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.optionYes`)}
            </Radio>

            <Radio
              aria-label={t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.optionNo`)}
              value={false}
              className="horizontal-radios"
            >
              {t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.optionNo`)}
            </Radio>
          </Radio.Group>
        </Form.Item>
        {form.getFieldValue('keineEvaluierung') === true && (
          <Form.Item
            rules={[
              {
                required: question1To3AnsweredYes && form.getFieldValue('keineEvaluierung') === true,
                whitespace: true,
                message: t('egfa.modules.validationMessages.defaultVoraussetzungen'),
              },
            ]}
            name="keineEvaluierungBegruendung"
            label={<span>{t(`egfa.modules.evaluierung.gruendeFuerVerzicht.content.additionalText`)}</span>}
          >
            <TextArea rows={8} />
          </Form.Item>
        )}
      </FormWrapper>
    </div>
  );
}
