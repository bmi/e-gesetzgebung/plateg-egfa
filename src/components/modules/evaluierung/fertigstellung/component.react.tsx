// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './fertigstellung.less';

import React from 'react';

import { EgfaModuleEvaluierungEntityDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { ModulesListController } from '../../../egfa/main/modules-list/controller';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungWrapper } from '../../general/zusammenfassung-wrapper/component.react';
import { EvalueirungFertigstellung } from './evalueirung-fertigstellung/component.react';

export interface FertigstellungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  handleFormChange?: () => void;
  previousLinkName: string;
  ergebnisdokumentation?: boolean;
}

export function Fertigstellung(props: FertigstellungProps<EgfaModuleEvaluierungEntityDTO>): React.ReactElement {
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const PDFExportBtn = ctrl.getPDFExportAction(
    props.formData?.egfaId as string,
    props.moduleName,
    props.formData?.status,
  );
  if (!props.ergebnisdokumentation) {
    return (
      <div className="evaluierung-fertigstellung">
        <ZusammenfassungWrapper
          egfaId={props.egfaId}
          prevPage={`${props.moduleName}/${props.previousLinkName}`}
          onSave={props.onSave(undefined, EgfaModuleStatusType.VollstaendigBearbeitet)}
          moduleData={props.formData}
          moduleNameRoute={props.moduleName}
          additionalActions={PDFExportBtn}
        >
          <EvalueirungFertigstellung
            formData={props.formData}
            ergebnisdokumentation={false}
            onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
          />
        </ZusammenfassungWrapper>
      </div>
    );
  } else {
    return <EvalueirungFertigstellung formData={props.formData} ergebnisdokumentation={false} />;
  }
}
