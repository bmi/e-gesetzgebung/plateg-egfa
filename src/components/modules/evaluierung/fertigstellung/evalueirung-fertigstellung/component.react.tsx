// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { AktionType, EgfaModuleEvaluierungEntityDTO } from '@plateg/rest-api';

import { routes } from '../../../../../shares/routes';
import { checkPruefungErforderlichkeitContainTrueAnswer } from '../../pruefung-erforderlichkeit/component.react';

interface EvalueirungFertigstellungProps {
  formData: EgfaModuleEvaluierungEntityDTO | undefined;
  ergebnisdokumentation: boolean;
  onSave?: () => void;
}
interface EvalueirungFertigstellungState {
  pruefungErforderlichkeitContainTrueAnswer: boolean;
  pruefung1to3Questions?: boolean;
}
export const EvalueirungFertigstellung = (props: EvalueirungFertigstellungProps): React.ReactElement => {
  const { t } = useTranslation();
  const [state, setState] = useState<EvalueirungFertigstellungState>({
    pruefungErforderlichkeitContainTrueAnswer: false,
  });
  const [form] = Form.useForm();
  useEffect(() => {
    if (props.formData) {
      form.setFieldsValue(props.formData);
      const pruefungErforderlichkeitContainTrueAnswer = checkPruefungErforderlichkeitContainTrueAnswer(form, t);
      const pruefung1to3Questions = checkPruefungErforderlichkeitContainTrueAnswer(form, t, 'schwellenWerte');
      setState({
        ...state,
        pruefungErforderlichkeitContainTrueAnswer: pruefungErforderlichkeitContainTrueAnswer,
        pruefung1to3Questions: pruefung1to3Questions,
      });
    }
  }, [props.formData]);
  const noEvaluation = t('egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtDurchgefuehrt');
  const nichtVorgesehen = t('egfa.modules.evaluierung.zusammenfassung.evaluierungErforderlichkeitNichtVorgesehen');
  return (
    <>
      <div className="section-evaluierung">
        <div className="title">
          <span>{t(`egfa.modules.evaluierung.zusammenfassung.linkName`)}</span>
          {props.formData?.egfaId &&
            props.formData?.aktionen?.includes(AktionType.Schreiben) &&
            !props.ergebnisdokumentation && (
              <span className="zur_bearbeitung">
                <Link
                  style={{ whiteSpace: 'nowrap' }}
                  to={`/egfa/${props.formData.egfaId}/${routes.MODULE}/${routes.EVALUIERUNG}/${routes.ZUSAMMENFASSUNG}`}
                  onClick={props.onSave}
                >
                  {t(`egfa.modules.general.summaryBearbeitung`)}
                </Link>
              </span>
            )}
        </div>
      </div>

      {!state.pruefungErforderlichkeitContainTrueAnswer && <>{noEvaluation}</>}

      {state.pruefungErforderlichkeitContainTrueAnswer && !form.getFieldValue('keineEvaluierung') && (
        <>
          <Title level={3}>{t(`egfa.modules.evaluierung.fertigstellung.aussagenZurDurchfuehrung`)}</Title>

          <p className="summary-text">{props.formData?.zusammenfassung}</p>
        </>
      )}

      {state.pruefungErforderlichkeitContainTrueAnswer &&
        state.pruefung1to3Questions &&
        form.getFieldValue('keineEvaluierung') && (
          <>
            <Title level={3}>{t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`)}</Title>

            <p className="summary-text">{props.formData?.keineEvaluierungBegruendung}</p>
          </>
        )}

      {state.pruefungErforderlichkeitContainTrueAnswer &&
        !state.pruefung1to3Questions &&
        form.getFieldValue('keineEvaluierung') &&
        props?.formData?.keineEvaluierungBegruendung &&
        props?.formData?.keineEvaluierungBegruendung?.length > 0 && (
          <>
            <Title level={3}>{t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`)}</Title>
            <p className="summary-text">{props.formData?.keineEvaluierungBegruendung}</p>
          </>
        )}

      {state.pruefungErforderlichkeitContainTrueAnswer &&
        !state.pruefung1to3Questions &&
        form.getFieldValue('keineEvaluierung') &&
        !props.formData?.keineEvaluierungBegruendung && (
          <>
            <Title level={3}>{t(`egfa.modules.evaluierung.fertigstellung.aussagenZurNichtDurchfuehrung`)}</Title>
            {nichtVorgesehen}
          </>
        )}
    </>
  );
};
