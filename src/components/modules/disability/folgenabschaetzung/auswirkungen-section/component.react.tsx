// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDisabilityEntityDTO } from '@plateg/rest-api';
import { Constants } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';
import { CheckboxSectionProps } from '../../hauptpruefung/hauptpruefung-checkbox-section/component.react';
import { AuswirkungenList } from './auswirkungen-list/component.react';

interface AuswirkungenSectionProps {
  formData?: EgfaModuleDisabilityEntityDTO;
}
export function AuswirkungenSection(props: AuswirkungenSectionProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  const [selectedItemLists, setSelectedItemLists] = useState<{
    veraenderung?: string[];
    teilhabe?: string[];
    zugang?: string[];
  }>();

  useEffect(() => {
    const sections: CheckboxSectionProps[] = t(`egfa.modules.disability.hauptpruefung.sections`, {
      returnObjects: true,
    }) as [];
    setSelectedItemLists(
      Object.fromEntries(
        sections.map((section) => [
          [section.name],
          section.items
            .filter((item) => {
              return props.formData?.[item.name as keyof EgfaModuleDisabilityEntityDTO] === true;
            })
            .map((item) => {
              const isSonstiges = item.name === section.items[section.items.length - 1].name;
              const additionalText = (() => {
                if (isSonstiges) {
                  const sonstigesContent = props.formData?.[
                    section.sonstigesName as keyof EgfaModuleDisabilityEntityDTO
                  ] as string;
                  return sonstigesContent ? `: ${sonstigesContent}` : '';
                }
                return '';
              })();
              return (
                <>
                  {item.title}
                  {additionalText}
                </>
              );
            }),
        ]),
      ) as Object,
    );
  }, [props.formData]);

  return (
    <div className="disability-auswirkungen">
      <Title level={2}>{t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.title`)}</Title>
      <p>{t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.intro`)}</p>

      <AuswirkungenList
        title={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.veraenderungLabel`)}
        items={selectedItemLists?.veraenderung}
      />
      <AuswirkungenList
        title={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.teilhabeLabel`)}
        items={selectedItemLists?.teilhabe}
      />
      <AuswirkungenList
        title={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.zugangLabel`)}
        items={selectedItemLists?.zugang}
      />

      <p>{t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.leitfragen.title`)}</p>
      <div
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.disability.folgenabschaetzung.auswirkungen.leitfragen.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      />

      <p>{t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.hinweise.title`)}</p>
      <div
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.disability.folgenabschaetzung.auswirkungen.hinweise.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      />

      <EgfaInfoCollapse
        panelKey="2"
        headerText={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.info.title`)}
        text={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.info.text`)}
      />

      <Form.Item
        label={t(`egfa.modules.disability.folgenabschaetzung.auswirkungen.input.label`)}
        name="auswirkungenSummary"
        rules={[
          {
            required: true,
            whitespace: true,
            message: t('egfa.modules.disability.folgenabschaetzung.auswirkungen.input.error'),
          },
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('egfa.modules.disability.folgenabschaetzung.textLengthError', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
            }),
          },
        ]}
      >
        <TextArea rows={8} />
      </Form.Item>
    </div>
  );
}
