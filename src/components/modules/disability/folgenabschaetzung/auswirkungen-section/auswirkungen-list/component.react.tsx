// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { CheckOutlined } from '@plateg/theme';

interface AuswirkungenListProps {
  title: string;
  items?: string[];
}
export function AuswirkungenList(props: AuswirkungenListProps): React.ReactElement {
  if (!props.items?.length) {
    return <></>;
  }
  return (
    <div className="auswirkungen-list-container">
      <p>{props.title}</p>
      <ul className="egfa-checked-list auswirkungen-list">
        {props.items.map((item, index) => {
          return (
            <li key={`auswirkung-${props.title}-${index}`}>
              <CheckOutlined />
              &nbsp;
              <span>{item}</span>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
