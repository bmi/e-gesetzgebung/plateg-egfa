// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaInfoCollapse } from '../../../general/egfa-info-collapse/component.react';

export function FolgenabschaetzungIntroSection(): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className="folgenabschaetzung-intro">
      <p
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.disability.folgenabschaetzung.intro.text`, {
            interpolation: { escapeValue: false },
          }),
        }}
      />
      <EgfaInfoCollapse
        panelKey="1"
        headerText={t(`egfa.modules.disability.folgenabschaetzung.intro.info.title`)}
        text={t(`egfa.modules.disability.folgenabschaetzung.intro.info.text`)}
      />
    </div>
  );
}
