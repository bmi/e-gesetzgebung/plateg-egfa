// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Constants } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export function MassnahmenSection(): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  return (
    <div className="disability-auswirkungen">
      <Title level={2}>{t(`egfa.modules.disability.folgenabschaetzung.massnahmen.title`)}</Title>
      <div
        dangerouslySetInnerHTML={{
          __html: t('egfa.modules.disability.folgenabschaetzung.massnahmen.intro.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      />
      <Form.Item
        label={t(`egfa.modules.disability.folgenabschaetzung.massnahmen.input.label`)}
        name="massnahmenSummary"
        rules={[
          {
            max: Constants.TEXT_AREA_LENGTH,
            message: t('egfa.modules.disability.folgenabschaetzung.textLengthError', {
              maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
            }),
          },
        ]}
      >
        <TextArea rows={8} />
      </Form.Item>
    </div>
  );
}
