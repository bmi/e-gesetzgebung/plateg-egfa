// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './folgenabschaetzung.less';

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDisabilityEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { AuswirkungenSection } from './auswirkungen-section/component.react';
import { FolgenabschaetzungIntroSection } from './intro-section/component.react';
import { MassnahmenSection } from './massnahmen-section/component.react';

export interface ModuleDisabilityFolgenabschaetzungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName: string;
  sectionName: string;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
}
export function ModuleDisabilityFolgenabschaetzung(
  props: ModuleDisabilityFolgenabschaetzungProps<EgfaModuleDisabilityEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    form.setFieldsValue(props.formData);
    setIsDirty(false);
  }, [props.formData]);

  return (
    <div className="disability-folgenabschaetzung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.folgenabschaetzung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <FolgenabschaetzungIntroSection />
        {props.referenzen}
        <AuswirkungenSection formData={props.formData} />
        <MassnahmenSection />
      </FormWrapper>
    </div>
  );
}
