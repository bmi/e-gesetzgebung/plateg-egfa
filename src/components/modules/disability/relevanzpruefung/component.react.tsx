// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './relevanzpruefung.less';

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDisabilityEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { RelevanzSection1 } from './relevanz-section-1/component.react';
import { RelevanzSection2 } from './relevanz-section-2/component.react';

export interface ModuleDisabilityRelevanzpruefungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName?: string;
  previousLinkName?: string;
  sectionName: string;
  updateItemDisableStatus?: (
    itemName: string | string[] | { name: string; isDisabled: boolean }[],
    isDisabled?: boolean,
  ) => void;
  handleFormChange?: () => void;
}
export function ModuleDisabilityRelevanzpruefung(
  props: ModuleDisabilityRelevanzpruefungProps<EgfaModuleDisabilityEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [mittelbarBetroffen, setMittelbarBetroffen] = useState<boolean>();
  const [unmittelbarBetroffen, setUnmittelbarBetroffen] = useState<boolean>();
  const [hasNoInfluence, setHasNoInfluence] = useState<boolean>();
  const [nextLink, setNextLink] = useState<string>(props.nextLinkName || '');
  const [isDirty, setIsDirty] = useState(false);

  const initalValue = {
    faktorBehinderteUnmittelbarBetroffen: false,
    faktorBehinderteMittelbarBetroffen: false,
    faktorAnhaltspunkteUnterschiedlichBetroffen: false,
    faktorAnhaltspunkteProfitierenNichtGleich: false,
    faktorAnhaltspunkteBesondereBelastungen: false,
  };

  //set formvalues and state when formData changes
  useEffect(() => {
    if (props.formData) {
      const data = { ...initalValue, ...props.formData };
      form.setFieldsValue(data);
      setMittelbarBetroffen(data.faktorBehinderteMittelbarBetroffen);
      setUnmittelbarBetroffen(data.faktorBehinderteUnmittelbarBetroffen);
      setHasNoInfluence(!data.influenceExpected);
      setIsDirty(false);
    }
  }, [props.formData]);

  //reset section 2 fields if both section 1 items are false
  useEffect(() => {
    if (mittelbarBetroffen === false && unmittelbarBetroffen === false) {
      setHasNoInfluence(true);
      form.setFieldsValue({
        faktorAnhaltspunkteUnterschiedlichBetroffen: false,
        faktorAnhaltspunkteProfitierenNichtGleich: false,
        faktorAnhaltspunkteBesondereBelastungen: false,
      });
    }
  }, [mittelbarBetroffen, unmittelbarBetroffen]);

  //set summary and sidebar status + next page link depending on influence status
  useEffect(() => {
    const noInfluenceText = t(`egfa.modules.${props.moduleName}.relevanzpruefung.noInfluence`);
    const itemsToBeChanged = [];
    if (hasNoInfluence === true) {
      form.setFieldsValue({
        influenceExpected: false,
        influenceSummary: noInfluenceText,
      });
      setNextLink(routes.ZUSAMMENFASSUNG);
      itemsToBeChanged.push(
        ...[
          { name: routes.HAUPTPRUEFUNG, isDisabled: true },
          { name: routes.FOLGENABSCHAETZUNG, isDisabled: true },
        ],
      );
      itemsToBeChanged.push({
        name: routes.FERTIGSTELLUNG,
        isDisabled: false,
      });
    }
    if (hasNoInfluence === false) {
      form.setFieldsValue({
        influenceExpected: true,
        influenceSummary:
          props.formData?.influenceSummary === noInfluenceText ? null : props.formData?.influenceSummary,
      });
      setNextLink(props.nextLinkName || routes.ZUSAMMENFASSUNG);
      itemsToBeChanged.push(
        ...[
          { name: routes.HAUPTPRUEFUNG, isDisabled: false },
          { name: routes.FOLGENABSCHAETZUNG, isDisabled: false },
        ],
      );
      itemsToBeChanged.push({
        name: routes.FERTIGSTELLUNG,
        isDisabled: props.formData?.influenceSummary === noInfluenceText || !props.formData?.influenceSummary?.length,
      });
    }
    props.updateItemDisableStatus?.(itemsToBeChanged);
  }, [hasNoInfluence]);

  return (
    <div className="relevanzpruefung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.relevanzpruefung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${nextLink}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={false}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        <p
          dangerouslySetInnerHTML={{
            __html: t(`egfa.modules.${props.moduleName}.relevanzpruefung.intro`, {
              interpolation: { escapeValue: false },
            }),
          }}
        />
        <EgfaInfoCollapse
          panelKey="1"
          headerText={t(`egfa.modules.${props.moduleName}.relevanzpruefung.additionalInfos.info1.title`)}
          text={t(`egfa.modules.${props.moduleName}.relevanzpruefung.additionalInfos.info1.text`)}
        />
        <EgfaInfoCollapse
          panelKey="2"
          headerText={t(`egfa.modules.${props.moduleName}.relevanzpruefung.additionalInfos.info2.title`)}
          text={t(`egfa.modules.${props.moduleName}.relevanzpruefung.additionalInfos.info2.text`)}
        />
        <RelevanzSection1
          setMittelbarBetroffen={setMittelbarBetroffen}
          setUnmittelbarBetroffen={setUnmittelbarBetroffen}
        />
        <RelevanzSection2
          isDisabled={mittelbarBetroffen !== true && unmittelbarBetroffen !== true}
          setHasNoInfluence={setHasNoInfluence}
          form={form}
        />
      </FormWrapper>
    </div>
  );
}
