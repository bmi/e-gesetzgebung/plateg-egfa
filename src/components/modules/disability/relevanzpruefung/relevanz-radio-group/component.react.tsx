// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Radio, RadioChangeEvent } from 'antd';
import React from 'react';

export interface RelevanzRadioGroupProps {
  name: string;
  onChange?: (e: RadioChangeEvent) => void;
  isDisabled?: boolean;
}
export function RelevanzRadioGroup(props: RelevanzRadioGroupProps): React.ReactElement {
  return (
    <Radio.Group
      id={`id-${props.name}`}
      className="horizontal-radios"
      onChange={props.onChange}
      disabled={props.isDisabled}
      {...props}
    >
      <Radio id={`egfa-relevanzpruefung-${props.name}Ja-radio`} className="horizontal-radios" value={true}>
        Ja
      </Radio>
      <Radio id={`egfa-relevanzpruefung-${props.name}Nein-radio`} className="horizontal-radios" value={false}>
        Nein
      </Radio>
    </Radio.Group>
  );
}
