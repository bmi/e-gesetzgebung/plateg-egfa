// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDisabilityEntityDTO } from '@plateg/rest-api';

import { RelevanzRadioGroup } from '../relevanz-radio-group/component.react';

interface RelevanzSection2Props {
  isDisabled: boolean;
  setHasNoInfluence: (hasNoInfluence: boolean) => void;
  form: FormInstance;
}
export function RelevanzSection2(props: RelevanzSection2Props): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  const setHasNoInfluence = () => {
    const vals = props.form.getFieldsValue() as EgfaModuleDisabilityEntityDTO;
    props.setHasNoInfluence(
      !vals.faktorAnhaltspunkteUnterschiedlichBetroffen &&
        !vals.faktorAnhaltspunkteProfitierenNichtGleich &&
        !vals.faktorAnhaltspunkteBesondereBelastungen,
    );
  };

  if (props.isDisabled) {
    return <></>;
  }

  return (
    <div>
      <Title level={2}>{t(`egfa.modules.disability.relevanzpruefung.section2.title`)}</Title>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t(`egfa.modules.disability.relevanzpruefung.section2.gleicheWeise.title`)}</legend>
        <Form.Item
          name="faktorAnhaltspunkteUnterschiedlichBetroffen"
          label={t(`egfa.modules.disability.relevanzpruefung.section2.gleicheWeise.title`)}
        >
          <RelevanzRadioGroup
            name="faktorAnhaltspunkteUnterschiedlichBetroffen"
            isDisabled={props.isDisabled}
            onChange={() => setHasNoInfluence()}
          />
        </Form.Item>
      </fieldset>
      <fieldset className="fieldset-form-items">
        <legend className="seo">
          {t(`egfa.modules.disability.relevanzpruefung.section2.vorgeseheneVorteile.title`)}
        </legend>
        <Form.Item
          name="faktorAnhaltspunkteProfitierenNichtGleich"
          label={t(`egfa.modules.disability.relevanzpruefung.section2.vorgeseheneVorteile.title`)}
        >
          <RelevanzRadioGroup
            name="faktorAnhaltspunkteProfitierenNichtGleich"
            isDisabled={props.isDisabled}
            onChange={() => setHasNoInfluence()}
          />
        </Form.Item>
      </fieldset>
      <fieldset className="fieldset-form-items">
        <legend className="seo">
          {t(`egfa.modules.disability.relevanzpruefung.section2.besondersBelastet.title`)}
        </legend>
        <Form.Item
          name="faktorAnhaltspunkteBesondereBelastungen"
          label={t(`egfa.modules.disability.relevanzpruefung.section2.besondersBelastet.title`)}
        >
          <RelevanzRadioGroup
            name="faktorAnhaltspunkteBesondereBelastungen"
            isDisabled={props.isDisabled}
            onChange={() => setHasNoInfluence()}
          />
        </Form.Item>
      </fieldset>
    </div>
  );
}
