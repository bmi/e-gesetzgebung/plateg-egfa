// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

import { RelevanzRadioGroup } from '../relevanz-radio-group/component.react';

interface RelevanzSection1Props {
  setMittelbarBetroffen: (mittelbarBetroffen: boolean) => void;
  setUnmittelbarBetroffen: (unmittelbarBetroffen: boolean) => void;
}

export function RelevanzSection1(props: RelevanzSection1Props): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();

  return (
    <>
      <Title level={2}>{t(`egfa.modules.disability.relevanzpruefung.section1.title`)}</Title>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t(`egfa.modules.disability.relevanzpruefung.section1.unmittelbar.title`)}</legend>
        <FormItemWithInfo
          name="faktorBehinderteUnmittelbarBetroffen"
          label={
            <span>
              {t(`egfa.modules.disability.relevanzpruefung.section1.unmittelbar.title`)}
              <InfoComponent title={t(`egfa.modules.disability.relevanzpruefung.section1.unmittelbar.title`)} withLabel>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t(`egfa.modules.disability.relevanzpruefung.section1.unmittelbar.info`),
                  }}
                />
              </InfoComponent>
            </span>
          }
        >
          <RelevanzRadioGroup
            onChange={(e) => props.setUnmittelbarBetroffen(e.target.value as boolean)}
            name="faktorBehinderteUnmittelbarBetroffen"
          />
        </FormItemWithInfo>
      </fieldset>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t(`egfa.modules.disability.relevanzpruefung.section1.mittelbar.title`)}</legend>
        <FormItemWithInfo
          name="faktorBehinderteMittelbarBetroffen"
          label={
            <span>
              {t(`egfa.modules.disability.relevanzpruefung.section1.mittelbar.title`)}
              <InfoComponent title={t(`egfa.modules.disability.relevanzpruefung.section1.mittelbar.title`)} withLabel>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t(`egfa.modules.disability.relevanzpruefung.section1.mittelbar.info`),
                  }}
                />
              </InfoComponent>
            </span>
          }
        >
          <RelevanzRadioGroup
            onChange={(e) => props.setMittelbarBetroffen(e.target.value as boolean)}
            name="faktorBehinderteMittelbarBetroffen"
          />
        </FormItemWithInfo>
      </fieldset>
    </>
  );
}
