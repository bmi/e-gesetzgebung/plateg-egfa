// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zusammenfassung.less';

import { Form, Typography } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, EgfaModuleDisabilityEntityDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { Constants, FormWrapper } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../shares/routes';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { ZusammenfassungItem } from '../../general/zusammenfassung-wrapper/zusammenfassung-item/component.react';

export interface ModuleDisabilityZusammenfassungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName: string;
  sectionName: string;
  handleFormChange?: () => void;
}
export function ModuleDisabilityZusammenfassung(
  props: ModuleDisabilityZusammenfassungProps<EgfaModuleDisabilityEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const noAnswer = <i>keine Angabe</i>;
  const isRelevant =
    props.formData?.faktorAnhaltspunkteBesondereBelastungen ||
    props.formData?.faktorAnhaltspunkteProfitierenNichtGleich ||
    props.formData?.faktorAnhaltspunkteUnterschiedlichBetroffen;
  const [isDirty, setIsDirty] = useState(false);

  useEffect(() => {
    form.setFieldsValue(props.formData);
    setIsDirty(false);
  }, [props.formData]);

  return (
    <div className="disability-zusammenfassung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.zusammenfassung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={true}
        handleFormChanges={() => {
          setIsDirty(true);
          props.handleFormChange?.();
        }}
      >
        {!isRelevant && <p>{t(`egfa.modules.${props.moduleName}.fertigstellung.noImpact`)}</p>}
        {isRelevant && (
          <>
            <div className="zusammenfassung-div">
              <ZusammenfassungItem
                title={t(`egfa.modules.${props.moduleName}.zusammenfassung.auswirkungenTitle`)}
                bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.FOLGENABSCHAETZUNG}`}
                onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
                egfaId={props.egfaId}
                allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
              >
                <span>{props.formData?.auswirkungenSummary || noAnswer}</span>
              </ZusammenfassungItem>

              <ZusammenfassungItem
                title={t(`egfa.modules.${props.moduleName}.zusammenfassung.massnahmenTitle`)}
                bearbeitungsLink={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${routes.FOLGENABSCHAETZUNG}`}
                onSave={props.onSave(undefined, EgfaModuleStatusType.InBearbeitung)}
                egfaId={props.egfaId}
                allowedToModify={props.formData?.aktionen?.includes(AktionType.Schreiben)}
              >
                <span>{props.formData?.massnahmenSummary || noAnswer}</span>
              </ZusammenfassungItem>
            </div>
            <Form.Item
              name="influenceSummary"
              label={t(`egfa.modules.${props.moduleName}.zusammenfassung.zusammenfassungTitle`)}
              rules={[
                { required: true, whitespace: true, message: t('egfa.modules.validationMessages.default') },
                {
                  max: Constants.TEXT_AREA_LENGTH,
                  message: t('egfa.modules.disability.folgenabschaetzung.textLengthError', {
                    maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                  }),
                },
              ]}
            >
              <TextArea id="egfa-disabilityAbschliessendeBewertung-txtArea" rows={8} />
            </Form.Item>
          </>
        )}
      </FormWrapper>
    </div>
  );
}
