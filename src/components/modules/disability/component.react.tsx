// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleDisabilityControllerApi,
  EgfaModuleDisabilityEntityDTO,
  EgfaModuleDisabilityEntityResponseDTO,
  EgfaModuleStatusType,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { ExtendedModuleData, ReferenzenTypes } from '../general/controller';
import { ReferenzenGenericComponent } from '../general/referenzen-generic/component.react';
import {
  DemografieReferenzenTypes,
  EnapReferenzenTypes,
  GleichstellungReferenzenTypes,
  GleichwertigkeitReferenzenTypes,
} from '../general/referenzen-generic/referenzen-item/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateDisabilityJSON } from './controller';
import { ModuleDisabilityFolgenabschaetzung } from './folgenabschaetzung/component.react';
import { ModuleDisabilityHauptpruefung } from './hauptpruefung/component.react';
import { ModuleDisabilityRelevanzpruefung } from './relevanzpruefung/component.react';
import { ModuleDisabilityZusammenfassung } from './zusammenfassung/component.react';

export function DisabilityMainstreaming(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [disabilityeData, setDisabilityeData] = useState<EgfaModuleDisabilityEntityDTO & ExtendedModuleData>();
  const egfaDisabilityController = GlobalDI.get<EgfaModuleDisabilityControllerApi>('egfaModuleDisabilityControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [referenzenData, setReferenzenData] = useState<ReferenzenTypes>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaDisabilityController.getEgfaModule12({ egfaId: egfaId });
  };
  const putData = (localData: EgfaModuleDisabilityEntityDTO) => {
    return egfaDisabilityController.modifyEgfaModule12({
      egfaId: egfaId,
      egfaModuleDisabilityEntityDTO: localData,
    });
  };

  useEffect(() => {
    props.setEgfaName(routes.DISABILITY);
  }, []);
  const EGFA_LINK = `#/egfa/${egfaId}/module`;
  useEffect(() => {
    if (disabilityeData && disabilityeData.referenzenData) {
      setReferenzenData(disabilityeData.referenzenData);
    }
  }, [disabilityeData]);
  const PageRoutingDM = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (
      itemName: string | string[] | { name: string; isDisabled: boolean }[],
      isDisabled?: boolean,
    ) => void,
    handleFormChange?: () => void,
    _updateMenuItems?: (items: any, isModuleDisabled?: boolean) => void,
    egfaErsteller?: UserEntityResponseDTO,
  ): React.ReactElement => {
    const contentPageProps = { egfaId, setFormInstance, formData: disabilityeData, onSave, handleFormChange };
    useEffect(() => {
      const formData = disabilityeData;
      const itemsToBeChanged = [];

      if (!updateItemDisableStatus || !formData) return;

      if (formData.status === EgfaModuleStatusType.InBearbeitung) {
        const isExpected = formData.influenceExpected;
        const isSummary = formData.influenceSummary;

        itemsToBeChanged.push(
          { name: routes.HAUPTPRUEFUNG, isDisabled: !isExpected },
          { name: routes.FOLGENABSCHAETZUNG, isDisabled: !isExpected },
          { name: routes.FERTIGSTELLUNG, isDisabled: isExpected && !isSummary },
        );
      } else {
        const isExpected = formData.influenceExpected;
        const isSummary = formData.influenceSummary;

        itemsToBeChanged.push({ name: routes.FERTIGSTELLUNG, isDisabled: isExpected && !isSummary });
      }

      updateItemDisableStatus(itemsToBeChanged, false);
    }, [disabilityeData]);

    const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
    const PDFExportBtn = ctrl.getPDFExportAction(
      disabilityeData?.egfaId as string,
      moduleName,
      disabilityeData?.status,
    );

    const referenzenDataProps = {
      egfaLink: EGFA_LINK,
      egfaErsteller: egfaErsteller,
    };

    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName={routes.DISABILITY}
            isLight={false}
            previousLinkName={routes.FOLGENABSCHAETZUNG}
            customEditLink={`/egfa/${egfaId}/${routes.MODULE}/${routes.DISABILITY}/${routes.ZUSAMMENFASSUNG}`}
            additionalActions={PDFExportBtn}
          />
        </Route>
        {(disabilityeData?.aktionen?.includes(AktionType.Schreiben) === false ||
          disabilityeData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.DISABILITY}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.DISABILITY}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.RELEVANZPRUEFUNG}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.RELEVANZPRUEFUNG}`]}>
          <ModuleDisabilityRelevanzpruefung
            {...contentPageProps}
            moduleName={`${routes.DISABILITY}`}
            sectionName={routes.RELEVANZPRUEFUNG}
            previousLinkName={routes.EINLEITUNG}
            nextLinkName={routes.HAUPTPRUEFUNG}
            updateItemDisableStatus={updateItemDisableStatus}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.HAUPTPRUEFUNG}`]}>
          <ModuleDisabilityHauptpruefung
            {...contentPageProps}
            moduleName={`${routes.DISABILITY}`}
            sectionName={routes.HAUPTPRUEFUNG}
            previousLinkName={routes.RELEVANZPRUEFUNG}
            nextLinkName={routes.FOLGENABSCHAETZUNG}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.disabilityEnapReferenzen,
                    conditionsList: [
                      EnapReferenzenTypes.hasSelectedCheckboxSdg1,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg31,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg41,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg85,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg10,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg11,
                      EnapReferenzenTypes.hasSelectedCheckboxSdg16,
                    ],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.disabilityGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedMobilitaet,
                      GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge,
                      GleichwertigkeitReferenzenTypes.hasSelectedEngagement,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.disabilityDemografieReferenzen,
                    conditionsList: [DemografieReferenzenTypes.hasSelectedArbeit],
                    moduleName: routes.DEMOGRAFIE_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
            referenzen2={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.disabilityEnapReferenzen,
                    conditionsList: [EnapReferenzenTypes.hasSelectedCheckboxSdg11],
                    moduleName: routes.ENAP,
                    extendedInfo: true,
                  },
                  {
                    referenzenData: referenzenData?.disabilityGleichwertigkeitReferenzen,
                    conditionsList: [
                      GleichwertigkeitReferenzenTypes.hasSelectedMobilitaet,
                      GleichwertigkeitReferenzenTypes.hasSelectedDaseinsvorsoge,
                    ],
                    moduleName: routes.GLEICHWERTIGKEITS_CHECK,
                    extendedInfo: true,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.FOLGENABSCHAETZUNG}`]}>
          <ModuleDisabilityFolgenabschaetzung
            {...contentPageProps}
            moduleName={`${routes.DISABILITY}`}
            sectionName={routes.FOLGENABSCHAETZUNG}
            previousLinkName={routes.HAUPTPRUEFUNG}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            referenzen={
              <ReferenzenGenericComponent
                {...referenzenDataProps}
                referenzenDataConfig={[
                  {
                    referenzenData: referenzenData?.disabilityGleichstellungReferenzen,
                    conditionsList: [
                      GleichstellungReferenzenTypes.hasSelectedExplicitGoal,
                      GleichstellungReferenzenTypes.hasSelectedMenAndWomanInfluenced,
                    ],
                    moduleName: routes.GLEICHSTELLUNG,
                  },
                ]}
              />
            }
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}/${routes.ZUSAMMENFASSUNG}`]}>
          <ModuleDisabilityZusammenfassung
            {...contentPageProps}
            moduleName={`${routes.DISABILITY}`}
            sectionName={routes.FOLGENABSCHAETZUNG}
            previousLinkName={
              contentPageProps.formData?.influenceExpected ? routes.FOLGENABSCHAETZUNG : routes.RELEVANZPRUEFUNG
            }
            nextLinkName={routes.FERTIGSTELLUNG}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.DISABILITY}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.DISABILITY}/${routes.EINLEITUNG}`} />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleDisabilityEntityDTO, EgfaModuleDisabilityEntityResponseDTO>
      setFormData={setDisabilityeData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRoutingDM}
      pageName={currentPageName}
      customResultPage={routes.FERTIGSTELLUNG}
      generateJson={generateDisabilityJSON}
    />
  );
}
