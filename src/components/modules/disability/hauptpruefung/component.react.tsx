// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './hauptpruefung.less';

import { Form, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaModuleDisabilityEntityDTO } from '@plateg/rest-api';
import { FormWrapper } from '@plateg/theme';

import { routes } from '../../../../shares/routes';
import { EgfaInfoCollapse } from '../../general/egfa-info-collapse/component.react';
import { ContentPageProps } from '../../general/simple-module/auswirkungen/component.react';
import { CheckboxSectionProps, HauptpruefungCheckboxSection } from './hauptpruefung-checkbox-section/component.react';
import { KonsistenzPruefung } from './konsistenzpruefung/component.react';

export interface ModuleDisabilityHauptpruefungProps<P> extends ContentPageProps<P> {
  moduleName: string;
  nextLinkName: string;
  previousLinkName?: string;
  sectionName: string;
  handleFormChange?: () => void;
  referenzen?: React.ReactElement;
  referenzen2?: React.ReactElement;
}
export function ModuleDisabilityHauptpruefung(
  props: ModuleDisabilityHauptpruefungProps<EgfaModuleDisabilityEntityDTO>,
): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const [isConsistent, setIsConsistent] = useState(false);
  const [isDirty, setIsDirty] = useState(false);

  const checkboxSections: CheckboxSectionProps[] = t(`egfa.modules.disability.hauptpruefung.sections`, {
    returnObjects: true,
  }) as [];

  useEffect(() => {
    onLoad();
    setIsDirty(false);
  }, [props.formData]);

  const onLoad = () => {
    const data = props.formData;
    let consistent = false;
    const customFormVals = Object.fromEntries(
      checkboxSections.map((section) => [
        [section.name],
        section.items
          .filter((item) => {
            return data?.[item.name as keyof EgfaModuleDisabilityEntityDTO] === true;
          })
          .map((item) => {
            consistent = true;
            return item.name;
          }),
      ]),
    ) as Object;
    form.setFieldsValue({
      ...data,
      ...customFormVals,
    });
    setIsConsistent(consistent);
  };

  const onFormValChange = () => {
    const checkboxFields = checkboxSections.flatMap((section) => {
      return section.items.map((item) => {
        return item.name;
      });
    });
    const selectedCheckboxes = checkboxSections
      .flatMap((section) => {
        return section.name;
      })
      .flatMap((name) => {
        return form.getFieldValue(name) as string;
      });
    setIsConsistent(selectedCheckboxes?.length > 0);
    form.setFieldsValue(
      Object.fromEntries(
        checkboxFields.map((checkboxSection) => [[checkboxSection], selectedCheckboxes.includes(checkboxSection)]),
      ),
    );
    props.handleFormChange?.();
  };

  return (
    <div className="relevanzpruefung">
      <FormWrapper
        projectName="eGFA"
        title={<Title level={1}>{t(`egfa.modules.${props.moduleName}.hauptpruefung.title`)}</Title>}
        previousPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.previousLinkName as string}`}
        nextPage={`/egfa/${props.egfaId}/${routes.MODULE}/${props.moduleName}/${props.nextLinkName}`}
        saveDraft={props.onSave(form)}
        isDirty={() => isDirty}
        form={form}
        formInitialValue={{}}
        setFormInstance={props.setFormInstance}
        showMandatoryFieldInfo={false}
        handleFormChanges={() => {
          setIsDirty(true);
          onFormValChange();
        }}
      >
        {checkboxSections.map((section, ind) => {
          let referenzen;
          if (ind === 1) {
            referenzen = props.referenzen;
          } else if (ind === 2) {
            referenzen = props.referenzen2;
          }
          return (
            <HauptpruefungCheckboxSection
              sonstigesIsVisibleInitally={
                props.formData?.[
                  section.items[section.items.length - 1].name as keyof EgfaModuleDisabilityEntityDTO
                ] as boolean
              }
              checkboxProps={section}
              form={form}
              referenzen={referenzen}
            />
          );
        })}
        <KonsistenzPruefung consistent={isConsistent} />
        <EgfaInfoCollapse
          panelKey="1"
          headerText={t(`egfa.modules.${props.moduleName}.hauptpruefung.info1.title`)}
          text={t(`egfa.modules.${props.moduleName}.hauptpruefung.info1.text`)}
        />
        <EgfaInfoCollapse
          panelKey="2"
          headerText={t(`egfa.modules.${props.moduleName}.hauptpruefung.info2.title`)}
          text={t(`egfa.modules.${props.moduleName}.hauptpruefung.info2.text`)}
        />
        <EgfaInfoCollapse
          panelKey="3"
          headerText={t(`egfa.modules.${props.moduleName}.hauptpruefung.info3.title`)}
          text={t(`egfa.modules.${props.moduleName}.hauptpruefung.info3.text`)}
        />
      </FormWrapper>
    </div>
  );
}
