// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './hauptpruefung-checkbox-section.less';

import { Form, FormInstance, Input } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CheckboxGroupWithInfo, InfoComponent } from '@plateg/theme';

export interface CheckboxSectionItem {
  name: string;
  title: string;
  info?: { title: string; content: string };
}

export interface CheckboxSectionProps {
  title: string;
  name: string;
  frageIndex: number;
  sonstigesName: string;
  sonstigesTitle?: string;
  items: CheckboxSectionItem[];
}

interface HauptpruefungCheckboxSectionProps {
  checkboxProps: CheckboxSectionProps;
  sonstigesIsVisibleInitally: boolean;
  form: FormInstance;
  referenzen?: React.ReactElement;
}
export function HauptpruefungCheckboxSection(props: HauptpruefungCheckboxSectionProps): React.ReactElement {
  const { t } = useTranslation();
  const [sonstigesIsVisible, setSonstigesIsVisible] = useState(props.sonstigesIsVisibleInitally);

  useEffect(() => {
    if (!sonstigesIsVisible) {
      props.form.setFieldsValue({ [props.checkboxProps.sonstigesName]: null });
    }
  }, [sonstigesIsVisible]);

  return (
    <div className="hauptpruefung-checkbox-section">
      <fieldset className="fieldset-form-items">
        <legend>{props.checkboxProps.title}</legend>
        <Form.Item name={props.checkboxProps.name}>
          <CheckboxGroupWithInfo
            name={props.checkboxProps.name}
            id={`optionen-${props.checkboxProps.name}`}
            items={[
              ...props.checkboxProps.items.map((item, index) => {
                return {
                  label: (
                    <span className="nonBreakingArea">
                      <span className="normalBreakingArea">{item.title}</span>
                      &nbsp;
                      {item.info && (
                        <InfoComponent title={item.info.title}>
                          <p
                            dangerouslySetInnerHTML={{
                              __html: item.info.content,
                            }}
                          ></p>
                        </InfoComponent>
                      )}
                    </span>
                  ),
                  value: item.name,
                  title: item.title,
                  onChange: (e: CheckboxChangeEvent) => {
                    if (index + 1 === props.checkboxProps.items.length) {
                      setSonstigesIsVisible(e.target.checked);
                    }
                  },
                };
              }),
            ]}
          />
        </Form.Item>
      </fieldset>
      {sonstigesIsVisible && (
        <Form.Item
          name={props.checkboxProps.sonstigesName}
          label={
            props.checkboxProps.sonstigesTitle ??
            t(`egfa.modules.disability.hauptpruefung.checkboxSection.sonstigesTitle`)
          }
        >
          <Input />
        </Form.Item>
      )}
      {props.referenzen}
    </div>
  );
}
