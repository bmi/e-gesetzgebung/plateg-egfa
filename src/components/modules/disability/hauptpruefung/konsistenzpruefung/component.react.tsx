// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BASE_PATH } from '@plateg/rest-api';

interface KonsistenzPruefungProps {
  consistent: boolean;
}
export function KonsistenzPruefung(props: KonsistenzPruefungProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const GGO_LINK = BASE_PATH + '/arbeitshilfen/download/34#page=';

  return (
    <div className="konsistenz-pruefung-section">
      <Title level={2}>
        {t(
          `egfa.modules.disability.hauptpruefung.konsistenzPruefung.title.${
            props.consistent ? 'consistent' : 'inconsistent'
          }`,
        )}
      </Title>
      <p
        dangerouslySetInnerHTML={{
          __html: t(
            `egfa.modules.disability.hauptpruefung.konsistenzPruefung.${
              props.consistent ? 'consistent' : 'inconsistent'
            }`,
            {
              interpolation: { escapeValue: false },
              link1: GGO_LINK + '69',
              link2: GGO_LINK + '36',
            },
          ),
        }}
      />
    </div>
  );
}
