// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleDisabilityControllerApi,
  EgfaModuleDisabilityEntityDTOAllOf,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportDisabilityToEditor = (egfaId: string, onFinish: (success: boolean) => void): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleDisabilityControllerApi>('egfaModuleDisabilityControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule12({ egfaId }).subscribe({
    next: (disabilityData) => {
      const disabilityJSONExport: EgfaDatenuebernahmeDTO = generateDisabilityJSON(disabilityData.dto);
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Disability,
          egfaDatenuebernahmeDTO: disabilityJSONExport,
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false);
            loadingStatusController.setLoadingStatus(false);
            console.error('Disability Mainstreaming data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Disability Mainstreaming data', error);
    },
  });
};
export const generateDisabilityJSON = (
  data: EgfaModuleDisabilityEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const disabilityJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'disabilityErgebnisdokumentation',
    title: i18n.t(`egfa.modules.disability.name`),
    content: [],
    children: [],
  };
  if (data.influenceExpected && data.influenceSummary) {
    disabilityJSONExport.content.push({
      id: 'disabilityDataSummary',
      type: EgfaItemType.Text,
      data: data.influenceSummary,
    });
  } else {
    disabilityJSONExport.content.push({
      id: 'disabilityDataSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.disability.fertigstellung.noImpact`).toString(),
    });
  }
  return disabilityJSONExport;
};
