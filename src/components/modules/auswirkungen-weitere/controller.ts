// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaDatenuebernahmeDTO,
  EgfaItemType,
  EgfaModuleEntityRequestDTO,
  EgfaModuleType,
  EgfaModuleWeitereControllerApi,
  EgfaModuleWeitereEntityDTOAllOf,
} from '@plateg/rest-api';
import { GlobalDI, LoadingStatusController } from '@plateg/theme';

export const exportWeitereToEditor = (
  egfaId: string,
  onFinish: (success: boolean, error?: AjaxError) => void,
): void => {
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaControllerApi');
  const egfaModuleController = GlobalDI.get<EgfaModuleWeitereControllerApi>('egfaModuleWeitereControllerApi');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  loadingStatusController.setLoadingStatus(true);
  egfaModuleController.getEgfaModule({ egfaId }).subscribe({
    next: (weitereData) => {
      egfaController
        .sendEgfaDatenuebernahme({
          id: egfaId,
          moduleType: EgfaModuleType.Weitere,
          egfaDatenuebernahmeDTO: generateWeitereJSON(weitereData.dto),
        })
        .subscribe({
          next: () => {
            onFinish(true);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error) => {
            onFinish(false, error as AjaxError);
            loadingStatusController.setLoadingStatus(false);
            console.error('Weitere data export to Editor failed', error);
          },
        });
    },
    error: (error) => {
      onFinish(false);
      console.error('could not load Weitere data', error);
    },
  });
};
export const generateWeitereJSON = (
  weitereData: EgfaModuleWeitereEntityDTOAllOf & EgfaModuleEntityRequestDTO,
): EgfaDatenuebernahmeDTO => {
  const weitereJSONExport: EgfaDatenuebernahmeDTO = {
    id: 'weitereErgebnisdokumentation',
    title: i18n.t(`egfa.modules.weitere.fertigstellung.subtitle`),
    content: [],
    children: [],
  };
  if (weitereData.influenceExpected && weitereData.influenceSummary) {
    weitereJSONExport.content.push({
      id: 'weitereSummary',
      type: EgfaItemType.Text,
      data: weitereData.influenceSummary,
    });
  } else {
    weitereJSONExport.content.push({
      id: 'weitereSummary',
      type: EgfaItemType.Text,
      data: i18n.t(`egfa.modules.weitere.fertigstellung.noImpact`).toString(),
    });
  }
  return weitereJSONExport;
};
