// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';

import {
  AktionType,
  EgfaModuleStatusType,
  EgfaModuleVerbraucherEntityDTO,
  EgfaModuleVerbraucherEntityResponseDTO,
  EgfaModuleWeitereControllerApi,
  EgfaModuleWeitereEntityDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../shares/routes';
import { ModulesListController } from '../../egfa/main/modules-list/controller';
import { ModuleProps } from '../auswirkungen-verbraucher/component.react';
import { SimpleModuleAuswirkungen } from '../general/simple-module/auswirkungen/component.react';
import { SimpleModuleEinleitung } from '../general/simple-module/einleitung/component.react';
import { SimpleModuleZusammenfassung } from '../general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../module-wrapper/component.react';
import { generateWeitereJSON } from './controller';

export function AuswirkungenWeitere(props: ModuleProps): React.ReactElement {
  const { egfaId, moduleName } = props.moduleProps;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [weitereData, setWeitereData] = useState<EgfaModuleWeitereEntityDTO>();
  const egfaWeitereController = GlobalDI.get<EgfaModuleWeitereControllerApi>('egfaModuleWeitereControllerApi');
  const [currentPageName, setCurrentPageName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [pageName]);

  const getData = () => {
    return egfaWeitereController.getEgfaModule({ egfaId: egfaId });
  };

  const putData = (localData: EgfaModuleWeitereEntityDTO) =>
    egfaWeitereController.modifyEgfaModule({
      egfaId: egfaId,
      egfaModuleWeitereEntityDTO: localData,
    });

  useEffect(() => {
    props.setEgfaName(routes.AUSWIRKUNGEN_WEITERE);
  }, []);

  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());

  const pageRouting = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    _updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
  ) => {
    const contentPageProps = { egfaId, setFormInstance, formData: weitereData, onSave, handleFormChange };
    const PDFExportBtn = ctrl.getPDFExportAction(weitereData?.egfaId as string, moduleName, weitereData?.status);
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung {...contentPageProps} moduleName="weitere" additionalActions={PDFExportBtn} />
        </Route>
        {(weitereData?.aktionen?.includes(AktionType.Schreiben) === false ||
          weitereData?.status === EgfaModuleStatusType.VollstaendigBearbeitet) && (
          <Route
            path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}`]}
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Redirect
                to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.FERTIGSTELLUNG}`}
              />
            )}
          />
        )}
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.EINLEITUNG}`]}>
          <SimpleModuleEinleitung
            moduleName={`${routes.AUSWIRKUNGEN_WEITERE}`}
            sectionName={routes.EINLEITUNG}
            nextLinkName={routes.WESENTLICHE_AUSWIRKUNGEN}
          />
        </Route>
        <Route
          exact
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.WESENTLICHE_AUSWIRKUNGEN}`]}
        >
          <SimpleModuleAuswirkungen {...contentPageProps} moduleName="weitere" />
        </Route>
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.ZUSAMMENFASSUNG}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.FERTIGSTELLUNG}`}
            />
          )}
        />
        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => (
            <Redirect
              to={`/egfa/${match.params.id}/${routes.MODULE}/${routes.AUSWIRKUNGEN_WEITERE}/${routes.EINLEITUNG}`}
            />
          )}
        />
      </Switch>
    );
  };

  return (
    <ModuleWrapper<EgfaModuleVerbraucherEntityDTO, EgfaModuleVerbraucherEntityResponseDTO>
      setFormData={setWeitereData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={pageRouting}
      pageName={currentPageName}
      generateJson={generateWeitereJSON}
    />
  );
}
