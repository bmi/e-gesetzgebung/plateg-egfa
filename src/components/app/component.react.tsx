// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-egfa.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import {
  AbstimmungControllerApi,
  BerechtigungControllerApi,
  CodelistenControllerApi,
  Configuration,
  EditorRemoteControllerApi,
  EgfaControllerApi,
  EgfaModuleDemografieControllerApi,
  EgfaModuleDisabilityControllerApi,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEnapControllerApi,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleExperimentierklauselControllerApi,
  EgfaModuleGleichstellungControllerApi,
  EgfaModuleGleichwertigkeitControllerApi,
  EgfaModuleKmuControllerApi,
  EgfaModulePreiseControllerApi,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleVerbraucherControllerApi,
  EgfaModuleWeitereControllerApi,
  ExportControllerApi,
  FileControllerApi,
  RegelungsvorhabenControllerApi,
} from '@plateg/rest-api';
import { configureRestApi, ErrorController, HeaderController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../shares/routes';
import { LayoutWrapperEgfa } from '../egfa/component.react';
import { ModulesRoutes } from '../modules/component.react';

export function AppEgfa(): React.ReactElement {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  GlobalDI.getOrRegister('errorController', () => new ErrorController());
  configureRestApi(registerRestApis);

  return (
    <Switch>
      <Route path={[`/egfa/:id/${routes.MODULE}`]}>
        <ModulesRoutes />
      </Route>
      <Route path="/egfa">
        <LayoutWrapperEgfa />
      </Route>
    </Switch>
  );
}
function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('egfaControllerApi', () => new EgfaControllerApi(configRestCalls));
  GlobalDI.getOrRegister('fileControllerApi', () => new FileControllerApi(configRestCalls));
  GlobalDI.getOrRegister('enormController', () => new AbstimmungControllerApi(configRestCalls));
  GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('egfaController', () => new EgfaControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'egfaModuleVerbraucherControllerApi',
    () => new EgfaModuleVerbraucherControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister(
    'egfaModuleSonstigeKostenControllerApi',
    () => new EgfaModuleSonstigeKostenControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister('egfaModulePreiseControllerApi', () => new EgfaModulePreiseControllerApi(configRestCalls));
  GlobalDI.getOrRegister('egfaModuleWeitereControllerApi', () => new EgfaModuleWeitereControllerApi(configRestCalls));
  GlobalDI.getOrRegister('egfaModuleKmuControllerApi', () => new EgfaModuleKmuControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'egfaModuleGleichwertigkeitControllerApi',
    () => new EgfaModuleGleichwertigkeitControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister('egfaModuleEaOehhControllerApi', () => new EgfaModuleEaOehhControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'egfaModuleGleichwertigkeitControllerApi',
    () => new EgfaModuleGleichwertigkeitControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister(
    'egfaModuleDemografieControllerApi',
    () => new EgfaModuleDemografieControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister(
    'egfaModuleDisabilityControllerApi',
    () => new EgfaModuleDisabilityControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister(
    'egfaGleichstellungControllerApi',
    () => new EgfaModuleGleichstellungControllerApi(configRestCalls),
  );

  GlobalDI.getOrRegister(
    'egfaErfuellungsaufwandControllerApi',
    () => new EgfaModuleErfuellungsaufwandControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister('egfaEvaluierungControllerApi', () => new EgfaModuleEvaluierungControllerApi(configRestCalls));
  GlobalDI.getOrRegister('egfaModuleEnapControllerApi', () => new EgfaModuleEnapControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'egfaModuleErfuellungsaufwandControllerApi',
    () => new EgfaModuleErfuellungsaufwandControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister(
    'egfaModuleExperimentierklauselControllerApi',
    () => new EgfaModuleExperimentierklauselControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister('egfaHeaderController', () => new HeaderController());
  GlobalDI.getOrRegister('exportControllerApi', () => new ExportControllerApi(configRestCalls));
  GlobalDI.getOrRegister('editorRemoteControllerApi', () => new EditorRemoteControllerApi(configRestCalls));
  GlobalDI.getOrRegister('codelistenControllerApi', () => new CodelistenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('berechtigungControllerApi', () => new BerechtigungControllerApi(configRestCalls));
}
