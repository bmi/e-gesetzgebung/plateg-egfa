// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { of } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  EditorRemoteControllerApi,
  EgfaControllerApi,
  RegelungsvorhabenEntityListResponseShortDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { NeueGesetzesfolgenabschaetzungController } from './controller';

describe(`Test: NeueGesetzesfolgenabschaetzungController`, () => {
  const egfaCtrl = GlobalDI.getOrRegister('egfaController', () => new EgfaControllerApi(new Configuration()));
  const editorRemoteControllerApi = GlobalDI.getOrRegister(
    'editorRemoteControllerApi',
    () => new EditorRemoteControllerApi(new Configuration()),
  );
  const ctrl = new NeueGesetzesfolgenabschaetzungController();
  it(`Create Methode testen`, () => {
    const createStub = sinon.stub(egfaCtrl, 'createEgfa');
    ctrl.createEgfaCall({ title: '1', regelungsentwurfId: '1', regelungsvorhabenId: '1' });
    sinon.assert.calledOnce(createStub);
    createStub.restore();
  });
  it('prepareInitData testen', () => {
    const getStub = sinon.stub(egfaCtrl, 'getRegelungsvorhabenList').returns(
      of({
        dtos: [
          { dto: { abkuerzung: 'xyz', bearbeitetAm: '2021-11-11' } },
          { dto: { abkuerzung: 'abc', bearbeitetAm: '2022-01-01' } },
        ],
      } as RegelungsvorhabenEntityListResponseShortDTO),
    );
    const expectedOutput = [
      { dto: { abkuerzung: 'abc', bearbeitetAm: '2022-01-01' } },
      { dto: { abkuerzung: 'xyz', bearbeitetAm: '2021-11-11' } },
    ];
    const result = ctrl.prepareInitData();
    sinon.assert.calledOnce(getStub);
    result.subscribe((data) => {
      expect(data).to.eql(expectedOutput);
    });
    getStub.restore();
  });
  it(`getDokumentenmappenCall Methode testen`, () => {
    const getDokumentenmappenStub = sinon.stub(editorRemoteControllerApi, 'getDokumentenmappen');
    ctrl.getDokumentenmappenCall('1');
    sinon.assert.calledOnce(getDokumentenmappenStub);
    getDokumentenmappenStub.restore();
  });
});
