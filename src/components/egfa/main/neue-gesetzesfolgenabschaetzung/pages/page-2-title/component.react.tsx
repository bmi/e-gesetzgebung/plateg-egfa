// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityWithDetailsResponseDTO } from '@plateg/rest-api';
import { Constants, FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { EgfaFormComponent } from '../../egfa-form-component/component.react';

export interface Page2TitleComponentProps {
  name: string;
  selectedRegelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  setEgfaTitle: (egfaTitle: string) => void;
  egfaTitle?: string;
}

export function Page2TitleComponent(props: Page2TitleComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <EgfaFormComponent
        form={Form.useForm()[0]}
        name={props.name}
        title={t('egfa.neueGesetzesfolgenabschaetzung.page2.title')}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        values={{
          title: props.egfaTitle,
        }}
        setValues={(value: { title: string }) => {
          props.setEgfaTitle(value.title);
        }}
      >
        <FormItemWithInfo
          label={
            <span>
              {t('egfa.neueGesetzesfolgenabschaetzung.page2.item1.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <HiddenInfoComponent
                  title={t('egfa.neueGesetzesfolgenabschaetzung.page2.item1.info.title')}
                  text={t('egfa.neueGesetzesfolgenabschaetzung.page2.item1.info.text')}
                />
              </span>
            </span>
          }
          rules={[
            {
              required: true,
              whitespace: true,
              message: t('egfa.neueGesetzesfolgenabschaetzung.page2.item1.requiredMsg'),
            },
            {
              max: Constants.TEXT_BOX_LENGTH,
              message: t('egfa.neueGesetzesfolgenabschaetzung.textLength', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_BOX_LENGTH),
              }),
            },
          ]}
          name="title"
          customName={`${props.name}title`}
        >
          <Input type="text" />
        </FormItemWithInfo>
      </EgfaFormComponent>
    </>
  );
}
