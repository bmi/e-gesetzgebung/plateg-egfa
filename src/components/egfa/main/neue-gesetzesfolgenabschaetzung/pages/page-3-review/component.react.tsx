// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './page-3-review.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EgfaEntityRequestDTO, RegelungsvorhabenEntityWithDetailsResponseDTO } from '@plateg/rest-api';

export interface Page3ReviewComponentProps {
  newEgfa: EgfaEntityRequestDTO;
  selectedRegelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO;
}

export function Page3ReviewComponent(props: Page3ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <div className="metadata-section">
      <Title level={2}>{t('egfa.neueGesetzesfolgenabschaetzung.page3.title')}</Title>
      <dl>
        <div className="review-row">
          <dt>{t('egfa.neueGesetzesfolgenabschaetzung.page3.definitionTitles.rv')}</dt>
          <dd>{props.selectedRegelungsvorhaben?.dto.kurzbezeichnung}</dd>
        </div>
        <div className="review-row">
          <dt>{t('egfa.neueGesetzesfolgenabschaetzung.page3.definitionTitles.titel')}</dt>
          <dd>{props.newEgfa.title}</dd>
        </div>
      </dl>
    </div>
  );
}
