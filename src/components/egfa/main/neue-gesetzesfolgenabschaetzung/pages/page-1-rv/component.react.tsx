// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenEntityWithDetailsResponseDTO } from '@plateg/rest-api';
import { CloseOutlined, FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';

import { EgfaFormComponent } from '../../egfa-form-component/component.react';

export interface Page1RegelungsvorhabenAndEntwurfComponentProps {
  name: string;
  selectedRegelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO;
  setSelectedRegelungsvorhaben: (selectedRegelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO) => void;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  regelungsvorhaben?: RegelungsvorhabenEntityWithDetailsResponseDTO[];
}

export function Page1RegelungsvorhabenAndEntwurfComponent(
  props: Page1RegelungsvorhabenAndEntwurfComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  useEffect(() => {
    if (props.selectedRegelungsvorhaben) {
      form.setFieldValue('regelungsvorhabenId', props.selectedRegelungsvorhaben?.base.id);
    }
  }, [props.selectedRegelungsvorhaben]);

  return (
    <>
      <EgfaFormComponent
        name={props.name}
        form={form}
        title={t('egfa.neueGesetzesfolgenabschaetzung.page1.title')}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        values={{
          regelungsvorhabenId: props.selectedRegelungsvorhaben?.base.id,
        }}
        setValues={(value: { regelungsvorhabenId: string }) => {
          props.setSelectedRegelungsvorhaben(
            props.regelungsvorhaben?.find((rv) => {
              return rv.base.id === value.regelungsvorhabenId;
            }),
          );
        }}
      >
        <>
          <FormItemWithInfo
            label={
              <span>
                {t('egfa.neueGesetzesfolgenabschaetzung.page1.item1.label')}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <HiddenInfoComponent
                    title={t('egfa.neueGesetzesfolgenabschaetzung.page1.item1.info.title')}
                    text={t('egfa.neueGesetzesfolgenabschaetzung.page1.item1.info.text')}
                  />
                </span>
              </span>
            }
            rules={[
              {
                required: true,
                message: t('egfa.neueGesetzesfolgenabschaetzung.page1.item1.requiredMsg'),
              },
            ]}
            name="regelungsvorhabenId"
            customName={`${props.name}_regelungsvorhabenId`}
          >
            <SelectWrapper
              id={`${props.name}_regelungsvorhabenId`}
              allowClear={{ clearIcon: <CloseOutlined /> }}
              placeholder={t('egfa.neueGesetzesfolgenabschaetzung.page1.item1.placeholder')}
              options={props.regelungsvorhaben?.map((element, index) => ({
                label: (
                  <span
                    aria-label={element?.dto.kurzbezeichnung}
                    key={`regelungsvorhaben-option-${element?.dto.kurzbezeichnung || ''}-${index}`}
                  >
                    {`${element?.dto.abkuerzung} - ${element?.dto.kurzbezeichnung}`}
                  </span>
                ),

                value: element.base.id,
                title: `${element?.dto.abkuerzung} - ${element?.dto.kurzbezeichnung}`,
              }))}
            />
          </FormItemWithInfo>
        </>
      </EgfaFormComponent>
    </>
  );
}
