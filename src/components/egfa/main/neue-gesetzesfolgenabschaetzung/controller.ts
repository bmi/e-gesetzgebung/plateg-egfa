// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  DokumentenmappeDTO,
  EditorRemoteControllerApi,
  EgfaControllerApi,
  EgfaEntityRequestDTO,
  EgfaEntityResponseDTO,
  RegelungsvorhabenEntityListResponseShortDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

export class NeueGesetzesfolgenabschaetzungController {
  private readonly egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');
  private readonly editorRemoteControllerApi = GlobalDI.get<EditorRemoteControllerApi>('editorRemoteControllerApi');

  public prepareInitData(): Observable<Array<RegelungsvorhabenEntityListResponseShortDTO>> {
    return this.egfaController.getRegelungsvorhabenList().pipe(
      map((data: RegelungsvorhabenEntityListResponseShortDTO) => {
        return [...data.dtos].sort((o1, o2) => new Date(o2.dto.bearbeitetAm) - new Date(o1.dto.bearbeitetAm));
      }),
    );
  }

  public createEgfaCall(egfa: EgfaEntityRequestDTO): Observable<EgfaEntityResponseDTO> {
    const body = {
      egfaEntityRequestDTO: {
        ...egfa,
      },
    };
    return this.egfaController.createEgfa(body);
  }

  public getDokumentenmappenCall(rvId: string): Observable<DokumentenmappeDTO[]> {
    return this.editorRemoteControllerApi.getDokumentenmappen({ rvId });
  }
}
