// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './egfa-form-component.less';

import { FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { GeneralFormWrapper } from '@plateg/theme';

export interface EgfaFormComponentProps {
  name: string;
  title: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  values?: Object;
  setValues: Function;
  children: React.ReactElement;
  form: FormInstance<any>;
}

export function EgfaFormComponent(props: EgfaFormComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = [props.form];

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function }).focus();
  };

  return (
    <GeneralFormWrapper
      onFinishFailed={handleError}
      form={form}
      id={props.name}
      name={props.name}
      className="new-egfa"
      layout="vertical"
      initialValues={props.values}
      onFinish={() => {
        props.setValues(form.getFieldsValue());
        props.setActivePageIndex(props.activePageIndex + 1);
      }}
    >
      <Title level={2}>{props.title}</Title>
      <p className="ant-typography p-no-style">{t('egfa.neueGesetzesfolgenabschaetzung.requiredInfo')}</p>
      {props.children}
    </GeneralFormWrapper>
  );
}
