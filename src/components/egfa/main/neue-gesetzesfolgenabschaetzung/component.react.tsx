// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { flushSync } from 'react-dom';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { AjaxError } from 'rxjs/ajax';

import { EgfaEntityRequestDTO, RegelungsvorhabenEntityWithDetailsResponseDTO } from '@plateg/rest-api';
import { displayMessage, ErrorController, LoadingStatusController, MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { NeueGesetzesfolgenabschaetzungController } from './controller';
import { Page1RegelungsvorhabenAndEntwurfComponent } from './pages/page-1-rv/component.react';
import { Page2TitleComponent } from './pages/page-2-title/component.react';
import { Page3ReviewComponent } from './pages/page-3-review/component.react';

export interface NeueGesetzesfolgenabschaetzungComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  resetTab: () => void;
  rvId?: string;
}

export function NeueGesetzesfolgenabschaetzungComponent(
  props: NeueGesetzesfolgenabschaetzungComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const emptyEgfa: EgfaEntityRequestDTO = {
    title: undefined,
    regelungsvorhabenId: undefined,
  };
  const ctrl = GlobalDI.getOrRegister(
    'neueGesetzesfolgenabschaetzungController',
    () => new NeueGesetzesfolgenabschaetzungController(),
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [regelungsvorhaben, setRegelungsvorhaben] = useState<RegelungsvorhabenEntityWithDetailsResponseDTO[]>();
  const [selectedRegelungsvorhaben, setSelectedRegelungsvorhaben] =
    useState<RegelungsvorhabenEntityWithDetailsResponseDTO>();
  const [newEgfa, setNewEgfa] = useState<EgfaEntityRequestDTO>(emptyEgfa);
  const [isDraftSubmitted, setIsDraftSubmitted] = useState<boolean>(false);

  useEffect(() => {
    if (props.isVisible) {
      loadContent();
    } else {
      setActivePageIndex(0);
      setSelectedRegelungsvorhaben(undefined);
      setNewEgfa(emptyEgfa);
    }
    setIsDraftSubmitted(false);
  }, [props.isVisible]);

  const loadContent = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = ctrl.prepareInitData().subscribe({
      next: (sortedRegelungsvorhaben: RegelungsvorhabenEntityWithDetailsResponseDTO[]) => {
        setRegelungsvorhaben(sortedRegelungsvorhaben);
        if (props.rvId) {
          const foundRv = sortedRegelungsvorhaben.find((item) => item.base.id === props.rvId);
          if (foundRv) {
            setSelectedRegelungsvorhaben(foundRv);
          }
        }
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        console.error(error, error);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
      },
    });
    return () => sub.unsubscribe();
  };

  return (
    <>
      {regelungsvorhaben && (
        <MultiPageModalComponent
          key={`new-egfa-${props.isVisible.toString()}`}
          isVisible={props.isVisible}
          setIsVisible={props.setIsVisible}
          activePageIndex={activePageIndex}
          setActivePageIndex={setActivePageIndex}
          title={t('egfa.neueGesetzesfolgenabschaetzung.mainTitle')}
          cancelBtnText={t('egfa.neueGesetzesfolgenabschaetzung.btnCancel')}
          nextBtnText={t('egfa.neueGesetzesfolgenabschaetzung.btnNext')}
          prevBtnText={t('egfa.neueGesetzesfolgenabschaetzung.btnPrev')}
          componentReferenceContext={`new-egfa-${props.isVisible.toString()}`}
          pages={[
            {
              formName: 'page1',
              content: (
                <Page1RegelungsvorhabenAndEntwurfComponent
                  name="page1"
                  activePageIndex={activePageIndex}
                  setActivePageIndex={setActivePageIndex}
                  regelungsvorhaben={regelungsvorhaben}
                  selectedRegelungsvorhaben={selectedRegelungsvorhaben}
                  setSelectedRegelungsvorhaben={(regelungsvorhaben) => {
                    flushSync(() => {
                      setNewEgfa({
                        ...newEgfa,
                        regelungsvorhabenId: regelungsvorhaben?.base.id || newEgfa.regelungsvorhabenId,
                        title: `${regelungsvorhaben?.dto.abkuerzung || ''} - GFA`,
                      });
                    });
                    flushSync(() => {
                      setSelectedRegelungsvorhaben(regelungsvorhaben);
                    });
                  }}
                />
              ),
            },
            {
              formName: 'page2',
              content: (
                <Page2TitleComponent
                  name="page2"
                  activePageIndex={activePageIndex}
                  setActivePageIndex={setActivePageIndex}
                  egfaTitle={newEgfa.title}
                  setEgfaTitle={(egfaTitle: string) => {
                    setNewEgfa({ ...newEgfa, title: egfaTitle });
                  }}
                />
              ),
              primaryInsteadNextBtn: {
                buttonText: t('egfa.neueGesetzesfolgenabschaetzung.page2.btnNext'),
                shouldNavToNextPage: true,
              },
            },
            {
              content: <Page3ReviewComponent newEgfa={newEgfa} selectedRegelungsvorhaben={selectedRegelungsvorhaben} />,
              primaryInsteadNextBtn: {
                buttonText: t('egfa.neueGesetzesfolgenabschaetzung.page3.btnNext'),
                disabled: isDraftSubmitted,
              },
              nextOnClick: () => {
                setIsDraftSubmitted(true);
                ctrl.createEgfaCall(newEgfa).subscribe({
                  next: () => {
                    loadingStatusController.setLoadingStatus(false);
                    history.push(`/egfa/${routes.MEINE_EGFAS}`);
                    displayMessage(
                      t('egfa.neueGesetzesfolgenabschaetzung.successMsg', { title: newEgfa.title }),
                      'success',
                    );
                    props.setIsVisible(false);
                    props.resetTab();
                  },
                  error: (error: AjaxError) => {
                    loadingStatusController.setLoadingStatus(false);
                    errorCtrl.displayErrorMsg(error, 'egfa.neueGesetzesfolgenabschaetzung.errorMsg');
                    console.log('Unable to create eGFA', error);
                  },
                });
              },
            },
          ]}
        />
      )}
    </>
  );
}
