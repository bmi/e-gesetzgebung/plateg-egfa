// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy, Suspense, useState } from 'react';
import { Route, Switch } from 'react-router-dom';

import { routes } from '../../../shares/routes';

const EgfaHelpComponent = lazy(() =>
  import('./help/component.react').then((module) => {
    return { default: module.EgfaHelpComponent };
  }),
);
const ErgebnisdokumentationComponent = lazy(() =>
  import('./ergebnisdokumentation/component.react').then((module) => {
    return { default: module.ErgebnisdokumentationComponent };
  }),
);
const ModulesList = lazy(() =>
  import('./modules-list/component.react').then((module) => {
    return { default: module.ModulesList };
  }),
);
const TabsEgfa = lazy(() =>
  import('./tabs/component.react').then((module) => {
    return { default: module.TabsEgfa };
  }),
);

export function MainRoutesEgfa(): React.ReactElement {
  const [checkedList, setCheckedList] = useState<string[]>([]);
  return (
    <Suspense>
      <Switch>
        <Route
          exact
          path={[
            '/egfa',
            `/egfa/${routes.MEINE_EGFAS}`,
            `/egfa/${routes.ARCHIV}`,
            `/egfa/${routes.NEUE_GESETZESFOLGENABSCHAETZUNG}/${routes.REGELUNGSVORHABEN}/:id`,
          ]}
        >
          <TabsEgfa />
        </Route>

        <Route exact path={[`/egfa/${routes.HILFE}`]}>
          <EgfaHelpComponent />
        </Route>

        <Route path={[`/egfa/:id/${routes.MODUL_UEBERSICHT}`]}>
          <ModulesList checkedList={checkedList} setCheckedList={setCheckedList} />
        </Route>

        <Route path={[`/egfa/:id/${routes.ERGEBNISDOKUMENTATION}`]}>
          <ErgebnisdokumentationComponent checkedList={checkedList} />
        </Route>
      </Switch>
    </Suspense>
  );
}
