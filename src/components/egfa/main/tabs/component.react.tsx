// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './home.less';

import { Button, Col, Row, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { EgfaTableFilterDTO, PaginierungDTO } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  ErrorController,
  HeaderController,
  LoadingStatusController,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  setPaginationFilterInfo,
  setPaginationInitState,
  setPaginationResult,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { EgfaHelpHeader } from '../../component.react';
import { NeueGesetzesfolgenabschaetzungComponent } from '../neue-gesetzesfolgenabschaetzung/component.react';
import { ArchivTabComponent } from './archiv-tab/component.react';
import { MeineEgfasTabComponent } from './meine-egfas-tab/component.react';
import { EGFAListItem, EgfaPagData, MeineEgfasController } from './meine-egfas-tab/controller';

import type { TabsProps } from 'antd';
interface MapTabsContentInterface {
  [key: string]: MapTabsContentItemInterface;
}

interface MapTabsContentItemInterface {
  loadMethod: () => Observable<EgfaPagData>;
  setMethod: Function;
  getTableMethod?: Function;
  getFilters: () => Observable<EgfaTableFilterDTO>;
}

export function TabsEgfa(): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const history = useHistory();
  const ctrl = GlobalDI.getOrRegister('meineEgfasController', () => new MeineEgfasController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const routeMatcherneueGesetzesfolgenabschaetzung = useRouteMatch<{
    neueGesetzesfolgenabschaetzung?: string;
    rvId: string;
    rv: string;
  }>('/egfa/:neueGesetzesfolgenabschaetzung/:rv/:rvId');
  const neueGesetzesfolgenabschaetzungRoute =
    routeMatcherneueGesetzesfolgenabschaetzung?.params.neueGesetzesfolgenabschaetzung;
  const rvId = routeMatcherneueGesetzesfolgenabschaetzung?.params.rvId;
  const rv = routeMatcherneueGesetzesfolgenabschaetzung?.params.rv;
  const routeMatcher = useRouteMatch<{ tabKey: string }>('/egfa/:tabKey');
  const tabKey =
    !routeMatcher?.params?.tabKey || neueGesetzesfolgenabschaetzungRoute === routes.NEUE_GESETZESFOLGENABSCHAETZUNG
      ? routes.MEINE_EGFAS
      : routeMatcher?.params?.tabKey;

  const [activeTab, setActiveTab] = useState<string>(tabKey);
  const [loadedTabs, setLoadedTabs] = useState<string[]>([]);
  const [meineEgfas, setMeineEgfas] = useState<EGFAListItem[]>([]);
  const [archivData, setArchivData] = useState<EGFAListItem[]>([]);
  const isNewEgfaUrl =
    neueGesetzesfolgenabschaetzungRoute === routes.NEUE_GESETZESFOLGENABSCHAETZUNG &&
    rv === routes.REGELUNGSVORHABEN &&
    !!rvId;
  const [newEgfaModalIsVisible, setNewEgfaModalIsVisible] = useState(isNewEgfaUrl);

  const newEgfaModalButtonRef = useRef<HTMLButtonElement>(null);
  const tablePaginationTab = useAppSelector((state) => state.tablePagination.tabs[tabKey]);
  const pagDataRequest =
    tabKey === routes.MEINE_EGFAS || tabKey === routes.ARCHIV ? tablePaginationTab.request : undefined;

  const dispatch = useAppDispatch();
  const initRender = useRef(true);

  const requestData: PaginierungDTO = {
    filters: pagDataRequest?.filters || {},
    pageNumber: pagDataRequest?.currentPage || 0,
    pageSize: 20,
    sortBy: pagDataRequest?.columnKey,
    sortDirection: pagDataRequest?.sortOrder,
  };

  const mapTabsContent: MapTabsContentInterface = {
    meineGesetzesfolgeabschaetzungen: {
      loadMethod: () => ctrl.getEGFACall(requestData),
      setMethod: setMeineEgfas,
      getFilters: () => ctrl.getEgfaPagFilters(false),
    },
    archiv: {
      loadMethod: () => ctrl.getEGFACall(requestData, true),
      setMethod: setArchivData,
      getFilters: () => ctrl.getEgfaPagFilters(true),
    },
  };

  useEffect(() => {
    if (tabKey && tabKey !== activeTab && tabKey !== routes.neueGesetzesfolgenabschaetzung) {
      setActiveTab(tabKey);
      if (!loadedTabs.includes(tabKey)) {
        initRender.current = true;
        loadContent(tabKey);
      }
      return;
    }

    loadContent(activeTab);
  }, [
    pagDataRequest?.currentPage,
    pagDataRequest?.columnKey,
    pagDataRequest?.sortOrder,
    pagDataRequest?.filters,
    tabKey,
  ]);

  useEffect(() => {
    setBreadcrumb(activeTab);
  }, [activeTab]);

  useEffect(() => {
    newEgfaModalButtonRef.current?.focus();
  }, [newEgfaModalIsVisible]);

  const loadContent = (key: string) => {
    // If tabContent doesn't exist like for Archiv now - do nothing
    if (!mapTabsContent[`${key}`]) {
      return;
    }

    if (initRender.current) {
      loadingStatusController.setLoadingStatus(true);
      initRender.current = false;

      const loadSub = mapTabsContent[`${key}`].getFilters().subscribe({
        next: (data) => {
          const { erstellerFilter, filterNames, regelungsvorhabenFilter } = data;
          // initstate needs to be before set of filterInfo
          dispatch(setPaginationInitState({ tabKey: key }));
          dispatch(setPaginationFilterInfo({ tabKey: key, erstellerFilter, filterNames, regelungsvorhabenFilter }));
          loadSub.unsubscribe();
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
          loadSub.unsubscribe();
          loadingStatusController.setLoadingStatus(false);
        },
      });
    }

    const loadSub = mapTabsContent[`${key}`].loadMethod().subscribe({
      next: (data) => {
        mapTabsContent[`${key}`].setMethod(data.content);
        setLoadedTabs([...loadedTabs, key]);
        const { allContentEmpty, currentPage, totalItems } = data.pagData;
        dispatch(
          setPaginationResult({
            tabKey: key,
            totalItems,
            currentPage,
            allContentEmpty,
          }),
        );
        loadingStatusController.setLoadingStatus(false);
        loadSub.unsubscribe();
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
        loadSub.unsubscribe();
      },
    });
  };

  const setBreadcrumb = (tabName: string) => {
    const mainText = (
      <span key="egfa-home">
        {t(`egfa.header.breadcrumbs.projectName`)} - {t(`egfa.home.gesetzesfolgenabschatzung.${tabName}.title`)}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[mainText]} />],
      headerCenter: [],
      headerRight: [<EgfaHelpHeader key="egfa-header-help" />],
    });
  };

  const resetTab = (key: string | string[]) => {
    if (Array.isArray(key)) {
      setLoadedTabs(
        loadedTabs.filter((tab: string) => {
          return !key.includes(tab);
        }),
      );
    } else {
      setLoadedTabs(
        loadedTabs.filter((tab: string) => {
          return tab !== key;
        }),
      );
    }
  };

  const tabItems: TabsProps['items'] = [
    {
      key: routes.MEINE_EGFAS,
      label: t(`egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.title`),
      children: (
        <MeineEgfasTabComponent
          tabKey={routes.MEINE_EGFAS}
          data={meineEgfas}
          loadEgfa={() => {
            loadContent(routes.MEINE_EGFAS);
          }}
          resetTab={resetTab}
        />
      ),
    },
    {
      key: routes.ARCHIV,
      label: t(`egfa.home.gesetzesfolgenabschatzung.archiv.title`),
      children: <ArchivTabComponent tabKey={routes.ARCHIV} data={archivData} />,
    },
  ];

  return (
    <div className="egfa-home-page">
      <NeueGesetzesfolgenabschaetzungComponent
        resetTab={() => loadContent(routes.MEINE_EGFAS)}
        isVisible={newEgfaModalIsVisible}
        setIsVisible={setNewEgfaModalIsVisible}
        rvId={isNewEgfaUrl ? rvId : ''}
      />
      <TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className="heading-holder">
              <Title level={1}>{t(`egfa.home.mainTitle`)}</Title>
              <Button
                id="egfa-neueEGFABeginnen-btn"
                type="primary"
                size={'large'}
                onClick={() => {
                  setNewEgfaModalIsVisible(true);
                }}
                ref={newEgfaModalButtonRef}
              >
                {t('egfa.home.btnNew')}
              </Button>
            </div>
          </Col>
        </Row>
      </TitleWrapperComponent>
      <Row>
        <Col xs={{ span: 22, offset: 1 }}>
          <TabsWrapper
            items={tabItems}
            activeKey={activeTab}
            className="egfa-tabs standard-tabs"
            onChange={(key: string) => history.push(`/egfa/${key}`)}
            moduleName={'Test'}
          />
        </Col>
      </Row>
    </div>
  );
}
