// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { map } from 'rxjs/operators';

import { BASE_PATH, EgfaControllerApi } from '@plateg/rest-api';
import {
  AktionType,
  EgfaTableFilterDTO,
  PaginierungDTO,
  UserEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
  VorhabenStatusType,
} from '@plateg/rest-api/models';
import {
  compareDates,
  displayMessage,
  DropdownMenu,
  ErrorController,
  getDateTimeString,
  LoadingStatusController,
  TableComponentProps,
  TextAndContact,
  TwoLineText,
} from '@plateg/theme';
import { PagResult } from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../../shares/routes';
import { EgfaArchiveVals, EgfaRenameVals } from './component.react';

export function getFilterButton<T extends EGFAListItem>(
  initialContent: T[],
  column: { name: string; columnIndex: number },
): { displayName: string; labelContent: string; options: Set<string> } {
  const displayName = i18n.t(
    `egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.filter.displayname.${column.name}`,
  );
  let options: Set<string> = new Set();
  const labelContent = i18n.t(
    `egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.filter.labelContent`,
  );
  if (column.name === 'regelungsvorhaben') {
    options = new Set(
      initialContent.map((row) => {
        return row.abkuerzung as string;
      }),
    );
  }
  if (column.name === 'ersteller') {
    options = new Set(
      initialContent.map((row) => {
        return row.ersteller?.dto.name as string;
      }),
    );
  }
  return { displayName, labelContent, options };
}

export function getFilteredRows<T extends EGFAListItem>(
  actualContent: T[],
  filteredBy: string,
  column: { name: string; columnIndex: number },
): T[] {
  let newRows: Set<T> = new Set();
  if (column.name === 'regelungsvorhaben') {
    newRows = new Set(
      actualContent.filter((row) => {
        return row.abkuerzung === filteredBy;
      }),
    );
  }
  if (column.name === 'ersteller') {
    newRows = new Set(
      actualContent.filter((row) => {
        return row.ersteller?.dto.name === filteredBy;
      }),
    );
  }
  return Array.from(newRows);
}

const getRegelungsvorhabenRenderer = (
  record: EGFAListItem,
  currentUser?: UserEntityWithStellvertreterResponseDTO,
): ReactElement => {
  const archivedLabel = record.isArchived ? i18n.t('egfa.header.breadcrumbs.archived') : '';
  let link: string | undefined;
  if (currentUser && currentUser.base.id === record.ersteller?.base.id) {
    link = record.rvId ? `#/regelungsvorhaben/meineRegelungsvorhaben/${record.rvId}/datenblatt` : '';
  }
  return (
    <div className="regelung-cell">
      <TwoLineText
        firstRow={`${archivedLabel}${record.abkuerzung as string}` || ''}
        firstRowBold={true}
        secondRow={record.kurzbezeichnung || ''}
        secondRowBold={false}
        secondRowLight={false}
        elementId={record.id}
      />
    </div>
  );
};

// helper func to reduce complexity
const buildColumnsHelper = (columns: ColumnsType<EGFAListItem>) => {
  columns.push({
    title: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.erstellt').toString(),
    key: 'erstellt',
    sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
    render: (record: EGFAListItem) => {
      if (record.ersteller && record.erstelltAm) {
        return (
          <TextAndContact
            drawerId={`erstellt-drawer-${record.id}`}
            drawerTitle={i18n.t('egfa.modules.general.contactPersonTitle')}
            firstRow={getDateTimeString(record.erstelltAm)}
            user={record.ersteller.dto}
          />
        );
      } else {
        return {
          props: { colSpan: 0 },
        };
      }
    },
  });
  columns.push({
    title: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.bearbeiter').toString(),
    key: 'bearbeiter',
    render: (record: EGFAListItem) => {
      if (record.bearbeitetAm && record.bearbeiter) {
        const user = record.bearbeiterStellvertreter || record.bearbeiter;
        return (
          <TextAndContact
            drawerId={`bearbeitet-drawer-${record.id}`}
            drawerTitle={i18n.t(
              'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.bearbeiter',
            )}
            firstRow={getDateTimeString(record.bearbeitetAm)}
            user={user.dto}
            isStellvertretung={!!record.bearbeiterStellvertreter}
          />
        );
      } else {
        return {
          props: { colSpan: 0 },
        };
      }
    },
  });
};

// helper func to reduce complexity
const buildColumnsHelper2 = (
  columns: ColumnsType<EGFAListItem>,
  archiveTab: boolean,
  egfaRenameVals?: EgfaRenameVals,
  setEgfaArchiveVals?: (egfaArchiveVals: EgfaArchiveVals) => void,
  setEgfaRenameVals?: (egfaRenameVals: EgfaRenameVals) => void,
) => {
  columns.push({
    title: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.aktionen').toString(),
    key: 'aktionen',
    render: (record: EGFAListItem) => {
      const items: DropdownMenuItem[] = !archiveTab
        ? [
            {
              element: i18n.t(
                'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.menuItemTitle',
              ),
              onClick: () => {
                if (setEgfaRenameVals && egfaRenameVals) {
                  setEgfaRenameVals({
                    ...egfaRenameVals,
                    egfaRenameIsVisible: true,
                    id: record.id,
                    currentName: record.title ?? '',
                  });
                }
              },
              disabled: () => !record.aktionen?.includes(AktionType.Schreiben),
            },
            {
              element: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.saveAsPdf'),
              disabled: () => !record.aktionen?.includes(AktionType.Exportieren),
              onClick: () =>
                openFile(`${BASE_PATH}/file/egfaexport/${record.id}/pdf`, {
                  target: '_blank',
                  fileName: `${record.title ?? 'egfa'}.pdf`,
                }),
            },
            {
              element: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.archive'),
              disabled: () => !record.aktionen?.includes(AktionType.Archivieren),
              onClick: () => {
                if (setEgfaArchiveVals) {
                  setEgfaArchiveVals({ id: record.id, isVisible: true, title: record.title || '' });
                }
              },
            },
          ]
        : [];
      return (
        <DropdownMenu
          isDisabled={!record.aktionen?.includes(AktionType.Lesen)}
          openLink={`/egfa/${record.id.toString()}/${routes.MODUL_UEBERSICHT}`}
          items={items}
          elementId={record.id}
        />
      );
    },
  });
};

export function getEGFATableVals(
  content: EGFAListItem[],
  archiveTab: boolean,
  setEgfaRenameVals?: (egfaRenameVals: EgfaRenameVals) => void,
  egfaRenameVals?: EgfaRenameVals,
  setEgfaArchiveVals?: (egfaArchiveVals: EgfaArchiveVals) => void,
  currentUser?: UserEntityWithStellvertreterResponseDTO,
): TableComponentProps<EGFAListItem> {
  const columns: ColumnsType<EGFAListItem> = [
    {
      title: i18n
        .t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.regelungsvorhaben')
        .toString(),
      key: 'regelungsvorhaben',
      fixed: false,
      render: (record: EGFAListItem): ReactElement => {
        return getRegelungsvorhabenRenderer(record, currentUser);
      },
    },
    {
      title: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.gfa').toString(),
      key: 'gfa',
      fixed: false,
      render: (record: EGFAListItem): ReactElement => {
        if (!record.aktionen?.includes(AktionType.Lesen)) {
          return <>{record.title}</>;
        }
        return <a href={`#/egfa/${record.id.toString()}/${routes.MODUL_UEBERSICHT}`}>{record.title}</a>;
      },
    },
  ];
  if (!archiveTab) {
    buildColumnsHelper(columns);
  } else {
    columns.push({
      title: i18n.t('egfa.home.gesetzesfolgenabschatzung.archiv.table.archiviertAm').toString(),
      key: 'archiviertAm',
      sorter: (a, b) => compareDates(a.archiviertAm, b.archiviertAm),
      render: (record: EGFAListItem) => {
        if (record.archiviertAm) {
          return getDateTimeString(record.archiviertAm);
        } else {
          return {
            props: { colSpan: 0 },
          };
        }
      },
    });
  }

  buildColumnsHelper2(columns, archiveTab, egfaRenameVals, setEgfaArchiveVals, setEgfaRenameVals);

  const additionalArchiveSorters = archiveTab
    ? [
        {
          columnKey: 'archiviertAm',
          titleAsc: i18n.t('egfa.home.gesetzesfolgenabschatzung.archiv.table.archiviertAmAsc'),
          titleDesc: i18n.t('egfa.home.gesetzesfolgenabschatzung.archiv.table.archiviertAmDesc'),
        },
      ]
    : [];

  return {
    id: 'my-egfa-table',
    expandable: false,
    columns,
    content,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'ersteller', columnIndex: 2 },
    ],
    sorterOptions: archiveTab
      ? additionalArchiveSorters
      : [
          {
            columnKey: 'erstellt',
            titleAsc: i18n.t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.erstelltAsc'),
            titleDesc: i18n.t(
              'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.erstelltDesc',
            ),
          },
        ],
    customDefaultSortIndex: 2,
  };
}

export interface EGFAListItem {
  id: string;
  erstelltAm?: string;
  ersteller?: UserEntityResponseDTO;
  erstellerStellvertreter?: UserEntityResponseDTO;
  bearbeitetAm?: string;
  bearbeiter?: UserEntityResponseDTO;
  bearbeiterStellvertreter?: UserEntityResponseDTO;
  abkuerzung?: string;
  kurzbezeichnung?: string;
  isArchived?: boolean;
  title?: string;
  rvId: string;
  allowedToArchive?: boolean;
  archiviertAm?: string;
  aktionen: AktionType[];
}

export interface EgfaPagData {
  content: EGFAListItem[] | undefined;
  pagData: PagResult;
}
export class MeineEgfasController {
  private readonly egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');

  public getEGFACall(paginierungDTO: PaginierungDTO, archive?: boolean): Observable<EgfaPagData> {
    const getDataFunction = archive
      ? this.egfaController.getEgfaArchiv({ paginierungDTO })
      : this.egfaController.getEgfa({ paginierungDTO });
    return getDataFunction.pipe(
      map((data) => {
        const { content, totalElements, number } = data.dtos;
        const { allContentEmpty } = data;
        return {
          pagData: {
            allContentEmpty,
            currentPage: number,
            totalItems: totalElements,
          },
          content: content?.map((item) => {
            return {
              id: item.id,
              erstelltAm: item.erstelltAm,
              ersteller: item.ersteller,
              erstellerStellvertreter: item.erstellerStellvertreter,
              bearbeitetAm: item.bearbeitetAm,
              bearbeiter: item.bearbeiter,
              bearbeiterStellvertreter: item.bearbeiterStellvertreter,
              abkuerzung: item.regelungsvorhaben?.dto.abkuerzung,
              kurzbezeichnung: item.regelungsvorhaben?.dto.kurzbezeichnung,
              title: item.title,
              isArchived: item.regelungsvorhaben?.dto.status === VorhabenStatusType.Archiviert,
              rvId: item.regelungsvorhaben?.base.id,
              allowedToArchive: item.allowedToArchive,
              archiviertAm: item.archiviertAm,
              aktionen: item.aktionen,
            };
          }),
        };
      }),
    );
  }

  public getEgfaPagFilters(archiviert: boolean): Observable<EgfaTableFilterDTO> {
    return this.egfaController.getEgfaFilter({ archiviert });
  }

  public renameEGFACall(id: string, title: string): Observable<void> {
    return this.egfaController.renameEgfa({ id: id, egfaEntityRequestRenameDTO: { title: title } });
  }

  public setArchived<T extends { id: string }>(
    id: string,
    title: string,
    content: Array<T>,
    setContent: (content: Array<T>) => void,
    onSuccess: () => void,
  ): void {
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
    loadingStatusController.setLoadingStatus(true);
    this.egfaController.archiveEgfa({ id }).subscribe({
      next: () => {
        setContent(
          content.filter((entry) => {
            return entry.id !== id;
          }),
        );
        displayMessage(
          i18n.t(
            'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.archiveEgfaModal.archivedSuccess',
            { title: title },
          ),
          'success',
        );
        loadingStatusController.setLoadingStatus(false);
        onSuccess();
      },
      error: (error: AjaxError) => {
        console.error('Error sub', error);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });
  }
}
