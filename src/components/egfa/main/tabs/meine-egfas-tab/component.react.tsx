// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  GlobalDI,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../shares/routes';
import { EGFAListItem, getEGFATableVals, getFilterButton, getFilteredRows, MeineEgfasController } from './controller';
import { RenameEgfaModal } from './rename-egfa/component.react';

interface MeineEgfasTabComponentInterface {
  data: EGFAListItem[];
  loadEgfa: () => void;
  resetTab: (tab: string) => void;
  tabKey: string;
}
export interface EgfaRenameVals {
  egfaRenameIsVisible: boolean;
  id: string;
  currentName: string;
  loadEgfa: () => void;
}
export interface EgfaArchiveVals {
  isVisible: boolean;
  id: string;
  title: string;
}
export function MeineEgfasTabComponent(props: MeineEgfasTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('meineEgfasController', () => new MeineEgfasController());
  const [meineEgfas, setMeineEgfas] = useState(props.data);

  const [eGFATableVals, setEGFATableVals] = useState<TableComponentProps<EGFAListItem>>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
    id: '',
  });
  const [egfaRenameVals, setEgfaRenameVals] = useState<EgfaRenameVals>({
    egfaRenameIsVisible: false,
    id: '',
    currentName: '',
    loadEgfa: props.loadEgfa,
  });

  const [egfaArchiveVals, setEgfaArchiveVals] = useState<EgfaArchiveVals>({
    isVisible: false,
    id: '',
    title: '',
  });

  const appStoreUser = useAppSelector((state) => state.user);
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setMeineEgfas(props.data);
  }, [props.data]);

  useEffect(() => {
    setEGFATableVals(
      getEGFATableVals(meineEgfas, false, setEgfaRenameVals, egfaRenameVals, setEgfaArchiveVals, appStoreUser.user),
    );
  }, [meineEgfas]);
  return (
    <div className="main-holder">
      {!pagInfoResult.allContentEmpty && (
        <>
          <RenameEgfaModal
            id={egfaRenameVals.id}
            currentName={egfaRenameVals.currentName}
            loadEgfa={egfaRenameVals.loadEgfa}
            isModalVisible={egfaRenameVals.egfaRenameIsVisible}
            setIsModalVisible={(isModalVisible: boolean) =>
              setEgfaRenameVals({ ...egfaRenameVals, egfaRenameIsVisible: isModalVisible })
            }
          />
          <ArchivConfirmComponent
            actionTitle={t(
              'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.archiveEgfaModal.content',
            )}
            okText={t(
              'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.archiveEgfaModal.okText',
            )}
            cancelText={t(
              'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.archiveEgfaModal.cancelText',
            )}
            visible={egfaArchiveVals.isVisible}
            setVisible={(visible) => setEgfaArchiveVals({ ...egfaArchiveVals, isVisible: visible })}
            setArchived={() => {
              ctrl.setArchived(egfaArchiveVals.id, egfaArchiveVals.title, meineEgfas, setMeineEgfas, () => {
                props.resetTab(routes.ARCHIV);
                setEgfaArchiveVals({ ...egfaArchiveVals, isVisible: false });
                props.loadEgfa();
              });
            }}
          />

          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id="meine-egfas-table"
            customDefaultSortIndex={eGFATableVals.customDefaultSortIndex}
            expandable={eGFATableVals.expandable}
            columns={eGFATableVals.columns}
            content={eGFATableVals.content}
            filteredColumns={eGFATableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            sorterOptions={eGFATableVals.sorterOptions}
          />
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.emptyContentLabel1`),
            imgSrc: require(
              `../../../../../media/empty-content-images/gesetzesfolgenabschatzung/img1.svg`,
            ) as ImageModule,
            height: 124,
          },
          {
            label: t(`egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.emptyContentLabel2`),
            imgSrc: require(
              `../../../../../media/empty-content-images/gesetzesfolgenabschatzung/img2.svg`,
            ) as ImageModule,
            height: 96,
          },
          {
            label: t(`egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.emptyContentLabel3`),
            imgSrc: require(
              `../../../../../media/empty-content-images/gesetzesfolgenabschatzung/img3.svg`,
            ) as ImageModule,
            height: 118,
          },
        ]}
        noIndex
      >
        <p className="ant-typography p-no-style info-text">
          {t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.emptyContentText')}
        </p>
        <p className="ant-typography p-no-style info-text">
          {t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.emptyContentText2')}
        </p>
      </EmptyContentComponent>
    </div>
  );
}
