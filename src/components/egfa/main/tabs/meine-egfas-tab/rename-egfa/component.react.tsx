// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './rename-egfa.less';

import { Form, Input } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  Constants,
  displayMessage,
  ErrorController,
  GeneralFormWrapper,
  GlobalDI,
  LoadingStatusController,
  ModalWrapper,
} from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { MeineEgfasController } from '../controller';

export interface FeedbackModalProps {
  isModalVisible: boolean;
  setIsModalVisible: (value: boolean) => void;
  id: string;
  currentName?: string;
  loadEgfa: Function;
}

export function RenameEgfaModal(props: FeedbackModalProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const ctrl = GlobalDI.getOrRegister('meineEgfasController', () => new MeineEgfasController());
  const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  useEffect(() => {
    form.setFieldsValue({ name: props.currentName });
  }, [props.currentName, props.id]);

  const handleClose = () => {
    props.setIsModalVisible(!props.isModalVisible);
  };

  const handleSubmit = () => {
    loadingStatusCtrl.setLoadingStatus(true);
    const newName = form.getFieldValue('name') as string;
    ctrl.renameEGFACall(props.id, newName).subscribe({
      next: () => {
        displayMessage(
          t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.success', {
            name: newName,
          }),
          'success',
        );
        handleClose();
        props.loadEgfa();
      },
      error: (error: AjaxError) => {
        loadingStatusCtrl.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
      },
    });
  };

  return (
    <ModalWrapper
      className="rename-egfa-modal"
      open={props.isModalVisible}
      closable={true}
      title={
        <h3>{t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.title')}</h3>
      }
      cancelText={t(
        'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.cancel',
      )}
      okText={t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.ok')}
      onCancel={handleClose}
      onOk={form.submit}
    >
      <GeneralFormWrapper form={form} onFinish={handleSubmit} layout="vertical">
        <Form.Item
          name="name"
          label={
            <span>
              {t('egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.ok')}
            </span>
          }
          rules={[
            {
              required: true,
              whitespace: true,
              message: t(
                'egfa.home.gesetzesfolgenabschatzung.meineGesetzesfolgeabschaetzungen.table.renameEgfaModal.requiredMsg',
              ),
            },
            {
              max: Constants.TEXT_BOX_LENGTH,
              message: t('egfa.neueGesetzesfolgenabschaetzung.textLength', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_BOX_LENGTH),
              }),
            },
          ]}
        >
          <Input />
        </Form.Item>
      </GeneralFormWrapper>
    </ModalWrapper>
  );
}
