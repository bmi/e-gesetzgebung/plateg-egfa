// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { EGFAListItem, getEGFATableVals, getFilterButton, getFilteredRows } from '../meine-egfas-tab/controller';

interface MeineEgfasTabComponentInterface {
  data: EGFAListItem[];
  tabKey: string;
}
export function ArchivTabComponent(props: MeineEgfasTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const [meineEgfas, setMeineEgfas] = useState(props.data);

  const [eGFATableVals, setEGFATableVals] = useState<TableComponentProps<EGFAListItem>>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
    id: '',
  });
  const pagInfoResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setMeineEgfas(props.data);
  }, [props.data]);

  useEffect(() => {
    setEGFATableVals(getEGFATableVals(meineEgfas, true));
  }, [meineEgfas]);
  return (
    <div className="main-holder">
      {!pagInfoResult.allContentEmpty && (
        <>
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id="egfa-archiv-table"
            customDefaultSortIndex={eGFATableVals.customDefaultSortIndex}
            expandable={eGFATableVals.expandable}
            columns={eGFATableVals.columns}
            content={eGFATableVals.content}
            filteredColumns={eGFATableVals.filteredColumns}
            filterRowsMethod={getFilteredRows}
            prepareFilterButtonMethod={getFilterButton}
            sorterOptions={eGFATableVals.sorterOptions}
          />
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`egfa.home.gesetzesfolgenabschatzung.archiv.emptyContentLabel1`),
            imgSrc: require(`../../../../../media/empty-content-images/archiv/img1.svg`) as ImageModule,
            height: 130,
          },
          {
            label: t(`egfa.home.gesetzesfolgenabschatzung.archiv.emptyContentLabel2`),
            imgSrc: require(`../../../../../media/empty-content-images/archiv/img2.svg`) as ImageModule,
            height: 137,
          },
        ]}
        noIndex
      >
        <p className="ant-typography p-no-style info-text">
          {t('egfa.home.gesetzesfolgenabschatzung.archiv.emptyContentText')}
        </p>
      </EmptyContentComponent>
    </div>
  );
}
