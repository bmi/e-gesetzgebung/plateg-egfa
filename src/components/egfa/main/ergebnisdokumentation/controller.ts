// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { catchError, forkJoin, map, Observable, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaModuleDemografieControllerApi,
  EgfaModuleDisabilityControllerApi,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEnapControllerApi,
  EgfaModuleEntityResponseDTO,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleExperimentierklauselControllerApi,
  EgfaModuleGleichstellungControllerApi,
  EgfaModuleGleichwertigkeitControllerApi,
  EgfaModuleKmuControllerApi,
  EgfaModulePreiseControllerApi,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleType,
  EgfaModuleVerbraucherControllerApi,
  EgfaModuleWeitereControllerApi,
  EgfaWithModulesTableResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

export class ErgebnisdokumentationController {
  private readonly egfaVerbraucherController = GlobalDI.get<EgfaModuleVerbraucherControllerApi>(
    'egfaModuleVerbraucherControllerApi',
  );
  private readonly egfaPreiseController = GlobalDI.get<EgfaModulePreiseControllerApi>('egfaModulePreiseControllerApi');
  private readonly egfaWeitereController = GlobalDI.get<EgfaModuleWeitereControllerApi>(
    'egfaModuleWeitereControllerApi',
  );
  private readonly egfaModuleKmuController = GlobalDI.get<EgfaModuleKmuControllerApi>('egfaModuleKmuControllerApi');
  private readonly egfaModuleDemografieController = GlobalDI.get<EgfaModuleDemografieControllerApi>(
    'egfaModuleDemografieControllerApi',
  );

  private readonly egfaModuleSonstigeKostenController = GlobalDI.get<EgfaModuleSonstigeKostenControllerApi>(
    'egfaModuleSonstigeKostenControllerApi',
  );
  private readonly egfaModuleGleichwertigkeitController = GlobalDI.get<EgfaModuleGleichwertigkeitControllerApi>(
    'egfaModuleGleichwertigkeitControllerApi',
  );
  private readonly egfaDisabilityController = GlobalDI.get<EgfaModuleDisabilityControllerApi>(
    'egfaModuleDisabilityControllerApi',
  );
  private readonly egfaModuleEnapController = GlobalDI.get<EgfaModuleEnapControllerApi>('egfaModuleEnapControllerApi');
  private readonly egfaGleichstellungController = GlobalDI.get<EgfaModuleGleichstellungControllerApi>(
    'egfaGleichstellungControllerApi',
  );
  private readonly egfaModuleEaOehhControllerApi = GlobalDI.get<EgfaModuleEaOehhControllerApi>(
    'egfaModuleEaOehhControllerApi',
  );
  private readonly egfaErfuellungsaufwandController = GlobalDI.get<EgfaModuleErfuellungsaufwandControllerApi>(
    'egfaErfuellungsaufwandControllerApi',
  );
  private readonly egfaModuleEvaluierungControllerApi =
    GlobalDI.get<EgfaModuleEvaluierungControllerApi>('egfaEvaluierungControllerApi');

  private readonly egfaModuleExperimentierklauselControllerApi =
    GlobalDI.get<EgfaModuleExperimentierklauselControllerApi>('egfaModuleExperimentierklauselControllerApi');
  public loadModules(
    data: EgfaWithModulesTableResponseDTO,
    checkedList: string[],
  ): Observable<EgfaModuleEntityResponseDTO[]> {
    const modules = data.modules.filter((module) => checkedList.some((moduleId) => moduleId === module.id));

    const observables = modules.map((module) => {
      switch (module.typ) {
        case EgfaModuleType.Verbraucher:
          return this.egfaVerbraucherController.getEgfaModule1({ egfaId: data.egfa.id });
        case EgfaModuleType.Preise:
          return this.egfaPreiseController.getEgfaModule3({ egfaId: data.egfa.id });
        case EgfaModuleType.Weitere:
          return this.egfaWeitereController.getEgfaModule({ egfaId: data.egfa.id });
        case EgfaModuleType.Kmu:
          return this.egfaModuleKmuController.getEgfaModule4({ egfaId: data.egfa.id });
        case EgfaModuleType.SonstigeKosten:
          return this.egfaModuleSonstigeKostenController.getEgfaModule2({ egfaId: data.egfa.id });
        case EgfaModuleType.Gleichwertigkeit:
          return this.egfaModuleGleichwertigkeitController.getEgfaModule5({ egfaId: data.egfa.id });
        case EgfaModuleType.Demografie:
          return this.egfaModuleDemografieController.getEgfaModule13({ egfaId: data.egfa.id });
        case EgfaModuleType.Gleichstellung:
          return this.egfaGleichstellungController.getEgfaModule6({ egfaId: data.egfa.id });
        case EgfaModuleType.Disability:
          return this.egfaDisabilityController.getEgfaModule12({ egfaId: data.egfa.id });
        case EgfaModuleType.Enap:
          return this.egfaModuleEnapController.getEgfaModule10({ egfaId: data.egfa.id });
        case EgfaModuleType.EaOehh:
          return this.egfaModuleEaOehhControllerApi.getEgfaModule11({ egfaId: data.egfa.id });
        case EgfaModuleType.Erfuellungsaufwand:
          return this.egfaErfuellungsaufwandController.getEgfaModule9({ egfaId: data.egfa.id });
        case EgfaModuleType.Evaluierung:
          return this.egfaModuleEvaluierungControllerApi.getEgfaModule8({ egfaId: data.egfa.id });
        case EgfaModuleType.Experimentierklausel:
          return this.egfaModuleExperimentierklauselControllerApi.getEgfaModule7({ egfaId: data.egfa.id });
        default:
          console.warn(`module type not implemented: ${module.typ as string}`);
          return undefined;
      }
    });
    return forkJoin(
      observables
        .filter((obs) => !!obs)
        .map((obs) =>
          obs.pipe(
            catchError((error: AjaxError) => {
              console.log('Error loading module:', error);
              return of(undefined);
            }),
          ),
        ),
    ).pipe(map((results: unknown[]) => results.filter((result) => result !== undefined))) as Observable<
      EgfaModuleEntityResponseDTO[]
    >;
  }
}
