// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  EgfaModuleDemografieControllerApi,
  EgfaModuleDisabilityControllerApi,
  EgfaModuleEaOehhControllerApi,
  EgfaModuleEnapControllerApi,
  EgfaModuleErfuellungsaufwandControllerApi,
  EgfaModuleEvaluierungControllerApi,
  EgfaModuleExperimentierklauselControllerApi,
  EgfaModuleGleichstellungControllerApi,
  EgfaModuleGleichwertigkeitControllerApi,
  EgfaModuleKmuControllerApi,
  EgfaModulePreiseControllerApi,
  EgfaModuleSonstigeKostenControllerApi,
  EgfaModuleStatusType,
  EgfaModuleType,
  EgfaModuleVerbraucherControllerApi,
  EgfaModuleWeitereControllerApi,
  RegelungsvorhabenEntityResponseShortDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { evaluierungTestData } from '../../../modules/evaluierung/controller.test';
import { ErgebnisdokumentationController } from './controller';

describe(`Test: Ergebnisdokumentation`, () => {
  const verbraucherCtrl = GlobalDI.getOrRegister(
    'egfaModuleVerbraucherControllerApi',
    () => new EgfaModuleVerbraucherControllerApi(new Configuration()),
  );
  const preiseCtrl = GlobalDI.getOrRegister(
    'egfaModulePreiseControllerApi',
    () => new EgfaModulePreiseControllerApi(new Configuration()),
  );
  const weitereCtrl = GlobalDI.getOrRegister(
    'egfaModuleWeitereControllerApi',
    () => new EgfaModuleWeitereControllerApi(new Configuration()),
  );
  const kmuCtrl = GlobalDI.getOrRegister(
    'egfaModuleKmuControllerApi',
    () => new EgfaModuleKmuControllerApi(new Configuration()),
  );
  const gleichwertigkeitCtrl = GlobalDI.getOrRegister(
    'egfaModuleGleichwertigkeitControllerApi',
    () => new EgfaModuleGleichwertigkeitControllerApi(new Configuration()),
  );
  const demografieCtrl = GlobalDI.getOrRegister(
    'egfaModuleDemografieControllerApi',
    () => new EgfaModuleDemografieControllerApi(new Configuration()),
  );

  const sonstigeKostenCtrl = GlobalDI.getOrRegister<EgfaModuleSonstigeKostenControllerApi>(
    'egfaModuleSonstigeKostenControllerApi',
    () => new EgfaModuleSonstigeKostenControllerApi(new Configuration()),
  );

  const disabilityCtrl = GlobalDI.getOrRegister(
    'egfaModuleDisabilityControllerApi',
    () => new EgfaModuleDisabilityControllerApi(new Configuration()),
  );

  const enapCtrl = GlobalDI.getOrRegister(
    'egfaModuleEnapControllerApi',
    () => new EgfaModuleEnapControllerApi(new Configuration()),
  );

  const egfaEaOehhCtrl = GlobalDI.getOrRegister(
    'egfaModuleEaOehhControllerApi',
    () => new EgfaModuleEaOehhControllerApi(new Configuration()),
  );

  const egfaGleichstellungControllerCtrl = GlobalDI.getOrRegister(
    'egfaGleichstellungControllerApi',
    () => new EgfaModuleGleichstellungControllerApi(new Configuration()),
  );

  const egfaErfuellungsaufwandControllerCtrl = GlobalDI.getOrRegister(
    'egfaErfuellungsaufwandControllerApi',
    () => new EgfaModuleErfuellungsaufwandControllerApi(new Configuration()),
  );
  const egfaEvaluierungControllerCtrl = GlobalDI.getOrRegister(
    'egfaEvaluierungControllerApi',
    () => new EgfaModuleEvaluierungControllerApi(new Configuration()),
  );
  const egfaModuleExperimentierklauselControllerApi = GlobalDI.getOrRegister(
    'egfaModuleExperimentierklauselControllerApi',
    () => new EgfaModuleExperimentierklauselControllerApi(new Configuration()),
  );

  const ctrl = new ErgebnisdokumentationController();
  const inputData = {
    egfa: { id: '1', erstelltAm: '', regelungsvorhaben: {} as RegelungsvorhabenEntityResponseShortDTO },
    modules: [
      { erstelltAm: '', status: EgfaModuleStatusType.InBearbeitung, typ: EgfaModuleType.Verbraucher, id: '1' },
      { erstelltAm: '', status: EgfaModuleStatusType.InBearbeitung, typ: EgfaModuleType.Preise, id: '2' },
      { erstelltAm: '', status: EgfaModuleStatusType.VollstaendigBearbeitet, typ: EgfaModuleType.Weitere, id: '3' },
      { erstelltAm: '', status: EgfaModuleStatusType.VollstaendigBearbeitet, typ: EgfaModuleType.Weitere, id: '3' },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: 'Hello' as EgfaModuleType,
        id: '3',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Verbraucher,
        id: '4',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Kmu,
        id: '5',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.SonstigeKosten,
        id: '6',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Demografie,
        id: '7',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Gleichwertigkeit,
        id: '8',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Disability,
        id: '9',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Enap,
        id: '10',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Gleichstellung,
        id: '11',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.EaOehh,
        id: '12',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Erfuellungsaufwand,
        id: '13',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Evaluierung,
        id: '14',
      },
      {
        erstelltAm: '',
        status: EgfaModuleStatusType.VollstaendigBearbeitet,
        typ: EgfaModuleType.Experimentierklausel,
        id: '15',
      },
    ],
  };
  const checkedList = ['1', '2', '3', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15'];
  it(`Load Modules testen`, () => {
    const verbraucherStub = sinon.stub(verbraucherCtrl, 'getEgfaModule1');
    const preiseStub = sinon.stub(preiseCtrl, 'getEgfaModule3');
    const weitereStub = sinon.stub(weitereCtrl, 'getEgfaModule');
    const kmuStub = sinon.stub(kmuCtrl, 'getEgfaModule4');
    const demographieStub = sinon.stub(demografieCtrl, 'getEgfaModule13');
    const sonstigeKostenStub = sinon.stub(sonstigeKostenCtrl, 'getEgfaModule2');
    const gleichwertigkeitStub = sinon.stub(gleichwertigkeitCtrl, 'getEgfaModule5');
    const disabilityStub = sinon.stub(disabilityCtrl, 'getEgfaModule12');
    const enapStub = sinon.stub(enapCtrl, 'getEgfaModule10');
    const egfaEaOehhStub = sinon.stub(egfaEaOehhCtrl, 'getEgfaModule11');
    const gleichstellungStub = sinon.stub(egfaGleichstellungControllerCtrl, 'getEgfaModule6');
    const erfuellungsaufwandStub = sinon.stub(egfaErfuellungsaufwandControllerCtrl, 'getEgfaModule9');
    const evaluierungStub = sinon.stub(egfaEvaluierungControllerCtrl, 'getEgfaModule8');
    const experimentierklauselnStub = sinon.stub(egfaModuleExperimentierklauselControllerApi, 'getEgfaModule7');
    verbraucherStub.returns(of(undefined));
    preiseStub.returns(of(undefined));
    weitereStub.returns(of(undefined));
    kmuStub.returns(of(undefined));
    demographieStub.returns(of(undefined));
    sonstigeKostenStub.returns(of(undefined));
    gleichwertigkeitStub.returns(of(undefined));
    disabilityStub.returns(of(undefined));
    enapStub.returns(of(undefined));
    egfaEaOehhStub.returns(of(undefined));
    gleichstellungStub.returns(of(undefined));
    erfuellungsaufwandStub.returns(of(undefined));
    evaluierungStub.returns(of(evaluierungTestData));
    evaluierungStub.calledWith({
      id: '123',
    });
    experimentierklauselnStub.returns(of(undefined));
    ctrl.loadModules(inputData, checkedList);
    sinon.assert.calledOnce(preiseStub);
    sinon.assert.calledOnce(verbraucherStub);
    sinon.assert.calledTwice(weitereStub);
    sinon.assert.calledOnce(kmuStub);
    sinon.assert.calledOnce(demographieStub);
    sinon.assert.calledOnce(sonstigeKostenStub);
    sinon.assert.calledOnce(gleichwertigkeitStub);
    sinon.assert.calledOnce(disabilityStub);
    sinon.assert.calledOnce(enapStub);
    sinon.assert.calledOnce(gleichstellungStub);
    sinon.assert.calledOnce(egfaEaOehhStub);
    sinon.assert.calledOnce(erfuellungsaufwandStub);
    sinon.assert.calledOnce(evaluierungStub);
    sinon.assert.calledOnce(experimentierklauselnStub);
    verbraucherStub.restore();
    preiseStub.restore();
    weitereStub.restore();
    kmuStub.restore();
    enapStub.restore();
    sonstigeKostenStub.restore();
    demographieStub.restore();
    gleichwertigkeitStub.restore();
    gleichstellungStub.restore();
    erfuellungsaufwandStub.restore();
    disabilityStub.restore();
    egfaEaOehhStub.restore();
    evaluierungStub.restore();
    experimentierklauselnStub.restore();
  });
});
