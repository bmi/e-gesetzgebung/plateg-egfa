// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, Row, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  EgfaControllerApi,
  EgfaModuleEaOehhEntityDTOAllOf,
  EgfaModuleEntityRequestDTO,
  EgfaModuleKMUEntityDTOAllOf,
  EgfaModuleType,
  GetEgfaWithModulesRequest,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  BtnBackComponent,
  ErrorController,
  HeaderController,
  LoadingStatusController,
} from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { ErgebnisdokumentationSummaryComponent } from '../../../modules/auswirkungen-einnahmen-ausgaben/ergebnisdokumentation-summary/component.react';
import { FertigstellungErfuellungsaufwandContent } from '../../../modules/erfuellungsaufwand/fertigstellung/fertigstellung-content/component.react';
import { EvalueirungFertigstellung } from '../../../modules/evaluierung/fertigstellung/evalueirung-fertigstellung/component.react';
import { EgfaHomeBreadcrumb } from '../../component.react';
import { ErgebnisdokumentationController } from './controller';

interface ErgebnisdokumentationComponentProps {
  checkedList: string[];
}

export function ErgebnisdokumentationComponent(props: ErgebnisdokumentationComponentProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const routeMatcher = useRouteMatch<{ id: string }>(`/egfa/:id/${routes.ERGEBNISDOKUMENTATION}`);
  const id = routeMatcher?.params.id;
  const egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');
  const ctrl = GlobalDI.getOrRegister(
    'egfaErgebnisdokumentationController',
    () => new ErgebnisdokumentationController(),
  );
  interface ModuleEntity extends EgfaModuleEntityRequestDTO {
    order?: number;
  }
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [modules, setModules] = useState<ModuleEntity[]>([]);
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const sub = egfaController.getEgfaWithModules({ id } as GetEgfaWithModulesRequest).subscribe({
      next: (data) => {
        ctrl.loadModules(data, props.checkedList).subscribe({
          next: (modulesData) => {
            const tmpModules = modulesData.map((response) => {
              return { ...response.dto, order: 0 };
            });
            let counter = 13;
            tmpModules.forEach((entry) => {
              switch (entry.title) {
                case t('egfa.modules.einnahmenAusgaben.name'):
                  entry.order = 1;
                  break;
                case t('egfa.modules.erfuellungsaufwand.name'):
                  entry.order = 2;
                  break;
                case t('egfa.modules.preise.name'):
                  entry.order = 3;
                  break;
                case t('egfa.modules.kmu.name'):
                  entry.order = 4;
                  break;
                case t('egfa.modules.sonstigeKosten.name'):
                  entry.order = 5;
                  break;
                case t('egfa.modules.enap.name'):
                  entry.order = 6;
                  break;
                case t('egfa.modules.gleichwertigkeit.name'):
                  entry.order = 7;
                  break;
                case t('egfa.modules.demografie.name'):
                  entry.order = 8;
                  break;
                case t('egfa.modules.gleichstellung.name'):
                  entry.order = 9;
                  break;
                case t('egfa.modules.disability.name'):
                  entry.order = 10;
                  break;
                case t('egfa.modules.verbraucher.name'):
                  entry.order = 11;
                  break;

                case t('egfa.modules.weitere.name'):
                  entry.order = 12;
                  break;
                case t('egfa.modules.evaluierung.name'):
                  entry.order = 13;
                  break;
                case t('egfa.modules.experimentierklauseln.name'):
                  entry.order = 14;
                  break;
                default:
                  entry.order = counter;
                  counter++;
                  break;
              }
            });
            tmpModules.sort((a, b) => (a.order && b.order ? a.order - b.order : 0));

            setModules(tmpModules);
          },
          error: (error: AjaxError) => {
            errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
          },
          complete: () => {
            loadingStatusController.setLoadingStatus(false);
          },
        });
        setBreadcrumb(data.egfa.regelungsvorhaben.dto.abkuerzung, !!data.egfa.archiviertAm);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => {
      sub.unsubscribe();
    };
  }, []);

  const setBreadcrumb = (eGFAItemAbkuerzung: string, isArchived: boolean) => {
    const egfaLink = (
      <Link id="egfa-moduleUebersicht-link" key="egfa-moduluebersicht" to="modulUebersicht">
        {eGFAItemAbkuerzung} {t('egfa.header.breadcrumbs.allModules')}
      </Link>
    );
    const breadcrumbTitle = (
      <span key="ergebnisdokumentation">{t('egfa.header.breadcrumbs.ergebnisdokumentation')}</span>
    );
    headerController.setHeaderProps({
      headerLeft: [
        <BreadcrumbComponent
          key="breadcrumb"
          items={[<EgfaHomeBreadcrumb isArchived={isArchived} />, egfaLink, breadcrumbTitle]}
        />,
      ],
    });
  };

  const renderSummary = (module: EgfaModuleEntityRequestDTO): React.ReactElement => {
    const typ: string = module.typ.toLowerCase();
    let moduleSummary: React.ReactElement;
    if (module.typ === EgfaModuleType.Kmu) {
      moduleSummary = renderKmuSummary(module as EgfaModuleKMUEntityDTOAllOf, typ);
    } else if (module.typ === EgfaModuleType.EaOehh) {
      moduleSummary = renderEaOehhSummary(module as EgfaModuleEaOehhEntityDTOAllOf);
    } else if (module.typ === EgfaModuleType.Gleichstellung) {
      moduleSummary = renderGleichstellungSummary(module);
    } else if (module.typ === EgfaModuleType.Erfuellungsaufwand) {
      moduleSummary = rendererfeullungAufwandSummary(module);
    } else if (module.typ === EgfaModuleType.Evaluierung) {
      moduleSummary = renderEvaluierungSummary(module);
    } else if (module.typ === EgfaModuleType.Experimentierklausel) {
      moduleSummary = renderExperimentierklauselnSummary(module);
    } else {
      moduleSummary = renderDefaultSummary(module, typ);
    }
    return (
      <div key={module.typ} style={{ marginTop: '80px' }}>
        <Title level={2}>{t(`egfa.modules.${typ}.name`)}</Title>
        {moduleSummary}
      </div>
    );
  };

  const renderDefaultSummary = (module: EgfaModuleEntityRequestDTO, typ: string): React.ReactElement => {
    return (
      <div key={module.typ}>
        {module.influenceExpected ? (
          <p className="ant-typography p-no-style">{module.influenceSummary}</p>
        ) : (
          <p className="ant-typography p-no-style">{t(`egfa.modules.${typ}.zusammenfassung.noImpact`)}</p>
        )}
      </div>
    );
  };

  const renderExperimentierklauselnSummary = (module: EgfaModuleEntityRequestDTO): React.ReactElement => {
    return (
      <div key={module.typ}>
        {module.influenceExpected ? (
          <p className="ant-typography p-no-style">{module.influenceSummary}</p>
        ) : (
          <p className="ant-typography p-no-style">{t(`egfa.modules.experimentierklauseln.fertigstellung.noImpact`)}</p>
        )}
      </div>
    );
  };

  const renderKmuSummary = (module: EgfaModuleKMUEntityDTOAllOf, typ: string): React.ReactElement => {
    let affectedKey = 'affected';
    if (!module.influenceExpected) {
      affectedKey = 'notAffected';
    } else if (module.influenceExpected && !module.rvHoherErfuellungsaufwand) {
      affectedKey = 'notRelevantAffected';
    }
    const affectedMsg = t(`egfa.modules.${typ}.zusammenfassung.${affectedKey}`);
    return (
      <div key={module.typ}>
        <p>{affectedMsg}</p>
        {module.rvHoherErfuellungsaufwand && module.influenceSummary && (
          <div>
            <Title level={3}>{t(`egfa.modules.${typ}.zusammenfassung.impactHoherErfuellungsaufwand`)}</Title>
            <p>{module.rvUmsetzungSummary || t(`egfa.modules.${typ}.zusammenfassung.unaffected`)}</p>
            <Title level={3}>{t(`egfa.modules.${typ}.zusammenfassung.impactSonstigeKosten`)}</Title>
            <p>{module.rvBelastungSummary}</p>
            <Title level={3}>{t(`egfa.modules.${typ}.zusammenfassung.impactMassnahmen`)}</Title>
            <p>{module.rvUnterstuetzungsmassnahmenSummary}</p>
            <Title level={3}>{t(`egfa.modules.${typ}.zusammenfassung.ergebnisse.title`)}</Title>
            <p>{module.influenceSummary}</p>
          </div>
        )}
      </div>
    );
  };

  const renderEaOehhSummary = (module: EgfaModuleEaOehhEntityDTOAllOf): React.ReactElement => {
    return (
      <div key={module.typ} style={{ maxWidth: 990 }}>
        <ErgebnisdokumentationSummaryComponent formData={module} />
      </div>
    );
  };

  const renderGleichstellungSummary = (module: EgfaModuleEntityRequestDTO): React.ReactElement => {
    return (
      <div key={module.typ}>
        <p className="ant-typography p-no-style">{module.influenceSummary}</p>
      </div>
    );
  };
  const rendererfeullungAufwandSummary = (module: EgfaModuleEntityRequestDTO): React.ReactElement => {
    return (
      <div key={module.typ}>
        <FertigstellungErfuellungsaufwandContent
          formData={module}
          moduleName={routes.ERFUELLUNGSAUFWAND}
          egfaId={module.egfaId || ''}
          ergebnisdokumentation={true}
        />
      </div>
    );
  };

  const renderEvaluierungSummary = (module: EgfaModuleEntityRequestDTO): React.ReactElement => {
    return (
      <div key={module.typ}>
        <EvalueirungFertigstellung formData={module} ergebnisdokumentation={true} />
      </div>
    );
  };

  return (
    <Row>
      <Col xs={{ span: 21, offset: 2 }}>
        <Title level={1}>{t('egfa.ergebnisdokumentation.mainTitle')}</Title>
        <p className="ant-typography p-no-style">{t('egfa.ergebnisdokumentation.instruction')}</p>
        {modules.map((module) => renderSummary(module))}
        <BtnBackComponent
          styleBtn={{ marginTop: 80 }}
          id="ergebnisdokumentaion-back-btn"
          url={`/egfa/${routeMatcher?.params.id}/modulUebersicht`}
        />
      </Col>
    </Row>
  );
}
