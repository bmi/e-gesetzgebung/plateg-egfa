// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { BASE_PATH } from '@plateg/rest-api';
import React from 'react';
import { useTranslation } from 'react-i18next';

export function EgfaHelpComponent(): React.ReactElement {
  const { t } = useTranslation();

  return (
    <p
      className="egfa-help-content"
      dangerouslySetInnerHTML={{
        __html: t('egfa.help.drawerText', {
          linkGGO_43: `${BASE_PATH}/arbeitshilfen/download/34#page=33`,
          linkGGO_44: `${BASE_PATH}/arbeitshilfen/download/34#page=34`,
        }),
      }}
    />
  );
}
