// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Checkbox } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { Observable, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { map, shareReplay } from 'rxjs/operators';

import {
  AktionType,
  BASE_PATH,
  EgfaControllerApi,
  EgfaModuleStatusType,
  EgfaWithModulesTableResponseDTO,
  RegelungsvorhabenEntityFullResponseDTO,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
  RessourceType,
  RolleLokalType,
  UserEntityResponseDTO,
  VorhabenStatusType,
} from '@plateg/rest-api';
import {
  CheckOutlinedGreen,
  compareDates,
  displayMessage,
  DropdownMenu,
  getDateTimeString,
  InfoComponent,
  LoadingStatusController,
  TableComponentProps,
  TextAndContact,
} from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { EgfaWithType } from './freigabe-modal/controller';

interface EGFAModuleItemExpanded extends EGFAModuleItem {
  type?: string;
  order?: number;
}

const onSendToEditorFinish = (success: boolean, moduleName: string, error?: AjaxError) => {
  if (success) {
    displayMessage(`${i18n.t('egfa.modules.exportToEditor.successMessage', { moduleName })}`, 'success');
  } else {
    let status = '';
    if (error) {
      status = (
        error?.response as {
          status: string;
        }
      ).status;
    }
    const editorErrorMessage = status ? i18n.t(`egfa.modules.exportToEditor.errorCodes.${status}`) : '';
    displayMessage(
      `${i18n.t('egfa.modules.exportToEditor.errorMessage', { moduleName })}${editorErrorMessage}`,
      'error',
    );
  }
  const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  loadingStatusCtrl.setLoadingStatus(false);
};
export function getEGFATableVals(
  data: EGFAItem,
  setIsFreigebenModalVisible: (val: boolean) => void,
  setSelectedEgfa: (val: EgfaWithType) => void,
): TableComponentProps<EGFAModuleItem> {
  const rollDisabled = (record: EGFAModuleItemExpanded) => record.rolleTyp === RolleLokalType.Keine;
  const getFreigabeRolleText = (rolle?: string) => {
    const rolleTextMapping: { [key: string]: string } = {
      [RolleLokalType.Federfuehrer]: 'Federführung',
      [RolleLokalType.EgfaModulMitarbeit]: 'Mitarbeit',
      [RolleLokalType.Mitarbeiter]: 'Mitarbeit',
      [RolleLokalType.Gast]: 'Beobachtung',
    };
    return rolleTextMapping[rolle ?? ''] ?? '';
  };

  const content = data.modules;
  const columns: ColumnsType<EGFAModuleItem> = [
    {
      title: i18n.t('egfa.viewEGFA.table.auswahl').toString(),
      key: 'auswahl',
      width: 100,
      className: 'auswahl-cell',
      render: (record: EGFAModuleItemExpanded): ReactElement => {
        let disabled = record.status !== EgfaModuleStatusType.VollstaendigBearbeitet || rollDisabled(record);
        if (record.type) {
          disabled = true;
        }
        return (
          <Checkbox id={`egfa-viewEGFATableAuswahl-chk-${record.id}`} value={record.id} disabled={disabled}></Checkbox>
        );
      },
    },
    {
      title: i18n.t('egfa.viewEGFA.table.module').toString(),
      key: 'module',
      sorter: (a, b) => a.name.localeCompare(b.name),
      render: (record: EGFAModuleItemExpanded): ReactElement => {
        if (record?.type === 'FakeEntry' || rollDisabled(record) || !record.aktionen?.includes(AktionType.Lesen)) {
          return <Text>{record.name}</Text>;
        } else if (record?.type === 'externalLink') {
          return (
            <a id="egfa-eNapExternalNavigation-link" href={'https://www.enap.bund.de/intro'} target="_blank">
              {record.name}
            </a>
          );
        }

        return (
          <Link
            id={`egfa-module-${record.name.toLowerCase().split(' ').join('-')}-link`}
            to={`/egfa/${record.eGFAId}/${routes.MODULE}/${computeLink(
              record.name,
              record.status,
              record.isRvArchived,
            )}`}
          >
            {record.name}
          </Link>
        );
      },
    },
    {
      title: i18n.t('egfa.viewEGFA.table.status').toString(),
      key: 'status',
      render: (record: EGFAModuleItemExpanded) => {
        const icon = record.status === EgfaModuleStatusType.VollstaendigBearbeitet ? <CheckOutlinedGreen /> : undefined;
        if (record?.type === 'externalLink') {
          return <></>;
        }
        return (
          <div className={record.status}>
            {icon}
            <Text>{i18n.t(`egfa.viewEGFA.status.${record.status}`).toString()}</Text>
          </div>
        );
      },
    },
    {
      title: i18n.t('egfa.viewEGFA.table.bearbeitetAm').toString(),
      key: 'bearbeitetAm',
      sorter: (a, b) => compareDates(a.bearbeitetAm, b.bearbeitetAm),
      render: (record: EGFAModuleItemExpanded) => {
        const user = record.bearbeiterStellvertreter || record.bearbeiter;
        return (
          <TextAndContact
            drawerId={record.id}
            firstRow={getDateTimeString(record.bearbeitetAm)}
            drawerTitle={i18n.t('enorm.myVotes.tabs.drafts.table.contactPersonDrawerTitle')}
            user={user.dto || record.bearbeiter.dto}
            isStellvertretung={!!record.bearbeiterStellvertreter?.dto}
          />
        );
      },
    },
    {
      title: (
        <>
          <span className="info-column">
            {i18n.t('egfa.viewEGFA.table.zugriffsrecht').toString()}
            <InfoComponent title={i18n.t(`egfa.viewEGFA.table.zugriffsrechtInfo.title`)}>
              <div
                dangerouslySetInnerHTML={{
                  __html: i18n.t(`egfa.viewEGFA.table.zugriffsrechtInfo.drawerText`),
                }}
              ></div>
            </InfoComponent>
          </span>
        </>
      ),
      key: 'zugriffsrecht',
      render: (record: EGFAModuleItemExpanded): ReactElement => {
        return <Text>{getFreigabeRolleText(record.rolleTyp)}</Text>;
      },
    },
    {
      title: i18n.t('egfa.viewEGFA.table.aktionen').toString(),
      key: 'aktionen',
      render: (record: EGFAModuleItemExpanded) => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t('egfa.viewEGFA.table.freigeben'),
            disabled: () => !record.aktionen.includes(AktionType.BerechtigungenLesen),
            onClick: () => {
              setIsFreigebenModalVisible(true);
              setSelectedEgfa({
                egfa: data,
                type: computeeGFAType(record.name),
                rolleTyp: record.rolleTyp,
                moduleId: record.id,
                aktionen: record.aktionen,
              });
            },
          },
          {
            element: i18n.t('egfa.modules.enap.fertigstellung.exportBtn'),
            disabled: () =>
              !record.aktionen.includes(AktionType.Exportieren) ||
              record.status !== EgfaModuleStatusType.VollstaendigBearbeitet,
            onClick: () => {
              openFile(
                `${BASE_PATH}/file/egfaexport/${record.eGFAId}/${computeLink(
                  record.name,
                  '',
                  false,
                  true,
                  true,
                ).toUpperCase()}/pdf`,
                { target: '_blank', fileName: `${record.name}.pdf` },
              );
            },
          },
        ];
        if (record?.type === 'FakeEntry') {
          return <></>;
        }
        return (
          <DropdownMenu
            isDisabled={rollDisabled(record)}
            openLink={`/egfa/${record.eGFAId}/${routes.MODULE}/${computeLink(
              record.name,
              record.status,
              record.isRvArchived,
            )}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];
  const sortedContent = sortEntries(content);

  return {
    id: data.id,
    expandable: false,
    columns,
    content: sortedContent,
    sorterOptions: [
      {
        columnKey: 'bearbeitetAm',
        titleAsc: i18n.t('egfa.viewEGFA.table.bearbeitetAmAsc'),
        titleDesc: i18n.t('egfa.viewEGFA.table.bearbeitetAmDesc'),
      },
      {
        columnKey: 'module',
        titleAsc: i18n.t('egfa.viewEGFA.table.alphabetischAsc'),
        titleDesc: i18n.t('egfa.viewEGFA.table.alphabetischDesc'),
      },
    ],
  };
}
export function computeeGFAType(moduleName: string): RessourceType | undefined {
  switch (moduleName) {
    case i18n.t('egfa.modules.verbraucher.name'):
      return RessourceType.GfaModulVerbraucher;
    case i18n.t('egfa.modules.preise.name'):
      return RessourceType.GfaModulPreise;
    case i18n.t('egfa.modules.weitere.name'):
      return RessourceType.GfaModulWeitere;
    case i18n.t('egfa.modules.kmu.name'):
      return RessourceType.GfaModulKmu;
    case i18n.t('egfa.modules.gleichwertigkeit.name'):
      return RessourceType.GfaModulGleichwertigkeit;
    case i18n.t('egfa.modules.einnahmenAusgaben.name'):
      return RessourceType.GfaModulEaoehh;
    case i18n.t('egfa.modules.demografie.name'):
      return RessourceType.GfaModulDemografie;
    case i18n.t('egfa.modules.gleichstellung.name'):
      return RessourceType.GfaModulGleichstellung;
    case i18n.t('egfa.modules.disability.name'):
      return RessourceType.GfaModulDisability;
    case i18n.t('egfa.modules.enap.name'):
      return RessourceType.GfaModulEnap;
    case i18n.t('egfa.modules.sonstigeKosten.name'):
      return RessourceType.GfaModulSonstigeKosten;
    case i18n.t('egfa.modules.erfuellungsaufwand.name'):
      return RessourceType.GfaModulErfuellungsaufwand;
    case i18n.t('egfa.modules.evaluierung.name'):
      return RessourceType.GfaModulEvaluierung;
    case i18n.t('egfa.modules.experimentierklauseln.name'):
      return RessourceType.GfaModulExperimentierklauseln;
    default:
      return undefined;
  }
}
export function computeLink(
  moduleName: string,
  recordStatus: string,
  isRvArchived: boolean,
  isPDFExport?: boolean,
  isException?: boolean,
): string {
  let moduleLink = '';

  switch (moduleName) {
    case i18n.t('egfa.modules.verbraucher.name'):
      moduleLink = routes.AUSWIRKUNGEN_VERBRAUCHER;
      break;
    case i18n.t('egfa.modules.preise.name'):
      moduleLink = routes.AUSWIRKUNGEN_PREISE;
      break;
    case i18n.t('egfa.modules.weitere.name'):
      moduleLink = routes.AUSWIRKUNGEN_WEITERE;
      break;

    case i18n.t('egfa.modules.kmu.name'):
      moduleLink = routes.MITTELSTAENDISCHE_UNTERNEHMEN;
      break;

    case i18n.t('egfa.modules.gleichwertigkeit.name'):
      moduleLink = routes.GLEICHWERTIGKEITS_CHECK;
      break;

    case i18n.t('egfa.modules.einnahmenAusgaben.name'):
      moduleLink = routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN;
      break;

    case i18n.t('egfa.modules.demografie.name'):
      moduleLink = routes.DEMOGRAFIE_CHECK;
      break;

    case i18n.t('egfa.modules.gleichstellung.name'):
      moduleLink = routes.GLEICHSTELLUNG;
      break;

    case i18n.t('egfa.modules.disability.name'):
      moduleLink = routes.DISABILITY;
      break;

    case i18n.t('egfa.modules.enap.name'):
      moduleLink = routes.ENAP;
      break;

    case i18n.t('egfa.modules.sonstigeKosten.name'):
      moduleLink = isException ? 'sonstige_kosten' : routes.SONSTIGE_KOSTEN;
      break;

    case i18n.t('egfa.modules.erfuellungsaufwand.name'):
      moduleLink = routes.ERFUELLUNGSAUFWAND;
      break;
    case i18n.t('egfa.modules.evaluierung.name'):
      moduleLink = routes.EVALUIERUNG;
      break;

    case i18n.t('egfa.modules.experimentierklauseln.name'):
      moduleLink = routes.EXPERIMENTIERKLAUSELN;
      break;
  }
  if (
    moduleLink !== routes.ENAP &&
    moduleLink !== routes.DISABILITY &&
    moduleLink !== routes.GLEICHSTELLUNG &&
    moduleLink !== routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN &&
    moduleLink !== routes.ERFUELLUNGSAUFWAND &&
    moduleLink !== routes.EVALUIERUNG
  ) {
    if (recordStatus === EgfaModuleStatusType.VollstaendigBearbeitet.toString() || isRvArchived) {
      moduleLink += '/zusammenfassung';
    }
  }
  if (
    (recordStatus === EgfaModuleStatusType.VollstaendigBearbeitet.toString() || isRvArchived) &&
    (moduleLink === routes.ENAP ||
      moduleLink === routes.DISABILITY ||
      moduleLink === routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN ||
      moduleLink === routes.GLEICHSTELLUNG ||
      moduleLink === routes.ERFUELLUNGSAUFWAND ||
      moduleLink === routes.EVALUIERUNG ||
      moduleLink === routes.SONSTIGE_KOSTEN)
  ) {
    moduleLink += '/fertigstellung';
  }
  if (isPDFExport && moduleLink === routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN) {
    moduleLink = routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN_2;
  }
  if (isPDFExport && moduleLink === routes.EXPERIMENTIERKLAUSELN) {
    moduleLink = `experimentierklausel`;
  }
  return moduleLink;
}

//merge hardcoded fake module entries with real entries from backend to achive correct order
function sortEntries(entries: EGFAModuleItemExpanded[]): EGFAModuleItemExpanded[] {
  const newEntries = [...entries];

  let counter = 13;
  newEntries.forEach((entry) => {
    switch (entry.name) {
      case i18n.t('egfa.modules.einnahmenAusgaben.name'):
        entry.order = 1;
        break;
      case i18n.t('egfa.modules.erfuellungsaufwand.name'):
        entry.order = 2;
        break;
      case i18n.t('egfa.modules.preise.name'):
        entry.order = 3;
        break;
      case i18n.t('egfa.modules.kmu.name'):
        entry.order = 4;
        break;
      case i18n.t('egfa.modules.sonstigeKosten.name'):
        entry.order = 5;
        break;
      case i18n.t('egfa.modules.enap.name'):
        entry.order = 6;
        break;
      case i18n.t('egfa.modules.gleichwertigkeit.name'):
        entry.order = 7;
        break;
      case i18n.t('egfa.modules.demografie.name'):
        entry.order = 8;
        break;
      case i18n.t('egfa.modules.gleichstellung.name'):
        entry.order = 9;
        break;
      case i18n.t('egfa.modules.disability.name'):
        entry.order = 10;
        break;
      case i18n.t('egfa.modules.verbraucher.name'):
        entry.order = 11;
        break;

      case i18n.t('egfa.modules.weitere.name'):
        entry.order = 12;
        break;
      case i18n.t('egfa.modules.evaluierung.name'):
        entry.order = 13;
        break;
      case i18n.t('egfa.modules.experimentierklauseln.name'):
        entry.order = 14;
        break;
      default:
        entry.order = counter;
        counter++;
        break;
    }
  });
  newEntries.sort((a, b) => (a.order && b.order ? a.order - b.order : 0));
  return newEntries;
}

export interface EGFAItem {
  name: string;
  id: string;
  abkuerzung: string;
  isArchived: boolean;
  modules: EGFAModuleItem[];
  erstelltAm: string;
  ersteller: UserEntityResponseDTO;
  regelungsvorhaben: RegelungsvorhabenEntityWithDetailsResponseDTO | RegelungsvorhabenEntityFullResponseDTO;
  aktionen: AktionType[];
}
export interface EGFAModuleItem {
  eGFAId: string;
  name: string;
  id: string;
  status: EgfaModuleStatusType;
  isRvArchived: boolean;
  bearbeitetAm: string;
  bearbeiter: UserEntityResponseDTO;
  bearbeiterStellvertreter: UserEntityResponseDTO;
  rolleTyp: RolleLokalType;
  aktionen: AktionType[];
}

export class ModulesListController {
  private readonly egfaController = GlobalDI.get<EgfaControllerApi>('egfaController');
  private isRVArchived: boolean | undefined;
  private eGFAId = '';

  public getEGFAItemCall(id: string): Observable<EGFAItem> {
    this.eGFAId = id;
    return this.egfaController.getEgfaWithModules({ id }).pipe(
      map((data: EgfaWithModulesTableResponseDTO) => {
        const isRVArchived =
          data.egfa.regelungsvorhaben.dto.status === VorhabenStatusType.Archiviert ||
          (data.egfa.archiviertAm !== undefined && data.egfa.archiviertAm !== null);
        this.isRVArchived = isRVArchived;
        return {
          name: `${data.egfa.regelungsvorhaben.dto.abkuerzung} - ${data.egfa.regelungsvorhaben.dto.kurzbezeichnung}`,
          id: data.egfa.id,
          abkuerzung: data.egfa.regelungsvorhaben.dto.abkuerzung,
          isArchived: isRVArchived,
          erstelltAm: data.egfa.erstelltAm,
          ersteller: data.egfa.ersteller,
          regelungsvorhaben: data.egfa.regelungsvorhaben,
          aktionen: data.egfa.aktionen,
          modules: data.modules.map((item) => {
            return {
              eGFAId: item.egfaId as string,
              name: item.title as string,
              id: item.id as string,
              status: item.status,
              isRvArchived: isRVArchived,
              bearbeitetAm: item.bearbeitetAm || data.egfa.erstelltAm,
              bearbeiter: item.bearbeiter || data.egfa.ersteller,
              bearbeiterStellvertreter: item.bearbeiterStellvertreter,
              rolleTyp: item.rolleTyp,
              aktionen: item.aktionen,
            };
          }),
        } as EGFAItem;
      }),
    );
  }

  public getIsRVArchived(id: string): Observable<boolean> {
    let obs: Observable<boolean>;
    if (!this.isRVArchived) {
      const elementId = id || this.eGFAId;
      obs = this.egfaController.getEgfaWithModules({ id: elementId }).pipe(
        map((data: EgfaWithModulesTableResponseDTO) => {
          this.isRVArchived = data.egfa.regelungsvorhaben.dto.status === VorhabenStatusType.Archiviert;
          return this.isRVArchived;
        }),
      );
      obs.pipe(shareReplay(1));
      obs.subscribe((data) => {
        this.isRVArchived = data;
      });
    } else {
      obs = of(this.isRVArchived);
    }
    return obs;
  }

  public getPDFExportAction(id: string, moduleName: string, status?: EgfaModuleStatusType): ReactElement[] {
    if (status !== EgfaModuleStatusType.VollstaendigBearbeitet) {
      return [];
    }
    const fileName = i18n.t(`egfa.modules.${moduleName}.name`).toString();

    if (moduleName === routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN) {
      moduleName = routes.AUSWIRKUNGEN_EINNAHMENAUSGABEN_2;
    }
    if (moduleName === routes.SONSTIGE_KOSTEN) {
      moduleName = 'sonstige_kosten';
    }

    if (moduleName === routes.EXPERIMENTIERKLAUSELN) {
      moduleName = 'experimentierklausel';
    }

    return [
      <Button
        key="pdf-export-action-btn"
        type="default"
        onClick={(e) => {
          const pdfUrl = `${BASE_PATH}/file/egfaexport/${id}/${moduleName.toUpperCase()}/pdf`;
          openFile(pdfUrl, {
            target: '_blank',
            fileName: `${fileName}.pdf`,
            onStart: () => {
              e.target.disabled = true;
            },
            onSuccess: () => {
              e.target.disabled = false;
            },
            onError: (error) => {
              console.log(error);
              e.target.disabled = false;
            },
          });
        }}
      >
        {i18n.t(`egfa.modules.enap.fertigstellung.exportBtn`).toString()}
      </Button>,
    ];
  }
}
