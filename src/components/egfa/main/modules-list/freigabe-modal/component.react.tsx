// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import './freigabe-modal.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  BerechtigungAnBenutzerDTO,
  BerechtigungResponseDTO,
  FreigabeEntityResponseDTO,
  RolleLokalType,
  UserEntityDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { FreigabeModal } from '@plateg/theme';
import { UserWithRolle } from '@plateg/theme/src/components/freigabe-modal/freigabe-list/component.react';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { EgfaWithType, FreigabeModalEgfaController } from './controller';

interface FreigabeModalEgfaProps {
  disabled?: boolean;
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  selectedEgfa: EgfaWithType | undefined;
  allowChanges?: boolean;
  loadContent?: () => void;
}

export function FreigabeModalEgfa(props: FreigabeModalEgfaProps): React.ReactElement {
  const { t } = useTranslation();
  const freigebenModalController = GlobalDI.getOrRegister(
    'freigabeBerechtigungController',
    () => new FreigabeModalEgfaController(),
  );
  const appStore = useAppSelector((state) => state.user);
  const [checkOwner, setCheckOwner] = useState(true);

  const [federfuehrung, setFederfuehrung] = useState<UserEntityDTO | undefined>();
  const [freigaben, setFreigaben] = useState<FreigabeEntityResponseDTO[]>([]);
  const [documentEditor, setDocumentEditor] = useState<UserWithRolle | undefined>();
  const [editorFreigabe, setEditorFreigabe] = useState<UserWithRolle | undefined>();
  const getFreigaben = (listEditor = false) => {
    if (props.selectedEgfa) {
      freigebenModalController.getFreigaben(props.selectedEgfa, (value: BerechtigungResponseDTO[]) => {
        const foundEditorFreigabe = value.find(
          (item) =>
            item.berechtigter.base.id === appStore.user?.base.id &&
            (item.rolleType === RolleLokalType.EgfaModulMitarbeit || item.rolleType === RolleLokalType.Mitarbeiter),
        );
        setEditorFreigabe(foundEditorFreigabe?.berechtigter);

        if (foundEditorFreigabe && !listEditor) {
          setCheckOwner(false);
          setDocumentEditor({ ...foundEditorFreigabe.berechtigter, rolleType: foundEditorFreigabe.rolleType });
          value = value.filter((item) => item.berechtigter.base.id !== foundEditorFreigabe.berechtigter.base.id);
        } else {
          setCheckOwner(false);
          setDocumentEditor(undefined);
        }

        const federfuehrung = value.filter((item) => item.rolleType === RolleLokalType.Federfuehrer);
        if (federfuehrung.length > 0) {
          setFederfuehrung(federfuehrung[0].berechtigter.dto);
        }

        setFreigaben(
          value
            .filter((item) => item.rolleType !== RolleLokalType.Federfuehrer)
            .map((item) => {
              return {
                base: item.berechtigter.base,
                dto: {
                  readOnlyAccess:
                    item.rolleType === RolleLokalType.Gast ||
                    (item.berechtigter.base.id === foundEditorFreigabe?.berechtigter.base.id ?? true),
                  user: item.berechtigter,
                },
                rolleType: item.rolleType,
              };
            }),
        );
      });
    }
  };
  const texts = {
    title: `Modul: „${freigebenModalController.getModuleName(props.selectedEgfa?.type) || ''}“`,
  };
  useEffect(() => {
    if (props.selectedEgfa && props.isVisible !== false) {
      getFreigaben();
    }
  }, [props.selectedEgfa, props.isVisible]);
  const freigeben = (vals: BerechtigungAnBenutzerDTO[], resetModal: (isVisible: boolean) => void) => {
    const oldUsers: BerechtigungAnBenutzerDTO[] = freigaben.map((item) => {
      return {
        email: item.dto.user.dto.email,
        rolleType: item.dto.readOnlyAccess ? RolleLokalType.Gast : RolleLokalType.Mitarbeiter,
      };
    });
    const resultOfComparing = Filters.prepareBerechtigungenRequestBody(vals, oldUsers);
    if (!documentEditor && editorFreigabe) {
      resultOfComparing['modify'].push({
        email: editorFreigabe.dto.email,
        rolleType: RolleLokalType.Gast,
      });
    }

    if (props.selectedEgfa) {
      freigebenModalController.vorlageFreigeben(
        props.selectedEgfa.egfa.name,
        props.selectedEgfa,
        resultOfComparing,
        resetModal,
        !documentEditor && editorFreigabe ? props.loadContent : getFreigaben,
      );
    }
  };
  return (
    <FreigabeModal
      documentEditor={documentEditor}
      ersteller={federfuehrung}
      checkOwner={checkOwner}
      {...props}
      ressortCompare={false}
      freigaben={freigaben}
      leserechtOnly={false}
      className="rv-freigabe-modal"
      texts={texts}
      freigeben={freigeben}
      isRBACActivated={true}
      isEGFAModule={true}
      customModalText={{
        title: t('egfa.modules.freigabeModal.title'),
        bodyText: (
          <div
            dangerouslySetInnerHTML={{
              __html: t('egfa.modules.freigabeModal.bodyText', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></div>
        ),
      }}
      setDocumentEditor={(user: UserEntityResponseDTO | undefined) => {
        getFreigaben(true);
        setDocumentEditor(user);
      }}
    />
  );
}
