// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import i18n from 'i18next';
import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import {
  BerechtigungAnBenutzerDTO,
  BerechtigungControllerApi,
  Configuration,
  FreigabeBerechtigungEntityListResponseDTO,
  RessourceType,
  RolleLokalType,
  UpdatePermissionsDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { loadingStatusSetStub } from '../../../../../general.test';
import { EgfaWithType, FreigabeModalEgfaController } from './controller';

const freigebenModalController = GlobalDI.getOrRegister(
  'freigabeBerechtigungController',
  () => new FreigabeModalEgfaController(),
);
const berechtigungController = GlobalDI.getOrRegister<BerechtigungControllerApi>(
  'berechtigungControllerApi',
  () => new BerechtigungControllerApi(new Configuration()),
);
describe('TEST: Freigaben Modal controller', () => {
  let getFreigabenBerechtigungenStub: sinon.SinonStub;
  let setFreigabenBerechtigungenStub: sinon.SinonStub;
  let handleSaveStub: sinon.SinonStub;
  let getSuccessMsgStub: sinon.SinonStub;
  const egfa: EgfaWithType = {
    egfa: {
      name: 'test',
      id: '123',
      abkuerzung: 'test',
      isArchived: true,
      modules: [],
      erstelltAm: '11/02/2023',
      ersteller: undefined,
      regelungsvorhaben: undefined,
    },
    type: RessourceType.GfaModulDemografie,
  };
  before(() => {
    setFreigabenBerechtigungenStub = sinon.stub(berechtigungController, 'setBerechtigungen');
    handleSaveStub = sinon.stub(FreigabeModalEgfaController.prototype, <any>'handleSave');
    getSuccessMsgStub = sinon.stub(FreigabeModalEgfaController.prototype, <any>'getSuccessMsg');
  });
  afterEach(() => {
    handleSaveStub.resetHistory();
    getSuccessMsgStub.resetHistory();
    loadingStatusSetStub.resetHistory();
    setFreigabenBerechtigungenStub.resetHistory();
    handleSaveStub.resetHistory();
  });
  after(() => {
    loadingStatusSetStub.reset();
  });
  it('check if setFreigabenBerechtigungen API is called success', (done) => {
    setFreigabenBerechtigungenStub.returns(of(void 0));
    loadingStatusSetStub.returns(void 0);
    const freigebenValues: UpdatePermissionsDTO = {
      add: [{ rolleType: RolleLokalType.Gast, email: 'test@test.test' }] as BerechtigungAnBenutzerDTO[],
      modify: [] as BerechtigungAnBenutzerDTO[],
      remove: [] as BerechtigungAnBenutzerDTO[],
    };
    freigebenModalController.vorlageFreigeben('title', egfa, freigebenValues, (visible: boolean) => {
      console.log(visible);
    });
    setTimeout(() => {
      sinon.assert.calledOnce(setFreigabenBerechtigungenStub);
      sinon.assert.called(loadingStatusSetStub);
      sinon.assert.called(handleSaveStub);
      done();
    }, 500);
  });
  it('check if setFreigabenBerechtigungen API is called error', (done) => {
    setFreigabenBerechtigungenStub.returns(throwError({ status: 404 }));
    loadingStatusSetStub.returns(void 0);
    const freigebenValues: UpdatePermissionsDTO = {
      add: [{ rolleType: RolleLokalType.Gast, email: 'test@test.test' }] as BerechtigungAnBenutzerDTO[],
      modify: [] as BerechtigungAnBenutzerDTO[],
      remove: [] as BerechtigungAnBenutzerDTO[],
    };
    freigebenModalController.vorlageFreigeben('title', egfa, freigebenValues, (visible: boolean) => {
      console.log(visible);
    });
    setTimeout(() => {
      sinon.assert.calledOnce(setFreigabenBerechtigungenStub);
      sinon.assert.called(loadingStatusSetStub);
      sinon.assert.notCalled(handleSaveStub);
      sinon.assert.notCalled(getSuccessMsgStub);
      done();
    }, 500);
  });

  it('check if getFreigabenBerechtigungen API is called', (done) => {
    getFreigabenBerechtigungenStub = sinon.stub(berechtigungController, 'getBerechtigungen');
    getFreigabenBerechtigungenStub.returns(of({ dtos: [] } as FreigabeBerechtigungEntityListResponseDTO));
    const cbObj = {
      cb: () => {
        console.log('cb called');
      },
    };
    const cbStub = sinon.stub(cbObj, 'cb');
    freigebenModalController.getFreigaben(egfa, cbObj.cb);
    setTimeout(() => {
      sinon.assert.calledOnce(getFreigabenBerechtigungenStub);
      sinon.assert.calledOnce(cbStub);
      done();
    }, 50);
  });
  it('check if getModuleName return right value', () => {
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulEaoehh)).to.eql(
      i18n.t('egfa.modules.einnahmenAusgaben.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulVerbraucher)).to.eql(
      i18n.t('egfa.modules.verbraucher.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulPreise)).to.eql(
      i18n.t('egfa.modules.preise.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulWeitere)).to.eql(
      i18n.t('egfa.modules.weitere.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulKmu)).to.eql(i18n.t('egfa.modules.kmu.name'));
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulGleichwertigkeit)).to.eql(
      i18n.t('egfa.modules.gleichwertigkeit.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulDemografie)).to.eql(
      i18n.t('egfa.modules.demografie.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulGleichstellung)).to.eql(
      i18n.t('egfa.modules.gleichstellung.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulDisability)).to.eql(
      i18n.t('egfa.modules.disability.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulEnap)).to.eql(i18n.t('egfa.modules.enap.name'));
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulSonstigeKosten)).to.eql(
      i18n.t('egfa.modules.sonstigeKosten.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulErfuellungsaufwand)).to.eql(
      i18n.t('egfa.modules.erfuellungsaufwand.name'),
    );
    expect(freigebenModalController.getModuleName(RessourceType.GfaModulEvaluierung)).to.eql(
      i18n.t('egfa.modules.evaluierung.name'),
    );
    expect(freigebenModalController.getModuleName(undefined)).to.eql(undefined);
  });
});
