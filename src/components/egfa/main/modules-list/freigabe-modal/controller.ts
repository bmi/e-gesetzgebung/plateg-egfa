// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  BerechtigungControllerApi,
  BerechtigungListResponseDTO,
  BerechtigungResponseDTO,
  RessourceType,
  RolleLokalType,
  UpdatePermissionsDTO,
} from '@plateg/rest-api';
import { displayMessage, ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { EGFAItem } from '../controller';

export interface EgfaWithType {
  egfa: EGFAItem;
  type: RessourceType | undefined;
  rolleTyp?: RolleLokalType;
  moduleId: string;
  aktionen?: AktionType[];
}
export class FreigabeModalEgfaController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly berechtigungController = GlobalDI.getOrRegister<BerechtigungControllerApi>(
    'berechtigungControllerApi',
    () => new BerechtigungControllerApi(),
  );

  private readonly errorController = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  public getFreigaben(egfaWithType: EgfaWithType, setFreigaben: (value: BerechtigungResponseDTO[]) => void) {
    if (egfaWithType.type) {
      this.berechtigungController
        .getBerechtigungen({
          id: egfaWithType.moduleId,
          type: egfaWithType.type,
        })
        .subscribe({
          next: (data: BerechtigungListResponseDTO) => {
            setFreigaben(data.dtos);
          },
          error: (error: AjaxError) => {
            setFreigaben([]);
            this.errorCallback(error);
          },
        });
    }
  }

  public vorlageFreigeben(
    objectTitle: string | undefined,
    egfaWithType: EgfaWithType,
    freigebenValues: UpdatePermissionsDTO,
    setIsVisible: (visible: boolean) => void,
    reloadFreigeben?: () => void,
  ): void {
    setIsVisible(false);
    this.loadingStatusController.setLoadingStatus(true);
    if (egfaWithType.type) {
      const freigabeRequest = this.berechtigungController.setBerechtigungen({
        type: egfaWithType.type,
        id: egfaWithType.moduleId,
        updatePermissionsDTO: freigebenValues,
      });

      freigabeRequest.subscribe({
        next: () => {
          this.handleSave(objectTitle || '');
          if (reloadFreigeben) {
            reloadFreigeben();
          }
        },
        error: (error: AjaxError) => this.errorCallback(error),
      });
    }
  }
  public getModuleName(moduleType: RessourceType | undefined): string | undefined {
    switch (moduleType) {
      case RessourceType.GfaModulVerbraucher:
        return i18n.t('egfa.modules.verbraucher.name');
      case RessourceType.GfaModulPreise:
        return i18n.t('egfa.modules.preise.name');
      case RessourceType.GfaModulWeitere:
        return i18n.t('egfa.modules.weitere.name');
      case RessourceType.GfaModulKmu:
        return i18n.t('egfa.modules.kmu.name');
      case RessourceType.GfaModulGleichwertigkeit:
        return i18n.t('egfa.modules.gleichwertigkeit.name');
      case RessourceType.GfaModulEaoehh:
        return i18n.t('egfa.modules.einnahmenAusgaben.name');
      case RessourceType.GfaModulDemografie:
        return i18n.t('egfa.modules.demografie.name');
      case RessourceType.GfaModulGleichstellung:
        return i18n.t('egfa.modules.gleichstellung.name');
      case RessourceType.GfaModulDisability:
        return i18n.t('egfa.modules.disability.name');
      case RessourceType.GfaModulEnap:
        return i18n.t('egfa.modules.enap.name');
      case RessourceType.GfaModulSonstigeKosten:
        return i18n.t('egfa.modules.sonstigeKosten.name');
      case RessourceType.GfaModulErfuellungsaufwand:
        return i18n.t('egfa.modules.erfuellungsaufwand.name');
      case RessourceType.GfaModulEvaluierung:
        return i18n.t('egfa.modules.evaluierung.name');
      case RessourceType.GfaModulExperimentierklauseln:
        return i18n.t('egfa.modules.experimentierklauseln.name');
      default:
        return undefined;
    }
  }

  private handleSave(title: string): void {
    this.loadingStatusController.setLoadingStatus(false);
    displayMessage(this.getSuccessMsg(title), 'success');
  }

  private errorCallback(error: AjaxError): void {
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorController.displayErrorMsg(error, 'egfa.viewEGFA.table.freigabe.error');
  }

  private getSuccessMsg(title: string): string {
    return i18n.t(`egfa.viewEGFA.table.freigabe.success`, {
      title: title,
    });
  }
}
