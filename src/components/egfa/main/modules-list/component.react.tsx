// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './view-egfa.less';

import { Button, Checkbox, Col, Row, Typography } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import React, { Fragment, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  BASE_PATH,
  CodelistenControllerApi,
  CodelistenResponseDTO,
  EgfaModuleStatusType,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityFullResponseDTO,
  RegelungsvorhabenEntityWithDetailsResponseDTO,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  DropdownMenu,
  ErrorController,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
  RVInfoComponent,
  TableComponent,
  TableComponentProps,
  TitleWrapperComponent,
} from '@plateg/theme';
import { openFile } from '@plateg/theme/src/controllers/RestConfigController';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { EgfaHelpHeader } from '../../component.react';
import { EGFAItem, EGFAModuleItem, getEGFATableVals, ModulesListController } from './controller';
import { FreigabeModalEgfa } from './freigabe-modal/component.react';
import { EgfaWithType } from './freigabe-modal/controller';

interface ModulesListProps {
  checkedList: string[];
  setCheckedList: (moduleIds: string[]) => void;
}
export function ModulesList(props: ModulesListProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title, Paragraph } = Typography;
  const ctrl = GlobalDI.getOrRegister('viewEGFAController', () => new ModulesListController());
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');
  const routeMatcherVorbereitung = useRouteMatch<{ id: string }>('/egfa/:id');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const regelungsvorhabenCtrl = GlobalDI.getOrRegister(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  const codelistenControllerApi = GlobalDI.getOrRegister(
    'codelistenControllerApi',
    () => new CodelistenControllerApi(),
  );
  const history = useHistory();
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);
  const [selectedEgfa, setSelectedEgfa] = useState<EgfaWithType | undefined>();
  const [eGFAItemTableVals, setEGFAItemTableVals] = useState<TableComponentProps<EGFAModuleItem>>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
  });
  const [eGFAItem, setEGFAItem] = useState<EGFAItem>({
    name: '',
    id: 0,
    abkuerzung: '',
    modules: [],
    isArchived: false,
  });

  const [checkAll, setCheckAll] = useState<boolean>(false);
  const [codeList, setCodeList] = useState<CodelistenResponseDTO>();
  useEffect(() => {
    setBreadcrumb(eGFAItem.abkuerzung, eGFAItem.isArchived, eGFAItem.regelungsvorhaben, codeList);
  }, [eGFAItem]);
  useEffect(() => {
    loadContent();
    const codelistSub = codelistenControllerApi.getCodelisten().subscribe({
      next: (codeListResponse: CodelistenResponseDTO) => setCodeList(codeListResponse),
    });
    return () => {
      codelistSub.unsubscribe();
    };
  }, []);

  const loadContent = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = ctrl.getEGFAItemCall(routeMatcherVorbereitung?.params?.id || '').subscribe({
      next: (data) => {
        props.setCheckedList([]);
        setEGFAItemTableVals(getEGFATableVals(data, setIsFreigebenModalVisible, setSelectedEgfa));
        regelungsvorhabenCtrl.getRegelungsvorhabenFull({ id: data.regelungsvorhaben.base.id }).subscribe({
          next: (rvData) => {
            setEGFAItem({ ...data, regelungsvorhaben: rvData as RegelungsvorhabenEntityFullResponseDTO });
          },
          error: (error: AjaxError) => {
            console.error('Error loading RV data', error);
          },
        });
        loadingStatusController.setLoadingStatus(false);
        sub.unsubscribe();
      },
      error: (error: AjaxError) => {
        sub.unsubscribe();
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
      },
    });

    return () => {
      sub.unsubscribe();
    };
  };

  const setBreadcrumb = (
    eGFAItemAbkuerzung: string,
    isArchived: boolean,
    regelungsvorhaben: RegelungsvorhabenEntityWithDetailsResponseDTO,
    codeList?: CodelistenResponseDTO,
  ) => {
    const homeLink = (
      <Link id="egfa-home-link" key="egfa-home" to={`/egfa/${isArchived ? routes.ARCHIV : routes.MEINE_EGFAS}`}>
        {t(`egfa.header.breadcrumbs.projectName`)} -{' '}
        {t(`egfa.home.gesetzesfolgenabschatzung.${isArchived ? 'archiv' : 'meineGesetzesfolgeabschaetzungen'}.title`)}
      </Link>
    );
    const archivedLabel = isArchived ? t('egfa.header.breadcrumbs.archived') : '';
    const btnDataExport =
      eGFAItem.id && eGFAItem.aktionen?.includes(AktionType.Exportieren) ? (
        <Button
          key="egfa-export-pdf-btn"
          id="egfa-export-pdf-btn"
          onClick={(e) => {
            openFile(`${BASE_PATH}/file/egfaexport/${eGFAItem.id}/pdf`, {
              target: '_blank',
              fileName: `${eGFAItem.name}.pdf`,
              onStart: () => {
                e.target.disabled = true;
              },
              onSuccess: () => {
                e.target.disabled = false;
              },
              onError: (error) => {
                console.log(error);
                e.target.disabled = false;
              },
            });
          }}
          type="primary"
        >
          {t('egfa.modules.general.dataExportBtn')}
        </Button>
      ) : (
        <Fragment key={'fragment-egfa-export-pdf-btn'}></Fragment>
      );
    const breadcrumbTitle = (
      <span key={`modulUebersicht`}>
        {`${archivedLabel}${eGFAItemAbkuerzung}`} {t('egfa.header.breadcrumbs.allModules')}
      </span>
    );
    const rvInfoDrawer = eGFAItem.aktionen?.includes(AktionType.Lesen) ? (
      <InfoComponent
        isContactPerson={false}
        title={t('egfa.modules.general.infoRvTitle')}
        buttonText={t('egfa.modules.general.infoRv')}
        key="egfa-header-rv-info"
        titleWithoutPrefix={true}
      >
        <RVInfoComponent
          regelungsvorhaben={regelungsvorhaben}
          codeList={codeList}
          isCreator={eGFAItem.aktionen.includes(AktionType.Lesen)}
        />
      </InfoComponent>
    ) : (
      <Fragment key={'fragment-rv-info-btn'}></Fragment>
    );
    const rightHeaderButtons = [
      btnDataExport,
      rvInfoDrawer,
      <EgfaHelpHeader key="egfa-header-help" />,
      <DropdownMenu
        key={'egfa-module-list-header'}
        items={[
          {
            element: t('egfa.modules.general.dataExportBtn'),
            onClick: () => openFile(`${BASE_PATH}/file/egfaexport/${eGFAItem.id}/pdf`, { target: '_blank' }),
            disabled: () => !eGFAItem.aktionen?.includes(AktionType.Exportieren),
          },
          {
            element: rvInfoDrawer,
          },
          { element: <EgfaHelpHeader key="egfa-header-help" /> },
        ]}
        elementId={'headerRightAlternative'}
        overlayClass={'headerRightAlternative-overlay'}
      />,
    ];

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[homeLink, breadcrumbTitle]} />],
      headerRight: rightHeaderButtons,
    });
  };

  const onChangeCheckbox = (checkedValues: (string | boolean)[]) => {
    props.setCheckedList(checkedValues as string[]);
    setCheckAll(checkedValues.length === eGFAItem.modules.length);
  };

  const onCheckAllChange = (e: CheckboxChangeEvent) => {
    if (e.target.checked) {
      const modulesToCheck = eGFAItem.modules
        .filter((item) => item.status === EgfaModuleStatusType.VollstaendigBearbeitet)
        .map((item) => item.id);

      props.setCheckedList(modulesToCheck);
    } else {
      props.setCheckedList([]);
    }
    setCheckAll(e.target.checked);
  };

  return (
    <>
      <FreigabeModalEgfa
        isVisible={isFreigebenModalVisible}
        setIsVisible={setIsFreigebenModalVisible}
        selectedEgfa={selectedEgfa}
        disabled={!selectedEgfa?.aktionen?.includes(AktionType.BerechtigungenAendern)}
        allowChanges={
          selectedEgfa?.aktionen?.includes(AktionType.BerechtigungenAendern) ||
          selectedEgfa?.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)
        }
        loadContent={loadContent}
      />

      <div className="view-egfa-page">
        <TitleWrapperComponent>
          <Row>
            <Col xs={{ span: 22, offset: 1 }}>
              <div className="heading-holder">
                <Title level={2}>
                  {eGFAItem.regelungsvorhaben?.dto.abkuerzung} {t('egfa.header.breadcrumbs.allModules')}
                </Title>
              </div>
            </Col>
          </Row>
        </TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            {(eGFAItem.regelungsvorhaben as RegelungsvorhabenEntityFullResponseDTO)?.dto.grundtyp
              ?.toString()
              .toUpperCase()
              .includes('RECHTSVERORDNUNG') && (
              <div className="hinweis-holder">
                <Title style={{ margin: 0, marginBottom: 4 }} level={5}>
                  {t('egfa.viewEGFA.notice.title')}
                </Title>
                <Paragraph style={{ fontSize: 16, margin: 0 }}>{t('egfa.viewEGFA.notice.text')}</Paragraph>
              </div>
            )}
            <div className="main-holder">
              {eGFAItemTableVals?.content?.length > 0 && (
                <>
                  <div className="actions-holder">
                    <Checkbox
                      id="egfa-viewEGFACheckAll-chk"
                      onChange={onCheckAllChange}
                      checked={checkAll}
                      className="check-all"
                      disabled={
                        eGFAItem.modules.every((item) => item.status !== EgfaModuleStatusType.VollstaendigBearbeitet) ||
                        !eGFAItem.aktionen.includes(AktionType.Lesen)
                      }
                    >
                      {t('egfa.viewEGFA.checkAll')}
                    </Checkbox>
                    <div className="button-holder">
                      <Button
                        id="egfa-viewEGFAExport-btn"
                        type="default"
                        size="small"
                        disabled={!props.checkedList.length}
                        onClick={() => {
                          history.push(
                            `/egfa/${routeMatcherVorbereitung?.params.id as string}/${routes.ERGEBNISDOKUMENTATION}`,
                          );
                        }}
                      >
                        {t('egfa.viewEGFA.exportBtn')}
                      </Button>
                    </div>
                  </div>
                  <Checkbox.Group onChange={onChangeCheckbox} value={props.checkedList}>
                    <TableComponent
                      id="egfa-view-table"
                      expandable={eGFAItemTableVals.expandable}
                      columns={eGFAItemTableVals.columns}
                      content={eGFAItemTableVals.content}
                      filteredColumns={eGFAItemTableVals.filteredColumns}
                      sorterOptions={eGFAItemTableVals.sorterOptions}
                      autoHeight={true}
                    ></TableComponent>
                  </Checkbox.Group>
                </>
              )}
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
}
