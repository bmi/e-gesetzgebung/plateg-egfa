// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { HeaderComponent, HeaderController, InfoComponent, LoadingStatusController } from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { MainRoutesEgfa } from './main/component.react';
import { EgfaHelpComponent } from './main/help/component.react';

export function EgfaHomeBreadcrumb(props: { isArchived?: boolean }): React.ReactElement {
  const { t } = useTranslation();
  if (props.isArchived) {
    return (
      <Link id="egfa-home-link" key="egfa-home" to="/egfa/archiv">
        {`${t('egfa.header.breadcrumbs.projectName')} - ${t('egfa.home.gesetzesfolgenabschatzung.archiv.title')}`}
      </Link>
    );
  }
  return (
    <Link id="egfa-home-link" key="egfa-home" to="/egfa">
      {t('egfa.header.linkHome')}
    </Link>
  );
}

export function EgfaHelpHeader(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <InfoComponent title={t('egfa.help.drawerTitle')} buttonText={t('egfa.header.linkHelp')}>
      <EgfaHelpComponent />
    </InfoComponent>
  );
}

const { Header, Content } = Layout;

export function LayoutWrapperEgfa(): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);
  const headerController = GlobalDI.get<HeaderController>('egfaHeaderController');

  useEffect(() => {
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error) => {
        console.log('Error sub', error);
      },
    });
    loadingStatusController.initLoadingStatus();

    return () => subs.unsubscribe();
  }, []);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{
          height: '100%',
          display: loadingStatus === true ? 'none' : 'unset',
        }}
        className="site-layout-background"
      >
        <HeaderComponent ctrl={headerController} headerId="egfa-header" />
        <Content className="ant-layout main-content-area has-drawer">
          <div className="ant-layout-content">
            <MainRoutesEgfa />
          </div>
        </Content>
      </Layout>
    </Spin>
  );
}
