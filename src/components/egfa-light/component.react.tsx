// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../styles/plateg-egfa.less';

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Configuration, EgfaControllerApi, EgfaModuleEnapControllerApi, ExportControllerApi } from '@plateg/rest-api';
import { configureRestApi, GlobalDI, HeaderController, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../shares/routes';
import { HomeLightComponent } from './main/home/component.react';
import { EnapLightSteps } from './main/steps/component.react';

configureRestApi(registerRestApis);
export function AppLightEgfa(): React.ReactElement {
  return (
    <Switch>
      <Route exact path="/egfa">
        <HomeLightComponent />
      </Route>
      <Route path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}`]}>
        <EnapLightSteps />
      </Route>
    </Switch>
  );
}
function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  GlobalDI.getOrRegister('egfaModuleEnapControllerApi', () => new EgfaModuleEnapControllerApi(configRestCalls));
  GlobalDI.getOrRegister('egfaHeaderController', () => new HeaderController());
  GlobalDI.getOrRegister('egfaController', () => new EgfaControllerApi(configRestCalls));
  GlobalDI.getOrRegister('exportControllerApi', () => new ExportControllerApi(configRestCalls));
}
