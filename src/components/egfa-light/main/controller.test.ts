// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import sinon from 'sinon';
import { v4 as uuidv4 } from 'uuid';

import { EgfaModuleEnapSdg1EntityDTO, EgfaModuleStatusType, EgfaModuleType } from '@plateg/rest-api';

import { initLocalstorageMock } from '../../../general.test';
import { collectionKey, EnapCollectionInterface, EnapCollectionItemInterface, EnapLightController } from './controller';
describe('Test Save Regelungsentwurf', () => {
  const ctrl = new EnapLightController();
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Update in Storage', () => {
    const mockId1 = ctrl.createNewRegelungsentwurf('Draft1');
    let mock1 = ctrl.getDraftById(mockId1);

    //add some details
    ctrl.saveDraftContent({
      egfaId: mockId1,
      typ: 'ENAP' as EgfaModuleType,
      influenceExpected: false,
      deleted: false,
      status: 'IN_BEARBEITUNG' as EgfaModuleStatusType,
      sdg1: {
        indikatoren: [
          {
            indikatorKey: 'sdg1_indikatoren_0_indikatorKey',
            begruendungen: [
              {
                begruendungKey: 'sdg1_indikatoren_0_begruendungen_0_begruendungKey',
                begruendungText: 'Begründung zum Indikator 1.1.a',
              },
              { begruendungKey: 'sdg1_indikatoren_0_begruendungen_1_begruendungKey' },
            ],
            relevant: true,
          },
        ],
      } as EgfaModuleEnapSdg1EntityDTO,
    });
    mock1 = ctrl.getDraftById(mockId1);
    expect(mock1?.dto?.sdg1?.indikatoren.length).to.equal(1);
    expect(mock1?.dto?.sdg1?.indikatoren[0].begruendungen.length).to.equal(2);
    expect(mock1?.dto?.sdg1?.indikatoren[0].relevant).to.be.true;
  });
});

describe('Retrieve draftList from Localstorage', () => {
  const ctrl = new EnapLightController();
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Empty Storage', () => {
    expect(ctrl.getDraftList()).to.eql([]);
  });
  it('Empty Draftcollection', () => {
    global.window.localStorage.setItem(collectionKey, '{}');
    expect(ctrl.getDraftList()).to.eql([]);
  });
  it('Existing draft', () => {
    global.window.localStorage.setItem(
      collectionKey,
      '{"5ddcca45-5085-4c8a-8198-81daf59a7e70":{"name":"hello","lastmodified":"2021-11-26T10:52:43.239Z","id":"5ddcca45-5085-4c8a-8198-81daf59a7e70","dto":{"egfaId":"5ddcca45-5085-4c8a-8198-81daf59a7e70","typ":"ENAP","influenceExpected":false,"deleted":false,"status":"IN_BEARBEITUNG","sdg1":null,"sdg2":null,"sdg3":null,"sdg4":null,"sdg5":null,"sdg6":null,"sdg7":null,"sdg8":null,"sdg9":null,"sdg10":null,"sdg11":null,"sdg12":null,"sdg13":null,"sdg14":null,"sdg15":null,"sdg16":null,"sdg17":null,"prinzipien":null,"sdgs":[]}}}',
    );
    expect(ctrl.getDraftList()).to.eql([
      {
        name: 'hello',
        lastmodified: '2021-11-26T10:52:43.239Z',
        id: '5ddcca45-5085-4c8a-8198-81daf59a7e70',
        dto: {
          egfaId: '5ddcca45-5085-4c8a-8198-81daf59a7e70',
          typ: 'ENAP',
          influenceExpected: false,
          deleted: false,
          status: 'IN_BEARBEITUNG',
          sdg1: null,
          sdg2: null,
          sdg3: null,
          sdg4: null,
          sdg5: null,
          sdg6: null,
          sdg7: null,
          sdg8: null,
          sdg9: null,
          sdg10: null,
          sdg11: null,
          sdg12: null,
          sdg13: null,
          sdg14: null,
          sdg15: null,
          sdg16: null,
          sdg17: null,
          prinzipien: null,
          sdgs: [],
        },
      },
    ]);
  });
});

describe('Test Create New Regelungsentwurf', () => {
  const ctrl = new EnapLightController();
  beforeEach(() => {
    initLocalstorageMock();
  });
  it('Empty Storage', () => {
    const mockId1 = ctrl.createNewRegelungsentwurf('Draft1');
    const mockId2 = ctrl.createNewRegelungsentwurf('');
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: EnapCollectionInterface = JSON.parse(draftCollectionString) as EnapCollectionInterface;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
    expect(draftCollection[mockId1]).to.have.all.keys('lastmodified', 'name', 'id', 'dto');
    expect(draftCollection[mockId2]).to.have.all.keys('lastmodified', 'name', 'id', 'dto');
    expect(draftCollection[mockId1]['name']).to.eql('Draft1');
    expect(draftCollection[mockId2]['name']).to.eql('');
  });
});

describe('Test delete Regelungsentwurf', () => {
  const ctrl = new EnapLightController();
  beforeEach(() => {
    initLocalstorageMock();
  });

  it('Non-Empty Storage', () => {
    const mockId1 = ctrl.createNewRegelungsentwurf('Draft1');
    ctrl.deleteDraft(mockId1);
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: EnapCollectionInterface = JSON.parse(draftCollectionString) as EnapCollectionInterface;
    expect(Object.keys(draftCollection)).to.have.lengthOf(0);
  });
});

describe('Test Load Regelungsentwurf', () => {
  const ctrl = new EnapLightController();
  beforeEach(() => {
    initLocalstorageMock();
  });

  it('Add to Storage', () => {
    const mockId1 = ctrl.createNewRegelungsentwurf('Draft1');
    const mock1: EnapCollectionItemInterface | null = ctrl.getDraftById(mockId1);
    const mockId2 = uuidv4();
    const mockName2 = 'Draft 2';
    if (mock1 != null) {
      const mock2: EnapCollectionItemInterface = copyDraft(mock1, mockId2, mockName2);
      ctrl.loadDraft(mock2);
    }
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '';
    const draftCollection: EnapCollectionItemInterface = JSON.parse(
      draftCollectionString,
    ) as EnapCollectionItemInterface;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
    expect(ctrl.getDraftById(mockId2)).to.have.property('name', mockName2);
  });
});

const copyDraft = (
  sourceObj: EnapCollectionItemInterface,
  newId: string,
  newName: string,
): EnapCollectionItemInterface => {
  return Object.assign({}, sourceObj, { id: newId, name: newName });
};
