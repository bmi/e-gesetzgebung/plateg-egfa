// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ButtonType } from 'antd/lib/button';
import { FormInstance } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, RouteComponentProps, Switch, useRouteMatch } from 'react-router-dom';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { EgfaModuleEnapEntityDTO, EgfaModuleEnapEntityResponseDTO, EgfaModuleStatusType } from '@plateg/rest-api';
import { InfoComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares/injector';

import { routes } from '../../../../shares/routes';
import { exportEnap } from '../../../modules/enap/controller';
import { PrinzipienComponent } from '../../../modules/enap/prinzipien/component.react';
import { SDG10UngleichheitenComponent } from '../../../modules/enap/SDG10Ungleichheiten/component.react';
import { SDG11NachhaltigeStaedteGemeindenComponent } from '../../../modules/enap/SDG11NachhaltigeStaedteGemeinden/component.react';
import { SDG12NachhaltigKonsumProduktionComponent } from '../../../modules/enap/SDG12NachhaltigKonsumProduktion/component.react';
import { SDG13KlimaschutzComponent } from '../../../modules/enap/SDG13Klimaschutz/component.react';
import { SDG14LebenUnterWasserComponent } from '../../../modules/enap/SDG14LebenUnterWasser/component.react';
import { SDG15LebenAnLandComponent } from '../../../modules/enap/SDG15LebenAnLand/component.react';
import { SDG16FriedenGerechtigkeitComponent } from '../../../modules/enap/SDG16FriedenGerechtigkeit/component.react';
import { SDG17PartnerschaftenZurErreichungDerZieleComponent } from '../../../modules/enap/SDG17PartnerschaftenZurErreichungDerZiele/component.react';
import { SDG1KeineArmutComponent } from '../../../modules/enap/SDG1KeineArmut/component.react';
import { SDG2KeinHungerComponent } from '../../../modules/enap/SDG2KeinHunger/component.react';
import { SDG3GesundheitComponent } from '../../../modules/enap/SDG3Gesundheit/component.react';
import { SDG4HochwertigeBildungComponent } from '../../../modules/enap/SDG4HochwertigeBildung/component.react';
import { SDG5GeschlechtergleichstellungComponent } from '../../../modules/enap/SDG5Geschlechtergleichstellung/component.react';
import { SDG6SauberesWasserComponent } from '../../../modules/enap/SDG6SauberesWasser/component.react';
import { SDG7EnergieComponent } from '../../../modules/enap/SDG7Energie/component.react';
import { SDG8ArbeitWirtschaftComponent } from '../../../modules/enap/SDG8ArbeitWirtschaft/component.react';
import { SDG9IndustrieComponent } from '../../../modules/enap/SDG9Industrie/component.react';
import { EnapZusammenfassung } from '../../../modules/enap/zusammenfassung/component.react';
import { SimpleModuleZusammenfassung } from '../../../modules/general/simple-module/zusammenfassung/component.react';
import { ModuleWrapper } from '../../../modules/module-wrapper/component.react';
import { EnapLightController } from '../controller';

export function EnapLightSteps(): React.ReactElement {
  const { t } = useTranslation();

  const [formInstance, setFormInstance] = useState<FormInstance>();
  const [enapData, setEnapData] = useState<EgfaModuleEnapEntityDTO>();
  const enapLightCtrl = GlobalDI.getOrRegister('enapLightController', () => new EnapLightController());
  const [currentPageName, setCurrentPageName] = useState<string>();
  const [pruefenName, setPruefenName] = useState<string>();
  const routeMatcherVorbereitung = useRouteMatch<{ id: string; moduleName: string; pageName: string }>(
    `/egfa/:id/${routes.MODULE}/:moduleName/:pageName`,
  );
  const pageName = routeMatcherVorbereitung?.params?.pageName;
  const egfaId = routeMatcherVorbereitung?.params?.id;
  const moduleName = routeMatcherVorbereitung?.params?.moduleName;

  useEffect(() => {
    setCurrentPageName(pageName);
  }, [routeMatcherVorbereitung?.params?.pageName]);
  useEffect(() => {
    setPruefenName(enapLightCtrl.getDraftById(egfaId as string)?.name);
  }, []);
  const getData = () => {
    return of(enapLightCtrl.getDraftById(egfaId as string)).pipe(delay(1));
  };
  const putData = (localData: EgfaModuleEnapEntityDTO) => {
    return of(enapLightCtrl.saveDraftContent(localData)).pipe(delay(1));
  };

  const getExportEnapAction = (type: ButtonType) => (
    <Button type={type} onClick={() => exportEnap(enapData)}>
      {t(`egfa.modules.enap.fertigstellung.exportBtn`)}
    </Button>
  );

  const PageRoutingCompLight = (
    onSave: (form?: FormInstance, status?: EgfaModuleStatusType, isDraftSaving?: boolean) => () => void,
    updateItemDisableStatus?: (itemName: string | string[], isDisabled: boolean) => void,
    handleFormChange?: () => void,
  ): React.ReactElement => {
    useEffect(() => {
      if (updateItemDisableStatus) {
        const fertigstellungIsDisabled = !enapData?.influenceSummary || enapData?.influenceSummary === '';
        updateItemDisableStatus([routes.FERTIGSTELLUNG], fertigstellungIsDisabled);
      }
    }, [enapData]);

    const contentPageProps = { egfaId, setFormInstance, formData: enapData, onSave, handleFormChange };
    return (
      <Switch>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG1}`]}>
          <SDG1KeineArmutComponent
            {...contentPageProps}
            sectionName={routes.SDG1}
            nextLinkName={routes.SDG2}
            previousLinkName={routes.EINLEITUNG}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG2}`]}>
          <SDG2KeinHungerComponent
            {...contentPageProps}
            sectionName={routes.SDG2}
            nextLinkName={routes.SDG3}
            previousLinkName={routes.SDG1}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG3}`]}>
          <SDG3GesundheitComponent
            {...contentPageProps}
            sectionName={routes.SDG3}
            nextLinkName={routes.SDG4}
            previousLinkName={routes.SDG2}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG4}`]}>
          <SDG4HochwertigeBildungComponent
            {...contentPageProps}
            sectionName={routes.SDG4}
            nextLinkName={routes.SDG5}
            previousLinkName={routes.SDG3}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG5}`]}>
          <SDG5GeschlechtergleichstellungComponent
            {...contentPageProps}
            sectionName={routes.SDG5}
            nextLinkName={routes.SDG6}
            previousLinkName={routes.SDG4}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG6}`]}>
          <SDG6SauberesWasserComponent
            {...contentPageProps}
            sectionName={routes.SDG6}
            nextLinkName={routes.SDG7}
            previousLinkName={routes.SDG5}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG7}`]}>
          <SDG7EnergieComponent
            {...contentPageProps}
            sectionName={routes.SDG7}
            nextLinkName={routes.SDG8}
            previousLinkName={routes.SDG6}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG8}`]}>
          <SDG8ArbeitWirtschaftComponent
            {...contentPageProps}
            sectionName={routes.SDG8}
            nextLinkName={routes.SDG9}
            previousLinkName={routes.SDG7}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG9}`]}>
          <SDG9IndustrieComponent
            {...contentPageProps}
            sectionName={routes.SDG9}
            nextLinkName={routes.SDG10}
            previousLinkName={routes.SDG8}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG10}`]}>
          <SDG10UngleichheitenComponent
            {...contentPageProps}
            sectionName={routes.SDG10}
            nextLinkName={routes.SDG11}
            previousLinkName={routes.SDG9}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG11}`]}>
          <SDG11NachhaltigeStaedteGemeindenComponent
            {...contentPageProps}
            sectionName={routes.SDG11}
            nextLinkName={routes.SDG12}
            previousLinkName={routes.SDG10}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG12}`]}>
          <SDG12NachhaltigKonsumProduktionComponent
            {...contentPageProps}
            sectionName={routes.SDG12}
            nextLinkName={routes.SDG13}
            previousLinkName={routes.SDG11}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG13}`]}>
          <SDG13KlimaschutzComponent
            {...contentPageProps}
            sectionName={routes.SDG13}
            nextLinkName={routes.SDG14}
            previousLinkName={routes.SDG12}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG14}`]}>
          <SDG14LebenUnterWasserComponent
            {...contentPageProps}
            sectionName={routes.SDG14}
            nextLinkName={routes.SDG15}
            previousLinkName={routes.SDG13}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG15}`]}>
          <SDG15LebenAnLandComponent
            {...contentPageProps}
            sectionName={routes.SDG15}
            nextLinkName={routes.SDG16}
            previousLinkName={routes.SDG14}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG16}`]}>
          <SDG16FriedenGerechtigkeitComponent
            {...contentPageProps}
            sectionName={routes.SDG16}
            nextLinkName={routes.SDG17}
            previousLinkName={routes.SDG15}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.SDG17}`]}>
          <SDG17PartnerschaftenZurErreichungDerZieleComponent
            {...contentPageProps}
            sectionName={routes.SDG17}
            nextLinkName={routes.prinzipien}
            previousLinkName={routes.SDG16}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.prinzipien}`]}>
          <PrinzipienComponent
            {...contentPageProps}
            sectionName={routes.prinzipien}
            nextLinkName={routes.ZUSAMMENFASSUNG}
            previousLinkName={routes.SDG17}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.ZUSAMMENFASSUNG}`]}>
          <EnapZusammenfassung
            {...contentPageProps}
            sectionName={routes.ZUSAMMENFASSUNG}
            nextLinkName={routes.FERTIGSTELLUNG}
            previousLinkName={routes.prinzipien}
            updateNextLink={updateItemDisableStatus}
            isLight={true}
          />
        </Route>
        <Route exact path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}/${routes.FERTIGSTELLUNG}`]}>
          <SimpleModuleZusammenfassung
            {...contentPageProps}
            moduleName="enap"
            additionalActions={[getExportEnapAction('primary')]}
            isLight={true}
          />
        </Route>

        <Route
          path={[`/egfa/:id/${routes.MODULE}/${routes.ENAP}`]}
          render={({ match }: RouteComponentProps<{ id: string }>) => <Redirect to={`/egfa`} />}
        />
      </Switch>
    );
  };

  const contactDrawer = (
    <InfoComponent
      isContactPerson={false}
      title={t('egfa.modules.enap.drawerTitle')}
      buttonText={t('egfa.modules.general.contactPerson')}
      key="egfa-header-contact-person"
      titleWithoutPrefix={true}
    >
      <div
        dangerouslySetInnerHTML={{
          __html: t(`egfa.modules.enap.contactPerson.content`),
        }}
      />
    </InfoComponent>
  );

  return (
    <ModuleWrapper<EgfaModuleEnapEntityDTO, EgfaModuleEnapEntityResponseDTO>
      setFormData={setEnapData}
      getData={getData}
      putData={putData}
      moduleIdentifier={moduleName}
      egfaId={egfaId}
      form={formInstance}
      pageRouting={PageRoutingCompLight}
      pageName={currentPageName}
      customContactDrawer={contactDrawer}
      customResultPage="fertigstellung"
      isLight={true}
      pruefenName={pruefenName}
    />
  );
}
