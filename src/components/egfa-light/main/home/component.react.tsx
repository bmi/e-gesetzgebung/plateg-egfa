// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './home-light.less';

import { Layout } from 'antd';
import React from 'react';

import { GlobalDI, HeaderComponent, HeaderController } from '@plateg/theme';

import { EinleitungEnap } from '../../../modules/enap/einleitung/component.react';

const { Header, Content } = Layout;

export function HomeLightComponent(): React.ReactElement {
  const headerController = GlobalDI.getOrRegister('headerController', () => new HeaderController());
  return (
    <Layout style={{ height: '100%', display: 'flex' }} className="site-layout-background enap-light-home">
      <HeaderComponent ctrl={headerController} />

      <Content className="ant-layout main-content-area has-drawer">
        <div className="ant-layout-content">
          <div className="enap-light-holder">
            <EinleitungEnap isLight={true} />
          </div>
        </div>
      </Content>
    </Layout>
  );
}
