// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { v4 as uuidv4 } from 'uuid';

import { EgfaModuleEnapEntityDTO, EgfaModuleStatusType, EgfaModuleType } from '@plateg/rest-api';
export const collectionKey = 'enapCollection';

export interface EnapCollectionInterface {
  [key: string]: EnapCollectionItemInterface;
}
export interface EnapCollectionItemInterface {
  dto: EgfaModuleEnapEntityDTO;
  name: string;
  lastmodified: string;
  id: string;
}

export class EnapLightController {
  private readonly patternItem = {
    egfaId: '',
    typ: EgfaModuleType.Enap,
    influenceExpected: false,
    deleted: false,
    status: EgfaModuleStatusType.InBearbeitung,
    allowedToModify: true,
    sdg1: null,
    sdg2: null,
    sdg3: null,
    sdg4: null,
    sdg5: null,
    sdg6: null,
    sdg7: null,
    sdg8: null,
    sdg9: null,
    sdg10: null,
    sdg11: null,
    sdg12: null,
    sdg13: null,
    sdg14: null,
    sdg15: null,
    sdg16: null,
    sdg17: null,
    prinzipien: null,
    sdgs: [],
  };
  private _getDraftCollection(): EnapCollectionInterface {
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '{}';
    return JSON.parse(draftCollectionString) as EnapCollectionInterface;
  }

  private _setDraftCollection(draftCollection: EnapCollectionInterface): void {
    global.window.localStorage.setItem(collectionKey, JSON.stringify(draftCollection));
  }

  public createNewRegelungsentwurf(draftName: string): string {
    const newId = uuidv4();
    const draftCollection: EnapCollectionInterface = this._getDraftCollection();
    draftCollection[newId] = {
      name: draftName,
      lastmodified: new Date().toISOString(),
      id: newId,
      dto: { ...this.patternItem, egfaId: newId, title: draftName },
    };
    this._setDraftCollection(draftCollection);
    return newId;
  }

  public getDraftList(): EnapCollectionItemInterface[] {
    const draftCollection: EnapCollectionInterface = this._getDraftCollection();
    const draftList: EnapCollectionItemInterface[] = [];
    Object.keys(draftCollection).forEach((key) => {
      const tmpRegelungsentwurf: EnapCollectionItemInterface = draftCollection[key];
      tmpRegelungsentwurf['id'] = key;
      draftList.push(tmpRegelungsentwurf);
    });
    return draftList;
  }

  public deleteDraft(id: string): void {
    const draftCollection: EnapCollectionInterface = this._getDraftCollection();
    delete draftCollection[id];
    this._setDraftCollection(draftCollection);
  }

  public loadDraft(draft: EnapCollectionItemInterface): void {
    const draftCollection: EnapCollectionInterface = this._getDraftCollection();
    draftCollection[draft.id] = draft;
    this._setDraftCollection(draftCollection);
  }

  public getDraftById(id: string): EnapCollectionItemInterface | null {
    return this._getDraftCollection()[id];
  }

  public saveDraftContent(values: EgfaModuleEnapEntityDTO): void {
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '{}';
    const draftCollection: EnapCollectionInterface = JSON.parse(draftCollectionString) as EnapCollectionInterface;
    const draftID = values.egfaId as string;
    let currentDraft: EnapCollectionItemInterface = draftCollection[draftID];
    currentDraft = { ...currentDraft, dto: values, lastmodified: new Date().toISOString() };

    draftCollection[draftID] = currentDraft;

    global.window.localStorage.setItem(collectionKey, JSON.stringify(draftCollection));
  }

  public saveDraft(data: string, routeChangeInitiate: (url: string) => void): void {
    const draft = JSON.parse(data) as EnapCollectionItemInterface;
    this.loadDraft(draft);
    routeChangeInitiate(`/egfa/${draft.id}`);
  }
}
