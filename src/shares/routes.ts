// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { EgfaModuleType } from '@plateg/rest-api';

export const routes: { [key: string]: string } = {
  NEUE_GESETZESFOLGENABSCHAETZUNG: 'neueGesetzesfolgenabschaetzung',
  REGELUNGSVORHABEN: 'regelungsvorhaben',
  HILFE: 'hilfe',
  MODUL_UEBERSICHT: 'modulUebersicht',
  ERGEBNISDOKUMENTATION: 'ergebnisdokumentation',

  MEINE_EGFAS: 'meineGesetzesfolgeabschaetzungen',
  ARCHIV: 'archiv',

  MODULE: 'module',
  AUSWIRKUNGEN_VERBRAUCHER: EgfaModuleType.Verbraucher.toLocaleLowerCase(),
  AUSWIRKUNGEN_WEITERE: EgfaModuleType.Weitere.toLocaleLowerCase(),
  AUSWIRKUNGEN_PREISE: EgfaModuleType.Preise.toLocaleLowerCase(),
  AUSWIRKUNGEN_ENAP: EgfaModuleType.Enap.toLocaleLowerCase(),
  AUSWIRKUNGEN_DISABILITY: EgfaModuleType.Disability.toLocaleLowerCase(),
  AUSWIRKUNGEN_GLEICHSTELLUNG: EgfaModuleType.Gleichstellung.toLocaleLowerCase(),
  AUSWIRKUNGEN_SONSTIGEKOSTEN: 'sonstigeKosten',
  MITTELSTAENDISCHE_UNTERNEHMEN: EgfaModuleType.Kmu.toLocaleLowerCase(),
  GLEICHWERTIGKEITS_CHECK: EgfaModuleType.Gleichwertigkeit.toLocaleLowerCase(),
  AUSWIRKUNGEN_EINNAHMENAUSGABEN: 'einnahmenAusgaben',
  AUSWIRKUNGEN_EINNAHMENAUSGABEN_2: 'EA_OEHH',
  AUSWIRKUNGEN_EXPERIMENTIERKLAUSELN: 'experimentierklauseln',
  DEMOGRAFIE_CHECK: EgfaModuleType.Demografie.toLocaleLowerCase(),
  GLEICHSTELLUNG: EgfaModuleType.Gleichstellung.toLocaleLowerCase(),
  DISABILITY: EgfaModuleType.Disability.toLocaleLowerCase(),
  ENAP: EgfaModuleType.Enap.toLocaleLowerCase(),
  ERFUELLUNGSAUFWAND: EgfaModuleType.Erfuellungsaufwand.toLocaleLowerCase(),
  EVALUIERUNG: EgfaModuleType.Evaluierung.toLocaleLowerCase(),
  EXPERIMENTIERKLAUSELN: 'experimentierklauseln',
  PRUEFUNG_ERFORDERLICHKEIT: 'pruefungErforderlichkeit',
  AUSGESTALTUNG_DER_EVALUIERUNG: 'ausgestaltungDerEvaluierung',
  WAS: 'was',
  WIE: 'wie',
  WANN: 'wann',
  SCHWELLENWERTE: 'schwellenWerte',
  WEITERE_EVALUIERUNGSANLAESSE: 'weitereEvaluierungsanlaesse',
  GRUENDE_VERZICHT: 'gruendeFuerVerzicht',
  SONSTIGE_KOSTEN: 'sonstigeKosten',

  WESENTLICHE_AUSWIRKUNGEN: 'wesentlicheAuswirkungen',
  ZUSAMMENFASSUNG: 'zusammenfassung',
  FERTIGSTELLUNG: 'fertigstellung',
  EINLEITUNG: 'einleitung',
  DEMOGRAFIE_EINLEITUNG: 'einleitung',
  DEMOGRAFIE_FRAGENKATALOG: 'fragenkatalog',
  DEMOGRAFIE_DEMO_ENTWICKLUNG: 'demografischeEntwicklung',
  DEMOGRAFIE_FAMILIE: 'familieKinderJugendliche',
  DEMOGRAFIE_ARBEIT: 'arbeitBerufWirtschaft',
  DEMOGRAFIE_ALTER: 'alterWohnenPflege',
  DEMOGRAFIE_REGIONEN: 'regionen',
  DEMOGRAFIE_GENERATIONEN: 'generationen',
  RELEVANZPRUEFUNG: 'relevanzpruefung',
  RELEVANZPRUEFUNG_1: 'relevanzpruefung1',
  RELEVANZPRUEFUNG_2: 'relevanzpruefung2',
  HAUPTPRUEFUNG: 'hauptpruefung',
  FOLGENABSCHAETZUNG: 'folgenabschaetzung',
  BESONDERE_BELASTUNGEN: 'besondereBelastungen',
  REGELUNGSALTERNATIVEN: 'regelungsalternativen',
  DARSTELLUNG_AUSWIRKUNGEN: 'darstellungAuswirkungen',
  BESCHREIBUNG: 'beschreibung',
  ARBEIT: 'arbeit',
  GELD: 'geld',
  WISSEN: 'wissen',
  ZEIT: 'zeit',
  ENTSCHEIDUNGSMACHT: 'entscheidungsmacht',
  GESUNDHEIT: 'gesundheit',
  GEWALT: 'gewalt',
  AUSWIRKUNGEN: 'auswirkungen',
  GESCHLECHTERGERECHTIGKEIT: 'geschlechtergerechtigkeit',
  GLEICHSTELLUNG_ZUSAMMENFASSUNG: 'gleichstellungZusammenfassung',
  GESAMTSUMMEN_NORMADRESSATEN: 'gesamtsummenNormadressaten',
  SONSTIGE_KOSTEN_PRUEFUNG: 'pruefungSonstigeKosten',

  BUERGER: 'buerger',
  WIRTSCHAFT: 'wirtschaft',
  VERWALTUNG: 'verwaltung',
  ALGEMEINEANGABEN: 'algemeineangaben',
  BEGRUENDUNGSTEIL: 'begruendungsteil',

  FAKTOR_FINANZSITUATION: 'faktorFinanzsituation',
  FAKTOR_WIRTSCHAFT: 'faktorWirtschaft',
  FAKTOR_MOBILITAET: 'faktorMobilitaet',
  FAKTOR_DASEINSVORSORGE: 'faktorDaseinsvorsorge',
  FAKTOR_ENGAGEMENT: 'faktorEngagement',
  FAKTOR_WOHNRAUM: 'faktorWohnraum',
  FAKTOR_NATUR: 'faktorNatur',

  ABSCHLUSS_ERFOLGREICH: 'abschlussErfolgreich',
  ABSCHLUSS_FEHLER: 'abschlussFehler',

  SDG1: 'sdg1',
  SDG2: 'sdg2',
  SDG3: 'sdg3',
  SDG4: 'sdg4',
  SDG5: 'sdg5',
  SDG6: 'sdg6',
  SDG7: 'sdg7',
  SDG8: 'sdg8',
  SDG9: 'sdg9',
  SDG10: 'sdg10',
  SDG11: 'sdg11',
  SDG12: 'sdg12',
  SDG13: 'sdg13',
  SDG14: 'sdg14',
  SDG15: 'sdg15',
  SDG16: 'sdg16',
  SDG17: 'sdg17',
  prinzipien: 'prinzipien',

  ZWECKAUSGABEN: 'zweckausgaben',
  VOLLZUGSAUFWAND: 'vollzugsaufwand',
  NORMADRESSAT: 'normadressat',
  VERWALTUNGSUNDSONSTIGE: 'verwaltungsUndSonstige',
  AENDERUNGENSTEUEREINNAHMEN: 'aenderungenSteuereinnahmen',
  SONSTIGERBEREICHESTAATES: 'sonstigerBereicheStaates',
  KOMPENSATIONKOSTEN: 'kompensationKosten',

  INTRO: 'intro',
  STANDORT_UND_TITEL: 'standortUndTitel',
  ABSATZ1: 'absatz1',
  ERPROBUNGSZWECK: 'erprobungszweck',
  ABSATZ2: 'absatz2',
  ZUSTAENDIGKEIT: 'zustaendigkeit',
  ERMAECHTIGUNGBEHOERDE: 'ermaechtigungBehoerde',
  ENTSCHEIDUNGSINHALT: 'entscheidungsinhalt',
  ERPROBUNGSGEGENSTAND: 'erprobungsgegenstand',
  MATERIELLEBEGRENZUNG: 'materielleBegrenzung',
  VERFAHRENSVORGABEN: 'verfahrensvorgaben',
  UMFANGERPROBUNG: 'umfangErprobung',
  BEGLEITENDEPFLICHTEN: 'begleitendePflichten',
  BEFRISTUNGERPROBUNG: 'befristungErprobung',
  VERLAENGERUNGSMOEGLICHKEIT: 'verlaengerungsmoeglichkeit',
  VERHAELTNISAUFSCHIEBENDEWIRKUNG: 'verhaeltnisAufschiebendeWirkung',
  WEITERENEBENBESTIMMUNGEN: 'weitereNebenbestimmungen',
  AENDERUNGREALLABORGESTALTUNG: 'aenderungReallaborgestaltung',
  MOEGLICHKEITAUFHEBUNG: 'moeglichkeitAufhebung',
  ABSATZZWEIZUSAMMENFASSUNG: 'absatzZweiZusammenfassung',
  ABSATZ3: 'absatz3',
  EVALUATIONUNDTRANSFER: 'evaluationUndTransfer',
  BEFRISTUNGEXPERIMENTIERKLAUSEL: 'befristungExperimentierklausel',
  ABSATZDREIZUSAMMENFASSUNG: 'absatzDreiZusammenfassung',
  ABSATZ4: 'absatz4',
  VERORDNUNGSERMAECHTIGUNG: 'verordnungsermaechtigung',
};
