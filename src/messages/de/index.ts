// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_demografie } from './modules/demografie';
import { de_disability } from './modules/disability';
import { de_einnahmenAusgaben } from './modules/einnahmenAusgaben';
import { de_enap } from './modules/enap';
import { de_erfuellungsaufwand } from './modules/erfuellungsaufwand';
import { de_evaluierung } from './modules/evaluierung';
import { de_experimentierklauseln } from './modules/experimentierklauseln';
import { de_gleichstellung } from './modules/gleichstellung';
import { de_gleichwertigkeit } from './modules/gleichwertigkeit';
import { de_kmu } from './modules/kmu';
import { de_preise } from './modules/preise';
import { de_sonstige } from './modules/sonstige';
import { de_verbraucher } from './modules/verbraucher';
import { de_weitere } from './modules/weitere';

export const de = {
  langCode: 'de-DE',
  egfa: {
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    textLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
    header: {
      linkBack: 'Zurück',
      linkHelp: 'Hilfe',
      linkHome: 'Gesetzesfolgenabschätzung - Gesetzesfolgenabschätzungen',
      linkHomeLight: 'Startseite Nachhaltigkeitsprüfung',
      breadcrumbs: {
        modulUebersicht: 'Gesetzesfolgenabschätzung',
        ergebnisdokumentation: 'Ergebnisdokumentation',
        archived: 'Archiviert: ',
        allModules: 'Gesetzesfolgenabschätzung - Alle GFA-Module',
        projectName: 'Gesetzesfolgenabschätzung',
      },
      btnContinueLater: {
        title: 'Bearbeitung später fortsetzen?',
        btnText: 'Zwischenspeichern',
        drawerText:
          'Speichern Sie Ihren Zwischenstand, um das Modul zu einem späteren Zeitpunkt weiter zu bearbeiten. Sie können es im Reiter „Modulübersicht“ erneut aufrufen.',
      },
    },
    help: {
      drawerTitle: 'Gesetzesfolgenabschätzung',
      drawerText:
        '<p>Die Gesetzesfolgenabschätzung unterstützt Sie bei der Abschätzung und Formulierung der Auswirkungen eines Regelungsvorhabens nach den <a href={{linkGGO_43}} target="_blank"> §§ 43</a> und <a href={{linkGGO_44}} target="_blank">44 GGO</a>. Die Ergebnisse können Sie für die Darstellung im Vorblatt und in der Begründung des Regelungsentwurfs nutzen.</p>' +
        '<p>In der Gesetzesfolgenabschätzung gibt es mehrere Module, die die verschiedenen Prüfanforderungen zur Abschätzung der Regelungsfolgen abbilden.</p>' +
        '<p>Um mit der Bearbeitung zu beginnen, legen Sie für ein Regelungsvorhaben eine Gesetzesfolgenabschätzung an und öffnen diese anschließend durch Klick in der Übersicht.</p>' +
        '<p>Die Modulübersicht ist dann Ihr zentraler Navigationspunkt, von dem aus Sie in die verschiedenen Module springen können. Sie zeigt Ihnen auch den Bearbeitungsstand der Module und bietet eine Ergebnisübersicht Ihrer Eingaben an.</p>' +
        '<p>Die weiteren Module der Gesetzesfolgenabschätzung werden nach und nach hinzugefügt und sind dann ebenfalls hier zu finden.</p>',
    },
    home: {
      btnNew: 'Neue Gesetzesfolgenabschätzung anlegen',
      mainTitle: 'Gesetzesfolgenabschätzung',
      gesetzesfolgenabschatzung: {
        meineGesetzesfolgeabschaetzungen: {
          title: 'Gesetzesfolgenabschätzungen',
          emptyContentLabel1: 'Neue Folgenabschätzung anlegen',
          emptyContentLabel2: 'Folgenabschätzung mit Hilfe von 13 Modulen gemeinsam erarbeiten und abstimmen',
          emptyContentLabel3: 'Ergebnisübersicht ermöglicht die Übernahme in den Regelungsentwurf',
          emptyContentText:
            'Diese Anwendung unterstützt Sie dabei, die Gesetzesfolgenabschätzung für Regelungsvorhaben entsprechend den Vorgaben in der GGO vorzunehmen und darzustellen.',
          emptyContentText2:
            'Sie können die Ergebnisse kopieren und in das Vorblatt und die Begründung Ihres Regelungsentwurfs übernehmen.',
          table: {
            regelungsvorhaben: 'Regelungsvorhaben',
            gfa: 'Gesetzesfolgenabschätzung',
            erstellt: 'Erstellt',
            ersteller: 'Erstellt durch',
            bearbeiter: 'Zuletzt bearbeitet',
            thead5: 'Module fertig',
            aktionen: 'Aktionen',
            menuItem1: 'Duplizieren',
            menuItem2: 'Löschen',
            menuItem3: 'PLACEHOLDER',
            erstelltAsc: 'Erstellt (älteste zuerst)',
            erstelltDesc: 'Erstellt (neueste zuerst)',
            sorterTitle: 'Sortieren nach',
            filter: {
              displayname: {
                regelungsvorhaben: 'Regelungsvorhaben',
                regelungsvorhabenType: 'Abstimmungsarten',
                ersteller: 'Erstellerin oder Ersteller',
              },
              labelContent: 'Filtern nach',
            },
            open: 'Öffnen',
            renameEgfaModal: {
              menuItemTitle: 'Umbenennen',
              title: 'Gesetzesfolgenabschätzung umbenennen',
              inputTitle: 'Neuen Namen eingeben',
              cancel: 'Abbrechen',
              ok: 'Neuen Namen speichern',
              success: 'Sie haben die Gesetzesfolgenabschätzung erfolgreich in „{{name}}“ umbenannt.',
              requiredMsg: 'Bitte geben sie den neuen Namen an',
            },
            archiveEgfaModal: {
              content:
                'Wenn Sie diese Gesetzesfolgenabschätzung archivieren, können Sie sie nur noch im Lesezugriff öffnen. Änderungen sind dann nicht mehr möglich.',
              okText: 'Ja, archivieren',
              cancelText: 'Nein, nicht archivieren',
              archivedSuccess: 'Sie haben die Gesetzesfolgenabschätzung {{title}} archiviert.',
            },
            saveAsPdf: 'Zusammenfassung als PDF erstellen',
            archive: 'Archivieren',
          },
        },
        archiv: {
          title: 'Archiv',
          emptyContentLabel1: 'Alle Vorgänge zu einer Gesetzesfolgenabschätzung archivieren',
          emptyContentLabel2: 'Lese-Zugriff auf alle Gesetzesfolgenabschätzungen bleibt erhalten',
          emptyContentText:
            'Gesetzesfolgenabschätzungen, die Sie nicht mehr bearbeiten möchten, können Sie in das Archiv sortieren. Sie und alle weiteren Nutzenden behalten weiterhin Lese-Zugriff. Eine Archivierung kann nicht rückgängig gemacht werden.',
          table: {
            archiviertAm: 'Archiviert',
            archiviertAmAsc: 'Archiviert (älteste zuerst)',
            archiviertAmDesc: 'Archiviert (neueste zuerst)',
          },
        },
        typeOfVote: {
          label: 'Art der Abstimmung',
          typeHomeVote: 'Hausabstimmung',
          typeDepartmentVote: 'Ressortabstimmung',
          error: 'Bitte geben Sie die Art der Abstimmung an.',
        },
      },
    },
    neueGesetzesfolgenabschaetzung: {
      mainTitle: 'Neue Gesetzesfolgenabschätzung anlegen',
      instruction:
        'Bitte wählen Sie ein Regelungsvorhaben aus, zu dem Sie eine Gesetzesfolgenabschätzung beginnen möchten:',
      emptyRvList:
        'Bisher sind noch keine Regelungsvorhaben mit Ihrem Account verknüpft.<br />' +
        'Sie können <a href="/cockpit/#/regelungsvorhaben">hier</a> ein neues Regelungsvorhaben anlegen.',
      list: {
        typeLabel: 'Vorhabentyp:',
        ressortLabel: 'Federführendes Ressort:',
        createdLabel: 'Erstellt von',
      },
      formError: 'Bitte wählen Sie ein Regelungsvorhaben aus',
      btnNext: 'Nächster Schritt',
      btnPrev: 'Vorheriger Schritt',
      btnCancel: 'Abbrechen',
      successMsg: 'Sie haben die Gesetzesfolgenabschätzung „{{title}}“ angelegt.',
      errorMsg: 'Es gab einen Fehler beim Anlegen Ihrer neuen Gesetzesfolgenabschätzung. {{fehlercode}}',
      requiredInfo: 'Pflichtfelder sind mit einem * gekennzeichnet.',
      textLength: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
      page1: {
        title: 'Schritt 1 von 2: Regelungsvorhaben auswählen',
        item1: {
          info: {
            title: 'Erklärung zu: Regelungsvorhaben',
            text: 'Wählen Sie das Regelungsvorhaben, zu dem Sie eine Gesetzesfolgenabschätzung anlegen möchten. Für jeden Regelungsentwurf kann nur eine Gesetzesfolgenabschätzung angelegt werden. Die Gesetzesfolgenabschätzung besteht aus mehreren Modulen. Sie können einzelne Module der Gesetzesfolgenabschätzung an Kolleginnen und Kollegen zur Bearbeitung weiterleiten.',
          },
          label: 'Regelungsvorhaben',
          placeholder: 'Bitte auswählen',
          requiredMsg: 'Bitte wählen Sie ein Regelungsvorhaben aus',
        },
        item2: {
          label: 'Regelungsentwurf (Editor-Dokumentenmappe)',
          placeholder: 'Bitte auswählen',
          requiredMsg: 'Bitte wählen Sie ein Editor-Dokumentenmappe aus',
        },
      },
      page2: {
        title: 'Schritt 2 von 2: Titel der Gesetzesfolgenabschätzung vergeben',
        item1: {
          info: {
            title: 'Erklärung zu: Titel der Gesetzesfolgenabschätzung',
            text: 'Dieser Titel ist ein systemseitiger Vorschlag. Eine Änderung an dieser Stelle wirkt sich auch auf alle anderen Orte aus, an denen der Titel der Gesetzesfolgenabschätzung verwendet wird.',
          },
          label: 'Titel der Gesetzesfolgenabschätzung',
          placeholder: 'Bitte ausfüllen',
          requiredMsg: 'Bitte geben Sie einen Titel an',
        },
        btnNext: 'Eingaben prüfen',
      },
      page3: {
        title: 'Eingaben prüfen',
        definitionTitles: {
          rv: 'Dazugehöriges Regelungsvorhaben:',
          titel: 'Titel der Gesetzesfolgenabschätzung:',
        },
        btnNext: 'Gesetzesfolgenabschätzung jetzt anlegen',
      },
    },
    ergebnisdokumentation: {
      mainTitle: 'Ergebnisdokumentation',
      instruction:
        'Übertragen Sie Ihre Prüfergebnisse in die Begründung, in das Vorblatt beziehungsweise in den Regelungstext Ihrer Dokumentenmappe. Die Übertragung in Vorblatt und Begründung ist direkt in den Einzeldokumenten im Editor möglich. Für eine Übertragung in den Regelungstext (z. B. für eine Experimentierklausel) können Sie den Text händisch an die passende Stelle kopieren.',
    },
    viewEGFA: {
      tabs: {
        moduleOverview: 'Modulübersicht',
        problemZielanalyse: 'Problem- und Zielanalyse',
      },
      notice: {
        title: 'Hinweis',
        text: `Gem. § 62 Abs. 2 S. 2 GGO kann in der Begründung zu einer Rechtsverordnung auf Ausführungen nach den §§
                43 und 44 GGO verzichtet werden, soweit solche Ausführungen in der Begründung eines ermächtigenden
                Gesetzes oder einer vorangegangenen Rechtsverordnung enthalten sind und die Begründung darauf verweist.`,
      },
      table: {
        freigeben: 'Berechtigungen',
        anEditorSenden: 'Ergebnis in Editordokument übertragen',
        freigabe: {
          success: 'Sie haben die Berechtigungen für die Gesetzesfolgenabschätzung „{{title}}“ aktualisiert.',
          error: 'Die Anzeige der Berechtigungen ist gerade nicht möglich. Bitte versuchen Sie es später noch einmal.',
        },
        auswahl: 'Auswahl',
        module: 'Module',
        status: 'Status',
        bearbeitetAm: 'Zuletzt bearbeitet',
        zugriffsrecht: 'Meine Zugriffsrechte',
        zugriffsrechtInfo: {
          title: 'Meine Zugriffsrechte',
          drawerText: `<h2>Mögliche Zugriffsrechte</h2>
          <ul>
            <li>„Federführung“: Als Federführung des Regelungsvorhabens können Sie das Modul öffnen und die jeweilige Prüfung durchführen. Sie können das Modul fertig stellen und damit die Prüfung abschließen.</li>
            <li>„Mitarbeit”:  Sie können das Modul öffnen und die jeweilige Prüfung durchführen. Sie können das Modul fertig stellen und damit die Prüfung abschließen. Die Rolle “Mitarbeit” des Regelungsvorhabens beinhaltet die Mitarbeit an allen Modulen zugehöriger Gesetzesfolgenabschätzungen. Zudem können Sie die Mitarbeit an einzelnen Modulen der Gesetzesfolgenabschätzung an Personen vergeben, die die Rolle “Beobachtung” für das übergeordnete Regelungsvorhaben haben.</li>
            <li>„Beobachtung”: Sie können das Modul öffnen. Wenn die Prüfung fertig gestellt wurde, können Sie das Prüfungsergebnis einsehen. Die Rolle “Beobachtung” des Regelungsvorhabens beinhaltet die Beobachtung aller Module zugehöriger Gesetzesfolgenabschätzungen.</li>
          </ul>
          <h2>Zugriffsrechte ändern</h2>
          <p>Öffnen Sie im Aktionen-Menü (Drei-Punkte-Menü) den Dialog „Berechtigungen“. Alternativ wählen Sie im jeweiligen Modul oben rechts den Button „Berechtigungen“ aus. Dort können Sie die Mitarbeit für einzelne Module der Gesetzesfolgenabschätzung vergeben. Die Mitarbeit oder Beobachtung an allen GFA-Modulen können Sie über den Berechtigungsdialog des übergeordneten Regelungsvorhabens vergeben.</p>`,
        },
        aktionen: 'Aktionen',
        bearbeitetAmAsc: 'Zuletzt bearbeitet (ältestes zuerst)',
        bearbeitetAmDesc: 'Zuletzt bearbeitet (neuestes zuerst)',
        alphabetischAsc: 'Alphabetisch (A-Z)',
        alphabetischDesc: 'Alphabetisch (Z-A)',
      },
      status: {
        IN_BEARBEITUNG: 'In Bearbeitung',
        NICHT_BEARBEITET: 'Noch nicht bearbeitet',
        VOLLSTAENDIG_BEARBEITET: 'Fertig gestellt',
      },
      checkAll: 'Alle fertiggestellten Module auswählen',
      exportBtn: 'Ergebnisdokumentation anzeigen',
    },
    modules: {
      general: {
        help: 'Hilfe',
        infoRvTitle: 'Informationen zum Regelungsvorhaben',
        infoRv: 'Informationen zum Regelungsvorhaben',
        dataExportBtn: 'Datenexport (PDF)',
        contactPerson: 'Fachliche Zuständigkeit',
        contactPersonTitle: 'Kontakt',
        summaryTitle: 'Fertigstellung',
        summaryExplanation:
          'Die Angaben auf dieser Seite können im Anschluss an die Fertigstellung in den Begründungsteil/in das Vorblatt des Regelungsentwurfs übernommen werden.',
        summarySubmit: 'Modul jetzt fertig stellen',
        summaryBearbeitung: 'Zur Bearbeitung',
        successTitle: 'Das Modul wurde erfolgreich fertiggestellt.',
        successText:
          'Auf der Modulübersicht können Sie die Zusammenfassung als Ergebnisdokumentation für Ihren Regelungsentwurf übernehmen.',
        successLink: 'Zur Modulübersicht',
        errorTitle: 'Das Modul wurde nicht erfolgreich fertiggestellt.',
        errorText: 'Auf der Modulübersicht können Sie das Modul zu einem späteren Zeitpunkt noch einmal auswählen.',
        errorLink: 'Zum Modulübersicht',
        moduleStarten: 'Modul jetzt starten',
        moduleAbbrechen: 'Abbrechen',
      },
      validationMessages: {
        default: 'Bitte geben Sie die Auswirkungen an.',
        defaultVoraussetzungen: 'Bitte geben Sie die Voraussetzungen an.',
        radioGroup: 'Bitte wählen Sie eine Antwort aus.',
        defaultRequiredField: 'Bitte füllen Sie das Pflichtfeld aus.',
      },
      exportToEditor: {
        successMessage: '{{moduleName}} Daten erfolgreich in den Editor übertragen.',
        errorMessage: 'Fehler beim Übertragen der {{moduleName}} Daten in den Editor.',
        errorCodes: {
          1081: `<br />Das Modul der Gesetzesfolgenabschätzung wurde im Editor noch nicht implementiert. 1081`,
          1082: `<br />Es wurde keine Dokumentenmappe gefunden. Bitte legen Sie erst eine Dokumentenmappe zu dem Regelungsvorhaben der Gesetzesfolgenabschätzung an. 1082`,
          1083: `<br/>Es wurde kein Vorblatt gefunden. Bitte fügen Sie der Dokumentenmappe erst ein Dokument vom Typ Vorblatt hinzu. 1083`,
          1084: `<br/>Es wurde keine Begründung gefunden. Bitte fügen Sie der Dokumentenmappe erst ein Dokument vom Typ Begründung hinzu. 1084`,
          1085: `<br/>Das Regelungsvorhaben der Gesetzesfolgenabschätzung ist dem Editor unbekannt. 1085`,
          1086: `<br/>Sie haben keine Zugriffsrechte. 1086`,
        },
      },
      verbraucher: de_verbraucher,
      sonstige_kosten: {
        name: 'Sonstige Kosten',
        zusammenfassung: {
          noImpact: 'Es sind keine sonstigen Kosten zu erwarten.',
        },
      },
      sonstigeKosten: de_sonstige,
      preise: de_preise,
      weitere: de_weitere,
      kmu: de_kmu,
      gleichwertigkeit: de_gleichwertigkeit,
      einnahmenAusgaben: de_einnahmenAusgaben,
      ea_oehh: {
        name: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
      },
      demografie: de_demografie,
      gleichstellung: de_gleichstellung,
      disability: de_disability,
      enap: de_enap,
      evaluierung: de_evaluierung,
      erfuellungsaufwand: de_erfuellungsaufwand,
      experimentierklauseln: de_experimentierklauseln,
      experimentierklausel: {
        name: 'Assistenten für Experimentierklauseln – Prüfung und Formulierung',
      },
      referenzenText: {
        extendedText: {
          hasRights: `Schauen Sie sich die Ergebnisse auf der <a target="_blank" href="{{moduleLink}}">Fertigstellungsseite des Moduls</a> an.`,
          noRights: `Derzeit fehlen Ihnen die Leserechte für das Modul. Bitte wenden Sie sich an die für die Gesetzesfolgenabschätzung zuständige Person: <a href="mailto:{{verantwortlichenPersonEmail}}">{{verantwortlichenPersonName}}</a>.`,
        },
        extendedInfoText:
          'Im Modul „{{moduleName}}” der Gesetzesfolgenabschätzung wurden relevante Auswirkungen festgestellt:',
        hasRights: `Schauen Sie sich dazu auch das Modul <a target="_blank" href="{{moduleLink}}">„{{moduleName}}“</a> der Gesetzesfolgenabschätzung an.`,
        noRights: `Schauen Sie sich dazu auch das Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung an. <br> Derzeit fehlen Ihnen die Leserechte für das Modul. Bitte wenden Sie sich an die für die Gesetzesfolgenabschätzung zuständige Person: <a href="mailto:{{verantwortlichenPersonEmail}}">{{verantwortlichenPersonName}}</a>.`,
        requestRights: `Derzeit fehlen Ihnen die Lese-/Schreibrechte für das Modul. Bitte wenden Sie sich an die für die Gesetzesfolgenabschätzung zuständige Person: <a href="mailto:{{verantwortlichenPersonEmail}}">{{verantwortlichenPersonName}}</a>.`,
        enap: {
          moduleTitle: 'eNAP - elektronische Nachhaltigkeitsprüfung',
          hasSelectedCheckboxSdg1: 'SDG 1 Keine Armut',
          hasSelectedCheckboxSdg31: 'Indikatorenbereich 3.1 Gesundheit und Ernährung: Länger gesund leben',
          hasSelectedCheckboxSdg32: 'Indikatorenbereich 3.2 Luftbelastung: Gesunde Umwelt erhalten',
          hasSelectedCheckboxSdg4: 'SDG 4 Hochwertige Bildung',
          hasSelectedCheckboxSdg41:
            'Indikatorenbereich 4.1 Bildung: Bildung und Qualifikation kontinuierlich verbessern',
          hasSelectedCheckboxSdg42:
            'Indikatorenbereich 4.2 Perspektiven für Familien: Vereinbarkeit von Familie und Beruf verbessern',
          hasSelectedCheckboxSdg5: 'SDG 5 Geschlechtergerechtigkeit',
          hasSelectedCheckboxSdg82:
            'Indikatorenbereich 8.2 Staatsverschuldung: Staatsfinanzen konsolidieren – Generationengerechtigkeit schaffen',
          hasSelectedCheckboxSdg83:
            'Indikatorenbereich 8.3 Wirtschaftliche Zukunftsvorsorge: Gute Investitionsbedingungen schaffen – Wohlstand dauerhaft erhalten',
          hasSelectedCheckboxSdg85: 'Indikatorenbereich 8.5 Beschäftigung: Beschäftigungsniveau steigern',
          hasSelectedCheckboxSdg9: 'SDG 9 Industrie, Innovationen und Infrastruktur',
          hasSelectedCheckboxSdg91b:
            'Indikator 9.1.b Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung',
          hasSelectedCheckboxSdg10: 'SDG 10 Weniger Ungleichheiten',
          hasSelectedCheckboxSdg101:
            'Indikatorenbereich 10.1 Gleiche Bildungschancen: Schulische Bildungserfolge von Ausländern in Deutschland verbessern',
          hasSelectedCheckboxSdg11: 'SDG 11 Nachhaltige Städte und Gemeinden',
          hasSelectedCheckboxSdg111: 'Indikatorenbereich 11.1 Flächeninanspruchnahme: Flächen nachhaltig nutzen',
          hasSelectedCheckboxSdg112: 'Indikatorenbereich 11.2 Mobilität: Mobilität sichern – Umwelt schonen',
          hasSelectedCheckboxSdg113: 'Indikatorenbereich 11.3 Wohnen: Bezahlbarer Wohnraum für alle',
          hasSelectedCheckboxSdg121:
            'Indikatorenbereich 12.1 Nachhaltiger Konsum: Konsum umwelt- und sozialverträglich gestalten',
          hasSelectedCheckboxSdg141:
            'Indikatorenbereich 14.1 Meere schützen: Meere und Meeresressourcen schützen und nachhaltig nutzen',
          hasSelectedCheckboxSdg16: 'Indikatorenbereich 16.1 Kriminalität: Persönliche Sicherheit weiter erhöhen',
          hasSelectedCheckboxSdg161: 'Indikatorenbereich 16.1: Kriminalität: Persönliche Sicherheit weiter erhöhen',
          staticTextSDG2:
            'Schauen Sie sich dazu auch das Kapitel 3.1.4 „<a target="_blank" href="{{link}}">Beeinträchtigung von Ökosystemen</a>“ in der Arbeitshilfe „Leitfaden zur Nutzen-Kosten-Abschätzung umweltrelevanter Effekte in der Gesetzesfolgenabschätzung“ an.',
          staticTextSDG3:
            'Schauen Sie sich dazu auch das Kapitel 3.1.1 „<a target="_blank" href="{{link}}">Luftschadstoffemissionen</a>“ in der Arbeitshilfe „Leitfaden zur Nutzen-Kosten-Abschätzung umweltrelevanter Effekte in der Gesetzesfolgenabschätzung“ an.',
          staticTextSDG6: `Schauen Sie sich dazu auch die Kapitel 3.1.4 „<a target="_blank" href="{{link}}">Beeinträchtigung von Ökosystemen</a>“ und 3.1.5 „<a target="_blank" href="{{link2}}">Emission toxischer Stoffe</a>“ in der Arbeitshilfe „Leitfaden zur Nutzen-Kosten-Abschätzung umweltrelevanter Effekte in der Gesetzesfolgenabschätzung“ an.`,
          staticTextSDG13: `Schauen Sie sich dazu auch das Kapitel 3.1.2 „<a target="_blank" href="{{link}}">Treibhausgasemissionen</a>“ in der Arbeitshilfe „Leitfaden zur Nutzen-Kosten-Abschätzung umweltrelevanter Effekte in der Gesetzesfolgenabschätzung“ an.`,
          staticTextSDG15: `Schauen Sie sich dazu auch die Arbeitshilfe „<a target="_blank" href="{{link}}">Leitfaden zur Nutzen-Kosten-Abschätzung umweltrelevanter Effekte in der Gesetzesfolgenabschätzung</a>“ an. `,
        },
        erfuellungsaufwand: {
          moduleTitle: 'Erfüllungsaufwand',
        },
        gleichwertigkeit: {
          moduleTitle: 'Gleichwertigkeits - Check',
          hasSelectedEngagement: 'Engagement, Zusammenhalt und Teilhabe',
          hasSelectedWohnraum: 'Räumliche Strukturen und Wohnraum',
          hasSelectedDaseinsvorsoge: 'Daseinsvorsorge',
          hasSelectedMobilitaet: 'Mobilität und digitale Infrastruktur',
          hasSelectedDaseinsvorsorge: 'Daseinsvorsorge',
          hasSelectedWirtschaft: 'Wirtschaft und Innovation',
          hasSelectedLebensgrundlage: 'Natürliche Lebensgrundlagen',
        },
        gleichstellung: {
          moduleTitle: 'Gleichstellungsorientierte GFA',
          hasSelectedCheckboxArbeit: 'Arbeit',
          hasSelectedCheckboxWissen: 'Wissen',
          hasSelectedCheckboxZeit: 'Zeit',
          hasSelectedArbeit: 'Arbeit',
          hasSelectedZeit: 'Zeit',
          hasSelectedGeld: 'Geld',
          hasSelectedGesundheit: 'Gesundheit',
          hasSelectedEntscheidungsmacht: 'Entscheidungsmacht',
          hasSelectedGewalt: 'Gewalt',
        },
        einnahmenAusgaben: {
          moduleTitle: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
        },
        sonstigeKosten: {
          moduleTitle: 'Sonstige Kosten',
        },
        disability: {
          moduleTitle: 'Disability Mainstreaming',
          hasSelectedCheckboxTeilhabe: 'Teilhabe- und Entfaltungsmöglichkeiten von Menschen mit Behinderungen',
          hasSelectedCheckboxHauptpruefung2: 'Teilhabe- und Entfaltungsmöglichkeiten von Menschen mit Behinderungen',
          hasSelectedCheckboxHauptpruefung3: 'Zugangs- und Nutzungsmöglichkeiten von Menschen mit Behinderungen',
          hasSelectedCheckboxZugang: 'Zugangs- und Nutzungsmöglichkeiten von Menschen mit Behinderungen',
        },
        demografie: {
          moduleTitle: 'Demografie-Check',
          hasSelectedArbeit: 'Arbeit, Beruf, Wirtschaft',
          hasSelectedFamilie: 'Familie, Kinder, Jugendliche',
          hasSelectedZusammenhalt: 'Zusammenhalt der Generationen, Zivilgesellschaftliches Engagement',
          hasSelectedAlter: 'Alter, Wohnen, Pflege',
        },
        kmu: {
          moduleTitle: 'KMU-Test',
          erfuellungsaufwand: {
            notFertiggestelltText: {
              hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen.`,
              noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen.`,
              moreThanOneMioEurWirtschaft: {
                hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob der {{currentModuleName}} durchgeführt werden sollte.`,
                noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob der {{currentModuleName}} durchgeführt werden sollte.`,
              },
            },
            moreThanOneMioEurWirtschaft: {
              satisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der <a target="_blank" href="{{moduleLink}}">jährliche Erfüllungsaufwand für die gesamte Wirtschaft</a> auf mehr als 1 Mio. EUR geschätzt. Wählen Sie deshalb hier „Ja“ aus und führen Sie den {{currentModuleName}} durch.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die gesamte Wirtschaft auf mehr als 1 Mio. EUR geschätzt. Wählen Sie deshalb hier „Ja“ aus und führen Sie den {{currentModuleName}} durch.`,
              },
              notsatisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der <a target="_blank" href="{{moduleLink}}">jährliche Erfüllungsaufwand für die gesamte Wirtschaft</a> auf weniger als 1 Mio. EUR geschätzt. Prüfen Sie dennoch, ob der Erfüllungsaufwand pro Unternehmen/Jahr mehr als 100 EUR beträgt.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die gesamte Wirtschaft auf weniger als 1 Mio. EUR geschätzt. Prüfen Sie dennoch, ob der Erfüllungsaufwand pro Unternehmen/Jahr mehr als 100 EUR beträgt.`,
              },
            },
          },
          sonstigeKosten: {
            notFertiggestelltText: {
              hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” zu prüfen, ob und in welcher Höhe das Regelungsvorhaben <a target="_blank" href="{{moduleLink}}">sonstige Kosten</a> verursacht`,
              noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” zu prüfen, ob und in welcher Höhe das Regelungsvorhaben sonstige Kosten verursacht.`,
            },
          },
        },
        evaluierung: {
          moduleTitle: 'Evaluierung',
          erfuellungsaufwand: {
            notFertiggestelltText: {
              hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen.`,
              noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen.`,
              moreThanOneMioEurSachaufwandBuergermoreThanHundredThousandHrsZeitaufwandBuerger: {
                hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob der {{currentModuleName}} durchgeführt werden sollte.`,
                noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob der {{currentModuleName}} durchgeführt werden sollte.`,
              },
              moreThanOneMioEurWirtschaft: {
                hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob eine Evaluierung durchgeführt werden sollte.`,
                noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob eine Evaluierung durchgeführt werden sollte.`,
              },
              moreThanOneMioEurVerwaltung: {
                hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den <a target="_blank" href="{{moduleLink}}">{{moduleName}}</a> Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob eine Evaluierung durchgeführt werden sollte.`,
                noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” den {{moduleName}} Ihres Regelungsvorhabens zu berechnen, da dieser ein wichtiges Entscheidungskriterium dafür ist, ob eine Evaluierung durchgeführt werden sollte.`,
              },
            },
            moreThanOneMioEurSachaufwandBuergermoreThanHundredThousandHrsZeitaufwandBuerger: {
              satisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für Bürgerinnen und Bürger</a> auf mehr als eine Million Euro jährliche Sachkosten und/oder 100.000 Stunden jährlicher Aufwand geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
                noRights: `Im Modul „Erfüllungsaufwand“ der Gesetzesfolgenabschätzung wurde der Erfüllungsaufwand für Bürgerinnen und Bürger auf mehr als eine Million Euro jährliche Sachkosten und/oder 100.000 Stunden jährlicher Aufwand geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
              },
              notsatisfies: {
                hasRights:
                  'Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für Bürgerinnen und Bürger</a> auf weniger als eine Million Euro jährliche Sachkosten und 100.000 Stunden jährlicher Aufwand geschätzt. Wählen Sie deshalb hier „Nein“ aus.',
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der Erfüllungsaufwand für Bürgerinnen und Bürger auf weniger als eine Million Euro jährliche Sachkosten und 100.000 Stunden jährlicher Aufwand geschätzt. Wählen Sie deshalb hier „Nein“ aus.`,
              },
            },
            moreThanOneMioEurWirtschaft: {
              satisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für die Wirtschaft</a> auf mehr als eine Million Euro geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die Wirtschaft auf mehr als eine Million Euro geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
              },
              notsatisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für die Wirtschaft</a> auf weniger als eine Million Euro geschätzt. Wählen Sie deshalb hier „Nein“ aus.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die Wirtschaft auf weniger als eine Million Euro geschätzt. Wählen Sie deshalb hier „Nein“ aus.`,
              },
            },
            moreThanOneMioEurVerwaltung: {
              satisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für die Verwaltung</a> auf mehr als eine Million Euro geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die Verwaltung auf mehr als eine Million Euro geschätzt. Wählen Sie deshalb hier „Ja“ aus.`,
              },
              notsatisfies: {
                hasRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche <a target="_blank" href="{{moduleLink}}">Erfüllungsaufwand für die Verwaltung</a> auf weniger als eine Million Euro geschätzt. Wählen Sie deshalb hier „Nein“ aus.`,
                noRights: `Im Modul „{{moduleName}}“ der Gesetzesfolgenabschätzung wurde der jährliche Erfüllungsaufwand für die Verwaltung auf weniger als eine Million Euro geschätzt. Wählen Sie deshalb hier „Nein“ aus.`,
              },
            },
          },
          sonstigeKosten: {
            notFertiggestelltText: {
              hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” zu prüfen, ob und in welcher Höhe das Regelungsvorhaben <a target="_blank" href="{{moduleLink}}">sonstige Kosten</a> verursacht, da diese ein wesentliches Entscheidungskriterium dafür sind, ob eine Evaluierung durchgeführt werden sollte.`,
              noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” zu prüfen, ob und in welcher Höhe das Regelungsvorhaben sonstige Kosten verursacht, da diese ein wesentliches Entscheidungskriterium dafür sind, ob eine Evaluierung durchgeführt werden sollte.`,
            },
          },
          einnahmenAusgaben: {
            notFertiggestelltText: {
              hasRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” die <a target="_blank" href="{{moduleLink}}">Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte</a> Ihres Vorhabens zu berechnen, da diese ein wichtiges Entscheidungskriterium dafür sind, ob eine Evaluierung durchgeführt werden sollte.`,
              noRights: `Es ist erforderlich, vor der Bearbeitung des Moduls „{{currentModuleName}}” die Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte Ihres Vorhabens zu berechnen, da diese ein wichtiges Entscheidungskriterium dafür sind, ob eine Evaluierung durchgeführt werden sollte.`,
            },
          },
        },
      },
      freigabeModal: {
        title: 'Berechtigungen',
        bodyText: `<p>Hier sehen Sie alle Personen, die eine Berechtigung für das Modul haben.</p>
          <p>Die Berechtigungen für ein Modul der Gesetzesfolgenabschätzung richten sich nach den Berechtigungen des zugehörigen Regelungsvorhabens. Falls Sie Änderungen an den Berechtigungen des Regelungsvorhabens vornehmen möchten, müssen Sie das beim zugehörigen Regelungsvorhaben tun.</p>
          <p>Darüber hinaus können Sie hier Personen, die für das Regelungsvorhaben die Rolle "Beobachtung" haben, die Mitarbeit für einzelne Module der Gesetzesfolgenabschätzung erteilen.</p>
        `,
      },
    },
  },
};
