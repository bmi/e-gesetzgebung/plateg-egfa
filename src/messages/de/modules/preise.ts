// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_preise = {
  name: 'Auswirkungen auf Einzelpreise und Preisniveau',
  einleitung: {
    linkName: 'Einführung',
    title: 'Auswirkungen auf Einzelpreise und Preisniveau',
    text: `<p>In diesem Modul beschreiben Sie die Auswirkungen Ihres Regelungsvorhabens auf Einzelpreise und Preisniveau nach <a href="{{basePath}}/arbeitshilfen/download/34#page=35" target="_blank" style="white-space: normal">§ 44 Absatz 5 Nr. 1 GGO</a>.</p>
          <p>Anschließend können Sie die Beschreibung für Vorblatt und Begründung Ihres Regelungsvorhabens verwenden.</p>`,
  },
  wesentlicheAuswirkungen: {
    linkName: 'Wesentliche Auswirkungen',
    title: 'Wesentliche Auswirkungen',
    question: 'Sind durch das Regelungsvorhaben wesentliche Auswirkungen auf Einzelpreise und Preisniveau zu erwarten?',
    additionalExplanation: 'Zusammenfassung der Auswirkungen',
    drawerText:
      'Die Angaben auf dieser Seite können im Anschluss an die Fertigstellung in den Begründungsteil/ in das Vorblatt des Regelungsentwurfs übernommen werden.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    subtitle: 'Auswirkungen auf Einzelpreise und Preisniveau',
    noImpact: 'Es sind keine wesentlichen Auswirkungen auf Einzelpreise und Preisniveau zu erwarten.',
    impact: 'Es sind folgende wesentliche Auswirkungen auf Einzelpreise und Preisniveau zu erwarten:',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung zur Darstellung der Auswirkungen auf die Einzelpreise und das Preisniveau in das Vorblatt und die Begründung des Regelungsentwurfs übernehmen.',
  },
  zusammenfassung: {
    noImpact: 'Es sind keine wesentlichen Auswirkungen auf Einzelpreise und Preisniveau zu erwarten.',
  },
  contactPerson: de_contacts.preise.contactPerson,
};
