// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_kmu = {
  name: 'Auswirkungen auf mittelständische Unternehmen - KMU-Test',
  einleitung: {
    linkName: 'Einführung',
    title: 'Auswirkungen auf mittelständische Unternehmen - KMU-Test',
    text: `<p>Zahlreiche nationale und internationale Studien belegen, dass kleine und mittlere Unternehmen (KMU) durch die Umsetzung rechtlicher Regelungen oftmals besonders belastet sind.</p> 
<p>Dieses Modul soll Ihnen bereits beim Entwurf Ihres Regelungsvorhabens (Anwendung für alle Gesetze und Verordnungen des Bundes einschließlich der 1:1-Umsetzung von EU-Richtlinien) helfen, Belange mittelständischer Unternehmen im Rahmen der Gesetzesfolgenabschätzung (GFA) nach <a href="{{basePath}}/arbeitshilfen/download/34#page=33" target="_blank">§§ 43</a> und <a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank">44 der Gemeinsamen Geschäftsordnung der Bundesministerien (GGO)</a> sowie <a href="https://www.gesetze-im-internet.de/nkrg/__1.html" target="_blank">§ 1 Absatz 3</a> und <a href="https://www.gesetze-im-internet.de/nkrg/__4.html" target="_blank">§ 4 Absatz 2 NKRG</a> leichter zu berücksichtigen und mögliche Regelungsalternativen einfacher zu prüfen.</p>
<p>Regelungsvorhaben können sich für mittelständische Unternehmen sowohl nutzbringend als auch belastend auswirken. Dieses Modul beschränkt sich auf die von der GGO geforderte Darstellung der Kostentransparenz. Es bleibt in Ihrer Verantwortung, ob und in welcher Ihnen geeignet erscheinenden Weise Sie auch den Regelungsnutzen insbesondere für KMU in der Gesetzesbegründung qualitativ beschreiben und/oder quantitativ den darzustellenden Belastungen gegenüberstellen.</p>
<p>Soweit in diesem Modul nach der Wirkung des Regelungsvorhabens auf Unternehmen bzw. KMU gefragt ist, bezieht sich die Frage nicht auf ein einzelnes Unternehmen, sondern auf die Regelungswirkung für die Gruppe der kleinen und mittleren Unternehmen im Verhältnis zur Gruppe der großen Unternehmen.</p>
<p>Unternehmen bzw. KMU im Sinne dieses Leitfadens sind auch die Freien Berufe sowie die öffentlichen Unternehmen im Sinne des <a href="https://www.gesetze-im-internet.de/fpstatg/__2.html" target="_blank">§ 2 Absatz 3 des Finanz- und Personalstatistikgesetzes (FPStatG)</a>.</p>
<p>Weit über 90 Prozent aller Unternehmen in Deutschland sind KMU, der ganz überwiegende Teil davon sind Kleinstunternehmen mit 1 bis 9 Beschäftigten.</p>
<p>Bei der Beantwortung der folgenden Fragen wird empfohlen, sich die Wirkungen des Regelungsvorhabens auf ein deutsches KMU mit 4 Beschäftigten (inkl. der Inhaberin oder des Inhabers) und 600.000 EUR Jahresumsatz vorzustellen.</p>
`,
  },
  relevanzpruefung: {
    linkName: 'Relevanzprüfung',
    title: 'Relevanzprüfung',
    question1: '1. Sind von dem Regelungsvorhaben KMU betroffen?',
    question1TextareaLabel: 'Bitte begründen Sie Ihre Auswahl.',
    question2:
      '2. Wird durch das Regelungsvorhaben schätzungsweise ein Erfüllungsaufwand von mehr als 1 Mio. EUR für die gesamte Wirtschaft oder von mehr als 100 EUR pro Unternehmen/Jahr ausgelöst?',
    question2TextareaLabel: 'Bitte begründen Sie Ihre Auswahl.',
    drawerTitle: 'Schwellenwerte für die Pflicht zur Prüfung der Belange mittelständischer Unternehmen',
    drawerText: `<ul><li><p><strong>Erfüllungsaufwand:</strong> Der hier zu schätzende Erfüllungsaufwand umfasst den gesamten messbaren Zeitaufwand und die Kosten, die durch die Befolgung einer bundesrechtlichen Regelung für die Wirtschaft entstehen. Für hilfreiche Informationen zur Methodik der Schätzung des Erfüllungsaufwands siehe: <a href="https://www.destatis.de/DE/Themen/Staat/Buerokratiekosten/Erfuellungsaufwand/erfuellungsaufwand.html" target="_blank">Methodische Erläuterungen</a></p></li>
<li><p><strong>1 Mio. EUR für die gesamte Wirtschaft:</strong> Bitte nehmen Sie hier eine erste, vorläufige Schätzung vor. Wenn z. B. 100.000 Unternehmen betroffen wären, müsste ein Erfüllungsaufwand von über 10 EUR pro Unternehmen/Jahr anfallen, damit der Schwellenwert von 1 Mio. EUR überschritten wird. Zudem kann im Fall einer Änderung einer rechtlichen Regelung der geschätzte Erfüllungsaufwand der bestehenden rechtlichen Regelung als Orientierung für die erste, vorläufige Schätzung herangezogen werden.</p></li>
<li><p><strong>100 EUR pro Unternehmen/Jahr:</strong> Dieser Schwellenwert greift nur für Regelungen mit weniger als 10 000 betroffenen Unternehmen, weil anderenfalls der Schwellenwert von 1 Mio. Euro überschritten wird.</p></li>`,
  },
  besondereBelastungen: {
    linkName: 'Besondere Belastungen mittelständischer Unternehmen',
    title: 'Besondere Belastungen mittelständischer Unternehmen',
    title2:
      'Nachfolgend werden Ihnen die aus Erfahrung bekannten Kostentreiber für den Erfüllungsaufwand sowie die sonstigen Kosten vorgestellt und kurz erläutert. Im Verhältnis zu Großunternehmen sind KMU häufig besonders belastet. Die Kenntnis der möglichen Quellen besonderer KMU-Belastungen kann Ihnen die Formulierung der Gesetzesbegründung erleichtern.',
    EgfaKMUUmsetzungType: {
      title: 'In welchen Fällen werden KMU vermutlich durch Erfüllungsaufwand besonders belastet?',
      items: {
        RV_UMSETZUNG_NICHT_ELEKTRONISCH: {
          label:
            'Die Umsetzung des Regelungsvorhabens im Unternehmen erfolgt per Hand statt elektronisch (vor allem in Kleinstunternehmen).',
          tooltip:
            'Viele Regelungsvorgaben werden durch elektronisch/ automatisiert ablaufende Prozesse schnell und effizient bearbeitet. Wenn die dazu notwendigen technischen Voraussetzungen nicht gegeben sind bzw. soweit sich diese technischen Investitionen aufgrund der geringen Fallzahl für KMU nicht lohnen, können daraus besondere Belastungen erwachsen.',
        },
        RV_UMSETZUNG_ROUTINE_FEHLT: {
          label: 'Dem Unternehmen fehlt die Routine bei der Umsetzung des Regelungsvorhabens.',
          tooltip:
            'Die Bearbeitungshäufigkeit bestimmter Arbeitsschritte erhöht erfahrungsgemäß die Routine (Lernkurve). Ein Beispiel hierfür ist die Arbeitsbescheinigung für ausscheidende Beschäftigte: je größer das Unternehmen, desto häufiger werden Arbeitsbescheinigungen ausgestellt. KMU sind häufig weniger geübt im Umgang mit den Unterlagen und in der Beschaffung der notwendigen Daten als Personalabteilungen in großen Unternehmen.',
        },
        RV_UMSEZUNG_SPEZIALISTEN_FEHLEN: {
          label:
            'Im Unternehmen fehlen Spezialistinnen und Spezialisten zur Umsetzung des Regelungsvorhabens (fachfremde Beschäftigte oder die Unternehmerin oder der Unternehmer selbst müssen sich nötiges Wissen erst aneignen).',
          tooltip:
            'Viele Regelungsvorgaben werden in großen Unternehmen meist durch spezialisierte Beschäftigte, z. B. in Finanz-, Personal- oder Rechtsabteilungen bearbeitet. In KMU kümmert sich vielfach die Unternehmerin oder der Unternehmer selbst oder fachfremdes Personal um die Bearbeitung. Zudem besitzen große Unternehmen teilweise einen zusätzlichen Informationsvorsprung, da sie sich stärker auf Verbandsebene engagieren als KMU.',
        },
        RV_UMSETZUNG_INVESTITIONEN: {
          label:
            'Die Umsetzung des Regelungsvorhabens bedingt einmalige oder laufende Investitionen und/oder es fallen einmalige oder laufende externe Kosten an.',
          tooltip:
            'Manche Regelungsvorhaben sind mit hohem Sachaufwand in Form von Investitionskosten oder Kosten für externe Dienstleister verbunden. Als Beispiel für Investitionskosten kann die Anschaffung eines Pfandautomaten oder einer bestimmten Software, die zur Umsetzung der Regelungsvorgaben nötig ist, dienen. Insbesondere dann, wenn die Investitionskosten nicht proportional mit der Unternehmensgröße steigen, sind KMU relativ stärker belastet als Großunternehmen. Kosten für externe Dienstleister können z. B. die Kosten für die Steuerberatung sein, die notwendige Aufgaben, die sich aus einem Regelungsvorhaben ergeben, für das KMU übernimmt.',
        },
        RV_UMSETZUNG_ANDERE: {
          label: 'Andere',
        },
      },
    },
    rvUmsetzungSummary: {
      label:
        'Bitte stellen Sie hier besondere Belastungen dar, die Sie durch Erfüllungsaufwand für kleine und mittlere Unternehmen vermuten oder für die Sie Anhaltspunkte aus der Wirtschaft erhalten.',
      tooltip: 'Bei Ihrer Beschreibung können Ihnen die obigen Punkte helfen.',
    },
    EgfaKMUBelastungType: {
      title: 'In welchen Fällen werden KMU durch sonstige Kosten besonders belastet?',
      items: {
        RV_BELASTUNG_MARKTANTEILE: {
          label: 'Das Regelungsvorhaben hat voraussichtlich negative Auswirkungen auf Marktanteile oder Umsätze.',
          tooltip:
            'Naturgemäß variieren die konkreten, zu berücksichtigenden negativen Auswirkungen eines Regelungsvorhabens auf den Marktanteil bzw. die Umsätze von KMU von Fall zu Fall. Eine besondere Belastung von KMU kann vermutet werden, wenn das Regelungsvorhaben eine Anpassung von KMU erfordert, damit diese mindestens im gleichen Umfang wie zuvor am Markt teilnehmen können.',
        },
        RV_BELASTUNG_MARKTEINTRITTSBARRIERREN: {
          label: 'Das Regelungsvorhaben schafft voraussichtlich Markteintrittsbarrieren.',
          tooltip:
            'Es ist denkbar, dass ein Regelungsvorhaben hohe Markteintrittshürden schafft oder Zutrittschancen erschwert. Ebenso kann es sich insbesondere für KMU „nicht mehr lohnen“, an einem Markt teilzunehmen, z. B. weil unverhältnismäßig hohe (Investitions-) Kosten entstehen.',
        },
        RV_BELASTUNG_PRODUKTION: {
          label: 'Das Regelungsvorhaben erschwert die Verfügbarkeit von bzw. den Zugang zu Produktionsfaktoren.',
          tooltip:
            'Regelungsvorhaben können z. B. höhere Kosten für die Beschaffung von Material, die Finanzierung mit Fremdkapital oder die Inanspruchnahme externer Dienstleistungen verursachen.',
        },
        RV_BELASTUNG_FORSCHUNG: {
          label: 'Das Regelungsvorhaben erschwert die Fähigkeit, Forschung und Entwicklung zu betreiben.',
          tooltip:
            'Regelungsvorhaben können die Produkt- und Prozessinnovation beeinflussen, sodass es für KMU schwieriger wird, neue bzw. verbesserte Produkte auf den Markt zu bringen. Ein Beispiel für negative Auswirkungen auf die Innovationsfähigkeit könnte die Erschwerung von Patentanmeldungen sein.',
        },
        RV_BELASTUNG_INTERNATIONALER_WETTBEWERB: {
          label:
            'Das Regelungsvorhaben hat voraussichtlich negative Auswirkungen auf die internationale Wettbewerbsfähigkeit.',
          tooltip:
            'Insbesondere in Branchen, die internationalen Handel betreiben, können KMU in Deutschland durch ein Regelungsvorhaben Nachteile gegenüber ihren internationalen Wettbewerbern entstehen. Beispielsweise könnte die Verschärfung bestimmter Produktanforderungen im Inland zu steigenden Produktionskosten dieser Produkte führen. Internationale Wettbewerber, welche diesen Auflagen nicht unterliegen, hätten folglich einen Wettbewerbsvorteil auf dem internationalen Markt.',
        },
        RV_BELASTUNG_UMSETZUNG_FREIWILLIG: {
          label: 'Die Umsetzung der Regelungsvorgaben ist freiwillig.',
          tooltip:
            'Regelungsoptionen, die freiwillig genutzt werden dürfen, können sich auch negativ auf KMU auswirken. Wird es Unternehmen z.B. freigestellt, die Ladenöffnungszeiten zu verlängern, dann ist zu bedenken, dass die damit verbundenen Personal-/ Sachkosten KMU stärker belasten als konkurrierende Großunternehmen.',
        },
        RV_BELASTUNG_GEWINNUNG_VON_FACHKRAEFTEN: {
          label: 'Das Regelungsvorhaben erschwert die Gewinnung von Fachkräften.',
          tooltip:
            'Regelungsvorhaben können die Ausbildung, Zulassung oder Einwanderung von Fachkräften beschränken. Während Großunternehmen häufig global produzieren und Personal auf internationalen Arbeitsmärkten rekrutieren, agieren KMU in der Regel national und sind allein auf den heimischen Arbeitsmarkt angewiesen.',
        },
        RV_BELASTUNG_AUSLAGEN: {
          label:
            'Das Regelungsvorhaben generiert Gebühren/Auslagen, Beiträge oder Umlagen an, die nicht von unternehmensspezifischen Messgrößen abhängen.',
          tooltip:
            'Im Rahmen von Regelungsvorhaben können Gebühren/ Auslagen, Beiträge und Umlagen nach Festbeträgen oder gestaffelt etwa nach der Anzahl der Beschäftigten oder der Betriebstätten erhoben werden. Beispielsweise berechnet sich der Rundfunkbeitrag für Unternehmen nach der Anzahl der Beschäftigten, der Anzahl der Betriebstätten und der Anzahl gewerblich genutzter Kfz.',
        },
        RV_BELASTUNG_ANDERE: {
          label: 'Andere',
        },
      },
    },
    rvBelastungSummary: {
      label:
        'Bitte stellen Sie hier die besonderen Belastungen dar, die Sie durch sonstige Kosten für kleine und mittlere Unternehmen vermuten oder für die Sie Anhaltspunkte aus der Wirtschaft erhalten.',
      tooltip: 'Bei Ihrer Beschreibung können Ihnen die obigen Punkte helfen.',
    },
  },
  regelungsalternativen: {
    linkName: 'Regelungsalternativen/Unterstützungsmaßnahmen',
    title: 'Regelungsalternativen/Unterstützungsmaßnahmen',
    additionalTitle:
      'Wie könnten von dem Regelungsvorhaben betroffene KMU durch geeignete Regelungsalternativen/Unterstützungsmaßnahmen weniger belastet werden?',
    infobox: `Im Folgenden finden Sie eine Liste mit möglichen Regelungsalternativen und Maßnahmen, welche zu Entlastung von KMU beitragen können. <br>
<strong>Prüfen Sie bitte alle vorgeschlagenen Optionen und ihre Anwendbarkeit auf das Regelungsvorhaben.</strong><br>
Die Optionen können dazu beitragen KMU insgesamt oder gezielt bestimmte Gruppen, wie z. B. Kleinstunternehmen oder Gründerinnen und Gründer zu entlasten. <strong>Sollten Ausnahmeregelungen oder unterstützende Maßnahmen sinnvoll und möglich sein, so berücksichtigen Sie diese bitte in Ihrem Gesetzentwurf. Wenn sich z. B. wegen der Natur der Regelung (gleichmäßige Besteuerung, genereller Arbeitsschutz für alle Beschäftigten, Gleichheit der Lebensverhältnisse, einheitlicher Verbraucherschutz) keine geeignete Regelungsalternative oder unterstützende Maßnahme anbietet, erläutern Sie dies bitte kurz und zusammenfassend in der Gesetzesbegründung.</strong> <br>
Es ist nicht notwendig, jede Nicht-Inanspruchnahme einer Ausnahmeregelung oder Unterstützungsmaßnahme einzeln zu begründen.`,
    list1: {
      title: 'Liste möglicher Regelungsalternativen und Maßnahmen zur Entlastung von KMU',
      item1: `Das Regelungsvorhaben ermöglicht durch optionale Regelungen mehrere Durchführungswege und überlässt dem Unternehmen, welcher Prozess sich besser in den bestehenden Prozess im Unternehmen integrieren lässt. <br>
Beispiel: Produktrücknahme im Laden oder in einer externen Sammelstelle`,
      item2: `Die Häufigkeit, in der einer Pflicht nachgekommen werden muss, variiert nach Unternehmensgrößenklasse. <br>
Beispiel: Berichtspflicht für große Unternehmen jährlich, für KMU alle zwei Jahre`,
      item3: `Nach dem Bundesgebührengesetz zulässige Rahmengebührensätze lassen variable Gebühren nach unternehmensspezifischen Messgrößen zu; auch Beiträge und Umlagen können entsprechend variiert werden.`,
      item4: `Die Umsetzungsfrist variiert (verlängerte Übergangsfristen) nach Unternehmensgrößenklasse. <br>
Beispiel: Große Unternehmen setzen zum 01.01.2020 um, KMU zum 01.01.2021`,
    },
    list2: {
      title: 'Flankierende Unterstützungsmaßnahmen',
      item1: `Durchführung spezifischer Informationskampagnen für die Zielgruppe der KMU<br>
Beispiel: Roadshow mit der IHK`,
      item2: `Aufbereitung zielgruppenspezifischer Informationen für KMU <br>
Beispiel: Informationssammlung auf Webseite des Ressorts`,
      item3: `Prüfung der möglichen Inanspruchnahme bestehender Förderangebote (zum Beispiel Zuschüsse und Beratungsleistungen) – einen Überblick über Förderprogramme für Unternehmen finden Sie in der Förderdatenbank des Bundes unter <a href="https://foerderdatenbank.de/" target="_blank">https://foerderdatenbank.de/</a>.`,
      item4: `Gewährung direkter finanzieller Unterstützung für KMU <br>
Beispiel: Bereitstellung von Fördergeldern im Rahmen verfügbarer Haushaltsmittel`,
    },
    textareaLabel:
      'Hier können Sie kurz und zusammenfassend die Maßnahmen oder die Gründe, warum Sie keine Maßnahmen ergreifen, beschreiben.',
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    subtitle: 'Berücksichtigung der Belange mittelständischer Unternehmen',
    infoParagraph:
      'Diese Angaben können Sie nach Fertigstellung des Moduls in den allgemeinen Teil der Begründung des Regelungsentwurfs übernehmen, um die Auswirkungen des Regelungsvorhabens auf mittelständische Unternehmen unter dem Punkt „Gesetzesfolgen/Regelungsfolgen“ darzustellen.',
    notAffected: 'Kleine und mittlere Unternehmen sind von dem Regelungsvorhaben nicht betroffen.',
    notRelevantAffected:
      'Kleine und mittlere Unternehmen sind von dem Regelungsvorhaben nicht in relevantem Umfang betroffen.',
    affected: 'Kleine und mittlere Unternehmen sind von dem Regelungsvorhaben in relevantem Umfang betroffen.',
    unaffected:
      'Durch das Regelungsvorhaben sind keine besonderen Belastungen für kleine und mittlere Unternehmen zu erwarten.',
    impact: 'Es sind folgende wesentliche Berücksichtigungen der Belange mittelständischer Unternehmen zu erwarten:',
    impactHoherErfuellungsaufwand:
      'Es sind folgende besondere Belastungen durch den Erfüllungsaufwand für kleine und mittlere Unternehmen zu erwarten:',
    impactSonstigeKosten:
      'Es sind folgende besondere Belastungen durch sonstige Kosten für kleine und mittlere Unternehmen zu erwarten:',
    impactMassnahmen: 'Maßnahmen oder Gründe, warum keine Maßnahmen ergriffen werden:',
    ergebnisse: {
      title: 'Zusammenfassung der Ergebnisse',
      label: 'Bitte fassen Sie Ihre Ergebnisse abschließend zusammen',
      requiredMsg: 'Bitte fassen Sie Ihre Ergebnisse abschließend zusammen.',
    },
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    subtitle: 'Auswirkungen auf mittelständische Unternehmen - KMU-Test',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie nach Fertigstellung des Moduls in den Begründungsteil und in das Vorblatt des Regelungsentwurfs übernehmen, um die Auswirkungen des Regelungsvorhabens auf mittelständische Unternehmen darzustellen.',
    affected: 'Kleine und mittlere Unternehmen sind von dem Regelungsvorhaben in relevantem Umfang betroffen.',
    unaffected:
      'Durch das Regelungsvorhaben sind keine besonderen Belastungen für kleine und mittlere Unternehmen zu erwarten.',
    impact: 'Es sind folgende wesentliche Berücksichtigungen der Belange mittelständischer Unternehmen zu erwarten:',
    impactHoherErfuellungsaufwand:
      'Es sind folgende besondere Belastungen durch den Erfüllungsaufwand für kleine und mittlere Unternehmen zu erwarten:',
    impactSonstigeKosten:
      'Es sind folgende besondere Belastungen durch sonstige Kosten für kleine und mittlere Unternehmen zu erwarten:',
    impactMassnahmen: 'Maßnahmen oder Gründe, warum keine Maßnahmen ergriffen werden:',
  },
  contactPerson: de_contacts.kmu.contactPerson,
};
