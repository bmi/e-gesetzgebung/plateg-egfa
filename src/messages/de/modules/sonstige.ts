// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_sonstige = {
  name: 'Sonstige Kosten',
  einleitung: {
    linkName: 'Sonstige Kosten',
    title: 'Sonstige Kosten',
    text: `<p>Sie haben in der Gesetzesfolgenabschätzung an verschiedenen Stellen die Möglichkeit, Kosten, die durch ein geplantes Regelungsvorhaben entstehen können, anzugeben. Hierzu zählen die Module der Gesetzesfolgenabschätzung</p>
          <ul>
            <li><a href={{auswirkungOHH}} target='_blank'>Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte</a> </li>
            <li><a href={{erfuellungsaufwand}} target='_blank'>Erfüllungsaufwand</a> </li>
            <li><a href={{auswirkungEinzelpreise}} target='_blank'>Auswirkungen auf Einzelpreise und Preisniveau</a> </li>
            <li><a href={{auswirkungKMU}} target='_blank'>Auswirkungen auf mittelständische Unternehmen – KMU-Test</a> </li>
          </ul>
          <p>Alle Kosten, die durch das Regelungsvorhaben entstehen, aber nicht durch diese Module der Gesetzesfolgenabschätzung erfasst werden, können Sie in diesem Modul angeben.</p>`,
  },
  pruefungSonstigeKosten: {
    linkName: 'Prüfung und Beschreibung der sonstigen Kosten',
    title: 'Prüfung und Beschreibung der sonstigen Kosten',
    question:
      'Sind durch das Regelungsvorhaben sonstige Kosten nach <a href={{link}} target="_blank">§ 44 Abs. 5 Nr. 1 GGO</a> zu erwarten, die nicht in anderen Module der Gesetzesfolgenabschätzung erfasst werden?',
    additionalExplanation: 'Beschreibung der sonstigen Kosten',
    drawerText:
      'Beschreiben Sie bitte in diesem Modul die sonstigen Kosten nach <a href={{link44GGO}} target="_blank">§ 44 Abs. 5 Nr.1 GGO</a>, die nicht durch die Module <a href={{auswirkungOHH}} target="_blank">Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte</a>, <a href={{erfuellungsaufwand}} target="_blank">Erfüllungsaufwand</a>, <a href={{auswirkungEinzelpreise}} target="_blank">Auswirkungen auf Einzelpreise und Preisniveau</a> oder den <a href={{auswirkungKMU}} target="_blank">KMU-Test</a> abgedeckt sind.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    noImpact: 'Es sind keine sonstigen Kosten zu erwarten.',
    subtitle: 'Sonstige Kosten',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung zur Darstellung der sonstigen Kosten in das Vorblatt sowie in die Begründung des Regelungsentwurfs übernehmen.',
  },
  contactPerson: de_contacts.sonstige.contactPerson,
};
