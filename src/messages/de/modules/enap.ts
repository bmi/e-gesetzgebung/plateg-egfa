// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_enap = {
  light: {
    einleitung: {
      title: 'Elektronische Nachhaltigkeitsprüfung',
    },
  },
  name: 'eNAP - elektronische Nachhaltigkeitsprüfung',
  drawerTitle: 'Fachliche Zuständigkeit',
  weitereInformationen: {
    anderweitigRelevantRvTitleDrawerText: `
            <strong>Anderweitige Relevanz (SDG)</strong>
            <p>Hier können Sie angeben, dass das Regelungsvorhaben über Ihre Ausführungen zu Indikatoren bzw. Indikatorenbereichen der Deutschen Nachhaltigkeitsstrategie hinaus weitere Auswirkungen in nennenswertem Umfang auf die Umsetzung eines der globalen Ziele für nachhaltige Entwicklung (<span lang="en">Sustainable Development Goal</span> – SDG) bzw. eines seiner Unterziele hat.</p>`,
    anderweitigRelevantRvTitle: 'Weitere Auswirkungen auf das „{{title}}“',
    anderweitigRelevantRvLabel: 'Ist dieses SDG anderweitig für das Regelungsvorhaben relevant?',
    anderweitigRelevantRvSummaryLabel: 'Begründung der weiteren Auswirkungen auf das „{{title}}“',
  },
  anderweitigRelevant: {
    title: 'Anderweitige Relevanz für den Bereich „{{bereich}}“',
    text: 'Hier können Sie Angaben machen, wenn Ihr Regelungsvorhaben nennenswerte Auswirkungen im jeweiligen Indikatorenbereich hat, die nicht durch die Indikatoren/Ziele der Deutschen Nachhaltigkeitsstrategie erfasst werden.',
    label: 'Begründung der anderweitigen Relevanz',
  },
  relevant: {
    label: 'Ist dieser Indikatorenbereich für das Regelungsvorhaben relevant?',
    prinzipien: 'Ist dieses Prinzip für das Regelungsvorhaben relevant?',
    relevantAreaTitle: 'Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:',
  },
  einleitung: {
    linkName: 'Einführung',
    title: 'eNAP - elektronische Nachhaltigkeitsprüfung',
    section1: {
      title: 'Einführung',
      text: `Herzlich willkommen in der Anwendung eNAP – elektronische Nachhaltigkeitsprüfung! Im Rahmen der Gesetzesfolgenabschätzung ist gemäß <a href='/egesetzgebung-platform-backend/arbeitshilfen/download/34#page=34' target='_blank'>§ 44 Abs. 1 S. 4 GGO</a> zu prüfen, ob die Wirkungen eines Vorhabens einer nachhaltigen Entwicklung entsprechen. Zentraler Bezugspunkt für die Prüfung ist die <a target="_blank" class="internal" href="{{basePath}}/arbeitshilfen/download/28">Deutsche Nachhaltigkeitsstrategie 2021 (PDF)</a> mit ihren Zielen und Prinzipien für eine nachhaltige Entwicklung.`,
    },
    section2: {
      title: `Die Deutsche Nachhaltigkeitsstrategie zur Umsetzung der Ziele für Nachhaltige Entwicklung / <span lang="en">Sustainable Development Goals</span> (SDGs)`,
    },
    section3: {
      title: `Hintergrundinformationen`,
      text: 'Hier finden Sie weitere Informationen zu den Inhalten und Zielen der Deutschen Nachhaltigkeitsstrategie.',
      links: {
        nachhaltigkeitsstrategie:
          '<a target="_blank" class="internal" href="{{basePath}}/arbeitshilfen/download/28">Deutsche Nachhaltigkeitsstrategie - Weiterentwicklung 2021 (PDF)</a>',
        indikatorenbericht:
          '<a target="_blank"  href="https://www.destatis.de/DE/Themen/Gesellschaft-Umwelt/Nachhaltigkeitsindikatoren/Publikationen/Downloads-Nachhaltigkeit/indikatoren-0230001219004.pdf">Indikatorenbericht des Statistischen Bundesamtes</a>',
        grundsatzbeschluss_nachhaltigkeitsstrategie:
          '<a target="_blank" class="internal"  href="{{basePath}}/arbeitshilfen/download/36">Grundsatzbeschluss 2022 zur Deutschen Nachhaltigkeitsstrategie (PDF)</a>',
        nhk_ziele:
          '<a target="_blank" class="internal"  href="{{basePath}}/arbeitshilfen/download/35fb44cc-3e4c-4a26-8299-e7215104af5b">Empfehlungen Einbeziehung NHK-Ziele (PDF)</a>',
        empfehlung_zur_staerkung_der_verbindlichkeit:
          '<a target="_blank" class="internal"  href="{{basePath}}/arbeitshilfen/download/37">Empfehlungen zur Stärkung der Verbindlichkeit der Nachhaltigkeitsziele bei der Erstellung von Gesetzen und Verordnungen 2022 (PDF)</a>',
        best_practice_nachhaltigkeitspruefung:
          '<a target="_blank" class="internal"  href="{{basePath}}/arbeitshilfen/download/69">Best Practice Nachhaltigkeitsprüfung: Verordnung zur Änderung der Verordnung über die Verbrennung und die Mitverbrennung von Abfällen und zur Änderung der Chemikalien-Verbotsverordnung (PDF)</a>',
        informationen:
          '<a target="_blank" href="https://www.bundesregierung.de/breg-de/themen/nachhaltigkeitspolitik/eine-strategie-begleitet-uns">Weitere Informationen</a>',
        zentraleBegriffe: 'Zentrale Begriffe',
        nutzungshinweise: 'Nutzungshinweise',
      },
      zentraleBegriffe: {
        title: 'Zentrale Begriffe',
        content: `<dl>
            <dt>Deutsche Nachhaltigkeitsstrategie (DNS)</dt>
            <dd>Die Deutsche Nachhaltigkeitsstrategie ist ein wesentlicher Rahmen für die Umsetzung der globalen Nachhaltigkeitsziele (SDG) in, durch und mit Deutschland. Die Strategie wird regelmäßig überarbeitet, zuletzt in der Weiterentwicklung 2021 (Beschluss des Bundeskabinetts erfolgte am 10. März 2021).</dd>
            <dt>eNAP</dt>
            <dd>Bezeichnung für die Fachanwendung zur Durchführung der Nachhaltigkeitsprüfung. Die Abkürzung steht für „elektronische Nachhaltigkeitsprüfung“.</dd>
            <dt>Indikator</dt>
            <dd>Die DNS enthält zu jedem globalen Ziel für nachhaltige Entwicklung (<span lang="en">Sustainable Development Goal</span>, SDG) der Agenda 2030 der Vereinten Nationen zusätzliche nationale Indikatoren, insgesamt 75 Indikatoren und Ziele in 39 Bereichen. Die 17 SDG bilden die Struktur des Indikatorensystems der DNS. Jeder Indikator ist an ein Ziel gebunden. Die Analyse der Indikatorenentwicklung erfolgt durch das Statistische Bundesamt, das hierzu Wettersymbole vergibt.</dd>
            <dt>Indikatorenbereich</dt>
            <dd>Die Indikatorenbereiche der DNS beschreiben das Themenfeld, in dem eine oder mehrere spezifische Indikatoren von der Bundesregierung festgelegt worden sind. Sie werden ergänzend erläutert durch sogenannte Nachhaltigkeitspostulate.</dd>
            <dt>Nachhaltigkeitsprüfung</dt>
            <dd>Für alle Regelungsvorhaben der Bundesregierung muss das federführende Ressort gemäß § 44 Absatz 1 Satz 4 der gemeinsamen Geschäftsordnung der Bundesministerien (GGO) prüfen und in der Begründung zum Regelungsvorhaben darstellen, welche Auswirkungen das Vorhaben auf eine nachhaltige Entwicklung hat. Bezugspunkt für die Prüfung sind die Prinzipien einer nachhaltigen Entwicklung, Indikatoren und Ziele der DNS.</dd>
            <dt>Prinzipien einer nachhaltigen Entwicklung</dt>
            <dd>Die Prinzipien einer nachhaltigen Entwicklung sind in der DNS festgelegt und definieren grundsätzliche Anforderungen an eine nachhaltige Politik.</dd>
            <dt>Relevant</dt>
            <dd>Sie können alle zu prüfenden Elemente in der Fachanwendung als relevant oder als nicht relevant einstufen. Ein Element ist relevant für ein Regelungsvorhaben, wenn dieses Auswirkungen in nennenswertem Umfang auf das Element hat.</dd>
            <dt>Relevanz, anderweitige (Indikatorenbereich)</dt>
            <dd>Sie können angeben, dass das Regelungsvorhaben Auswirkungen auf einen Indikatorenbereich der DNS hat, auch wenn die Auswirkungen des Regelungsvorhabens nicht direkt durch einen Indikator (bei Indikatorenbereichen) erfasst werden.</dd>
            <dt>Relevanz, anderweitige (SDG)</dt>
            <dd>Sie können angeben, dass das Regelungsvorhaben Auswirkungen auf ein SDG hat, die noch nicht durch Ihre Angaben zu einem Indikator / zu einem Indikatorenbereich der DNS erfasst wurden.</dd>
            <dt>Regelungsvorhaben</dt>
            <dd>Oberbegriff für alle Entwürfe von Rechtsakten, für die Sie mithilfe der Fachanwendung eine Nachhaltigkeitsprüfung durchführen können (Gesetz, Verordnung oder Verwaltungsvorschrift des Bundes).</dd>
            <dt>Ziele für Nachhaltige Entwicklung / <span lang="en">Sustainable Development Goals</span> (SDGs)</dt>
            <dd>In der Agenda 2030 für nachhaltige Entwicklung 2015 festgelegte Zielvereinbarungen der Vereinten Nationen zur nachhaltigen Entwicklung (17 SDGs mit insgesamt 169 Unterzielen). Hinweis: Die Nummerierung der Unterziele ist nicht identisch mit den Indikatoren der DNS.</dd>
            <dt>Ziel der DNS</dt>
            <dd>Den einzelnen Indikatoren der DNS sind Ziele zugeordnet, meist für das Jahr 2030.</dd>
            <dt>Zusammenfassung</dt>
            <dd>In dieser Ansicht finden Sie alle Begründungstexte der Indikatorenbereiche, Indikatoren, SDGs und Prinzipien einer nachhaltigen Entwicklung, die Sie als relevant eingestuft haben. Auf dieser Basis können Sie für die Begründung des Regelungsentwurfs eine Zusammenfassung seiner Auswirkungen auf eine nachhaltige Entwicklung erarbeiten. Diese soll in wertender Gesamtbetrachtung eine nachvollziehbare Aussage dazu enthalten, ob die Wirkungen des Vorhabens einer nachhaltigen Entwicklung entsprechen, insbesondere welche langfristigen Wirkungen das Vorhaben hat. Hierbei sollen– soweit relevant – auch etwaige Zielkonflikte transparent dargestellt und darauf eingegangen werden, welche Schlussfolgerung hieraus für das Vorhaben gezogen wurden.</dd>
            </dl>`,
      },
    },
    btnNewProject: 'Neue Prüfung beginnen',
    btnLoadProject: 'Prüfung laden',
    newDraftTitle: 'Neue Prüfung beginnen',
    newDraftSubtitle: 'Name der Prüfung',
    btnStartProject: 'Prüfung beginnen',
    btnAbort: 'Abbrechen',
    lastModifiedTitle: 'Ihre Prüfungen',
    iconBtnDelete: 'Prüfung löschen',
    deleteConfirmation: {
      title: 'Möchten Sie die Prüfung löschen?',
      warningText: 'Achtung, wenn Sie die Prüfung löschen, ist sie nicht mehr verfügbar.',
      message: 'Die Prüfung wurde gelöscht.',
      btnDelete: 'Löschen',
      btnCancel: 'Abbrechen',
    },
    lastEdited: 'Zuletzt bearbeitet: {{date}} Uhr',
  },
  sdgGeneral: {
    drawerTextIndikatoren:
      'Weitere Informationen zu diesem Indikator einschließlich der Bewertung aus dem Indikatorenbericht finden Sie auf den Seiten des Statistischen Bundesamtes:',
    begruendungDrawerIntro: 'Bitte geben Sie Ihren Begründungstext ein.',
  },
  sdg0: {
    title: 'Ziele für Nachhaltige Entwicklung',
  },
  sdg1: {
    linkName: 'SDG 1 Keine Armut',
    title: 'SDG 1 Keine Armut',
    subtitle: 'Armut in allen ihren Formen und überall beenden',
    titleDrawer: 'SDG 1 Keine Armut',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 1 „Armut in allen ihren Formen und überall beenden“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>1.1 Bis 2030 die extreme Armut - gegenwärtig definiert als der Anteil der Menschen, die mit weniger als 1,25 Dollar pro Tag auskommen müssen - für alle Menschen überall auf der Welt beseitigen</p>
            <p>1.2 Bis 2030 den Anteil der Männer, Frauen und Kinder jeden Alters, die in Armut in all ihren Dimensionen nach der jeweiligen nationalen Definition leben, mindestens um die Hälfte senken</p>
            <p>1.3 Den nationalen Gegebenheiten entsprechende Sozialschutzsysteme und -maßnahmen für alle umsetzen, einschließlich eines Basisschutzes, und bis 2030 eine breite Versorgung der Armen und Schwachen erreichen</p>
            <p>1.4 Bis 2030 sicherstellen, dass alle Männer und Frauen, insbesondere die Armen und Schwachen, die gleichen Rechte auf wirtschaftliche Ressourcen sowie Zugang zu grundlegenden Diensten, Grundeigentum und Verfügungsgewalt über Grund und Boden und sonstigen Vermögensformen, Erbschaften, natürlichen Ressourcen, geeigneten neuen Technologien und Finanzdienstleistungen einschließlich Mikrofinanzierung haben</p>
            <p>1.5 Bis 2030 die Widerstandsfähigkeit der Armen und der Menschen in prekären Situationen erhöhen und ihre Exposition und Anfälligkeit gegenüber klimabedingten Extremereignissen und anderen wirtschaftlichen, sozialen und ökologischen Schocks und Katastrophen verringern</p>
            <p>1.a Eine erhebliche Mobilisierung von Ressourcen aus einer Vielzahl von Quellen gewährleisten, einschließlich durch verbesserte Entwicklungszusammenarbeit, um den Entwicklungsländern und insbesondere den am wenigsten entwickelten Ländern ausreichende und berechenbare Mittel für die Umsetzung von Programmen und Politiken zur Beendigung der Armut in all ihren Dimensionen bereitzustellen</p>
            <p>1.b Auf nationaler, regionaler und internationaler Ebene solide politische Rahmen auf der Grundlage armutsorientierter und geschlechtersensibler Entwicklungsstrategien schaffen, um beschleunigte Investitionen in Maßnahmen zur Beseitigung der Armut zu unterstützen</p>`,

    indikatoren: [
      {
        relevantTitle: '1.1 Armut: Armut begrenzen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
                <strong>1.1.a Materielle Deprivation</strong>
                <p>Ziel: Anteil der Personen, die materiell depriviert sind, bis 2030 deutlich unter EU-28-Wert halten</p>
                <strong>1.1.b Erhebliche materielle Deprivation</strong>
                <p>Ziel: Anteil der Personen, die erheblich materiell depriviert sind, bis 2030 deutlich unter EU-28-Wert halten</p>`,
        begruendungen: [
          {
            title: '1.1.a Materielle Deprivation',
            drawerLink: 'https://dns-indikatoren.de/1-1-ab/',
            text: 'Ziel: Anteil der Personen, die materiell depriviert sind, bis 2030 deutlich unter EU-28-Wert halten',
            summaryLabel: 'Begründung zum Indikator 1.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Materielle Deprivation (Indikator 1.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Materielle Deprivation (Indikator 1.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Materielle Deprivation (Indikator 1.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '1.1.b Erhebliche materielle Deprivation',
            drawerLink: 'https://dns-indikatoren.de/1-1-ab/',
            text: 'Ziel: Anteil der Personen, die erheblich materiell depriviert sind, bis 2030 deutlich unter EU-28-Wert halten',
            summaryLabel: 'Begründung zum Indikator 1.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Erhebliche materielle Deprivation (Indikator 1.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Erhebliche materielle Deprivation (Indikator 1.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Erhebliche materielle Deprivation (Indikator 1.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Armut',
      },
    ],
  },
  sdg2: {
    linkName: 'SDG 2 Kein Hunger',
    title: 'SDG 2 Kein Hunger',
    subtitle:
      'Den Hunger beenden, Ernährungssicherheit und eine bessere Ernährung erreichen und eine nachhaltige Landwirtschaft fördern',
    titleDrawer: 'SDG 2 Kein Hunger',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 2 „Den Hunger beenden, Ernährungssicherheit und eine bessere Ernährung erreichen und eine nachhaltige Landwirtschaft fördern“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>2.1 Bis 2030 den Hunger beenden und sicherstellen, dass alle Menschen, insbesondere die Armen und Menschen in prekären Situationen, einschließlich Kleinkindern, ganzjährig Zugang zu sicheren, nährstoffreichen und ausreichenden Nahrungsmitteln haben</p>
            <p>2.2 Bis 2030 alle Formen der Mangelernährung beenden, einschließlich durch Erreichung der international vereinbarten Zielvorgaben in Bezug auf Wachstumshemmung und Auszehrung bei Kindern unter 5 Jahren bis 2025, und den Ernährungsbedürfnissen von heranwachsenden Mädchen, schwangeren und stillenden Frauen und älteren Menschen Rechnung tragen</p>
            <p>2.3 Bis 2030 die landwirtschaftliche Produktivität und die Einkommen von kleinen Nahrungsmittelproduzenten, insbesondere von Frauen, Angehörigen indigener Völker, land-wirtschaftlichen Familienbetrieben, Weidetierhaltern und Fischern, verdoppeln, unter anderem durch den sicheren und gleichberechtigten Zugang zu Grund und Boden, anderen Produktionsressourcen und Betriebsmitteln, Wissen, Finanzdienstleistungen, Märkten so-wie Möglichkeiten für Wertschöpfung und außerlandwirtschaftliche Beschäftigung</p>
            <p>2.4 Bis 2030 die Nachhaltigkeit der Systeme der Nahrungsmittelproduktion sicherstellen und resiliente landwirtschaftliche Methoden anwenden, die die Produktivität und den Er-trag steigern, zur Erhaltung der Ökosysteme beitragen, die Anpassungsfähigkeit an Klimaänderungen, extreme Wetterereignisse, Dürren, Überschwemmungen und andere Katastrophen erhöhen und die Flächen- und Bodenqualität schrittweise verbessern</p>
            <p>2.5 Bis 2020 die genetische Vielfalt von Saatgut, Kulturpflanzen sowie Nutz- und Haustieren und ihren wildlebenden Artverwandten bewahren, unter anderem durch gut verwaltete und diversifizierte Saatgut- und Pflanzenbanken auf nationaler, regionaler und internationaler Ebene, und den Zugang zu den Vorteilen aus der Nutzung der genetischen Ressourcen und des damit verbundenen traditionellen Wissens sowie die ausgewogene und gerechte Aufteilung dieser Vorteile fördern, wie auf internationaler Ebene vereinbart</p>
            <p>2.a Die Investitionen in die ländliche Infrastruktur, die Agrarforschung und landwirtschaftliche Beratungsdienste, die Technologieentwicklung sowie Genbanken für Pflanzen und Nutztiere erhöhen, unter anderem durch verstärkte internationale Zusammenarbeit, um die landwirtschaftliche Produktionskapazität in den Entwicklungsländern und insbesondere den am wenigsten entwickelten Ländern zu verbessern</p>
            <p>2.b Handelsbeschränkungen und -verzerrungen auf den globalen Agrarmärkten korrigieren und verhindern, unter anderem durch die parallele Abschaffung aller Formen von Agrarexportsubventionen und aller Exportmaßnahmen mit gleicher Wirkung im Einklang mit dem Mandat der Doha-Entwicklungsrunde</p>
            <p>2.c Maßnahmen zur Gewährleistung des reibungslosen Funktionierens der Märkte für Nahrungsmittelrohstoffe und ihre Derivate ergreifen und den raschen Zugang zu Marktinformationen, unter anderem über Nahrungsmittelreserven, erleichtern, um zur Begrenzung der extremen Schwankungen der Nahrungsmittelpreise beizutragen</p>`,
    indikatoren: [
      {
        relevantTitle: '2.1 Landbewirtschaftung: In unseren Kulturlandschaften umweltverträglich produzieren',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>2.1.a Stickstoffüberschuss der Landwirtschaft</strong>
              <p>Ziel: Verringerung der Stickstoffüberschüsse der Gesamtbilanz für Deutschland auf 70 Kilogramm je Hektar landwirtschaftlich genutzter Fläche im Jahresmittel 2028 – 2032</p>
              <strong>2.1.b Ökologischer Landbau</strong>
              <p>Ziel: Erhöhung des Anteils des ökologischen Landbaus an der landwirtschaftlich genutzten Fläche auf 20 % bis 2030</p>`,
        begruendungen: [
          {
            title: '2.1.a Stickstoffüberschuss der Landwirtschaft',
            drawerLink: 'https://dns-indikatoren.de/2-1-a/',
            text: 'Ziel: Verringerung der Stickstoffüberschüsse der Gesamtbilanz für Deutschland auf 70 Kilogramm je Hektar landwirtschaftlich genutzter Fläche im Jahresmittel 2028 – 2032',
            summaryLabel: 'Begründung zum Indikator 2.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Stickstoffüberschuss der Landwirtschaft (Indikator 2.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Stickstoffüberschuss (Indikator 2.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Stickstoffüberschuss der Landwirtschaft (Indikator 2.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '2.1.b Ökologischer Landbau',
            drawerLink: 'https://dns-indikatoren.de/2-1-b/',
            text: 'Ziel: Erhöhung des Anteils des ökologischen Landbaus an der landwirtschaftlich genutzten Fläche auf 20 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 2.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Ökologischer Landbau (Indikator 2.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Ökologischer Landbau (Indikator 2.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Ökologischer Landbau (Indikator 2.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Landbewirtschaftung',
      },
      {
        relevantTitle: '2.2 Ernährungssicherung: Das Recht auf Nahrung weltweit verwirklichen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>2.2.a Unterstützung guter Regierungsführung bei der Erreichung einer angemessenen Ernährung weltweit</strong>
              <p>Ziel: Angemessene Steigerung des Anteils der ausgezahlten Mittel für die Anwendung von Leitlinien und Empfehlungen des VN-Welternährungsausschusses (CFS) an den Gesamtausgaben für Ernährungssicherung in % bis 2030</p>`,
        begruendungen: [
          {
            title:
              '2.2.a Unterstützung guter Regierungsführung bei der Erreichung einer angemessenen Ernährung weltweit',
            drawerLink: 'https://dns-indikatoren.de/2-2/',
            text: 'Ziel: Angemessene Steigerung des Anteils der ausgezahlten Mittel für die Anwendung von Leitlinien und Empfehlungen des VN-Welternährungsausschusses (CFS) an den Gesamtausgaben für Ernährungssicherung in % bis 2030',
            summaryLabel: 'Begründung zum Indikator 2.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich 2.2.a Unterstützung guter Regierungsführung bei der Erreichung einer angemessenen Ernährung weltweit (Indikator 2.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Unterstützung guter Regierungsführung (Indikator 2.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Unterstützung guter Regierungsführung bei der Erreichung einer angemessenen Ernährung weltweit (Indikator 2.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Ernährungssicherung',
      },
    ],
  },
  sdg3: {
    linkName: 'SDG 3 Gesundheit und Wohlergehen',
    title: 'SDG 3 Gesundheit und Wohlergehen',
    subtitle: 'Ein gesundes Leben für alle Menschen jeden Alters gewährleisten und ihr Wohlergehen fördern',
    titleDrawer: 'SDG 3 Gesundheit und Wohlergehen',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 3 „Ein gesundes Leben für alle Menschen jeden Alters gewährleisten und ihr Wohlergehen fördern“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>3.1 Bis 2030 die weltweite Müttersterblichkeit auf unter 70 je 100.000 Lebendgeburten senken</p>
            <p>3.2 Bis 2030 den vermeidbaren Todesfällen bei Neugeborenen und Kindern unter 5 Jahren ein Ende setzen, mit dem von allen Ländern zu verfolgenden Ziel, die Sterblichkeit bei Neugeborenen mindestens auf 12 je 1.000 Lebendgeburten und bei Kindern unter 5 Jahren mindestens auf 25 je 1.000 Lebendgeburten zu senken</p>
            <p>3.3 Bis 2030 die Aids-, Tuberkulose- und Malariaepidemien und die vernachlässigten Tropenkrankheiten beseitigen und Hepatitis, durch Wasser übertragene Krankheiten und andere übertragbare Krankheiten bekämpfen</p>
            <p>3.4 Bis 2030 die Frühsterblichkeit aufgrund von nichtübertragbaren Krankheiten durch Prävention und Behandlung um ein Drittel senken und die psychische Gesundheit und das Wohlergehen fördern</p>
            <p>3.5 Die Prävention und Behandlung des Substanzmissbrauchs, namentlich des Suchtstoffmissbrauchs und des schädlichen Gebrauchs von Alkohol, verstärken</p>
            <p>3.6 Bis 2020 die Zahl der Todesfälle und Verletzungen infolge von Verkehrsunfällen weltweit halbieren</p>
            <p>3.7 Bis 2030 den allgemeinen Zugang zu sexual- und reproduktionsmedizinischer Versorgung, einschließlich Familienplanung, Information und Aufklärung, und die Einbeziehung der reproduktiven Gesundheit in nationale Strategien und Programme gewährleisten</p>
            <p>3.8 Die allgemeine Gesundheitsversorgung, einschließlich der Absicherung gegen finanzielle Risiken, den Zugang zu hochwertigen grundlegenden Gesundheitsdiensten und den Zugang zu sicheren, wirksamen, hochwertigen und bezahlbaren unentbehrlichen Arznei-mitteln und Impfstoffen für alle erreichen<p>
            <p>3.9 Bis 2030 die Zahl der Todesfälle und Erkrankungen aufgrund gefährlicher Chemikalien und der Verschmutzung und Verunreinigung von Luft, Wasser und Boden erheblich verringern<p>
            <p>3.a Die Durchführung des Rahmenübereinkommens der Weltgesundheitsorganisation zur Eindämmung des Tabakgebrauchs in allen Ländern nach Bedarf stärken<p>
            <p>3.b Forschung und Entwicklung zu Impfstoffen und Medikamenten für übertragbare und nichtübertragbare Krankheiten, von denen hauptsächlich Entwicklungsländer betroffen sind, unterstützen, den Zugang zu bezahlbaren unentbehrlichen Arzneimitteln und Impfstoffen gewährleisten, im Einklang mit der Erklärung von Doha über das TRIPS-Übereinkommen und die öffentliche Gesundheit, die das Recht der Entwicklungsländer bekräftigt, die Bestimmungen in dem Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums über Flexibilitäten zum Schutz der öffentlichen Gesundheit voll auszuschöpfen, und insbesondere den Zugang zu Medikamenten für alle zu gewährleisten<p>
            <p>3.c Die Gesundheitsfinanzierung und die Rekrutierung, Aus- und Weiterbildung und Bindung von Gesundheitsfachkräften in den Entwicklungsländern und insbesondere in den am wenigsten entwickelten Ländern und den kleinen Inselentwicklungsländern deutlich erhöhen<p>
            <p>3.d Die Kapazitäten aller Länder, insbesondere der Entwicklungsländer, in den Bereichen Frühwarnung, Risikominderung und Management nationaler und globaler Gesundheitsrisiken stärken<p>`,
    indikatoren: [
      {
        relevantTitle: '3.1 Gesundheit und Ernährung: Länger gesund leben',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>3.1.a Vorzeitige Sterblichkeit (Frauen)</strong>
              <p>Ziel: Senkung auf 100 Todesfälle je 100.000 Einwohner (Frauen) bis 2030</p>
              <strong>3.1.b Vorzeitige Sterblichkeit (Männer)</strong>
              <p>Ziel: Senkung auf 190 Todesfälle je 100.000 Einwohner (Männer) bis 2030</p>
              <strong>3.1.c Raucherquote von Jugendlichen</strong>
              <p>Ziel: Senkung auf 7 % bis 2030</p>
              <strong>3.1.d Raucherquote von Erwachsenen</strong>
              <p>Ziel: Senkung auf 19 % bis 2030</p>
              <strong>3.1.e Adipositasquote von Kindern und Jugendlichen</strong>
              <p>Ziel: Anstieg dauerhaft stoppen</p>
              <strong>3.1.f Adipositasquote von Erwachsenen</strong>
              <p>Ziel: Anstieg dauerhaft stoppen</p>
              `,
        begruendungen: [
          {
            title: '3.1.a Vorzeitige Sterblichkeit (Frauen)',
            drawerLink: 'https://dns-indikatoren.de/3-1-ab/',
            text: 'Ziel: Senkung auf 100 Todesfälle je 100.000 Einwohner (Frauen) bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Vorzeitige Sterblichkeit (Frauen) (Indikator 3.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Vorzeitige Sterblichkeit (Frauen) (Indikator 3.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Vorzeitige Sterblichkeit (Frauen) (Indikator 3.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.1.b Vorzeitige Sterblichkeit (Männer)',
            drawerLink: 'https://dns-indikatoren.de/3-1-ab/',
            text: 'Ziel: Senkung auf 190 Todesfälle je 100.000 Einwohner (Männer) bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Vorzeitige Sterblichkeit (Männer) (Indikator 3.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Vorzeitige Sterblichkeit (Männer) (Indikator 3.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Vorzeitige Sterblichkeit (Männer) (Indikator 3.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.1.c Raucherquote von Jugendlichen',
            drawerLink: 'https://dns-indikatoren.de/3-1-cd/',
            text: 'Ziel: Senkung auf 7 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.1.c',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Raucherquote von Jugendlichen (Indikator 3.1.c) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Raucherquote von Jugendlichen (Indikator 3.1.c) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Raucherquote von Jugendlichen (Indikator 3.1.c) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.1.d Raucherquote von Erwachsenen',
            drawerLink: 'https://dns-indikatoren.de/3-1-cd/',
            text: 'Ziel: Senkung auf 19 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.1.d',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Raucherquote von Erwachsenen (Indikator 3.1.d) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Raucherquote von Erwachsenen (Indikator 3.1.d) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Raucherquote von Erwachsenen (Indikator 3.1.d) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.1.e Adipositasquote von Kindern und Jugendlichen',
            drawerLink: 'https://dns-indikatoren.de/3-1-e/',
            text: 'Ziel: Anstieg dauerhaft stoppen',
            summaryLabel: 'Begründung zum Indikator 3.1.e',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Adipositasquote von Kindern und Jugendlichen (Indikator 3.1.e) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Adipositasquote von Kindern und Jugendlichen (Indikator 3.1.e) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Adipositasquote von Kindern und Jugendlichen (Indikator 3.1.e) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.1.f Adipositasquote von Erwachsenen',
            drawerLink: 'https://dns-indikatoren.de/3-1-f/',
            text: 'Ziel: Anstieg dauerhaft stoppen',
            summaryLabel: 'Begründung zum Indikator 3.1.f',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Adipositasquote von Erwachsenen (Indikator 3.1.f) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Adipositasquote von Erwachsenen (Indikator 3.1.f) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Adipositasquote von Erwachsenen (Indikator 3.1.f) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Gesundheit und Wohlergehen',
      },
      {
        relevantTitle: '3.2 Luftbelastung: Gesunde Umwelt erhalten',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>3.2.a Emissionen von Luftschadstoffen</strong>
              <p>Ziel: Reduktion der Emissionen des Jahres 2005 auf 55 % (ungewichtetes Mittel der fünf Schadstoffe) bis 2030</p>
              <strong>3.2.b Anteil der Bevölkerung mit erhöhter PM10-Feinstaubexposition</strong>
              <p>Ziel: Erreichung des Feinstaub WHO-Richtwerts von 20 Mikrogramm/Kubikmeter für PM10 im Jahresmittel möglichst flächendeckend bis 2030</p>`,
        begruendungen: [
          {
            title: '3.2.a Emissionen von Luftschadstoffen',
            drawerLink: 'https://dns-indikatoren.de/3-2-a/',
            text: 'Ziel: Reduktion der Emissionen des Jahres 2005 auf 55 % (ungewichtetes Mittel der fünf Schadstoffe) bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Emissionen von Luftschadstoffen (Indikator 3.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Emissionen von Luftschadstoffen (Indikator 3.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Emissionen von Luftschadstoffen (Indikator 3.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '3.2.b Anteil der Bevölkerung mit erhöhter PM10-Feinstaubexposition',
            drawerLink: 'https://dns-indikatoren.de/3-2-b/',
            text: 'Ziel: Erreichung des Feinstaub WHO-Richtwerts von 20 Mikrogramm/Kubikmeter für PM10 im Jahresmittel möglichst flächendeckend bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil der Bevölkerung mit erhöhter PM10-Feinstaubexposition (Indikator 3.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil der Bevölkerung mit erhöhter PM10-Feinstaubexposition (Indikator 3.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil der Bevölkerung mit erhöhter PM10-Feinstaubexposition (Indikator 3.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Luftbelastung',
      },
      {
        relevantTitle: '3.3. Globale Gesundheit: Globale Gesundheitsarchitektur stärken',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>3.3.a Beitrag Deutschlands zur globalen Pandemieprävention und -reaktion</strong>
              <p>Ziel: Steigerung der Ausgaben bis 2030</p>`,
        begruendungen: [
          {
            title: '3.3.a Beitrag Deutschlands zur globalen Pandemieprävention und -reaktion',
            drawerLink: 'https://dns-indikatoren.de/3-3/',
            text: 'Ziel: Steigerung der Ausgaben bis 2030',
            summaryLabel: 'Begründung zum Indikator 3.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Beitrag Deutschlands zur globalen Pandemieprävention und -reaktion (Indikator 3.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Beitrag Deutschlands zur globalen Pandemieprävention und -reaktion (Indikator 3.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Beitrag Deutschlands zur globalen Pandemieprävention und -reaktion (Indikator 3.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Globale Gesundheit',
      },
    ],
  },
  sdg4: {
    linkName: 'SDG 4 Hochwertige Bildung',
    title: 'SDG 4 Hochwertige Bildung',
    subtitle: 'Ein gesundes Leben für alle Menschen jeden Alters gewährleisten und ihr Wohlergehen fördern',
    titleDrawer: 'SDG 4 Hochwertige Bildung',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 4 „Inklusive, gerechte und hochwertige Bildung gewährleisten und Möglichkeiten des lebenslangen Lernens für alle fördern“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>4.1 Bis 2030 sicherstellen, dass alle Mädchen und Jungen gleichberechtigt eine kostenlose und hochwertige Grund- und Sekundarschulbildung abschließen, die zu brauchbaren und effektiven Lernergebnissen führt</p>
            <p>4.2 Bis 2030 sicherstellen, dass alle Mädchen und Jungen Zugang zu hochwertiger frühkindlicher Erziehung, Betreuung und Vorschulbildung erhalten, damit sie auf die Grundschule vorbereitet sind</p>
            <p>4.3 Bis 2030 den gleichberechtigten Zugang aller Frauen und Männer zu einer erschwinglichen und hochwertigen fachlichen, beruflichen und tertiären Bildung einschließlich universitärer Bildung gewährleisten</p>
            <p>4.4 Bis 2030 die Zahl der Jugendlichen und Erwachsenen wesentlich erhöhen, die über die entsprechenden Qualifikationen einschließlich fachlicher und beruflicher Qualifikationen für eine Beschäftigung, eine menschenwürdige Arbeit und Unternehmertum verfügen</p>
            <p>4.5 Bis 2030 geschlechtsspezifische Disparitäten in der Bildung beseitigen und den gleichberechtigen Zugang der Schwachen in der Gesellschaft, namentlich von Menschen mit Behinderungen, Angehörigen indigener Völker und Kindern in prekären Situationen, zu allen Bildungs- und Ausbildungsebenen gewährleisten</p>
            <p>4.6 Bis 2030 sicherstellen, dass alle Jugendlichen und ein erheblicher Anteil der männlichen und weiblichen Erwachsenen lesen, schreiben und rechnen lernen</p>
            <p>4.7 Bis 2030 sicherstellen, dass alle Lernenden die notwendigen Kenntnisse und Qualifikationen zur Förderung nachhaltiger Entwicklung erwerben, unter anderem durch Bildung für nachhaltige Entwicklung und nachhaltige Lebensweisen, Menschenrechte, Geschlechtergleichstellung, eine Kultur des Friedens und der Gewaltlosigkeit, Weltbürgerschaft und die Wertschätzung kultureller Vielfalt und des Beitrags der Kultur zu nachhaltiger Entwicklung</p>
            <p>4.a Bildungseinrichtungen bauen und ausbauen, die kinder-, behinderten- und geschlechtergerecht sind und eine sichere, gewaltfreie, inklusive und effektive Lernumgebung für alle bieten</p>
            <p>4.b Bis 2020 weltweit die Zahl der verfügbaren Stipendien für Entwicklungsländer, ins-besondere für die am wenigsten entwickelten Länder, die kleinen Inselentwicklungsländer und die afrikanischen Länder, zum Besuch einer Hochschule, einschließlich zur Berufsbildung und zu Informations- und Kommunikationstechnik-, Technik-, Ingenieurs- und Wissenschaftsprogrammen, in entwickelten Ländern und in anderen Entwicklungsländern wesentlich erhöhen</p>
            <p>4.c Bis 2030 das Angebot an qualifizierten Lehrkräften unter anderem durch internationale Zusammenarbeit im Bereich der Lehrerausbildung in den Entwicklungsländern und insbesondere in den am wenigsten entwickelten Ländern und kleinen Inselentwicklungsländern wesentlich erhöhen</p>
            `,
    indikatoren: [
      {
        relevantTitle: '4.1 Bildung: Bildung und Qualifikation kontinuierlich verbessern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>4.1.a Frühe Schulabgängerinnen und Schulabgänger</strong>
              <p>Ziel: Verringerung des Anteils auf 9,5 % bis 2030</p>
              <strong>4.1.b Akademisch Qualifizierte und beruflich Höherqualifizierte (30- bis 34-Jährige mit tertiärem oder postsekundarem nichttertiärem Abschluss)</strong>
              <p>Ziel: Steigerung des Anteils auf 55 % bis 2030</p>`,
        begruendungen: [
          {
            title: '4.1.a Frühe Schulabgängerinnen und Schulabgänger',
            drawerLink: 'https://dns-indikatoren.de/4-1-a/',
            text: 'Ziel: Verringerung des Anteils auf 9,5 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 4.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Frühe Schulabgängerinnen und Schulabgänger (Indikator 4.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Frühe Schulabgängerinnen und Schulabgänger (Indikator 4.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Frühe Schulabgängerinnen und Schulabgänger (Indikator 4.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '4.1.b Akademisch Qualifizierte und beruflich Höherqualifizierte (30- bis 34-Jährige mit tertiärem oder postsekundarem nichttertiärem Abschluss)',
            drawerLink: 'https://dns-indikatoren.de/4-1-b/',
            text: 'Ziel: Steigerung des Anteils auf 55 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 4.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Akademisch Qualifizierte und beruflich Höherqualifizierte (30- bis 34-Jährige mit tertiärem oder postsekundarem nichttertiärem Abschluss) (Indikator 4.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Akademisch Qualifizierte und beruflich Höherqualifizierte (30- bis 34-Jährige mit tertiärem oder postsekundarem nichttertiärem Abschluss) (Indikator 4.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Akademisch Qualifizierte und beruflich Höherqualifizierte (30- bis 34-Jährige mit tertiärem oder postsekundarem nichttertiärem Abschluss) (Indikator 4.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Bildung',
      },
      {
        relevantTitle: '4.2 Perspektiven für Familien: Vereinbarkeit von Familie und Beruf verbessern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>4.2.a Ganztagsbetreuung für Kinder 0- bis 2-Jährige</strong>
              <p>Ziel: Anstieg auf 35 % bis 2030</p>
              <strong>4.2.b Ganztagsbetreuung für Kinder 3- bis 5-Jährige</strong>
              <p>Ziel: Anstieg auf 60 % bis 2020 und auf 70 % bis 2030</p>`,
        begruendungen: [
          {
            title: '4.2.a Ganztagsbetreuung für Kinder 0- bis 2-Jährige',
            drawerLink: 'https://dns-indikatoren.de/4-2-ab/',
            text: 'Ziel: Anstieg auf 35 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 4.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Ganztagsbetreuung für Kinder 0- bis 2-Jährige (Indikator 4.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Ganztagsbetreuung für Kinder 0- bis 2-Jährige (Indikator 4.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Ganztagsbetreuung für Kinder 0- bis 2-Jährige (Indikator 4.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '4.2.b Ganztagsbetreuung für Kinder 3- bis 5-Jährige',
            drawerLink: 'https://dns-indikatoren.de/4-2-ab/',
            text: 'Ziel: Anstieg auf 60 % bis 2020 und auf 70 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 4.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Ganztagsbetreuung für Kinder 3- bis 5-Jährige (Indikator 4.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Ganztagsbetreuung für Kinder 3- bis 5-Jährige (Indikator 4.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Ganztagsbetreuung für Kinder 3- bis 5-Jährige (Indikator 4.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Perspektiven für Familien',
      },
    ],
  },
  sdg5: {
    linkName: 'SDG 5 Geschlechtergleichstellung',
    title: 'SDG 5 Geschlechtergleichstellung',
    subtitle: 'Geschlechtergleichstellung erreichen und alle Frauen und Mädchen zur Selbstbestimmung befähigen',
    titleDrawer: 'SDG 5 Geschlechtergleichstellung',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 5 „Geschlechtergleichstellung erreichen und alle Frauen und Mädchen zur Selbstbestimmung befähigen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>5.1 Alle Formen der Diskriminierung von Frauen und Mädchen überall auf der Welt beenden</p>
            <p>5.2 Alle Formen von Gewalt gegen alle Frauen und Mädchen im öffentlichen und im privaten Bereich einschließlich des Menschenhandels und sexueller und anderer Formen der Ausbeutung beseitigen</p>
            <p>5.3 Alle schädlichen Praktiken wie Kinderheirat, Frühverheiratung und Zwangsheirat sowie die Genitalverstümmelung bei Frauen und Mädchen beseitigen</p>
            <p>5.4 Unbezahlte Pflege- und Hausarbeit durch die Bereitstellung öffentlicher Dienstleistungen und Infrastrukturen, Sozialschutzmaßnahmen und die Förderung geteilter Verantwortung innerhalb des Haushalts und der Familie entsprechend den nationalen Gegebenheiten anerkennen und wertschätzen</p>
            <p>5.5 Die volle und wirksame Teilhabe von Frauen und ihre Chancengleichheit bei der Übernahme von Führungsrollen auf allen Ebenen der Entscheidungsfindung im politischen, wirtschaftlichen und öffentlichen Leben sicherstellen</p>
            <p>5.6 Den allgemeinen Zugang zu sexueller und reproduktiver Gesundheit und reproduktiven Rechten gewährleisten, wie im Einklang mit dem Aktionsprogramm der Internationalen Konferenz über Bevölkerung und Entwicklung, der Aktionsplattform von Beijing und den Ergebnisdokumenten ihrer Überprüfungskonferenzen vereinbart</p>
            <p>5.a Reformen durchführen, um Frauen die gleichen Rechte auf wirtschaftliche Ressourcen sowie Zugang zu Grundeigentum und zur Verfügungsgewalt über Grund und Boden und sonstige Vermögensformen, zu Finanzdienstleistungen, Erbschaften und natürlichen Ressourcen zu verschaffen, im Einklang mit den nationalen Rechtsvorschriften</p>
            <p>5.b Die Nutzung von Grundlagentechnologien, insbesondere der Informations- und Kommunikationstechnologien, verbessern, um die Selbstbestimmung der Frauen zu fördern</p>
            <p>5.c Eine solide Politik und durchsetzbare Rechtsvorschriften zur Förderung der Gleichstellung der Geschlechter und der Selbstbestimmung aller Frauen und Mädchen auf allen Ebenen beschließen und verstärken</p>`,
    indikatoren: [
      {
        relevantTitle: '5.1 Gleichstellung: Gleichstellung und partnerschaftliche Aufgabenteilung fördern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>5.1.a Verdienstabstand zwischen Frauen und Männern</strong>
              <p>Ziel: Verringerung des Abstandes auf 10 % bis 2020, Beibehaltung bis 2030</p>
              <strong>5.1.b Frauen in Führungspositionen in der Wirtschaft</strong>
              <p>Ziel: 30 % Frauen in Aufsichtsräten der börsennotierten und paritätisch mitbestimmten Unternehmen bis 2030.</p>
              <strong>5.1.c Frauen in Führungspositionen im öffentlichen Dienst des Bundes</strong>
              <p>Ziel: Gleichberechtigte Teilhabe von Frauen und Männern in Leitungsfunktionen des öffentlichen Dienstes bis 2025.</p>
              <strong>5.1.d Väterbeteiligung beim Elterngeld</strong>
              <p>Ziel: 65 % bis 2030</p>
              <p><strong>Wirtschaftliche Teilhabe von Frauen global stärken</strong></p>
              <strong>5.1.e Berufliche Qualifizierung von Frauen und Mädchen durch deutsche entwicklungspolitische Zusammenarbeit</strong>
              <p>Ziel: Sukzessive Steigerung bis 2030 um ein Drittel verglichen mit Basisjahr 2015</p>`,
        begruendungen: [
          {
            title: '5.1.a Verdienstabstand zwischen Frauen und Männer',
            drawerLink: 'https://dns-indikatoren.de/5-1-a/',
            text: 'Ziel: Verringerung des Abstandes auf 10 % bis 2020, Beibehaltung bis 2030',
            summaryLabel: 'Begründung zum Indikator 5.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Verdienstabstand zwischen Frauen und Männern (Indikator 5.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Verdienstabstand zwischen Frauen und Männern (Indikator 5.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Verdienstabstand zwischen Frauen und Männern (Indikator 5.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '5.1.b Frauen in Führungspositionen in der Wirtschaft',
            drawerLink: 'https://dns-indikatoren.de/5-1-bc/',
            text: 'Ziel: 30 % Frauen in Aufsichtsräten der börsennotierten und paritätisch mitbestimmten Unternehmen bis 2030.',
            summaryLabel: 'Begründung zum Indikator 5.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Frauen in Führungspositionen in der Wirtschaft (Indikator 5.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Frauen in Führungspositionen in der Wirtschaft (Indikator 5.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Frauen in Führungspositionen in der Wirtschaft (Indikator 5.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '5.1.c Frauen in Führungspositionen im öffentlichen Dienst des Bundes',
            drawerLink: 'https://dns-indikatoren.de/5-1-bc/',
            text: 'Ziel: Gleichberechtigte Teilhabe von Frauen und Männern in Leitungsfunktionen des öffentlichen Dienstes bis 2025.',
            summaryLabel: 'Begründung zum Indikator 5.1.c',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Frauen in Führungspositionen im öffentlichen Dienst des Bundes (Indikator 5.1.c) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Frauen in Führungspositionen im öffentlichen Dienst des Bundes (Indikator 5.1.c) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Frauen in Führungspositionen im öffentlichen Dienst des Bundes (Indikator 5.1.c) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '5.1.d Väterbeteiligung beim Elterngeld',
            drawerLink: 'https://dns-indikatoren.de/5-1-d/',
            text: 'Ziel: 65 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 5.1.d',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Väterbeteiligung beim Elterngeld (Indikator 5.1.d) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Väterbeteiligung beim Elterngeld (Indikator 5.1.d) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Väterbeteiligung beim Elterngeld (Indikator 5.1.d) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '5.1.e Berufliche Qualifizierung von Frauen und Mädchen durch deutsche entwicklungspolitische Zusammenarbeit',
            drawerLink: 'https://dns-indikatoren.de/5-1-e/',
            text: 'Ziel: Sukzessive Steigerung bis 2030 um ein Drittel verglichen mit Basisjahr 2015',
            summaryLabel: 'Begründung zum Indikator 5.1.e',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Berufliche Qualifizierung von Frauen und Mädchen durch deutsche entwicklungspolitische Zusammenarbeit (Indikator 5.1.e) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Berufliche Qualifizierung von Frauen und Mädchen durch deutsche entwicklungspolitische Zusammenarbeit (Indikator 5.1.e) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Berufliche Qualifizierung von Frauen und Mädchen durch deutsche entwicklungspolitische Zusammenarbeit (Indikator 5.1.e) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Gleichstellung',
      },
    ],
  },
  sdg6: {
    linkName: 'SDG 6 Sauberes Wasser und Sanitäreinrichtungen',
    title: 'SDG 6 Sauberes Wasser und Sanitäreinrichtungen',
    subtitle: 'Verfügbarkeit und nachhaltige Bewirtschaftung von Wasser und Sanitärversorgung für alle gewährleisten',
    titleDrawer: 'SDG 6 Sauberes Wasser und Sanitäreinrichtungen',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 6 „Verfügbarkeit und nachhaltige Bewirtschaftung von Wasser und Sanitärversorgung für alle gewährleisten“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>6.1 Bis 2030 den allgemeinen und gerechten Zugang zu einwandfreiem und bezahlbarem Trinkwasser für alle erreichen</p>
            <p>6.2 Bis 2030 den Zugang zu einer angemessenen und gerechten Sanitärversorgung und Hygiene für alle erreichen und der Notdurftverrichtung im Freien ein Ende setzen, unter besonderer Beachtung der Bedürfnisse von Frauen und Mädchen und von Menschen in prekären Situationen</p>
            <p>6.3 Bis 2030 die Wasserqualität durch Verringerung der Verschmutzung, Beendigung des Einbringens und Minimierung der Freisetzung gefährlicher Chemikalien und Stoffe, Halbierung des Anteils unbehandelten Abwassers und eine beträchtliche Steigerung der Wiederaufbereitung und gefahrlosen Wiederverwendung weltweit verbessern</p>
            <p>6.4 Bis 2030 die Effizienz der Wassernutzung in allen Sektoren wesentlich steigern und eine nachhaltige Entnahme und Bereitstellung von Süßwasser gewährleisten, um der Wasserknappheit zu begegnen und die Zahl der unter Wasserknappheit leidenden Menschen erheblich zu verringern</p>
            <p>6.5 Bis 2030 auf allen Ebenen eine integrierte Bewirtschaftung der Wasserressourcen umsetzen, gegebenenfalls auch mittels grenzüberschreitender Zusammenarbeit</p>
            <p>6.6 Bis 2020 wasserverbundene Ökosysteme schützen und wiederherstellen, darunter Berge, Wälder, Feuchtgebiete, Flüsse, Grundwasserleiter und Seen</p>
            <p>6.a Bis 2030 die internationale Zusammenarbeit und die Unterstützung der Entwicklungsländer beim Kapazitätsaufbau für Aktivitäten und Programme im Bereich der Wasser- und Sanitärversorgung ausbauen, einschließlich der Wassersammlung und -speicherung, Entsalzung, effizienten Wassernutzung, Abwasserbehandlung, Wiederaufbereitungs - und Wiederverwendungstechnologien</p>
            <p>6.b Die Mitwirkung lokaler Gemeinwesen an der Verbesserung der Wasserbewirtschaftung und der Sanitärversorgung unterstützen und verstärken</p>
            `,
    indikatoren: [
      {
        relevantTitle: '6.1 Gewässerqualität: Minderung der stofflichen Belastung von Gewässern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>6.1.a Phosphor in Fließgewässern</strong>
              <p>Ziel: Einhaltung oder Unterschreitung der gewässertypischen Orientierungswerte an allen Messstellen bis 2030</p>
              <strong>6.1.b Nitrat im Grundwasser</strong>
              <p>Ziel: Einhaltung des Nitrat Schwellenwertes von 50 Milligramm pro Liter an allen Messstellen bis 2030</p>`,
        begruendungen: [
          {
            title: '6.1.a Phosphor in Fließgewässern',
            drawerLink: 'https://dns-indikatoren.de/6-1-a/',
            text: 'Ziel: Einhaltung oder Unterschreitung der gewässertypischen Orientierungswerte an allen Messstellen bis 2030',
            summaryLabel: 'Begründung zum Indikator 6.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Phosphor in Fließgewässern (Indikator 6.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Phosphor in Fließgewässern (Indikator 6.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Phosphor in Fließgewässern (Indikator 6.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '6.1.b Nitrat im Grundwasser',
            drawerLink: 'https://dns-indikatoren.de/6-1-b/',
            text: 'Ziel: Einhaltung des Nitrat Schwellenwertes von 50 Milligramm pro Liter an allen Messstellen bis 2030',
            summaryLabel: 'Begründung zum Indikator 6.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Nitrat im Grundwasser (Indikator 6.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Nitrat im Grundwasser (Indikator 6.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Nitrat im Grundwasser (Indikator 6.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Gewässerqualität',
      },
      {
        relevantTitle:
          '6.2 Trinkwasser- und Sanitärversorgung: Besserer Zugang zu Trinkwasser und Sanitärversorgung weltweit, höhere (sichere) Qualität',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>6.2.a Anzahl der Menschen, die einen neuen oder hochwertigeren Zugang zur Trinkwasserversorgung durch deutsche Unterstützung erhalten</strong>
              <p>Ziel: 6 Millionen Menschen pro Jahr bis 2030</p>
              <strong>6.2.b Anzahl der Menschen, die einen neuen oder verbesserten Anschluss zur Sanitärversorgung durch deutsche Unterstützung erhalten</strong>
              <p>Ziel: 4 Millionen Menschen pro Jahr bis 2030</p>`,
        begruendungen: [
          {
            title:
              '6.2.a Anzahl der Menschen, die einen neuen oder hochwertigeren Zugang zur Trinkwasserversorgung durch deutsche Unterstützung erhalten',
            drawerLink: '	https://dns-indikatoren.de/6-2-ab/',
            text: 'Ziel: 6 Millionen Menschen pro Jahr bis 2030',
            summaryLabel: 'Begründung zum Indikator 6.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anzahl der Menschen, die einen neuen oder hochwertigeren Zugang zur Trinkwasserversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anzahl der Menschen, die einen neuen oder hochwertigeren Zugang zur Trinkwasserversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anzahl der Menschen, die einen neuen oder hochwertigeren Zugang zur Trinkwasserversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '6.2.b Anzahl der Menschen, die einen neuen oder verbesserten Anschluss zur Sanitärversorgung durch deutsche Unterstützung erhalten',
            drawerLink: '	https://dns-indikatoren.de/6-2-ab/',
            text: 'Ziel: 4 Millionen Menschen pro Jahr bis 2030',
            summaryLabel: 'Begründung zum Indikator 6.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anzahl der Menschen, die einen neuen oder verbesserten Anschluss zur Sanitärversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anzahl der Menschen, die einen neuen oder verbesserten Anschluss zur Sanitärversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anzahl der Menschen, die einen neuen oder verbesserten Anschluss zur Sanitärversorgung durch deutsche Unterstützung erhalten (Indikator 6.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Trinkwasser- und Sanitärversorgung',
      },
    ],
  },
  sdg7: {
    linkName: 'SDG 7 Bezahlbare und saubere Energie',
    title: 'SDG 7 Bezahlbare und saubere Energie',
    subtitle: 'Zugang zu bezahlbarer, verlässlicher, nachhaltiger und zeitgemäßer Energie für alle sichern',
    titleDrawer: 'SDG 7 Bezahlbare und saubere Energie',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 7 „Zugang zu bezahlbarer, verlässlicher, nachhaltiger und moderner Energie für alle sichern“"</p>
            <strong>Inhaltliche Ziele</strong>
            <p>7.1 Bis 2030 den allgemeinen Zugang zu bezahlbaren, verlässlichen und modernen Energiedienstleistungen sichern</p>
            <p>7.2 Bis 2030 den Anteil erneuerbarer Energie am globalen Energiemix deutlich erhöhen</p>
            <p>7.3 Bis 2030 die weltweite Steigerungsrate der Energieeffizienz verdoppeln</p>
            <p>7.a Bis 2030 die internationale Zusammenarbeit verstärken, um den Zugang zur Forschung und Technologie im Bereich saubere Energie, namentlich erneuerbare Energie, Energieeffizienz sowie fortschrittliche und saubere Technologien für fossile Brennstoffe, zu erleichtern, und Investitionen in die Energieinfrastruktur und saubere Energietechnologien fördern</p>
            <p>7.b Bis 2030 die Infrastruktur ausbauen und die Technologie modernisieren, um in den Entwicklungsländern und insbesondere in den am wenigsten entwickelten Ländern, den kleinen Inselentwicklungsländern und den Binnenentwicklungsländern im Einklang mit ihren jeweiligen Unterstützungsprogrammen moderne und nachhaltige Energiedienstleistungen für alle bereitzustellen</p>`,
    indikatoren: [
      {
        relevantTitle: '7.1 Ressourcenschonung: Ressourcen sparsam und effizient nutzen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>7.1.a Endenergieproduktivität</strong>
              <p>Ziel: Steigerung um 2,1 % pro Jahr im Zeitraum von 2008 – 20500</p>
              <strong>7.1.b Primärenergieverbrauch</strong>
              <p>Ziel: Senkung um 20 % bis 2020, um 30 % bis 2030 und um 50 % bis 2050 jeweils gegenüber 2008</p>`,
        begruendungen: [
          {
            title: '7.1.a Endenergieproduktivität',
            drawerLink: 'https://dns-indikatoren.de/7-1-ab/',
            text: 'Ziel: Steigerung um 2,1 % pro Jahr im Zeitraum von 2008 – 2050',
            summaryLabel: 'Begründung zum Indikator 7.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Endenergieproduktivität (Indikator 7.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Endenergieproduktivität (Indikator 7.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Endenergieproduktivität (Indikator 7.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '7.1.b Primärenergieverbrauch',
            drawerLink: 'https://dns-indikatoren.de/7-1-ab/',
            text: 'Ziel: Senkung um 20 % bis 2020, um 30 % bis 2030 und um 50 % bis 2050 jeweils gegenüber 2008',
            summaryLabel: 'Begründung zum Indikator 7.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Primärenergieverbrauch (Indikator 7.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Primärenergieverbrauch (Indikator 7.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Primärenergieverbrauch (Indikator 7.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Ressourcenschonung',
      },
      {
        relevantTitle: '7.2 Erneuerbare Energien: Zukunftsfähige Energieversorgung ausbauen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>7.2.a Anteil erneuerbarer Energien am Brutto-Endenergieverbrauch</strong>
              <p>Ziel: Anstieg auf 18 % bis 2020, auf 30 % bis 2030, auf 45 % bis 2040 und auf 60 % bis 2050</p>
              <strong>7.2.b Anteil des Stroms aus erneuerbaren Energiequellen am Bruttostromverbrauch</strong>
              <p>Ziel: Anstieg auf mindestens 35 % bis 2020, 65 % bis 2030 und Treibhausgasneutralität des in Deutschland erzeugten und verbrauchten Stroms bis 2050</p>`,
        begruendungen: [
          {
            title: '7.2.a Anteil erneuerbarer Energien am Brutto-Endenergieverbrauch',
            drawerLink: 'https://dns-indikatoren.de/7-2-a/',
            text: 'Ziel: Anstieg auf 18 % bis 2020, auf 30 % bis 2030, auf 45 % bis 2040 und auf 60 % bis 2050',
            summaryLabel: 'Begründung zum Indikator 7.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil erneuerbarer Energien am Brutto-Endenergieverbrauch (Indikator 7.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil erneuerbarer Energien am Brutto-Endenergieverbrauch (Indikator 7.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil erneuerbarer Energien am Brutto-Endenergieverbrauch (Indikator 7.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '7.2.b Anteil des Stroms aus erneuerbaren Energiequellen am Bruttostromverbrauch',
            drawerLink: 'https://dns-indikatoren.de/7-2-b/',
            text: 'Ziel: Anstieg auf mindestens 35 % bis 2020, 65 % bis 2030 und Treibhausgasneutralität des in Deutschland erzeugten und verbrauchten Stroms bis 2050',
            summaryLabel: 'Begründung zum Indikator 7.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil des Stroms aus erneuerbaren Energiequellen am Bruttostromverbrauch (Indikator 7.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil des Stroms aus erneuerbaren Energiequellen am Bruttostromverbrauch (Indikator 7.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil des Stroms aus erneuerbaren Energiequellen am Bruttostromverbrauch (Indikator 7.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Erneuerbare Energien',
      },
    ],
  },
  sdg8: {
    linkName: 'SDG 8 Menschenwürdige Arbeit und Wirtschaftswachstum',
    title: 'SDG 8 Menschenwürdige Arbeit und Wirtschaftswachstum',
    subtitle:
      'Dauerhaftes, inklusives und nachhaltiges Wirtschaftswachstum, produktive Vollbeschäftigung und menschenwürdige Arbeit für alle fördern',
    titleDrawer: 'SDG 8 Menschenwürdige Arbeit und Wirtschaftswachstum',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 8 „Dauerhaftes, breitenwirksames und nachhaltiges Wirtschaftswachstum, produktive Vollbeschäftigung und menschenwürdige Arbeit für alle fördern“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>8.1 Ein Pro-Kopf-Wirtschaftswachstum entsprechend den nationalen Gegebenheiten und insbesondere ein jährliches Wachstum des Bruttoinlandsprodukts von mindestens 7 Prozent in den am wenigsten entwickelten Ländern aufrechterhalten</p>
            <p>8.2 Eine höhere wirtschaftliche Produktivität durch Diversifizierung, technologische Modernisierung und Innovation erreichen, einschließlich durch Konzentration auf mit hoher Wertschöpfung verbundene und arbeitsintensive Sektoren</p>
            <p>8.3 Entwicklungsorientierte Politiken fördern, die produktive Tätigkeiten, die Schaffung menschenwürdiger Arbeitsplätze, Unternehmertum, Kreativität und Innovation unterstützen, und die Formalisierung und das Wachstum von Kleinst-, Klein- und Mittelunternehmen unter anderem durch den Zugang zu Finanzdienstleistungen begünstigen</p>
            <p>8.4 Bis 2030 die weltweite Ressourceneffizienz in Konsum und Produktion Schritt für Schritt verbessern und die Entkopplung von Wirtschaftswachstum und Umweltzerstörung anstreben, im Einklang mit dem Zehnjahres-Programmrahmen für nachhaltige Konsum- und Produktionsmuster, wobei die entwickelten Länder die Führung übernehmen</p>
            <p>8.5 Bis 2030 produktive Vollbeschäftigung und menschenwürdige Arbeit für alle Frauen und Männer, einschließlich junger Menschen und Menschen mit Behinderungen, sowie gleiches Entgelt für gleichwertige Arbeit erreichen</p>
            <p>8.6 Bis 2020 den Anteil junger Menschen, die ohne Beschäftigung sind und keine Schul- oder Berufsausbildung durchlaufen, erheblich verringern</p>
            <p>8.7 Sofortige und wirksame Maßnahmen ergreifen, um Zwangsarbeit abzuschaffen, moderne Sklaverei und Menschenhandel zu beenden und das Verbot und die Beseitigung der schlimmsten Formen der Kinderarbeit, einschließlich der Einziehung und des Einsatzes von Kindersoldaten, sicherstellen und bis 2025 jeder Form von Kinderarbeit ein Ende setzen</p>
            <p>8.8 Die Arbeitsrechte schützen und sichere Arbeitsumgebungen für alle Arbeitnehmer, einschließlich der Wanderarbeitnehmer, insbesondere der Wanderarbeitnehmerinnen, und der Menschen in prekären Beschäftigungsverhältnissen, fördern</p>
            <p>8.9 Bis 2030 Politiken zur Förderung eines nachhaltigen Tourismus erarbeiten und umsetzen, der Arbeitsplätze schafft und die lokale Kultur und lokale Produkte fördert</p>
            <p>8.10 Die Kapazitäten der nationalen Finanzinstitutionen stärken, um den Zugang zu Bank-, Versicherungs- und Finanzdienstleistungen für alle zu begünstigen und zu erweitern</p>
            <p>8.a Die im Rahmen der Handelshilfe gewährte Unterstützung für die Entwicklungsländer und insbesondere die am wenigsten entwickelten Länder erhöhen, unter anderem durch den Erweiterten integrierten Rahmenplan für handelsbezogene technische Hilfe für die am wenigsten entwickelten Länder</p>
            <p>8.b Bis 2020 eine globale Strategie für Jugendbeschäftigung erarbeiten und auf den Weg bringen und den Globalen Beschäftigungspakt der Internationalen Arbeitsorganisation umsetzen</p>`,
    indikatoren: [
      {
        relevantTitle: '8.1 Ressourcenschonung: Ressourcen sparsam und effizient nutzen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.1.a Gesamtrohstoffproduktivität</strong>
              <p>Ziel: Beibehaltung des Trends der Jahre 2000 – 2010 bis 2030</p>`,
        begruendungen: [
          {
            title: '8.1.a Gesamtrohstoffproduktivität',
            drawerLink: 'https://dns-indikatoren.de/8-1/',
            text: 'Ziel: Beibehaltung des Trends der Jahre 2000 – 2010 bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Gesamtrohstoffproduktivität (Indikator 8.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Gesamtrohstoffproduktivität (Indikator 8.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Gesamtrohstoffproduktivität (Indikator 8.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Ressourcenschonung',
      },
      {
        relevantTitle: '8.2 Staatsverschuldung: Staatsfinanzen konsolidieren – Generationengerechtigkeit schaffen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.2.a Staatsdefizit</strong>
              <p>Ziel: Jährliches Staatsdefizit kleiner als 3 % des BIP, Beibehaltung bis 2030</p>
              <strong>8.2.b Strukturelles Defizit</strong>
              <p>Ziel: Strukturell ausgeglichener Staatshaushalt, gesamtstaatliches strukturelles Defizit von max. 0,5 % des BIP, Beibehaltung bis 2030</p>
              <strong>8.2.c Schuldenstand</strong>
              <p>Ziel: Schuldenstandsquote max. 60 % des BIP, Beibehaltung bis 2030</p>`,
        begruendungen: [
          {
            title: '8.2.a Staatsdefizit',
            drawerLink: 'https://dns-indikatoren.de/8-2-ab/',
            text: 'Ziel: Jährliches Staatsdefizit kleiner als 3 % des BIP, Beibehaltung bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Staatsdefizit (Indikator 8.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Staatsdefizit (Indikator 8.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Staatsdefizit (Indikator 8.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '8.2.b Strukturelles Defizit',
            drawerLink: 'https://dns-indikatoren.de/8-2-ab/',
            text: 'Ziel: Strukturell ausgeglichener Staatshaushalt, gesamtstaatliches strukturelles Defizit von max. 0,5 % des BIP, Beibehaltung bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Strukturelles Defizit (Indikator 8.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Strukturelles Defizit (Indikator 8.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Strukturelles Defizit (Indikator 8.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '8.2.c Schuldenstand',
            drawerLink: '	https://dns-indikatoren.de/8-2-c/',
            text: 'Ziel: Schuldenstandsquote max. 60 % des BIP, Beibehaltung bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.2.c',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Schuldenstand (Indikator 8.2.c) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Schuldenstand (Indikator 8.2.c) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Schuldenstand (Indikator 8.2.c) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Staatsverschuldung',
      },
      {
        relevantTitle:
          '8.3 Wirtschaftliche Zukunftsvorsorge: Gute Investitionsbedingungen schaffen – Wohlstand dauerhaft erhalten',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.3.a Verhältnis der Bruttoanlageinvestitionen zum BIP</strong>
              <p>Ziel: Angemessene Entwicklung des Anteils, Beibehaltung bis 2030</p>`,
        begruendungen: [
          {
            title: '8.3.a Verhältnis der Bruttoanlageinvestitionen zum BIP',
            drawerLink: 'https://dns-indikatoren.de/8-3/',
            text: 'Ziel: Angemessene Entwicklung des Anteils, Beibehaltung bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Verhältnis der Bruttoanlageinvestitionen zum BIP (Indikator 8.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Verhältnis der Bruttoanlageinvestitionen zum BIP (Indikator 8.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Verhältnis der Bruttoanlageinvestitionen zum BIP (Indikator 8.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Wirtschaftliche Zukunftsvorsorge',
      },
      {
        relevantTitle:
          '8.4 Wirtschaftliche Leistungsfähigkeit: Wirtschaftsleistung umwelt- und sozialverträglich steigern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.4.a Bruttoinlandsprodukt je Einwohner</strong>
              <p>Ziel: Stetiges und angemessenes Wirtschaftswachstum</p>`,
        begruendungen: [
          {
            title: '8.4.a Bruttoinlandsprodukt je Einwohner',
            drawerLink: 'https://dns-indikatoren.de/8-4/',
            text: 'Ziel: Stetiges und angemessenes Wirtschaftswachstum',
            summaryLabel: 'Begründung zum Indikator 8.4.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Bruttoinlandsprodukt je Einwohner (Indikator 8.4.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Bruttoinlandsprodukt je Einwohner (Indikator 8.4.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Bruttoinlandsprodukt je Einwohner (Indikator 8.4.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Wirtschaftliche Leistungsfähigkeit',
      },
      {
        relevantTitle: '8.5 Beschäftigung: Beschäftigungsniveau steigern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.5.a Erwerbstätigenquote insgesamt (20 bis 64 Jahre)</strong>
              <p>Ziel: Erhöhung auf 78 % bis 2030</p>
              <strong>8.5.b Erwerbstätigenquote Ältere (60 bis 64 Jahre)</strong>
              <p>Ziel: Erhöhung auf 60 % bis 2030</p>`,
        begruendungen: [
          {
            title: '8.5.a Erwerbstätigenquote insgesamt (20 bis 64 Jahre)',
            drawerLink: 'https://dns-indikatoren.de/8-5-ab/',
            text: 'Ziel: Erhöhung auf 78 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.5.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Erwerbstätigenquote insgesamt (20 bis 64 Jahre) (Indikator 8.5.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Erwerbstätigenquote insgesamt (20 bis 64 Jahre) (Indikator 8.5.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Erwerbstätigenquote insgesamt (20 bis 64 Jahre) (Indikator 8.5.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '8.5.b Erwerbstätigenquote Ältere (60 bis 64 Jahre)',
            drawerLink: 'https://dns-indikatoren.de/8-5-ab/',
            text: 'Ziel: Erhöhung auf 60 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.5.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Erwerbstätigenquote Ältere (60 bis 64 Jahre) (Indikator 8.5.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Erwerbstätigenquote Ältere (60 bis 64 Jahre) (Indikator 8.5.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Erwerbstätigenquote Ältere (60 bis 64 Jahre) (Indikator 8.5.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Beschäftigung',
      },
      {
        relevantTitle: '8.6 Globale Lieferketten: Menschenwürdige Arbeit weltweit ermöglichen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>8.6.a Mitglieder des Textilbündnisses</strong>
              <p>Ziel: Signifikante Steigerung bis 2030</p>`,
        begruendungen: [
          {
            title: '8.6.a Mitglieder des Textilbündnisses',
            drawerLink: 'https://dns-indikatoren.de/8-6/',
            text: 'Ziel: Signifikante Steigerung bis 2030',
            summaryLabel: 'Begründung zum Indikator 8.6.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Mitglieder des Textilbündnisses (Indikator 8.6.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Mitglieder des Textilbündnisses (Indikator 8.6.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Mitglieder des Textilbündnisses (Indikator 8.6.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Globale Lieferketten',
      },
    ],
  },
  sdg9: {
    linkName: 'SDG 9 Industrie, Innovationen und Infrastruktur',
    title: 'SDG 9 Industrie, Innovationen und Infrastruktur',
    subtitle:
      'Eine belastbare Infrastruktur aufbauen, inklusive und nachhaltige Industrialisierung fördern und Innovationen unterstützen',
    titleDrawer: 'SDG 9 Industrie, Innovationen und Infrastruktur',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 9 „Eine widerstandsfähige Infrastruktur aufbauen, breitenwirksame und nachhaltige Industrialisierung fördern und Innovationen unterstützen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>9.1 Eine hochwertige, verlässliche, nachhaltige und widerstandsfähige Infrastruktur aufbauen, einschließlich regionaler und grenzüberschreitender Infrastruktur, um die wirtschaftliche Entwicklung und das menschliche Wohlergehen zu unterstützen, und dabei den Schwerpunkt auf einen erschwinglichen und gleichberechtigten Zugang für alle legen</p>
            <p>9.2 Eine breitenwirksame und nachhaltige Industrialisierung fördern und bis 2030 den Anteil der Industrie an der Beschäftigung und am Bruttoinlandsprodukt entsprechend den nationalen Gegebenheiten erheblich steigern und den Anteil in den am wenigsten entwickelten Ländern verdoppeln</p>
            <p>9.3 Insbesondere in den Entwicklungsländern den Zugang kleiner Industrie- und anderer Unternehmen zu Finanzdienstleistungen, einschließlich bezahlbaren Krediten, und ihre Einbindung in Wertschöpfungsketten und Märkte erhöhen</p>
            <p>9.4 Bis 2030 die Infrastruktur modernisieren und die Industrien nachrüsten, um sie nachhaltig zu machen, mit effizienterem Ressourceneinsatz und unter vermehrter Nutzung sauberer und umweltverträglicher Technologien und Industrieprozesse, wobei alle Länder Maßnahmen entsprechend ihren jeweiligen Kapazitäten ergreifen</p>
            <p>9.5 Die wissenschaftliche Forschung verbessern und die technologischen Kapazitäten der Industriesektoren in allen Ländern und insbesondere in den Entwicklungsländern ausbauen und zu diesem Zweck bis 2030 unter anderem Innovationen fördern und die Anzahl der im Bereich Forschung und Entwicklung tätigen Personen je 1 Million Menschen sowie die öffentlichen und privaten Ausgaben für Forschung und Entwicklung beträchtlich erhöhen</p>
            <p>9.a Die Entwicklung einer nachhaltigen und widerstandsfähigen Infrastruktur in den Entwicklungsländern durch eine verstärkte finanzielle, technologische und technische Unterstützung der afrikanischen Länder, der am wenigsten entwickelten Länder, der Binnenentwicklungsländer und der kleinen Inselentwicklungsländer erleichtern</p>
            <p>9.b Die einheimische Technologieentwicklung, Forschung und Innovation in den Entwicklungsländern unterstützen, einschließlich durch Sicherstellung eines förderlichen politischen Umfelds, unter anderem für industrielle Diversifizierung und Wertschöpfung im Rohstoffbereich</p>
            <p>9.c Den Zugang zur Informations- und Kommunikationstechnologie erheblich erweitern sowie anstreben, in den am wenigsten entwickelten Ländern bis 2020 einen allgemeinen und erschwinglichen Zugang zum Internet bereitzustellen</p>`,
    indikatoren: [
      {
        relevantTitle: '9.1 Innovation: Zukunft mit neuen Lösungen nachhaltig gestalten',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>9.1.a Private und öffentliche Ausgaben für Forschung und Entwicklung</strong>
              <p>Ziel: Jährlich mindestens 3,5 % des BIP bis 2025</p>
              <strong>9.1.b Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung</strong>
              <p>Ziel: Flächendeckender Aufbau von Gigabit-netzen bis 2025.</p>`,
        begruendungen: [
          {
            title: '9.1.a Private und öffentliche Ausgaben für Forschung und Entwicklung',
            drawerLink: 'https://dns-indikatoren.de/9-1-a/',
            text: 'Ziel: Jährlich mindestens 3,5 % des BIP bis 2025',
            summaryLabel: 'Begründung zum Indikator 9.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Private und öffentliche Ausgaben für Forschung und Entwicklung (Indikator 9.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Private und öffentliche Ausgaben für Forschung und Entwicklung (Indikator 9.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Private und öffentliche Ausgaben für Forschung und Entwicklung (Indikator 9.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '9.1.b Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung',
            drawerLink: 'https://dns-indikatoren.de/9-1-b/',
            text: 'Ziel: Flächendeckender Aufbau von Gigabit-netzen bis 2025.',
            summaryLabel: 'Begründung zum Indikator 9.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung (Indikator 9.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung (Indikator 9.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Breitbandausbau – Anteil der Haushalte mit Zugang zu Gigabit-Breitbandversorgung (Indikator 9.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Innovation',
      },
    ],
  },
  sdg10: {
    linkName: 'SDG 10 Weniger Ungleichheiten',
    title: 'SDG 10 Weniger Ungleichheiten',
    subtitle: 'Ungleichheit innerhalb von und zwischen Staaten verringern',
    titleDrawer: 'SDG 10 Weniger Ungleichheiten',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 10 „Ungleichheit in und zwischen Ländern verringern“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>10.1 Bis 2030 nach und nach ein über dem nationalen Durchschnitt liegendes Einkommenswachstum der ärmsten 40 Prozent der Bevölkerung erreichen und aufrechterhalten</p>
            <p>10.2 Bis 2030 alle Menschen unabhängig von Alter, Geschlecht, Behinderung, Rasse, Ethnizität, Herkunft, Religion oder wirtschaftlichem oder sonstigem Status zu Selbstbestimmung befähigen und ihre soziale, wirtschaftliche und politische Inklusion fördern</p>
            <p>10.3 Chancengleichheit gewährleisten und Ungleichheit der Ergebnisse reduzieren, namentlich durch die Abschaffung diskriminierender Gesetze, Politiken und Praktiken und die Förderung geeigneter gesetzgeberischer, politischer und sonstiger Maßnahmen in dieser Hinsicht</p>
            <p>10.4 Politische Maßnahmen beschließen, insbesondere fiskalische, lohnpolitische und den Sozialschutz betreffende Maßnahmen, und schrittweise größere Gleichheit erzielen</p>
            <p>10.5 Die Regulierung und Überwachung der globalen Finanzmärkte und -institutionen verbessern und die Anwendung der einschlägigen Vorschriften verstärken</p>
            <p>10.6 Eine bessere Vertretung und verstärkte Mitsprache der Entwicklungsländer bei der Entscheidungsfindung in den globalen internationalen Wirtschafts- und Finanzinstitutionen sicherstellen, um die Wirksamkeit, Glaubwürdigkeit, Rechenschaftslegung und Legitimation dieser Institutionen zu erhöhen</p>
            <p>10.7 Eine geordnete, sichere, reguläre und verantwortungsvolle Migration und Mobilität von Menschen erleichtern, unter anderem durch die Anwendung einer planvollen und gut gesteuerten Migrationspolitik</p>
            <p>10.a Den Grundsatz der besonderen und differenzierten Behandlung der Entwicklungsländer, insbesondere der am wenigsten entwickelten Länder, im Einklang mit den Übereinkünften der Welthandelsorganisation anwenden</p>
            <p>10.b Öffentliche Entwicklungshilfe und Finanzströme einschließlich ausländischer Direktinvestitionen in die Staaten fördern, in denen der Bedarf am größten ist, insbesondere in die am wenigsten entwickelten Länder, die afrikanischen Länder, die kleinen Inselentwicklungsländer und die Binnenentwicklungsländer, im Einklang mit ihren jeweiligen nationalen Plänen und Programmen</p>
            <p>10.c Bis 2030 die Transaktionskosten für Heimatüberweisungen von Migrantinnen und Migranten auf weniger als 3 Prozent senken und Überweisungskorridore mit Kosten von über 5 Prozent beseitigen</p>`,
    indikatoren: [
      {
        relevantTitle:
          '10.1 Gleiche Bildungschancen: Schulische Bildungserfolge von Ausländern in Deutschland verbessern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>10.1.a Ausländische Schulabsolventinnen und Schulabsolventen</strong>
              <p>Ziel: Erhöhung des Anteils der ausländischen Schulabgänger mit mindestens Hauptschulabschluss und Angleichung an die Quote deutscher Schulabgänger bis 2030</p>`,
        begruendungen: [
          {
            title: '10.1.a Ausländische Schulabsolventinnen und Schulabsolventen',
            drawerLink: 'https://dns-indikatoren.de/10-1/',
            text: 'Ziel: Erhöhung des Anteils der ausländischen Schulabgänger mit mindestens Hauptschulabschluss und Angleichung an die Quote deutscher Schulabgänger bis 2030',
            summaryLabel: 'Begründung zum Indikator 10.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Ausländische Schulabsolventen und Schulabsolventinnen (Indikator 10.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Ausländische Schulabsolventen und Schulabsolventinnen (Indikator 10.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Ausländische Schulabsolventen und Schulabsolventinnen (Indikator 10.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Gleiche Bildungschancen',
      },
      {
        relevantTitle: '10.2 Verteilungsgerechtigkeit: Zu große Ungleichheit innerhalb Deutschlands verhindern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>10.2.a Gini-Koeffizient des Einkommens nach Sozialtransfers</strong>
              <p>Ziel: Gini-Koeffizient Einkommen nach Sozialtransfer bis 2030 unterhalb des EU-28-Wertes</p>`,
        begruendungen: [
          {
            title: '10.2.a Gini-Koeffizient des Einkommens nach Sozialtransfers',
            drawerLink: 'https://dns-indikatoren.de/10-2/',
            text: 'Ziel: Gini-Koeffizient Einkommen nach Sozialtransfer bis 2030 unterhalb des EU-28-Wertes',
            summaryLabel: 'Begründung zum Indikator 10.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich GINI-Koeffizient Einkommen nach Sozialtransfer (Indikator 10.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich GINI-Koeffizient Einkommen nach Sozialtransfer (Indikator 10.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich GINI-Koeffizient Einkommen nach Sozialtransfer (Indikator 10.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Verteilungsgerechtigkeit',
      },
    ],
  },
  sdg11: {
    linkName: 'SDG 11 Nachhaltige Städte und Gemeinden',
    title: 'SDG 11 Nachhaltige Städte und Gemeinden',
    subtitle: 'Städte und Siedlungen inklusiv, sicher, widerstandsfähig und nachhaltig gestalten',
    titleDrawer: 'SDG 11 Nachhaltige Städte und Gemeinden',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 11 „Städte und Siedlungen inklusiv, sicher, widerstandsfähig und nachhaltig gestalten“/p>
            <strong>Inhaltliche Ziele</strong>
            <p>11.1 Bis 2030 den Zugang zu angemessenem, sicherem und bezahlbarem Wohnraum und zur Grundversorgung für alle sicherstellen und Slums sanieren</p>
            <p>11.2 Bis 2030 den Zugang zu sicheren, bezahlbaren, zugänglichen und nachhaltigen Verkehrssystemen für alle ermöglichen und die Sicherheit im Straßenverkehr verbessern, insbesondere durch den Ausbau des öffentlichen Verkehrs, mit besonderem Augenmerk auf den Bedürfnissen von Menschen in prekären Situationen, Frauen, Kindern, Menschen mit Behinderungen und älteren Menschen</p>
            <p>11.3 Bis 2030 die Verstädterung inklusiver und nachhaltiger gestalten und die Kapazitäten für eine partizipatorische, integrierte und nachhaltige Siedlungsplanung und -steuerung in allen Ländern verstärken</p>
            <p>11.4 Die Anstrengungen zum Schutz und zur Wahrung des Weltkultur- und -naturerbes verstärken</p>
            <p>11.5 Bis 2030 die Zahl der durch Katastrophen, einschließlich Wasserkatastrophen, bedingten Todesfälle und der davon betroffenen Menschen deutlich reduzieren und die dadurch verursachten unmittelbaren wirtschaftlichen Verluste im Verhältnis zum globalen Bruttoinlandsprodukt wesentlich verringern, mit Schwerpunkt auf dem Schutz der Armen und von Menschen in prekären Situationen</p>
            <p>11.6 Bis 2030 die von den Städten ausgehende Umweltbelastung pro Kopf senken, unter anderem mit besonderer Aufmerksamkeit auf der Luftqualität und der kommunalen und sonstigen Abfallbehandlung</p>
            <p>11.7 Bis 2030 den allgemeinen Zugang zu sicheren, inklusiven und zugänglichen Grünflächen und öffentlichen Räumen gewährleisten, insbesondere für Frauen und Kinder, ältere Menschen und Menschen mit Behinderungen</p>
            <p>11.a Durch eine verstärkte nationale und regionale Entwicklungsplanung positive wirtschaftliche, soziale und ökologische Verbindungen zwischen städtischen, stadtnahen und ländlichen Gebieten unterstützen</p>
            <p>11.b Bis 2020 die Zahl der Städte und Siedlungen, die integrierte Politiken und Pläne zur Förderung der Inklusion, der Ressourceneffizienz, der Abschwächung des Klimawandels, der Klimaanpassung und der Widerstandsfähigkeit gegenüber Katastrophen beschließen und umsetzen, wesentlich erhöhen und gemäß dem Sendai-Rahmen für Katastrophenvorsorge 2015-2030 ein ganzheitliches Katastrophenrisikomanagement auf allen Ebenen entwickeln und umsetzen</p>
            <p>11.c Die am wenigsten entwickelten Länder unter anderem durch finanzielle und technische Hilfe beim Bau nachhaltiger und widerstandsfähiger Gebäude unter Nutzung einheimischer Materialien unterstützen</p>`,
    indikatoren: [
      {
        relevantTitle: '11.1 Flächeninanspruchnahme: Flächen nachhaltig nutzen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>11.1.a Anstieg der Siedlungs- und Verkehrsfläche in ha pro Tag</strong>
              <p>Ziel: Senkung auf durchschnittlich unter 30 ha pro Tag bis 2030</p>
              <strong>11.1.b Freiraumverlust</strong>
              <p>Ziel: Verringerung des einwohnerbezogenen Freiflächenverlustes</p>
              <strong>11.1.c Siedlungsdichte</strong>
              <p>Ziel: Keine Verringerung der Siedlungsdichte</p>`,
        begruendungen: [
          {
            title: '11.1.a Anstieg der Siedlungs- und Verkehrsfläche in ha pro Tag',
            drawerLink: 'https://dns-indikatoren.de/11-1-a/',
            text: 'Ziel: Senkung auf durchschnittlich unter 30 ha pro Tag bis 2030',
            summaryLabel: 'Begründung zum Indikator 11.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anstieg der Siedlungs- und Verkehrsfläche in ha pro Tag (Indikator 11.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anstieg der Siedlungs- und Verkehrsfläche in ha pro Tag (Indikator 11.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anstieg der Siedlungs- und Verkehrsfläche in ha pro Tag (Indikator 11.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '11.1.b Freiraumverlust',
            drawerLink: 'https://dns-indikatoren.de/11-1-b/',
            text: 'Ziel: Verringerung des einwohnerbezogenen Freiflächenverlustes',
            summaryLabel: 'Begründung zum Indikator 11.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich (Indikator 11.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Freiraumverlust (Indikator 11.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Freiraumverlust (Indikator 11.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '11.1.c Siedlungsdichte',
            drawerLink: '	https://dns-indikatoren.de/11-1-c/',
            text: 'Ziel: Keine Verringerung der Siedlungsdichte',
            summaryLabel: 'Begründung zum Indikator 11.1.c',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Siedlungsdichte (Indikator 11.1.c) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Siedlungsdichte (Indikator 11.1.c) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Siedlungsdichte (Indikator 11.1.c) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Flächeninanspruchnahme',
      },
      {
        relevantTitle: '11.2 Mobilität: Mobilität sichern – Umwelt schonen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>11.2.a Endenergieverbrauch im Güterverkehr</strong>
              <p>Ziel: Senkung um 15 bis 20 % bis 2030</p>
              <strong>11.2.b Endenergieverbrauch im Personenverkehr</strong>
              <p>Ziel: Senkung um 15 bis 20 % bis 2030</p>
              <strong>11.2.c Erreichbarkeit von Mittel- und Oberzentren mit öffentlichen Verkehrsmitteln</strong>
              <p>Ziel: Verringerung der durchschnittlichen Reisezeit mit öffentlichen Verkehrsmitteln</p>`,
        begruendungen: [
          {
            title: '11.2.a Endenergieverbrauch im Güterverkehr',
            drawerLink: 'https://dns-indikatoren.de/11-2-a/',
            text: 'Ziel: Senkung um 15 bis 20 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 11.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Endenergieverbrauch im Güterverkehr (Indikator 11.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Endenergieverbrauch im Güterverkehr (Indikator 11.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Endenergieverbrauch im Güterverkehr (Indikator 11.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '11.2.b Endenergieverbrauch im Personenverkehr',
            drawerLink: 'https://dns-indikatoren.de/11-2-b/',
            text: 'Ziel: Senkung um 15 bis 20 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 11.2.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Endenergieverbrauch im Personenverkehr (Indikator 11.2.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Endenergieverbrauch im Personenverkehr (Indikator 11.2.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Endenergieverbrauch im Personenverkehr (Indikator 11.2.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '11.2.c Erreichbarkeit von Mittel- und Oberzentren mit öffentlichen Verkehrsmitteln',
            drawerLink: 'https://dns-indikatoren.de/11-2-c/',
            text: 'Ziel: Verringerung der durchschnittlichen Reisezeit mit öffentlichen Verkehrsmitteln',
            summaryLabel: 'Begründung zum Indikator 11.2.c',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Erreichbarkeit von Mittel- und Oberzentren mit öffentlichen Verkehrsmitteln (Indikator 11.2.c) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Erreichbarkeit von Mittel- und Oberzentren mit öffentlichen Verkehrsmitteln (Indikator 11.2.c) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Erreichbarkeit von Mittel- und Oberzentren mit öffentlichen Verkehrsmitteln (Indikator 11.2.c) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Mobilität',
      },
      {
        relevantTitle: '11.3 Wohnen: Bezahlbarer Wohnraum für alle',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>11.3.a Überlastung durch Wohnkosten</strong>
              <p>Ziel: Senkung des Anteils der überlasteten Personen an der Bevölkerung auf 13 % bis 2030</p>`,
        begruendungen: [
          {
            title: '11.3.a Überlastung durch Wohnkosten',
            drawerLink: 'https://dns-indikatoren.de/11-3/',
            text: 'Ziel: Senkung des Anteils der überlasteten Personen an der Bevölkerung auf 13 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 11.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Überlastung durch Wohnkosten (Indikator 11.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Überlastung durch Wohnkosten (Indikator 11.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Überlastung durch Wohnkosten (Indikator 11.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Wohnen',
      },
      {
        relevantTitle: '11.4 Kulturerbe: Zugang zum Kulturerbe verbessern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>11.4.a Zahl der Objekte in der Deutschen Digitalen Bibliothek</strong>
              <p>Ziel: Steigerung der Zahl der in der Deutschen Digitalen Bibliothek vernetzten Objekte auf 50 Millionen bis 2030</p>`,
        begruendungen: [
          {
            title: '11.4.a Zahl der Objekte in der Deutschen Digitalen Bibliothek',
            drawerLink: 'https://dns-indikatoren.de/11-4/',
            text: 'Ziel: Steigerung der Zahl der in der Deutschen Digitalen Bibliothek vernetzten Objekte auf 50 Millionen bis 2030',
            summaryLabel: 'Begründung zum Indikator 11.4.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Zahl der Objekte in der Deutschen Digitalen Bibliothek (Indikator 11.4.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Zahl der Objekte in der Deutschen Digitalen Bibliothek (Indikator 11.4.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Zahl der Objekte in der Deutschen Digitalen Bibliothek (Indikator 11.4.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Kulturerbe',
      },
    ],
  },
  sdg12: {
    linkName: 'SDG 12 Nachhaltige/r Konsum und Produktion',
    title: 'SDG 12 Nachhaltige/r Konsum und Produktion',
    subtitle: 'Nachhaltige Konsum- und Produktionsmuster sicherstellen',
    titleDrawer: 'SDG 12 Nachhaltige/r Konsum und Produktion',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 12 „Nachhaltige Konsum- und Produktionsmuster sicherstellen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>12.1 Den Zehnjahres-Programmrahmen für nachhaltige Konsum- und Produktionsmuster umsetzen, wobei alle Länder, an der Spitze die entwickelten Länder, Maßnahmen ergreifen, unter Berücksichtigung des Entwicklungsstands und der Kapazitäten der Entwicklungsländer</p>
            <p>12.2 Bis 2030 die nachhaltige Bewirtschaftung und effiziente Nutzung der natürlichen Ressourcen erreichen</p>
            <p>12.3 Bis 2030 die weltweite Nahrungsmittelverschwendung pro Kopf auf Einzelhandels- und Verbraucherebene halbieren und die entlang der Produktions- und Lieferkette entstehenden Nahrungsmittelverluste einschließlich Nachernteverlusten verringern</p>
            <p>12.4 Bis 2020 einen umweltverträglichen Umgang mit Chemikalien und allen Abfällen während ihres gesamten Lebenszyklus in Übereinstimmung mit den vereinbarten internationalen Rahmenregelungen erreichen und ihre Freisetzung in Luft, Wasser und Boden erheblich verringern, um ihre nachteiligen Auswirkungen auf die menschliche Gesundheit und die Umwelt auf ein Mindestmaß zu beschränken</p>
            <p>12.5 Bis 2030 das Abfallaufkommen durch Vermeidung, Verminderung, Wiederverwertung und Wiederverwendung deutlich verringern</p>
            <p>12.6 Die Unternehmen, insbesondere große und transnationale Unternehmen, dazu ermutigen, nachhaltige Verfahren einzuführen und in ihre Berichterstattung Nachhaltigkeitsinformationen aufzunehmen</p>
            <p>12.7 In der öffentlichen Beschaffung nachhaltige Verfahren fördern, im Einklang mit den nationalen Politiken und Prioritäten</p>
            <p>12.8 Bis 2030 sicherstellen, dass die Menschen überall über einschlägige Informationen und das Bewusstsein für nachhaltige Entwicklung und eine Lebensweise in Harmonie mit der Natur verfügen</p>
            <p>12.a Die Entwicklungsländer bei der Stärkung ihrer wissenschaftlichen und technologischen Kapazitäten im Hinblick auf den Übergang zu nachhaltigeren Konsum- und Produktionsmustern unterstützen</p>
            <p>12.b Instrumente zur Beobachtung der Auswirkungen eines nachhaltigen Tourismus, der Arbeitsplätze schafft und die lokale Kultur und lokale Produkte fördert, auf die nachhaltige Entwicklung entwickeln und anwenden</p>
            <p>12.c Die ineffiziente Subventionierung fossiler Brennstoffe, die zu verschwenderischem Verbrauch verleitet, durch Beseitigung von Marktverzerrungen entsprechend den nationalen Gegebenheiten rationalisieren, unter anderem durch eine Umstrukturierung der Besteuerung und die allmähliche Abschaffung dieser schädlichen Subventionen, um ihren Umweltauswirkungen Rechnung zu tragen, wobei die besonderen Bedürfnisse und Gegebenheiten der Entwicklungsländer in vollem Umfang berücksichtigt und die möglichen nachteiligen Auswirkungen auf ihre Entwicklung in einer die Armen und die betroffenen Gemeinwesen schützenden Weise so gering wie möglich gehalten werden</p>`,
    indikatoren: [
      {
        relevantTitle: '12.1 Nachhaltiger Konsum: Konsum umwelt- und sozialverträglich gestalten',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>12.1.a Marktanteil von Produkten mit staatlichen Umweltzeichen (perspektivisch: Marktanteil von Produkten und Dienstleistungen, die mit glaubwürdigen und anspruchsvollen Umwelt- und Sozialsiegeln ausgezeichnet sind)</strong>
              <p>Ziel: Steigerung des Marktanteils auf 34 % bis 2030</p>
              <strong>12.1.ba Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - Rohstoffeinsatz</strong>
              <p>Ziel: Kontinuierliche Reduzierung</p>
              <strong>12.1.bb Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - Energieverbrauch</strong>
              <p>Ziel: Kontinuierliche Reduzierung</p>
              <strong>12.1.bc Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - CO2-Emissionen</strong>
              <p>Ziel: Kontinuierliche Reduzierung</p>`,
        begruendungen: [
          {
            title:
              '12.1.a Marktanteil von Produkten mit staatlichen Umweltzeichen (perspektivisch: Marktanteil von Produkten und Dienstleistungen, die mit glaubwürdigen und anspruchsvollen Umwelt- und Sozialsiegeln ausgezeichnet sind)',
            drawerLink: 'https://dns-indikatoren.de/12-1-a/',
            text: 'Ziel: Steigerung des Marktanteils auf 34 % bis 2030',
            summaryLabel: 'Begründung zum Indikator 12.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Marktanteil von Produkten mit staatlichen Umweltzeichen (perspektivisch: Marktanteil von Produkten und Dienstleistungen, die mit glaubwürdigen und anspruchsvollen Umwelt- und Sozialsiegeln ausgezeichnet sind) (Indikator 12.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Marktanteil von Produkten mit staatlichen Umweltzeichen (perspektivisch: Marktanteil von Produkten und Dienstleistungen, die mit glaubwürdigen und anspruchsvollen Umwelt- und Sozialsiegeln ausgezeichnet sind) (Indikator 12.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Marktanteil von Produkten mit staatlichen Umweltzeichen (perspektivisch: Marktanteil von Produkten und Dienstleistungen, die mit glaubwürdigen und anspruchsvollen Umwelt- und Sozialsiegeln ausgezeichnet sind) (Indikator 12.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '12.1.ba Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - Rohstoffeinsatz',
            drawerLink: 'https://dns-indikatoren.de/12-1-b/',
            text: 'Ziel: Kontinuierliche Reduzierung',
            summaryLabel: 'Begründung zum Indikator 12.1.ba',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Rohstoffeinsatz (Indikator 12.1.ba) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Rohstoffeinsatz (Indikator 12.1.ba) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Rohstoffeinsatz (Indikator 12.1.ba) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '12.1.bb Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - Energieverbrauch',
            drawerLink: 'https://dns-indikatoren.de/12-1-b/',
            text: 'Ziel: Kontinuierliche Reduzierung',
            summaryLabel: 'Begründung zum Indikator 12.1.bb',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Energieverbrauch (Indikator 12.1.bb) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Energieverbrauch (Indikator 12.1.bb) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte – Energieverbrauch (Indikator 12.1.bb) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '12.1.bc Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - CO2-Emissionen',
            drawerLink: 'https://dns-indikatoren.de/12-1-b/',
            text: 'Ziel: Kontinuierliche Reduzierung',
            summaryLabel: 'Begründung zum Indikator 12.1.bc',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - CO2-Emissionen (Indikator 12.1.bc) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - CO2-Emissionen (Indikator 12.1.bc) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Globale Umweltinanspruchnahme durch den Konsum privater Haushalte - CO2-Emissionen (Indikator 12.1.bc) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Nachhaltiger Konsum',
      },
      {
        relevantTitle: '12.2 Nachhaltige Produktion: Anteil nachhaltiger Produktion stetig erhöhen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>12.2a Umweltmanagement EMAS</strong>
              <p>Ziel: 5.000 Organisationsstandorte bis 2030</p>`,
        begruendungen: [
          {
            title: '12.2a Umweltmanagement EMAS',
            drawerLink: 'https://dns-indikatoren.de/12-2/',
            text: 'Ziel: 5.000 Organisationsstandorte bis 2030',
            summaryLabel: 'Begründung zum Indikator 12.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Umweltmanagement EMAS (Indikator 12.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Umweltmanagement EMAS (Indikator 12.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Umweltmanagement EMAS (Indikator 12.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Nachhaltige Produktion',
      },
      {
        relevantTitle:
          '12.3 Nachhaltige Beschaffung: Vorbildwirkung der öffentlichen Hand für nachhaltige öffentliche Beschaffung verwirklichen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>12.3.a Anteil des Papiers mit Blauem Engel am Gesamtpapierverbrauch der unmittelbaren Bundesverwaltung</strong>
              <p>Ziel: Steigerung des Anteils auf 95 % bis 2020</p>
              <strong>12.3.b CO2-Emissionen von handelsüblichen Kraftfahrzeugen der öffentlichen Hand</strong>
              <p>Ziel: Signifikante Senkung</p>`,
        begruendungen: [
          {
            title:
              '12.3.a Anteil des Papiers mit Blauem Engel am Gesamtpapierverbrauch der unmittelbaren Bundesverwaltung',
            drawerLink: '	https://dns-indikatoren.de/12-3-ab/',
            text: 'Ziel: Steigerung des Anteils auf 95 % bis 2020',
            summaryLabel: 'Begründung zum Indikator 12.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil des Papiers mit Blauem Engel am Gesamtpapierverbrauch der unmittelbaren Bundesverwaltung (Indikator 12.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil des Papiers mit Blauem Engel am Gesamtpapierverbrauch der unmittelbaren Bundesverwaltung (Indikator 12.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil des Papiers mit Blauem Engel am Gesamtpapierverbrauch der unmittelbaren Bundesverwaltung (Indikator 12.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '12.3.b CO2-Emissionen von handelsüblichen Kraftfahrzeugen der öffentlichen Hand',
            drawerLink: '	https://dns-indikatoren.de/12-3-ab/',
            text: 'Ziel: Signifikante Senkung',
            summaryLabel: 'Begründung zum Indikator 12.3.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich CO2-Emissionen von handelsüblichen Kraftfahrzeugen der öffentlichen Hand (Indikator 12.3.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich CO2-Emissionen von handelsüblichen Kraftfahrzeugen der öffentlichen Hand (Indikator 12.3.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich CO2-Emissionen von handelsüblichen Kraftfahrzeugen der öffentlichen Hand (Indikator 12.3.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Nachhaltige Beschaffung',
      },
    ],
  },
  sdg13: {
    linkName: 'SDG 13 Maßnahmen zum Klimaschutz',
    title: 'SDG 13 Maßnahmen zum Klimaschutz',
    subtitle: 'Umgehend Maßnahmen zur Bekämpfung des Klimawandels und seiner Auswirkungen ergreifen',
    titleDrawer: 'SDG 13 Maßnahmen zum Klimaschutz',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 13 „Umgehend Maßnahmen zur Bekämpfung des Klimawandels und seiner Auswirkungen ergreifen*“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>13.1 Die Widerstandskraft und die Anpassungsfähigkeit gegenüber klimabedingten Gefahren und Naturkatastrophen in allen Ländern stärken</p>
            <p>13.2 Klimaschutzmaßnahmen in die nationalen Politiken, Strategien und Planungen einbeziehen</p>
            <p>13.3 Die Aufklärung und Sensibilisierung sowie die personellen und institutionellen Kapazitäten im Bereich der Abschwächung des Klimawandels, der Klimaanpassung, der Reduzierung der Klimaauswirkungen sowie der Frühwarnung verbessern</p>
            <p>13.a Die Verpflichtung erfüllen, die von den Vertragsparteien des Rahmenübereinkommens der Vereinten Nationen über Klimaänderungen, die entwickelte Länder sind, übernommen wurde, bis 2020 gemeinsam jährlich 100 Milliarden Dollar aus allen Quellen aufzubringen, um den Bedürfnissen der Entwicklungsländer im Kontext sinnvoller Klimaschutzmaßnahmen und einer transparenten Umsetzung zu entsprechen, und den Grünen Klimafonds vollständig zu operationalisieren, indem er schnellstmöglich mit den erforderlichen Finanzmitteln ausgestattet wird</p>
            <p>13.b Mechanismen zum Ausbau effektiver Planungs- und Managementkapazitäten im Bereich des Klimawandels in den am wenigsten entwickelten Ländern und kleinen Inselentwicklungsländern fördern, unter anderem mit gezielter Ausrichtung auf Frauen, junge Menschen sowie lokale und marginalisierte Gemeinwesen</p>
            <p>* In Anerkennung dessen, dass das Rahmenübereinkommen der Vereinten Nationen über Klimaänderungen das zentrale internationale zwischenstaatliche Forum für Verhandlungen über die globale Antwort auf den Klimawandel ist.</p>`,
    indikatoren: [
      {
        relevantTitle: '13.1 Klimaschutz: Treibhausgase reduzieren',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>13.1.a Treibhausgasemissionen</strong>
              <p>Ziel: Minderung um mindestens 40 % bis 2020, um mindestens 55 % bis 2030, jeweils gegenüber 1990; Erreichung von Treibhausgasneutralität bis 2050</p>
              <strong>13.1.b Beitrag zur internationalen Klimafinanzierung leisten Internationale Klimafinanzierung zur Reduktion von Treibhausgasen und zur Anpassung an den Klimawandel</strong>
              <p>Ziel: Verdopplung der Finanzierung bis 2020 gegenüber 2014</p>`,
        begruendungen: [
          {
            title: '13.1.a Treibhausgasemissionen',
            drawerLink: 'https://dns-indikatoren.de/13-1-a/',
            text: 'Ziel: Minderung um mindestens 40 % bis 2020, um mindestens 55 % bis 2030, jeweils gegenüber 1990; Erreichung von Treibhausgasneutralität bis 2050',
            summaryLabel: 'Begründung zum Indikator 13.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Treibhausgasemissionen (Indikator 13.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Treibhausgasemissionen (Indikator 13.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Treibhausgasemissionen (Indikator 13.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '13.1.b Beitrag zur internationalen Klimafinanzierung leisten Internationale Klimafinanzierung zur Reduktion von Treibhausgasen und zur Anpassung an den Klimawandel',
            drawerLink: 'https://dns-indikatoren.de/13-1-b/',
            text: 'Ziel: Verdopplung der Finanzierung bis 2020 gegenüber 2014',
            summaryLabel: 'Begründung zum Indikator 13.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Internationale Klimafinanzierung zur Reduktion von Treibhausgasen und zur Anpassung an den Klimawandel (Indikator 13.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Internationale Klimafinanzierung zur Reduktion von Treibhausgasen und zur Anpassung an den Klimawandel (Indikator 13.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Internationale Klimafinanzierung zur Reduktion von Treibhausgasen und zur Anpassung an den Klimawandel (Indikator 13.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Klimaschutz',
      },
    ],
  },
  sdg14: {
    linkName: 'SDG 14 Leben unter Wasser',
    title: 'SDG 14 Leben unter Wasser',
    subtitle:
      'Ozeane, Meere und Meeresressourcen im Sinne einer nachhaltigen Entwicklung erhalten und nachhaltig nutzen',
    titleDrawer: 'SDG 14 Leben unter Wasser',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 14 „Ozeane, Meere und Meeresressourcen im Sinne nachhaltiger Entwicklung erhalten und nachhaltig nutzen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>14.1 Bis 2025 alle Arten der Meeresverschmutzung, insbesondere durch vom Lande ausgehende Tätigkeiten und namentlich Meeresmüll und Nährstoffbelastung, verhüten und erheblich verringern</p>
            <p>14.2 Bis 2020 die Meeres- und Küstenökosysteme nachhaltig bewirtschaften und schützen, um unter anderem durch Stärkung ihrer Resilienz erhebliche nachteilige Auswirkungen zu vermeiden, und Maßnahmen zu ihrer Wiederherstellung ergreifen, damit die Meere wieder gesund und produktiv werden</p>
            <p>14.3 Die Versauerung der Ozeane auf ein Mindestmaß reduzieren und ihre Auswirkungen bekämpfen, unter anderem durch eine verstärkte wissenschaftliche Zusammenarbeit auf allen Ebenen</p>
            <p>14.4 Bis 2020 die Fangtätigkeit wirksam regeln und die Überfischung, die illegale, ungemeldete und unregulierte Fischerei und zerstörerische Fangpraktiken beenden und wissenschaftlich fundierte Bewirtschaftungspläne umsetzen, um die Fischbestände in kürzestmöglicher Zeit mindestens auf einen Stand zurückzuführen, der den höchstmöglichen Dauerertrag unter Berücksichtigung ihrer biologischen Merkmale sichert</p>
            <p>14.5 Bis 2020 mindestens 10 Prozent der Küsten- und Meeresgebiete im Einklang mit dem nationalen Recht und dem Völkerrecht und auf der Grundlage der besten verfügbaren wissenschaftlichen Informationen erhalten</p>
            <p>14.6 Bis 2020 bestimmte Formen der Fischereisubventionen untersagen, die zu Überkapazitäten und Überfischung beitragen, Subventionen abschaffen, die zu illegaler, ungemeldeter und unregulierter Fischerei beitragen, und keine neuen derartigen Subventionen einführen, in Anerkennung dessen, dass eine geeignete und wirksame besondere und differenzierte Behandlung der Entwicklungsländer und der am wenigsten entwickelten Länder einen untrennbaren Bestandteil der im Rahmen der Welthandelsorganisation geführten Verhandlungen über Fischereisubventionen bilden sollte*</p>
            <p>14.7 Bis 2030 die sich aus der nachhaltigen Nutzung der Meeresressourcen ergebenden wirtschaftlichen Vorteile für die kleinen Inselentwicklungsländer und die am wenigsten entwickelten Länder erhöhen, namentlich durch nachhaltiges Management der Fischerei, der Aquakultur und des Tourismus</p>
            <p>14.a Die wissenschaftlichen Kenntnisse vertiefen, die Forschungskapazitäten ausbauen und Meerestechnologien weitergeben, unter Berücksichtigung der Kriterien und Leitlinien der Zwischenstaatlichen Ozeanographischen Kommission für die Weitergabe von Meerestechnologie, um die Gesundheit der Ozeane zu verbessern und den Beitrag der biologischen Vielfalt der Meere zur Entwicklung der Entwicklungsländer, insbesondere der kleinen Inselentwicklungsländer und der am wenigsten entwickelten Länder, zu verstärken</p>
            <p>14.b Den Zugang der handwerklichen Kleinfischer zu den Meeresressourcen und Märkten gewährleisten</p>
            <p>14.c Die Erhaltung und nachhaltige Nutzung der Ozeane und ihrer Ressourcen verbessern und zu diesem Zweck das Völkerrecht umsetzen, wie es im Seerechtsübereinkommen der Vereinten Nationen niedergelegt ist, das den rechtlichen Rahmen für die Erhaltung und nachhaltige Nutzung der Ozeane und ihrer Ressourcen vorgibt, worauf in Ziffer 158 des Dokuments „Die Zukunft, die wir wollen“ hingewiesen wird</p>
            <p>* Unter Berücksichtigung der laufenden Verhandlungen im Rahmen der Welthandelsorganisation, der Entwicklungsagenda von Doha und des Mandats der Ministererklärung von Doha.</p>`,
    indikatoren: [
      {
        relevantTitle: '14.1 Meere schützen: Meere und Meeresressourcen schützen und nachhaltig nutzen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>14.1.aa. Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Ostsee</strong>
              <p>Ziel: Einhaltung des guten Zustands nach Oberflächengewässerverordnung (Jahresmittelwerte für Gesamtstickstoff bei in die Ostsee mündenden Flüssen sollen 2,6 Milligramm pro Liter nicht überschreiten)</p>
              <strong>14.1.ab Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Nordsee</strong>
              <p>Ziel: Einhaltung des guten Zustands nach Oberflächengewässerverordnung (Jahresmittelwerte für Gesamtstickstoff bei in die Nordsee mündenden Flüssen sollen 2,8 Milligramm pro Liter nicht überschreiten)</p>
              <strong>14.1.b Anteil der nachhaltig befischten Fischbestände in Nord- und Ostsee</strong>
              <p>Ziel: Alle wirtschaftlich genutzten Fischbestände sollen nach dem MSY-Ansatz nachhaltig bewirtschaftet werden bis 2020</p>`,
        begruendungen: [
          {
            title:
              '14.1.aa. Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Ostsee',
            drawerLink: 'https://dns-indikatoren.de/14-1-a/',
            text: 'Ziel: Einhaltung des guten Zustands nach Oberflächengewässerverordnung (Jahresmittelwerte für Gesamtstickstoff bei in die Ostsee mündenden Flüssen sollen 2,6 Milligramm pro Liter nicht überschreiten)',
            summaryLabel: 'Begründung zum Indikator 14.1.aa',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Ostsee (Indikator 14.1.aa) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Ostsee (Indikator 14.1.aa) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Ostsee (Indikator 14.1.aa) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '14.1.ab Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Nordsee',
            drawerLink: 'https://dns-indikatoren.de/14-1-a/',
            text: 'Ziel: Einhaltung des guten Zustands nach Oberflächengewässerverordnung (Jahresmittelwerte für Gesamtstickstoff bei in die Nordsee mündenden Flüssen sollen 2,8 Milligramm pro Liter nicht überschreiten)',
            summaryLabel: 'Begründung zum Indikator 14.1.ab',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Nordsee (Indikator 14.1.ab) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Nordsee (Indikator 14.1.ab) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Nährstoffeinträge in Küstengewässer und Meeresgewässer – Stickstoffeintrag über die Zuflüsse in die Nordsee (Indikator 14.1.ab) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '14.1.b Anteil der nachhaltig befischten Fischbestände in Nord- und Ostsee',
            drawerLink: '	https://dns-indikatoren.de/14-1-b/',
            text: 'Ziel: Alle wirtschaftlich genutzten Fischbestände sollen nach dem MSY-Ansatz nachhaltig bewirtschaftet werden bis 2020',
            summaryLabel: 'Begründung zum Indikator 14.1.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil der nachhaltig befischten Fischbestände Nord- und Ostsee (Indikator 14.1.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil der nachhaltig befischten Fischbestände Nord- und Ostsee (Indikator 14.1.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil der nachhaltig befischten Fischbestände Nord- und Ostsee (Indikator 14.1.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Meere schützen',
      },
    ],
  },
  sdg15: {
    linkName: 'SDG 15 Leben an Land',
    title: 'SDG 15 Leben an Land',
    subtitle:
      'Landökosysteme schützen, wiederherstellen und ihre nachhaltige Nutzung fördern, Wälder nachhaltig bewirtschaften, Wüstenbildung bekämpfen, Bodendegradation beenden und umkehren und dem Verlust der biologischen Vielfalt ein Ende setzen',
    titleDrawer: 'SDG 15 Leben an Land',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 15 „Landökosysteme schützen, wiederherstellen und ihre nachhaltige Nutzung fördern, Wälder nachhaltig bewirtschaften, Wüstenbildung bekämpfen, Bodendegradation beenden und umkehren und dem Verlust der biologischen Vielfalt ein Ende setzen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>15.1 Bis 2020 im Einklang mit den Verpflichtungen aus internationalen Übereinkünften die Erhaltung, Wiederherstellung und nachhaltige Nutzung der Land- und Binnensüßwasser-Ökosysteme und ihrer Dienstleistungen, insbesondere der Wälder, der Feuchtgebiete, der Berge und der Trockengebiete, gewährleisten</p>
            <p>15.2 Bis 2020 die nachhaltige Bewirtschaftung aller Waldarten fördern, die Entwaldung beenden, geschädigte Wälder wiederherstellen und die Aufforstung und Wiederaufforstung weltweit beträchtlich erhöhen</p>
            <p>15.3 Bis 2030 die Wüstenbildung bekämpfen, die geschädigten Flächen und Böden einschließlich der von Wüstenbildung, Dürre und Überschwemmungen betroffenen Flächen sanieren und eine Welt anstreben, in der die Landverödung neutralisiert wird</p>
            <p>15.4 Bis 2030 die Erhaltung der Bergökosysteme einschließlich ihrer biologischen Vielfalt sicherstellen, um ihre Fähigkeit zur Erbringung wesentlichen Nutzens für die nachhaltige Entwicklung zu stärken</p>
            <p>15.5 Umgehende und bedeutende Maßnahmen ergreifen, um die Verschlechterung der natürlichen Lebensräume zu verringern, dem Verlust der biologischen Vielfalt ein Ende zu setzen und bis 2020 die bedrohten Arten zu schützen und ihr Aussterben zu verhindern</p>
            <p>15.6 Die ausgewogene und gerechte Aufteilung der sich aus der Nutzung der genetischen Ressourcen ergebenden Vorteile und den angemessenen Zugang zu diesen Ressourcen fördern, wie auf internationaler Ebene vereinbart</p>
            <p>15.7 Dringend Maßnahmen ergreifen, um der Wilderei und dem Handel mit geschützten Pflanzen- und Tierarten ein Ende zu setzen und dem Problem des Angebots illegaler Produkte aus wildlebenden Pflanzen und Tieren und der Nachfrage danach zu begegnen</p>
            <p>15.8 Bis 2020 Maßnahmen einführen, um das Einbringen invasiver gebietsfremder Arten zu verhindern, ihre Auswirkungen auf die Land- und Wasserökosysteme deutlich zu reduzieren und die prioritären Arten zu kontrollieren oder zu beseitigen</p>
            <p>15.9 Bis 2020 Ökosystem- und Biodiversitätswerte in die nationalen und lokalen Planungen, Entwicklungsprozesse, Armutsbekämpfungsstrategien und Gesamtrechnungssysteme einbeziehen</p>
            <p>15.a Finanzielle Mittel aus allen Quellen für die Erhaltung und nachhaltige Nutzung der biologischen Vielfalt und der Ökosysteme aufbringen und deutlich erhöhen</p>
            <p>15.b Erhebliche Mittel aus allen Quellen und auf allen Ebenen für die Finanzierung einer nachhaltigen Bewirtschaftung der Wälder aufbringen und den Entwicklungsländern geeignete Anreize für den vermehrten Einsatz dieser Bewirtschaftungsform bieten, namentlich zum Zweck der Walderhaltung und Wiederaufforstung</p>
            <p>15.c Die weltweite Unterstützung von Maßnahmen zur Bekämpfung der Wilderei und des Handels mit geschützten Arten verstärken, unter anderem durch die Stärkung der Fähigkeit lokaler Gemeinwesen, Möglichkeiten einer nachhaltigen Existenzsicherung zu nutzen</p>`,
    indikatoren: [
      {
        relevantTitle: '15.1 Artenvielfalt: Arten erhalten – Lebensräume schützen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>15.1.a Artenvielfalt und Landschaftsqualität</strong>
              <p>Ziel: Erreichen des Indexwertes 100 bis 2030</p>`,
        begruendungen: [
          {
            title: '15.1.a Artenvielfalt und Landschaftsqualität',
            drawerLink: 'https://dns-indikatoren.de/15-1/',
            text: 'Ziel: Erreichen des Indexwertes 100 bis 2030',
            summaryLabel: 'Begründung zum Indikator 15.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Artenvielfalt und Landschaftsqualität (Indikator 15.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Artenvielfalt und Landschaftsqualität (Indikator 15.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Artenvielfalt und Landschaftsqualität (Indikator 15.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Artenvielfalt',
      },
      {
        relevantTitle: '15.2 Ökosysteme: Ökosysteme schützen, Ökosystemleistungen erhalten und Lebensräume bewahren',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>15.2.a Eutrophierung der Ökosysteme</strong>
              <p>Ziel: Verringerung um 35 % bis 2030 gegenüber 2005</p>`,
        begruendungen: [
          {
            title: '15.2.a Eutrophierung der Ökosysteme',
            drawerLink: 'https://dns-indikatoren.de/15-2/',
            text: 'Ziel: Verringerung um 35 % bis 2030 gegenüber 2005',
            summaryLabel: 'Begründung zum Indikator 15.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Eutrophierung der Ökosysteme (Indikator 15.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Eutrophierung der Ökosysteme (Indikator 15.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Eutrophierung der Ökosysteme (Indikator 15.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Ökosysteme',
      },
      {
        relevantTitle: '15.3 Wälder: Weltweit Entwaldung vermeiden und Böden schützen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>15.3.a Erhalt bzw. Wiederaufbau von Wäldern in Entwicklungsländern unter REDD+-Regelwerk</strong>
              <p>Ziel: Steigerung der Zahlungen bis 2030</p>
              <strong>15.3.b Deutsche bilaterale Bruttoentwicklungsausgaben zur Umsetzung des Übereinkommens der VN zur Bekämpfung der Wüstenbildung</strong>
              <p>Ziel: Steigerung der Zahlungen bis 2030</p>`,
        begruendungen: [
          {
            title: '15.3.a Erhalt bzw. Wiederaufbau von Wäldern in Entwicklungsländern unter REDD+-Regelwerk',
            drawerLink: 'https://dns-indikatoren.de/15-3-ab/',
            text: 'Ziel: Steigerung der Zahlungen bis 2030',
            summaryLabel: 'Begründung zum Indikator 15.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Erhalt bzw. Wiederaufbau von Wäldern in Entwicklungsländern unter REDD+-Regelwerk (Indikator 15.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Erhalt bzw. Wiederaufbau von Wäldern in Entwicklungsländern unter REDD+-Regelwerk (Indikator 15.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Erhalt bzw. Wiederaufbau von Wäldern in Entwicklungsländern unter REDD+-Regelwerk (Indikator 15.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title:
              '15.3.b Deutsche bilaterale Bruttoentwicklungsausgaben zur Umsetzung des Übereinkommens der VN zur Bekämpfung der Wüstenbildung',
            drawerLink: 'https://dns-indikatoren.de/15-3-ab/',
            text: 'Ziel: Steigerung der Zahlungen bis 2030',
            summaryLabel: 'Begründung zum Indikator 15.3.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Deutsche bilaterale Bruttoentwicklungsausgaben zur Umsetzung des Übereinkommens der VN zur Bekämpfung der Wüstenbildung (Indikator 15.3.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Deutsche bilaterale Bruttoentwicklungsausgaben zur Umsetzung des Übereinkommens der VN zur Bekämpfung der Wüstenbildung (Indikator 15.3.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Deutsche bilaterale Bruttoentwicklungsausgaben zur Umsetzung des Übereinkommens der VN zur Bekämpfung der Wüstenbildung (Indikator 15.3.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Wälder',
      },
    ],
  },
  sdg16: {
    linkName: 'SDG 16 Frieden, Gerechtigkeit und starke Institutionen',
    title: 'SDG 16 Frieden, Gerechtigkeit und starke Institutionen',
    subtitle:
      'Friedliche und inklusive Gesellschaften für nachhaltige Entwicklung fördern, allen Menschen Zugang zur Justiz ermöglichen und leistungsfähige, rechenschaftspflichtige und inklusive Institutionen auf allen Ebenen aufbauen',
    titleDrawer: 'SDG 16 Frieden, Gerechtigkeit und starke Institutionen',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 16 „Friedliche und inklusive Gesellschaften für nachhaltige Entwicklung fördern, allen Menschen Zugang zur Justiz ermöglichen und leistungsfähige, rechenschaftspflichtige und inklusive Institutionen auf allen Ebenen aufbauen“</p>
            <strong>Inhaltliche Ziele</strong>
            <p>16.1 Alle Formen der Gewalt und die gewaltbedingte Sterblichkeit überall deutlich verringern</p>
            <p>16.2 Missbrauch und Ausbeutung von Kindern, den Kinderhandel, Folter und alle Formen von Gewalt gegen Kinder beenden</p>
            <p>16.3 Die Rechtsstaatlichkeit auf nationaler und internationaler Ebene fördern und den gleichberechtigten Zugang aller zur Justiz gewährleisten</p>
            <p>16.4 Bis 2030 illegale Finanz- und Waffenströme deutlich verringern, die Wiedererlangung und Rückgabe gestohlener Vermögenswerte verstärken und alle Formen der organisierten Kriminalität bekämpfen</p>
            <p>16.5 Korruption und Bestechung in allen ihren Formen erheblich reduzieren</p>
            <p>16.6 Leistungsfähige, rechenschaftspflichtige und transparente Institutionen auf allen Ebenen aufbauen</p>
            <p>16.7 Dafür sorgen, dass die Entscheidungsfindung auf allen Ebenen bedarfsorientiert, inklusiv, partizipatorisch und repräsentativ ist</p>
            <p>16.8 Die Teilhabe der Entwicklungsländer an den globalen Lenkungsinstitutionen erweitern und verstärken</p>
            <p>16.9 Bis 2030 insbesondere durch die Registrierung der Geburten dafür sorgen, dass alle Menschen eine rechtliche Identität haben</p>
            <p>16.10 Den öffentlichen Zugang zu Informationen gewährleisten und die Grundfreiheiten schützen, im Einklang mit den nationalen Rechtsvorschriften und völkerrechtlichen Übereinkünften</p>
            <p>16.a Die zuständigen nationalen Institutionen namentlich durch internationale Zusammenarbeit beim Kapazitätsaufbau auf allen Ebenen zur Verhütung von Gewalt und zur Bekämpfung von Terrorismus und Kriminalität unterstützen, insbesondere in den Entwicklungsländern</p>
            <p>16.b Nichtdiskriminierende Rechtsvorschriften und Politiken zugunsten einer nachhaltigen Entwicklung fördern und durchsetzen</p>`,
    indikatoren: [
      {
        relevantTitle: '16.1 Kriminalität: Persönliche Sicherheit weiter erhöhen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>16.1.a Straftaten</strong>
              <p>Ziel: Rückgang der Zahl der erfassten Straftaten je 100.000 Einwohner/-innen auf unter 6.500 bis 2030</p>`,
        begruendungen: [
          {
            title: '16.1.a Straftaten',
            drawerLink: 'https://dns-indikatoren.de/16-1/',
            text: 'Ziel: Rückgang der Zahl der erfassten Straftaten je 100.000 Einwohner/-innen auf unter 6.500 bis 2030',
            summaryLabel: 'Begründung zum Indikator 16.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Straftaten (Indikator 16.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Straftaten (Indikator 16.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Straftaten (Indikator 16.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Kriminalität',
      },
      {
        relevantTitle:
          '16.2 Frieden und Sicherheit: Praktische Maßnahmen zur Bekämpfung der Proliferation, insbesondere von Kleinwaffen ergreifen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>16.2.a Anzahl der in betroffenen Weltregionen durchgeführten Projekte zur Sicherung, Registrierung und Zerstörung von Kleinwaffen und leichten Waffen durch Deutschland</strong>
              <p>Ziel: Mindestens 15 Projekte pro Jahr bis 2030</p>`,
        begruendungen: [
          {
            title:
              '16.2.a Anzahl der in betroffenen Weltregionen durchgeführten Projekte zur Sicherung, Registrierung und Zerstörung von Kleinwaffen und leichten Waffen durch Deutschland',
            drawerLink: 'https://dns-indikatoren.de/16-2/',
            text: 'Ziel: Mindestens 15 Projekte pro Jahr bis 2030',
            summaryLabel: 'Begründung zum Indikator 16.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anzahl der in betroffenen Weltregionen durchgeführten Projekte zur Sicherung, Registrierung und Zerstörung von Kleinwaffen und leichten Waffen durch Deutschland (Indikator 16.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anzahl der in betroffenen Weltregionen durchgeführten Projekte zur Sicherung, Registrierung und Zerstörung von Kleinwaffen und leichten Waffen durch Deutschland (Indikator 16.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anzahl der in betroffenen Weltregionen durchgeführten Projekte zur Sicherung, Registrierung und Zerstörung von Kleinwaffen und leichten Waffen durch Deutschland (Indikator 16.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Frieden und Sicherheit',
      },
      {
        relevantTitle: '16.3 Gute Regierungsführung: Korruption bekämpfen ',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>16.3.a Corruption Perception Index in Deutschland</strong>
              <p>Ziel: Verbesserung gegenüber 2012 bis 2030</p>
              <strong>16.3.b Corruption Perception Index in den Partnerländern der deutschen Entwicklungszusammenarbeit</strong>
              <p>Ziel: Verbesserung gegenüber 2012 bis 2030</p>`,
        begruendungen: [
          {
            title: '16.3.a Corruption Perception Index in Deutschland',
            drawerLink: 'https://dns-indikatoren.de/16-3-ab/',
            text: 'Ziel: Verbesserung gegenüber 2012 bis 2030',
            summaryLabel: 'Begründung zum Indikator 16.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Corruption Perception Index in Deutschland (Indikator 16.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Corruption Perception Index in Deutschland (Indikator 16.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Corruption Perception Index in Deutschland (Indikator 16.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
          {
            title: '16.3.b Corruption Perception Index in den Partnerländern der deutschen Entwicklungszusammenarbeit',
            drawerLink: 'https://dns-indikatoren.de/16-3-ab/',
            text: 'Ziel: Verbesserung gegenüber 2012 bis 2030',
            summaryLabel: 'Begründung zum Indikator 16.3.b',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Corruption Perception Index in den Partnerländern der deutschen Entwicklungszusammenarbeit (Indikator 16.3.b) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Corruption Perception Index in den Partnerländern der deutschen Entwicklungszusammenarbeit (Indikator 16.3.b) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Corruption Perception Index in den Partnerländern der deutschen Entwicklungszusammenarbeit (Indikator 16.3.b) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Gute Regierungsführung',
      },
    ],
  },
  sdg17: {
    linkName: 'SDG 17 Partnerschaften zur Erreichung der Ziele',
    title: 'SDG 17 Partnerschaften zur Erreichung der Ziele',
    subtitle:
      'Umsetzungsmittel stärken und die Globale Partnerschaft für nachhaltige Entwicklung mit neuem Leben erfüllen',
    titleDrawer: 'SDG 17 Partnerschaften zur Erreichung der Ziele',
    titleDrawerText: `<p>UN-Zielvorgaben für das SDG 17 „Umsetzungsmittel stärken und die Globale Partnerschaft für nachhaltige Entwicklung mit neuem Leben erfüllen“</p>
            <strong>Inhaltliche Ziele</strong><br>
            <strong>Finanzierung</strong>
            <p>17.1 Die Mobilisierung einheimischer Ressourcen verstärken, einschließlich durch internationale Unterstützung für die Entwicklungsländer, um die nationalen Kapazitäten zur Erhebung von Steuern und anderen Abgaben zu verbessern</p>
            <p>17.2 Sicherstellen, dass die entwickelten Länder ihre Zusagen im Bereich der öffentlichen Entwicklungshilfe voll einhalten, einschließlich der von vielen entwickelten Ländern eingegangenen Verpflichtung, die Zielvorgabe von 0,7 Prozent ihres Bruttonationaleinkommens für öffentliche Entwicklungshilfe zugunsten der Entwicklungsländer und 0,15 bis 0,20 Prozent zugunsten der am wenigsten entwickelten Länder zu erreichen; den Gebern öffentlicher Entwicklungshilfe wird nahegelegt, die Bereitstellung von mindestens 0,20 Prozent ihres Bruttonationaleinkommens zugunsten der am wenigsten entwickelten Länder als Zielsetzung zu erwägen</p>
            <p>17.3 Zusätzliche finanzielle Mittel aus verschiedenen Quellen für die Entwicklungsländer mobilisieren</p>
            <p>17.4 Den Entwicklungsländern dabei behilflich sein, durch eine koordinierte Politik zur Förderung der Schuldenfinanzierung, der Entschuldung beziehungsweise der Umschuldung die langfristige Tragfähigkeit der Verschuldung zu erreichen, und das Problem der Auslandsverschuldung hochverschuldeter armer Länder angehen, um die Überschuldung zu verringern</p>
            <p>17.5 Investitionsförderungssysteme für die am wenigsten entwickelten Länder beschließen und umsetzen</p>
            <strong>Technologie</strong>
            <p>17.6 Die regionale und internationale Nord-Süd- und Süd-Süd-Zusammenarbeit und Dreieckskooperation im Bereich Wissenschaft, Technologie und Innovation und den Zugang dazu verbessern und den Austausch von Wissen zu einvernehmlich festgelegten Bedingungen verstärken, unter anderem durch eine bessere Abstimmung zwischen den vorhandenen Mechanismen, insbesondere auf Ebene der Vereinten Nationen, und durch einen globalen Mechanismus zur Technologieförderung</p>
            <p>17.7 Die Entwicklung, den Transfer, die Verbreitung und die Diffusion von umweltverträglichen Technologien an die Entwicklungsländer zu gegenseitig vereinbarten günstigen Bedingungen, einschließlich Konzessions- und Vorzugsbedingungen, fördern</p>
            <p>17.8 Die Technologiebank und den Mechanismus zum Kapazitätsaufbau für Wissenschaft, Technologie und Innovation für die am wenigsten entwickelten Länder bis 2017 vollständig operationalisieren und die Nutzung von Grundlagentechnologien, insbesondere der Informations- und Kommunikationstechnologien, verbessern</p>
            <strong>Kapazitätsaufbau</strong>
            <p>17.9 Die internationale Unterstützung für die Durchführung eines effektiven und gezielten Kapazitätsaufbaus in den Entwicklungsländern verstärken, um die nationalen Pläne zur Umsetzung aller Ziele für nachhaltige Entwicklung zu unterstützen, namentlich im Rahmen der Nord-Süd- und Süd-Süd-Zusammenarbeit und der Dreieckskooperation</p>
            <strong>Handel</strong>
            <p>17.10 Ein universales, regelgestütztes, offenes, nichtdiskriminierendes und gerechtes multilaterales Handelssystem unter dem Dach der Welthandelsorganisation fördern, insbesondere durch den Abschluss der Verhandlungen im Rahmen ihrer Entwicklungsagenda von Doha</p>
            <p>17.11 Die Exporte der Entwicklungsländer deutlich erhöhen, insbesondere mit Blick darauf, den Anteil der am wenigsten entwickelten Länder an den weltweiten Exporten bis 2020 zu verdoppeln</p>
            <p>17.12 Die rasche Umsetzung des zoll- und kontingentfreien Marktzugangs auf dauerhafter Grundlage für alle am wenigsten entwickelten Länder im Einklang mit den Beschlüssen der Welthandelsorganisation erreichen, unter anderem indem sichergestellt wird, dass die für Importe aus den am wenigsten entwickelten Ländern geltenden präferenziellen Ursprungsregeln transparent und einfach sind und zur Erleichterung des Marktzugangs beitragen</p>
            <strong>Systemische Fragen</strong><br>
            <em>Politik- und institutionelle Kohärenz</em>
            <p>17.13 Die globale makroökonomische Stabilität verbessern, namentlich durch Politikkoordinierung und Politikkohärenz</p>
            <p>17.14 Die Politikkohärenz zugunsten nachhaltiger Entwicklung verbessern</p>
            <p>17.15 Den politischen Spielraum und die Führungsrolle jedes Landes bei der Festlegung und Umsetzung von Politiken zur Armutsbeseitigung und für nachhaltige Entwicklung respektieren</p>
            <em>Multi-Akteur-Partnerschaften</em>
            <p>17.16 Die Globale Partnerschaft für nachhaltige Entwicklung ausbauen, ergänzt durch Multi-Akteur-Partnerschaften zur Mobilisierung und zum Austausch von Wissen, Fachkenntnissen, Technologie und finanziellen Ressourcen, um die Erreichung der Ziele für nachhaltige Entwicklung in allen Ländern und insbesondere in den Entwicklungsländern zu unterstützen</p>
            <p>17.17 Die Bildung wirksamer öffentlicher, öffentlich-privater und zivilgesellschaftlicher Partnerschaften aufbauend auf den Erfahrungen und Mittelbeschaffungsstrategien bestehender Partnerschaften unterstützen und fördern</p>
            <em>Daten, Überwachung und Rechenschaft</em>
            <p>17.18 Bis 2020 die Unterstützung des Kapazitätsaufbaus für die Entwicklungsländer und namentlich die am wenigsten entwickelten Länder und die kleinen Inselentwicklungsländer erhöhen, mit dem Ziel, über erheblich mehr hochwertige, aktuelle und verlässliche Daten zu verfügen, die nach Einkommen, Geschlecht, Alter, Rasse, Ethnizität, Migrationsstatus, Behinderung, geografischer Lage und sonstigen im nationalen Kontext relevanten Merkmalen aufgeschlüsselt sind</p>
            <p>17.19 Bis 2030 auf den bestehenden Initiativen aufbauen, um Fortschrittsmaße für nachhaltige Entwicklung zu erarbeiten, die das Bruttoinlandsprodukt ergänzen, und den Aufbau der statistischen Kapazitäten der Entwicklungsländer unterstützen</p>`,
    indikatoren: [
      {
        relevantTitle: '17.1 Entwicklungszusammenarbeit: Nachhaltige Entwicklung unterstützen',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>17.1.a Anteil öffentlicher Entwicklungsausgaben am Bruttonationaleinkommen</strong>
              <p>Ziel: Steigerung des Anteils auf 0,7 % des Bruttonationaleinkommens bis 2030</p>`,
        begruendungen: [
          {
            title: '17.1.a Anteil öffentlicher Entwicklungsausgaben am Bruttonationaleinkommen',
            drawerLink: 'https://dns-indikatoren.de/17-1/',
            text: 'Ziel: Steigerung des Anteils auf 0,7 % des Bruttonationaleinkommens bis 2030',
            summaryLabel: 'Begründung zum Indikator 17.1.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anteil öffentlicher Entwicklungsausgaben am Bruttonationaleinkommen (Indikator 17.1.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anteil öffentlicher Entwicklungsausgaben am Bruttonationaleinkommen (Indikator 17.1.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anteil öffentlicher Entwicklungsausgaben am Bruttonationaleinkommen (Indikator 17.1.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Entwicklungszusammenarbeit',
      },
      {
        relevantTitle: '17.2 Wissenstransfer insbesondere im technischen Bereich: Wissen international vermitteln',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>17.2.a Anzahl der Studierenden und Forschenden aus Entwicklungsländern sowie LDCs pro Jahr</strong>
              <p>Ziel: Steigerung der Anzahl um 10 % von 2015 bis 2020, anschließend Verstetigung</p>`,
        begruendungen: [
          {
            title: '17.2.a Anzahl der Studierenden und Forschenden aus Entwicklungsländern sowie LDCs pro Jahr',
            drawerLink: 'https://dns-indikatoren.de/17-2/',
            text: 'Ziel: Steigerung der Anzahl um 10 % von 2015 bis 2020, anschließend Verstetigung',
            summaryLabel: 'Begründung zum Indikator 17.2.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Anzahl der Studierenden und Forschenden aus Entwicklungsländern sowie LDCs pro Jahr (Semester) (Indikator 17.2.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Anzahl der Studierenden und Forschenden aus Entwicklungsländern sowie LDCs pro Jahr (Semester)  (Indikator 17.2.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Anzahl der Studierenden und Forschenden aus Entwicklungsländern sowie LDCs pro Jahr (Semester) (Indikator 17.2.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Wissenstransfer insbesondere im technischen Bereich',
      },
      {
        relevantTitle: '17.3 Märkte öffnen: Handelschancen der Entwicklungsländer verbessern',
        relevantTitleDrawerText: `<p>Dieser Indikatorenbereich enthält die folgenden Indikatoren und Ziele:</p>
              <strong>17.3.a Einfuhren aus am wenigsten entwickelten Ländern</strong>
              <p>Ziel: Steigerung des Anteils um 100 % bis 2030 gegenüber 2014</p>`,
        begruendungen: [
          {
            title: '17.3.a Einfuhren aus am wenigsten entwickelten Ländern',
            drawerLink: 'https://dns-indikatoren.de/17-3/',
            text: 'Ziel: Steigerung des Anteils um 100 % bis 2030 gegenüber 2014',
            summaryLabel: 'Begründung zum Indikator 17.3.a',
            begruendungDrawerContent:
              'Beispiele: "Das Regelungsvorhaben hat (voraussichtlich/ggf.) Auswirkungen auf den Bereich Einfuhren aus am wenigsten entwickelten Ländern (Indikator 17.3.a) der Deutschen Nachhaltigkeitsstrategie, indem ...", "Das Regelungsvorhaben trägt zur Erreichung der Ziele im Bereich Einfuhren aus am wenigsten entwickelten Ländern (Indikator 17.3.a) der Deutschen Nachhaltigkeitsstrategie bei, indem ...", "Beim Regelungsvorhaben bestehen mögliche Zielkonflikte mit dem Bereich Einfuhren aus am wenigsten entwickelten Ländern (Indikator 17.3.a) der Deutschen Nachhaltigkeitsstrategie. Daher …"',
          },
        ],
        anderweitigRelevantLabel: 'Märkte öffnen',
      },
    ],
  },
  prinzipien: {
    linkName: 'Prinzipien einer nachhaltigen Entwicklung',
    title: 'Prinzipien einer nachhaltigen Entwicklung',
    subtitle:
      'Die sechs Prinzipien einer nachhaltigen Entwicklung enthalten grundsätzliche Anforderungen an eine nachhaltige Politik. Sie dienen der Operationalisierung des Leitprinzips einer nachhaltigen Entwicklung und orientieren sich an der Agenda 2030 für nachhaltige Entwicklung vor dem Hintergrund eines dringend erforderlichen Wandels unserer Gesellschaft und Wirtschaft.',
    indikatoren: [
      {
        relevantTitle:
          '1. Nachhaltige Entwicklung als Leitprinzip konsequent in allen Bereichen und bei allen Entscheidungen anwenden',
        relevantTitleDrawerText: `<p>Übergreifendes Ziel und Maßstab allen Handelns ist es, die natürlichen Lebensgrundlagen der Erde dauerhaft zu sichern und allen Menschen jetzt und in Zukunft ein Leben in Würde zu ermöglichen.</p> 
              <p>Hierfür sind bei allen Entscheidungen wirtschaftliche Leistungsfähigkeit, der Schutz der natürlichen Lebensgrundlagen sowie soziale 
              Gerechtigkeit und gleichberechtigte Teilhabe unter Berücksichtigung systemischer Wechselwirkungen 
              sowie technologischer und gesellschaftlicher Innovationen so zusammenzudenken, dass Entwicklungen für heutige und künftige Generationen auch in globaler Betrachtung ökologisch und sozial tragfähig sind. Politisches Handeln muss kohärent sein.</p>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<p>Übergreifendes Ziel und Maßstab allen Handelns ist es, die natürlichen Lebensgrundlagen der Erde dauerhaft zu sichern und allen Menschen jetzt und in Zukunft ein Leben in Würde zu ermöglichen.</p>
              <p>Hierfür sind bei allen Entscheidungen wirtschaftliche Leistungsfähigkeit, der Schutz der natürlichen Lebensgrundlagen sowie soziale 
              Gerechtigkeit und gleichberechtigte Teilhabe unter Berücksichtigung systemischer Wechselwirkungen 
              sowie technologischer und gesellschaftlicher Innovationen so zusammenzudenken, dass Entwicklungen für heutige und künftige Generationen auch in globaler Betrachtung ökologisch und sozial tragfähig sind. Politisches Handeln muss kohärent sein.</p>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
      {
        relevantTitle: '2. Global Verantwortung wahrnehmen',
        relevantTitleDrawerText: `<ol class="prinzipien-list" style="list-style: lower-alpha;">
              <li>Im Einklang mit der Agenda 2030 für nachhaltige Entwicklung der Vereinten Nationen und dem Pariser Klimaabkommen sind auf globaler Ebene zu verknüpfen:
              <ul style="list-style: disc;">
              <li>die Achtung, der Schutz und die Gewährleistung der Menschenrechte,</li>
              <li>die umfassende Teilhabe aller an wirtschaftlicher und sozialer Entwicklung,</li>
              <li>der Schutz der Umwelt, insbesondere des Klimas, einschließlich der Einhaltung der Grenzen der ökologischen Belastbarkeit im regionalen und globalen Rahmen</li>
              <li>die Bekämpfung von Armut, Hunger und sozialer Ungleichheit und Ausgrenzung,</li>
              <li>sowie rechtsstaatliches und verantwortungsvolles Regierungshandeln.</li>
              </ul>
              </li>
              <li>Deutschland soll die nachhaltige Entwicklung in anderen Ländern berücksichtigen und fördern. Unser Handeln in Deutschland soll möglichst nicht zu Belastungen für die Menschen und die Umwelt in anderen Ländern führen.</li>
              </ol>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<ol class="prinzipien-list" style="list-style: lower-alpha;">
                  <li>Im Einklang mit der Agenda 2030 für nachhaltige Entwicklung der Vereinten Nationen und dem Pariser Klimaabkommen sind auf globaler Ebene zu verknüpfen:
                  <ul style="list-style: disc;">
                  <li>die Achtung, der Schutz und die Gewährleistung der Menschenrechte,</li>
                  <li>die umfassende Teilhabe aller an wirtschaftlicher und sozialer Entwicklung,</li>
                  <li>der Schutz der Umwelt, insbesondere des Klimas, einschließlich der Einhaltung der Grenzen der ökologischen Belastbarkeit im regionalen und globalen Rahmen</li>
                  <li>die Bekämpfung von Armut, Hunger und sozialer Ungleichheit und Ausgrenzung,</li>
                  <li>sowie rechtsstaatliches und verantwortungsvolles Regierungshandeln.</li>
                  </ul>
                  </li>
                  <li>Deutschland soll die nachhaltige Entwicklung in anderen Ländern berücksichtigen und fördern. Unser Handeln in Deutschland soll möglichst nicht zu Belastungen für die Menschen und die Umwelt in anderen Ländern führen.</li>
                  </ol>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
      {
        relevantTitle: '3. Natürliche Lebensgrundlagen erhalten',
        relevantTitleDrawerText: `
              <ol class="prinzipien-list" style="list-style: lower-alpha;">
              <li>Zur Erhaltung der natürlichen Lebensgrundlagen und zur Einhaltung der planetaren Grenzen müssen Stoffkreisläufe so schnell wie möglich geschlossen bzw. in Einklang mit ökosystemischen Prozessen und Funktionen gebracht werden. Hierfür</li>
              <ul style="list-style: disc;"><li>dürfen erneuerbare Naturgüter (wie z. B. Wälder oder Fischbestände) und Böden nur im Rahmen ihrer Regenerationsfähigkeit genutzt sowie ihre weiteren ökologischen Funktionen nicht beeinträchtigt werden;</li>
              <li>sind nicht-erneuerbare Naturgüter (wie z. B. mineralische Rohstoffe oder fossile Energieträger) so sparsam wie möglich zu nutzen. Erneuerbare Ressourcen sollen die Nutzung nicht-erneuerbarer Ressourcen ersetzen, soweit dies die Umweltbelastung mindert und diese Nutzung auch in allen Aspekten nachhaltig ist;</li>
              <li>darf die Freisetzung von Stoffen nur unter Beachtung des Vorsorgeprinzips im Rahmen der ökologischen Grenzen der Tragfähigkeit natürlicher Systeme (Reaktionsvermögen der Umwelt) erfolgen.</li></ul>
              <li>Gefahren und unvertretbare Risiken für die menschliche Gesundheit und die Natur sind zu vermeiden.</li>
              </ol>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<ol class="prinzipien-list" style="list-style: lower-alpha;">
                  <li>Zur Erhaltung der natürlichen Lebensgrundlagen und zur Einhaltung der planetaren Grenzen müssen Stoffkreisläufe so schnell wie möglich geschlossen bzw. in Einklang mit ökosystemischen Prozessen und Funktionen gebracht werden. Hierfür</li>
                  <ul style="list-style: disc;"><li>dürfen erneuerbare Naturgüter (wie z. B. Wälder oder Fischbestände) und Böden nur im Rahmen ihrer Regenerationsfähigkeit genutzt sowie ihre weiteren ökologischen Funktionen nicht beeinträchtigt werden;</li>
                  <li>sind nicht-erneuerbare Naturgüter (wie z. B. mineralische Rohstoffe oder fossile Energieträger) so sparsam wie möglich zu nutzen. Erneuerbare Ressourcen sollen die Nutzung nicht-erneuerbarer Ressourcen ersetzen, soweit dies die Umweltbelastung mindert und diese Nutzung auch in allen Aspekten nachhaltig ist;</li>
                  <li>darf die Freisetzung von Stoffen nur unter Beachtung des Vorsorgeprinzips im Rahmen der ökologischen Grenzen der Tragfähigkeit natürlicher Systeme (Reaktionsvermögen der Umwelt) erfolgen.</li></ul>
                  <li>Gefahren und unvertretbare Risiken für die menschliche Gesundheit und die Natur sind zu vermeiden.</li>
                  </ol>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
      {
        relevantTitle: '4. Nachhaltiges Wirtschaften stärken',
        relevantTitleDrawerText: `<ol class="prinzipien-list" style="list-style: lower-alpha;"><li>Der notwendige Strukturwandel für globales nachhaltiges Konsumieren und Produzieren und die dafür nutzbar zu machenden technischen Modernisierungen sollen wirtschaftlich erfolgreich sowie im deutschen und globalen Kontext ökologisch und sozial tragfähig sowie generationengerecht gestaltet werden.</li>
              <li>Energie- und Ressourcenverbrauch sowie die Verkehrsleistung müssen vom Wirtschaftswachstum entkoppelt werden. Zugleich ist anzustreben, dass der Anstieg der Nachfrage nach Energie, Ressourcen und Verkehrsleistungen kleiner wird und durch Effizienzgewinne abnehmende Verbräuche (absolute Entkopplung) entstehen.</li>
              <li>Eine nachhaltige Land- und Fischereiwirtschaft muss produktiv, wettbewerbsfähig sowie sozial- und umweltverträglich sein; sie muss insbesondere Biodiversität, Böden und Gewässer schützen und erhalten sowie die Anforderungen an eine tiergerechte Nutztierhaltung und den vorsorgenden, insbesondere gesundheitlichen Verbraucherschutz beachten.</li>
              <li>Die öffentlichen Haushalte sind der Generationengerechtigkeit in allen Dimensionen der Nachhaltigkeit verpflichtet. Die Finanzmärkte sollen die Erfordernisse einer nachhaltigen Entwicklung berücksichtigen.</li>
              </ol>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<ol class="prinzipien-list" style="list-style: lower-alpha;"><li>Der notwendige Strukturwandel für globales nachhaltiges Konsumieren und Produzieren und die dafür nutzbar zu machenden technischen Modernisierungen sollen wirtschaftlich erfolgreich sowie im deutschen und globalen Kontext ökologisch und sozial tragfähig sowie generationengerecht gestaltet werden.</li>
                  <li>Energie- und Ressourcenverbrauch sowie die Verkehrsleistung müssen vom Wirtschaftswachstum entkoppelt werden. Zugleich ist anzustreben, dass der Anstieg der Nachfrage nach Energie, Ressourcen und Verkehrsleistungen kleiner wird und durch Effizienzgewinne abnehmende Verbräuche (absolute Entkopplung) entstehen.</li>
                  <li>Eine nachhaltige Land- und Fischereiwirtschaft muss produktiv, wettbewerbsfähig sowie sozial- und umweltverträglich sein; sie muss insbesondere Biodiversität, Böden und Gewässer schützen und erhalten sowie die Anforderungen an eine tiergerechte Nutztierhaltung und den vorsorgenden, insbesondere gesundheitlichen Verbraucherschutz beachten.</li>
                  <li>Die öffentlichen Haushalte sind der Generationengerechtigkeit in allen Dimensionen der Nachhaltigkeit verpflichtet. Die Finanzmärkte sollen die Erfordernisse einer nachhaltigen Entwicklung berücksichtigen.</li>
                  </ol>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
      {
        relevantTitle: '5. Sozialen Zusammenhalt in einer offenen Gesellschaft wahren und verbessern',
        relevantTitleDrawerText: `<p>Um den sozialen Zusammenhalt zu stärken und niemanden zurückzulassen, sollen</p>
              <ul class="prinzipien-list" style="list-style: disc;"><li>Armut und soziale Ausgrenzung soweit wie möglich überwunden bzw. ihnen vorgebeugt und inklusiver Wohlstand gefördert werden,</li>
              <li>regional gleichwertige Lebensverhältnisse angestrebt werden,</li>
              <li>alle die gleichberechtigte Chance erhalten, sich an der wirtschaftlichen Entwicklung zu beteiligen,</li>
              <li>notwendige Anpassungen an die demografische Entwicklung frühzeitig in Politik, Wirtschaft und Gesellschaft erfolgen,</li>
              <li>alle am gesellschaftlichen, kulturellen und politischen Leben umfassend und diskriminierungsfrei teilhaben können,</li>
              <li>Beiträge zur Reduzierung von Armut und Ungleichheit weltweit geleistet werden.</li>
              </ul>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<p>Um den sozialen Zusammenhalt zu stärken und niemanden zurückzulassen, sollen</p>
                  <ul class="prinzipien-list" style="list-style: disc;list-style-position: initial !important;margin-left: 35px;"><li>Armut und soziale Ausgrenzung soweit wie möglich überwunden bzw. ihnen vorgebeugt und inklusiver Wohlstand gefördert werden,</li>
                  <li>regional gleichwertige Lebensverhältnisse angestrebt werden,</li>
                  <li>alle die gleichberechtigte Chance erhalten, sich an der wirtschaftlichen Entwicklung zu beteiligen,</li>
                  <li>notwendige Anpassungen an die demografische Entwicklung frühzeitig in Politik, Wirtschaft und Gesellschaft erfolgen,</li>
                  <li>alle am gesellschaftlichen, kulturellen und politischen Leben umfassend und diskriminierungsfrei teilhaben können,</li>
                  <li>Beiträge zur Reduzierung von Armut und Ungleichheit weltweit geleistet werden.</li>
                  </ul>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
      {
        relevantTitle: '6. Bildung, Wissenschaft und Innovation als Treiber einer nachhaltigen Entwicklung nutzen',
        relevantTitleDrawerText: `
              <ul class="prinzipien-list" style="list-style: lower-alpha;">
              <li>Die notwendigen Qualifikationen und Handlungskompetenzen sind im Sinne einer „Bildung für nachhaltige Entwicklung“ im gesamten Bildungssystem zu verankern. Die Möglichkeiten zur Teilhabe an qualitativ hochwertiger Bildung und dem Erwerb von Handlungskompetenzen für nachhaltige Entwicklung sind unabhängig von Herkunft, Geschlecht und Alter weiter zu verbessern.</li>
              <li>Wissenschaftliche Erkenntnisse sind als Grundlage bei allen Entscheidungen zu berücksichtigen. Wissenschaft und Forschung sind aufgerufen, sich verstärkt an den Zielen und Herausforderungen einer globalen nachhaltigen Entwicklung auszurichten.</li>
              <li>Nachhaltigkeitsaspekte sind bei Innovationsprozessen, insbesondere im Kontext der Digitalisierung, von Beginn an konsequent zu berücksichtigen, damit Chancen für eine nachhaltige Entwicklung genutzt und Risiken für Mensch und Umwelt vermieden werden können. Gleichzeitig sollen Innovationsfreudigkeit und -reichweite gestärkt werden.</li>
              </ul>`,
        begruendungen: [
          {
            title: 'Dieses Prinzip enthält die folgenden Anforderungen an eine nachhaltige Politik:',
            text: `<ol class="prinzipien-list" style="list-style: lower-alpha;">
                  <li>Die notwendigen Qualifikationen und Handlungskompetenzen sind im Sinne einer „Bildung für nachhaltige Entwicklung“ im gesamten Bildungssystem zu verankern. Die Möglichkeiten zur Teilhabe an qualitativ hochwertiger Bildung und dem Erwerb von Handlungskompetenzen für nachhaltige Entwicklung sind unabhängig von Herkunft, Geschlecht und Alter weiter zu verbessern.</li>
                  <li>Wissenschaftliche Erkenntnisse sind als Grundlage bei allen Entscheidungen zu berücksichtigen. Wissenschaft und Forschung sind aufgerufen, sich verstärkt an den Zielen und Herausforderungen einer globalen nachhaltigen Entwicklung auszurichten.</li>
                  <li>Nachhaltigkeitsaspekte sind bei Innovationsprozessen, insbesondere im Kontext der Digitalisierung, von Beginn an konsequent zu berücksichtigen, damit Chancen für eine nachhaltige Entwicklung genutzt und Risiken für Mensch und Umwelt vermieden werden können. Gleichzeitig sollen Innovationsfreudigkeit und -reichweite gestärkt werden.</li>
                  </ol>`,
            summaryLabel: 'Begründung',
          },
        ],
      },
    ],
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    intro1:
      'Auf dieser Seite finden Sie alle Begründungstexte der Indikatorenbereiche, Indikatoren, SDGs und Prinzipien einer nachhaltigen Entwicklung, die Sie als relevant eingestuft haben.',
    intro2:
      'Fassen Sie Ihre Ergebnisse am Ende der Seite zusammen, um gemäß <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34#page=34" target="_blank">§ 44 Absatz 1 Satz 4 GGO</a> in der Begründung des Regelungsentwurfs darzustellen, ob die Wirkungen des Vorhabens einer nachhaltigen Entwicklung entsprechen, insbesondere welche langfristigen Wirkungen es hat.',
    unfinishedSdgs: {
      title: 'In folgenden SDGs haben Sie noch nicht alle Indikatorenbereiche bearbeitet',
      zurBearbeitungBtn: 'Zur Bearbeitung',
      allFinished: 'Es wurden alle Indikatorenbereiche bearbeitet.',
    },
    reasons: {
      title: 'Überblick über relevante Indikatorenbereiche und Ihre Begründungstexte',
      anderweitig: 'Anderweitige Relevanz: {{bereich}}',
      switchLabel: 'Nur Begründungstexte anzeigen',
      drawers: {
        begruessung: {
          title: 'Nur Begründungstexte anzeigen',
          content:
            'Hier können Sie sich Ihre Begründungstexte ohne Zwischenüberschriften anzeigen lassen. Diesen Textentwurf können Sie als Grundlage für Ihre Darstellung der Auswirkungen auf eine nachhaltige Entwicklung in der Begründung des Regelungsentwurfs nutzen.',
        },
      },
    },
    auswirkungen: {
      title: 'Auswirkungen auf eine nachhaltige Entwicklung',
      label: 'Zusammenfassung der Auswirkungen',
      drawers: {
        zusammenfassung: {
          title: 'Zusammenfassung der Auswirkungen',
          content: `<p>Hier können Sie eine Zusammenfassung der Auswirkungen des Regelungsvorhabens auf eine nachhaltige Entwicklung für die Aufnahme in die Gesetz- oder Verordnungsbegründung erarbeiten.</p>
                <p>Gemäß § 44 Abs. 1 Satz 4 der Gemeinsamen Geschäftsordnung der Bundesministerien (GGO) ist in der Begründung von Gesetzen eine Aussage zu nachhaltiger Entwicklung aufzunehmen. Diese Verpflichtung besteht auch für Rechtsverordnungsentwürfe (§ 62 Abs. 2 i. V. m. 44 Abs. 1 Satz 4 GGO).</p>
                <p>Für diese Begründung ist eine Zusammenfassung bzw. Auflistung der bisherigen Prüfergebnisse nicht ausreichend. Vielmehr soll die Begründung des Regelvorhabens in wertender Gesamtbetrachtung eine nachvollziehbare Aussage dazu enthalten, „ob die Wirkungen des Vorhabens einer nachhaltigen Entwicklung entsprechen, insbesondere welche langfristigen Wirkungen das Vorhaben hat“ (§ 44 Abs. 1 Satz 4 GGO).</p>
                <p>Um Ihre eingegebenen Begründungstexte für einzelne Indikatorenbereiche, Indikatoren, SDGs oder Prinzipien für die Begründung des Regelungsvorhabens zu nutzen, kopieren Sie den obigen Text in dieses Feld.</p>
                <p>Bitte stellen Sie bei der Ausarbeitung der Begründung – soweit relevant – auch etwaige Zielkonflikte transparent dar und gehen Sie darauf ein, welche Schlussfolgerung hieraus für das Vorhaben gezogen wurden. Hinweis: In der Deutschen Nachhaltigkeitsstrategie 2021 sind sechs sogenannte Transformationsbereiche aufgeführt (Bereiche, in denen Fortschritte derzeit besonders relevant sind: 1. Menschliches Wohlbefinden und Fähigkeiten, soziale Gerechtigkeit; 2. Energiewende und Klimaschutz; 3. Kreislaufwirtschaft; 4. Nachhaltiges Bauen und Verkehrswende; 5. Nachhaltige Agrar- und Ernährungssysteme; 6. Schadstofffreie Umwelt; weitere Informationen hierzu vgl. <a target="_blank" href="{{basePath}}/arbeitshilfen/download/28#page=49">Deutsche Nachhaltigkeitsstrategie 2021, S. 48ff.</a>).</p>`,
        },
      },
      requiredMsg: 'Bitte geben Sie die Auswirkungen auf eine nachhaltige Entwicklung an',
    },
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    subtitle: 'Auswirkungen auf eine nachhaltige Entwicklung',
    noImpact: 'Es sind keine wesentlichen Auswirkungen auf eine nachhaltige Entwicklung zu erwarten.',
    impact: 'Es sind folgende wesentliche Auswirkungen auf eine nachhaltige Entwicklung zu erwarten:',
    exportBtn: 'Datenexport (PDF)',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung zur Darstellung der Auswirkungen auf eine nachhaltige Entwicklung in die Begründung des Regelungsentwurfs übernehmen.',
  },
  contactPerson: de_contacts.enap.contactPerson,
};
