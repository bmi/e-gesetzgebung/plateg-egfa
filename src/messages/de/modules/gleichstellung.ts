// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_gleichstellung = {
  name: 'Gleichstellungsorientierte GFA',
  kernbereiche: [
    { name: 'Arbeit', text: 'Arbeit' },
    { name: 'Geld', text: 'Geld' },
    { name: 'Wissen', text: 'Wissen' },
    { name: 'Zeit', text: 'Zeit' },
    { name: 'Entscheidungsmacht', text: 'Ent&shy;schei&shy;dungs&shy;macht' },
    { name: 'Gesundheit', text: 'Ge&shy;sund&shy;heit' },
    { name: 'Gewalt', text: 'Gewalt' },
  ],
  einleitung: {
    linkName: 'Einführung',
    title: 'Gleichstellungsorientierte Gesetzesfolgenabschätzung',
    infoTitle: 'Hinweis zu: Gleichstellungsorientierte Gesetzesfolgenabschätzung',
    infoText: `Die Arbeitshilfe basiert auf:
          <ul><li><strong>Art. 3 Abs. 2 Grundgesetz (GG)</strong>: „Männer und Frauen sind gleichberechtigt. Der Staat fördert die tatsächliche Durchsetzung der Gleichberechtigung von Frauen und Männern und wirkt auf die Beseitigung bestehender Nachteile hin.“</li>
          <li><strong>Art. 3. Abs. 3 Vertrag über die Europäische Union (EUV) i. V. m. Art. 8 Vertrag über die Arbeitsweise der Europäischen Union (AEUV)</strong>: Sie machen die Förderung der Gleichstellung von Frauen und Männern zu einem Teil der Rechtsordnung der Europäischen Union.</li>
          <li><strong>Art. 157 Abs. 1 AEUV</strong> erhebt die Entgeltgleichheit für Frauen und Männer bei gleicher oder gleichwertiger Arbeit zu einem Grundsatz, dessen Anwendung von jedem Mitgliedstaat der Europäischen Union sichergestellt werden muss. <strong>Art. 157 Abs. 4 AEUV</strong> erlaubt ausdrücklich spezifische Vergünstigungen zur Erleichterung der Berufstätigkeit des unterrepräsentierten Geschlechts oder zur Verhinderung beziehungsweise zum Ausgleich von Benachteiligungen in der beruflichen Laufbahn.</li>
          <li>Auch die <strong>Charta der Grundrechte der Europäischen Union (GRCh)</strong> sieht in <strong>Art. 23 Abs. 1</strong> vor, Gleichheit von Frauen und Männern in allen Bereichen sicherzustellen. Nach Art. 23 Abs. 2 dürfen dafür spezifische Vergünstigungen für das unterrepräsentierte Geschlecht beibehalten oder eingeführt werden.</li>
          <li><strong>§ 4 Bundesgleichstellungsgesetz (BGleiG)</strong>: Hier werden alle Beschäftigten der Bundesverwaltung verpflichtet, die Gleichstellung von Frauen und Männern zu fördern und diese Verpflichtung als durchgängiges Leitprinzip bei allen Aufgabenbereichen und Entscheidungen der Dienststelle zu berücksichtigen. Abs. 3 verpflichtet die Bundesverwaltung, die Gleichstellung von Frauen und Männern auch sprachlich zum Ausdruck zu bringen.</li>
          <li><strong>§ 2 und Kapitel 6 (Rechtsetzung) Gemeinsame Geschäftsordnung der Bundesministerien (GGO)</strong>:
          <ul>
          <li><strong>§ 2 GGO</strong> bestimmt die Förderung der Gleichstellung von Frauen und Männern zum Leitprinzip bei allen politischen, normgebenden und verwaltenden Maßnahmen.</li>
          <li><strong>§ 45 Abs. 1 i. V. m. Anlage 6 Nr. 9 a GGO</strong> schreibt die Beteiligung des Bundesministeriums für Familie, Senioren, Frauen und Jugend (BMFSFJ) zu der Frage vor, ob durch das Gesetz beziehungsweise die Rechtsverordnung (dazu <strong>§ 62 Abs. 2 GGO</strong>) Auswirkungen von gleichstellungspolitischer Bedeutung zu erwarten sind.</li>
          <li>Nach <strong>§ 43 Nr. 5 GGO</strong> sind in der Begründung die Gesetzesfolgen (<strong>§ 44 Abs. 1 GGO</strong>) darzustellen. Diese beabsichtigten oder unbeabsichtigten Auswirkungen einer Regelung sind – auch hinsichtlich ihrer gleichstellungspolitischen Bedeutung – zu analysieren und in der Begründung darzustellen.</li>
          <li><strong>§ 42 Abs. 5 GGO</strong> verpflichtet, die Gleichstellung sprachlich zum Ausdruck zu bringen.</li>
          </ul>
          </li>
          <li>Das <strong>Bundesgremienbesetzungsgesetz (BGremBG)</strong> verpflichtet den Bund, darauf hinzuwirken, dass eine gleichberechtigte Teilhabe von Frauen und Männern in Gremien, für die er Berufungs- oder Entsenderechte hat, geschaffen wird.</li></ul>`,
    text: `<p>In diesem Modul beschreiben Sie die spezifischen Auswirkungen Ihres Regelungsvorhabens auf Menschen mit Behinderungen gegenüber Menschen ohne Behinderungen. Vor allem sollen dabei eine etwaige Benachteiligung sowie mögliche Beteiligungsdefizite von Menschen mit Behinderungen identifiziert werden.</p>
          <p>Anschließend können Sie die Beschreibung für Vorblatt und Begründung Ihres Regelungsvorhabens verwenden.</p>`,
    content: {
      paragraph1: {
        content1:
          'Das Grundgesetz verpflichtet den Staat zur Förderung der <strong>tatsächlichen Durchsetzung der Gleichberechtigung von Frauen und Männern</strong> und zur Hinwirkung auf die <strong>Beseitigung bestehen­der Nachteile</strong> (<a target="_blank" style="white-space: normal" href="https://www.gesetze-im-internet.de/gg/art_3.html">Art. 3 Abs. 2 S. 2 GG</a>). Die Gemeinsame Geschäftsordnung der Bundesministerien (GGO) erhebt die <strong>Gleichstellung von Frauen und Männern zum durchgängigen Leitprinzip</strong> (<a target="_blank" style="white-space: normal" href="{{GGO_LINK_2}}">§ 2 GGO</a>)',
        content2: '.',
        info: {
          title: 'Rechtliche Vorgaben',
        },
      },
      paragraph2: {
        content1:
          'Dieses Modul unterstützt Sie bei der Vorbereitung von Rechtsvorschriften und der gleich­stellungsorientierten Gesetzesfolgenabschätzung. Bei <strong>jedem</strong> Rechtsetzungsvorhaben ist zu prüfen, ob und gegebenenfalls wie die Gleichstellung der Geschlechter gefördert werden kann. Dabei geht es ganz konkret darum, fachlich zu prüfen, ob das Regelungsvorhaben <strong>unterschiedliche Wirkungen auf die Lebenssituationen von Frauen und Männern</strong> hat. Es sollen negative wie auch positive Auswirkungen und Potenziale eines Regelungsvorhabens systematisch ermittelt und Maßnahmen zur Verbesserung der Regelung abgeleitet werden. <strong>Bewertungsmaßstab</strong> sollte die <strong>Gleichstellungsstrategie der Bundesregierung</strong>',
        content2:
          'sein. Die Betrachtung kann sich auch bei Regelungsvorhaben lohnen, deren Ziel schon die Gleichstellung von Frauen und Männern ist. Die Erkenntnisse dieser Betrachtung können Unterstützung bei der Begründung der Regelungsnotwendigkeit bieten. Gleichzeitig kann die Arbeitshilfe auch dabei helfen, unerwünschte Nebenwirkungen dieser Regelungen aufzudecken.',
        info: {
          title: 'Die Gleichstellungsstrategie der Bundesregierung als Bewertungsmaßstab',
          content: `<p>Gemäß der <a target="_blank" style="white-space: normal" href="https://www.gleichstellungsstrategie.de/">Gleichstellungsstrategie der Bundesregierung</a> sollen Frauen und Männer im Lebensverlauf gleichermaßen gut von ihrem Einkommen leben, sich beruflich entwickeln und Fürsorgeaufgaben wahrnehmen können. Sie sollen gleichermaßen an der Gestaltung der Zukunft des Landes in Wirtschaft, Politik, Kultur und Wissenschaft beteiligt sein. Die Bundesregierung hat sich zur Aufgabe gemacht, die Gleichberechtigung zwischen Frauen und Männern in allen Politikbereichen herzustellen<br><br>Die Strategie leitet hieraus neun gleichstellungspolitische Ziele ab:</p>
                  <h3>Wie schaffen wir es, dass Frauen und Männer im Lebensverlauf gleichermaßen gut von ihrem Einkommen leben, sich beruflich entwickeln und Fürsorgeaufgaben wahrnehmen können?</h3>
                  <ol>
                  <li>Ziel 1: Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf</li>
                  <li>Ziel 2: Soziale Berufe als attraktive und durchlässige Karriereberufe stärken</li>
                  <li>Ziel 3: Gleichstellungspolitische Standards in der digitalen Lebens- und Arbeitswelt</li>
                  <li>Ziel 4: Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern</li>
                  </ol>
                  <h3>Wie schaffen wir es, dass Frauen und Männer gleichermaßen an der Gestaltung der Zukunft unseres Landes in Wirtschaft, Politik, Kultur und Wissenschaft beteiligt sind?</h3>
                  <ol>
                  <li>Ziel 5: Gleichberechtigte Karrierechancen und Teilhabe von Frauen und Männern in Führungspositionen</li>
                  <li>Ziel 6: Gleichberechtigte Beteiligung von Frauen in Parlamenten auf allen Ebenen</li>
                  <li>Ziel 7: Gleichberechtigte Präsenz und Teilhabe von Frauen und Männern in Kultur und Wissenschaft</li>
                  </ol>
                  <h3>Wie kann die Bundesregierung Gleichberechtigung zwischen Frauen und Männern in allen Politikbereichen herstellen?</h3>
                  <ol>
                  <li>Ziel 8: Der öffentliche Dienst des Bundes baut bei der Vereinbarkeit und gleichberechtigten Teilhabe an Führungspositionen seine Vorreiterrolle aus.</li>
                  <li>Ziel 9: Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.</li>
                  </ol>
                  <p>Ergänzend formuliert der Bericht „Agenda 2030 – Nachhaltige Familienpolitik“ konkrete und überprüfbare Entwicklungsziele für die Familienpolitik bis zum Jahr 2030. Hier hat sich die Bundesregierung in Anlehnung an SDG 5 „Geschlechtergerechtigkeit“ einige Ziele gesetzt, die zu einer weiteren Gleichstellung der Geschlechter bis 2030 beitragen sollen:</p>
                  <ul>
                  <li>Die Müttererwerbstätigkeit nähert sich der Vätererwerbstätigkeit weiter an.</li>
                  <li>Rund 80 Prozent der erwerbstätigen Mütter erzielen ein existenzsicherndes Erwerbseinkommen.</li>
                  <li>Rund jeder zweite Vater nimmt Elternzeit, bezieht Elterngeld und nimmt sich damit Zeit für seine Kinder.</li>
                  <li>Der Abstand bei der Zeit, die Väter und Mütter minderjähriger Kinder in die Familie investieren, reduziert sich um 30 Prozentpunkte.</li>
                  </ul>
                  `,
        },
      },
      paragraph3: {
        content:
          'Sofern auch Auswirkungen zu erwarten sind, die Personen mit dem personenstandsrechtlichen Geschlechtseintrag „divers“ oder mit offenem Geschlechtseintrag in spezifischer Weise betreffen, sollten diese Auswirkungen bei der Prüfung von Gesetzesfolgen berücksichtigt werden.',
      },
      paragraph4: {
        title: 'Hinweise zur Gleichstellung und Geschlechtergerechtigkeit in der Sprache',
        content: `<p>In <a href="https://www.gesetze-im-internet.de/bgleig_2015/__4.html" target="_blank">§ 4 Abs. 3</a> des <strong>Bundesgleichstellungsgesetzes</strong> (BGleiG) heißt es zur geschlechtergerechten Sprache: „Rechts- und Verwaltungsvorschriften des Bundes sollen die Gleichstellung von Frauen und Männern auch sprachlich zum Ausdruck bringen. Dies gilt auch für den dienstlichen Schriftverkehr.“</p>
                <p>Das <strong>Handbuch der Rechtsförmlichkeit</strong> gibt in <a href="https://hdr.bmj.de/page_b.1.html#an_110" target="_blank">Teil B, Nr. 1.8 (Rn. 110−123)</a> Hinweise zur sprachlichen Gleichbehandlung von Frauen und Männern in Gesetzen und Rechtsverordnungen.</p>`,
      },
    },
  },
  relevanzpruefung1: {
    linkName: 'Relevanzprüfung: Teil 1',
    title: 'Relevanzprüfung: Teil 1',
    text: '<p>Die Relevanzprüfung ist eine Vorprüfung zu Gleichstellungswirkungen des Regelungsvorhabens. Ermitteln Sie in der Relevanzprüfung zunächst, ob eine vertiefende Betrachtung hinsichtlich geschlechterdifferenzierter Gesetzesfolgen geboten erscheint. Dies ist der Fall, wenn Anhaltspunkte dafür vorliegen, dass sich ein Regelungsvorhaben unterschiedlich auf Frauen und Männer auswirken kann.</p> <p>Machen Sie sich dazu zunächst Gedanken über die Ziele und die Zielgruppe sowie weitere betroffene Akteure Ihres Regelungsvorhabens.</p>',
    enapText:
      'Im Modul <strong>„eNAP - elektronische Nachhaltigkeitsprüfung“</strong> haben Sie das <strong>„SDG 5 Geschlechtergerechtigkeit“</strong> als relevant für das Regelungsvorhaben eingestuft. Dort sind folgende Texte hinterlegt:',
    subheading1: 'Angaben zur Relevanz',
    question1: 'Hat das Regelungsvorhaben die Gleichstellung der Geschlechter als explizites Ziel?',
  },
  relevanzpruefung2: {
    linkName: 'Relevanzprüfung: Teil 2',
    title: 'Relevanzprüfung: Teil 2',
    introText:
      '<p>Prüfen Sie, ob Sie bei Ihren Überlegungen zwischen verschiedenen Zielgruppen und Szenarien differenziert haben. Bei „dem Normadressaten“ handelt es sich für gewöhnlich nicht um eine homogene Gruppe, sondern um Menschen mit unterschiedlichen Merkmalen und in verschiedenen Lebenssituationen.</p>',
    question1: 'Sind durch das Regelungsvorhaben Frauen und Männer mittelbar oder unmittelbar betroffen?',
    question1Info:
      '<p><strong>Unmittelbar</strong> betroffen sind diejenigen Personen, die Zielgruppe des Regelungsvorhabens sind.</p><p><strong>Mittelbar</strong> betroffen sind diejenigen Personen, auf die das Regelungsvorhaben Auswirkungen hat, obwohl sie nicht Zielgruppe des Vorhabens sind.</p>',
    text: 'Jedes Regelungsvorhaben wirkt sich innerhalb der im Regelungsfeld vorhandenen Strukturen aus. Diese Strukturen sind beispielsweise die Einkommensverteilung, der Bildungsstand oder die Altersstruktur der betroffenen Akteure. Wenn es im Regelungsfeld systematische Unterschiede zwischen Frauen und Männern gibt, so wirkt auch das Regelungsvorhaben potenziell unterschiedlich auf Frauen und Männer. Dies ist insbesondere der Fall, wenn Frauen oder Männer in einer bestimmten Personengruppe unter- oder überrepräsentiert sind oder Ressourcen zwischen den Geschlechtern ungleich verteilt sind.',
    hiddenInfo1: {
      title: 'Beispiel',
      content:
        '<p>In Deutschland liegt die Erwerbsquote von Frauen derzeit unter der von Männern. So können sich scheinbar neutrale Vorhaben, die im Zusammenhang mit der Erwerbstätigkeit stehen, unterschiedlich auf Frauen und Männer auswirken.</p> <p>Prüfen Sie also zunächst, ob es systematische Unterschiede zwischen Frauen und Männern (Geschlechterstrukturen) im Regelungsfeld und damit möglicher­weise unterschiedliche Auswirkungen der Regelung gibt.</p>',
    },
    hiddenInfo2: {
      title: 'Weitere Anhaltspunkte',
      content: {
        text1:
          'Anhaltspunkte für unterschiedliche Auswirkungen auf Frauen und Männer bestehen vor allem dann, wenn ein Regelungsvorhaben in einem der folgenden <strong>Lebensbereiche</strong> wirkt. Diese sieben Kernbereiche entsprechen denen des Gleichstellungsindex (Gender Equality Index) des Europäischen Instituts für Gleichstellungsfragen (EIGE).',
        text2:
          'In der Hauptprüfung werden diese Bereiche ausdifferenziert und weitere Informationen zu bestehenden Geschlechterstrukturen bereitgestellt.',
      },
    },
    question2:
      'Bestehen – auf Grundlage Ihrer Überlegungen – Anhaltspunkte dafür, dass das Regelungsvorhaben unterschiedlich auf Frauen und Männer wirkt?',
    question2Info:
      '<p><span>Eine unterschiedliche Wirkung kann dadurch entstehen, dass …</span><ul><li>… das verfolgte Ziel für Frauen und Männer nicht in gleicher Weise erreicht wird.</li><li>… Frauen und Männer von vorgesehenen Vorteilen nicht in gleicher Weise profitieren.</li><li>… Frauen oder Männer durch das Vorhaben in besonderer Weise belastet werden.</li></ul></p>',
  },
  hauptpruefung: {
    linkName: 'Hauptprüfung',
    title: 'Hauptprüfung',
    text: `<p>Die Hauptprüfung dient dazu, die <strong>gleichstellungspolitischen Wirkungen des Vorhabens zu identifizieren</strong>. Untersuchen Sie dazu vertieft die Auswirkungen des Regelungsvorhabens in verschiedenen Bereichen. Achten Sie dabei besonders auf verdeckte Benachteiligungen, Beteiligungsdefizite und die <strong>Verfestigung tradierter Rollenmuster</strong>. Bedenken Sie, dass sich neben <strong>biologischen Unterschieden auch Verhaltens- und Bedürfnismuster sowie der Zugang zu Informationen</strong> in einem bestimmten Themenfeld zwischen den Geschlechtern unterscheiden können.</p>
              <p>Jedem <strong>Kernbereich</strong> sind mehrere <strong>Bereiche</strong> zugeordnet, die eine Hilfestellung geben können, mögliche Wirkungen Ihres Regelungsvorhabens zu identifizieren und gleichstellungsrelevante Punkte zu berücksichtigen. Die Lebensbereiche sind so konzipiert, dass durch sie viele <strong>mögliche Wirkzusammenhänge</strong> abgedeckt und <strong>Denkanstöße zu versteckten Nebenwirkungen</strong> gesetzlicher Regelungen gegeben werden. Gleichwohl ist es schlicht nicht möglich, im Vorhinein alle denkbaren Wirkzusammenhänge gesetzlicher Regelungen abzubilden.</p>
              <p>Im Folgenden finden Sie eine Sammlung von Beispielen zu Wirkbereichen. Diese zeigen gleichstellungsrelevante Wirkungen auf und verdeutlichen die teilweise verdeckten Wirkzusammenhänge gesetzlicher Regelungen im Geschlechterkontext. Jedem Bereich sind inhaltlich passende Ziele aus der Gleichstellungsstrategie der Bundesregierung zugeordnet.</p>`,
    arbeit: {
      linkName: 'Arbeit',
      title: 'Arbeit',
      options: [
        {
          key: 'sorgearbeit',
          title: 'Sorgearbeit und Haushaltsführung',
          drawerContent: {
            introText:
              'Frauen wenden durchschnittlich mehr Zeit für die unbezahlte Hausarbeit und Betreuung innerhalb der Familie auf als Männer. Der Indikator für den Unterschied in der Zeitverwendung („Gender Care Gap“) wird aus Daten der Zeitverwendungsstudie des Statistischen Bundesamts ermittelt (<a target="_blank" href="https://www.destatis.de/DE/Themen/Gesellschaft-Umwelt/Einkommen-Konsum-Lebensbedingungen/Zeitverwendung/Tabellen/aktivitaeten-geschlecht-zve.html">DESTATIS 2019</a>).',
            ergebnisse: [
              'Frauen leisten im Schnitt täglich 52 Prozent mehr unbezahlte Sorgearbeit als Männer (Erziehung von Kindern, Hausarbeit, Pflege von Angehörigen, Ehrenämter; Erhebung 2012/2013) (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 20</a>). Dabei zeigen sich die größten Unterschiede bei den 34-Jährigen: Hier beträgt der Gender Care Gap 111 Prozent. Frauen verbringen täglich im Schnitt fünf Stunden und 18 Minuten mit Care-Arbeit, Männer hingegen zwei Stunden und 31 Minuten (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/themen/gleichstellung/gender-care-gap/indikator-fuer-die-gleichstellung/gender-care-gap---ein-indikator-fuer-die-gleichstellung/137294">BMFSFJ 2019</a>).',
              'Obwohl der Anteil pflegender Männer seit den 1990ern gestiegen ist, machen Frauen immer noch die Mehrheit der pflegenden Personen aus. Der Frauenanteil steigt dabei mit dem Pflegeumfang (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 88</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern',
            ],
          },
        },
        {
          key: 'erwerbsarbeit',
          title: 'Erwerbsarbeit und Arbeitslosigkeit',
          drawerContent: {
            introText:
              'Die Erwerbsquote von Frauen liegt unter der von Männern (<a target="_blank" href="https://statistik.arbeitsagentur.de/DE/Statischer-Content/Statistiken/Themen-im-Fokus/Frauen-und-Maenner/generische-Publikationen/Frauen-Maenner-Arbeitsmarkt.html">Bundesagentur für Arbeit 2019: 6 f.</a>), vor allem liegen die Erwerbsarbeitszeitvolumen deutlich auseinander.',
            ergebnisse: [
              'Als „Nichterwerbspersonen“ werden Menschen im erwerbsfähigen Alter bezeichnet, die weder einem Erwerb nachgehen noch arbeitsuchend erfasst werden. Diese Gruppe machte 2019 mehr als 11,1 Millionen Personen aus, davon über 6,6 Millionen Frauen und rund 4,5 Millionen Männer (Agentur für Querschnittsziele im ESF 2020). (Die Quelle mit Stand 2020 ist online nicht mehr zu finden)',
              'Die Quote der Teilzeitbeschäftigten an den sozialversicherungspflichtigen Beschäftigten betrug bei Frauen 48 Prozent und bei Männern 11 Prozent (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 14</a>).',
            ],
            ziele: ['Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf'],
          },
        },
        {
          key: 'aufstiegschancen',
          title: 'Aufstiegschancen',
          drawerContent: {
            introText:
              'Frauen sind in Führungspositionen der Privatwirtschaft und des öffentlichen Dienstes unterrepräsentiert. Die Gründe hierfür sind vielfältig (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 86</a>).',
            ergebnisse: [
              'Seit dem Jahr 2012 hat sich der Anteil der weiblichen Führungskräfte (Vorstände und Geschäftsführerinnen sowie Führungskräfte in Handel, Produktion und Dienstleistungen) kaum verändert: 2018 war jede dritte Führungskraft (29,4 Prozent) weiblich, das sind 0,8 Prozentpunkte mehr als 2012 (<a target="_blank" href="https://www.destatis.de/DE/Themen/Arbeit/Arbeitsmarkt/Qualitaet-Arbeit/Dimension-1/frauen-fuehrungspositionen.html">DESTATIS o. J.a</a>).',
              'Im Bereich der Wissenschaft machen Verlaufsanalysen (Studienbeginn 1999 bis Berufung (2016−2018)) deutlich, dass dem Wissenschaftssystem ab der Promotion Frauen „verlorengehen“: Bei den Studienberechtigten liegt der Frauenanteil noch etwas höher als der Männeranteil, sowohl am Anfang als auch beim Abschluss sind Studentinnen und Studenten etwa gleichverteilt. Beim Übergang zur Promotion sinkt dann der Frauenanteil auf 44 Prozent und bei der Habilitation auf 29 Prozent. Bei den Berufungszahlen liegt der Frauenanteil im Durchschnitt bei 33 Prozent. Dieses Phänomen wird als Leaky Pipeline bezeichnet (<a target="_blank" href="https://www.gesis.org/cews/unser-angebot/informationsangebote/statistiken/thematische-suche/detailanzeige/article/frauen-und-maenneranteile-im-qualifikationsverlauf-analyse-idealtypischer-karriereverlaeufe-studienbeginn-bis-berufung">CEWS 2019</a>).',
              'Im Bereich des Öffentlichen Dienstes wurde durch die Auswertung von Beurteilungsrichtlinien und -statistiken nach gewiesen, dass sowohl Verfahren als auch Kriterien zur Leistungsbeurteilung zum Teil nicht objektiv sind. Stereotype Annahmen zum Leistungs- und Leitungsverhalten von Beschäftigten (zum Beispiel geringere Leistungsfähigkeit bei Teilzeitbeschäftigten; Initiative und Eigenständigkeit bei Männern gegenüber Teamfähigkeit und Servicebereitschaft bei Frauen) können die Leistungsbewertung und somit die Karrieren von Frauen und Männern beeinflussen (<a target="_blank" href="https://d-nb.info/1209709236/34">Frey 2015</a>).',
            ],
            ziele: ['Gleichberechtigte Karrierechancen und Teilhabe von Frauen und Männern in Führungspositionen'],
          },
        },
        {
          key: 'arbeitsrecht',
          title: 'Arbeitsrecht und Arbeitszeitgestaltung',
          drawerContent: {
            introText:
              'Teilzeitarbeit und Minijobs sind Frauendomänen. Mehr als viermal so viele Frauen wie Männer arbeiteten 2018 in Teilzeit (<a target="_blank" href="https://statistik.arbeitsagentur.de/DE/Statischer-Content/Statistiken/Themen-im-Fokus/Frauen-und-Maenner/generische-Publikationen/Frauen-Maenner-Arbeitsmarkt.html">Bundesagentur für Arbeit 2019: 10</a>).',
            ergebnisse: [
              'Frauen arbeiten häufig in Pflegeberufen. Diese sind selten durch allgemeinverbindliche Tarifverträge bestimmt, sondern fallen häufig unter das kirchliche Arbeitsrecht oder sind im privat-gewerblichen Bereich angesiedelt, wo Tarifverträge nicht vorliegen. Diese Zersplitterung in unterschiedliche Tarifregelungen macht es schwer, eine gemeinsame Interessengruppe zu bilden und eine Lohnerhöhung für Pflegeberufe durchzusetzen (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 151</a>).',
              'Ganz überwiegend sind es Frauen, die einen Minijob innehaben, im gewerblichen Bereich über 58 Prozent, in den Privathaushalten sogar rund 90 Prozent (<a target="_blank" href="https://www.minijob-zentrale.de/DE/02_fuer_journalisten/02_berichte_trendreporte/quartalsberichte_archiv/2020/1_2020.pdf">Minijob-Zentrale 2020</a>). Problematisch an diesem Beschäftigungsverhältnis ist die begrenzte soziale Absicherung (Gesetzliche Unfallversicherung und Rentenversicherung, soweit nicht eine Befreiung beantragt wird). Insbesondere die überwiegend fehlende Altersvorsorge macht Minijobs laut Deutschem Institut für Wirtschaftsforschung zu einer „riskanten Beschäftigungsform“ (<a target="_blank" href="https://www.diw.de/documents/publikationen/73/diw_01.c.397546.de/diw_sp0436.pdf">Klenner und Schmidt 2012</a>).',
            ],
            ziele: [
              'Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf',
              'Gleichstellungspolitische Standards in der digitalen Lebens- und Arbeitswelt',
            ],
          },
        },
        {
          key: 'berufswahl',
          title: 'Berufswahl und Berufsausübung',
          drawerContent: {
            introText:
              'Frauen und Männer setzen nach wie vor unterschiedliche Schwerpunkte in ihrer Berufswahl und bei den Branchen, in denen sie tätig sind. Allgemein sind Frauen eher im Dienstleistungssektor (Gesundheits- und Sozialwesen, Erziehung und Unterricht, Öffentliche Verwaltung) tätig. Männer sind hingegen vor allem in der Industrie, dem Verkehr sowie im Baugewerbe beschäftigt (<a target="_blank" href="https://statistik.arbeitsagentur.de/DE/Statischer-Content/Statistiken/Themen-im-Fokus/Frauen-und-Maenner/generische-Publikationen/Frauen-Maenner-Arbeitsmarkt.html">Bundesagentur für Arbeit 2019: 12 f.</a>).',
            ergebnisse: [
              'Die Wahl der Ausbildungsberufe erfolgt deutlich nach geschlechtsbezogenen Mustern: Von allen Männern, die im Jahr 2019 neu einen Ausbildungsberuf wählten, wählten 23 Prozent diese fünf Berufe: Kraftfahrzeugmechatroniker, Fachinformatiker, Elektroniker, Anlagenmechaniker für Sanitär-, Heizungs- und Klimatechnik sowie Industriemechaniker (<a target="_blank" href="https://www.bibb.de/dokumente/pdf/naa309/naa309_2019_tab068_0bund.pdf">BIBB 2019b</a>). Von allen Frauen, die in 2019 einen Ausbildungsberuf wählten, wählten 37 Prozent diese fünf Berufe: Kauffrau für Büromanagement, Medizinische Fachangestellte, Zahnmedizinische Fachangestellte, Verkäuferin sowie Kauffrau im Einzelhandel (<a target="_blank" href="https://www.bibb.de/dokumente/pdf/naa309/naa309_2019_tab069_0bund.pdf">BIBB 2019a</a>).',
              'Die Studienfachwahl weist hohe Geschlechterdisparitäten auf: Die Ingenieurwissenschaften weisen den geringsten Frauenanteil auf (25 Prozent), innerhalb des Faches bestehen aber Unterschiede. Die Informatik, Elektrotechnik sowie Verkehrstechnik und Nautik haben einen Frauenanteil von unter 25 Prozent, Architektur und Raumplanung weisen dagegen 55 bis 62 Prozent Studienanfängerinnen auf. Einen überdurchschnittlich hohen Frauenanteil von 60 bis über 80 Prozent verzeichnen die Geisteswissenschaften, die Kunst und die Kunstwissenschaften, die Humanmedizin und die Gesundheitswissenschaften, die Psychologie, das Sozialwesen, die Erziehungswissenschaft und die Veterinärmedizin (<a target="_blank" href="https://www.bildungsbericht.de/de/bildungsberichte-seit-2006/bildungsbericht-2020/pdf-dateien-2020/bildungsbericht-2020.pdf">Autorengruppe Bildungsberichterstattung 2020: 191 f.</a>).',
            ],
            ziele: [
              'Soziale Berufe als attraktive und durchlässige Karriereberufe stärken',
              'Gleichberechtigte Präsenz und Teilhabe von Frauen und Männern in Kultur und Wissenschaft',
            ],
          },
        },
        {
          key: 'arbeitsteilung',
          title: 'Arbeitsteilung in Partnerschaften',
          drawerContent: {
            introText:
              'Vor der Familiengründung verteilen sich die Arbeitszeiten von Frauen und Männern ähnlich. Nach der Gründung einer Familie und dem Ende einer etwaigen Elternzeit ergibt sich ein anderes Bild. Insbesondere in Westdeutschland herrscht das Bild einer Zuverdiener-Familie vor. Dieser Effekt tritt bei Müttern mit Migrationshintergrund noch deutlicher hervor. Ihre Erwerbsquote liegt deutlich unter jener von Müttern ohne Migrationshintergrund (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 87</a>).',
            ergebnisse: [
              'Mütter nehmen durchschnittlich deutlich häufiger und länger Elternzeit als Väter. Von den 1,9 Millionen Personen, die in 2019 Elterngeld erhielten, waren 1,41 Millionen Frauen und 456.000 Männer. Dabei betrug die geplante Bezugsdauer bei Frauen (ausschließlich Basiselterngeld) durchschnittlich 11,7 Monate (Elterngeld Plus: 19,9 Monate), bei Männern betrug die angestrebte Bezugsdauer durchschnittlich 2,9 Monate (Elterngeld Plus: 8,5 Monate) (<a target="_blank" href="https://www.destatis.de/DE/Presse/Pressemitteilungen/2020/03/PD20_104_22922.html">DESTATIS 2020</a>).',
              'Frauen leisten im Schnitt täglich 52 Prozent mehr unbezahlte Sorgearbeit als Männer (Erziehung von Kindern, Hausarbeit, Pflege von Angehörigen, Ehrenämter; Erhebung 2012/2013) (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 20</a>). Dabei zeigen sich die größten Unterschiede bei den 34-Jährigen: Hier beträgt der Gender Care Gap 111 Prozent. Frauen verbringen täglich im Schnitt fünf Stunden und 18 Minuten mit Care-Arbeit, Männer hingegen zwei Stunden und 31 Minuten (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/themen/gleichstellung/gender-care-gap/indikator-fuer-die-gleichstellung/gender-care-gap---ein-indikator-fuer-die-gleichstellung/137294">BMFSFJ 2019</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern',
            ],
          },
        },
        {
          key: 'wertschaetzung',
          title: 'Gesellschaftliche Wertschätzung',
          drawerContent: {
            introText:
              'Häufig wird „Arbeit“ mit Erwerbsarbeit gleichgesetzt. Unbezahlte Tätigkeiten, wie zum Beispiel die Führung des Haushalts und die Versorgung der (arbeitenden) Familienmitglieder sowie der Kinder und pflegebedürftiger Verwandter, werden seltener direkt mit Arbeit assoziiert, obwohl die Care-Arbeit das gesellschaftliche Überleben sichert (<a target="_blank" href="https://www.diw.de/de/diw_01.c.616037.de/publikationen/wochenberichte/2019_10_3/auch_an_erwerbsfreien_tagen_erledigen_frauen_einen_grossteil_der_hausarbeit_und_kinderbetreuung.html">Samtleben 2019: 140 f.</a>). Aber auch die bezahlten Care-Berufe sind häufig von geringen Löhnen und schlechten Arbeitsbedingungen geprägt.',
            ergebnisse: [
              'Frauen leisten im Schnitt mehr unbezahlte Sorgearbeit als Männer. Zwei Drittel der Hautpflegepersonen in Privathaushalten mit pflegebedürftigen Personen sind weiblich (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 20</a>).',
              'Private Haushalte wendeten im Jahr 2013 35 Prozent mehr Zeit für bezahlte Erwerbsarbeit als für unbezahlte Sorgearbeit auf. Der Wert dieser Arbeit entspricht etwa einem Drittel der im Bruttoinlandsprodukt ausgewiesenen Bruttowertschöpfung (987 Milliarden Euro). Somit liegt sie deutlich über der Wertschöpfung im Produzierenden Gewerbe (769 Milliarden Euro) (<a target="_blank" href="https://www.destatis.de/DE/Methoden/WISTA-Wirtschaft-und-Statistik/2016/02/unbezahlte-arbeit-022016.pdf">Schwarz und Schwahn 2016: 46</a>).',
              'In den in der Coronakrise systemrelevanten Berufsgruppen der „ersten Stunde“ liegt der Anteil an Frauen bei 60 Prozent, gleichzeitig sind dies größtenteils unterdurchschnittlich bezahlte Berufe (<a target="_blank" href="https://www.diw.de/documents/publikationen/73/diw_01.c.792728.de/diw_aktuell_48.pdf">Koebe et al. 2020</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern',
              'Soziale Berufe als attraktive und durchlässige Karriereberufe stärken',
            ],
          },
        },
      ],
    },
    geld: {
      linkName: 'Geld',
      title: 'Geld',
      options: [
        {
          key: 'steuern',
          title: 'Steuern',
          drawerContent: {
            introText:
              'Obwohl Steuergesetze geschlechtsneutral formuliert sind, haben einzelne Regelungen zum Beispiel durch Anreizsetzungen bei der Erwerbstätigkeit unterschiedliche Effekte auf das Einkommen jeweils von Frauen und Männern.',
            ergebnisse: [
              'Das Ehegattensplitting bewirkt eine steuerlich höhere Belastung des Einkommens des Partners beziehungsweise der Partnerin, der oder die weniger verdient (<a target="_blank" href="https://www.bundesfinanzministerium.de/Content/DE/Standardartikel/Ministerium/Geschaeftsbereich/Wissenschaftlicher_Beirat/Gutachten_und_Stellungnahmen/Ausgewaehlte_Texte/2018-09-27-Gutachten-Besteuerung-von-Ehegatten-anlage.pdf">BMF 2018: 23</a>). Denn durch das Ehegattensplitting wird das zu versteuernde Einkommen beider Eheleute beziehungsweise Lebenspartnerinnen und -partner halbiert, und jede Hälfte unterliegt dem gleichen Steuersatz. Damit unterliegt das Einkommen der Person mit dem geringeren Einkommen – häufiger ist dies die Frau – einer höheren steuerlichen Belastung, als es dem individuellen Einkommen entspricht. Somit nehmen für Erstverdienende (häufiger Männer) die Erwerbsanreize zu, während der hohe Grenzsteuersatz die Erwerbstätigkeit für die Person mit dem geringeren Einkommen (häufiger Frauen) teurer macht. Für Zweitverdienende wird damit der Erwerbsanreiz geschwächt.',
              'Darüber hinaus kann die Wahl der Lohnsteuerklassen III und V von Eheleuten zu zunächst unterschiedlichen Steuerbelastungen innerhalb der Ehe beziehungsweise Partnerschaft führen, meistens zum Nachteil von Frauen, deren Höhe sich aber bei der Zusammenveranlagung ändern kann. Die Berechnung der Lohnsteuer anhand der am häufigsten genutzten Steuerklassenkombination III/V führt dazu, dass die Lohnsteuer in Steuerklasse V, in der zu circa 90 Prozent Frauen sind, sehr hoch und der Nettolohn entsprechend niedrig ausfällt. Dazu kommt, dass Lohnersatzleistungen, wie Krankengeld, Arbeitslosengeld oder Elterngeld als prozentualer Anteil des Nettolohns berechnet werden. Daher fallen auch diese Leistungen bei gleichem Bruttoeinkommen und gleichen Sozialversicherungsbeiträgen in Steuerklasse V erheblich geringer aus als in Steuerklasse III − in der Regel zum Nachteil von Frauen (<a target="_blank" href="https://www.boeckler.de/de/pressemitteilungen-2675-lohnsteuerklasse-v-reduziert-lohnersatzleistungen-drastisch-25384.hmt">Hans-Böckler-Stiftung 2020b</a>; <a target="_blank" href="https://www.boeckler.de/fpdf/HBS-007819/p_fofoe_WP_190_2020.pdf">Spangenberg et al. 2020</a>).',
            ],
            ziele: ['Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf'],
          },
        },
        {
          key: 'einkommen',
          title: 'Einkommen',
          drawerContent: {
            introText: 'Frauen verdienen im Durchschnitt weniger als Männer.',
            ergebnisse: [
              'Der durchschnittliche Bruttostundenverdienst von Frauen ist um 20 Prozent niedriger als der von Männern (Stand 2019). Allerdings bestehen erhebliche Unterschiede zwischen alten und neuen Bundesländern. Im Osten beträgt der Unterschied 7 Prozent, im Westen Deutschlands (und Berlin) 21 Prozent (unbereinigter Gender Pay Gap) (<a target="_blank" href="https://www.destatis.de/DE/Themen/Arbeit/Verdienste/Verdienste-GenderPayGap/Tabellen/ugpg-01-gebietsstand.html">DESTATIS o. J.c</a>).',
              'Auch bei gleichen Rahmenbedingungen ist das Einkommen von Frauen oft geringer. Bei einer 38-Stunden-Woche und zehn Jahren Berufserfahrung gibt es Unterschiede im Bruttoarbeitslohn in verschiedenen Berufen: So verdienen Filial- oder Verkaufsstellenleiterinnen 18 Prozent weniger als ihre männlichen Kollegen, Juristinnen 10 Prozent weniger, Bankkauffrauen 14 Prozent weniger als Bankkaufmänner und Krankenpflegerinnen 10 Prozent weniger als Krankenpfleger (<a target="_blank" href="https://www.boeckler.de/de/boeckler-impuls-21959.htm">Hans-Böckler-Stiftung 2020a</a>).',
            ],
            ziele: ['Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf'],
          },
        },
        {
          key: 'sozialleistungen',
          title: 'Sozialleistungen und Sozialversicherungen',
          drawerContent: {
            introText:
              'Aufgrund der im Schnitt geringeren Erwerbseinkommen erzielen Frauen weniger eigenständige Sozialleistungen.',
            ergebnisse: [
              'Der „Gender Pension Gap“ lag 2019 bei 49 Prozent (<a target="_blank" href="https://www.bmas.de/SharedDocs/Downloads/DE/Rente/alterssicherungsbericht-2020.pdf">BMAS 2020: 103</a>). Er misst den prozentualen Unterschied zwischen den durchschnittlichen persönlichen eigenen Alterssicherungseinkommen von Frauen gegenüber denen von Männern ab 65 Jahren. Die Kennzahl blendet jedoch den Haushaltskontext wie auch Hinterbliebenenleistungen aus – und sie bezieht sich auf Frauen und Männer, die häufiger noch traditionelle Partnerschaftsmodelle gelebt haben beziehungsweise leben als heute üblich (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 14</a>).',
              'Die durchschnittliche Anspruchshöhe (inklusive Sozialversicherungsbeiträge) von Leistungsbeziehenden bei Arbeitslosigkeit unterscheidet sich nach Geschlecht: Während Männer im Schnitt 1.910 Euro bekamen, waren es bei Frauen im Schnitt 1.491 Euro (Zahlen vom April 2020) (<a target="_blank" href="https://statistik.arbeitsagentur.de/DE/Navigation/Statistiken/Fachstatistiken/Leistungen-SGBIII/Arbeitslosengeld/Arbeitslosengeld-Nav.html">Bundesagentur für Arbeit 2020</a>).',
            ],
            ziele: ['Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf'],
          },
        },
        {
          key: 'berufswahl',
          title: 'Berufswahl und Berufsausübung',
          drawerContent: {
            introText:
              'Frauen sind vermehrt in Berufen der sozialen, personenbezogenen und haushaltsnahen Dienstleistungen tätig. Diese werden häufig geringer bewertet und vergütet als gleichwertige Berufe, die von Männern dominiert werden, zum Beispiel in der Industrie (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 93</a>).',
            ergebnisse: [
              'Die Bewertung und Entlohnung von Arbeit ist gesellschaftlichen Wertvorstellungen unterworfen und fällt zuungunsten der weiblichen Erwerbstätigen aus. So werden Fachkräfte in der Krankenpflege und in der Geburtshilfe für die sehr hohen Anforderungen und Belastungen ihres Berufes vergleichsweise gering entlohnt (<a target="_blank" href="https://www.boeckler.de/pdf/p_wsi_studies_14_2018.pdf">Klammer et al. 2018: 63</a>).',
              'In den in der Coronakrise systemrelevanten Berufsgruppen der „ersten Stunde“ liegt der Anteil an Frauen bei 60 Prozent, gleichzeitig sind dies größtenteils unterdurchschnittlich bezahlte Berufe (<a target="_blank" href="https://www.diw.de/documents/publikationen/73/diw_01.c.792728.de/diw_aktuell_48.pdf">Koebe et al. 2020</a>).',
            ],
            ziele: ['Soziale Berufe als attraktive und durchlässige Karriereberufe stärken'],
          },
        },
        {
          key: 'sorgerecht',
          title: 'Sorgerecht und Unterhalt',
          drawerContent: {
            introText:
              'Nach einer Scheidung beziehungsweise Trennung wohnen die Kinder in den meisten Fällen bei der Mutter. Dies hat für die Frauen erhebliche ökonomische Folgen.',
            ergebnisse: [
              'Fast 90 Prozent aller Alleinerziehenden sind Mütter (<a target="_blank" href="https://www.bib.bund.de/DE/Fakten/Fakt/L31-Alleinerziehende-Geschlecht-ab-1996.html">BiB o. J.</a>). Alleinerziehende sind einer besonderen Armutsgefährdung ausgesetzt. Die Unterschiede im Lebensstandard zeigen sich beispielsweise bei der durchschnittlichen Wohnfläche: So wohnen Alleinerziehende im Durchschnitt auf etwa 34 Quadratmetern pro Person – in der Gesamtbevölkerung sind es über 56 Quadratmeter pro Person (<a target="_blank" href="https://www.econstor.eu/handle/10419/221972">Rehm 2020: 247</a>)',
              'Wenn Alleinerziehende für ihre Kinder keinen Unterhalt vom anderen Elternteil erhalten, werden sie mit dem Unterhaltsvorschuss staatlich unterstützt. Im Jahr 2018 erhielten bundesweit 713.514 Alleinerziehende einen solchen Unterhaltsvorschuss, in 89 Prozent der Fälle war der betreuende Elternteil eine Frau (<a target="_blank" href="https://www.bundesfinanzministerium.de/Content/DE/Standardartikel/Ministerium/Geschaeftsbereich/Wissenschaftlicher_Beirat/Gutachten_und_Stellungnahmen/Ausgewaehlte_Texte/2018-09-27-Gutachten-Besteuerung-von-Ehegatten-anlage.pdf">Deutscher Bundestag 2018: 517</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern',
              'Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf',
            ],
          },
        },
        {
          key: 'vermoegen',
          title: 'Vermögen, Geldanlage und Versicherungen',
          drawerContent: {
            introText:
              'Die Vermögen von Frauen und Männern sind ungleich verteilt, auch das Anlageverhalten und das Finanzwissen weisen nach Geschlecht Unterschiede auf.',
            ergebnisse: [
              'Laut Deutschem Institut für Wirtschaftsforschung (DIW) ist eine größere Vorsicht von Frauen bei Geldanlagen darin begründet, dass sie im Durchschnitt nur halb so viel Geldvermögen zur Verfügung haben wie Männer. Eine prinzipielle Risikoscheu von Frauen kann als Klischee bezeichnet werden, denn der Einfluss des Vermögens beim Anlageverhalten ist für beide Geschlechter gleich (<a target="_blank" href="https://www.diw.de/documents/publikationen/73/diw_01.c.343849.de/09-48-1.pdf">Badunenko et al. 2009</a>).',
              'Wie auch in anderen Ländern zeigt sich in Deutschland eine deutliche Lücke zwischen dem Finanzwissen der Geschlechter. So sind Frauen deutlich weniger über die Möglichkeiten des privaten Vermögensaufbaus und der Alterssicherung informiert als Männer (<a target="_blank" href="https://www.insm.de/fileadmin/insm-dms/downloads/INSM-Bildungsmonitor-Schwerpunktthema-2019.pdf">Anger et al. 2019</a>).',
              'In der Gruppe der Millionärinnen beziehungsweise Millionäre beträgt der Frauenanteil etwa 31 Prozent (<a target="_blank" href="https://www.diw.de/documents/publikationen/73/diw_01.c.793785.de/20-29-1.pdf">Schröder et al. 2020: 517</a>).',
            ],
            ziele: ['Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf'],
          },
        },
        {
          key: 'pflege',
          title: 'Pflege und Betreuung von Kindern und Erwachsenen',
          drawerContent: {
            introText:
              'Viele Frauen reduzieren ihre Erwerbstätigkeit auf Teilzeittätigkeiten, sobald Verantwortung für ein Kind übernommen werden muss. Erwerbstätige Väter wechseln wegen familiärer Verpflichtungen selten in die Teilzeit. Das Zuverdiener-Arrangement wirkt sich oft langfristig negativ auf die Karriereperspektiven von Müttern aus und schlägt sich in Einkommensnachteilen im Lebensverlauf sowie in geringeren Rentenansprüchen nieder (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 154</a>).',
            ergebnisse: [
              'Obwohl der Anteil pflegender Männer seit den 1990ern gestiegen ist, machen Frauen immer noch die Mehrheit der pflegenden Personen aus. Der Frauenanteil steigt dabei mit dem Pflegeumfang (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 88</a>).',
              'Die Schätzungen für pflegende Angehörige belaufen sich auf insgesamt 2,47 Millionen Hauptpflegepersonen, von denen rund zwei Drittel, nämlich 1,65 Millionen, weiblich sind (<a target="_blank" href="https://www.barmer.de/presse/infothek/studien-und-reporte/pflegereport/pflegereport-2018-312014">Rothgang und Müller 2018: 113</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männern fördern',
              'Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf',
            ],
          },
        },
      ],
    },
    wissen: {
      linkName: 'Wissen',
      title: 'Wissen',
      options: [
        {
          key: 'berufswahl',
          title: 'Berufswahl und Berufsausübung',
          drawerContent: {
            introText:
              'Frauen und Männer setzen nach wie vor unterschiedliche Schwerpunkte in ihrer Berufswahl und den Branchen, in denen sie tätig sind. Im Allgemeinen sind Frauen eher im Dienstleistungssektor (Gesundheits- und Sozialwesen, Erziehung und Unterricht, Öffentliche Verwaltung) tätig. Männer sind hingegen vor allem in der Industrie, dem Verkehr sowie im Baugewerbe beschäftigt (<a target="_blank" href="https://statistik.arbeitsagentur.de/DE/Statischer-Content/Statistiken/Themen-im-Fokus/Frauen-und-Maenner/generische-Publikationen/Frauen-Maenner-Arbeitsmarkt.html">Bundesagentur für Arbeit 2019: 12 f.</a>).',
            ergebnisse: [
              'Die Wahl der Ausbildungsberufe erfolgt deutlich nach geschlechtsbezogenen Mustern: Von allen Männern, die sich im Jahr 2019 neu für einen Ausbildungsberuf entschieden, wählten 23 Prozent diese fünf Berufe: Kraftfahrzeugmechatroniker, Fachinformatiker, Elektroniker, Anlagenmechaniker für Sanitär-, Heizungs- und Klimatechnik sowie Industriemechaniker (<a target="_blank" href="https://www.bibb.de/dokumente/pdf/naa309/naa309_2019_tab068_0bund.pdf">BIBB 2019b</a>). Von allen Frauen, die in 2019 einen Ausbildungsberuf wählten, wählten 37 Prozent diese fünf Berufe: Kauffrau für Büromanagement, Medizinische Fachangestellte, Zahnmedizinische Fachangestellte, Verkäuferin sowie Kauffrau im Einzelhandel (<a target="_blank" href="https://www.bibb.de/dokumente/pdf/naa309/naa309_2019_tab069_0bund.pdf">BIBB 2019a</a>).',
              'Die Studienfachwahl weist hohe Geschlechterdisparitäten auf: Die Ingenieurwissenschaften weisen den geringsten Frauenanteil auf (25 Prozent), innerhalb des Faches bestehen aber Unterschiede: Die Informatik, Elektrotechnik sowie Verkehrstechnik und Nautik haben einen Frauenanteil von unter 25 Prozent, Architektur und Raumplanung weisen dagegen 55 bis 62 Prozent Studienanfängerinnen auf. Einen überdurchschnittlich hohen Frauenanteil von 60 bis über 80 Prozent verzeichnen die Geisteswissenschaften, die Kunst und die Kunstwissenschaften, die Humanmedizin und die Gesundheitswissenschaften, die Psychologie, das Sozialwesen, die Erziehungswissenschaft und die Veterinärmedizin (<a target="_blank" href="https://www.bildungsbericht.de/de/bildungsberichte-seit-2006/bildungsbericht-2020/pdf-dateien-2020/bildungsbericht-2020.pdf">Autorengruppe Bildungsberichterstattung 2020: 191 f.</a>).',
            ],
            ziele: ['Soziale Berufe als attraktive und durchlässige Karriereberufe stärken'],
          },
        },
        {
          key: 'ausbildung',
          title: 'Aus- und Fortbildung',
          drawerContent: {
            introText:
              'Während etwa gleich viele Frauen und Männer eine Berufsausbildung oder einen Fachschulabschluss (Meister, Techniker, Fachschule für Sozialberufe et cetera) absolvieren, ist der Anteil der Männer bei höheren Bildungsabschlüssen (Master, Diplom, Promotion) höher (<a target="_blank" href="https://www.bildungsbericht.de/de/datengrundlagen/daten-2020">DIPF 2020</a>).',
            ergebnisse: [
              'Männer nehmen stärker an beruflicher Weiterbildung teil als Frauen; sie bekommen die Weiterbildungsmaßnahme auch häufiger vom Arbeitgeber finanziert. In der Konsequenz profitieren sie mit einem höheren Einkommen (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 86</a>).',
              'Der Anteil an den Promotionen von Frauen lag im Jahr 2018 in Deutschland bei 45 Prozent − im europäischen Vergleich ist dies leicht unterdurchschnittlich (Anteil in den 28 Staaten der Europäischen Union: 48 Prozent) (<a target="_blank" href="https://www.gesis.org/cews/unser-angebot/informationsangebote/statistiken/thematische-suche/detailanzeige/article/frauenanteile-an-den-promotionen-isced-8-im-internationalen-vergleich">CEWS 2020</a>).',
            ],
            ziele: [
              'Gleichberechtigte Präsenz und Teilhabe von Frauen und Männern in Kultur und Wissenschaft',
              'Gleichberechtigte Karrierechancen und Teilhabe von Frauen und Männern in Führungspositionen',
            ],
          },
        },
        {
          key: 'zugangWissen',
          title: 'Zugang zu Wissen und Bildung',
          drawerContent: {
            introText:
              'Wissen und Bildung sind unterschiedlich verteilt, dabei spielt nicht nur das Geschlecht, sondern auch die soziale und kulturelle Herkunft eine Rolle.',
            ergebnisse: [
              'Etwas mehr Männer als Frauen sind „gering literalisiert“, haben also bereits Schwierigkeiten, eine einfache schriftliche Arbeitsanweisung zu lesen. Dies betrifft hierzulande 12 Prozent der Erwachsenen (also rund 6,2 Millionen), davon sind 58 Prozent Männer (<a target="_blank" href="https://www.bmbf.de/bmbf/shareddocs/downloads/files/2019-05-07-leo-presseheft_2019-vers10.pdf?__blob=publicationFile&v=1">Grotlüschen et al. 2019: 5, 7</a>).',
              'Die Hochschulreife erwerben heute rund 57 Prozent der jungen Frauen und 45 Prozent der jungen Männer (Studienberechtigtenquote), was vor allem an der höheren Gymnasialquote von Mädchen liegt. Die studienberechtigten Männer nehmen dann allerdings häufiger auch ein Studium auf. Trotz des weiblichen Vorsprungs im Erwerb der Hochschulreife ist deswegen nach wie vor der Geschlechteranteil unter Studienanfängerinnen und -anfängern in Deutschland fast ausgeglichen (<a target="_blank" href="https://www.bildungsbericht.de/de/bildungsberichte-seit-2006/bildungsbericht-2020/pdf-dateien-2020/bildungsbericht-2020.pdf">Autorengruppe Bildungsberichterstattung 2020: 185</a>).',
            ],
            ziele: [
              'Gleichberechtigte Präsenz und Teilhabe von Frauen und Männern in Kultur und Wissenschaft',
              'Gleichberechtigte Karrierechancen und Teilhabe von Frauen und Männern in Führungspositionen',
            ],
          },
        },
      ],
    },
    zeit: {
      linkName: 'Zeit',
      title: 'Zeit',
      options: [
        {
          key: 'arbeitsteilung',
          title: 'Arbeitsteilung in Partnerschaften',
          drawerContent: {
            introText:
              'Vor der Familiengründung verteilen sich die Erwerbs-Arbeitszeiten von Frauen und Männern ähnlich. Nach der Gründung einer Familie und dem Ende einer etwaigen Elternzeit ergibt sich ein anderes Bild. Dabei sind Erwerbsunterbrechungen nach der Geburt eines Kindes nach Region und Migrationsherkunft unterschiedlich ausgeprägt. Bei Männern hat die Familiengründung kaum einen Einfluss auf die Erwerbsbeteiligung, im Gegenteil: Väter sind noch häufiger erwerbstätig als kinderlose Männer (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 87</a>).',
            ergebnisse: [
              'Frauen leisten im Schnitt täglich 52 Prozent mehr unbezahlte Sorgearbeit als Männer (Erziehung von Kindern, Hausarbeit, Pflege von Angehörigen, Ehrenämter; Erhebung 2012/2013) (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 20</a>). Dabei zeigen sich die größten Unterschiede bei den 34-Jährigen: Hier beträgt der Gender Care Gap 111 Prozent. Frauen verbringen täglich im Schnitt fünf Stunden und 18 Minuten mit Care-Arbeit, Männer hingegen zwei Stunden und 31 Minuten (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/themen/gleichstellung/gender-care-gap/indikator-fuer-die-gleichstellung/gender-care-gap---ein-indikator-fuer-die-gleichstellung/137294">BMFSFJ 2019</a>).',
              'Ostdeutsche Mütter haben ihre Erwerbstätigkeit schon immer deutlich kürzer als westdeutsche Mütter unterbrochen, wobei die Gesamtdauer der Erwerbsunterbrechungen in beiden Teilen des Landes mit steigender Kinderzahl zunimmt. Wenn das jüngste Kind drei Jahre oder jünger ist, waren 38 Prozent der Mütter und 84 Prozent der Väter in ganz Deutschland ohne Migrationshintergrund im Jahr 2015 aktiv erwerbstätig (<a target="_blank" href="https://digital.zlb.de/viewer/resolver?urn=urn:nbn:de:kobv:109-1-15433197">Pimminger 2015: 18</a>).',
              'Mütter mit Migrationshintergrund sind zu einem geringeren Anteil erwerbstätig als Mütter ohne Migrationshintergrund, wenn das jüngste Kind drei Jahre oder jünger ist: Im Jahr 2015 waren 21 Prozent der Mütter und 77 Prozent der Väter mit Migrationshintergrund in dieser Lebensphase aktiv erwerbstätig (<a target="_blank" href="https://digital.zlb.de/viewer/resolver?urn=urn:nbn:de:kobv:109-1-15433197">Pimminger 2015: 18</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männer fördern',
              'Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf',
            ],
          },
        },
        {
          key: 'pflege',
          title: 'Pflege und Betreuung von Kindern und Erwachsenen',
          drawerContent: {
            introText:
              'Sobald Verantwortung für ein Kind übernommen werden muss, reduzieren viele Frauen ihre Erwerbstätigkeit auf Teilzeittätigkeiten. Erwerbstätige Väter wechseln wegen familiärer Verpflichtungen selten in die Teilzeit (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 154</a>).',
            ergebnisse: [
              'Die Müttererwerbstätigenquote mit einem jüngsten Kind unter 18 Jahren beträgt 69 Prozent (2018) (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">nach BMFSFJ 2020b: 20</a>).',
              'Die Schätzungen für pflegende Angehörige belaufen sich auf insgesamt 2,47 Millionen Hauptpflegepersonen, von denen rund zwei Drittel, nämlich 1,65 Millionen weiblich sind (<a target="_blank" href="https://www.barmer.de/presse/infothek/studien-und-reporte/pflegereport/pflegereport-2018-312014">Rothgang und Müller 2018: 113</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männer fördern',
              'Entgeltgleichheit und eigenständige wirtschaftliche Sicherung im Lebensverlauf',
            ],
          },
        },
        {
          key: 'haushaltsfuehrung',
          title: 'Haushaltsführung',
          drawerContent: {
            introText:
              'Frauen wenden durchschnittlich mehr Zeit für die Hausarbeit und Betreuung innerhalb der Familie auf als Männer.',
            ergebnisse: [
              'Frauen leisten im Schnitt täglich 52 Prozent mehr unbezahlte Sorgearbeit als Männer (Erziehung von Kindern, Hausarbeit, Pflege von Angehörigen, Ehrenämter; Erhebung 2012/2013) (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 20</a>). Dabei zeigen sich die größten Unterschiede bei den 34-Jährigen: Hier beträgt der Gender Care Gap 111 Prozent. Frauen verbringen täglich im Schnitt fünf Stunden und 18 Minuten mit Care-Arbeit, Männer hingegen zwei Stunden und 31 Minuten (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/themen/gleichstellung/gender-care-gap/indikator-fuer-die-gleichstellung/gender-care-gap---ein-indikator-fuer-die-gleichstellung/137294">BMFSFJ 2019</a>).',
              'Im Bereich der direkten Sorgearbeit (Kinderbetreuung im Haushalt sowie Unterstützung/ Pflege/ Betreuung von erwachsenen Haushaltsmitgliedern und dazugehörige Wegezeiten) ist der Gender Care Gap deutlich höher als bei der unterstützenden Care-Arbeit (z. B. Kochen, Einkaufen, Ehrenamt, handwerkliche Tätigkeiten) (<a target="_blank" href="https://www.gleichstellungsbericht.de/de/article/51.expertisen.html">Klünder 2017: 7</a>). Frauen leisten mehr als doppelt so lange (108 Prozent) direkte Care-Arbeit im Vergleich zu Männern. Die Lücke bei der unterstützenden Care-Arbeit beträgt 47 Prozent (<a target="_blank" href="https://www.gleichstellungsbericht.de/de/article/51.expertisen.html">Klünder 2017: 11</a>).',
            ],
            ziele: [
              'Vereinbarkeit von Familie, Pflege und Beruf stärken – eine gleichberechtigte Verteilung von Erwerbsarbeit und unbezahlter Sorgearbeit zwischen Frauen und Männer fördern',
            ],
          },
        },
        {
          key: 'engagement',
          title: 'Gesellschaftliches Engagement',
          drawerContent: {
            introText:
              'Frauen engagieren sich etwas seltener freiwillig als Männer (43 Prozent bei Frauen gegenüber 46 Prozent bei Männern) (<a target="_blank" href="https://www.bmfsfj.de/blob/113702/53d7fdc57ed97e4124fffec0ef5562a1/vierter-freiwilligensurvey-monitor-data.pdf">Simonson et al. 2016: 16</a>). Die Zahl der freiwillig engagierten Frauen und Männer hat sich in den letzten 20 Jahren angenähert, allerdings bestehen nach Alter, Region und Art des Engagements Unterschiede (<a target="_blank" href="https://www.bmfsfj.de/blob/118460/1a128b69e46adb3fa370afc4334f08aa/freiwilliges-engagement-von-frauen-und-maennern-data.pdf">Kausmann et al. 2017</a>).',
            ergebnisse: [
              'Das Engagement von Frauen und Männern weist regionale Unterschiede auf: Im städtischen Raum beträgt der Abstand zwischen den Geschlechtern 3 Prozentpunkte, in ländlichen Gebieten 7 Prozentpunkte (<a target="_blank" href="https://www.bmfsfj.de/blob/113702/53d7fdc57ed97e4124fffec0ef5562a1/vierter-freiwilligensurvey-monitor-data.pdf">Simonson et al. 2016: 27</a>). Männer engagieren sich deutlich häufiger in Leitungs- oder Vorstandsfunktionen (<a target="_blank" href="https://www.bmfsfj.de/blob/113702/53d7fdc57ed97e4124fffec0ef5562a1/vierter-freiwilligensurvey-monitor-data.pdf">Simonson et al. 2016: 35</a>). Frauen engagieren sich häufiger als Männer in der Zielgruppe der Kinder und Jugendlichen und im kirchlichen oder religiösen Bereich. Männer engagieren sich vor allem in den Bereichen Sport, Politik und politische und berufliche Interessenvertretung sowie im Unfall- und Rettungsdienst und bei der Freiwilligen Feuerwehr (<a target="_blank" href="https://www.bmfsfj.de/blob/118460/1a128b69e46adb3fa370afc4334f08aa/freiwilliges-engagement-von-frauen-und-maennern-data.pdf">Kausmann et al. 2017: 6</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
        {
          key: 'mobilitaet',
          title: 'Mobilität',
          drawerContent: {
            introText:
              'Frauen haben andere Mobilitätsmuster als Männer. Die Ursachen liegen unter anderem in der unterschiedlichen Erwerbsquote, der Teilzeitquote und der Arbeitsteilung in Partnerschaften. Dies hat Auswirkungen auf ihre Mobilitätsmuster und das Nutzungsverhalten verschiedener Verkehrsmittel (<a target="_blank" href="https://www.umweltbundesamt.de/sites/default/files/medien/1410/publikationen/2018-03-15_texte_23-2018_gender-klima.pdf">Röhr et al. 2018: 46 ff.</a>).',
            ergebnisse: [
              'Im ländlichen Raum entstehen durch die anfallende Sorgearbeit hohe Mobilitätsanforderungen. Frauen sind jedoch gerade im ländlichen Raum weniger mobil als Männer, da sie seltener einen PKW zur Verfügung haben und der öffentliche Nahverkehr häufig schlecht ausgebaut ist (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 92</a>).',
              'Während im Hinblick auf die an einem Tag zurückgelegten Anzahl der Wege nur geringe Unterschiede bestehen, beträgt die durchschnittliche Wegelänge bei Männern 14 Kilometer und bei Frauen 11 Kilometer. Die durchschnittliche Tagesstrecke weist deutliche Unterschiede auf: Hier kommen Männer auf 46 Kilometer und Frauen auf 33 Kilometer. Dabei fahren Männer im Schnitt täglich zehn Kilometer mehr Auto als Frauen (<a target="_blank" href="https://www.mobilitaet-in-deutschland.de/publikationen2017.html">BMVI 2018: 29, 51</a>).',
              'Insgesamt besaßen im Jahr 2017 91 Prozent der Männer und 85 Prozent der Frauen (ab 18 Jahren) einen PKW-Führerschein. Der Anteil der Personen mit einem Führerschein ist in der Gruppe der bis 50-Jährigen zwischen Frauen und Männern fast gleich hoch, geht jedoch in den Altersgruppen der über 50-Jährigen auseinander (<a target="_blank" href="https://www.mobilitaet-in-deutschland.de/archive/pdf/MiD2017_Ergebnisbericht.pdf">Follmer und Gruschwitz 2019: 23</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
      ],
    },
    entscheidungsmacht: {
      linkName: 'Entscheidungsmacht',
      title: 'Entscheidungsmacht',
      options: [
        {
          key: 'mitbestimmung',
          title: 'Mitbestimmung und Vertretung in politischen und zivilgesellschaftlichen Gremien',
          drawerContent: {
            introText:
              'Frauen beteiligen sich in geringerem Umfang als Männer politisch – und dies „an allen politischen Partizipationsformen“ (<a target="_blank" href="https://www.bmfsfj.de/blob/113702/53d7fdc57ed97e4124fffec0ef5562a1/vierter-freiwilligensurvey-monitor-data.pdf">Simonson et al. 2016: 199</a>). Dieser Unterschied ist bei der Übernahme politischer Ämter besonders ausgeprägt.',
            ergebnisse: [
              'Der Frauenanteil im Bundestag beträgt 31 Prozent, der in den Landesparlamenten 30 Prozent und der an den Mandaten in den kommunalen Vertretungen 28 Prozent (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 24</a>).',
              'Der Anteil der Bürgermeisterinnen in Deutschland schwankt zwischen 8 Prozent in Gemeinden unter 10.000 Einwohnerinnen/Einwohnern und 18 Prozent in Gemeinden zwischen 50.000 und 100.000 Einwohnerinnen/Einwohnern. Im Jahr 2014 wurden von den 295 deutschen Landratsämtern 27 von Landrätinnen geführt (10 Prozent), dabei war ihr Anteil in den neuen Bundesländern deutlich höher (16 Prozent) als in den alten Bundesländern (7 Prozent) (<a target="_blank" href="https://www.eaf-berlin.de/fileadmin/eaf/Publikationen/Dokumente/2014_EAF_Berlin_Lukoschat_Belschner_Frauen-Fuehren-Kommunen_Studie.pdf">Lukoschat und Belschner 2014: 16</a>).',
            ],
            ziele: ['Gleichberechtigte Beteiligung von Frauen in Parlamenten auf allen Ebenen'],
          },
        },
        {
          key: 'aufstiegschancen',
          title: 'Aufstiegschancen und Führungspositionen',
          drawerContent: {
            introText:
              'Frauen sind in Führungspositionen der Privatwirtschaft und des öffentlichen Dienstes nach wie vor unterrepräsentiert. Die Gründe hierfür sind vielfältig (<a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/publikationen/zweiter-gleichstellungsbericht-der-bundesregierung/119796">Bundesregierung 2017: 86</a>).',
            ergebnisse: [
              'Seit dem Jahr 2012 hat sich der Anteil der weiblichen Führungskräfte (Vorstände und Geschäftsführerinnen/Geschäftsführer sowie Führungskräfte in Handel, Produktion und Dienstleistungen) kaum verändert: 2018 war jede dritte Führungskraft (29,4 Prozent) weiblich, das sind 0,8 Prozentpunkte mehr als 2012 (<a target="_blank" href="https://www.destatis.de/DE/Themen/Arbeit/Arbeitsmarkt/Qualitaet-Arbeit/Dimension-1/frauen-fuehrungspositionen.html">DESTATIS o. J.a</a>).',
              'Der Anteil der Unternehmen, die sich die Zielgröße „Null“ oder gar keine Zielgröße für den Vorstand gesetzt haben, lag an allen unter das Führungspositionengesetz (FüPoG I) fallenden Unternehmen 2017 bei 78 Prozent (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 22</a>).',
              'Der Frauenanteil in den Aufsichtsräten der Unternehmen, die unter die feste Quote fallen, stieg von 2015 bis 2017 im absoluten Vergleich von 25 Prozent auf 33 Prozent. In den Unternehmen, die nicht der festen Quote unterliegen, stieg der Frauenanteil in den Aufsichtsräten im absoluten Vergleich von 18 Prozent in 2015 auf 20 Prozent im Jahr 2017 (<a target="_blank" href="https://www.bmfsfj.de/quote/daten/Vierte_Jaehrliche_Information.pdf">BMFSFJ 2020c: 15</a>).',
            ],
            ziele: [
              'Gleichberechtigte Karrierechancen und Teilhabe von Frauen und Männern in Führungspositionen',
              'Der öffentliche Dienst des Bundes baut bei der Vereinbarkeit und gleichberechtigten Teilhabe an Führungspositionen seine Vorreiterrolle aus.',
            ],
          },
        },
        {
          key: 'rechte',
          title: 'Staatsbürgerliche Rechte',
          drawerContent: {
            introText:
              'Frauen wurden erst spät alle staatsbürgerlichen Rechte gewährt, dies wirkt sich teilweise bis heute auf die Lebensverhältnisse von Frauen und Männern aus.',
            ergebnisse: [
              'Auch wenn bereits im Grundgesetz von 1949 der Satz „Männer und Frauen sind gleichberechtigt“ enthalten ist, wurden die Geschlechter in Westdeutschland erst nach vielen Reformen rechtlich gleichgestellt. Denn es galt das Bürgerliche Gesetzbuch (BGB) von 1896, in dem zum Beispiel in der Ehe der Mann zum Oberhaupt einer Familie gemacht wurde. So konnte er bis 1958 etwa alleine über Wohnort und Wohnung entscheiden. Auch durften Ehefrauen nur mit Zustimmung des Ehemanns ein Konto eröffnen. Als 1958 das „Gesetz über die Gleichberechtigung von Mann und Frau auf dem Gebiet des bürgerlichen Rechts“ in Kraft trat, wurde diese Vorherrschaft abgeschwächt. Erst 1977 beseitigte eine erneute Reform des Ehe- und Familienrechts im BGB eine Regelung, nach der es einer Ehefrau nur dann erlaubt war, einer Erwerbsarbeit nachzugehen, wenn sie die Familie nicht vernachlässigte  (<a target="_blank" href="https://www.bpb.de/politik/hintergrund-aktuell/271712/gleichberechtigung">bpb 2018</a>). 1994 wurde das Grundgesetz in Bezug auf die Gleichberechtigung der Geschlechter erweitert: „Der Staat fördert die tatsächliche Durchsetzung der Gleichberechtigung von Frauen und Männern und wirkt auf die Beseitigung bestehender Nachteile hin“. Damit sollte nicht nur die formale, sondern auch die faktische Gleichstellung befördert werden (<a target="_blank" href="https://hundertjahrefrauenwahlrecht.de/1994-die-erweiterung-des-grundgesetzes/">Tatarinov o. J.</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
        {
          key: 'selbstbestimmung',
          title: 'Selbstbestimmung und freie Entscheidung über Lebensformen',
        },
      ],
    },
    gesundheit: {
      linkName: 'Gesundheit',
      title: 'Gesundheit',
      options: [
        {
          key: 'gesundheitswesen',
          title: 'Gesundheitswesen (Medizin, Gesundheitsvorsorge)',
          drawerContent: {
            introText:
              'Bei vielen Aspekten von Gesundheit und Krankheit zeigen sich Geschlechterunterschiede. Sowohl das biologische als auch das soziale Geschlecht haben hier einen Einfluss.',
            ergebnisse: [
              'Herz-Kreislauf-Erkrankungen sind hierzulande die häufigste Todesursache. Im Jahr 2017 verstarben 156.177 Männer und 188.347 Frauen an Erkrankungen des Herz-Kreislauf-Systems. Davon verstarben 27.130 Männer und 19.836 Frauen an einem Herzinfarkt und 4.898 Männer und 7.689 Frauen an einem Schlaganfall (BZgA 2020: 4). (Die Quelle mit Stand 2020 ist online nicht mehr zu finden)',
              'Frauen und Männer zeigen unterschiedliche Verhaltensweisen in Bezug auf ihre Gesundheit. So nehmen beispielsweise 44,3 Prozent der Männer und 49,8 Prozent der Frauen einen Gesundheits-Check-up regelmäßig in Anspruch (<a target="_blank" href="https://www.gbe-bund.de/gbe/pkg_olap_tables.prc_sort_ind?p_uid=gast&p_aid=33355292&p_sprache=D&p_help=2&p_indnr=779&p_ansnr=24588042&p_version=5&p_sortorder=d&p_dim_1=D.100&p_dw_1=10102">Zentralinstitut für die kassenärztliche Versorgung in der Bundesrepublik Deutschland 2020</a>).',
              'Die Lebenserwartung von Frauen und Männern bei der Geburt ist ungleich: Sie lag im Bundesdurchschnitt (2016/2018) für Frauen bei 83,3 Jahren und für Männer bei 78,5 Jahren, wobei Unterschiede auch nach Bundesländern festzustellen sind (<a target="_blank" href="https://www.bmfsfj.de/blob/160308/50628ce931557ca11439f70536115e54/4--atlas-zur-gleichstellung-von-frauen-und-maennern-in-deutschland-broschuere-data.pdf">BMFSFJ 2020a</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
        {
          key: 'krankenversicherung',
          title: 'Kranken- und Pflegeversicherung',
          drawerContent: {
            introText:
              'Im Bereich der Kranken- und Pflegeversicherungen bestehen aufgrund unterschiedlicher Rahmenbedingungen und Einkommensverteilungen teilweise Unterschiede zwischen Frauen und Männern.',
            ergebnisse: [
              'Von den insgesamt etwa 7.146.400 in 2018 privat Krankenvollversicherten waren rund 39 Prozent Frauen und 61 Prozent Männer. Von den darunter circa 3.582.500 Versicherten ohne Beihilfen waren 27 Prozent Frauen und 73 Prozent Männer (<a target="_blank" href="https://www.pkv.de/fileadmin/user_upload/PKV/3_PDFs/Publikationen/Zahlenbericht_2018.pdf">PKV 2019</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
        {
          key: 'gesundheitsinformationen',
          title: 'Zugang zu Gesundheitsinformationen',
          drawerContent: {
            introText:
              'Der Zugang zu Gesundheitsinformationen muss für Frauen und Männer gleichermaßen sichergestellt sein.',
            ergebnisse: [
              'Aktuelle Studien zeigen, dass etwa die Hälfte der Bevölkerung in Deutschland Schwierigkeiten damit hat, Gesundheitsinformationen suchen, finden, beurteilen und in der Praxis anwenden zu können. Dieser Mangel an Gesundheitskompetenz betrifft Frauen und Männer gleichermaßen. Deshalb müssen sich auch Maßnahmen zur Stärkung der Gesundheitskompetenz gleichermaßen an Frauen und Männer richten. Studien zeigen aber zugleich, dass Gesundheitsinformationen zielgruppenspezifisch ausgerichtet werden sollten, um Wirkung zu entfalten. Das bedeutet, dass auch geschlechtsspezifische Gesundheitsinformationen notwendig sind. Eine geschlechterbezogene Förderung der Gesundheitskompetenz kann so zur Stärkung der Gesundheit von Frauen, Männern und Familien deutlich beitragen (<a target="_blank" href="https://edoc.rki.de/handle/176904/2526">Jordan und Hoebel 2015, Kelb et al. 2016</a>).',
            ],
            ziele: ['Die Bundesregierung fördert die tatsächliche Gleichstellung querschnittlich und strukturell.'],
          },
        },
      ],
    },
    gewalt: {
      linkName: 'Gewalt',
      title: 'Gewalt',
      options: [
        {
          key: 'schutz',
          title: 'Schutz vor physischer und psychischer Gewalt',
          drawerContent: {
            introText:
              'Laut Gleichstellungsstrategie der Bundesregierung ist Gleichstellung nur ohne Gewalt möglich: „Geschlechtsbezogene Gewalt ist eine besonders schwerwiegende Form des Machtmissbrauchs und der Menschenrechtsverletzung“ (<a target="_blank" href="https://www.bmfsfj.de/blob/158356/c84e875879472d507bfec567203b4a74/gleichstellungsstrategie-der-bundesregierung-data.pdf">BMFSFJ 2020b: 34</a>).',
            ergebnisse: [
              'Bei Straftaten lassen sich deutliche Unterschiede zwischen den Geschlechtern feststellen. Während männliche Opfer hauptsächlich bei Raub, Mord, Totschlag und Tötungen auf Verlangen und Körperverletzung registriert werden, sind Opfer von Straftaten gegen die sexuelle Selbstbestimmung meist weiblichen Geschlechts (<a target="_blank" href="https://www.bka.de/DE/AktuelleInformationen/StatistikenLagebilder/PolizeilicheKriminalstatistik/PKS2019/PKSTabellen/BundOpfertabellen/bundopfertabellen.html">BKA 2020b</a>).',
              'Laut Bundeskriminalamt gab es in 2019 insgesamt 141.792 Personen, die Opfer partnerschaftlicher Gewalt wurden. Davon waren etwa 81 Prozent Frauen und 19 Prozent Männer. Darunter fallen zum Beispiel versuchter und vollendeter Mord und Totschlag, hier waren 301 Frauen und 93 Männer Opfer. Bei Freiheitsberaubung waren 1.514 Frauen und 183 Männer Opfer, und bei Zwangsprostitution 51 Frauen und ein Mann (<a target="_blank" href="https://www.bka.de/SharedDocs/Downloads/DE/Publikationen/JahresberichteUndLagebilder/Partnerschaftsgewalt/Partnerschaftsgewalt_2019.html">BKA 2020a: 23</a>).',
            ],
          },
        },
        {
          key: 'belaestigung',
          title: 'Sexuelle Belästigung und sexuelle Gewalt',
          drawerContent: {
            introText: 'Opfer von Straftaten gegen die sexuelle Selbstbestimmung sind meist weiblich.',
            ergebnisse: [
              'Frauen erfahren häufiger sexuelle Belästigung am Arbeitsplatz. Laut einer repräsentativen Studie der Antidiskriminierungsstelle des Bundes ist etwa jede elfte erwerbstätige Person von sexueller Belästigung am Arbeitsplatz betroffen, wobei dies Frauen zu rund 13 Prozent und Männer zu rund fünf Prozent betraf (<a target="_blank" href="https://www.antidiskriminierungsstelle.de/SharedDocs/downloads/DE/publikationen/Expertisen/umgang_mit_sexueller_belaestigung_am_arbeitsplatz.html">Schröttle et al. 2019: 12</a>).',
            ],
          },
        },
        {
          key: 'straftaten',
          title: 'Weitere Straftaten (zum Beispiel Beleidigung, Straftaten im Internet)',
          drawerContent: {
            introText:
              'Die Kriminalstatistik weist erhebliche Unterschiede nach Geschlecht auf. Es werden mehr Männer Opfer krimineller Handlungen als Frauen. Gleichzeitig gibt es auch deutlich mehr männliche als weibliche Tatverdächtige (<a target="_blank" href="https://www.bmi.bund.de/SharedDocs/downloads/DE/publikationen/themen/sicherheit/pks-2019.pdf">BKA 2020c</a>).',
            ergebnisse: [
              'Von den 1.013.048 Opfern, die in der polizeilichen Statistik in 2019 erfasst wurden, sind 603.080 männlich und 409.968 weiblich (<a target="_blank" href="https://www.bmi.bund.de/SharedDocs/downloads/DE/publikationen/themen/sicherheit/pks-2019.pdf">BKA 2020c: 11</a>).',
              'Insgesamt wurden im Jahr 2018 über 578.421 Männer (circa 81 Prozent) und fast 133.917 Frauen (circa 19 Prozent) rechtskräftig verurteilt (<a target="_blank" href="https://www.destatis.de/DE/Themen/Staat/Justiz-Rechtspflege/Publikationen/Downloads-Strafverfolgung-Strafvollzug/strafverfolgung-2100300207004.html">DESTATIS o. J.b</a>).',
              'Frauen sind häufiger von Mobbing und Cybermobbing betroffen: In Deutschland gaben 24 Prozent der Männer und 37 Prozent der Frauen an, bereits Opfer von Mobbing geworden zu sein; für Cybermobbing liegen diese Werte bei 7 Prozent (Frauen) beziehungsweise 11 Prozent (Männer) (<a target="_blank" href="https://www.destatis.de/DE/Themen/Staat/Justiz-Rechtspflege/Publikationen/Downloads-Strafverfolgung-Strafvollzug/strafverfolgung-2100300207004.html">Schneider und Leest 2018: 22</a>).',
            ],
          },
        },
      ],
    },
    auswirkungen: {
      linkName: 'Beschreibung von Auswirkungen',
      title: 'Beschreibung von Auswirkungen',
      intro:
        'Beschreiben Sie nun die <strong>Auswirkungen in den einzelnen Lebensbereichen</strong> genauer. Dabei sollten Sie Ihre Überlegungen zu betroffenen Akteuren des Regelungsvorhabens aus der Relevanzprüfung berücksichtigen. Bei der Analyse von Auswirkungen kann es sinnvoll sein, zwischen kurzfristigen und langfristigen Auswirkungen zu unterscheiden. Letztlich geht es darum, die <strong>betroffenen Akteure in ihrer Differenziertheit zu betrachten</strong> und abzuschätzen, inwiefern sich das Regelungsvorhaben unterschiedlich auf die betroffenen Menschen auswirkt.',
      paragraph1: {
        title: 'Hinweis zum Zusammenwirken des Geschlechts mit weiteren Merkmalen',
        content:
          'Je nach <strong>Merkmalen</strong> (neben dem Geschlecht auch zum Beispiel Haushaltstyp, Einkommen, Bildung, Alter, Behinderung, Migrationshintergrund oder sexuelle Orientierung) und <strong>Lebenssituation</strong> (zum Beispiel Berufsstart, Renteneintritt, Geburt eines Kindes) der Betroffenen kann das Regelungsvorhaben unterschiedliche Wirkungen entfalten. Häufig ergeben sich nachteilige Auswirkungen von Regelungen erst im <strong>Zusammenwirken solcher anderer Merkmale</strong> mit dem Geschlecht oder werden durch dieses erheblich <strong>verstärkt</strong>. Gleichzeitig können rechtliche Regelungen auch langfristig dazu führen, dass sich die Geschlechterstrukturen in den Lebensbereichen ändern.',
      },
      paragraph2: {
        title: 'Daten und Statistiken',
        info: '<h3>Amtliche Statistik</h3><ul><li><a target="_blank" href="https://www.destatis.de/DE/Startseite.html">Statistisches Bundesamt</a></li><li><a target="_blank" href="https://ec.europa.eu/eurostat/de/home">Eurostat</a></li></ul><h3>Berichte und Veröffentlichungen der Bundesregierung und der Europäischen Union</h3><ul><li><a target="_blank" href="https://www.gleichstellungsstrategie.de/">Informationen und Kennzahlen zu den Zielen in der Gleichstellungsstrategie der Bundesregierung</a></li><li><a target="_blank" href="https://ec.europa.eu/info/policies/justice-and-fundamental-rights/gender-equality/gender-equality-strategy_de">Gleichstellungsstrategie der EU-Kommission</a></li><li><a target="_blank" href="https://www.bmfsfj.de/bmfsfj/service/gleichstellungsatlas">Gleichstellungsatlas des BMFSFJ</a></li><li><a target="_blank" href="https://www.gleichstellungsbericht.de/">Gleichstellungsbericht der Bundesregierung</a></li><li><a target="_blank" href="https://eige.europa.eu/gender-equality-index/2019">Gleichstellungsindex (Gender Equality Index) des Europäischen Instituts für Gleichstellungsfragen (EIGE)</a></li></ul>',
        content:
          'Ziehen Sie für die Abschätzung der Auswirkungen möglichst <strong>Daten und Statistiken zur Geschlechterstruktur</strong> im Regelungsfeld heran. Dies können zum Beispiel geschlechterdifferenzierte Statistiken oder andere Analysen sein. Wenn diese nicht verfügbar sind, können <strong>Hypothesen</strong> über Wirkzusammenhänge auch anhand qualitativer Daten oder mithilfe plausibler Annahmen aufgestellt werden. Geben Sie in Ihrer Beschreibung an, worauf Ihre Abschätzung beruht.',
      },
      paragraph3: {
        title: 'Angaben zu den Auswirkungen',
        lebensbereichTitle: 'Lebensbereich: {{title}}',
        weitereAuswirkungenTitle: 'Weitere Auswirkungen',
        noBereich: {
          content:
            'Sie haben angegeben, dass das Vorhaben in den genannten Lebensbereichen keine Auswirkungen auf die Gleichstellung der Geschlechter hat.',
          question1: 'Welche gleichstellungspolitischen Auswirkungen sind durch das Vorhaben zu erwarten?',
          auswirkungenTitle: 'Auswirkungen',
        },
        singleBereich: {
          content:
            'Sie haben angegeben, dass das Vorhaben im folgenden Lebensbereich Auswirkungen auf die Gleichstellung der Geschlechter hat:',
          question1:
            'Welche unmittelbaren und mittelbaren Auswirkungen hat das Regelungsvorhaben auf Frauen und Männer in dem identifizierten Bereich?',
          question2: 'Sind darüber hinaus weitere gleichstellungspolitische Auswirkungen zu erwarten?',
          auswirkungenTitle: 'Auswirkungen in dem Bereich',
        },
        multipleBereiche: {
          content:
            'Sie haben angegeben, dass das Vorhaben in folgenden Lebensbereichen Auswirkungen auf die Gleichstellung der Geschlechter hat:',
          question1:
            'Welche unmittelbaren und mittelbaren Auswirkungen hat das Regelungsvorhaben auf Frauen und Männer in den identifizierten Bereichen?',
          question2: 'Sind darüber hinaus weitere gleichstellungspolitische Auswirkungen zu erwarten?',
          auswirkungenTitle: 'Auswirkungen in den Bereichen',
        },
      },
    },
  },
  wirkbereich: {
    question:
      'Steht das Regelungsvorhaben in einem Zusammenhang mit gleichstellungsrelevanten Fragen in folgenden Lebensbereichen? Hat das Regelungsvorhaben dadurch unterschiedliche Auswirkungen auf Frauen und Männer?',
    subtitle: 'Wählen Sie alle relevanten Bereiche aus.',
    info: {
      moreInfo: 'Weitere Informationen',
      studienergebnisse: 'Studienergebnisse',
      ziel: 'Ziel in der Gleichstellungsstrategie der Bundesregierung',
      ziele: 'Ziele in der Gleichstellungsstrategie der Bundesregierung',
      link: '<a target="_blank" href="{{link}}">Link zur Gleichstellungsstrategie</a>',
    },
  },
  geschlechtergerechtigkeit: {
    linkName: 'Maßnahmen',
    title: 'Entwicklung von Maßnahmen zur Förderung von Geschlechtergerechtigkeit',
    introText:
      '<p>Im vorherigen Schritt haben Sie die Auswirkungen des Regelungsvorhabens auf Frauen und Männer identifiziert. Überlegen Sie nun zunächst, ob die Regelungen zielgenauer gefasst oder Ausnahmen vorgesehen werden können. Zudem sind ergänzende <strong>gesetzliche oder nicht-gesetzliche Maßnahmen</strong> denkbar, um die Geschlechterungleichheiten im Regelungsfeld abzubauen, selbst wenn die Gleichstellung der Geschlechter bereits explizites Ziel der Regelung ist. Zum Ausgleich von tatsächlichen Nachteilen oder bereits zur Verhinderung von Nachteilen sind geschlechtsspezifische Maßnahmen zulässig.</p><p>Auch eine nachfolgende <strong>Evaluation</strong> kann dabei helfen, solche Maßnahmen zu entwickeln. Sollte die <strong>unzureichende Datenlage</strong> einer differenzierten Betrachtung der Auswirkungen eines Regelungsvorhabens entgegen stehen, können Maßnahmen dabei helfen, die <strong>Datenlage</strong> zu den Geschlechterstrukturen im Regelungsfeld zu <strong>verbessern</strong>.</p>',
    info: {
      title: 'Rechtliche Grundlagen zur Zulässigkeit geschlechtsspezifischer Maßnahmen',
      drawerTitle: 'Rechtliche Grundlagen',
      content: `<span>Die Arbeitshilfe basiert auf:</span>
            <ul><li><strong>Art. 3 Abs. 2 Grundgesetz (GG)</strong>: „Männer und Frauen sind gleichberechtigt. Der Staat fördert die tatsächliche Durchsetzung der Gleichberechtigung von Frauen und Männern und wirkt auf die Beseitigung bestehender Nachteile hin.“</li>
            <li><strong>Art. 3. Abs. 3 Vertrag über die Europäische Union (EUV) i. V. m. Art. 8 Vertrag über die Arbeitsweise der Europäischen Union (AEUV)</strong>: Sie machen die Förderung der Gleichstellung von Frauen und Männern zu einem Teil der Rechtsordnung der Europäischen Union.</li>
            <li><strong>Art. 157 Abs. 1 AEUV</strong>: erhebt die Entgeltgleichheit für Frauen und Männer bei gleicher oder gleichwertiger Arbeit zu einem Grundsatz, dessen Anwendung von jedem Mitgliedstaat der Europäischen Union sichergestellt werden muss. Art. <strong>157 Abs. 4 AEUV</strong> erlaubt ausdrücklich spezifische Vergünstigungen zur Erleichterung der Berufstätigkeit des unterrepräsentierten Geschlechts oder zur Verhinderung beziehungsweise zum Ausgleich von Benachteiligungen in der beruflichen Laufbahn.</li>
            <li>Auch die <strong>Charta der Grundrechte der Europäischen Union (GRCh)</strong> sieht in <strong>Art. 23 Abs. 1</strong> vor, Gleichheit von Frauen und Männern in allen Bereichen sicherzustellen. Nach <strong>Art. 23 Abs. 2</strong> dürfen dafür spezifische Vergünstigungen für das unterrepräsentierte Geschlecht beibehalten oder eingeführt werden.</li>
            <li><strong>§ 4 Bundesgleichstellungsgesetz (BGleiG)</strong>: Hier werden alle Beschäftigten der Bundesverwaltung verpflichtet, die Gleichstellung von Frauen und Männern zu fördern und diese Verpflichtung als durchgängiges Leitprinzip bei allen Aufgabenbereichen und Entscheidungen der Dienststelle zu berücksichtigen. Abs. 3 verpflichtet die Bundesverwaltung, die Gleichstellung von Frauen und Männern auch sprachlich zum Ausdruck zu bringen.</li>
            <li><strong>§ 2 und Kapitel 6 (Rechtsetzung) Gemeinsame Geschäftsordnung der Bundesministerien (GGO):</strong>
            <ul><li><strong>§ 2 GGO</strong> bestimmt die Förderung der Gleichstellung von Frauen und Männern zum Leitprinzip bei allen politischen, normgebenden und verwaltenden Maßnahmen.</li>
            <li><strong>§ 45 Abs. 1 i. V. m. Anlage 6 Nr. 9 a GGO</strong> schreibt die Beteiligung des Bundesministeriums für Familie, Senioren, Frauen und Jugend (BMFSFJ) zu der Frage vor, ob durch das Gesetz beziehungsweise die Rechtsverordnung (dazu <strong>§ 62 Abs. 2 GGO</strong>) Auswirkungen von gleichstellungspolitischer Bedeutung zu erwarten sind.</li>
            <li>Nach <strong>§ 43 Nr. 5 GGO</strong> sind in der Begründung die Gesetzesfolgen (<strong>§ 44 Abs. 1 GGO</strong>) darzustellen. Diese beabsichtigten oder unbeabsichtigten Auswirkungen einer Regelung sind – auch hinsichtlich ihrer gleichstellungspolitischen Bedeutung – zu analysieren und in der Begründung darzustellen.</li>
            <li><strong>§ 42 Abs. 5 GGO</strong> verpflichtet, die Gleichstellung sprachlich zum Ausdruck zu bringen.</li></ul></li>
            <li>Das <strong>Bundesgremienbesetzungsgesetz (BGremBG)</strong> verpflichtet den Bund, darauf hinzuwirken, dass eine gleichberechtigte Teilhabe von Frauen und Männern in Gremien, für die er Berufungs- oder Entsenderechte hat, geschaffen wird.</li></ul>`,
    },
    section1: {
      title: 'Leitfragen der Gleichstellungsstrategie',
      part1:
        'Als <strong>Leitprinzipien</strong> zur Herstellung von Geschlechtergerechtigkeit können die Leitfragen aus der <a target="_blank" href="https://www.gleichstellungsstrategie.de/">Gleichstellungsstrategie der Bundesregierung</a> dienen.',
      hiddenInfo: {
        trigger: 'Leitfragen einblenden',
        content: `<ul><li>Wie schaffen wir es, dass sich Frauen und Männer im Lebensverlauf gleichermaßen beruflich entwickeln, mit ihrem Einkommen gut leben und Fürsorgeaufgaben wahrnehmen können?</li>
                <li>Wie schaffen wir es, dass Frauen und Männer gleichermaßen an der Gestaltung der Zukunft unseres Landes in Wirtschaft, Politik, Kultur und Wissenschaft beteiligt sind?</li>
                <li>Wie kann die Bundesregierung Gleichberechtigung zwischen Frauen und Männern in allen Politikbereichen herstellen?</li></ul>`,
      },
      part2:
        'In der <a target="_blank" href="https://www.gleichstellungsstrategie.de/">Gleichstellungsstrategie der Bundesregierung</a> finden Sie konkrete Ziele sowie Beispiele für Regelungsalternativen und Maßnahmen.',
    },
    section2: {
      title: 'Entwicklungsziele der„Agenda 2030 - Nachhaltige Familienpolitik“',
      part1:
        'Ergänzend formuliert der Bericht <strong>„Agenda 2030 – Nachhaltige Familienpolitik“</strong> konkrete und überprüfbare Entwicklungsziele für die Familienpolitik bis zum Jahr 2030. Hier hat sich die Bundesregierung in Anlehnung an <strong>SDG 5 „Geschlechtergerechtigkeit“</strong> einige Ziele gesetzt, die zu einer weiteren Gleichstellung der Geschlechter bis 2030 beitragen sollen.',
      hiddenInfo: {
        trigger: 'Ziele einblenden',
        content: `<ul><li>Die Müttererwerbstätigkeit nähert sich der Vätererwerbstätigkeit weiter an.</li>
                <li>Rund 80 Prozent der erwerbstätigen Mütter erzielen ein existenzsicherndes Erwerbseinkommen.</li>
                <li>Rund jeder zweite Vater nimmt Elternzeit, bezieht Elterngeld und nimmt sich damit Zeit für seine Kinder.</li>
                <li>Der Abstand bei der Zeit, die Väter und Mütter minderjähriger Kinder in die Familie investieren, reduziert sich um 30 Prozentpunkte.</li></ul>`,
      },
      part2: '',
    },
    section3: {
      title: 'Angaben zu Maßnahmen',
      content:
        'Ergeben sich aus diesen Überlegungen heraus Maßnahmen, mit denen nachteilige Auswirkungen vermieden oder Potenziale zur Förderung von Geschlechtergerechtigkeit genutzt werden können?',
      massnahmenInputLabel: 'Maßnahmen',
    },
  },
  gleichstellungZusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    section1:
      '<p>Die Zusammenfassung können Sie zur Darstellung geschlechterdifferenzierter Gesetzesfolgen in einem Regelungsentwurf nutzen. Dabei bietet es sich auch an, die gleichstellungspolitischen Wirkungen des Vorhabens vor dem Hintergrund der Ziele aus der <a target="_blank" href="https://www.gleichstellungsstrategie.de/">Gleichstellungsstrategie der Bundesregierung</a> darzustellen.</p><p>Es ist zudem sinnvoll, diese Ergebnisse für die Nachhaltigkeitsprüfung, insbesondere das Sustainable Development Goal (SDG) 5 „Geschlechtergerechtigkeit“ zu nutzen.</p>',
    auswirkungen: {
      title: 'Beschreibung der Auswirkungen',
      zurBearbeitungBtn: 'zur Bearbeitung',
      unmittelbareUndMittelbar: 'Unmittelbare und mittelbare Auswirkungen',
      weitere: 'Weitere gleichstellungspolitische Auswirkungen',
      keineAuswirkungen: 'Keine Auswirkungen',
    },
    massnahmen: {
      title: 'Maßnahmen',
      maßnahmenGerechtigkeit: 'Maßnahmen zur Förderung der Geschlechtergerechtigkeit',
      keineMassnahmen: 'Es wurden keine Maßnahmen zur Förderung der Geschlechtergerechtigkeit beschrieben.',
    },
    ergebnisse: {
      title: 'Zusammenfassung der Ergebnisse',
      abschliessendeZusammenfassung: 'Abschließende Zusammenfassung',
    },
    irrelevant: {
      case1:
        'Von dem Regelungsvorhaben sind Frauen und Männer nicht unterschiedlich betroffen. Eine Gleichstellungsrelevanz ist nicht gegeben.',
      case2:
        'Von dem Regelungsvorhaben sind Frauen und Männer nicht mittelbar oder unmittelbar betroffen. Eine Gleichstellungsrelevanz ist nicht gegeben.',
      zurBearbeitung: 'zur Bearbeitung',
    },
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    subtitle: 'Gleichstellungspolitische Auswirkungen',
    infoParagraph:
      'Diese Angaben können Sie nach Fertigstellung des Moduls in den allgemeinen Teil der Begründung des Regelungsentwurfs übernehmen, um die gleichstellungspolitischen Auswirkungen des Regelungsvorhabens unter dem Punkt „Gesetzesfolgen/Regelungsfolgen“ darzustellen.',
  },
  contactPerson: de_contacts.gleichstellung.contactPerson,
};
