// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_evaluierung = {
  name: 'Evaluierung',
  einleitung: {
    linkName: 'Einführung',
    title: 'Evaluierung',
    content: {
      paragraph1: {
        content: `Das Modul Evaluierung basiert auf den Vorgaben des <a href='/egesetzgebung-platform-backend/arbeitshilfen/download/34#page=35' target='_blank'>§ 44 Abs. 7 GGO</a> sowie der Beschlüsse des Staatssekretärsausschusses Bessere Rechtssetzung und Bürokratieabbau vom 23. Januar 2013 zur <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/51#page=35" target="_blank">Konzeption zur Evaluierung neuer Regelungsvorhaben</a> sowie zur <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/51#page=38" target="_blank">Fortentwicklung der Evaluationskonzeption der Bundesregierung</a> vom 26. November 2019.  Eine Evaluierung stellt einen Zusammenhang her zwischen Ziel und Zweck einer Regelung und den tatsächlich erzielten Wirkungen sowie den damit verbundenen Kosten.`,
      },
      paragraph2: {
        content: `Das Modul unterstützt Sie bei der Prüfung, ob eine spätere Evaluierung des Regelungsvorhabens erforderlich ist. Zum anderen erhalten Sie Informationen, die Ihnen bei der Planung der Evaluierung sowie der konkreten Ausgestaltung der Evaluierungsklausel als Teil der Begründung des Regelungsentwurfs mit Blick auf die zeitlichen und inhaltlichen Vorgaben helfen.`,
      },
    },
  },
  pruefungErforderlichkeit: {
    linkName: 'Prüfung der Erforderlichkeit einer Evaluierung',
    schwellenWerte: {
      linkName: 'Schwellenwerte Erfüllungsaufwand',
    },
    weitereEvaluierungsanlaesse: {
      linkName: 'Weitere Evaluierungsanlässe',
    },
    content: {
      paragraph1: `Bei allen wesentlichen Regelungsvorhaben sind in der Begründung zum Regelungsentwurf gem. <a href="/egesetzgebung-platform-backend/arbeitshilfen/download/34#page=35" target='_blank'>§ 44 Abs. 7 GGO</a> Aussagen zur Durchführung oder Nicht-Durchführung von Evaluierungen aufzunehmen. Als wesentlich gelten Regelungsentwürfe, bei denen ein jährlicher Erfüllungsaufwand von mindestens <ul><li>1 Mio. € Sachkosten oder 100.000 Stunden Aufwand für Bürgerinnen und Bürger oder</li> <li>1 Mio. € für die Wirtschaft oder</li><li>1 Mio. € für die Verwaltung</a></ul> aufgrund der Ex ante-Abschätzung zu erwarten ist oder – ist eine solche Abschätzung nicht möglich – nicht ausgeschlossen werden kann.`,
      paragraph2:
        'Das Unterschreiten der festgelegten Schwellenwerte bedeutet jedoch nicht, dass keine Prüfung mehr erforderlich ist, da der Staatssekretärsbeschluss auch auf weitere Anlässe für eine Evaluierung hinweist (z. B. hoher finanzieller Gesamtaufwand, besondere politische Bedeutung). Umgekehrt ist es aber auch möglich, dass ein Ressort seine im Regelungsentwurf getroffenen Evaluierungserwägungen überprüfen kann, sollte die Nachmessung ergeben, dass der Schwellenwert unterschritten wird.',
      paragraph3:
        'Nachfolgende Fragen sollen Sie in einem ersten Schritt bei der Prüfung unterstützen, ob eine Evaluierung des Regelungsvorhabens erforderlich ist bzw. vorgesehen werden sollte.',
      drawer: {
        title: 'Wesentlichkeit eines Regelungsvorhabens',
        content:
          '<p>Die Wesentlichkeit eines Regelungsvorhabens im Sinne des Arbeitsprogramms bessere Rechtsetzung bemisst sich grundsätzlich nach der Höhe des zu erwartenden jährlichen Erfüllungsaufwands (Schwellenwert). Damit wird der Tatsache Rechnung getragen, dass eine Evaluierung auch einen Beitrag dazu leisten soll, den Erfüllungsaufwand dauerhaft niedrig zu halten.<br>Als wesentlich gelten danach Regelungsentwürfe, bei denen ein jährlicher Erfüllungsaufwand von mindestens</p><ul><li>1 Mio. € Sachkosten oder 100.000 Stunden Aufwand für Bürgerinnen und Bürger oder</li><li>1 Mio. € für die Wirtschaft oder</li><li>1 Mio. € für die Verwaltung</li></ul><p>aufgrund der Ex ante- Abschätzung zu erwarten ist oder – ist eine solche Abschätzung nicht möglich – nicht ausgeschlossen werden kann.</p>',
      },
      optionsTitle:
        'Nachfolgende Fragen sollen Sie in einem ersten Schritt bei der Prüfung unterstützen, ob eine Evaluierung des Regelungsvorhabens erforderlich ist.',
      optionYes: 'Ja',
      optionYesUndZwar: 'Ja und zwar',
      optionNo: 'Nein',
      options: {
        minErfuellungsaufwandFuerBuerger:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro Sachkosten oder 100.000 Stunden Aufwand <span>für Bürgerinnen und Bürger</span> verursachen?',
        minErfuellungsaufwandFuerWirtschaft:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro <span>für die Wirtschaft</span> verursachen?',
        minErfuellungsaufwandFuerVerwaltung:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro <span>für die Verwaltung</span> verursachen?',
        hoherFinanziellerGesamtaufwand:
          'Wird der Regelungsentwurf voraussichtlich einen hohen finanziellen Gesamtaufwand (hohe Haushaltsausgaben oder sonstige Kosten) verursachen?',
        grosseUnsicherheiten:
          'Sind bei der Abschätzung der möglichen Folgen des Regelungsentwurfs große Unsicherheiten über Wirkungen und Verwaltungsvollzug festgestellt worden?',
        besonderePolitischeBedeutung: 'Hat der Regelungsentwurf eine besondere politische Bedeutung?',
        gruendeEvaluierungNachJahren:
          'Gibt es weitere Gründe, die für eine Evaluierung des Regelungsentwurfs nach drei bis fünf Jahren sprechen?',
      },
      optionsschwellenWerte: {
        minErfuellungsaufwandFuerBuerger:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro Sachkosten oder 100.000 Stunden Aufwand <span>für Bürgerinnen und Bürger</span> verursachen?',
        minErfuellungsaufwandFuerWirtschaft:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro <span>für die Wirtschaft</span> verursachen?',
        minErfuellungsaufwandFuerVerwaltung:
          'Wird der Regelungsentwurf voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro <span>für die Verwaltung</span> verursachen?',
      },
      optionsweitereEvaluierungsanlaesse: {
        hoherFinanziellerGesamtaufwand:
          'Wird der Regelungsentwurf voraussichtlich einen hohen finanziellen Gesamtaufwand (hohe Haushaltsausgaben oder sonstige Kosten) verursachen?',
        grosseUnsicherheiten:
          'Sind bei der Abschätzung der möglichen Folgen des Regelungsentwurfs große Unsicherheiten über Wirkungen und Verwaltungsvollzug festgestellt worden?',
        besonderePolitischeBedeutung: 'Hat der Regelungsentwurf eine besondere politische Bedeutung?',
        gruendeEvaluierungNachJahren:
          'Gibt es weitere Gründe, die für eine Evaluierung des Regelungsentwurfs nach drei bis fünf Jahren sprechen?',
      },
      optionsweitereEvaluierungsanlaesseInfoDrawerTitle5: 'Große Unsicherheiten über Wirkungen und Verwaltungsvollzug',
      optionsweitereEvaluierungsanlaesseInfoDrawerContent5:
        'Auch wenn für einen Regelungsbereich eine große Unsicherheit bezüglich Rechtsfolgen besteht, weil in diesem Bereich bislang wenig Evaluierungserkenntnisse vorliegen, kann einer Evaluierung hohe Bedeutung zukommen.',
      optionsweitereEvaluierungsanlaesseInfoDrawerTitle6: 'Besondere politische Bedeutung',
      optionsweitereEvaluierungsanlaesseInfoDrawerContent6:
        'Anhaltspunkte für die Bedeutung eines Vorhabens können sich aus der politischen und gesellschaftlichen Diskussion bei der Abstimmung des Vorhabens ergeben. Auch die Intensität von Eingriffen und/oder die Relevanz von Leistungen im Einzelfall können unabhängig von der Höhe der Fallzahl auf eine große Bedeutung hinweisen.',
      summaryError: 'Bitte geben Sie die Zusammenfassung an.',
      summaryLabel: `Eine Evaluierung ist laut Ihren Angaben nicht erforderlich. Bitte fassen Sie gem. <a href='/egesetzgebung-platform-backend/arbeitshilfen/download/34#page=34' target='_blank'>§ 44 Abs. 7 GGO</a> für die Begründung des Regelungsentwurfs kurz zusammen, warum eine Evaluierung nicht erforderlich ist.`,
      summary: 'Zusammenfassung',
    },
  },
  gruendeFuerVerzicht: {
    linkName: 'Gründe für Verzicht auf eine Evaluierung',
    content: {
      infoDrawerTitle: 'Evaluierung durch die EU-Kommission und Nachbarcheck',
      infoDrawerContent:
        'Wird mit einem Regelungsvorhaben in Deutschland eine EU-Richtlinie umgesetzt, sollte geprüft werden, ob auf europäischer Ebene bereits eine Evaluierung stattgefunden hat. Die EU-Kommission evaluiert eigene Rechtsvorschriften. Sollte hier bereits eine geeignete Evaluierung vorliegen, ist zu prüfen, inwieweit eine eigene Evaluierung sinnvoll ist. Dies hängt maßgeblich von der konkreten Umsetzung in Deutschland ab und inwieweit bei der Evaluierung auf EU-Ebene eine nationale Umsetzung berücksichtigt worden ist. Sollte eine weitere Evaluierung durch das zuständige Ressort gewünscht sein, ist die Umsetzung der Rechtsvorschrift auf nationaler Ebene zu prüfen, um ggf. Probleme bei der Umsetzung oder bei der entsprechenden Rechtsvorschrift identifizieren zu können. Hier bietet sich auch der sogenannte Nachbarcheck an. Neben den Evaluierungsergebnissen der EU-Kommission sollten demnach in dem eigenen Evaluierungsbericht die Erfahrungen aus anderen Mitgliedsstaaten einbezogen werden. Beispielsweise könnte untersucht werden, ob die Umsetzung in den Niederlanden oder in Frankreich besser funktioniert hat. Best-Practice-Beispiele können so identifiziert und Bessere Rechtsetzung auf europäischer und nationaler Ebene gewährleistet werden.',
      paragraph1:
        'Die Erforderlichkeitsprüfung hat ergeben, dass die Voraussetzungen für eine Evaluierung gemäß dem Beschluss des Staatssekretärsausschusses Bessere Rechtsetzung und Bürokratieabbau vom 23. Januar 2013 vorliegen. Sollte es sich bei dem Regelungsvorhaben um die Umsetzung einer EU-Richtlinie handeln, sind weitere Aspekte zu berücksichtigen.',
      paragraph2:
        'Darüber hinaus ist zu beachten, dass der Aufwand für die Evaluierung in einem angemessenen Verhältnis zu den daraus zu gewinnenden Erkenntnissen stehen muss. Dies gilt zum Beispiel im Hinblick auf:<ul> <li> gerichtliche Entscheidungen oder internationale bzw. EU-Vorgaben ohne Umsetzungsspielraum (1:1);</li> <li> anderweitige vergleichbare Berichtspflichten – auch gegenüber oder von internationalen bzw. EU-Institutionen – oder eine gesetzlich vorgeschriebene Wirkungsforschung als Daueraufgabe. </li> </ul> Bevor Sie sich weitere Gedanken zur konkreten Ausgestaltung der Evaluierung machen, ist daher zunächst zu klären, ob es aus Ihrer Sicht Gründe gibt, trotz der vorliegenden Voraussetzungen von einer Evaluierung abzusehen.',

      optionFrage: 'Wollen Sie von einer Evaluierung absehen, obwohl die Voraussetzungen hierfür vorliegen?',
      optionYes: 'Ja',
      optionNo: 'Nein',
      additionalText:
        'Begründen Sie Ihre Entscheidung, warum Sie von einer Evaluierung absehen wollen, obwohl die Voraussetzungen hierfür vorliegen',
    },
  },
  ausgestaltungDerEvaluierung: {
    linkName: 'Ausgestaltung der Evaluierungsklausel',
    was: {
      linkName: 'Was wird evaluiert?',
    },
    wie: {
      linkName: 'Wie wird evaluiert?',
    },
    wann: {
      linkName: 'Wann wird evaluiert?',
    },
    intro: {
      part1:
        'Wenn Sie die Entscheidung getroffen haben, dass das Regelungsvorhaben evaluiert werden soll, überlegen Sie sich, wie die Evaluierungsklausel',
      introInfoDrawer1: {
        title: 'Erklärung zu "Evaluierungsklausel":',
        content: `
            <p>Es gibt grundsätzlich zwei Möglichkeiten, wie eine Evaluierung im Regelungsentwurf verankert werden kann. Zum einen können Angaben zu einer Evaluierung unter Punkt VII. „Befristung; Evaluierung“ im allgemeinen Teil der Begründung gemacht werden. Zum anderen kann eine Evaluierung im Regelungstext genannt werden (gesetzliche Evaluierungsklausel). Auf Grundlage der in diesem Modul gemachten Angaben kann auch eine gesetzliche Evaluierungsklausel formuliert werden. Die Formulierung selbst erfolgt jedoch direkt im Editor unter Berücksichtigung der Vorgaben des Handbuchs der Rechtsförmlichkeit.</p>`,
      },
      part2:
        'ausgestaltet werden soll. In der Evaluierungsklausel wird spezifiziert, welche Ziele bei der Evaluierung zugrunde gelegt und welche Indikatoren für die Zielerreichung dabei voraussichtlich herangezogen werden. Zudem sollten Sie, soweit möglich, überlegen, welchen Zeitrahmen die Evaluierung haben soll und welche möglichen Datenquellen für die Evaluierung herangezogen werden können. Auch die von der Regelung Betroffenen',
      introInfoDrawer2: {
        title: 'Hinweis zu Betroffenen einer Regelung:',
        content: `
            <p>Sofern Sie die Anwendung Vorbereitung genutzt haben, finden Sie Informationen zu diesem Aspekt auch in Schritt 1: Analyse des Regelungsfeldes: Problem- und Systemanalyse der Anwendung.</p>`,
      },
      part3:
        'sollten identifiziert werden. Ob genaue Angaben hierzu bereits in die Evaluierungsklausel übernommen werden, entscheidet das Ressort aus fachlicher Sicht. Da sich Regelungen während eines Gesetzgebungsverfahrens noch verändern können, müssen im weiteren Verfahren ggf. auch die Überlegungen zur Evaluierung sowie die Formulierung der Evaluierungsklausel angepasst werden.',
    },
    intro2:
      'Die nachfolgenden Schritte sollen Sie bei der Festlegung unterstützen, nach welchen Kriterien, wie und wann die Evaluierung durchgeführt werden soll. Nicht alle Überlegungen, die Sie hier darlegen, müssen notwendigerweise in den allgemeinen Teil der Begründung übernommen werden. Sie können die nachfolgenden Schritte nutzen, um zunächst einmal Ideen für die Ausgestaltung der vorgesehenen Evaluierung zu sammeln. Diese können Sie beispielsweise für die konkrete Planung der Evaluierung zu einem späteren Zeitpunkt nutzen. Die Formulierung der Evaluierungsklausel für den allgemeinen Teil der Begründung erfolgt im Schritt „Zusammenfassung“.',
    intro3: `Weiterführende Hinweise zur Planung und Durchführung einer Evaluierung finden Sie in der <a href='/egesetzgebung-platform-backend/arbeitshilfen/download/51' target='_blank'>Arbeitshilfe zur Evaluierung von Regelungsvorhaben der Bundesregierung</a>. Zudem können Sie das Beratungsangebot des Statistischen Bundesamtes in Anspruch nehmen. Hierfür wenden Sie sich bitte per E-Mail an <a href="mailto:{{email}}" title="mailto:{{email}}">{{email}}</a>`,
    intro3_email: de_contacts.evaluierung.ausgestaltungDerEvaluierung.intro3_email,
    questionPages: {
      was: {
        title: 'Was wird evaluiert?',
        intro:
          'Bei der Evaluierung können verschiedene Evaluierungskriterien herangezogen werden, die auch in der Evaluierungsklausel zu nennen sind. Wichtigstes Kriterium ist die Zielerreichung',
        introInfoDrawer1: {
          title: 'Erklärung zur Zielerreichung:',
          content: `
            <p>Bei einer Evaluierung sollte frühzeitig festgelegt werden, welche Indikatoren für die Zielerreichung voraussichtlich herangezogen werden. Die Indikatoren sind die tatsächlichen Umstände, anhand deren Veränderung bewertet wird, ob die vom Regelungsvorhaben angestrebten Ziele erreicht werden. Ist Ziel des Regelungsvorhabens beispielsweise den Ausbau der Leitungs- und Ladeinfrastruktur für die Elektromobilität im Gebäudebereich zu beschleunigen, kann die Wirksamkeit des Gesetzes z. B. anhand der Anzahl der Ladesäulen bemessen werden.</p>`,
        },
        introPart2:
          ', da Regelungen, die das im Regelungsentwurf formulierte Ziel verfehlen, meist unnötigen Erfüllungsaufwand verursachen. Weitere Prüfkriterien können – je nach Umfang der Evaluierung – sein:',
        introList: [
          'positive oder negative Nebenfolgen der Regelung,',
          'Akzeptanz der Regelung (z. B. Inanspruchnahme staatlicher Angebote),',
          'Praktikabilität der Regelung, die im Hinblick auf vermeidbaren Erfüllungsaufwand zu prüfen ist (z. B. Bündelung von Aufgaben bei einer Vollzugsbehörde) sowie',
          'Abwägungen, ob die entstandenen Kosten in einem angemessenen Verhältnis zu den Ergebnissen stehen. Dies muss nicht zwangsläufig eine monetäre Betrachtung sein, sondern kann auch als Abwägung von Vor- und Nachteilen einer Regelung erfolgen. Politische Entscheidungen sollen dabei nicht vorweggenommen werden.',
        ],
        intro2: 'Es sollen auch Angaben dazu gemacht werden,',
        intro2List: [
          'aufgrund welcher Daten die Zielerreichung ermittelt wird,',
          'wie die Verfügbarkeit dieser Daten sichergestellt wird sowie',
          'über welchen Zeitraum diese Zahlen erhoben werden.',
        ],
        intro3:
          'Eventuelle datenschutzrechtliche Fragestellungen sollten frühzeitig bei der Planung, Konzeptionierung und Datensammlung einer Evaluierung berücksichtigt werden.',
        labelTextarea: 'Benennen Sie hier die Evaluierungskriterien sowie mögliche Datengrundlagen',
        fieldName: 'wasWirdEvaluiert',
      },
      wie: {
        title: 'Wie wird evaluiert?',
        intro:
          'Eine Evaluierung ist hinsichtlich ihrer Tiefe (z. B. Regelungsvorhaben insgesamt, Teile eines Artikelgesetzes, einzelne Bereiche eines Stammgesetzes, Vollzug), der Methodik (interne Selbst- oder externe Fremdevaluierung)',
        introInfoDrawer1: {
          title: 'Erklärung zu "Selbst- und Fremdevaluierung":',
          content: `
            <p>In der Literatur wird grundsätzlich zwischen Selbst- und Fremdevaluierung unterschieden. Eine Selbstevaluierung liegt vor, wenn das für die konkrete Regelung zuständige Fachreferat die Evaluierung selbst durchführt. Wird die Evaluierung von einem anderen Fachreferat des Ressorts oder von nachgeordneten Behörden geplant und durchgeführt, handelt es sich um eine Mischform aus Selbst- und Fremdevaluierung. Eine häufig benutzte Kombination ist auch die, in der Externe spezifische Aufgaben übernehmen oder Arbeitspakete erstellen, die Gesamtbewertung aber vom Fachreferat vorgenommen und der Bericht auch intern erstellt wird. Beide beschriebenen Evaluierungen werden hier unter dem Begriff der internen Evaluierung zusammengefasst. Abgegrenzt dazu handelt es sich bei der vollständigen Fremdevaluierung um eine Vergabe an externe Evaluierende.</p>`,
        },
        introPart2:
          ' und des Umfangs nicht vorbestimmt. Diese Entscheidungen obliegen dem jeweils federführenden Ressort und sind in der Evaluierungsklausel darzustellen. Die Evaluierung kann auf den Ergebnissen der Nachmessung des Erfüllungsaufwands durch das Statistische Bundesamt aufbauen bzw. mit dieser verknüpft werden.',
        intro2: 'Die Dokumentation der Ergebnisse soll mittels Evaluierungsbericht',
        intro2InfoDrawer1: {
          title: 'Erklärung zu "Evaluierungsbericht":',
          content: `
            <p>Die Evaluierungsberichte werden den betroffenen Ressorts sowie dem Koordinator der Bundesregierung für Bürokratieabbau und bessere Rechtsetzung und dem Nationalen Normenkontrollrat zur Kenntnis gegeben.</p>
            <p>Interne Evaluierungsberichte werden grundsätzlich vor ihrer Veröffentlichung durch eine unabhängige Stelle hinsichtlich ihrer Qualität überprüft. Bei Regelungsvorhaben, bei denen der jährliche Erfüllungsaufwand einen Wert von 5 Millionen Euro überschreitet, findet eine Qualitätssicherung stets statt. Die Prüfung sollte sich darauf beziehen, ob in der Evaluierung nachvollziehbar und plausibel auf die im Gesetzentwurf getroffenen Annahmen hinsichtlich der Ziele und der Wirkungen eingegangen wurde und welche Daten dafür herangezogen wurden. Der NKR bietet an, diese Qualitätssicherung durchzuführen.</p>
            <p>Bei Evaluierungen, die den Erfüllungsaufwand zum Gegenstand haben können, binden die Ressorts das Statistische Bundesamt (StBA) frühzeitig in die Planung von Evaluierungen ein, um eine sinnvolle Verzahnung der Nachmessung des Erfüllungsaufwands und der Evaluierung sicherzustellen.</p>`,
        },
        intro2Part2:
          ' erfolgen, der Aussagen zur Abgrenzung des Untersuchungsgegenstands, zu den zugrundeliegenden Daten und Annahmen sowie zu den relevanten Evaluierungskriterien enthält. Der Bericht soll in anschaulicher Form die wesentlichen Erkenntnisse der Evaluierung darstellen.',
        labelTextarea: 'Legen Sie hier den Umfang und die Methodik der Evaluierung fest',
        fieldName: 'wieWirdEvaluiert',
      },
      wann: {
        title: 'Wann wird evaluiert?',
        intro:
          'Das federführende Ressort entscheidet nach fachlichem Ermessen über den Zeitpunkt der Evaluierung. Bei der Wahl eines geeigneten Zeitpunkts sind der erwartete Eintritt von Wirkungen sowie ggfs. weitere Änderungen des regulatorischen Umfelds zu berücksichtigen. In der Regel soll eine Evaluierung 3-5 Jahre nach Inkrafttreten eines Regelungsvorhabens durchgeführt werden. Angaben zum Zeitpunkt der Evaluierung sind ebenfalls in die Evaluierungsklausel aufzunehmen.',
        labelTextarea: 'Legen Sie hier den Zeitpunkt der Evaluierung fest',
        fieldName: 'wannWirdEvaluiert',
      },
    },
    wasWirdEvaluiertLabel: 'Was wird evaluiert?',
    wasWirdEvaluiertError: 'Bitte geben Sie an, was evaluiert werden soll.',
    wasWirdEvaluiertDrawer: {
      content:
        '<p>Wichtiges Evaluierungskriterium ist die Zielerreichung, da Regelungen, die das im Regelungsentwurf formulierte Ziel verfehlen, meist unnötigen Erfüllungsaufwand verursachen. Dabei sollte frühzeitig festgelegt werden, welche Indikatoren für die Zielerreichung voraussichtlich herangezogen werden. Die Indikatoren sind die tatsächlichen Umstände, anhand deren Veränderung bewertet wird und ob die vom Regelungsvorhaben angestrebten Ziele erreicht werden. Ist Ziel des Regelungsvorhabens beispielsweise, den Ausbau der Leitungs- und Ladeinfrastruktur für die Elektromobilität im Gebäudebereich zu beschleunigen, kann die Wirksamkeit des Gesetzes z. B. anhand der Anzahl der Ladesäulen bemessen werden.</p><p>Darüber hinaus sollen Angaben darüber gemacht werden,</p><ul><li>aufgrund welcher Daten die Zielerreichung ermittelt wird,</li><li>wie die Verfügbarkeit dieser Daten sichergestellt wird sowie</li><li>über welchen Zeitraum diese Zahlen erhoben werden.</li></ul><p>Eventuelle datenschutzrechtliche Fragestellungen sollten frühzeitig bei der Planung, Konzeptionierung und Datensammlung einer Evaluierung berücksichtigt werden.</p><p>Weitere Prüfkriterien können – je nach Umfang der Evaluierung – sein:</p><ul><li>Positive oder negative Nebenfolgen der Regelung</li><li>Akzeptanz der Regelung (z. B. Inanspruchnahme staatlicher Angebote)</li><li>Praktikabilität der Regelung, die im Hinblick auf vermeidbaren Erfüllungsaufwand zu prüfen ist (z. B. Bündelung von Aufgaben bei einer Vollzugsbehörde)</li><li>Abwägungen, ob die entstandenen Kosten in einem angemessenen Verhältnis zu den Ergebnissen stehen. Dies muss nicht zwangsläufig eine monetäre Betrachtung sein, sondern kann auch als Abwägung von Vor- und Nachteilen einer Regelung erfolgen. Politische Entscheidungen sollen dabei nicht vorweggenommen werden.</li></ul>',
    },
    wieWirdEvaluiertLabel: 'Wie wird evaluiert?',
    wieWirdEvaluiertError: 'Bitte geben Sie an, wie evaluiert werden soll.',
    wieWirdEvaluiertDrawer: {
      content:
        '<p>Eine Evaluierung ist hinsichtlich ihrer Tiefe (z. B. Regelungsvorhaben insgesamt, Teile eines Artikelgesetzes, einzelne Bereiche eines Stammgesetzes, Vollzug), der Methodik (von interner bis hin zu einer wissenschaftlichen Evaluierung) und des Umfangs (von „Zwei-Seiten-Bericht“ bis hin zu einem ausführlichen Bericht – auch abhängig von den zur Verfügung stehenden Ressourcen) nicht vorbestimmt. Diese Entscheidungen obliegen dem jeweils federführenden Ressort. Die Evaluierung kann auf den Ergebnissen der Nachmessung des Erfüllungsaufwands durch das Statistische Bundesamt aufbauen.</p><p>Die Dokumentation der Ergebnisse soll mittels Evaluierungsbericht erfolgen, der Aussagen zur Abgrenzung des Untersuchungsgegenstands, zu den zugrundeliegenden Daten und Annahmen sowie zu den relevanten Prüfkriterien enthält. Der Bericht soll in anschaulicher Form die wesentlichen Erkenntnisse der Evaluierung darstellen.</p><p>Diese Berichte werden den betroffenen Ressorts sowie dem Koordinator der Bundesregierung für Bürokratieabbau und bessere Rechtsetzung im Bundeskanzleramt und dem Nationalen Normenkontrollrat zur Kenntnis gegeben.</p>',
    },
    wannWirdEvaluiertLabel: 'Wann wird evaluiert?',
    wannWirdEvaluiertError: 'Bitte geben Sie an, wann evaluiert werden soll.',
    wannWirdEvaluiertDrawer: {
      content:
        'Das federführende Ressort entscheidet nach fachlichem Ermessen über den Zeitpunkt der Evaluierung. Bei der Wahl eines geeigneten Zeitpunkts sind der erwartete Eintritt von Wirkungen sowie ggfs. weitere Änderungen des regulatorischen Umfelds zu berücksichtigen. I.d.R. soll eine Evaluierung 3-5 Jahre nach Inkrafttreten eines Regelungsvorhabens durchgeführt werden.',
    },
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    gruendeFuerDenVerzicht: 'Gründe für den Verzicht auf eine Evaluierung',
    keineEvaluierung:
      'Sie können Ihre Angaben zu den drei Leitfragen noch einmal überprüfen und auf dieser Grundlage eine Evaluierungsklausel für den allgemeinen Teil der Begründung (VII. Befristung; Evaluierung) formulieren.',
    summary: 'Formulieren Sie hier eine Evaluierungsklausel für den allgemeinen Teil der Begründung.',
    summaryInfoDrawer: {
      title: 'Hinweise zur Evaluierungsklausel',
      content: `
                  <h2>Evaluierungsklauseln</h2>
                  <p>
                  Evaluierungsklauseln können je nach Regelungsvorhaben unterschiedlich ausgestaltet sein. So kann es Evaluierungsklauseln geben, in denen deutlich zu machen ist, dass Evaluierungen – auch kumulativ – auf unterschiedlichen Ebenen der Rechtsetzung möglich sind und z.B. die nationale Evaluierung eines Umsetzungsgesetzes mit der Evaluierung der zugrundeliegenden Richtlinie auf Unionsebene verknüpft sein kann. Daneben kann es Evaluierungsklauseln geben, innerhalb derer zwischen verschiedenen Aspekten des Regelungsentwurfs unterschieden wird, da z.B. nicht alle Teile des Regelungsvorhabens evaluierungsbedürftig sind oder sich die Datengrundlagen je nach zu evaluierendem Teilaspekt unterscheiden.
                  </p>
                  <h3>Beispiel Evaluierungsklausel: SanktionenrechtsÜbG</h3>
                  <p>
                  Bei den Änderungen im Bereich der Ersatzfreiheitsstrafe […] soll anhand der Angaben des Statistischen Bundesamtes […] drei Jahre nach Inkrafttreten der Neuregelung untersucht werden, wie sich die Zahl der an den monatlichen Stichtagen wegen einer Ersatzfreiheitsstrafe inhaftierten Personen in Relation zur Anzahl der insgesamt verhängten Geldstrafen entwickelt hat. Außerdem soll zu diesem Zeitpunkt untersucht werden, ob die Halbierung des Umrechnungsmaßstabes zu einem wesentlichen Nachlassen des Tilgungsdrucks und damit der Zahlungsbereitschaft geführt hat; dies soll insbesondere anhand der Entwicklung des Anteils der durch Zahlung erledigten Geldstrafen geschehen […].
                  </p>
                  <p>
                  Im Hinblick auf die Änderungen im Recht der Unterbringung in einer Entziehungsanstalt […] soll beobachtet werden, wie sich die Zahl der nach § 64 StGB untergebrachten Personen […] nach Inkrafttreten der Neuregelungen fortentwickelt. Zu diesem Zweck sollen die jährlichen Angaben des Statistischen Bundesamts zur Anzahl der nach § 64 StGB untergebrachten Personen […], die jährlichen Zahlen des Statistischen Bundesamtes zu den Anordnungen nach § 64 StGB […] und Angaben der Länder zur durchschnittlichen Dauer der Unterbringung ausgewertet werden […]. Dabei wird es vor allem darum gehen, ob der seit vielen Jahren zu beobachtende Anstieg der Zahl der nach § 64 StGB untergebrachten Personen gebremst werden kann.
                  </p>`,
    },
    summaryError: 'Bitte geben Sie die Zusammenfassung an.',
    evaluierungErforderlichkeitNichtDurchgefuehrt: 'Eine Evaluierung des Regelungsvorhabens ist nicht erforderlich.',
    evaluierungErforderlichkeitNichtVorgesehen: 'Eine Evaluierung des Regelungsvorhabens ist nicht vorgesehen.',
    evaluierungErforderlichkeitDurchgefuehrt: 'Gründe, warum eine Evaluierung erforderlich ist',
    evaluierungAusgestaltungDurchgefuehrt:
      'Begründung, warum trotz Vorliegen der Voraussetzungen von einer Evaluierung abgesehen wird',
    keineEvaluierungOptions: {
      minErfuellungsaufwandFuerBuerger:
        'Der Regelungsentwurf verursacht voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro Sachkosten oder 100.000 Stunden Aufwand für Bürgerinnen und Bürger.',
      minErfuellungsaufwandFuerWirtschaft:
        'Der Regelungsentwurf verursacht voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro für die Wirtschaft.',
      minErfuellungsaufwandFuerVerwaltung:
        'Das Regelungsvorgaben verursacht voraussichtlich einen jährlichen Erfüllungsaufwand von mindestens einer Million Euro für die Verwaltung.',
      hoherFinanziellerGesamtaufwand:
        'Der Regelungsentwurf verursacht voraussichtlich einen hohen finanziellen Gesamtaufwand.',
      grosseUnsicherheiten:
        'Bei der Abschätzung der möglichen Folgen des Regelungsentwurfs wurden große Unsicherheiten über Wirkungen und Verwaltungsvollzug festgestellt.',
      besonderePolitischeBedeutung: 'Der Regelungsentwurf hat eine besondere politische Bedeutung.',
      gruendeEvaluierungNachJahren:
        'Es gibt weitere Gründe, die für eine Evaluierung des vorliegenden Regelungsentwurfs nach drei bis fünf Jahren sprechen.',
    },
    summaryWeitereGruende: 'Weitere Gründe für eine Evaluierung',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    infoParagraph:
      'Sie können den Text in den allgemeinen Teil der Begründung (VII. Befristung; Evaluierung) des Regelungsentwurfs übernehmen. Falls Sie eine gesetzliche Evaluierungsklausel einfügen möchten, können Sie den Text zusätzlich für deren Formulierung sowie für den besonderen Teil der Begründung nutzen. Die Formulierung der Evaluierungsklausel erfolgt jedoch im Editor.',
    aussagenZurNichtDurchfuehrung: 'Aussagen zur Nicht-Durchführung einer Evaluierung',
    aussagenZurDurchfuehrung: 'Aussagen zur Durchführung einer Evaluierung',
  },
  contactPerson: de_contacts.evaluierung.contactPerson,
};
