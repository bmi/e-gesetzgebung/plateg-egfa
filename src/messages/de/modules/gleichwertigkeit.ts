// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_gleichwertigkeit = {
  name: 'Gleichwertigkeits-Check',
  auswirkungenDrawer: {
    title: 'Erklärung zu: ',
    intro: '<p>Folgende Fragen können Sie beim Formulieren der Auswirkungen unterstützen:</p>',
  },
  einleitung: {
    linkName: 'Einführung',
    title: 'Gleichwertigkeits-Check',
    additionalInfoTitle: 'Hintergrund',
    text1: `<h3>Ziel des Gleichwertigkeits-Checks</h3>
          <p>Ziel des <strong>Gleichwertigkeits-Checks</strong> ist es, bei <strong>Gesetzesvorhaben</strong> des Bundes Gleichwertigkeits-Belange mitzudenken und für die <strong>Gleichwertigkeit der Lebensverhältnisse</strong> zu sensibilisieren. Die Auseinandersetzung mit dem Thema ist insoweit auch ein Beitrag zu <strong>besserer Rechtsetzung</strong>. Der Gleichwertigkeits-Check hat appellativen Charakter, es handelt sich um keine explizit in <strong><a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank"> § 44 GGO</a></strong> genannte Gesetzesfolge. Die Auswirkung eines Gesetzes auf die Gleichwertigkeit von Lebensverhältnissen stellt aber eine <strong>wesentliche Auswirkung i.S.d. Absatzes 1 Satz 1</strong> dar. Der vorliegende Leitfaden dient als <strong>Hilfestellung</strong> zur Vornahme dieser Gesetzesfolgenabschätzung.</p>
`,
    text2: `<p>Deutschland ist ein vielfältiges Land. Diese Vielfalt ist Teil der Erfolgsgeschichte der Bundesrepublik. Alle in Deutschland lebenden Menschen sollten faire und gleichwertige Chancen auf gute Lebens- und Arbeitsbedingungen haben. Allerdings ist sowohl zwischen als auch innerhalb von Regionen eine Verschiebung der Chancenverteilung zu beobachten, die Wanderungsbewegungen in attraktivere Regionen mit Folgen für Auslastung und Anpassungsbedarf in Weg- und Zuzugsregionen auslösen kann.</p>
          <p>Es besteht die Gefahr, dass sich Ungleichgewichte verstetigen oder zunehmen und eine nachhaltige Entwicklung und den Zusammenhalt in unserem Land beeinträchtigen. Die Kommission „Gleichwertige Lebensverhältnisse“ hat von September 2018 bis Juli 2019 Empfehlungen erarbeitet, wie dieser Gefahr begegnet werden kann. Ausgehend von Vorschlägen der vorsitzenden Bundesministerinnen und Bundesminister der Kommission hat das Bundeskabinett in seiner Sitzung vom 10. Juli 2019 u.a. beschlossen, dass <strong>der Bund künftig bei allen Gesetzesvorhaben prüfen wird, welche Wirkungen sie auf die Wahrung und Förderung gleichwertiger Lebensverhältnisse in Deutschland haben („Gleichwertigkeits-Check“)</strong>. Wirtschaft, Beschäftigung, Infrastruktur, Daseinsvorsorge, demografische Entwicklung sowie kulturelles und soziales Zusammenleben sollen überall in Deutschland gute Lebensperspektiven befördern. Ziel ist es, allen Menschen in allen Regionen nach Möglichkeit gleichwertige Chancen und Teilhabe zu eröffnen. Die Herstellung gleichwertiger Lebensverhältnisse ist ein übergeordnetes Ziel von großer politischer Bedeutung.</p>
           `,
    text3: `<h3>Prüfung</h3>
          <p>Prüfen Sie in diesem Modul, ob und wie sich <strong>Gesetzesvorhaben des Bundes</strong> auf die <strong>Gleichwertigkeit der Lebensverhältnisse der Menschen</strong> auswirken, d.h. etwa ungleichwertige Lebensverhältnisse verringern, nicht beeinflussen/verfestigen oder verstärken.</p>
          <p>Lebensverhältnisse sind anhand verschiedener Faktoren bewertbar. Eine einheitliche Definition von Gleichwertigkeit gibt es allerdings nicht, so dass auch <strong>Aussagen zu möglichen Auswirkungen einer im Einzelfall zu treffenden Einschätzung</strong> unterliegen. In Fällen von Auswirkungen wird das <strong>Prüfergebnis im Gesetzentwurf</strong> und dort <strong>im Allgemeinen Teil der Begründung</strong> grundsätzlich <strong>unter „VI. 6. Weitere Gesetzesfolgen“ abgebildet</strong> und kurz erläutert. Sollten Sie in anderen Modulen der Gesetzesfolgenabschätzung oder in weiteren Prüfungen identische Aspekte geprüft haben, können Sie im Gleichwertigkeits-Check darauf verweisen. Bei den einzelnen zu prüfenden Faktoren werden inhaltliche Anknüpfungspunkte zu anderen Modulen aufgezeigt.</p>
          <p>Inhaltliche Überschneidungen gibt es insbesondere zu folgenden Modulen der Gesetzesfolgenabschätzung:</p>
          <ul>
            <li><a href="{{demoCheckLink}}" target="_blank" style="white-space: normal">Demografie-Check</a></li>
            <li><a href="{{enapLink}}" target="_blank" style="white-space: normal">eNAP – elektronische Nachhaltigkeitsprüfung</a></li>
            <li><a href="{{gfaLink}}" target="_blank" style="white-space: normal">Gleichstellungsorientierte GFA</a></li>
            <li><a href="{{disabilityLink}}" target="_blank" style="white-space: normal">Disability Mainstreaming</a></li>
          </ul>
          <p>Die Gleichwertigkeits-Prüfung kann unter Berücksichtigung der unterschiedlichen regionalen Entwicklungen und des demografischen Wandels anhand folgender Faktoren, die gleichwertige Lebensverhältnisse wesentlich beeinflussen, erfolgen. Wird ein Faktor für das Regelungsvorhaben als relevant identifiziert, werden beispielhaft vertiefende Prüffragen genannt.</p>
          `,
  },
  faktorFinanzsituation: {
    linkName: 'Finanzsituation der Kommunen',
    title: 'Faktor „Finanzsituation der Kommunen“',
    text: '<p>Für die Gleichwertigkeit der Lebensverhältnisse sind die finanziellen Möglichkeiten der kommunalen Gebietskörperschaften und regionalen Netzwerke für Investitionen in Entwicklungsvorhaben und die Bereitstellung von technischen und sozialen Infrastrukturen oder Leistungen für die Bürgerinnen und Bürger von großer Bedeutung.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Gesetzesvorhaben, mit denen die Finanzlage in den Regionen beeinflusst wird, z.B. in den Bereichen Steuern, Sozialleistungen, staatliche Investitionen, Fördermaßnahmen, sind daher auf ihren Einklang mit dem Ziel der Herstellung gleichwertiger Lebensverhältnisse im Bundesgebiet zu überprüfen. Dabei ist zu berücksichtigen, dass die Länder die entscheidende Rolle haben, Unterschiede in der Entwicklung einzelner Kommunen innerhalb eines Landes auszugleichen.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorFinanzsituation',
          content: 'die finanziellen Möglichkeiten der regionalen Gebietskörperschaften oder der regionalen Netzwerke?',
        },
      ],
      text: 'faktorFinanzsituationSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchen Einnahme- bzw. Ausgabebereichen und auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) ist dies der Fall? </li>
              <li>Werden finanzschwache Gebietskörperschaften in ihren finanziellen Möglichkeiten gestärkt/nicht beeinflusst/geschwächt? </li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  faktorWirtschaft: {
    linkName: 'Wirtschaft und Innovation',
    title: 'Faktor „Wirtschaft und Innovation“',
    text: '<p>Die Entwicklungschancen von Regionen hängen wesentlich von den Beschäftigungs- und Einkommensmöglichkeiten der Menschen und den wirtschaftlichen Perspektiven der Unternehmen ab.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Daher sind Gesetzesvorhaben dahingehend zu prüfen, ob und inwieweit sie in den Regionen und ihrer Wirtschaft zu gleichwertigen Möglichkeiten für Arbeit und Einkommen, Qualifizierung und Fachkräfteangebot sowie Innovationen beitragen oder unterschiedliche Entwicklungschancen der Wirtschaft z.B. durch Umbrüche der Wirtschaftsstrukturen verstärken. Dabei stehen die Potenziale strukturschwacher Regionen sowie ihre wirtschaftliche Stabilität und Leistungsfähigkeit im Zentrum.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorWirtschaft',
          content: 'die wirtschaftliche Wettbewerbsfähigkeit von Regionen oder regional bedeutsamen Branchen?',
        },
      ],
      text: 'faktorWirtschaftSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchem Bereich (Wirtschaftsleistung, Produktivität, Gründungskultur, Forschungs- und Entwicklungs- sowie Investitionstätigkeit von Unternehmen) ist dies der Fall?</li>
              <li>Wirkt sich das Vorhaben auf Beschäftigung, Erwerbstätigkeit und Einkommen in den Regionen aus? Welche Regionen sind betroffen (z.B. Gemeinden, Städte, Kreise)?</li>
              <li>Werden strukturschwache Regionen in ihrer wirtschaftlichen Wettbewerbsfähigkeit gestärkt/nicht beeinflusst/geschwächt?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  faktorMobilitaet: {
    linkName: 'Mobilität und digitale Infrastruktur',
    title: 'Faktor „Mobilität und digitale Infrastruktur“',
    text: '<p>Die Anbindung an barrierefreie Mobilitätsangebote und an digitale Infrastruktur ist grundlegende Voraussetzung für die Erreichbarkeit von Einrichtungen der Daseinsvorsorge, gesellschaftliche Teilhabe, für Wirtschaftswachstum, Beschäftigung und Wohlstand.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Jeder in der Fläche wirkende Beitrag für ein flächendeckend verfügbares, nutzerorientiertes, leistungsfähiges, erschwingliches sowie nachhaltiges Mobilitäts- und Breitband-/Mobilfunkangebot ist daher eine Investition in gleichwertige Lebensverhältnisse. Gesetzesvorhaben insbesondere zu Mobilitätsangeboten und digitalen Infrastrukturen sollen nicht nur unter dem Wirtschaftlichkeitsaspekt, sondern müssen gerade auch im Hinblick auf die Bedeutung für die Regionalentwicklung und gesellschaftliche Teilhabe betrachtet werden. Bei begrenzt verfügbaren Ressourcen erscheint dennoch eine Priorisierung gesamtwirtschaftlich geboten.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorMobilitaetRegionen',
          content: 'die Mobilität in Regionen?',
        },
        {
          name: 'faktorMobilitaetInfrastrukturen',
          content: 'die Versorgung mit digitalen Infrastrukturen?',
        },
        {
          name: 'faktorMobilitaetDienstleistungen',
          content: 'die Erreichbarkeit von Dienstleistungen, Verwaltungsleistungen?',
        },
      ],
      text: 'faktorMobilitaetSummary',
    },
    summary: {
      text: `<ul>
              <li>Welche Auswirkungen sind dies und in welchem Bereich (z.B. Breitband, Mobilfunk, ÖPNV etc.) sowie auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) sind sie zu erwarten?</li>
              <li>Werden unterdurchschnittlich erschlossene Regionen in ihrer Anbindung gestärkt/nicht beeinflusst/geschwächt?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  faktorDaseinsvorsorge: {
    linkName: 'Daseinsvorsorge',
    title: 'Faktor „Daseinsvorsorge“',
    text: '<p>Die Daseinsvorsorge mit ihren unterschiedlichen Bereichen ist eine wesentliche Voraussetzung für gleichwertige Lebensverhältnisse und den gesellschaftlichen Zusammenhalt.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Daseinsvorsorge ist die Sicherung der barrierefreien Grundversorgung mit lebensnotwendigen Gütern und Dienstleistungen zu einem verträglichen Preis und in einer angemessenen Entfernung. Gesetzesvorhaben sind daher auf ihre Wirkungen insbesondere in den Bereichen Nahversorgung (Lebensmittel, Grundbedarf), Gesundheit und Pflege (Ärztliche, Krankenhaus- und Notfall-Versorgung, ambulante/stationäre Pflegeeinrichtungen), Teilhabe von Menschen mit Behinderungen, Vereinbarkeit von Familie, Pflege und Beruf, Kinderbetreuung und Bildung, der Sicherheit (Polizei, Feuerwehr, Katastrophenschutz) und Verwaltungsleistungen zu prüfen.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorDaseinsvorsorge',
          content: 'die Daseinsvorsorge?',
        },
      ],
      text: 'faktorDaseinsvorsorgeSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchem Bereich (z.B. Nahversorgung, Gesundheit, Pflege, Kinderbetreuung, Bildung, Barrierefreiheit, Verwaltungsleistungen, Sicherheit) sowie auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) ist dies der Fall?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  faktorEngagement: {
    linkName: 'Engagement, Zusammenhalt und Teilhabe',
    title: 'Faktor „Engagement, Zusammenhalt und Teilhabe“',
    text: '<p>Maßnahmen, die Respekt, Toleranz und Solidarität in unserer Gesellschaft stärken und das Bewusstsein für den Wert unserer Demokratie, unserer Grundrechte und unserer Staatsordnung stärken, tragen zum gesellschaftlichen Zusammenhalt bei.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Auch soziale Regeln des Miteinanders, die Verbundenheit mit dem Gemeinwesen und das Vertrauen in Institutionen sind dafür wichtige Bedingungen. Gesetzesvorhaben sind daher daraufhin zu prüfen, ob und inwieweit sie sich auf das Vertrauen in Institutionen, auf das soziale Miteinander auswirken und unseren gesellschaftlichen Zusammenhalt stärken. Gesellschaftlicher Zusammenhalt im Alltag wird zudem dadurch gefördert, dass die Vielfalt der Gesellschaft im staatlichen Handeln allgemein und bei Gesetzesvorhaben auch angemessen berücksichtigt wird.</p>
          <p>Auch die Möglichkeit zur Teilhabe an kulturellen Aktivitäten sowie gute Möglichkeiten der Freizeitgestaltung und Erholung sowie Orte der Begegnung sind wichtiger Bestandteil gesellschaftlicher Teilhabe. Aktivitäten im Ehrenamt und bürgerschaftlichen Engagement leisten wesentliche Beiträge zu einem offenen und partizipativen gemeinschaftlichen Leben. Dies gilt in besonderer Weise für Menschen mit Behinderungen. In strukturschwachen Gebieten ist es deshalb von hoher Bedeutung, politische Bildung sowie ehrenamtliches und bürgerschaftliches Engagement anzuerkennen und zu unterstützen und dadurch zu gleichwertigen Lebensverhältnissen beizutragen.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorEngagementZusammenhalt',
          content: 'den gesellschaftlichen Zusammenhalt, das Ehrenamt oder bürgerschaftliche Engagement?',
        },
        {
          name: 'faktorEngagementTeilhabe',
          content: 'die demokratische, gesellschaftliche und kulturelle Teilhabe, auf Freizeit und Erholung?',
        },
        {
          name: 'faktorEngagementEinbeziehung',
          separate: true,
          content:
            'die betroffene Bevölkerung oder werden bestimmte Zielgruppen in den Kommunen, Dörfern oder Quartieren bzw. deren Perspektiven in die Planungen oder das Vorhaben einbezogen?',
        },
      ],
      text: 'faktorEngagementSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchem Bereich sowie auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) ist dies der Fall? </li>
              <li>In welchen Bereichen (inhaltlich) wird der gesellschaftliche Zusammenhalt oder die Teilhabe gestärkt und etwaigen Benachteiligungen entgegengewirkt?</li>
              <li>Stärken die Maßnahmen besonders strukturschwache Regionen?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: ' Auswirkungen und/oder Art der Einbeziehung',
    },
  },
  faktorWohnraum: {
    linkName: 'Räumliche Strukturen und Wohnraum',
    title: 'Faktor „Räumliche Strukturen und Wohnraum“',
    text: '<p>Wohnraummangel und Gebäudeleerstände sowie über- und unterausgelastete Infrastrukturen und Einrichtungen haben unmittelbaren Einfluss auf die persönlichen Lebensbedingungen vor Ort. Gleiches gilt für möglichst zusammenhängende Natur- und Landschaftsräume.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Gesetzesvorhaben sind daher daraufhin zu prüfen, ob und inwieweit sie sich auf eine in diesem Sinne nachhaltige Siedlungs- und Raumstruktur, insbesondere auf den Vorrang der Innen- vor der Außenentwicklung, sowie die Erhaltung zusammenhängender Freiflächen und Wälder auswirken. Der Bund kann durch die Ansiedlung von Behörden und anderen Einrichtungen ebenfalls Einfluss auf eine nachhaltige Siedlungs- und/oder Bevölkerungsstruktur, mit einer möglichen Belebung auch der regionalen Wirtschaft, nehmen.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorWohnraumStruktur',
          content:
            'die vorhandene Siedlungs- und Raumstruktur, z.B. auf die regionale Über- und Unterauslastung von noch länger nutzbaren Gebäuden, Infrastrukturen und Einrichtungen?',
        },
        {
          name: 'faktorWohnraumInnenAussen',
          content: 'das Ziel „Innenentwicklung vor Außenentwicklung“?',
        },
        {
          name: 'faktorWohnraumAttraktivitaet',
          content:
            'die Attraktivität des Lebens und unentgeltlichen Arbeitens (Sorgearbeit und Ehrenamt) in strukturschwachen Regionen im Vergleich zu strukturstarken Räumen?',
        },
      ],
      text: 'faktorWohnraumSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchem Bereich sowie auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) wird die Siedlungsstruktur in strukturschwachen Gebieten (Beispiel: hohe Gebäudeleerstände) oder strukturstarken Gebieten (Beispiel: hohe Kosten für Wohnraum) gestärkt/nicht beeinflusst/geschwächt?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
              <li>Sofern mit dem Vorhaben die Aus- oder Neugründung einer Behörde bzw. öffentlichen Stelle betroffen ist: Werden mit dem Vorhaben strukturschwache Regionen gestärkt? Wenn ja, in welchem Ausmaß und in welchen Regionen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  faktorNatur: {
    linkName: 'Natürliche Lebensgrundlagen',
    title: 'Faktor „Natürliche Lebensgrundlagen“',
    text: '<p>Die natürlichen Lebensgrundlagen sind auch in Verantwortung für künftige Generationen zu schützen. Eine intakte Natur und Landschaft sowie die Umweltfaktoren sind auch im engeren räumlichen Umfeld ein wichtiger Faktor für die Gesundheit und die persönliche Lebensqualität.</p>',
    additionalInfoTitle: 'Weitere Informationen',
    additionalInfo: `<p>Gesetzesvorhaben sind daher daraufhin zu prüfen, ob und inwieweit sie sich auf Natur- und Kulturlandschaften, auf die Qualität von Luft und Wasser, auf das Klima und die Biodiversität sowie auf die Vermeidung von Lärm auswirken.</p>`,
    question: {
      label: `Hat das Vorhaben Auswirkungen auf`,
      radios: [
        {
          name: 'faktorNaturMenschen',
          content: 'den Schutz des Menschen?',
        },
        {
          name: 'faktorNaturTiere',
          content: 'Tiere, Pflanzen und biologische Vielfalt?',
        },
        {
          name: 'faktorNaturKlima',
          content: 'das Klima, auf Natur- und Kulturlandschaften?',
        },
        {
          name: 'faktorNaturFlaeche',
          content: 'den Flächenverbrauch?',
        },
        {
          name: 'faktorNaturQualitaet',
          content: 'die Qualität von Wasser, Boden und Luft?',
        },
      ],
      text: 'faktorNaturSummary',
    },
    summary: {
      text: `<ul>
              <li>In welchem Bereich sowie auf welcher räumlichen Ebene (z.B. Gemeinden, Städte, Kreise) ist dies der Fall?</li>
              <li>In welchen von negativen Umweltveränderungen oder -einflüssen betroffenen Regionen werden die natürlichen Lebensgrundlagen gestärkt/nicht beeinflusst/geschwächt?</li>
              <li>Sind bestimmte Bevölkerungsgruppen besonders betroffen?</li>
            </ul>`,
      label: 'Auswirkungen',
    },
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    summary: 'Bitte fassen Sie Ihre Ergebnisse abschließend zusammen.',
    noImpact: 'Das Vorhaben hat keine Auswirkungen auf die Gleichwertigkeit der Lebensverhältnisse.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    infoParagraph:
      'Diese Angaben können Sie nach Fertigstellung des Moduls in den allgemeinen Teil der Begründung des Regelungsentwurfs übernehmen, um die Auswirkungen des Regelungsvorhabens auf die Gleichwertigkeit der Lebensverhältnisse unter dem Punkt „Gesetzesfolgen/Regelungsfolgen“ darzustellen.',
    subtitle: 'Zusammenfassung',
    impact: 'Auswirkungen auf die Gleichwertigkeit der Lebensverhältnisse',
    noImpact: 'Das Vorhaben hat keine Auswirkungen auf die Gleichwertigkeit der Lebensverhältnisse.',
  },
  contactPerson: de_contacts.gleichwertigkeit.contactPerson,
};
