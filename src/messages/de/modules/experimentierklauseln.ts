// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_experimentierklauseln = {
  name: 'Assistenten für Experimentierklauseln – Prüfung und Formulierung',
  intro: {
    linkName: 'Einführung',
    title: 'Assistent für Experimentierklauseln',
    text1: `<strong>Reallabore</strong> sind ein Schlüssel für die Transformation. Unter möglichst realen Bedingungen ermöglichen sie die befristete Erprobung innovativer Technologien, Produkte, Dienstleistungen oder Ansätze, um innovative Lösungen schnell und sicher in die Anwendung zu bringen.`,
    hiddenSection1: {
      title: 'Definition Reallabore',
      text: `<p>Im Rahmen dieser Module werden Reallabore als Politikinstrument entsprechend den Definitionen des <a href="{{link1}}" target="_blank">Rates der Europäischen Union</a> (TZ 8-10) definiert. Demnach zeichnen sich Reallabore vor allem durch folgende Merkmale aus:</p>
              <ul>
                  <li>befristete Erprobung innovativer Technologien, Produkte, Dienstleistungen oder Ansätze,</li>
                  <li>möglichst reale Bedingungen,</li>
                  <li>Beteiligung der zuständigen Behörde, die die Erprobung je nach Ausgestaltung des Reallabors beaufsichtigt, aktiv begleitet, unterstützt und/oder kontrollierte Ausnahmen von allgemeinen rechtlichen Vorgaben gestattet, und</li>
                  <li>regulatorisches Lernen.</li>
                </ul>
      `,
    },
    text2: `Die rechtliche Grundlage für Reallabore sind oft <strong>Experimentierklauseln</strong> (auch Erprobungsklauseln genannt), also Rechtsvorschriften, die es den für ihre Umsetzung und Durchsetzung zuständigen Behörden ermöglichen, für die Erprobung innovativer Technologien, Produkte, Dienstleistungen oder Ansätze von Fall zu Fall ein gewisses Maß an Flexibilität walten zu lassen.`,
    hiddenSection2: {
      title: 'Experimentierklauseln und deren Bedeutung',
      text: `<p>In vielen Fällen definieren <strong>Experimentierklauseln</strong> den rechtlichen Rahmen für die Erprobung. Sie dienen zwei zentralen Zwecken: Zum einen schaffen sie sowohl mit befristeten Ausnahmen von fachrechtlichen Vorgaben und Verboten für private oder öffentliche Akteure als auch mit behördlicher Begleitung die Möglichkeit, Innovationen kontrolliert im Reallabor zu erproben, die im allgemeinen Rechtsrahmen an Grenzen oder auf offene Fragen stoßen. Zum anderen kann der Gesetzgeber mit Hilfe von Experimentierklauseln schon zu einem frühen Zeitpunkt Informationen über Innovationen, über deren Wirkungen unter realen Bedingungen und über den passenden rechtlichen Rahmen sammeln. Auf Grundlage der gewonnenen Informationen kann der allgemeine gesetzliche Rahmen so weiterentwickelt werden, dass die deutsche Wirtschaft und Gesellschaft von diesen Innovationen profitieren, ohne dass unvorhersehbare Sicherheitsrisiken eingegangen werden. Experimentierklauseln sind somit ein Baustein eines innovationsfreundlichen und evidenzbasierten Rechtsrahmens und ein entscheidender Faktor eines attraktiven Innovationsstandortes.</p>
              <p>Experimentierklauseln sind kein neues Phänomen – schon in den 1950er-Jahren wurden erste Experimentierklauseln im deutschen Recht verankert. Doch erst mit der rasant fortschreitenden Digitalisierung in Wirtschaft und Gesellschaft gewannen sie in den vergangenen Jahren in Deutschland und weltweit erheblich an Bedeutung.</p>
              <p>Der Rat der Europäischen Union hat im Rahmen der deutschen Ratspräsidentschaft 2020 erstmals <a href="{{link1}}" target="_blank">Schlussfolgerungen zu Reallaboren und Experimentierklauseln</a> als Instrumente eines innovationsfreundlichen und zukunftssicheren Rechtsrahmens beschlossen und somit deren Anwendung in EU-Rechtsakten gestärkt. Dabei verstehen die EU-Mitgliedstaaten Experimentierklauseln als:</p>
              <p>„Rechtsvorschriften, die es den für ihre Umsetzung und Durchsetzung zuständigen Behörden ermöglichen, für die Erprobung innovativer Technologien, Produkte, Dienstleistungen oder Ansätze von Fall zu Fall ein gewisses Maß an Flexibilität walten zu lassen; [...].“</p>
              `,
    },
    text3: `Im deutschen Recht sind Experimentierklauseln bereits in unterschiedlichen Regelungsbereichen verankert.`,
    hiddenSection3: {
      title: 'Beispiele für Experimentierklauseln',
      text: `<ul>
                <li><strong>§ 2 Abs. 7 Personenbeförderungsgesetz:</strong> „Zur praktischen Erprobung neuer Verkehrsarten oder Verkehrsmittel kann die Genehmigungsbehörde auf Antrag im Einzelfall Abweichungen von Vorschriften dieses Gesetzes oder von auf Grund dieses Gesetzes erlassenen Vorschriften für die Dauer von höchstens fünf Jahren genehmigen, soweit öffentliche Verkehrsinteressen nicht entgegenstehen.“</li>
                <li><strong>§ 13 Gewerbeordnung:</strong> „Die Landesregierungen werden ermächtigt, durch Rechtsverordnung zur Erprobung vereinfachender Maßnahmen, insbesondere zur Erleichterung von Existenzgründungen und Betriebsübernahmen, für einen Zeitraum von bis zu fünf Jahren Ausnahmen von Berufsausübungsregelungen nach diesem Gesetz und den darauf beruhenden Rechtsverordnungen zuzulassen, soweit diese Berufsausübungsregelungen nicht auf bindenden Vorgaben des Europäischen Gemeinschaftsrechts beruhen und sich die Auswirkungen der Ausnahmen auf das Gebiet des jeweiligen Landes beschränken.“</li>
                <li><strong>§ 11 Abs. 3 Vertrauensdienstegesetz:</strong> „Innovative Identifizierungsmethoden, die noch nicht durch Verfügung im Amtsblatt anerkannt sind, können von der Bundesnetzagentur im Einvernehmen mit dem Bundesamt für Sicherheit in der Informationstechnik und nach Anhörung der Bundesbeauftragten für den Datenschutz und die Informationsfreiheit für einen Zeitraum von bis zu zwei Jahren vorläufig anerkannt werden, sofern eine Konformitätsbewertungsstelle die gleichwertige Sicherheit der Identifizierungsmethode im Sinne des Artikels 24 Absatz 1 Unterabsatz 2 Buchstabe d der Verordnung (EU) Nr. 910/2014 bestätigt hat. Die Bundesnetzagentur veröffentlicht die vorläufig anerkannten Identifizierungsmethoden auf ihrer Internetseite. Die Bundesnetzagentur und das Bundesamt für Sicherheit in der Informationstechnik überwachen die Eignung der vorläufig anerkannten Identifizierungsmethoden über den gesamten Zeitraum der vorläufigen Anerkennung. Werden durch die Überwachung sicherheitsrelevante Risiken bei der vorläufig anerkannten Identifizierungsmethode erkannt, so kann die Aufsichtsstelle im Einvernehmen mit dem Bundesamt für Sicherheit in der Informationstechnik dem qualifizierten Vertrauensdiensteanbieter die Behebung dieser Risiken durch ergänzende Maßnahmen auferlegen, sofern dies sicherheitstechnisch sinnvoll ist. Lässt sich durch ergänzende Maßnahmen keine hinreichende Sicherheit der vorläufig anerkannten Identifizierungsmethode gewährleisten, so soll die Aufsichtsstelle dem qualifizierten Vertrauensdiensteanbieter die Nutzung dieser Identifizierungsmethode untersagen.“</li>
                <li>
                  <strong>§ 16 Autonome-Fahrzeuge-Genehmigungs-und-Betriebs-Verordnung</strong>
                  <br>
                  (1) „Kraftfahrzeuge, die zur Erprobung von Fahrzeugsystemen oder -teilen und deren Entwicklungsstufen für die Entwicklung automatisierter oder autonomer Fahrfunktionen dienen, dürfen im öffentlichen Straßenraum nur betrieben werden, wenn für das entsprechende Kraftfahrzeug eine Erprobungsgenehmigung des Kraftfahrt-Bundesamts nach § 1i des Straßenverkehrsgesetzes vorliegt. Die Erprobungsgenehmigung nach Satz 1 umfasst auch die Genehmigung zur Erprobung aller Teile, Systeme oder Einheiten des Kraftfahrzeugs. § 19 Absatz 6 der Straßenverkehrs-Zulassungs-Ordnung ist nicht anzuwenden.“
                  <br><br>
                  (2) „Die Erprobungsgenehmigung ist zu befristen und darf einen Geltungszeitraum von vier Jahren im Regelfall nicht überschreiten. Sie ist jeweils für weitere zwei Jahre zu verlängern, wenn die Voraussetzungen der Genehmigungserteilung weiter fortbestehen und der bisherige Verlauf der Erprobung einer Verlängerung nicht entgegensteht. Sollten Dritte gegen die Erprobungsgenehmigung oder deren Verlängerung Rechtsbehelfe einlegen, verlängert sich die Geltungsdauer der Erprobungsgenehmigung um die Anzahl der Tage, an denen der Rechtsbehelf aufschiebende Wirkung entfaltet.“
                  <br><br>
                  […]
                  <br><br>
                  (5) „Das Kraftfahrt-Bundesamt kann zum Zweck der Erprobung von Fahrzeugsystemen oder -teilen und deren Entwicklungsstufen für die Entwicklung automatisierter oder autonomer Fahrfunktionen Ausnahmen genehmigen von
                    <ol>
                      <li>den Vorschriften der §§ 1a und 1e des Straßenverkehrsgesetzes,</li>
                      <li>dieser Verordnung mit Ausnahme von den §§ 15 und 16 und der Straßenverkehrs-Zulassungs-Ordnung.“</li>
                    </ol>
              </li>
            </ul>
              `,
    },
    text4: `Mit dem fortschreitenden Wandel dürfte es in Zukunft zur Daueraufgabe werden, in der Gesetzgebung immer wieder neue Möglichkeiten zur Erprobung von innovativen Lösungen im realen Umfeld zu schaffen. Es ist daher von entscheidender Bedeutung, Experimentierklauseln von Anfang an bei der Erarbeitung und Novellierung von Gesetzen mitzudenken und stets ihre Erforderlichkeit und die Umsetzungsmöglichkeiten zu prüfen.`,
    text5: `Vor dem Hintergrund des Kabinettbeschlusses vom 13. April 2021 (22-Punkte-Paket für Bürokratieerleichterungen) ist eine verbindliche Prüfung der Erforderlichkeit von Experimentierklauseln im Rahmen von Gesetzgebungsverfahren durchzuführen. Die Pflicht zur Durchführung der Prüfung liegt bei der Bundesregierung. Diese Prüfpflicht gilt für alle neuen Gesetze sowie die Überarbeitung von Gesetzen.`,
    hiddenSection4: {
      title: 'Pflicht zur Prüfung von Experimentierklauseln',
      text: `<p>Beschluss des Bundeskabinetts vom 13. April 2021 zum „22-Punkte-Paket für Bürokratieerleichterungen“:</p>
              <p>„In Fachgesetzen soll die Möglichkeit zum „Ausprobieren“ verstärkt werden. Dazu sollen vermehrt Experimentierklauseln genutzt werden, um insbesondere Reallabore zu ermöglichen. Deshalb wollen wir im Rahmen des Ressortprinzips in Zukunft für jedes Gesetz prüfen, ob durch die Aufnahme einer Experimentierklausel innovativen Leistungen Freiraum gegeben werden kann.“</p>
              `,
    },
    text6: `Für die Durchführung der verbindlichen Prüfung der Erforderlichkeit von Experimentierklauseln kann das <strong>Modul zur Prüfung der Erforderlichkeit von Experimentierklauseln</strong> genutzt werden. Ist die Einführung oder Überarbeitung einer Experimentierklausel angezeigt, kann das <strong>Modul zur Formulierung von Experimentierklauseln</strong> genutzt werden. Grundlage für das Modul ist die <a href="{{link1}}" target="_blank">Arbeitshilfe zur Formulierung von Experimentierklauseln</a> sowie das der Arbeitshilfe zugrunde liegende <a href="{{link2}}" target="_blank">Rechtsgutachten aus 2020</a> und dessen <a href="{{link3}}" target="_blank">Erweiterung aus 2024</a>.`,
    text7: {
      link1: `Zum Modul zur Prüfung der Erforderlichkeit von Experimentierklauseln (ab Frühjahr 2025 verfügbar)`,
      link2: `Zum Modul zur Formulierung von Experimentierklauseln`,
    },
  },
  einleitung: {
    linkName: 'Einführung Formulierungsassistent',
    title: 'Einführung: Formulierungsassistent für Experimentierklauseln',
    title2: 'Hinweise zu Aufbau und Bearbeitung',
    text1: `<p>Eine Experimentierklausel kann abhängig von ihrem Regelungsinhalt vielfältig gestaltet werden. Eine idealtypische Experimentierklausel besteht aus bis zu 18 Elementen. Einige dieser Elemente sind notwendige Bestandteile, sofern sie nicht bereits an anderer Stelle im Fachrecht geregelt sind, andere sind optional und müssen nicht zwingend geregelt werden. Im Folgenden wird eine Aufteilung der 18 Elemente auf vier Absätze vorgeschlagen. Diese Aufteilung ist jedoch flexibel und abhängig vom jeweiligen Regelungsinhalt.</p>
            <p>Eine Experimentierklausel kann wie folgt aufgebaut sein:</p>
            <p><strong>Absatz 1</strong> regelt den Erprobungszweck, der das Ermessen der Fachbehörde anleitet und ein wichtiger Anker für die Auslegung der Norm ist.</p>
            <p><strong>Absatz 2</strong> bildet das Kernstück der Experimentierklausel. Dort wird zentral geregelt, ob und wie eine Erprobung stattfindet. Dieser Absatz umfasst zunächst die Grundlagen für die behördliche Entscheidung und wird ergänzt um weitere Elemente, welche die konkrete Ausgestaltung der Erprobung regeln.</p>
            <p><strong>Absatz 3</strong> enthält Bestimmungen zur Evaluation der Erprobung und Befristung der Experimentierklausel selbst. Er ist zentral für das regulatorische Lernen und den Wissenstransfer in die Gesetzgebung.</p>
            <p><strong>Absatz 4</strong> nimmt Bezug zu untergesetzlichem Regelwerk. Mit Verordnungen können ggf. flexible Regelungen geschaffen werden, die bei Bedarf vergleichsweise einfach nachjustiert werden können.</p>
    `,
    imgAltText: `
              Muster einer Experimentierklausel
              ABSATZ 1 - Erprobungszweck
              1. Erprobungszweck
              ABSATZ 2 - Erprobungszweck
              2. Zuständigkeit  
              3. Ermächtigung der Behörde  
              4. Entscheidungsinhalt  
              5. Erprobungsgegenstand  
              6. Materielle Begrenzung  
              7. Verfahrensvorgaben  
              8. Umfang der Erprobung  
              9. Begleitende Pflichten  
              10. Befristung der Erprobung  
              11. Verlängerungsmöglichkeit  
              12. Verhältnis zur aufschiebenden Wirkung von Widerspruch und Klage  
              13. Weitere Nebenbestimmungen  
              14. Möglichkeit der Änderung der Reallaborgestaltung während der Erprobung  
              15. Möglichkeit der Aufhebung
              ABSATZ 3 - Erprobungszweck
              16. Evaluation und Transfer  
              17. Befristung der Experimentierklausel
              ABSATZ 4 - Erprobungszweck
              18. Verordnungsermächtigung
              Quelle: Darstellung des BMWK in Anlehnung an Schmitz et al. (2020), S. 116.
                `,
    text2: `<p>Dieses Schema soll zur Orientierung dienen. Bei Experimentierklauseln, die viele Bausteine enthalten, sollten zum besseren Verständnis der Norm <strong>weitere Untergliederungen</strong> erfolgen.</p>
            <p>Dieses Modul kann genutzt werden, um die <strong>Experimentierklausel schrittweise zu formulieren</strong>, sodass diese anschließend in den Gesetzestext übernommen werden kann. Alternativ kann das Modul als <strong>Informationsquelle</strong> dienen, um Diskussionen vorzubereiten und die Experimentierklausel außerhalb des Tools zu erarbeiten.</p>
            <p>Das Modul ist so aufgebaut, dass Sie Schritt für Schritt die einzelnen Elemente der Experimentierklausel erarbeiten können. Bei jedem Element haben Sie die Möglichkeit, <strong>Eintragungen in den Freitextfeldern</strong> einzufügen. Das können <strong>ganze Sätze oder nur Stichworte</strong> sein, die erst im späteren Verlauf zu ganzen Sätzen und vollständigen Absätzen zusammengefügt werden. Sie können jederzeit, auch ohne Eingaben vorzunehmen, sowohl zu den folgenden Seiten gelangen als auch zu den vorherigen Seiten zurückkehren und ggf. Änderungen an Ihren Eintragungen vornehmen. Auch notwendige Elemente müssen nicht in jedem Sachbereich in der Experimentierklausel geregelt werden, wenn es bspw. bereits allgemeine Regelungen dazu gibt. Daher müssen auf den folgenden Seiten auch die als notwendig markierten Elemente <strong>nicht zwingend</strong> formuliert werden.</p>
            <p>Nachdem Sie die <strong>Elemente 1 bis 15</strong> (Absatz 1 und Absatz 2) durchgeklickt haben, gelangen Sie zu einer Überblicksseite, auf der Ihnen alle bisherigen Eintragungen angezeigt werden. Dort haben Sie die Möglichkeit zusammenhängende Absätze aus Ihren Eintragungen zu formulieren. Im Anschluss an die Seiten zu den <strong>Elementen 16 und 17</strong> (Absatz 3) erreichen Sie die nächste Überblicksseite. Nach <strong>Element 18</strong> (Absatz 4) gelangen Sie zu der letzten Überblicksseite, auf der Sie die bisherigen Formulierungen der Absätze Ihrer Experimentierklausel abschließend bearbeiten und <strong>zu einer vollständigen Experimentierklausel zusammenfügen</strong> können. Nach Fertigstellung wird Ihnen die vollständige von Ihnen formulierte Experimentierklausel angezeigt, die Sie kopieren und in Ihren Regelungsentwurf einfügen können. Das Ergebnis lässt sich außerdem als PDF herunterladen.</p>
    `,
  },
  standortUndTitel: {
    linkName: 'Standort und Titel der Experimentierklausel',
    title: 'Standort und Titel der Experimentierklausel',
    text: `<p>Eine Experimentierklausel kann an verschiedenen Orten stehen:</p>
          <ul>
            <li>direkt bei den Regelungen, von denen abgewichen werden soll,</li>
            <li>bei den Übergangsregelungen oder</li>
            <li>in einem separaten Paragrafen mit der Überschrift „Experimentierklausel [und Evaluierung]“ bzw. „Erprobung [und Evaluierung]“, z. B. am Ende des Gesetzes.</li>
          </ul>
          <h2 class="ant-typography">Ihre Angabe</h2>
          <p>Sofern Ihre Experimentierklausel in einem separaten Paragrafen stehen soll, können Sie im folgenden Freitextfeld den Titel formulieren:</p>
    `,
    titleExperimentierklausel: {
      label: 'Titel der Experimentierklausel',
    },
  },
  absatz1: {
    linkName: 'Absatz 1',
    erprobungszweck: {
      linkName: 'Erprobungszweck',
      title: 'Absatz 1 – Erprobungszweck (optional)',
      text1: `<p>Die Formulierung eines <strong>expliziten Erprobungszwecks</strong> dient der <strong>Normenklarheit</strong> für Rechtsanwender, ist <strong>Auslegungshilfe</strong> für Behörden und Gerichte in der Rechtspraxis und steuert die <strong>Ermessensausübung</strong> der genehmigenden Behörde. Ein klar formulierter, praxisorientierter Erprobungszweck kann daher erheblich zur Leistungsfähigkeit einer Experimentierklausel beitragen. Dieser kann, wie unten dargestellt, einen eigenen Absatz bilden. Alternativ kann er aber auch mit weiteren Elementen verbunden werden.</p>
            <p>In der Praxis wird bisher oft ein <strong>einfacher Erprobungszweck</strong> definiert. Ein darüberhinausgehender Erprobungszweck kann zusätzliche Schwerpunkte für die Erprobung setzen und das Ermessen der zuständigen Behörde lenken. Häufig empfiehlt es sich, einen <strong>doppelten Erprobungszweck</strong> zu formulieren, der neben der praktischen Erprobung der Innovation auch das <strong>regulatorische Lernen für eine dauerhafte Regulierung</strong> als zentrales Element experimenteller Gesetzgebung betont. Die explizite Nennung des doppelten Erprobungszweckes erleichtert die Auslegung und steuert die Ermessensausübung in Zweifelsfällen.</p>
    `,
      hiddenSection1: {
        title: 'Beispiele und Formulierungsmöglichkeiten',
        text: `
              <strong>Einfacher Erprobungszweck:</strong>
              <ul>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „Zur praktischen Erprobung neuer Verkehrsarten oder Verkehrsmittel […]“</li>
              </ul>
              <strong>Darüberhinausgehender Erprobungszweck:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Diese Vorschrift dient der praktischen Erprobung von […]. Die Erprobung nachhaltiger, insbesondere klimafreundlicher, [Technologien/Innovationen] soll als vorrangiger Belang berücksichtigt werden.“</li>
                  <li>Mögliche Formulierung: „Zur Erprobung von […] darf […], wobei nachhaltige, insbesondere klimafreundliche, [Technologien/Innovationen] als vorrangiger Belang berücksichtigt werden sollen.“</li>
              </ul>
              <strong>Doppelter Erprobungszweck:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Diese Vorschrift dient der praktischen Erprobung von […] und der Vorbereitung ihrer dauerhaften Regulierung durch Verwaltung und Gesetzgebung.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Die Formulierung des Erprobungszwecks ist optional. Im folgenden Freitextfeld können Sie den Erprobungszweck für Ihre Experimentierklausel formulieren. Sie können entweder direkt einen vollständigen Satz formulieren oder zunächst nur Stichworte eintragen, die später weiter ausformuliert und ggf. auch mit anderen Elementen verknüpft werden können.</p>`,
      formItem: {
        label: 'Absatz 1 – Element 1: Erprobungszweck (optional)',
      },
      overviewListLegend: 'Absatz 1 – Element 1: Erprobungszweck',
    },
  },
  absatz2: {
    linkName: 'Absatz 2',
    zustaendigkeit: {
      linkName: 'Zuständigkeit',
      title: 'Absatz 2 – Zuständigkeit (notwendig)',
      text1: `<p>Auf dieser und den folgenden Seiten werden einzeln die Elemente 2 bis 15 (Absatz 2) formuliert. In die Freitextfelder bei den einzelnen Elementen können entweder direkt ganze Sätze oder zunächst Stichworte eingetragen werden, die erst im späteren Verlauf zu ganzen Sätzen und vollständigen Absätzen zusammengefügt werden.</p>
            <p><strong>Zuständige Behörde:</strong> Es muss eine Behörde festgelegt werden, die entscheidet, ob und wie die Erprobung einer Innovation durchgeführt und dafür von fachrechtlichen Vorgaben abgewichen werden kann. Dies dürfte im Regelfall die <strong>jeweilige Fachbehörde</strong> sein, die über tiefes Wissen im jeweiligen Sachbereich verfügt. Oftmals ist dies bereits im Fachrecht mit einer <strong>allgemeinen Regelung</strong> definiert und muss dann in der Klausel nicht geregelt werden.</p>
            <p><strong>Behördenbeteiligung:</strong> In <strong>überschneidenden Aufgabengebieten</strong> ist zu beachten, dass sich Experimentierklauseln in die Systematik der Zuständigkeiten einfügen. Es ist zu prüfen, ob andere Behörden beteiligt werden müssen oder sollen. Dies kann – abhängig vom Sach­bereich und Zuständigkeiten – in Form einfacher Beteiligung, z.B. durch <strong>Anhörung</strong>, Abstimmung durch <strong>Benehmen</strong> oder intensiver Abstimmung durch <strong>Einvernehmen</strong> geschehen.</p>
    `,
      hiddenSection1: {
        title: 'Beispiele',
        text: `
                <ul>
                    <li>Beispiel: § 11 Abs. 3 S. 1 Hs. 1 Vertrauensdienstegesetz: „Innovative Identifizierungs­methoden, die noch nicht durch Verfügung im Amtsblatt anerkannt sind, können von der Bundesnetzagentur im Einvernehmen mit dem Bundesamt für Sicherheit in der Informationstechnik und nach Anhörung der Bundesbeauftragten für den Datenschutz und die Informationsfreiheit für einen Zeitraum von bis zu zwei Jahren vorläufig anerkannt werden […].“</li>
                    <li>Beispiel: Art. 31 Bayerisches Kinderbildungs- und -betreuungsgesetz: „Zur Erprobung innovativer Konzepte für die pädagogische Arbeit, die Förderung und das Bewilligungs- und Aufsichtsverfahren kann von den Vorschriften dieses Gesetzes und der hierzu ergangenen Ausführungsverordnung mit Zustimmung des Staatsministeriums unter Beteiligung der übrigen zuständigen Staatsministerien abgewichen werden.“</li>
                </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Sofern nicht an anderer Stelle im Fachrecht bereits vorgegeben, ist die Festlegung der Zuständigkeit in der Experimentierklausel erforderlich. Im folgenden Freitextfeld können Sie die zuständige(n) und zu beteiligende Behörden im Rahmen der Anwendung Ihrer Experimentierklausel notieren:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 2: Zuständigkeit (notwendig)',
      },
    },
    ermaechtigungBehoerde: {
      linkName: 'Ermächtigung der Behörde',
      title: 'Absatz 2 – Ermächtigung der Behörde (notwendig)',
      text1: `<p>Die zuständige Behörde kann unterschiedlich ermächtigt werden:</p>`,

      text1ListItem1:
        'Gebundene Entscheidung („muss“): Die Behörde muss genehmigen, sobald die Voraussetzungen des Tatbestands erfüllt sind.',
      drawerListItem1: {
        title: `Gebundene Entscheidung`,
        text: `In der Praxis gibt es eine solche Experimentierklausel im deutschen Recht bisher nicht.`,
      },
      text1ListItem2:
        'Einfaches Ermessen („kann“, „darf“, „ist berechtigt“, „entscheidet nach pflichtgemäßem Ermessen“): Der Behörde wird Entscheidungsspielraum eingeräumt. Sie entscheidet nach pflichtgemäßem Ermessen in einem zweckmäßigen Umfang. Sie hat im Regelfall ein Entschließungsermessen („Ob“) und Handlungsermessen („Wie“).',
      drawerListItem2: {
        title: `Einfaches Ermessen`,
        text: `Eine solche Ermessensentscheidung ist gerichtlich auf Ermessensfehler überprüfbar (§ 40 VwVfG).`,
      },
      text1ListItem3: `Intendiertes Ermessen (z.B. „soll“): Das Ermessen der Behörde „soll“ grundsätzlich in der vorgegebenen Weise ausgeübt werden, es sei denn es liegen atypische Umstände vor. Eine solche Regelung ist innovationsfreundlicher, sie kommt aber vor allem dann in Betracht, wenn von der Erprobung geringe Risiken erwartet werden.`,
      hiddenSection1: {
        title: 'Beispiele',
        text: `<ul>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „Zur praktischen Erprobung neuer Verkehrsarten oder Ver­kehrsmittel kann die Genehmigungsbehörde auf Antrag im Einzelfall Abweichungen […].“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Ermächtigung der Behörde eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 3: Ermächtigung der Behörde (notwendig)',
      },
    },
    entscheidungsinhalt: {
      linkName: 'Entscheidungsinhalt',
      title: 'Absatz 2 – Entscheidungsinhalt (notwendig)',
      text1: `<p>Auf Rechtsfolgenseite ist zu regeln, <strong>was die Behörde konkret entscheiden darf</strong>. Dazu sind drei Schritte nötig:</p>`,

      text1ListItem1: 'Detaillierte Analyse des Normbestands: ',
      text1ListItem11: `Die Beantwortung folgender Fragen kann bei der Analyse des Normbestands hilfreich sein: Wo liegen <strong>rechtliche Innovationshemmnisse</strong>? Bedarf es Abweichungen vom bestehenden Regulierungsrahmen? Wo sind Abweichungen und Ausnahmen aufgrund <strong>höherrangigem Recht</strong> nicht möglich? Gibt es im Normbestand <strong>konkurrierende Ausnahmeregelungen</strong>?`,
      drawerListItem1: {
        title: `Analyse des Normbestands`,
        text: `<p>Als höherrangiges Recht könnte Unionsrecht entgegenstehen.</p>
                <p>Ein Beispiel für konkurrierende Ausnahmegenehmigungen sind § 2 Abs. 6 Personenbeförderungsgesetz versus § 2 Abs. 7 Personenbeförderungsgesetz.</p>`,
      },
      text1ListItem2: 'Art der Entscheidung festlegen:',
      text1ListItem2ListItem1:
        '<strong>Erprobung mit Zulassung/Genehmigung einer Abweichung (Regelfall)</strong>: Die Experimentierklausel enthält die notwendigen Abweichungen von Vorschriften sowie die temporäre Öffnung des öffentlichen Raums für die Erprobung.',
      text1ListItem2ListItem2:
        '<strong>Erprobung ohne Abweichung</strong>: Die Experimentierklausel legt den rechtlichen Rahmen fest, insb. zu Maßnahmen der Begleitung durch die Behörde, in dem eine Erprobung im öffentlichen Raum erfolgen kann, ohne dass dazu von Vorschriften abgewichen wird.',
      text1ListItem3: `Bedarf es einer Abweichungsregelung, ist die konkrete Abweichung zu definieren:`,
      text1ListItem3ListItem1:
        '<strong>Enge Variante</strong>: konkrete Benennung der Vorschriften, von denen abgewichen werden kann.',
      text1ListItem3ListItem2:
        '<strong>Weite Variante</strong>: Es kann auch weitergehend geregelt werden, dass von den Vorgaben des jeweiligen Fachgesetzes abgewichen werden kann.',
      text2: `Im Rahmen der <strong>weiten Variante</strong> kann für die Fälle, in denen das einfache Recht durch <strong>höherrangiges Recht determiniert</strong> wird, bei Bedarf ergänzt werden, dass für bestimmte, höherrangig determinierte Vorschriften keine Möglichkeit des Abweichens besteht (<strong>konkrete Rückausnahme</strong>) oder die Möglichkeit des Abweichens nur besteht, soweit dies mit höherrangigem Recht vereinbar ist (<strong>weite Rückausnahme</strong>).`,
      hiddenSection2: {
        title: 'Beispiele',
        text: `
              <strong>Enge Variante:</strong>
              <ul>
                  <li>Beispiel: § 181 Abs. 1 Niedersächsisches Kommunalverfassungsgesetz: „Im Interesse der Erhaltung oder Verbesserung der Leistungsfähigkeit einer Kommune kann das für Inneres zuständige Ministerium für die Erprobung neuer Möglichkeiten der Aufnahme und Bewirtschaftung von Krediten im Einzelfall auf Antrag Ausnahmen von den §§ 120 und 122 zulassen.“</li>
              </ul>
              <strong>Weite Variante:</strong>
              <ul>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „[…] Abweichungen von Vorschriften dieses Gesetzes oder von auf Grund dieses Gesetzes erlassenen Vorschriften für die Dauer von höchstens fünf Jahren genehmigen, […].“</li>
              </ul>
              <strong>Kombination:</strong>
              <ul>
                  <li>
                      Beispiel: § 16 Abs. 5 Autonome-Fahrzeuge-Genehmigungs-und-Betriebs-Verordnung: „Das Kraftfahrt-Bundesamt kann zum Zweck der Erprobung von Fahrzeugsystemen oder -teilen und deren Entwicklungsstufen für die Entwicklung automatisierter oder autonomer Fahrfunktionen Ausnahmen genehmigen von
                      <ol>
                        <li>den Vorschriften der §§ 1a und 1e des Straßenverkehrsgesetzes,</li>
                        <li>dieser Verordnung mit Ausnahme von den §§ 15 und 16 und der Straßenverkehrs-Zulassungs-Ordnung.“</li>
                      </ol>
                  </li>
              </ul>
              <strong>Rückausnahme:</strong>
              <ul>
                  <li>Beispiel: § 13 Gewerbeordnung: „Die Landesregierungen werden ermächtigt, durch Rechtsverordnung zur Erprobung vereinfachender Maßnahmen, insbesondere zur Erleichterung von Existenzgründungen und Betriebsübernahmen, für einen Zeitraum von bis zu fünf Jahren Ausnahmen von Berufsausübungsregelungen nach diesem Gesetz und den darauf beruhenden Rechtsverordnungen zuzulassen, soweit diese Berufsausübungsregelungen nicht auf bindenden Vorgaben des Europäischen Gemeinschaftsrechts beruhen […].“</li>
              </ul>
      `,
      },
      text3: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für den Entscheidungsinhalt eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 4: Entscheidungsinhalt (notwendig)',
      },
    },
    erprobungsgegenstand: {
      linkName: 'Erprobungsgegenstand',
      title: 'Absatz 2 – Erprobungsgegenstand (notwendig)',
      text1: `<p>Die Definition des Erprobungsgegenstands ist die <strong>zentrale Stellschraube</strong> der Experimentierklausel und muss eine angemessene <strong>Balance zwischen Bestimmtheit und Flexibilität</strong> aufweisen. Bestimmtheit stellt <strong>Rechtssicherheit</strong> (z.B. rechtsstaatliches Bestimmtheitsgebot, allgemeines Gleichbehandlungsgebot) und <strong>Leistungsstärke</strong> (transparente Entscheidung, einheitliche und gezielte Anwendung) sicher. Die Behörde muss in die Lage versetzt werden, anhand von verlässlichen Kriterien sachlich nachvollziehbar zu entscheiden, welche Innovationen erprobt werden sollen. Flexibilität ist für eine hinreichende <strong>Innovationsoffenheit</strong> nötig. In diesem Spannungsfeld ist eine <strong>Umschreibung</strong> zur Konkretisierung des Erprobungsgegenstands einer detaillierten Definition vorzuziehen. Die Konkretisierung kann folgendermaßen erfolgen:</p>`,

      text1List: [
        {
          text: 'Verwendung anerkannter Oberbegriffe für bestimmbare Sachbereiche',
        },
        {
          text: 'Zusätzliche Verwendung unbestimmter Rechtsbegriffe',
          drawer: {
            title: `Zusätzliche Verwendung unbestimmter Rechtsbegriffe`,
            text: `<p>Wenn im Begriff selbst nicht der innovative bzw. neuartige Aspekt umfasst ist oder wenn mehrere innovative bzw. neuartige Technologien, Produkte, Dienstleistungen oder Ansätze unter dem Tatbestand einer Experimentierklausel erprobt werden sollen, ist es weiterhin möglich, mit unbestimmten Rechtsbegriffen wie „Innovation“, „neu“ oder „neuartig“ zu arbeiten. Dadurch wird eine Vielheit an zu erprobenden Technologien, Produkten, Dienstleistungen und Ansätzen erfasst.</p>
                <p>Unbestimmte Rechtsbegriffe müssen durch die Rechtsetzung nicht zwingend näher konkretisiert werden. Dies erfolgt dann in der Rechtsanwendung durch Behörden und Gerichte. Eine weitere Konkretisierung der unbestimmten Rechtsbegriffe kann jedoch mit Blick auf die Leistungsstärke sinnvoll sein und kann durch die Spezifizierung des Erprobungszecks erfolgen.</p>`,
          },
        },
        {
          text: 'Implizite Konkretisierung durch spezifizierten Erprobungszweck',
          drawer: {
            title: `Implizite Konkretisierung durch spezifizierten Erprobungszweck`,
            text: `<p>Indem der Erprobungszweck (s. Absatz 1 der Experimentierklausel) konkretisiert wird, kann auch der Erprobungsgegenstand eingegrenzt werden.</p>`,
          },
        },
        {
          text: 'Konkretisierung durch Definition',
          drawer: {
            title: `Konkretisierung durch Definition`,
            text: `<p>Der unbestimmte Rechtsbegriff kann im Folgesatz oder einem folgenden Absatz definiert werden. Aufgrund nötiger Flexibilität ist eine weiche Definition vorzuzuziehen.</p>`,
          },
        },
        {
          text: 'Konkretisierung durch Aufzählung ',
          drawer: {
            title: `Konkretisierung durch Aufzählung `,
            text: `<p>Es können bspw. innovative, neue oder neuartige Technologien, Produkte, Dienstleistungen oder Ansätze exemplarisch aufgezählt oder externe Kriterien genannt werden (z.B. Zugehörigkeit zu innovativer Branche oder Inhaberschaft eines Patents).</p>`,
          },
        },
        {
          text: 'Konkretisierung durch untergesetzliches Regelwerk',
          drawer: {
            title: `Konkretisierung durch untergesetzliches Regelwerk`,
            text: `<p>Der Vorteil einer Konkretisierung durch untergesetzliches Regelwerk (z.B. Rechtsverordnung) ist eine schnellere Möglichkeit der Anpassung und so eine höhere Innovationsoffenheit. Nähere Informationen dazu finden Sie in den Ausführungen zu Absatz 4 der Experimentierklausel.</p>`,
          },
        },
      ],

      hiddenSection1: {
        title: 'Beispiele und Formulierungsmöglichkeit',
        text: `
              <strong>Verwendung anerkannter Oberbegriffe für bestimmbare Sachbereiche:</strong>
              <ul>
                  <li>Beispiel: Art. 56 Abs. 1 Bayerisches Digitalgesetz: „Einführung und Fortentwicklung elektronischer Verwaltungsinfrastrukturen“</li>
              </ul>
              <strong>Zusätzliche Verwendung unbestimmter Rechtsbegriffe:</strong>
              <ul>
                  <li>Beispiel: § 11 Abs. 3 S. 1 Hs. 1 Vertrauensdienstegesetz: „Innovative Identifizierungs­methoden“</li>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „Zur praktischen Erprobung neuer Verkehrsarten oder Verkehrsmittel“</li>
              </ul>
              <strong>Implizite Konkretisierung durch spezifizierten Erprobungszweck:</strong>
              <ul>
                  <li>Beispiel: § 10b Abs. 1 Landesmediengesetz NRW: „Zwecke der Einführung und Weiterentwicklung digitaler terrestrischer Übertragungstechniken“, „Vorbereitung von Entscheidungen über die künftige Nutzung digitaler terrestrischer Übertragungstechniken“.</li>
                  <li>Beispiel: § 38 Abs. 1 Landesplanungsgesetz NRW: „Zum Zwecke der Verfahrensbeschleunigung, bei Vorhaben der Energiewende, zur Bewältigung der Auswirkungen des Klima- und des Strukturwandels oder im Zusammenhang mit den Anforderungen der Digitalisierung oder der Klimaanpassung können ein vereinfachtes Anzeigeverfahren gemäß § 19 Abs. 6, vereinfachte Zielabweichungsverfahren gemäß § 16, § 30 Abs. 2 und § 30 Abs. 3 und ein vereinfachtes Anpassungsverfahren gemäß § 34 erprobt werden.“</li>
              </ul>
              <strong>Konkretisierung durch Definition:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Innovationen im Sinne dieser Experimentierklausel sind gegenüber den am Markt bisher verbreiteten Angeboten neue oder verbesserte Angebote im Bereich der Digitalisierung.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für den Erprobungsgegenstand eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 5: Erprobungsgegenstand (notwendig)',
      },
    },
    materielleBegrenzung: {
      linkName: 'Materielle Begrenzung',
      title: 'Absatz 2 – Materielle Begrenzung (notwendig)',
      text1: `<p>Das Element der Begrenzung verfolgt den Zweck, die Vorteile der Erprobung den widerstreitenden Interessen und Belangen bereits auf Ebene des Tatbestandes gegenüberzustellen. Die materielle Begrenzung filtert einerseits die Innovationen raus, deren Erprobung aufgrund erheblicher Risiken nicht vertretbar ist, und die somit nicht erprobungsfähig sind (<strong>„Ob“</strong>). Zum anderen gibt das Element auch den Umfang vor, innerhalb dessen die Erprobung vertretbar ist (<strong>„Wie“</strong>). Dieser Umfang wird dann durch die weiteren Elemente von Absatz 2 auf den folgenden Seiten konkretisiert.</p>
              <ul>
                <li><strong>Allgemeine Begrenzungen</strong> dienen dazu, widerstreitende Interessen und Belange in Ausgleich zu bringen. Sind diese Interessen zahlreich oder unbestimmt, bietet sich eine <strong>offene negative Formulierung</strong> mit unbestimmten Rechtsbegriffen an, die je nach gewünschter Ermessenslenkung konkretisiert werden können. Sind die Interessen hinreichend bestimmbar, bietet sich eine <strong>konkrete Formulierung</strong> (negativ oder positiv) an.</li>
              </ul>
              `,
      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <strong>Offene negative Formulierung:</strong>
              <ul>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „[…] genehmigen, soweit öffentliche Verkehrsinteressen nicht entgegenstehen.“</li>
                  <li>Beispiel: § 17 Verordnung über die Arbeitszeit des Polizeivollzugsdienstes der Landespolizei Sachsen-Anhalt: „Auf Antrag […] kann das Ministerium […] Ausnahmen zulassen, wenn dienstliche Interessen nicht beeinträchtigt werden und der Gesundheitsschutz gewahrt bleibt.“</li>
              </ul>
              <strong>Konkrete positive Formulierung:</strong>
              <ul>
                  <li>Beispiel: § 113a S. 1 Niedersächsisches Schulgesetz: „[…] zulassen, soweit erwartet werden kann, dass dadurch die Wirtschaftlichkeit und Leistungsfähigkeit in der Verwaltung der Schulen verbessert wird.“</li>
              </ul>
              `,
      },
      text2: `<strong>Zusätzliche risikobezogene Anforderungen</strong> sind oftmals bei risikogeneigten Innovationen nötig. Hierzu gehören, erstens, <strong>gegenstandsbezogene Anforderungen</strong>, bei denen sich die Genehmigungsbehörde mit Untersuchungen, Bewertungen und Bescheinigungen vergewissert, dass die Innovation von einer anerkannten Stelle als gemeinwohlverträglich angesehen wird. Zweitens sind <strong>personenbezogene Anforderungen</strong> nötig, wenn es bei der Erprobung auf spezifische Kenntnisse und Erfahrungen von Personen ankommt. Drittens können <strong>haftungsbezogene Anforderungen</strong> den Abschluss einer Haftpflichtversicherung vorsehen oder – in engen verfassungsrechtlichen Grenzen – mit Haftungsfreistellungserklärungen das staatliche Haftungsrisiko für die unter Umständen noch wenig überschaubaren Schadensrisiken reduzieren.`,
      text2drawer: {
        title: `Haftungsfreistellungserklärung`,
        text: `<p>Die Nutzung einer Haftungsfreistellungserklärung sollte vorsichtig gewählt werden, weil es bei Innovatorinnen und Innovatoren Hürden für die Nutzung von Experimentierklauseln aufbauen kann. Das staatliche Haftungsrisiko kann bereits durch Anforderungen an die Erprobungsgenehmigung eingegrenzt werden.</p>`,
      },
      hiddenSection2: {
        title: 'Beispiele und Formulierungsmöglichkeit',
        text: `
              <strong>Gegenstandsbezogene Anforderungen:</strong>
              <ul>
                  <li>Beispiel: § 11 Abs. 3 S. 1 Vertrauensdienstegesetz: „sofern eine Konformitätsbewertungsstelle die gleichwertige Sicherheit der Identifizierungsmethode […] bestätigt hat.“</li>
              </ul>
              <strong>Personenbezogene Anforderungen:</strong>
              <ul>
                  <li>Beispiel: § 11 Abs. 1 S. 1 LKWÜberlStVAusnV: „Fahrzeuge und Fahrzeugkombinationen mit Überlänge nach § 3 Satz 1 Nummer 2 bis 5 dürfen am Straßenverkehr nur teilnehmen, wenn deren Fahrer seit mindestens fünf Jahren ununterbrochen im Besitz der Fahrerlaubnis der Klasse CE sind und über mindestens fünf Jahre Berufserfahrung im gewerblichen Straßengüter- oder Werkverkehr verfügen.“</li>
              </ul>
              <strong>Haftungsbezogene Anforderungen:</strong>
              <ul>
                  <li>Beispiel: § 43 Abs. 2 Luftverkehrsgesetz: „Der Halter eines Luftfahrzeugs ist verpflichtet, zur Deckung seiner Haftung auf Schadensersatz nach diesem Unterabschnitt eine Haftpflichtversicherung in einer durch Rechtsverordnung zu bestimmenden Höhe zu unterhalten.“</li>
                  <li>Mögliche Formulierung: „Eine Genehmigung darf/soll nur erteilt werden, wenn der Antragsteller bzw. die Antragstellerin das Land, in dessen Gebiet die Erprobung erfolgt, von allen/[näher bestimmten] Ansprüchen Dritter wegen Schäden freistellt, die durch ihn bzw. sie oder seine bzw. ihre Hilfskräfte in Ausübung der Erprobungsgenehmigung verursacht werden.“</li>
              </ul>
              
      `,
      },
      text3: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die materielle Begrenzung eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 6: Materielle Begrenzung (notwendig)',
      },
    },
    verfahrensvorgaben: {
      linkName: 'Verfahrensvorgaben',
      title: 'Absatz 2 – Verfahrensvorgaben (notwendig)',
      text1: `Wenn im Rahmen der Nutzung der Experimentierklausel ein Antragserfordernis besteht, bedarf es Vorgaben über Form, Inhalt und Verfahren der Beantragung, das die Innovatorin bzw. der Innovator bei der zuständigen Behörde zu durchlaufen hat. Dies muss nicht zwingend neu in der Experimentierklausel geregelt werden, wenn allgemeine Regeln im Fachrecht bestehen, die ausreichend sind und übertragen werden können.`,
      text1drawer: {
        title: `Unterschiede zwischen Erprobungsgenehmigung und allgemeinem Fachrecht`,
        text: `<p>In den meisten Fällen dürften sich Sachverhalte im Rahmen der Erprobungsgenehmigung von den allgemeinen Sachverhalten des Fachrechts unterscheiden. Daher bietet es sich an, spezifische Vorgaben über Form, Inhalt und Verfahren der Beantragung zu machen, die die temporäre Natur der Erprobung und auch die unterschiedliche Risikogeneigtheit der Innovationen berücksichtigen sollten.</p>`,
      },
      text1List: `<ul>
                    <li><strong>Verfahren:</strong> Einzelne Aspekte des Verfahrens (z.B. angemessene Frist zur behördlichen Entscheidung mit Verlängerungsmöglichkeit, Beteiligung Dritter, Beratung und Auskunft durch die Behörde) können geregelt werden, wobei zumeist auf allgemeine Vorschriften zurückgegriffen werden kann, wenn diese zum Erprobungscharakter passen.</li>
                    <li><strong>Einzureichende Unterlagen:</strong> Antragsunterlagen müssen der Behörde eine qualifizierte Entscheidung über die Erprobung („Ob“ und „Wie“) erlauben. Abhängig vom Sachverhalt sollte für eine Vereinfachung der Beantragung eine <strong>Standardisierung der Unterlagen</strong> angestrebt werden. Bei unterschiedlich risikogeneigten Innovationen sollten die Antragsunterlagen gleichzeitig nicht einheitlich vorgegeben werden, sondern <strong>risikobasiert Raum für Flexibilität</strong> belassen.</li>
                  </ul>`,
      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <ul>
                  <li>Beispiel: § 181 Abs. 2 Niedersächsisches Kommunalverfassungsgesetz: „In dem Antrag hat die Kommune <strong>darzulegen</strong>, zu welchem Zweck die Erprobung im Einzelnen dienen soll, von welchen Vorschriften Ausnahmen beantragt werden und welche Wirkungen erwartet werden.“</li>
              </ul>
              `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>`,
      angabe:
        'Bestehen im Fachrecht allgemeine Regeln zum Verfahren, die ausreichend sind und übertragen werden können?',
      verfahrensvorgabenBestehenRegelnJa: `Da im Fachrecht bereits allgemeine Verfahrensvorgaben vorliegen, die ausreichend sind und übertragen werden können, müssen in die Experimentierklausel keine Verfahrensvorgaben aufgenommen werden.`,
      verfahrensvorgabenBestehenRegelnNein: `Da keine allgemeinen Regeln im Fachrecht vorliegen, ist es notwendig, Verfahrensvorgaben in die Experimentierklausel aufzunehmen. Hier können Sie einen Formulierungsvorschlag für die Verfahrensvorgaben eintragen:`,
      formItem: {
        label: 'Absatz 2 – Element 7: Verfahrensvorgaben (notwendig)',
      },
    },
    umfangErprobung: {
      linkName: 'Umfang der Erprobung',
      title: 'Absatz 2 – Umfang der Erprobung (optional)',
      text1: `<p>Räumliche und sachliche Begrenzungen sind hilfreich, um die Erprobung einer Innovation für die Stadien ihrer Erprobung jeweils passgenau zu genehmigen. Das Element ist optional, weil es nicht bei allen Erprobungen erforderlich ist und zudem Fachbehörden bei ihrer Entscheidung über die Zulassung bzw. Genehmigung auch über den zulässigen Umfang einer Erprobung entscheiden. Die Formulierung sollte risikobasiert erfolgen.</p>
              <ul>
                <li><strong>Offene Formulierung:</strong> Das Gesetz gibt einen weiten Rahmen für die Ermessens­ausübung vor. Die Konkretisierung läge überwiegend bei der Fachbehörde.</li>
                <li><strong>Konkretisierte Formulierung:</strong> Dabei ist es oft besonders sinnvoll, die räumliche und sachliche Begrenzung vom Stadium der Erprobung abhängig zu machen. Risiken der Erprobung sind in einem frühen Erprobungsstadium häufig nicht gänzlich bekannt. Es bieten sich zunächst weitergehende Begrenzungen an, die schrittweise reduziert werden können, wenn sich die Erprobung in der Praxis bewährt.</li>
              </ul>
              `,
      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <strong>Offene Formulierung:</strong>
              <ul>
                  <li>Beispiel: Art. 56 Abs. 1 Bayerisches Digitalgesetz: „[…] kann die Staatsregierung durch Rechtsverordnung sachlich und räumlich begrenzte Abweichungen von folgenden Vorschriften vorsehen: […]“</li>
              </ul>
              <strong>Schrittweise Anpassung:</strong>
              <ul>
                  <li>Beispiel: Erprobung sog. „Gigaliner“: Die probeweise Zulassung von Gigalinern für den Straßenverkehr erfolgte auf einem bestimmten vorab festgelegten Streckennetz, das teils in der Verordnung selbst, teils in der Anlage zur Verordnung aufgeführt war. Dieses Streckennetz wurde u.a. durch mehrere Verordnungsänderungen während des Erprobungszeitraums von 2011 bis 2016 erweitert.</li>
              </ul>
              `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für den Umfang der Erprobung eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 8: Umfang der Erprobung (optional)',
      },
    },
    begleitendePflichten: {
      linkName: 'Begleitende Pflichten',
      title: 'Absatz 2 – Begleitende Pflichten (notwendig)',
      text1: `Begleitende Pflichten können sich an die Innovatorin bzw. den Innovator oder an die Fachbehörde richten, um insbesondere in risikogeneigten Bereichen <strong>staatliche Schutzpflichten</strong> gegenüber Dritten zu verwirklichen sowie die <strong>Evaluation</strong> der gesetzlichen Regelung vorzubereiten. In Betracht kommen insbesondere <strong>Überwachungs- oder Berichtspflichten</strong> sowie die Pflicht zur Teilnahme an <strong>wissenschaftlichen Untersuchungen</strong>.`,
      text1drawer: {
        title: `Abwägung von Grundrechten und Schutzpflichten bei Innovationsprojekten`,
        text: `<p>Da begleitende Pflichten in Grundrechte der Wirtschaftsakteure eingreifen können, müssen sie <strong>gerechtfertigt</strong> und <strong>verhältnismäßig</strong> sein und sich in ihrem Umfang an den Schutzpflichten orientieren und am legitimen Evaluationsinteresse ausrichten. Je risikogeneigter eine Innovation ist oder je früher das Erprobungsstadium ist, desto umfangreicher und engmaschiger können Überwachungs- und Berichtspflichten ausgestaltet sein.</p>`,
      },
      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <strong>Evaluation:</strong>
              <ul>
                  <li>Beispiel: § 12 Abs. 1 LKWÜberlStVAusnV a.F. (01.01.2012): „[…] dürfen am Straßenverkehr nur teilnehmen, wenn mit diesen an einer wissenschaftlichen Untersuchung durch die Bundesanstalt für Straßenwesen teilgenommen wird.“</li>
              </ul>
              <strong>Staatliche Überwachung:</strong>
              <ul>
                  <li>Beispiel: § 11 Abs. 3 S. 3-5 Vertrauensdienstegesetz: „Die Bundesnetzagentur und das Bundesamt für Sicherheit in der Informationstechnik überwachen die Eignung der vorläufig anerkannten Identifizierungsmethoden über den gesamten Zeitraum der vorläufigen Anerkennung. Werden durch die Überwachung sicherheitsrelevante […] erkannt, so kann die Aufsichtsstelle […] die Behebung dieser Risiken durch ergänzende Maßnahmen auferlegen, […].“</li>
              </ul>
              <strong>Offene Formulierung Intervalle:</strong>
              <ul>
                  <li>Beispiel: § 30 Abs. 2 Landesmediengesetz NRW: „Die LfM soll von den Veranstaltern und Anbietern in angemessenen zeitlichen Abständen einen Erfahrungsbericht über die laufenden Modell- und Betriebsversuche […] verlangen.“</li>
              </ul>
              <strong>Konkrete Formulierung Intervalle:</strong>
              <ul>
                  <li>Beispiel: Punkt 4.6 Abs. 5 Koordinierungsrahmen der Gemeinschaftsaufgabe zur Verbesserung der regionalen Wirtschaftsstruktur: „Die Länder berichten dem Unterausschuss jährlich schriftlich über die Verwendung der Mittel.“</li>
              </ul>
              `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die begleitenden Pflichten eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 9: Begleitende Pflichten (notwendig)',
      },
    },
    befristungErprobung: {
      linkName: 'Befristung der Erprobung',
      title: 'Absatz 2 – Befristung der Erprobung (notwendig)',
      text1: `<p>Die <strong>Befristung der Erprobung</strong> als zentrales Element ist so zu wählen, dass der vorübergehende Erprobungszweck der Regelung zum Tragen kommt, aber gleichzeitig ausreichend Zeit für die Erprobung und den damit verbundenen regulatorischen Lernprozess zur Verfügung steht. Die <strong>Dauer der Befristung</strong> kann unterschiedlich konkret ausgestaltet werden:</p>`,

      text1List: [
        {
          text: 'Keine oder offene Formulierung',
          drawer: {
            title: `Keine oder offene Formulierung`,
            text: `<p>In diesem Fall wird der Fachbehörde die Entscheidung über die zeitliche Befristung im Rahmen ihres zweckgebundenen Ermessens überlassen. Das bietet zwar Flexibilität, es kann aber zu einen „Flickenteppich“ in der Genehmigungspraxis führen und die betriebswirtschaftliche Planbarkeit einschränken.</p>`,
          },
        },
        {
          text: 'Konkrete Dauer',
          drawer: {
            title: `Konkrete Dauer`,
            text: `<p>Dadurch wird der zeitliche Umfang der Erprobung sowohl minimal als auch maximal determiniert. Bei risikogeneigten Innovationen wären dabei zunächst kürzere Erprobungshorizonte (mit Verlängerungsmöglichkeit, s.u.) vorzusehen. Es ist auch möglich, bspw. mit „in der Regel“ einen flexiblen Rahmen zu setzen.</p>`,
          },
        },
      ],

      hiddenSection1: {
        title: 'Beispiel und Formulierungsmöglichkeit',
        text: `
              <ul>
                  <li>Beispiel: § 2 Abs. 7 Personenbeförderungsgesetz: „Zur praktischen Erprobung neuer Verkehrsarten oder Verkehrsmittel kann die Genehmigungsbehörde auf Antrag im Einzelfall Abweichungen […] für die Dauer von höchstens fünf Jahren genehmigen, […].“</li>
              </ul>
              <strong>Flexibler Rahmen:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Die Erprobung ist für eine angemessene Zeit zu befristen. In der Regel ist eine Zeit von mindestens zwei, höchstens aber fünf Jahren angemessen.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Befristung der Erprobung eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 10: Befristung der Erprobung (notwendig)',
      },
    },
    verlaengerungsmoeglichkeit: {
      linkName: 'Verlängerungsmöglichkeit',
      title: 'Absatz 2 – Verlängerungsmöglichkeit (optional)',
      text1: `<p>Da mit der Erprobung von Regelungen und Innovationen unvorhergesehene Entwicklungen und Herausforderungen einhergehen, ist es im Regelfall nicht sinnvoll, die Dauer der Experimentierphase gesetzlich festzuschreiben, ohne dass im Einzelfall davon abgewichen werden kann. Die Verlängerungsmöglichkeiten sollten jedoch nicht überreizt werden. Der Regelbetrieb sollte – eine erfolgreiche Erprobung vorausgesetzt – das Ziel bleiben.</p>
              <p>Um den Verwaltungsaufwand gering zu halten und die Verlängerungsentscheidung zu beschleunigen, bieten sich folgende Ausgestaltungen an:</p>
              <ul>
                <li><strong>Soll-Vorschrift oder Regelung von Versagungsgründen:</strong> Die Behörde wird grundsätzlich verpflichtet, den Erprobungszeitraum einmal oder mehrfach zu verlängern und kann eine abweichende Entscheidung nur in atypischen Fällen oder bei Vorliegen eines gesetzlich bestimmten Versagungsgrundes treffen.</li>
                <li><strong>Genehmigungsfiktion bei Fristablauf:</strong> Die Verlängerung gilt einmal oder mehrfach nach Ablauf einer für die Entscheidung festgelegten Frist als erteilt, wenn die Behörde innerhalb dieser Frist keine Entscheidung getroffen hat.</li>
                <li><strong>Genehmigungsfreistellung:</strong> Die einmalige oder mehrfache Verlängerung bedarf keiner Genehmigung. Ihre rechtmäßige Fortführung liegt in der Verantwortung der Innovatorin bzw. des Innovators, die Behörde kann jederzeit einschreiten.</li>
              </ul>
              `,

      hiddenSection1: {
        title: 'Beispiel und Formulierungsmöglichkeit',
        text: `
              <strong>Soll-Vorschrift oder Regelung von Versagungsgründen:</strong>
              <ul>
                  <li>Beispiel: § 16 Abs. 2 S. 1, 2 Autonome-Fahrzeuge-Genehmigungs-und-Betriebs-Verordnung: „Die Erprobungsgenehmigung ist zu befristen und darf einen Geltungszeitraum von vier Jahren im Regelfall nicht überschreiten. Sie ist jeweils für weitere zwei Jahre zu verlängern, wenn die Voraussetzungen der Genehmigungserteilung weiter fortbestehen und der bisherige Verlauf der Erprobung einer Verlängerung nicht entgegensteht. […]“</li>
              </ul>
              <strong>Genehmigungsfiktion bei Fristablauf:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Die Erprobungsgenehmigung kann auf Antrag […]mal um [je …] Monate/ Jahre verlängert werden. Hat die Behörde nicht innerhalb einer Frist von drei Monaten über den Verlängerungsantrag entschieden, gilt die Verlängerungsgenehmigung als erteilt.“</li>
              </ul>
              <strong>Genehmigungsfreistellung:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Die […]malige Verlängerung der Erprobungsgenehmigung um [je …] ist genehmigungsfrei gestellt, wenn die Voraussetzungen für eine Erprobung vorliegen.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Verlängerungsmöglichkeit eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 11: Verlängerungsmöglichkeit (optional)',
      },
    },
    verhaeltnisAufschiebendeWirkung: {
      linkName: 'Verhältnis zur aufschiebenden Wirkung von Widerspruch und Klage',
      title: 'Absatz 2 – Verhältnis zur aufschiebenden Wirkung von Widerspruch und Klage (optional)',
      text1: `<p>Widerspruch und Klage haben nach § 80 Abs. 1 S. 1 und 2 VwGO aufschiebende Wirkung. Erprobungs­genehmigungen können dann nicht genutzt werden, was vor allem bei befristeten Regelungen ohne Verlängerungsmöglichkeit problematisch ist. Folgende Lösungsmöglichkeiten bieten sich an:</p>
              <ul>
                <li><strong>Entfall der aufschiebenden Wirkung eines Rechtsmittels gegen die Erprobungsgenehmigung</strong> (s. § 80 Abs. 2 S. 1 Nr. 3 VwGO). Bei dieser Variante kann die Erprobung trotz Erhebung eines Rechtsmittels durch Dritte weiter erfolgen.</li>
                <li><strong>Hemmung des Fristlaufs:</strong> Für die Dauer der aufschiebenden Wirkung von Drittanspruch oder Drittanfechtungsklage würde die Frist nicht weiterlaufen. Bei dieser Variante kann keine Erprobung bei Erhebung eines Rechtsmittels durch Dritte erfolgen.</li>
                <li><strong>Besondere Verlängerungsmöglichkeit:</strong> Im Fall der aufschiebenden Wirkung eines Rechtsmittels kann eine behördliche Verlängerung der Frist erfolgen. Bei dieser Variante kann keine Erprobung bei Erhebung eines Rechtsmittels durch Dritte erfolgen.</li>
              </ul>
              `,

      hiddenSection1: {
        title: 'Beispiel und Formulierungsmöglichkeit',
        text: `
              <strong>Entfall der aufschiebenden Wirkung eines Rechtsmittels gegen die Erprobungsgenehmigung:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Die aufschiebende Wirkung von Widerspruch und Anfechtungsklage eines Dritten gegen die Genehmigung der Erprobung entfällt nach § 80 Abs. 2 Satz 1 Nr. 3 VwGO.“</li>
              </ul>
              <strong>Hemmung des Fristlaufs:</strong>
              <ul>
                  <li>Beispiel: § 16 Abs. 2 S. 2 Autonome-Fahrzeuge-Genehmigungs-und-Betriebs-Verordnung: „[…] Sollten Dritte gegen die Erprobungsgenehmigung oder deren Verlängerung Rechtsbehelfe einlegen, verlängert sich die Geltungsdauer der Erprobungsgenehmigung um die Anzahl der Tage, an denen der Rechtsbehelf aufschiebende Wirkung entfaltet.“</li>
                  <li>Mögliche Formulierung: „Der Fristablauf wird gehemmt durch die Erhebung der Klage oder des Widerspruchs gegen die Genehmigung.“.</li>
              </ul>
              <strong>Besondere Verlängerungsmöglichkeit:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Im Falle der Erhebung der Klage oder des Widerspruchs gegen die Genehmigung soll die Dauer der Befristung entsprechend verlängert werden.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für das Verhältnis zur aufschiebenden Wirkung von Widerspruch und Klage eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 12: Verhältnis zur aufschiebenden Wirkung von Widerspruch und Klage (optional)',
      },
    },
    weitereNebenbestimmungen: {
      linkName: 'Weitere Nebenbestimmungen',
      title: 'Absatz 2 – Weitere Nebenbestimmungen (optional)',
      text1: `Als optionales Element kann in einer Experimentierklausel auch geregelt werden, ob und unter welchen Voraussetzungen die <strong>behördliche Entscheidung zur Erprobung mit Nebenbestimmungen versehen</strong> werden kann`,
      text1drawer: {
        title: `Nebenbestimmungen`,
        text: `<p>Die Befristung der Erprobung ist ein Unterfall einer Nebenbestimmung.</p>`,
      },
      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <ul>
                  <li>Beispiel: § 13 Abs. 2 S. 2 Geldwäschegesetz: „Bei Verfahren nach Nummer 3 können die Aufsichtsbehörden nach § 50 dazu ermächtigt werden, die Nutzung der Verfahren befristet, unter Vorbehalt eines Widerrufs und unter Auflagen zuzulassen.“</li>
              </ul>
              `,
      },
      text2: `Eine sondergesetzliche Regelung, wonach die Genehmigung mit Nebenbestimmungen versehen werden kann, ist <strong>nur klarstellender Natur</strong>. Denn Nebenbestimmungen sind im allgemeinen Verwaltungsrecht in § 36 VwVfG bereits geregelt. Aus Gründen der Normenklarheit sollte eine sondergesetzliche Regelung <strong>zurückhaltend eingesetzt</strong> werden. Sie sollte v.a. <strong>konkretisieren</strong>, welche Nebenbestimmungen unter welchen Voraussetzungen möglich sind. Durch Nebenbestimmungen können auf den Einzelfall zugeschnittene Lösungen geschaffen werden, die der Behörde weitere Handlungsoptionen eröffnen. Um spätere Anpassungen der Erprobungsentscheidung zu ermöglichen, kann die Behörde sich die Aufnahme, Änderung oder Ergänzung von Auflagen auch vorbehalten (§ 36 Abs. 2 Nr. 5 VwVfG).`,
      text3: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für weitere Nebenbestimmungen eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 13: Weitere Nebenbestimmungen (optional)',
      },
    },
    aenderungReallaborgestaltung: {
      linkName: 'Möglichkeit der Änderung der Reallaborgestaltung während der Erprobung',
      title: 'Absatz 2 – Möglichkeit der Änderung der Reallaborgestaltung während der Erprobung (optional)',
      text1: `Angesichts der Komplexität vieler erprobungsbedürftiger Innovationen zeigt erst die Durchführung der Erprobung, ob sich ein Anpassungsbedarf gegenüber der ursprünglichen Ausgestaltung des Reallabors ergibt. Eine leistungsstarke Experimentierklausel ermöglicht der zuständigen Behörde und den Innovatorinnen und Innovatoren eine einfache Anpassung ihrer Zulassungsentscheidung an Zwischenergebnisse der Erprobung.`,
      text1drawer: {
        title: `Anpassung des Rechtsrahmens für innovative Erprobungen`,
        text: `<p>Stellt sich heraus, dass eine Innovation Erprobungsbedingungen voraussetzt, welche die zuständige Behörde auf Grundlage der bestehenden Experimentierklausel nicht schaffen kann, bedarf es einer Anpassung des Rechtsrahmens. Damit der Gesetzgeber zeitnah und adäquat reagieren kann, bietet sich eine Flexibilisierung der Rechtsgestaltung durch Verordnungsermächtigungen in Experimentierklauseln an. Nähere Informationen dazu finden Sie in den Ausführungen zu Absatz 4 der Experimentierklausel.</p>`,
      },
      text2: `
              <p>Wenn die Innovatorin bzw. der Innovator die Erprobung auf eine Weise umgestalten will, die nicht mehr von der erteilten Genehmigung erfasst ist, bedarf es eines erneuten Genehmigungsverfahrens. Für nicht wesentliche Änderungen kann die Experimentierklausel ein vereinfachtes Verfahren vorsehen.</p>
              <p>Folgende Aspekte kann die Experimentierklausel regeln:</p>
              <ul>
                <li>Anzeige der Änderung durch die Innovatorin bzw. den Innovator innerhalb einer bestimmten Frist,</li>
                <li>Behördliche Prüfung der Genehmigungspflicht innerhalb einer bestimmten Frist: Genehmigungsverlangen bei wesentlicher Änderung (neuer Genehmigungsantrag erforderlich), Genehmigungsfreistellung bei unwesentlicher Änderung (kein neuer Genehmigungsantrag erforderlich),</li>
                <li>Freistellungsfiktion gemäß § 42a VwVfG bei ereignislosem Fristablauf, sodass die/der Anzeigende die Erprobung mit der Änderung fortführen darf.</li>
              </ul>
              `,
      hiddenSection1: {
        title: 'Formulierungsmöglichkeit',
        text: `
              <ul>
                  <li>Mögliche Formulierung: „Die wesentliche Änderung einer zugelassenen Erprobung bedarf der Genehmigung. Die Änderung einer zugelassenen Erprobung ist, sofern eine Genehmigung nicht beantragt wird, der zuständigen Behörde mindestens einen Monat, bevor mit der Änderung begonnen wird, schriftlich oder elektronisch anzuzeigen. Die zuständige Behörde bestätigt unverzüglich den Eingang der Anzeige. Sie hat unverzüglich, spätestens jedoch innerhalb eines Monats nach Eingang der Anzeige, zu prüfen, ob die Änderung einer Genehmigung bedarf. Der Erprobungsberechtigte darf die Änderung vornehmen, sobald die zuständige Behörde ihm mitteilt, dass die Änderung keiner Genehmigung bedarf, oder sich die zuständige Behörde innerhalb der in Satz 4 bestimmten Frist nicht geäußert hat.“</li>
              </ul>
              `,
      },

      text3: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Möglichkeit der Änderung der Reallaborgestaltung während der Erprobung eintragen:</p>`,
      formItem: {
        label:
          'Absatz 2 – Element 14: Möglichkeit der Änderung der Reallaborgestaltung während der Erprobung (optional)',
      },
    },
    moeglichkeitAufhebung: {
      linkName: 'Möglichkeit der Aufhebung',
      title: 'Absatz 2 – Möglichkeit der Aufhebung (optional)',
      text1: `Die Experimentierklausel kann eine Sondervorschrift über die Aufhebung der Zulassung oder Genehmigung enthalten. Da die Aufhebung (§ 43 Abs. 2 Var. 1 VwVfG) durch Rücknahme (§ 48 VwVfG) und Widerruf (§ 49 VwVfG) allgemein geregelt ist, besteht jedoch meist nur ein geringes zusätzliches Regelungsbedürfnis. Möglich wäre bspw. einen <strong>Widerrufsvorbehalt</strong> nach § 49 Abs. 2 Nr. 1 VwVfG zu regeln, wenn Gründe vorliegen, die dem <strong>Zweck der Experimentierklausel entsprechen</strong> (z.B. Erprobung wird nicht begonnen, Umfang deutlich unterschritten).`,
      hiddenSection1: {
        title: 'Formulierungsmöglichkeit',
        text: `
              <ul>
                  <li>Mögliche Formulierung: „Die Genehmigung ist widerruflich. Neben den in §§ 48, 49 VwVfG genannten Gründen kann die Genehmigung ohne Entschädigung ganz oder teilweise widerrufen werden, wenn die Erprobung nicht innerhalb von zwei Jahren ab Genehmigungserteilung durchgeführt wird oder der erlaubte Umfang der Erprobung erheblich unterschritten wird.“</li>
              </ul>
              `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Möglichkeit der Aufhebung eintragen:</p>`,
      formItem: {
        label: 'Absatz 2 – Element 15: Möglichkeit der Aufhebung (optional)',
      },
    },
    absatzZweiZusammenfassung: {
      linkName: 'Formulierung von Absatz 1 und 2',
      title: 'Formulierung von Absatz 1 und 2',
      text1: `Auf dieser Seite haben Sie nun die Möglichkeit, aus den bisher für die einzelnen Elemente bruchstückhaft eingetragenen Formulierungen für Absatz 1 und 2 zusammenhängende Absätze zu formulieren, wie es für Ihren Regelungsbereich zweckdienlich ist. Im Folgenden werden Ihnen Ihre Formulierung für Absatz 1 sowie die Eingaben zu den einzelnen Elementen von Absatz 2 übersichtsartig angezeigt.`,
      hinweis1: {
        title: `Änderungen übertragen sich automatisch`,
        content:
          'Wenn Sie hier Änderungen vornehmen, werden diese mit den Eingaben auf den ursprünglichen Seiten synchronisiert. <a href="{{linkHinweis1}}">Gehe zur ursprünglichen Seite</a>',
      },
      textLabelAbsatz2: `<h2 class="ant-typography">Absatz 2 – Elemente 2 bis 15:</h2>`,
      hinweis2: {
        title: `Änderungen übertragen sich automatisch`,
        content:
          'Wenn Sie hier Änderungen vornehmen, werden diese mit den Eingaben auf den ursprünglichen Seiten synchronisiert.',
      },
      overviewList: [
        { key: 'zustaendigkeit', label: `2: Zuständigkeit (notwendig)`, isMandatory: false },
        { key: 'ermaechtigungBehoerde', label: `3: Ermächtigung der Behörde (notwendig)`, isMandatory: false },
        {
          key: 'entscheidungsinhalt',
          label: `4: Entscheidungsinhalt (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'erprobungsgegenstand',
          label: `5: Erprobungsgegenstand (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'materielleBegrenzung',
          label: `6: Materielle Begrenzung (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'verfahrensvorgaben',
          label: `7: Verfahrensvorgaben (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'umfangErprobung',
          label: `8: Umfang der Erprobung (optional)`,
          isMandatory: false,
        },
        {
          key: 'begleitendePflichten',
          label: `9: Begleitende Pflichten (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'befristungErprobung',
          label: `10: Befristung der Erprobung (notwendig)`,
          isMandatory: false,
        },
        {
          key: 'verlaengerungsmoeglichkeit',
          label: `11: Verlängerungsmöglichkeit (optional)`,
          isMandatory: false,
        },
        {
          key: 'verhaeltnisAufschiebendeWirkung',
          label: `12: Verhältnis zur aufschiebenden Wirkung (optional)`,
          isMandatory: false,
        },
        {
          key: 'weitereNebenbestimmungen',
          label: `13: Weitere Nebenbestimmungen (optional)`,
          isMandatory: false,
        },
        {
          key: 'aenderungReallaborgestaltung',
          label: `14: Möglichkeit der Änderung während der Erprobung (optional)`,
          isMandatory: false,
        },
        {
          key: 'moeglichkeitAufhebung',
          label: `15: Möglichkeit der Aufhebung (optional)`,
          isMandatory: false,
        },
      ],
      overviewListItemLink: `Gehe zur ursprünglichen Seite`,
      overviewListLegend: `Absatz 2 – Elemente 2 bis 15:`,
      text2: `<p>Hier können Sie die einzelnen Elemente nun zu zusammenhängenden Sätzen verbinden.</p>`,
      formItem: {
        label: 'Absatz 2:',
        btnApplyText: 'Text von Element 2-15 übernehmen',
      },
    },
  },
  absatz3: {
    linkName: 'Absatz 3',
    evaluationUndTransfer: {
      linkName: 'Evaluation und Transfer',
      title: 'Absatz 3 – Evaluation und Transfer (notwendig)',
      text1: `<p>Die Evaluation ist ein zentrales Element, um die Erprobung von Innovationen aktiv mit regulatorischem Lernen zu verbinden. Evaluationen geben Aufschluss darüber, ob und inwieweit die Ziele der Regulatorik erreicht werden, wie die Experimentierklausel ggf.  nachzubessern ist und/oder ob und in welchem Umfang die Innovation regulatorisch in den Regelbetrieb überführt werden kann. Dafür können drei Ebenen evaluiert werden:</p>`,

      text1List: {
        item1: `Die <strong>Evaluation der einzelnen Reallabore</strong>, die auf Grundlage der Experimentier­klausel durchgeführt werden, dient dazu, die Erfahrungen aus der Praxis zusammenzufassen.`,
        item2: `Die <strong>Evaluation der Experimentierklausel</strong> dient dazu festzustellen, ob die Experimentierklausel mit ihren einzelnen Elementen richtig ausgestaltet ist.`,
        item3: `Die <strong>Evaluation des Gesetzes</strong>, in dem die Experimentierklausel verankert ist, dient dazu festzustellen, ob der gesetzliche Rahmen angepasst werden muss.`,
        item3Drawer: {
          title: `Evaluation des Gesetzes`,
          text: `<p>Innerhalb der Bundesregierung ist bei der Evaluation von Gesetzen auch die <a href="{{link1}}" target="_blank">Konzeption zur Evaluierung neuer Regelungsvorhaben von 2013</a> zu berücksichtigen, die der Staatssekretärsausschuss Bürokratieabbau und Bessere Rechtsetzung mit <a href="{{link2}}" target="_blank">Beschluss vom 26. November 2019</a> weiterentwickelt hat.</p>`,
        },
      },
      text2: `<p>Mehrere Bestandteile und Varianten der Regelung sind sinnvoll:</p>
              <ul>
                <li><strong>Begleitende Pflichten</strong> (siehe Absatz 2, Element 8): Darunter fallen beispielsweise die Teilnahme an wissenschaftlichen Untersuchungen sowie Berichtspflichten der Innovatorin bzw. des Innovators an die Verwaltung sowie Berichtspflichten der Verwaltung an den Gesetzgeber.</li>
              </ul>
      `,

      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <strong>Mitwirkungspflicht:</strong>
              <ul>
                  <li>Beispiel: § 12 Abs. 1 LKWÜberlStVAusnV a.F. von 2012: „[…] am Straßenverkehr nur teilnehmen, wenn mit diesen an einer wissenschaftlichen Untersuchung durch die Bundesanstalt für Straßenwesen teilgenommen wird.“</li>
              </ul>
              <strong>Berichtspflicht der Verwaltung:</strong>
              <ul>
                  <li>Beispiel: § 1c Straßenverkehrsgesetz: „Das Bundesministerium für Verkehr und digitale Infrastruktur wird die Anwendung der Regelungen […] auf wissenschaftlicher Grundlage evaluieren. Die Bundesregierung unterrichtet den Deutschen Bundestag über die Ergebnisse der Evaluierung.“</li>
                  <li>Beispiel: § 80 Zollfahndungsdienstgesetz: „Das Bundesministerium […] unterrichtet in Abständen von höchstens sechs Monaten ein Gremium, das aus neun vom Deutschen Bundestag bestimmten Abgeordneten besteht, über die Durchführung der Maßnahmen nach […]; dabei ist in Bezug auf die im Berichtszeitraum durchgeführten Maßnahmen nach diesen Vorschriften insbesondere über deren Anlass, Umfang, Dauer, Ergebnis und Kosten sowie […] zu berichten.“</li>
              </ul>
      `,
      },
      text3: `<ul>
                <li><strong>Ziele, Inhalte und Zeitrahmen:</strong> In der Experimentierklausel selbst oder in der Begründung des Regelungsvorhabens können Ziele und Kriterien für die Zielerreichung definiert werden, die der Evaluierung zugrunde liegen. Es können zudem Methoden und Adressaten der Ergebnisse genannt werden. Die Evaluierungsklausel sollte insbesondere vorsehen, wann die Evaluation beginnt. Die Evaluierung sollte eine angemessene Zeit nach Inkrafttreten der Experimentierklausel im Wege einer Ex-post-Evaluation erfolgen. Wichtig ist, dass bereits ausreichend Erfahrungen im Rahmen der Erprobung gesammelt wurden.</li>
              </ul>
      `,
      hiddenSection2: {
        title: 'Beispiele',
        text: `
              <strong>Ziele und Inhalt:</strong>
              <ul>
                  <li>Beispiel: § 21 Sächsisches E-Government-Gesetz: „(1) Die Staats­regierung legt dem Landtag im Jahr 2021 einen Bericht vor, in dem sie darlegt, 1. welche Auswirkungen dieses Gesetz […] hat, 2. welche Projekte auf der Basis der Experimentier­klausel des § 20 durchgeführt wurden, 3. wie sich Datenschutz und Barrierefreiheit […] entwickelt haben, 4. welche Kosten und Nutzen bei der Umsetzung dieses Gesetzes entstanden sind und 5. ob eine Weiterentwicklung der Vorschriften dieses Gesetzes erforderlich ist. Bei der Evaluierung ist auch die Perspektive der Nutzer der E-Government-Angebote, insbesondere der Bürgerinnen und Bürger sowie Unternehmen, zu berücksichtigen.“</li>
              </ul>
              <strong>Zeitrahmen:</strong>
              <ul>
                  <li>Beispiel: § 1l Satz 1 des Straßenverkehrsgesetzes: „Das Bundesministerium für Verkehr und digitale Infrastruktur wird die Anwendung der Regelungen des Gesetzes vom 12. Juli 2021 (BGBl. I S. 3108) nach Ablauf des Jahres 2023 insbesondere im Hinblick auf die Auswirkungen auf […] die aufgrund von Erprobungsgenehmigungen im Sinne des § 1i Absatz 2 gewonnenen Erkenntnisse auf wissenschaftlicher Grundlage in nicht personenbezogener Form evaluieren und den Deutschen Bundestag über die Ergebnisse der Evaluierung unterrichten. Sofern erforderlich, soll das Bundesministerium für Verkehr und digitale Infrastruktur die Evaluierung zu einem von ihm festzulegenden Zeitpunkt bis zum Jahr 2030 erneut durchführen.“</li>
              </ul>
      `,
      },
      text4: `<ul>
                <li><strong>Zuständigkeiten für den Wissenstransfer:</strong> Optimalerweise wird einerseits geregelt, dass die Genehmigungsinhaberin bzw. der Genehmigungsinhaber eine/n Verantwortliche/n benennt, die bzw. der für die Evaluation des Reallabors und die Übermittlung erforderlicher Informationen an die zuständige staatliche Stelle verantwortlich ist. Es bietet sich darüber hinaus an, die Evaluierung einer Institution oder Behörde zu übertragen, die der für die Genehmigung zuständigen Behörde übergeordnet ist und die von mehreren Behörden Informationen sammeln kann.</li>
              </ul>
      `,
      hiddenSection3: {
        title: 'Beispiel und Formulierungsmöglichkeit',
        text: `
              <strong>Verantwortliche Person beim Reallabor:</strong>
              <ul>
                  <li>Mögliche Formulierung: „Der Genehmigungsinhaber bzw. die Genehmigungsinhaberin hat eine für die Erfüllung der Pflichten nach […] verantwortliche Person zu benennen.“</li>
              </ul>
              <strong>Zuständige Behörde:</strong>
              <ul>
                  <li>Beispiel: § 14 S. 2 Informationsfreiheitsgesetz: „Der Deutsche Bundestag wird das Gesetz ein Jahr vor Außerkrafttreten auf wissenschaftlicher Grundlage evaluieren.“</li>
              </ul>
      `,
      },
      text5: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Regelung zu Evaluation und Transfer eintragen:</p>`,
      formItem: {
        label: 'Absatz 3 – Element 16: Evaluation und Transfer (notwendig)',
      },
    },
    befristungExperimentierklausel: {
      linkName: 'Befristung der Experimentierklausel',
      title: 'Absatz 3 – Befristung der Experimentierklausel (optional)',
      text1: `<p>Wie bei jeder gesetzlichen Regelung, kann auch der Geltungszeitraum der Experimentierklausel selbst als optionales Element durch eine Befristung begrenzt werden („<strong>Sunset-Klausel</strong>“), sofern diese selbst erprobt und auf ihre Effektivität und Praktikabilität hin getestet werden soll. In diesem Fall müsste eine <strong>angemessen lange Geltungsdauer</strong> vorgesehen werden, sodass auf Grundlage der Experimentierklausel eine aussagekräftige Anzahl an Erprobungen zugelassen und durchgeführt werden kann.</p>
    `,
      hiddenSection1: {
        title: 'Formulierungsmöglichkeit',
        text: `
              <ul>
                  <li>Beispiel: § 13 Abs. 1 und 2 LKWÜberlStVAusnV a.F.: „(1) Diese Verordnung tritt am 1. Januar 2012 in Kraft. (2) Sie tritt mit Ablauf des 31. Dezember 2016 außer Kraft.“</li>
              </ul>
      `,
      },
      text2: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Hier können Sie einen Formulierungsvorschlag für die Befristung der Experimentierklausel eintragen:</p>`,
      formItem: {
        label: 'Absatz 3 – Element 17: Befristung der Experimentierklausel (optional)',
      },
    },
    absatzDreiZusammenfassung: {
      linkName: 'Formulierung von Absatz 3',
      title: 'Formulierung von Absatz 3',
      text1: `Auf dieser Seite haben Sie nun die Möglichkeit, aus den bisher für die einzelnen Elemente bruchstückhaft eingetragenen Formulierungen für Absatz 3 zusammenhängende Absätze zu formulieren, wie es für Ihren Regelungsbereich zweckdienlich ist. Im Folgenden werden Ihnen Ihre Formulierungen für die Absätze 1 und 2 sowie die Eingaben zu den einzelnen Elementen von Absatz 3 übersichtsartig angezeigt.`,
      hinweis1: {
        title: `Änderungen übertragen sich automatisch`,
        content:
          'Wenn Sie hier Änderungen vornehmen, werden diese mit den Eingaben auf den ursprünglichen Seiten synchronisiert. <a href="{{linkHinweis1}}">Gehe zur ursprünglichen Seite</a>',
      },
      textLabelAbsatz2: `<h2 class="ant-typography">Absatz 3 – Elemente 16 und 17:</h2>`,
      hinweis2: {
        title: `Änderungen übertragen sich automatisch`,
        content:
          'Wenn Sie hier Änderungen vornehmen, werden diese mit den Eingaben auf den ursprünglichen Seiten synchronisiert.',
      },
      overviewList: [
        { key: 'evaluationUndTransfer', label: `16: Evaluation und Transfer (notwendig)`, isMandatory: false },
        {
          key: 'befristungExperimentierklausel',
          label: `17: Befristung der Experimentierklausel (optional)`,
          isMandatory: false,
        },
      ],
      overviewListItemLink: `Gehe zur ursprünglichen Seite`,
      overviewListLegend: `Absatz 3 – Elemente 16 und 17:`,
      text2: `<p>Hier können Sie die einzelnen Elemente nun zu zusammenhängenden Sätzen verbinden.</p>`,
      formItem: {
        label: 'Absatz 3:',
        btnApplyText: 'Text von Element 16 und 17 übernehmen',
      },
    },
  },
  absatz4: {
    linkName: 'Absatz 4',
    verordnungsermaechtigung: {
      linkName: 'Verordnungsermächtigung',
      title: 'Absatz 4 – Verordnungsermächtigung (optional)',
      text1: `<p>Nicht alle Vorgaben für die Erprobung innovativer Technologien, Produkte, Dienstleistungen oder Ansätze müssen in einem Parlamentsgesetz geregelt werden. Stattdessen besteht die Möglichkeit, Experimentierklauseln in Teilen durch Rechtsverordnungen zu regeln.</p>`,
      text2: `Die Regelung durch Rechtsverordnung empfiehlt sich insbesondere dort, wo Einzelheiten der Elemente, die auf die Risikoreduzierung abzielen, flexibel formuliert werden müssen, z.B. bei Verfahrensvorgaben wie materieller Begrenzung, Umfang oder begleitenden Pflichten.`,
      item2Drawer: {
        title: `Der Vorteil von Rechtsverordnungen`,
        text: `<p>Der Vorteil ist, dass Verordnungen leichter verändert werden können, wenn sich grundlegende Annahmen, wie beispielsweise Risikoeinschätzungen oder das Erprobungsbedürfnis im Laufe der Erprobung ändern. So ist eine effektivere Steuerung bei gleichzeitig erhöhter Flexibilität gewährleistet. Es kann auch der stärkeren Lenkung der Verwaltung, einer einheitlichen Behördenpraxis und einer Verfahrensbeschleunigung dienen.</p>
                <p>Wird der gesamte Prozess der Schaffung einer Experimentierklausel an einen anderen Normgeber delegiert, ist die zu Grunde liegende Öffnungsklausel selbst keine Experimentierklausel und dient nur der Übertragung der Normgebungsbefugnis.</p>`,
      },
      text3: `Grundsätzlich gilt: Je komplexer der Regelungsbereich ausfällt, desto konkreter sollte die Verwaltungslenkung durch untergesetzliches Regelwerk ausfallen. In manchen Fällen kann eine Experimentierklausel vollständig in einer Rechtsverordnung geregelt werden. Dies bietet sich insbesondere bei komplexen und sich rasch ändernden Materien an.`,
      item3Drawer: {
        title: `Verwendung von Rechtsverordnungen`,
        text: `<p>Rechtsverordnungen sind nicht grenzenlos möglich, sondern bewegen sich innerhalb der durch die Gewaltenteilung, den Parlamentsvorbehalt, das Demokratieprinzip und die Grundrechte vorgezeichneten Grenzen und sind speziell durch Art. 80 GG geregelt.</p>`,
      },
      text4: `<p>Grundlage ist immer eine <strong>im Gesetz verankerte Verordnungsermächtigung</strong>. Für den Fall, dass die Experimentierklausel gesetzlich geregelt wird und in einer zusätzlichen Rechtsverordnung näher konkretisiert wird, muss die Experimentierklausel selbst eine Verordnungsermächtigung enthalten. Diese Verordnungsermächtigung hat dem <strong>Parlamentsvorbehalts</strong> (wesentliche Entscheidungen müssen vom Gesetzgeber getroffen werden) und den Anforderungen des Art. 80 Abs. 1 S. 2 GG (Anforderungen an das Gesetz, welches zum Erlass einer Rechtsverordnung ermächtigt) zu genügen. Daneben ist in der Rechtsverordnung die gesetzliche Rechtsgrundlage anzugeben (Art. 80 Abs. 1 S. 3 GG).</p>
              <p>Sollte die Experimentierklausel vorsehen, dass die Ermächtigung zum Erlass von Verordnungen nur für einen bestimmten Zeitraum besteht, empfiehlt es sich, Regelungen aufzunehmen, die sich mit der Fortwirkung der auf Grund der Experimentierklausel geschaffenen Rechtsverordnung befassen. Grundsätzlich bleibt die Wirksamkeit von untergesetzlichen Regelungen vom Wegfall der Ermächtigungsgrundlage unberührt.</p>
      `,

      hiddenSection1: {
        title: 'Beispiele',
        text: `
              <strong>Verordnungsermächtigung:</strong>
              <ul>
                  <li>
                      Beispiel: § 6 Abs. 1 Nr. 15 lit. c StVG: „Das Bundesministerium für Verkehr und digitale Infrastruktur wird ermächtigt, […] Rechtsverordnungen mit Zustimmung des Bundesrates über Folgendes zu erlassen: […]
                      <ul>
                          <li>15. die Beschränkung des Straßenverkehrs einschließlich des ruhenden Verkehrs […]</li>
                          <li>c) zur Erforschung des Unfallgeschehens, des Verkehrsverhaltens, der Verkehrsabläufe oder zur Erprobung geplanter verkehrssichernder oder verkehrsregelnder Maßnahmen, […].“</li>
                      </ul>
                  </li>
                  <li>Beispiel: § 13 Gewerbeordnung: „Die Landesregierungen werden ermächtigt, durch Rechtsverordnung zur Erprobung vereinfachender Maßnahmen, insbesondere zur Erleichterung von Existenzgründungen und Betriebsübernahmen, für einen Zeitraum von bis zu fünf Jahren Ausnahmen von Berufsausübungsregelungen nach diesem Gesetz und den darauf beruhenden Rechtsverordnungen zuzulassen, soweit diese Berufsausübungsregelungen nicht auf bindenden Vorgaben des Europäischen Gemeinschaftsrechts beruhen und sich die Auswirkungen der Ausnahmen auf das Gebiet des jeweiligen Landes beschränken.“</li>
              </ul>
              <strong>Befristung der Verordnungsermächtigung:</strong>
              <ul>
                  <li>Beispiel: § 19 S. 2 Hessisches E-Government-Gesetz: „Die Rechtsverordnung nach Satz 1 ist auf höchstens fünf Jahre zu befristen, die Geltungsdauer kann nicht verlängert werden.“</li>
              </ul>
      `,
      },

      text5: `<h2 class="ant-typography">Ihre Angabe</h2>
            <p>Die Formulierung einer Verordnungsermächtigung ist optional. Im folgenden Freitextfeld können Sie die Verordnungsermächtigung für Ihre Experimentierklausel gerne als vollständigen Satz formulieren:</p>`,

      formItem: {
        label: 'Absatz 4 – Element 18: Verordnungsermächtigung (optional)',
      },
      overviewListLegend: 'Absatz 4 – Element 18: Verordnungsermächtigung',
    },
  },
  zusammenfassung: {
    linkName: 'Formulierung der vollständigen Experimentierklausel',
    title: 'Formulierung der vollständigen Experimentierklausel',
    text1: `Sie haben nun alle im betroffenen Regelungsbereich benötigten Elemente der Experimentierklausel formuliert. Im Folgenden werden Ihnen Ihre Formulierungen für den Titel und alle Absätze angezeigt, damit Sie letzte Änderungen vornehmen können.`,
    hinweis1: {
      title: `Änderungen übertragen sich automatisch`,
      content:
        'Wenn Sie hier Änderungen vornehmen, werden diese mit den Eingaben auf den ursprünglichen Seiten synchronisiert. <a href="{{linkHinweis1}}">Gehe zur ursprünglichen Seite</a>',
    },
    formItem: {
      label: 'Vollständige Experimentierklausel',
      btnApplyText: 'Titel und Absätze 1-4 übernehmen',
    },
    text2: `<p>Sofern Sie keine weiteren Änderungen mehr vornehmen möchten, ist die Formulierung abgeschlossen.</p>`,
    text3: `
      <h2 class="ant-typography">Formulierung der Experimentierklausel</h2>
      <p>Zur Finalisierung können Sie nun die formulierten Textteile mit Klick auf “Titel und Absätze 1-4 übernehmen” zu einer vollständigen Experimentierklausel zusammenfügen. Im Textfeld können Sie Änderungen an Inhalt und Aufbau der Experimentierklausel vornehmen, die sich nicht automatisch mit den früheren Eingaben synchronisieren. Die Aufteilung und Anzahl der Absätze können frei gewählt und angepasst werden. Sie müssen nicht der vorgeschlagenen Aufteilung in bis zu vier Absätze entsprechen.</p>`,
    overviewList: [
      { key: 'titleExperimentierklausel', label: ``, isMandatory: false },
      { key: 'absatzZweiZusammenfassung', label: ``, isMandatory: false },
      { key: 'absatzDreiZusammenfassung', label: ``, isMandatory: false },
      { key: 'verordnungsermaechtigung', label: ``, isMandatory: false },
    ],
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    noImpact: 'Es sind keine Auswirkung.',
    subtitle: 'Vollständige Experimentierklausel',
    infoParagraph:
      'Wenn Sie in der Modulübersicht auf den Assistenten für Experimentierklauseln klicken, können Sie den Text Ihrer finalisierten Experimentierklausel wiederfinden und kopieren, um ihn in Ihren Regelungsentwurf einfügen zu können. Dort finden Sie auch eine Zusammenfassung, die Sie als Ergebnisdokumentation herunterladen können.',
  },
  contactPerson: de_contacts.experimentierklauseln.contactPerson,
};
