// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_demografie = {
  name: 'Demografie-Check',

  demografischeEntwicklung: {
    linkName: 'Demografische Entwicklung',
  },
  familieKinderJugendliche: {
    linkName: 'Familie, Kinder, Jugendliche',
  },
  arbeitBerufWirtschaft: {
    linkName: 'Arbeit, Beruf, Wirtschaft',
  },
  alterWohnenPflege: {
    linkName: 'Alter, Wohnen, Pflege',
  },
  regionen: {
    linkName: 'Regionen',
  },
  generationen: {
    linkName: 'Zusammenhalt der Generationen, Zivilgesellschaftliches Engagement',
  },

  einleitung: {
    linkName: 'Einführung',
    title: 'Demografie-Check',
    text: `<p>Die Prüfung und Berücksichtigung demografischer Auswirkungen bei Gesetzes- und Verordnungsvorhaben ist Bestandteil der gemäß <a href="{{basePath}}/arbeitshilfen/download/34#page=33" target="_blank" style="white-space: normal">§§ 43 Absatz 1 Nr. 5</a> und <a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank" style="white-space: normal">44
Absatz 1</a> der Gemeinsamen Geschäftsordnung der Bundesministerien (GGO)
durchzuführenden Gesetzesfolgenabschätzung.</p> 
<p>Die Dokumentation in der Gesetzesbegründung ermöglicht es den politischen Entscheidungsträgerinnen und Entscheidungsträgern, die Auswirkungen eines Vorhabens und mögliche Zielkonflikte
zu erkennen und in ihre politischen Entscheidungen einzubeziehen.</p>
<p>In diesem Sinne dient der nachfolgende Katalog von 24 Prüffragen als Hilfestellung für die
Ermittlung und Prüfung der demografischen Folgen und Risiken eines Vorhabens.</p>
<p>Weitere Informationen zur Demografiepolitik der Bundesregierung: <br/><br/>

<a href="https://www.bmi.bund.de/SharedDocs/downloads/DE/publikationen/themen/heimat-integration/demografie/demografiestrategie-weiterentwicklung.html" target="_blank">Weiterentwicklung der Demografiestrategie der Bundesregierung</a><br/><br/>
<a href="https://www.demografie-portal.de/DE/Startseite.html" target="_blank">Demografieportal</a>
</p>`,
  },
  fragenkatalog: {
    linkName: 'Fragenkatalog',
    title: 'Abschnitt I - Fragenkatalog',

    demografischeEntwicklung: {
      categoryTitle: 'Demografische Entwicklung',
      mainQuestionAuswirkung: 'Hat das Vorhaben Auswirkungen auf',
      auswirkungen: {
        title: 'Hat das Vorhaben Auswirkungen auf',
        items: {
          GEBURTEN_ENTWICKLUNG: {
            label: 'die zukünftige Geburtenentwicklung?',
            shortDescription: 'Geburtenentwicklung',
          },
          ZU_UND_ABWANDERUNG: {
            label: 'die künftige Zu- und Abwanderung?',
            shortDescription: 'Zu- und Abwanderung',
          },
          ALTER_STRUKTUR_DER_BEVOELKERUNG: {
            label:
              'die künftige Altersstruktur der Bevölkerung (Verhältnis Anzahl junger Menschen zu älteren Menschen)?',
            shortDescription: 'Altersstruktur der Bevölkerung',
          },
          REGIONAL_VERTEILUNG_DER_BEVOLKERUNG: {
            label:
              'die zukünftige regionale Verteilung der Bevölkerung (etwa Zunahme in Ballungsgebieten, Abnahme in ländlichen Räumen)?',
            shortDescription: 'Regionale Verteilung der Bevölkerung',
          },
        },
      },
      mainQuestionBeruecksichtigung: 'Berücksichtigt das Vorhaben',
      beruecksichtigungen: {
        title: 'Berücksichtigt das Vorhaben',
        items: {
          AENDERUNG_ALTER_STRUKTUR: {
            label: ' die voraussichtliche Änderung von Zahl und Altersstruktur der Bevölkerung?',
            shortDescription: 'Änderung Altersstruktur der Bevölkerung',
          },
          STEIGENDER_ANTEIL_DER_MIGRANTEN: {
            label:
              'die voraussichtlichen Veränderungen aufgrund eines steigenden Anteils von Menschen mit Migrationshintergrund an der Gesamtbevölkerung?',
            shortDescription: 'Steigender Anteil Menschen mit Migrationshintergrund',
          },
        },
      },
    },

    familieKinderJugendliche: {
      categoryTitle: 'Familie, Kinder, Jugendliche ',
      mainQuestionAuswirkung: 'Hat das Vorhaben Auswirkungen auf',
      auswirkungen: {
        title: 'Hat das Vorhaben Auswirkungen auf',
        items: {
          KINDER_UND_FAMILIE_FREUNDLICHKEIT: {
            label:
              ' die Kinder- und Familienfreundlichkeit (Verfügbarkeit von familiengerechtem Wohnraum, familienfreundliche Infrastrukturen, Betreuungsangeboten, Freizeitangeboten) in Deutschland?',
            shortDescription: 'Kinder- und Familienfreundlichkeit',
          },
          VEREINBARKEIT_VON_FAMILIE_UND_BERUF: {
            label:
              ' die Vereinbarkeit von Familie und Beruf (z.B. Arbeitszeitflexibilität von Müttern und Vätern oder Angehörigen von Pflegepersonen, partnerschaftliche Aufteilung von Erwerbsarbeit oder Fürsorgearbeit, Kinderbetreuung und Pflege)? ',
            shortDescription: 'Vereinbarkeit von Familie und Beruf',
          },
          BETREUUNG_UND_AUSBILDUNG_VON_KINDER_UND_JUGENDLICHEN: {
            label: ' die Betreuungs-, Bildungs- und Ausbildungsbedingungen von Kindern und Jugendlichen?',
            shortDescription: 'Betreuungs-, Bildungs- und Ausbildungsbedingungen von Kindern und Jugendlichen',
          },
        },
      },
    },

    arbeitBerufWirtschaft: {
      categoryTitle: 'Arbeit, Beruf, Wirtschaft',
      mainQuestionAuswirkung: 'Hat das Vorhaben Auswirkungen auf',
      auswirkungen: {
        title: 'Hat das Vorhaben Auswirkungen auf',
        items: {
          CHANCEN_QUALIFIZIERUNG_UND_WEITERBILDUNG: {
            label:
              'die Chancen von Bevölkerungsgruppen bei der Qualifizierung und Weiterbildung im gesamten Lebensverlauf?',
            shortDescription: 'Chancen bei Qualifizierung und Weiterbildung',
          },
          KULTUR_LAENGEREN_ARBEITEN: {
            label:
              'eine Kultur des längeren Arbeitens (z.B. flexible Gestaltung der Lebensarbeitszeit, Veränderungen in Bezug auf Altersgrenzen)?',
            shortDescription: 'Kultur des längeren Arbeitens',
          },
          ALTERSGERECHTE_ARBEITSWELT_UND_ARBEITS_GESTALTUNG: {
            label: 'eine altersgerechte Arbeitswelt und altersgerechte Arbeitsgestaltung?',
            shortDescription: 'Altersgerechte Arbeitswelt und altersgerechte Arbeitsgestaltung',
          },
          GESUNDHEITSFOEDERUNG_UND_KRANKHEITSPRAEVENTION_AM_ARBEITSPLATZ: {
            label: 'die Gesundheitsförderung und Krankheitsprävention am Arbeitsplatz/im Betrieb?',
            shortDescription: 'Gesundheitsförderung und Krankheitsprävention am Arbeitsplatz',
          },
          SICHERUNG_FACHKRAEFTEBASIS: {
            label:
              'die Sicherung der Fachkräftebasis (z.B. Entwicklung des Arbeitskräftepotenzials, Zu- und Abwanderung Hochqualifizierter, frühzeitiger Übergang in die Rente, Erwerbsbeteiligung von Müttern mit betreuungspflichtigen Kindern, Bildungsbeteiligung, Bildungserfolg von Migrantinnen und Migranten)?',
            shortDescription: 'Sicherung Fachkräftebasis',
          },
          INTEGRATION_ZUWANDERER: {
            label: 'die Integration von Zugewanderten?',
            shortDescription: 'Integration Zugewanderter',
          },
        },
      },
      mainQuestionBeruecksichtigung: 'Berücksichtigt das Vorhaben',
      beruecksichtigungen: {
        title: 'Berücksichtigt das Vorhaben',
        items: {
          VERAENDERUNGEN_NACHFRAGEVERHALTEN_UND_NACHFRAGEVERSCHIEBUNGEN: {
            label:
              'Veränderungen im Nachfrageverhalten durch einen Bevölkerungsrückgang oder Nachfrageverschiebungen durch eine Änderung der Altersstruktur mit Folgen auch für die Angebotsseite?',
            shortDescription: 'Veränderungen Nachfrageverhalten und Nachfrageverschiebungen',
          },
        },
      },
    },

    alterWohnenPflege: {
      categoryTitle: 'Alter, Wohnen, Pflege',
      mainQuestionAuswirkung: 'Hat das Vorhaben Auswirkungen auf',
      auswirkungen: {
        title: 'Hat das Vorhaben Auswirkungen auf',
        items: {
          SITUATION_VON_HILFE_UND_PFLEGEBEDUERFTIGEN: {
            label: 'die Situation von Menschen, die Hilfe und Pflege benötigen?',
            shortDescription: 'Situation von Hilfe- und Pflegebedürftigen',
          },
          SELBSTBESTIMMTES_WOHNEN_UND_MOBILITAET: {
            label: 'ein selbstbestimmtes Wohnen im vertrauten Umfeld und auf die Mobilität alter Menschen?',
            shortDescription: 'Selbstbestimmtes Wohnen und Mobilität',
          },
          SITUATION_PFLENGENDER_UND_AUSSCHOEPFUNG_DES_PFLEGEPOTENZIAL: {
            label: 'die Situation Pflegender oder die Ausschöpfung des Pflegepotenzials?',
            shortDescription: 'Situation Pflegender und Ausschöpfung des Pflegepotentials',
          },
          WOHNORTNAHE_UND_BARRIEREARME_VERSORGUNG_DASEINVERSORGE: {
            label: 'die wohnortnahe und barrierearme Versorgung mit Angeboten und Einrichtungen der Daseinsvorsorge?',
            shortDescription: 'Wohnortnahe und barrierearme Versorgung/Daseinsvorsorge',
          },
        },
      },
    },

    regionen: {
      categoryTitle: 'Regionen',
      mainQuestionAuswirkung: 'Hat das Vorhaben Auswirkungen auf',
      auswirkungen: {
        items: {
          GLEICHWERTIGE_LEBENSVERHAELTNISSE: {
            label:
              'Hat das Vorhaben Auswirkungen auf die Gewährleistung gleichwertiger Lebensverhältnisse insbesondere in ländlichen Räumen?',
            shortDescription: 'Gleichwertige Lebensverhältnisse',
          },
        },
      },
    },
    zusammenhaltDerGenerationenZivilgesellschaftlichesEngagement: {
      categoryTitle: 'Zusammenhalt der Generationen, Zivilgesellschaftliches Engagement',
      auswirkungen: {
        items: {
          FINANZIELLE_BELASTUNG_KUENFTIGE_GENERATIONEN: {
            label:
              'Führt das Vorhaben zu finanziellen Belastungen (z.B. Steuer- oder Abgabeerhöhungen, Erhöhungen der Sozialversicherungsbeiträge) für künftige Generationen?',
            shortDescription: 'Finanzielle Belastungen für künftige Generationen',
          },
          ZUSAMMENLEBEN_UND_UNTERSTUETZUNG_DER_GENERATIONEN: {
            label:
              'Hat das Vorhaben Auswirkungen auf das Zusammenleben und die gegenseitige Unterstützung der Generationen?',
            shortDescription: 'Zusammenleben und Unterstützung der Generationen',
          },
          ZIVILGESELLSCHAFTLICHES_ENGAGEMENT: {
            label: 'Hat das Vorhaben Auswirkungen auf das zivilgesellschaftliche Engagement (z.B. Ehrenamt)?',
            shortDescription: 'Zivilgesellschaftliches Engagement',
          },
        },
      },
    },
    textareaLabel:
      'Hier können Sie kurz und zusammenfassend die Maßnahmen oder die Gründe, warum Sie keine Maßnahmen ergreifen, beschreiben.',
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    auswirkungen: {
      singularAreaSelectionLabel:
        'Sie haben angegeben, dass das Vorhaben in dem folgenden Bereich Auswirkungen auf die demografische Entwicklung hat:',
      severalAreasSelectionsLabel:
        'Sie haben angegeben, dass das Vorhaben in folgenden Bereichen Auswirkungen auf die demografische Entwicklung hat:',
      textAreaSingularSelectionLabel: 'Bitte beschreiben Sie hier die Auswirkungen im ausgewählten Bereich',
      textAreaSeveralSelectionsLabel: 'Bitte beschreiben Sie hier die Auswirkungen in den ausgewählten Bereichen',
      textAreaDefaultLabel: 'Bitte beschreiben Sie hier die Auswirkungen',
    },
    beruecksichtigungen: {
      singularAreaSelectionLabel:
        'Sie haben angegeben, dass das Regelungsvorhaben voraussichtliche Veränderungen in dem folgenden Bereich berücksichtigt:',
      severalAreasSelectionsLabel:
        'Sie haben angegeben, dass das Regelungsvorhaben voraussichtliche Veränderungen in folgenden Bereichen berücksichtigt:',
      textAreaSingularSelectionLabel:
        'Bitte beschreiben Sie inwiefern das Regelungsvorhaben voraussichtliche Veränderungen im ausgewählten Bereich berücksichtigt.',
      textAreaSeveralSelectionsLabel:
        'Bitte beschreiben Sie hier inwiefern das Regelungsvorhaben voraussichtliche Veränderungen in den ausgewählten Bereichen berücksichtigt.',
    },
    textAreaBothSeveralSelectionsLabel:
      'Bitte beschreiben Sie hier die Auswirkungen in den ausgewählten Bereichen bzw. inwiefern das Regelungsvorhaben voraussichtliche Veränderungen in den ausgewählten Bereichen berücksichtigt.',
    noImpact: 'Das Vorhaben hat keine demografischen Auswirkungen.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Demografische Auswirkungen',
    impact: 'Demografische Auswirkungen',
    noImpact: 'Das Vorhaben hat keine demografischen Auswirkungen.',
    auswirkungen: {
      singularAreaSelectionLabel:
        'Das Vorhaben hat Auswirkungen auf die demografische Entwicklung in dem folgenden Bereich:',
      severalAreasSelectionsLabel:
        'Das Vorhaben hat Auswirkungen auf die demografische Entwicklung in folgenden Bereichen:',
    },
    beruecksichtigungen: {
      singularAreaSelectionLabel: 'Das Vorhaben berücksichtigt Veränderungen in dem folgenden Bereich:',
      severalAreasSelectionsLabel: 'Das Vorhaben berücksichtigt Veränderungen in folgenden Bereichen:',
    },
    infoParagraph:
      'Diese Angaben können Sie nach Fertigstellung des Moduls in den allgemeinen Teil der Begründung des Regelungsentwurfs übernehmen, um die demografischen Auswirkungen des Regelungsvorhabens unter dem Punkt „Gesetzesfolgen/Regelungsfolgen“ darzustellen.',
  },
  contactPerson: de_contacts.demografie.contactPerson,
};
