// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_verbraucher = {
  name: 'Auswirkungen auf Verbraucherinnen und Verbraucher',
  einleitung: {
    linkName: 'Einführung',
    title: 'Auswirkungen auf Verbraucherinnen und Verbraucher',
    text: `<p>In diesem Modul beschreiben Sie die Auswirkungen Ihres Regelungsvorhabens auf Verbraucherinnen und Verbraucher nach <a href="{{basePath}}/arbeitshilfen/download/34#page=35" target="_blank" style="white-space: normal">§ 44 Absatz 5 Nr. 2 GGO</a>.</p>
          <p>Anschließend können Sie die Beschreibung für Vorblatt und Begründung Ihres Regelungsvorhabens verwenden.</p>`,
  },
  wesentlicheAuswirkungen: {
    linkName: 'Wesentliche Auswirkungen',
    title: 'Wesentliche Auswirkungen',
    question:
      'Sind durch das Regelungsvorhaben wesentliche Auswirkungen auf Verbraucherinnen und Verbraucher zu erwarten?',
    additionalExplanation: 'Zusammenfassung der Auswirkungen',
    drawerText:
      'Die Angaben auf dieser Seite können im Anschluss an die Fertigstellung in den Begründungsteil/ in das Vorblatt des Regelungsentwurfs übernommen werden.',
    linkEnap:
      'In der Gesetzesfolgenabschätzung im Modul „eNAP - elektronische Nachhaltigkeitsprüfung” wurden relevante Auswirkungen festgestellt: <ul> <li>Indikatorenbereich 12.1 Nachhaltiger Konsum: Konsum umwelt- und sozialverträglich gestalten</li></ul> Schauen Sie sich die Ergebnisse auf der <a href="{{linkEnap}}">Fertigstellungsseite des Moduls</a> an.',
    insufficientRights:
      'Derzeit fehlen Ihnen die Leserechte für das Modul. Bitte wenden Sie sich an die für die Gesetzesfolgenabschätzung zuständige Person: {{namePerson}}.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung zur Darstellung der Auswirkungen auf Verbraucherinnen und Verbraucher in die Begründung des Regelungsentwurfs übernehmen.',
    subtitle: 'Auswirkungen auf Verbraucherinnen und Verbraucher',
    noImpact: 'Es sind keine wesentlichen Auswirkungen auf die Verbraucherinnen und Verbraucher zu erwarten.',
  },
  zusammenfassung: {
    noImpact: 'Es sind keine wesentlichen Auswirkungen auf die Verbraucherinnen und Verbraucher zu erwarten.',
  },
  contactPerson: de_contacts.verbraucher.contactPerson,
};
