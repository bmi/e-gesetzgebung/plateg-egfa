// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_disability = {
  name: 'Disability Mainstreaming',
  einleitung: {
    linkName: 'Einführung',
    title: 'Disability Mainstreaming',
    text: `<p>In diesem Modul beschreiben Sie die spezifischen Auswirkungen Ihres Regelungsvorhabens auf Menschen mit Behinderungen gegenüber Menschen ohne Behinderungen. Vor allem sollen dabei eine etwaige Benachteiligung sowie mögliche Beteiligungsdefizite von Menschen mit Behinderungen identifiziert werden.</p>
          <p>Anschließend können Sie die Beschreibung für Vorblatt und Begründung Ihres Regelungsvorhabens verwenden.</p>`,
  },
  relevanzpruefung: {
    linkName: 'Relevanzprüfung',
    title: 'Relevanzprüfung',
    intro:
      'Die Relevanzprüfung zielt darauf ab, Anhaltspunkte für spezifische Auswirkungen eines Vorhabens auf Menschen mit Behinderungen gegenüber Menschen ohne Behinderungen zu erhalten. Vor allem sollen dabei eine etwaige Benachteiligung sowie mögliche Beteiligungsdefizite von Menschen mit Behinderungen identifiziert werden.',
    additionalInfos: {
      info1: {
        title: 'Behinderung',
        text: `<p>Nach der UN-Behindertenrechtskonvention ist Behinderung als Wechselwirkung von langfristigen körperlichen, psychischen, geistigen Beeinträchtigungen oder Sinnesbeeinträchtigungen mit sozialen und oder umweltbedingten Barrieren zu begreifen. Zu „Menschen mit Behinderungen“ zählen demnach auch Bürgerinnen und Bürger, die zwar mit Beeinträchtigungen leben, jedoch nicht als „behindert“ oder „schwerbehindert“ anerkannt sind. Als langfristig gelten Beeinträchtigungen, die mit hoher Wahrscheinlichkeit länger als sechs Monate andauern.</p>`,
      },
      info2: {
        title: 'Relevanz',
        text: `<p>Eine Relevanz ist nur dann gegeben, wenn Menschen mit Behinderungen im Vergleich zu anderen Menschen in spezifischer Weise durch das Vorhaben betroffen sind. Die Systematik der Fragen ist von der <a href="https://www.deutscher-behindertenrat.de/ID25209" target="_blank"href="https://www.bfarm.de/DE/Kodiersysteme/Klassifikationen/ICF/_node.html" target="_blank">International Classification of Functioning, Disability and Health</a> der Weltgesundheitsorganisation abgeleitet. Zentraler Begriff ist die Teilhabe am Leben in der Gesellschaft (kurz „Teilhabe“). Vgl. § 1 SGB IX.</p>`,
      },
    },
    section1: {
      title: 'Sind durch das Regelungsvorhaben Menschen mit Behinderungen mittelbar oder unmittelbar betroffen?',
      unmittelbar: {
        title: 'Betrifft das Vorhaben in seinen Auswirkungen Menschen mit Behinderungen unmittelbar?',
        info: 'Unmittelbar betroffen sind Menschen mit Behinderungen, wenn sie Zielgruppe des Vorhabens sind.',
      },
      mittelbar: {
        title: 'Betrifft das Vorhaben in seinen Auswirkungen Menschen mit Behinderungen mittelbar?',
        info: 'Mittelbar betroffen sind diejenigen Personen, auf die das Regelungsvorhaben Auswirkungen hat, obwohl sie nicht Zielgruppe des Vorhabens sind.',
      },
    },
    section2: {
      title:
        'Gibt es Anhaltspunkte dafür, dass Menschen mit und ohne Behinderungen unterschiedlich von dem beabsichtigten Vorhaben betroffen sein können?',
      gleicheWeise: {
        title:
          'Bestehen Anhaltspunkte dafür, dass das verfolgte Ziel für Menschen mit Behinderungen nicht in gleicher Weise erreicht wird?',
      },
      vorgeseheneVorteile: {
        title:
          'Bestehen Anhaltspunkte dafür, dass Menschen mit Behinderungen von vorgesehenen Vorteilen nicht in gleicher Weise profitieren können?',
      },
      besondersBelastet: {
        title:
          'Bestehen Anhaltspunkte dafür, dass Menschen mit Behinderungen durch das Vorhaben in besonderer Weise belastet werden?',
      },
    },
    noInfluence:
      'Eine Relevanz ist nicht gegeben, da keine Anhaltspunkte dafür vorliegen, dass Menschen mit Behinderungen im Vergleich zu anderen Menschen in besonderer Weise betroffen sind.',
  },
  hauptpruefung: {
    linkName: 'Hauptprüfung',
    title: 'Hauptprüfung',
    checkboxSection: {
      sonstigesTitle: 'Sonstiger Bereich',
    },
    sections: [
      {
        title:
          'Sind in einem oder mehreren der folgenden Bereiche durch das Vorhaben positive oder negative Veränderungen zu erwarten, die Menschen mit Behinderungen vorrangig betreffen?',
        name: 'veraenderung',
        frageIndex: 1,
        sonstigesName: 'veraenderungSonstigesSummary',
        sonstigesTitle: 'Sonstige Bereiche',
        items: [
          { name: 'veraenderungGeistigeundPsychischeFunktionen', title: 'Geistige und psychische Funktionen' },
          { name: 'veraenderungMobilitaet', title: 'Mobilität' },
          {
            name: 'veraenderungHautNervenMuskelnSkelett',
            title: 'Haut, Nerven, Muskeln, Skelett',
            info: {
              title: 'Haut, Nerven, Muskeln, Skelett',
              content:
                'Beispiel: Berücksichtigung von gehbehinderten Menschen im Fernbuslinienverkehr und im Öffentlichen Personennahverkehr.',
            },
          },
          {
            name: 'veraenderungSinnesfunktionen',
            title: 'Sinnesfunktionen',
            info: {
              title: 'Sinnesfunktionen',
              content:
                'Beispiel: Es gibt derzeit kein Alarmierungssystem, das im Not- oder Katastrophenfall gehörlose Menschen zuverlässig erreicht. Deshalb ist diese Personengruppe bei Änderungen des Telekommunikationsgesetzes und bei der Notrufverordnung und beim Übergang von ISDN (internationaler Standard für ein digitales Telekommunikationsnetz) zu VoIP (Internet-Telefonie) betroffen.',
            },
          },
          {
            name: 'veraenderungStimmUndSprechfunktionen',
            title: 'Stimm- und Sprechfunktionen',
          },
          {
            name: 'veraenderungSchmerz',
            title: 'Schmerz',
          },
          {
            name: 'veraenderungSonstiges',
            title: 'Sonstige Bereiche',
          },
        ],
      },
      {
        title:
          'Ist in einem oder mehreren der folgenden Bereiche zu erwarten, dass das Vorhaben die Teilhabe- und Entfaltungsmöglichkeiten von Menschen mit Behinderungen in spezifischer Weise positiv oder negativ beeinflusst?',
        name: 'teilhabe',
        frageIndex: 2,
        sonstigesName: 'teilhabeSonstigesSummary',
        sonstigesTitle: 'Sonstige Teilhabe- und Entfaltungsmöglichkeiten',
        items: [
          {
            name: 'teilhabeFamilieUndPartnerschaftSowieSozialesNetz',
            title: 'Familie und Partnerschaft sowie soziales Netz',
          },
          { name: 'teilhabeBildungUndAusbildung', title: 'Bildung und Ausbildung' },
          {
            name: 'teilhabeErwerbstaetigkeitUndEinkommen',
            title: 'Erwerbstätigkeit und Einkommen',
          },
          {
            name: 'teilhabeAlltaeglicheLebensfuehrung',
            title: 'Alltägliche Lebensführung',
          },
          {
            name: 'teilhabeFortbewegungMobilitaet',
            title: 'Fortbewegung, Mobilität',
            info: {
              title: 'Fortbewegung, Mobilität',
              content:
                'Beispiel: Berücksichtigung von gehbehinderten Menschen im Fernbuslinienverkehr und im Öffentlichen Personennahverkehr.',
            },
          },
          {
            name: 'teilhabeGesundheitFreizeitUndSportSowieKulturUndMedien',
            title: 'Gesundheit, Freizeit und Sport sowie Kultur und Medien',
            info: {
              title: 'Gesundheit, Freizeit und Sport sowie Kultur und Medien',
              content:
                'Beispiel: Berücksichtigung der Barrierefreiheit im Filmförderungsgesetz (insbesondere Pflicht zur Erstellung von Untertitelung und Audiodeskription) und der Barrierefreiheit von Internetauftritten und -angeboten gemäß § 11 Behindertengleichstellungsgesetz.',
            },
          },
          {
            name: 'teilhabeDigitalisierung',
            title: 'Digitalisierung',
            info: {
              title: 'Digitalisierung',
              content:
                'Beispiel: Berücksichtigung der Barrierefreiheit von Internetauftritten und ‑angeboten gemäß § 11 Behindertengleichstellungsgesetz.',
            },
          },
          {
            name: 'teilhabeSicherheitUndSchutzVorGewalt',
            title: 'Sicherheit und Schutz vor Gewalt',
          },
          {
            name: 'teilhabePolitikUndOeffentlichkeit',
            title: 'Politik und Öffentlichkeit',
            info: {
              title: 'Politik und Öffentlichkeit',
              content:
                'Beispiel: Regelungen im Bundeswahlgesetz zu Bürgerinnen und Bürgern mit Betreuung in allen Lebensbereichen.',
            },
          },
          {
            name: 'teilhabeSonstiges',
            title: 'Sonstige Teilhabe- und Entfaltungsmöglichkeiten',
          },
        ],
      },
      {
        title:
          'Kann das Vorhaben dazu führen, dass sich für Menschen mit Behinderungen in spezifischer Weise der Zugang oder die Nutzungsmöglichkeiten verändern in Bezug auf …?',
        name: 'zugang',
        frageIndex: 3,
        sonstigesName: 'zugangSonstigesSummary',
        sonstigesTitle: 'Sonstiger Zugang oder sonstige Nutzungsmöglichkeiten',
        items: [
          {
            name: 'zugangProdukteTechnologien',
            title: 'Produkte, Technologien',
            info: {
              title: 'Produkte, Technologien',
              content:
                `<p>Beispiel zu Technologien: Barrierefreiheit von Webseiten und digitalen Informationsangeboten (Zum Beispiel durch Vorlesefunktion, Vergrößerung, leichte Sprache).</p>` +
                `<p>Beispiel zu Produkten: Braille-Kennzeichnung auf Medikamentenpackungen.</p>` +
                `<p>Beispiel zu Produkten: Bedienbarkeit von Touchscreens.</p>`,
            },
          },
          {
            name: 'zugangUmwelt',
            title: 'Umwelt (natürliche und von Menschen gestaltete)',
          },
          {
            name: 'zugangUnterstuetzung',
            title: 'Unterstützung',
          },
          {
            name: 'zugangSystemeDienstleistungen',
            title: 'Systeme, Dienstleistungen',
          },
          {
            name: 'zugangSonstiges',
            title: 'Sonstiger Zugang oder sonstige Nutzungsmöglichkeiten',
          },
        ],
      },
    ],
    konsistenzPruefung: {
      title: { inconsistent: 'Konsistenzprüfung', consistent: 'Vorgehen bei der Folgenabschätzung' },
      inconsistent:
        'Sie haben in keinem der oben aufgeführten Bereiche spezifische Auswirkungen auf Menschen mit Behinderungen durch das Vorhaben identifiziert, prüfen Sie bitte noch einmal Ihre Eingaben bei der Relevanzprüfung auf der vorherigen Seite.',
      consistent:
        `<p>Es besteht die Vermutung, dass das Vorhaben Folgewirkungen für Menschen mit Behinderungen auslösen kann. Schätzen Sie deshalb im nächsten Schritt mögliche Folgen ab. Das Bundesministerium für Arbeit und Soziales (BMAS) ist gemäß <a href="{{link1}}" target="_blank">§ 45 Absatz 1 in Verbindung mit Anlage 6 Nummer 7b GGO</a> bei der Erstellung von Gesetzentwürfen, Entwürfen von Rechtsverordnungen und Verwaltungsvorschriften zu beteiligen, wenn Belange von Menschen mit Behinderungen berührt sind. Das BMAS kann das federführende Ressort bei der Beteiligung der Verbände unterstützen und Expertinnen sowie Experten benennen. Nutzen Sie bitte frühzeitig das Fachwissen der Verbände und Organisationen von Menschen mit Behinderungen zu Behinderungsaspekten. Achten Sie dabei auf die Barrierefreiheit des Beteiligungsverfahrens, vor allem in Bezug auf barrierefreie Information und Kommunikation, z.B. durch die Nutzung strukturierter (PDF-)Dokumente.</p>` +
        `<p>Die oder der Beauftragte der Bundesregierung für die Belange von Menschen mit Behinderungen ist gemäß <a href="{{link2}}" target="_blank">§§ 45, Absatz 3, 21 Absatz 1 GGO</a> bei der Erstellung von Gesetzentwürfen, Entwürfen von Rechtsverordnungen und Verwaltungsvorschriften frühzeitig zu beteiligen, wenn Belange von Menschen mit Behinderungen berührt sind.</p>`,
    },
    info1: {
      title: 'Behindertenpolitische Fachverbände und Fachinformationen',
      text: `<ul>
            <li>Mitgliedsorganisationen des Deutschen Behindertenrats: <a href="https://www.deutscher-behindertenrat.de/ID25209" target="_blank">https://www.deutscher-behindertenrat.de/ID25209</a></li>
            <li>Mitgliedsorganisationen der BAG Selbsthilfe: <a href="https://www.bag-selbsthilfe.de" target="_blank">https://www.bag-selbsthilfe.de</a></li>
            <li>Bundesarbeitsgemeinschaft für Rehabilitation: <a href="https://www.bar-frankfurt.de/" target="_blank">https://www.bar-frankfurt.de/</a></li>
            <li>Inklusionsbeirat und Fachausschüsse bei der oder dem Beauftragten der Bundesregierung für die Belange von Menschen mit Behinderungen nach Art.33 UN BRK: <a href="https://www.behindertenbeauftragter.de/DE/AS/rechtliches/un-brk/un-brk.html" target="_blank">https://www.behindertenbeauftragter.de/DE/AS/rechtliches/un-brk/un-brk.html</a></li>
            <li>Politische Interessenvertretung behinderter Frauen im Weibernetz e.V.: <a href="https://weibernetz.de" target="_blank">https://weibernetz.de</a></li>
            <li>Fachinformationen: <a href="https://www.rehadat.de" target="_blank">https://www.rehadat.de</a></li>
            <li>Reha-Wissenschaftlerinnen und Reha-Wissenschaftler: <a href="https://rehadat-forschung.de/de/rehawissenschaftler/index.html" target="_blank">https://rehadat-forschung.de/de/rehawissenschaftler/index.html</a></li>
            <li>Aktionsbündnis Teilhabeforschung: <a href="https://www.reha-wissenschaften-drv.de" target="_blank">https://www.reha-wissenschaften-drv.de</a></li>
            </ul>
            <p>Teilhabeforschung des BMAS / der Bundesregierung:</p>
            <ul>
            <li><a href="https://www.bmas.de/DE/Service/Publikationen/Broschueren/a125-21-teilhabebericht.html" target="_blank">3. Teilhabebericht der Bundesregierung über die Lebenslagen von Menschen mit Beeinträchtigungen</a></li>
            <li><a href="https://www.bmas.de/SharedDocs/Downloads/DE/Publikationen/a125-16l-teilhabebericht-leichte-sprache.pdf;jsessionid=D08D8312C80D71EB0EFEEFF853751048.delivery1-replication?__blob=publicationFile&v=1" target="_blank">2. Teilhabebericht der Bundesregierung über die Lebenslagen von Menschen mit Beeinträchtigungen</a></li>
            <li><a href="https://www.gemeinsam-einfach-machen.de/SharedDocs/Downloads/DE/AS/Teilhabebericht.pdf?__blob=publicationFile&v=2" target="_blank">Teilhabebericht der Bundesregierung über die Lebenslagen von Menschen mit Beeinträchtigungen</a></li>
            </ul>
            <p>Zum Weiterlesen:</p>
            <ul>
            <li>Handbuch Behindertenrechtskonvention. Teilhabe als Menschenrecht - Inklusion als gesellschaftliche Aufgabe; Bundeszentrale für Politische Bildung 2015 Bd. 1506 ; Degener, Theresia (Hrsg.).</li>
            </ul>
            `,
    },
    info2: {
      title: 'Barrierefreie Information und Kommunikation',
      text: `<ul>
            <li>Prüfverfahren für Barrierefreiheit von Webseiten: <a href="https://www.bitvtest.de" target="_blank">https://www.bitvtest.de</a></li>
            <li>Informationstechnikzentrum Bund (ITZBund), Beratung der Bundesverwaltung zum BGG: <a href="https://www.itzbund.de/DE/Leistungsangebot/Beratung/BGG/bgg_node.html" target="_blank">https://www.itzbund.de/DE/Leistungsangebot/Beratung/BGG/bgg_node.html</a></li>
            <li>Angemessener Sprachgebrauch: <a href="https://leidmedien.de/" target="_blank">https://leidmedien.de/</a></li>
            </ul>`,
    },
    info3: {
      title: 'Strukturierte (PDF-) Dokumente',
      text: `<p>Wesentliche Voraussetzungen für die Erzeugung eines barrierefreien PDF-Dokuments sind:</p>
            <ul>
            <li>die Nutzung der Gliederungsfunktion</li>
            <li>die Verwendung von Formatvorlagen (beispielweise für Überschriften und Aufzählungen)</li>
            <li>das Vergeben von Alternativtexten für Abbildungen im Textverarbeitungsprogramm</li>
            </ul>
            <p>So strukturierte Texte ermöglichen insbesondere Blinden und Sehbehinderten eine schnelle Orientierung im Text, da Braille-, Großschriftbrowser und Sprachausgaben das Anspringen von Textstellen aus dem Inhaltsverzeichnis erlauben.</p>
            <p>Unstrukturierte Texte lassen sich jedoch selbst mit diesen Hilfsmitteln nur mühsam erschließen. Die Nutzung der Funktionen von Textverarbeitungsprogrammen ist also für die barrierefreie Gestaltung von Dokumenten elementar.</p>
            <p>Weiterhin sollten Dokumente über vollständig ausgefüllte Metadaten (Titel, Verfasser, Stichwörter etc.) und über Lesezeichen verfügen. Ein manuelles Nacharbeiten hinsichtlich der Barrierefreiheit im PDF-Programm ist daher empfehlenswert. Bei Ihrer Anwenderbetreuung bzw. Ihrem IT-Dienstleister finden Sie Beratung und Unterstützung zur Gestaltung barrierefreier Dokumente.</p>
            <p>Bitte bedenken Sie, dass ein barrierefreies PDF-Dokument nie besser sein kann, als das Ausgangsdokument, das ihm zugrunde liegt. Erstellen Sie deshalb, wenn möglich zunächst ein optimiertes Word-Dokument und konvertieren Sie dieses dann in ein PDF-Dokument. Insbesondere eingescannte Dokumente unter Verwendung von OCR-Software (Texterkennungssoftware) sind oft sehr fehlerhaft.</p>`,
    },
  },
  folgenabschaetzung: {
    linkName: 'Folgenabschätzung',
    title: 'Folgenabschätzung',
    textLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
    intro: {
      text: 'Die Folgenabschätzung dient dazu, aufbauend auf den Ergebnissen der Relevanzprüfung die Wirkungen des Vorhabens auf die Teilhabe von Menschen mit Behinderungen zu identifizieren und unbeabsichtigten Folgen so weit wie möglich entgegenzuwirken. Dazu müssen die Folgen im Hinblick auf mögliche Unterschiede für Menschen mit und ohne Behinderungen untersucht werden. Hier ist auch die besondere Vulnerabilität von Mädchen und Frauen mit Behinderungen zu berücksichtigen, die darauf beruht, dass sie mehrdimensionalen Diskriminierungen ausgesetzt sind.',
      info: {
        title: 'Mehrdimensionale Diskriminierung',
        text: `<p>Aufgrund der mehrdimensionalen Diskriminierung, der Mädchen und Frauen mit Behinderungen ausgesetzt sind, ist es wichtig, dass bei allen Vorhaben geprüft wird, ob die volle Entfaltung, die Förderung und die Stärkung der Autonomie dieser Mädchen und Frauen sichergestellt ist. Beispiel für die Berücksichtigung mehrfacher Diskriminierung: Informationsangebote zu Themen, bei denen Frauen und Männer sowie Mädchen und Jungen mit Behinderungen betroffen sind:</p>
            <ul>
            <li>Schutz vor sexualisierter und sonstiger Gewalt (mittelbare Betroffenheit, weil davon häufiger betroffen)</li>
            <li>Unterstützung von Mädchen und Frauen sowie Jungen und Männern, die von sexualisierter oder sonstiger Gewalt betroffen sind (mittelbare Betroffenheit, weil barrierefreie Angebote unzureichend sind)</li>
            <li>Sexualaufklärung, Informationen zu Verhütung, Schwangerschaft und Geburt (mittelbare Betroffenheit)</li>
            <li>Unterstützung von Müttern mit Behinderungen (unmittelbare Betroffenheit, Zielgruppe)</li>
            </ul>`,
      },
    },
    auswirkungen: {
      title: 'Auswirkungen auf Menschen mit Behinderungen',
      intro: 'Sie haben angegeben, dass Menschen mit Behinderungen in den folgenden Bereichen betroffen sind:',
      veraenderungLabel: 'Bereiche',
      teilhabeLabel: 'Teilhabe- und Entfaltungsmöglichkeiten',
      zugangLabel: 'Zugang und Nutzungsmöglichkeiten',
      leitfragen: {
        title:
          'Bitte nutzen Sie die nachfolgenden Leitfragen zur Beschreibung der Auswirkungen des Regelungsvorhabens auf Menschen mit Behinderungen:',
        content: `<ul>
            <li>In welchen Lebensbereichen sind welche Wirkungen zu erwarten?</li>
            <li>Wie verändert sich dadurch die Lage von Menschen mit Behinderungen?</li>
            <li>Welche unterschiedlichen Folgewirkungen können durch das Vorhaben hinsichtlich der Teilhabechancen von Menschen mit und ohne Behinderungen auftreten?</li>
            <li>Welche Intensität haben diese Folgewirkungen im Hinblick auf Schwere und Dauer?</li>
            <li>Ergeben sich unter Berücksichtigung der UN-Behindertenrechtskonvention Allianzen oder Konflikte mit ressortpolitischen Zielen wie z.B. mit finanziellen Prioritätensetzungen?</li>
            </ul>`,
      },
      hinweise: {
        title: 'Allgemeine Hinweise zur Folgenabschätzung',
        content: `<ul>
            <li>Ziehen Sie für die Abschätzung der Auswirkungen Daten heran (Statistiken, Forschungsergebnisse usw.). Diese sollten möglichst nach Behinderung, Geschlecht und weiteren soziodemografischen Merkmalen differenziert sein.</li>
            <li>Beziehen Sie sachkundige Stellen und Organisationen der Menschen mit Behinderungen mit ihrer Expertise hinsichtlich der Teilhabeaspekte ein.</li>
            <li>Beachten Sie die Vorschriften zur Einbindung des BMAS und der/des Beauftragten der Bundesregierung für die Belange von Menschen mit Behinderungen.</li>
            <li>Thematisieren Sie in Abstimmungsprozessen die Auswirkungen auf Menschen mit Behinderungen.</li>
            </ul>`,
      },
      info: {
        title: 'Daten und Statistiken',
        text: `<ul>
            <li>Statistisches Bundesamt, Homepage: <a href="https://www.destatis.de/DE/Startseite.html" target="_blank">https://www.destatis.de/DE/Startseite.html</a></li>
            <li>Gesundheitsberichterstattung des Bundes, GBE: <a href="https://www.gbe-bund.de/" target="_blank">https://www.gbe-bund.de/</a></li>
            <li>Sozio-oekonomisches Panel: <a href="https://www.diw.de/de/soep" target="_blank">https://www.diw.de/de/soep</a></li>
            <li>Teilhabebericht der Bundesregierung über die Lebenslagen von Menschen mit Beeinträchtigungen: <a href="https://www.bmas.de/DE/Service/Publikationen/Broschueren/a125-21-teilhabebericht.html" target="_blank">https://www.bmas.de/DE/Service/Publikationen/Broschueren/a125-21-teilhabebericht.html</a></li>
            <li>Eurostat: <a href="https://ec.europa.eu/eurostat/de/home" target="_blank">https://ec.europa.eu/eurostat/de/home</a></li>
            <li>Prävalenzstudie des BMFSFJ „Lebenssituation und Belastungen von Frauen mit Beeinträchtigungen und Behinderungen in Deutschland. Ergebnisse der quantitativen Befragung im Endbericht“: <a href="https://www.bmfsfj.de/bmfsfj/service/publikationen/lebenssituation-und-belastungen-von-frauen-mit-behinderungen-und-beeintraechtigungen-in-deutschland-80578" target="_blank">https://www.bmfsfj.de/bmfsfj/service/publikationen/lebenssituation-und-belastungen-von-frauen-mit-behinderungen-und-beeintraechtigungen-in-deutschland-80578</a></li>
            </ul>`,
      },
      input: {
        label: 'Auswirkungen auf Menschen mit Behinderungen',
        error: 'Bitte geben Sie die Auswirkungen auf Menschen mit Behinderungen an.',
      },
    },
    massnahmen: {
      title: 'Maßnahmen',
      intro: {
        content: `<p>Wie möchten Sie der unterschiedlichen Betroffenheit von Menschen mit und ohne Behinderungen begegnen? Angesichts Ihrer Beschreibung der Auswirkungen, welche Schlussfolgerungen ergeben sich?
              <br>Berücksichtigen Sie dabei insbesondere die folgenden Aspekte:</p>
              <ul>
              <li>Sind teilhabepolitische Vor- oder Nachteile der geprüften Alternativen und Varianten zu erkennen? Sind flankierende Maßnahmen zu ergreifen?</li>
              <li>Sind Maßnahmen zur Verbesserung der Datenlage zu veranlassen?</li>
              </ul>`,
      },
      input: {
        label: 'Maßnahmen',
      },
    },
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    auswirkungenTitle: 'Auswirkungen auf Menschen mit Behinderungen',
    massnahmenTitle: 'Maßnahmen',
    zusammenfassungTitle: 'Bitte fassen Sie Ihre Ergebnisse abschließend zusammen.',
    noImpact:
      'Eine Relevanz ist nicht gegeben, da keine Anhaltspunkte dafür vorliegen, dass Menschen mit Behinderungen im Vergleich mit anderen Menschen in besonderer Weise betroffen sind.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    subtitle: 'Auswirkungen auf Menschen mit Behinderungen',
    noImpact:
      'Eine Relevanz ist nicht gegeben, da keine Anhaltspunkte dafür vorliegen, dass Menschen mit Behinderungen im Vergleich mit anderen Menschen in besonderer Weise betroffen sind.',
    infoParagraph:
      'Diese Angaben können Sie nach Fertigstellung des Moduls in den allgemeinen Teil der Begründung des Regelungsentwurfs übernehmen, um die Auswirkungen des Regelungsvorhabens auf Menschen mit Behinderung unter dem Punkt „Gesetzesfolgen/Regelungsfolgen“ darzustellen.',
  },
  contactPerson: de_contacts.disability.contactPerson,
};
