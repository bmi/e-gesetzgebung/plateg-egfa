// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_erfuellungsaufwand = {
  name: 'Erfüllungsaufwand',
  einleitung: {
    linkName: 'Einführung',
    title: 'Ermittlung und Darstellung des Erfüllungsaufwands',
    content: {
      paragraph1: {
        content: `In der <a href="{{link}}" target="_blank">GGO bestimmt § 44 Absatz 4</a>, dass Ressorts für die Vorlage von Regelungsvorhaben der Bundesregierung den Erfüllungsaufwand ermitteln und darstellen müssen.`,
      },
      paragraph2: {
        content: `Dieses Modul begleitet Sie dabei bei allen relevanten Arbeitsschritten. Dazu stehen Ihnen Formulare zur Ermittlung des Erfüllungsaufwands zur Verfügung.`,
      },
      paragraph3: {
        content: `Wissenschaftliche Genauigkeit ist nicht erforderlich; es kommt vielmehr darauf an, den Entscheidungsträgerinnen und -trägern sowie der Öffentlichkeit mit angemessenem Aufwand ein realitätsnahes Bild der aus der Perspektive der Normadressaten zu erwartenden Be- und Entlastungen zu geben.`,
      },
      paragraph4: {
        content: `Die Verpflichtung zur Quantifizierung der erwarteten Be- und Entlastungen dient dabei vor allem der konkreten Beschreibung dieser Folgen. Aus dem gleichen Grund basiert die Ermittlung des Erfüllungsaufwands in der Regel auf der Untersuchung des Aufwands im Einzelfall.`,
      },
      paragraph5: {
        content: `Für Rückfragen zu dieser Methodik stehen das Referat <strong>DA 2 (Bessere Rechtsetzung; Geschäftsstelle Bürokratieabbau) im Bundesjustizministerium</strong>,<strong> das Statistische Bundesamt (StBA)</strong> sowie das <strong>Sekretariat des NKR</strong> zur Verfügung.`,
      },
      paragraph6: {
        content: `Die Ermittlung des Erfüllungsaufwands steht in engem Zusammenhang mit einer Reihe weiterer bei der Erarbeitung von Regelungsvorhaben zu beachtenden Vorgaben, zu denen insbesondere folgende Dokumente zu beachten sind:
              <ol>
                <li><a href={{link1}} target='_blank'>Auswirkungen auf mittelständische Unternehmen (KMU-Test)</a></li>
                <li><a href={{link2}} target='_blank'>One in, one out-Regel (PDF)</a></li>
                <li><a href={{link3}} target='_blank'>Evaluierung neuer Regelungsvorhaben</a></li>
              </ol>`,
      },
    },
  },
  gesamtsummenNormadressaten: {
    linkName: 'Gesamtsummen der Normadressaten',
    title: 'Gesamtsummen der Normadressaten',
    expandIconCellHeader: 'Funktion',
    deleteIconCellHeader: 'Aktionen',
    BUERGER: {
      title: `E. 1 Bürgerinnen und Bürger`,
      textTitle: 'Bürgerinnen und Bürger',
      btnNewVorgabe: 'Neue Vorgabe hinzufügen',
      btnCsvExport: 'Bestehende Vorgabe exportieren (CSV)',
      btnCsvsExport: 'Bestehende Vorgaben exportieren (CSV)',
    },
    WIRTSCHAFT: {
      title: `E. 2 Wirtschaft`,
      textTitle: 'Wirtschaft',
      btnNewVorgabe: 'Neue Vorgabe hinzufügen',
      btnCsvsExport: 'Bestehende Vorgaben exportieren (CSV)',
      btnCsvExport: 'Bestehende Vorgabe exportieren (CSV)',
    },
    VERWALTUNG: {
      title: `E. 3 Verwaltung`,
      textTitle: 'Verwaltung',
      btnNewVorgabe: 'Neue Vorgabe hinzufügen',
      btnCsvExport: 'Bestehende Vorgabe exportieren (CSV)',
      btnCsvsExport: 'Bestehende Vorgaben exportieren (CSV)',
    },
    table: {
      th: {
        bezeichnung: 'Bezeichnung',
        jaehrlichenPersonalaufwands: 'Veränderung des jährlichen Personalaufwands',
        jaehrlichenPersonalaufwandsBuerger: 'Veränderung des jährlichen Zeitaufwands',
        jaehrlichenSachaufwands: 'Veränderung des jährlichen Sachaufwands',
        einmaligerPersonalaufwand: 'Einmaliger Personalaufwand',
        einmaligerPersonalaufwandBuerger: 'Einmaliger Zeitaufwand',
        einmaligerSachaufwand: 'Einmaliger Sachaufwand',
        actions: 'Aktionen',
      },
      noDataSimple: `<p>Sie haben noch keine Daten eingepflegt.</p>
      Berechnen Sie hier den Erfüllungsaufwand Ihres Regelungsvorhabens für die {{sectionKey}}. <br> Klicken Sie dazu auf „Neue Vorgabe hinzufügen“. <br> Sie können auch bestehende Datensätze importieren.`,
    },
    newVorgabeModal: {
      mainTitle: 'Normadressat {{sectionKey}}: Neue Vorgabe hinzufügen',
      mainTitleWirtschaft: 'Normadressat Wirtschaft: Neue Vorgabe hinzufügen',
      mainTitleWirtschaftEdit: 'Normadressat Wirtschaft: Vorgabe bearbeiten',
      mainTitleEdit: 'Normadressat {{sectionKey}}: Vorgabe bearbeiten',
      btnCancel: 'Abbrechen',
      btnNext: 'Nächster Schritt',
      btnPrev: 'Vorheriger Schritt',
      page1: {
        title: 'Schritt 1 von 2: Art und Bezeichnung der Vorgabe identifizieren',
        titleWirtschaft: 'Schritt 1 von 2: Art und Bezeichnung der Vorgabe identifizieren',
        item1: {
          label: 'Bitte wählen Sie die Art der Vorgabe aus',
          requiredMsg: 'Bitte wählen Sie die Art der Vorgabe aus',
          vorgabeLabel: 'weitere Vorgabe (z.B. Zielerreichung, Erfüllung von Anordnungen, Kooperation)',
          informationspflichtLabel: 'Informationspflicht (z.B. Anträge, Genehmigungen, Dokumentationen)',
        },
        item2: {
          labelVorgabe: 'Bezeichnung der {{artVorgabe}}',
          requiredMsg: 'Bitte geben Sie die Bezeichnung an',
        },
        spiegelvorgabeCheckboxLabel: 'Diese {{artVorgabe}} hat eine Spiegelvorgabe oder mehrere Spiegelvorgaben.',
        item3: {
          label: 'Spiegelvorgaben auswählen',
          requiredMsg: 'Bitte wählen Sie eine Spiegelvorgabe aus',
          placeholder: 'Bitte auswählen',
        },
        itemEbene: {
          label: 'Bitte wählen Sie die Verwaltungsebene der Vorgabe aus, auf der der Erfüllungsaufwand anfällt',
          requiredMsg: 'Bitte wählen Sie die Verwaltungsebene der Vorgabe aus, auf der der Erfüllungsaufwand anfällt',
          bundLabel: 'Bund',
          landLabel: 'Land (inkl. Kommunen)',
        },
      },
      page2: {
        title: 'Schritt 2 von 2: Rechtsgrundlage und Fallgruppe beschreiben',
        btnNext: 'Angaben überprüfen',
        item1: {
          label: 'Paragraf und Rechtsnorm spezifizieren',
        },
      },
      page3: {
        title: 'Eingaben prüfen',
        btnNext: 'Neue Vorgabe jetzt hinzufügen',
        btnNextEdit: 'Vorgabe jetzt aktualisieren',
        definitionTitles: {
          art: 'Art der Vorgabe:',
          artWirtschaft: 'Art der Vorgabe:',
          artType: {
            VORGABE: 'Vorgabe',
            INFORMATIONSPFLICHT: 'Informationspflicht',
          },
          bezeichnung: 'Bezeichnung der Vorgabe:',
          bezeichnungWirtschaft: 'Bezeichnung der Vorgabe:',
          spiegelvorgaben: 'Spiegelvorgaben:',
          paragrafUndRechtsnorm: 'Paragraf und Rechtsnorm:',
          fallgruppen: 'Fallgruppe:',
          ebene: 'Verwaltungsebene:',
          ebeneType: {
            BUND: 'Bund',
            LAND: 'Land (inkl. Kommunen)',
          },
        },
      },
    },
    expertMode: {
      switchLabel: 'Expertenmodus',
      drawerSwitchLabel: `<p>Der Expertenmodus ist für Personen gedacht, die bereits über Erfahrungen bei der Abschätzung des Erfüllungsaufwandes verfügen, und ermöglicht eine schnellere Bearbeitung der Erfüllungsaufwandsschätzung. Sie können in diesem Modus die Vorgaben direkt verwalten sowie die Werte mehrerer Vorgaben gleichzeitig bearbeiten. Sie erhalten dafür jedoch weniger Unterstützung. Beispielsweise werden die Eingaben nicht jederzeit vollständig validiert.</p>
            <p>Möchten Sie detailliertere Eingaben zu einzelnen Vorgaben machen, gehen Sie im Normalmodus in die Einzelansicht der betreffenden Vorgabe. Sie können den Bearbeitungsmodus jederzeit wechseln. Bei einem Wechsel bleiben Stammdaten, Gesamt- und Teilsummen sowie Fallzahlen, Standardzeiten und Lohnsätze der Vorgaben erhalten. Berechnungswege können nach einem Moduswechsel allerdings abweichen und eine erneute Bearbeitung erforderlich machen.</p>`,
      confirmModalTitle: 'Hinweis',
      enableText:
        'Sie wechseln nun in den Expertenmodus. Bitte prüfen Sie Ihre Eingaben sorgfältig, da sie nicht vollständig validiert werden. Detailliertere Eingaben sind im Normalmodus in der Einzelansicht der betreffenden Vorgabe möglich. Stammdaten, Gesamt- und Teilsummen sowie Fallzahlen, Standardzeiten und Lohnsätze der Vorgaben bleiben erhalten. Berechnungswege können nach dem Wechsel abweichen. Möchten Sie fortfahren?',
      disableText:
        'Sie wechseln nun in den geführten Modus. In der Einzelansicht der betreffenden Vorgabe können Sie detaillierte Berechnungswege eingeben. Stammdaten, Gesamt- und Teilsummen sowie Fallzahlen, Standardzeiten und Lohnsätze der Vorgaben aus dem Expertenmodus bleiben erhalten. Berechnungswege können nach dem Wechsel allerdings abweichen. Möchten Sie fortfahren?',
      btnOk: 'Ok, fortfahren',
      btnCancel: 'Abbrechen',
      table: {
        bezeichnung: 'Bezeichnung der {{type}}',
        bezeichnungSimple: 'Bezeichnung der Vorgabe',
        btnRemove: 'Vorgabe entfernen',
        art: 'Typ',
        ebene: 'Verwaltungs-ebene',
        paragraf: 'Paragraf und Rechtsnorm',
        spiegelvorgaben: 'Spiegelvorgabe(n)',
        bemerkungen: 'Bemerkungen',
        btnDelete: '',
        BUERGER: {
          personalaufwand_jaehrlich_title: 'Veränderung des jährlichen Zeitaufwands',
          sachaufwand_jaehrlich_title: 'Veränderung des jährlichen Sachaufwands',
          personalaufwand_einmalig_title: 'Einmaliger Zeitaufwand',
          sachaufwand_einmalig_title: 'Einmaliger Sachaufwand',
        },
        WIRTSCHAFT: {
          personalaufwand_jaehrlich_title: 'Veränderung des jährlichen Personalaufwands',
          sachaufwand_jaehrlich_title: 'Veränderung des jährlichen Sachaufwands',
          personalaufwand_einmalig_title: 'Einmaliger Personalaufwand',
          sachaufwand_einmalig_title: 'Einmaliger Sachaufwand',
        },
        VERWALTUNG: {
          personalaufwand_jaehrlich_title: 'Veränderung des jährlichen Personalaufwands',
          sachaufwand_jaehrlich_title: 'Veränderung des jährlichen Sachaufwands',
          personalaufwand_einmalig_title: 'Einmaliger Personalaufwand',
          sachaufwand_einmalig_title: 'Einmaliger Sachaufwand',
        },
        zeitaufwandProFall: 'Zeit-aufwand pro Fall',
        fallzahl: 'Fallzahl',
        zeitaufwandGesamt: 'Zeit-aufwand gesamt',
        sachaufwandProFall: 'Sach-aufwand Pro Fall',
        sachaufwandGesamt: 'Sach-aufwand gesamt',
        lohnsatz: 'Lohnsatz',
        personalaufwandProFall: 'Personal-aufwand pro Fall',
        personalaufwandGesamt: 'Personal-aufwand gesamt',
        btnAddNewItem: 'Neue Vorgabe hinzufügen',
        btnSave: 'Eingaben speichern',
      },
    },
  },
  vorgabePage: {
    title: '{{artType}} „{{vorgabeBezeichnung}}“:<br> Belastungen und Entlastungen ermitteln',
    artType: {
      VORGABE: 'Vorgabe',
      INFORMATIONSPFLICHT: 'Informationspflicht',
    },
    preview: {
      keineAngabe: 'Keine Angabe',
      offnen: 'Öffnen',
      bearbeiten: 'Bearbeiten',
      loeschen: 'Löschen',
      loeschentitle: 'Möchten Sie die Vorgabe löschen?',
      loeschenContent: 'Achtung, wenn Sie die Vorgabe {{vorgabeName}} löschen, ist diese nicht mehr verfügbar.',
      loeschenCancel: 'Abbrechen',
      loeschenConfirm: 'Löschen',
      fallgruppe: 'Fallgruppe:',
      bemerkungen: 'Bemerkungen:',
      paragrafUndRechtsnorm: 'Paragraf und Rechtsnorm:',
      vollzugsebene: 'Verwaltungsebene: ',
      spiegelvorgabe: 'Spiegelvorgabe:',
      btnEdit: 'Zur Bearbeitung',
      spiegelvorgabeDrawer: {
        title: 'Spiegelvorgabe: „{{spiegelvorgabeName}}“',
        normadressat: 'Normadressat:',
        paragraf: 'Paragraf:',
        jaehrlicherPersonalaufwand: 'Jährlicher Personalaufwand:',
        jaehrlicherPersonalaufwandBuerger: 'Jährlicher Zeitaufwand:',
        fallzahlenTitle: 'Fallzahl',
        zeitaufwandProFall: 'Zeitaufwand pro Fall',
        summeZeitaufwand: 'Summe Zeitaufwand',
        sachaufwandProFall: 'Sachaufwand pro Fall',
        summeSchaufwand: 'Summe Sachaufwand',
        jaehrlicherSachaufwand: 'Jährlicher Sachaufwand:',
        einmaligerPersonalaufwand: 'Einmaliger Personalaufwand:',
        einmaligerPersonalaufwandBuerger: 'Einmaliger Zeitaufwand:',
        einmaligerSachaufwand: 'Einmaliger Sachaufwand:',
        lintToVorgabe: 'Zu dieser Vorgabe wechseln',
      },
    },
    comment: {
      commentPreviewLabel: 'Bemerkungen und Hinweise:',
      commentCheckboxLabel: 'Bemerkungen und Hinweise hinzufügen.',
      textareaLabel: 'Bemerkungen und Hinweise',
      btnEdit: 'Zur Bearbeitung',
      btnSave: 'Bemerkungen jetzt speichern',
    },
    gesamtsummen: {
      title: 'Gesamtsummen',
      PERSONALAUFWAND_JAEHRLICH: 'Veränderung des jährlichen Personalaufwands',
      PERSONALAUFWAND_JAEHRLICHBuerger: 'Veränderung des jährlichen Zeitaufwands',
      SACHAUFWAND_JAEHRLICH: 'Veränderung des jährlichen Sachaufwands',
      PERSONALAUFWAND_EINMALIG: 'Einmaliger Personalaufwand',
      PERSONALAUFWAND_EINMALIGBuerger: 'Einmaliger Zeitaufwand',
      SACHAUFWAND_EINMALIG: 'Einmaliger Sachaufwand',
      btnEdit: 'Summe bearbeiten',
      btnSave: 'Bearbeitung jetzt abschließen',
    },
    tablesSection: {
      title: '{{sectionDefinition}} ermitteln',
      fallzahlTitle: '1. Fallzahl pro Jahr berechnen',
      fallzahlTable: {
        veraenderungAnzahl: 'Anzahl Betroffene',
        veraenderungHaeufigkeit: 'Häufigkeit',
        veraenderungFallzahl: 'Fallzahl',
        veraenderungHinweise: 'Hinweise',
      },
      aufwandBerechnenTitle: '2. Aufwand berechnen',
      aufwandBerechnenInfo:
        'Nutzen Sie vor einem Wechsel der Berechnungsart die Funktion „Zwischenspeichern“, um Datenverluste zu vermeiden.',
      berechnungsart: {
        GESAMTAUFWAND_PRO_FALL: 'Gesamtaufwand pro Fall angeben',
        EINZELNE_STANDARDAKTIVITAETEN: 'Einzelne Standardaktivitäten identifizieren',
        PERSONEN_TAGE_MONATE: 'Personentage/-monate angeben',
      },
    },
    expensesDrawer: {
      loadingError: 'Daten für Lohnkostendrawer konnten nicht geladen werden:',
      verwaltung: {
        title: 'Lohnkostentabelle Verwaltung',
        wirtschaftSelectLabel: 'Verwaltungsbereich auswählen',
        bund: 'Bund',
        laender: 'Länder',
        kommunen: 'Kommunen',
        sozialversicherung: 'Sozialversicherung',
        durchschnitt: 'Durchschnitt',
        loadingError: 'Daten für Lohnkostendrawer verwaltung konnten nicht geladen werden:',
        unit: '€/Std',
      },
      wirtschaft: {
        title: 'Lohnkostentabelle Wirtschaft',
        wirtschaftSelectLabel: 'Wirtschaftsbereich auswählen',
        niedrig: 'Niedrig',
        mittel: 'Mittel',
        hoch: 'Hoch',
        durchschnitt: 'Durchschnitt',
        loadingError: 'Daten für Lohnkostendrawer wirtschaft konnten nicht geladen werden:',
        hinweise: 'Hinweise',
        standDate: 'Stand: 2021',
        lowQualificationLevel:
          'Niedriges Qualifikationsniveau: Beschäftigte mit ausführenden Tätigkeiten sowie Beschäftigte ohne abgeschlossene berufliche Ausbildung.',
        mediumQualificationLevel:
          'Mittleres Qualifikationsniveau: Beschäftigte mit qualifizierten Tätigkeiten, die nach Anweisung erledigt werden.',
        highQualificationLevel:
          'Hohes Qualifikationsniveau: Geschäftsleitung bzw. Beschäftigte mit Führungsaufgaben/Entscheidungsbefugnis sowie Beschäftigte mit eigenständiger Leistung in verantwortlicher Tätigkeit.',
        traineesCost: 'Die Lohnkosten für Auszubildende betragen unabhängig vom Wirtschaftszweig 7,00 Euro pro Stunde.',
        unit: '€/Std',
      },
      wegezeiten: {
        title: 'Wege- und Wartezeiten',
        verwaltungsebene: 'Verwaltungsebene',
        wegezeitenMin: 'Wegezeiten (Min.)',
        name: 'Wegezeiten',
        standDate: 'Stand: 2021, Quelle: Statistisches Bundesamt.',
        unit: 'Min',
        description:
          'Die folgende Tabelle zeigt pauschale Wegezeiten in Abhängigkeit der zuständigen Verwaltungsebene. Gibt es belastbare Anhaltspunkte dafür, dass der nach der Tabelle ermittelte Wert aller Wahrscheinlichkeit nach über- oder unterzeichnet ist, sollte der aus Fachsicht realistischere Wert für die Ermittlung genutzt werden.',
      },
      pdfTableLink: 'Gesamte Lohnkostentabelle öffnen',
    },
    buttons: {
      btnAddNewItem: 'Selbst beschriebene Aktivität hinzufügen',
      btnAddNewItemMaterial: 'Selbst beschriebenen Sachaufwand hinzufügen',
      btnRestoreDefaultData: 'Standardaktivitäten wiederherstellen',
      btnRestoreDefaultDataMaterial: 'Sachaufwandsarten wiederherstellen',
      btnDeleteEmptyRows: 'Alle leeren Zeilen entfernen',
    },
    PERSONALAUFWAND_JAEHRLICH_GESAMTAUFWAND_PRO_FALL: {
      defaultBezeichnung: 'Gesamtaufwand pro Fall',
      tableHeaders: {
        bezeichnung: 'Bezeichnung',
        bemerkungen: 'Bemerkungen',
        zeitaufwandProFallGroup: 'Zeitaufwand pro Fall',
        zeitaufwandBisher: 'Bisher',
        zeitaufwandNeu: 'Neu/ Absolut',
        zeitaufwandDifferenz: 'Differenz',
        standardlohnsatzGroup: 'Standardlohnsatz Ø',
        lohnsatzBisher: 'Bisher',
        lohnsatzNeu: 'Neu/ Absolut',
        personalaufwandProFallGroup: 'Personalaufwand pro Fall',
        personalaufwandBisher: 'Bisher',
        personalaufwandNeu: 'Neu/ Absolut',
        personalaufwandDifferenz: 'Differenz',
      },
    },
    PERSONALAUFWAND_JAEHRLICH_EINZELNE_STANDARDAKTIVITAETEN: {
      zeitwerteLabel: 'Durchschnittliche Zeitwerte der Standardaktivitäten einblenden',
      tableHeaders: {
        bezeichnung: 'Bezeichnung',
        bemerkungen: 'Bemerkungen',
        zeitaufwandProFallGroup: 'Zeitaufwand pro Fall',
        zeitaufwandBisher: 'Bisher',
        zeitaufwandNeu: 'Neu/ Absolut',
        zeitaufwandDifferenz: 'Differenz',
        standardlohnsatzGroup: 'Standardlohnsatz Ø',
        personalaufwandProFall: 'Personalaufwand pro Fall',
      },
    },
    PERSONALAUFWAND_EINMALIG_GESAMTAUFWAND_PRO_FALL: {
      defaultBezeichnung: 'Gesamtaufwand pro Fall',
      categoryTitle: '3. Kosten einer Kategorie zuweisen',
      chooseCategory: 'Kategorie wählen',
      categoryRequiredMessage: 'Bitte wählen Sie eine Kategorie aus.',
      categoryOptions: {
        anpassungOrganisationsstrukturen: 'Anpassung von Organisationsstrukturen',
        digitalerProzessablaeufe: 'Einführung oder Anpassung digitaler Prozessabläufe',
        maschinenAnlagenGebaeude: 'Anschaffung oder Nachrüstung von Maschinen, Anlagen, Gebäuden',
        produkteFertigungsprozesseBeschaffungswesen:
          'Anpassung von Produkten, Fertigungsprozessen und Beschaffungswesen',
        informationspflicht: 'Einmalige Informationspflicht',
        schulungskosten: 'Schulungskosten',
        sonstiges: 'Sonstiges',
        placeHolder: 'Bitte auswählen ',
      },
      tableHeaders: {
        zeitaufwandProFallGroup: 'Zeitaufwand pro Fall',
        zeitaufwandBisher: 'Bisher',
        zeitaufwandNeu: 'Neu/ Absolut',
        zeitaufwandDifferenz: 'Differenz',
        bezeichnung: 'Bezeichnung',
        bemerkungen: 'Bemerkungen',
        standardlohnsatzGroup: 'Standardlohnsatz Ø',
        personalaufwandProFallGroup: 'Personalaufwand pro Fall',
      },
    },
    SACHAUFWAND_JAEHRLICH_GESAMTAUFWAND_PRO_FALL: {
      tableHeaders: {
        bezeichnung: 'Kategorie des Sachaufwands',
        bemerkungen: 'Bemerkungen',
        sachAufwandProFallGroup: 'Sachaufwand pro Fall',
        sachaufwandBisher: 'Bisher',
        sachaufwandNeu: 'Neu/ Absolut',
        sachaufwandDifferenz: 'Differenz',
      },
      zeileKategorien: {
        aufwandbeschaffung: 'Aufwand für die Beschaffung von Informations- und Kommunikationstechnik',
        anschaffungKosten: 'Kosten für sonstige Anschaffung',
        aufwandAnlagen: 'Aufwand für die Nachrüstung von Anlagen',
        wartungsAufwand: 'Wartungsaufwand',
        aufwandInanspruchnahme: 'Aufwendungen für die Inanspruchnahme Dritter',
        fortbildungen: 'Fortbildungen und Informationsmaterial für Dritte',
        sachmittel: 'Sachmittel (z.B. Büromaterial, Porto)',
        sonstigeSachkosten: 'Sonstige Sachkosten',
        wegesachkosten: 'Wegesachkosten',
      },
    },
    SACHAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN: {
      tableHeaders: {
        bezeichnung: 'Kategorie des Sachaufwands',
        bemerkungen: 'Bemerkungen',
        sachAufwandProFall: 'Sachaufwand pro Fall',
      },
      zeileKategorien: {
        aufwandbeschaffung: 'Aufwand für die Beschaffung von Informations- und Kommunikationstechnik',
        anschaffungKosten: 'Kosten für sonstige Anschaffung',
        aufwandAnlagen: 'Aufwand für die Nachrüstung von Anlagen',
        wartungsAufwand: 'Wartungsaufwand',
        aufwandInanspruchnahme: 'Aufwendungen für die Inanspruchnahme Dritter',
        fortbildungen: 'Fortbildungen und Informationsmaterial für Dritte',
        sachmittel: 'Sachmittel (z.B. Büromaterial, Porto)',
        sonstigeSachkosten: 'Sonstige Sachkosten',
        wegesachkosten: 'Wegesachkosten',
      },
    },
    PERSONEN_TAGE_MONATE_VERWALTUNG: {
      tableHeaders: {
        bezeichnung: 'Laufbahngruppe',
        bemerkungen: 'Bemerkungen',
        einheit: 'Einheit zur Berechnung',
        anzahlGroup: 'Anzahl',
        anzahlBisher: 'Bisher',
        anzahlNeu: 'Neu/ Absolut',
        anzahlDifferenz: 'Differenz',
        vollzugsebene: 'Vollzugsebene',
        lohnkosten: 'Lohnkosten',
        personalaufwandProFall: 'Personalaufwand pro Fall',
      },
      einmalig: {
        tableHeaders: {
          anzahlDifferenz: 'Anzahl',
        },
      },
      vollzugsebeneSelect: {
        itemA: 'Bund',
        itemB: 'Länder',
        itemC: 'Kommunen',
        itemD: 'Sozialversicherung',
        itemE: 'Durchschnitt (Vollzugsebene unbekannt)',
      },
      einheitSelect: {
        PERSONENMONATE_PM: 'Personenmonate (PM)',
        PERSONENTAGE_PT: 'Personentage (PT)',
        label: {
          PERSONENMONATE_PM: 'PM',
          PERSONENTAGE_PT: 'PT',
        },
      },
      staticData: {
        einfacherDienst: 'Mitarbeiter/Mitarbeiterinnen im einfachen Dienst',
        mittlererDienst: 'Mitarbeiter/Mitarbeiterinnen im mittleren Dienst',
        gehobenerDienst: 'Mitarbeiter/Mitarbeiterinnen im gehobenen Dienst',
        hoehererDienst: 'Mitarbeiter/Mitarbeiterinnen im höheren Dienst',
        durchschnitt: 'Durchschnitt (Laufbahngruppe unbekannt)',
      },
      summaryRows: {
        summeRow: 'Summe',
        arbeitszeitInStunden: 'Arbeitszeit in Stunden',
        gewichteterLohnsatz: 'Gewichteter Lohnsatz',
      },
    },
    PERSONEN_TAGE_MONATE_WIRTSCHAFT: {
      tableHeaders: {
        bezeichnung: 'Qualifikationsniveau',
        bemerkungen: 'Bemerkungen',
        einheit: 'Einheit zur Berechnung',
        anzahlGroup: 'Anzahl',
        anzahlBisher: 'Bisher',
        anzahlNeu: 'Neu/ Absolut',
        anzahlDifferenz: 'Differenz',
        vollzugsebene: 'Wirtschaftszweig',
        lohnkosten: 'Lohnkosten',
        personalaufwandProFall: 'Personalaufwand pro Fall',
      },
      einmalig: {
        tableHeaders: {
          anzahlDifferenz: 'Anzahl',
        },
      },
      vollzugsebeneSelect: {
        itemA: 'A-S Gesamtwirtschaft',
        itemAL: 'A Land- und Forstwirtschaft, Fischerei',
        itemB: 'B Bergbau und Gewinnung von Steinen und Erden',
        itemC: 'C Verarbeitendes Gewerbe',
        itemD: 'D Energieversorgung',
        itemE: 'E Wasserversorgung Abwasser- und Abfallentsorgung und Beseitigung von Umweltverschmutzungen',
        itemF: 'F Baugewerbe',
        itemG: 'G Handel, Instandhaltung und Reparatur von Kraftfahrzeugen',
        itemH: 'H Verkehr und Lagerei',
        itemI: 'I Gastgewerbe',
        itemJ: 'J Information und Kommunikation',
        itemK: 'K Erbringung von Finanz- und Versicherungsdienstleistungen',
        itemL: 'L Grundstücks- und Wohnungswesen',
        itemM: 'M Erbringung von freiberuflichen, wissenschaftlichen und technischen Dienstleistungen',
        itemN: 'N Erbringung von sonstigen wirtschaftlichen Dienstleistungen',
        itemP: 'P Erziehung und Unterricht',
        itemQ: 'Q Gesundheits- und Sozialwesen',
        itemR: 'R Kunst, Unterhaltung und Erholung',
        itemS: 'S Erbringung von sonstigen Dienstleistungen',
      },
      einheitSelect: {
        PERSONENMONATE_PM: 'Personenmonate (PM)',
        PERSONENTAGE_PT: 'Personentage (PT)',
        label: {
          PERSONENMONATE_PM: 'PM',
          PERSONENTAGE_PT: 'PT',
        },
      },
      staticData: {
        niedrig: 'Niedrig',
        mittel: 'Mittel',
        hoch: 'Hoch',
        durchschnitt: 'Durchschnitt',
      },
      summaryRows: {
        summeRow: 'Summe',
        arbeitszeitInStunden: 'Arbeitszeit in Stunden',
        gewichteterLohnsatz: 'Gewichteter Lohnsatz',
      },
    },
    PERSONALAUFWAND_EINMALIG_EINZELNE_STANDARDAKTIVITAETEN: {
      zeitwerteLabel: 'Durchschnittliche Zeitwerte der Standardaktivitäten einblenden',
      tableHeaders: {
        bezeichnung: 'Bezeichnung',
        bemerkungen: 'Bemerkungen',
        zeitaufwandBisher: 'Bisher',
        zeitaufwandNeu: 'Neu/ Absolut',
        zeitaufwandDifferenz: 'Differenz',
        zeitaufwandProFall: 'Zeitaufwand pro Fall',
        standardlohnsatzGroup: 'Standardlohnsatz Ø',
        personalaufwandProFall: 'Personalaufwand pro Fall',
      },
    },
    wirtschaftStandardActivities: {
      vorgabe: {
        informationspflicht: {
          bezeichnung: 'Einarbeitung in die Informationspflicht',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        datenBeschaffung: {
          bezeichnung: 'Beschaffung von Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        formulare: {
          bezeichnung: 'Formulare ausfüllen, Beschriftung, Kennzeichnung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>30 Min.</span></span>',
        },
        berechnungen: {
          bezeichnung: 'Berechnungen durchführen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>240 Min.</span></span>',
        },
        euberpreufung: {
          bezeichnung: 'Überprüfung der Daten und Eingaben',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        fehlerkorrektur: {
          bezeichnung: 'Fehlerkorrektur',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        datenAufbereitung: {
          bezeichnung: 'Aufbereitung der Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>268 Min.</span></span>',
        },
        dateneubermittlung: {
          bezeichnung: 'Datenübermittlung und Veröffentlichung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>5 Min.</span></span>',
        },
        interneSitzungen: {
          bezeichnung: 'Interne Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>600 Min.</span></span>',
        },
        externeSitzungen: {
          bezeichnung: 'Externe Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>10 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        zahlungsanweisungen: {
          bezeichnung: 'Ausführen von Zahlungsanweisungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>3 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>18 Min.</span></span>',
        },
        kopieren: {
          bezeichnung: 'Kopieren, Archivieren, Verteilen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>12 Min.</span></span>',
        },
        eoffentlicheStellen: {
          bezeichnung: 'Prüfung durch öffentliche Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>43 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        korrekturen: {
          bezeichnung: 'Korrekturen, die aufgrund von Prüfungen durchgeführt werden müssen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>4 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>30 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        informationsbeschaffung: {
          bezeichnung: 'Weitere Informationsbeschaffung, im Falle von Schwierigkeiten mit den zuständigen Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>15 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        fortbildungs: {
          bezeichnung: 'Fortbildungs- und Schulungsteilnahmen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>22 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        warenBeschaffen: {
          bezeichnung: 'Beschaffen von Waren und Sachleistungen',
        },
        dienstleistungenBeschaffen: {
          bezeichnung: 'Beschaffen von Dienstleistungen',
        },
        eigenenLeistungen: {
          bezeichnung: 'Erbringen von eigenen Leistungen',
        },
        internenProzessen: {
          bezeichnung: 'Anpassen von internen Prozessen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>300 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>1080 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>3280 Min.</span></span>',
        },
        euberwachungsmassnahmen: {
          bezeichnung: 'Überwachungsmaßnahmen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>30 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>240 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>4254 Min.</span></span>',
        },
        lagerhaltung: {
          bezeichnung: 'Lagerhaltung',
        },
        wartezeiten: {
          bezeichnung: 'Wege- und Wartezeiten',
        },
      },
      informationspflicht: {
        informationspflicht: {
          bezeichnung: 'Einarbeitung in die Informationspflicht',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        datenBeschaffung: {
          bezeichnung: 'Beschaffung von Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        formulare: {
          bezeichnung: 'Formulare ausfüllen, Beschriftung, Kennzeichnung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>30 Min.</span></span>',
        },
        berechnungen: {
          bezeichnung: 'Berechnungen durchführen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>240 Min.</span></span>',
        },
        euberpreufung: {
          bezeichnung: 'Überprüfung der Daten und Eingaben',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        fehlerkorrektur: {
          bezeichnung: 'Fehlerkorrektur',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        datenAufbereitung: {
          bezeichnung: 'Aufbereitung der Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>268 Min.</span></span>',
        },
        dateneubermittlung: {
          bezeichnung: 'Datenübermittlung und Veröffentlichung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>5 Min.</span></span>',
        },
        interneSitzungen: {
          bezeichnung: 'Interne Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>600 Min.</span></span>',
        },
        externeSitzungen: {
          bezeichnung: 'Externe Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>10 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        zahlungsanweisungen: {
          bezeichnung: 'Ausführen von Zahlungsanweisungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>3 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>18 Min.</span></span>',
        },
        kopieren: {
          bezeichnung: 'Kopieren, Archivieren, Verteilen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>12 Min.</span></span>',
        },
        eoffentlicheStellen: {
          bezeichnung: 'Prüfung durch öffentliche Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>43 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        korrekturen: {
          bezeichnung: 'Korrekturen, die aufgrund von Prüfungen durchgeführt werden müssen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>4 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>30 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        informationsbeschaffung: {
          bezeichnung: 'Weitere Informationsbeschaffung, im Falle von Schwierigkeiten mit den zuständigen Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>15 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        fortbildungs: {
          bezeichnung: 'Fortbildungs- und Schulungsteilnahmen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>22 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
      },
      einmalig: {
        informationspflicht: {
          bezeichnung: 'Einarbeitung in die Informationspflicht',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>60 Min.</span></span>',
        },
        datenBeschaffung: {
          bezeichnung: 'Beschaffung von Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>120 Min.</span></span>',
        },
        formulare: {
          bezeichnung: 'Formulare ausfüllen, Beschriftung, Kennzeichnung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>30 Min.</span></span>',
        },
        berechnungen: {
          bezeichnung: 'Berechnungen durchführen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>240 Min.</span></span>',
        },
        euberpreufung: {
          bezeichnung: 'Überprüfung der Daten und Eingaben',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>60 Min.</span></span>',
        },
        fehlerkorrektur: {
          bezeichnung: 'Fehlerkorrektur',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>60 Min.</span></span>',
        },
        datenAufbereitung: {
          bezeichnung: 'Aufbereitung der Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>268 Min.</span></span>',
        },
        dateneubermittlung: {
          bezeichnung: 'Datenübermittlung und Veröffentlichung',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>5 Min.</span></span>',
        },
        interneSitzungen: {
          bezeichnung: 'Interne Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>600 Min.</span></span>',
        },
        externeSitzungen: {
          bezeichnung: 'Externe Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>10 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>480 Min.</span></span>',
        },
        zahlungsanweisungen: {
          bezeichnung: 'Ausführen von Zahlungsanweisungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>3 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>18 Min.</span></span>',
        },
        kopieren: {
          bezeichnung: 'Kopieren, Archivieren, Verteilen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>12 Min.</span></span>',
        },
        eoffentlicheStellen: {
          bezeichnung: 'Prüfung durch öffentliche Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>43 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>480 Min.</span></span>',
        },
        korrekturen: {
          bezeichnung: 'Korrekturen, die aufgrund von Prüfungen durchgeführt werden müssen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>4 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>30 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>480 Min.</span></span>',
        },
        informationsbeschaffung: {
          bezeichnung: 'Weitere Informationsbeschaffung, im Falle von Schwierigkeiten mit den zuständigen Stellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>15 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>120 Min.</span></span>',
        },
        fortbildungs: {
          bezeichnung: 'Fortbildungs- und Schulungsteilnahmen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>22 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>480 Min.</span></span>',
        },
        warenBeschaffen: {
          bezeichnung: 'Beschaffen von Waren und Sachleistungen',
        },
        dienstleistungenBeschaffen: {
          bezeichnung: 'Beschaffen von Dienstleistungen',
        },
        eigenenLeistungen: {
          bezeichnung: 'Erbringen von eigenen Leistungen',
        },
        internenProzessen: {
          bezeichnung: 'Anpassen von internen Prozessen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>300 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>1080 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>3.280 Min.</span></span>',
        },
        euberwachungsmassnahmen: {
          bezeichnung: 'Überwachungsmaßnahmen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand:</span><span>30 Min.</span></span><span><span>Mittlerer Zeitaufwand:</span><span>240 Min.</span></span><span><span>Hoher Zeitaufwand:</span><span>4.254 Min.</span></span>',
        },
        lagerhaltung: {
          bezeichnung: 'Lagerhaltung',
        },
        wartezeiten: {
          bezeichnung: 'Wege- und Wartezeiten',
        },
      },
    },
    verwaltungStandardActivities: {
      vorgabe: {
        vorgabeEinarbeiten: {
          bezeichnung: 'Einarbeiten in die Vorgabe',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>35 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>413 Min.</span></span>',
        },
        beraten: {
          bezeichnung: 'Beraten, Vorgespräche führen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>4 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>30 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>573 Min.</span></span>',
        },
        formelle: {
          bezeichnung: 'Formelle Prüfung, Daten sichten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>30 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>90 Min.</span></span>',
        },
        fehlenderDaten: {
          bezeichnung: 'Eingang bestätigen oder Einholen fehlender Daten',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>20 Min.</span></span>',
        },
        datenErfassen: {
          bezeichnung: 'Inhaltliche Prüfung, Daten erfassen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>8 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>792 Min.</span></span>',
        },
        berechnungenDurchfeuhren: {
          bezeichnung: 'Berechnungen durchführen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>9 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        ergebnisseBerechnungen: {
          bezeichnung: 'Ergebnisse/Berechnungen überprüfen und ggf. korrigieren',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>4 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>3.030 Min.</span></span>',
        },
        interneSitzungen: {
          bezeichnung: 'Interne Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>2.120 Min.</span></span>',
        },
        externeSitzungen: {
          bezeichnung: 'Externe Sitzungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>2.460 Min.</span></span>',
        },
        datenEubermitteln: {
          bezeichnung: 'Daten übermitteln oder veröffentlichen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>60 Min.</span></span>',
        },
        informationenAufbereiten: {
          bezeichnung: 'Abschließende Informationen aufbereiten, Bescheid erstellen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>5 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        zahlungenAnweisen: {
          bezeichnung: 'Zahlungen anweisen, annehmen oder überwachen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>18 Min.</span></span>',
        },
        korrektur: {
          bezeichnung: 'Korrektur bzw. weitere Informationen bei Rückfragen vorlegen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>30 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>60 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>120 Min.</span></span>',
        },
        kopieren: {
          bezeichnung: 'Kopieren, archivieren, verteilen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>10 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>20 Min.</span></span>',
        },
        fortbildungen: {
          bezeichnung: 'Fortbildungen und Schulungen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>3 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        euberwachungs: {
          bezeichnung: 'Überwachungs- und Aufsichtsmaßnahmen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>8 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>207 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>56.220 Min.</span></span>',
        },
        warenBeschaffen: {
          bezeichnung: 'Beschaffen von Waren und Sachleistungen',
        },
        dienstleistungenBeschaffen: {
          bezeichnung: 'Beschaffen von Dienstleistungen',
        },
        anpassen: {
          bezeichnung: 'Anpassen von internen Prozessen',
        },
        Lagerhaltung: {
          bezeichnung: 'Lagerhaltung',
        },
        wartezeiten: {
          bezeichnung: 'Wege- und Wartezeiten',
        },
      },
    },
    buergerStandardActivities: {
      vorgabe: {
        verpflichtung: {
          bezeichnung: 'Sich mit der gesetzlichen Verpflichtung vertraut machen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>15 Min.</span></span>',
        },
        beratung: {
          bezeichnung: 'Fachliche Beratung in Anspruch nehmen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>6 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>20 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>15 Min.</span></span>',
        },
        informationenZusammenstellen: {
          bezeichnung: 'Informationen oder Daten sammeln und zusammenstellen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>68 Min.</span></span>',
        },
        informationenDaten: {
          bezeichnung: 'Informationen oder Daten aufbereiten (inkl. Berechnungen und Überprüfungen durchführen)',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>10 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>480 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>480 Min.</span></span>',
        },
        formulare: {
          bezeichnung: 'Formulare ausfüllen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>15 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>960 Min.</span></span>',
        },
        schriftstuecke: {
          bezeichnung: 'Schriftstücke aufsetzen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>10 Min.</span></span>',
        },
        uebermitteln: {
          bezeichnung: 'Informationen oder Daten an die zuständigen Stellen übermitteln ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>96 Min.</span></span>',
        },
        zahlungsanweisungen: {
          bezeichnung: 'Ausführen von Zahlungsanweisungen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>2 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>3 Min.</span></span>',
        },
        unterlagenKopieren: {
          bezeichnung: 'Unterlagen kopieren, abheften, abspeichern',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>1 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>3 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>240 Min.</span></span>',
        },
        oeffentlicheStellen: {
          bezeichnung: 'Prüfung durch öffentliche Stellen durchführen lassen',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>3 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>23 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>140 Min.</span></span>',
        },
        weitererInformationen: {
          bezeichnung: 'Vorlage weiterer Informationen bei Behörden im Falle von Rückfragen ',
          zeitwerte:
            '<span><span>Einfacher Zeitaufwand :</span><span>2 Min.</span></span><span><span>Mittlerer Zeitaufwand :</span><span>5 Min.</span></span><span><span>Hoher Zeitaufwand :</span><span>15 Min.</span></span>',
        },
        schulungen: { bezeichnung: 'Teilnahme an Schulungen, Unterrichtungen etc. ' },
        materialbeschaffen: { bezeichnung: 'Material beschaffen' },
        bestimmteLeistung: { bezeichnung: 'Bestimmte Leistung selbst erbringen oder Dritte beauftragen' },
        umsetzungVorgaben: { bezeichnung: 'Umsetzung von Vorgaben überprüfen (z.B. bei Leistung durch Dritte)' },
      },
    },
  },
  buerger: {
    linkName: 'Bürgerinnen und Bürger',
  },
  wirtschaft: {
    linkName: 'Wirtschaft',
  },
  verwaltung: {
    linkName: 'Verwaltung',
  },
  zusammenfassung: {
    noImpact: 'Keine Auswirkungen',
    linkName: 'Zusammenfassung',
    pflichtfelder: 'Pflichtfelder sind mit einem * gekennzeichnet.',
    description:
      '<p>Unten finden Sie Ihre Ausarbeitungen zu den einzelnen Normadressaten.</p><p>Fassen Sie diese Ausarbeitungen unten noch einmal kurz für das Vorblatt zusammen.</p><p>Im nächsten Schritt können Sie dann die Inhalte für den Begründungsteil erfassen.</p><p>Abschließend können Sie diese Inhalte in Ihr Dokument übernehmen.</p>',
    algemeineangaben: {
      null: 'Keine Angaben',
      linkName: 'Allgemeine Angaben und Vorblatt',
      title: 'Zusammenfassung: Allgemeine Angaben und Vorblatt',
      ausarbeitungen: 'Allgemeine Angaben zu den Ausarbeitungen',
      anfertigendeWaehlen: 'Anfertigende Stelle wählen*',
      DESTATIS: 'Statistisches Bundesamt (Destatis)',
      RESSORT: 'Ressort',
      umgesetztenTatbestandTitle: 'Umgesetzten Tatbestand wählen',
      umgesetztenTatbestandPlaceHolder: 'Bitte auswählen',
      umgesetztenTatbestandOptions: {
        null: 'Keine Angaben',
        GOLPLATING_VON_EU_RECHT: 'Goldplating von EU-Recht',
        EINS_ZU_EINS_UMSETZUNG_VON_EU_RECHT: '1:1-Umsetzung von EU-Recht',
        RECHTSPRECHUNG_BVERFG: 'Rechtsprechung BVerfG',
        RECHTSPRECHUNG_EUGH: 'Rechtsprechung EuGH',
        ABWEHR_ERHEBLICHER_GEFAHREN: 'Abwehr erheblicher Gefahren',
        UMSETZUNG_INTERNATIONALER_VERTRAEGE: 'Umsetzung internationaler Verträge',
        KEIN_BESONDERER_TATBESTAND: 'Kein besonderer Tatbestand',
      },
      eZusammenfassung: 'E. Erfüllungsaufwand',
      zusammenfassungVorblatt: 'Zusammenfassung für das Vorblatt',
      zusammenfassungVorblattRequired: 'Bitte geben Sie die Zusammenfassung für das Vorblatt an.',
      umgesetzterTatbestandRequired: 'Bitte wählen Sie den umgesetzten Tatbestand.',
    },
    inhalteVorblatt: {
      title: 'Vorblatt',
      buergerTitle: 'E. 1 Erfüllungsaufwand für Bürgerinnen und Bürger',
      wirtschaftTitle: 'E. 2 Erfüllungsaufwand für die Wirtschaft',
      verwaltungTitle: 'E. 3 Erfüllungsaufwand für die Verwaltung',
      zurBearbeitung: 'Zur Bearbeitung',
      davonBuerokratiekosten: 'davon Bürokratiekosten',
      bundesebene: 'davon Bundesebene',
      landesebene: 'davon Landesebene',
      table: {
        th: {
          bezeichnung: 'Bezeichnung',
          BUERGER: {
            jaehrlichenPersonalaufwands: 'Jährlicher Zeitaufwand',
            jaehrlichenSachaufwands: 'Jährlicher Sachaufwand',
            einmaligerPersonalaufwand: 'Einmaliger Zeitaufwand',
            einmaligerSachaufwand: 'Einmaliger Sachaufwand',
          },
          WIRTSCHAFT: {
            jaehrlichenPersonalaufwands: 'Jährlicher Personalaufwand',
            jaehrlichenSachaufwands: 'Jährlicher Sachaufwand',
            einmaligerPersonalaufwand: 'Einmaliger Personalaufwand',
            einmaligerSachaufwand: 'Einmaliger Sachaufwand',
          },
          VERWALTUNG: {
            jaehrlichenPersonalaufwands: 'Jährlicher Personalaufwand',
            jaehrlichenSachaufwands: 'Jährlicher Sachaufwand',
            einmaligerPersonalaufwand: 'Einmaliger Personalaufwand',
            einmaligerSachaufwand: 'Einmaliger Sachaufwand',
          },
          actions: 'Aktionen',
        },
      },
    },
    begruendungsteil: {
      linkName: 'Begründungsteil',
      title: 'Zusammenfassung: Begründungsteil',
      intro:
        'Unten finden Sie Ihre Ausarbeitungen zu den einzelnen Normadressaten. Fassen Sie diese Ausarbeitungen unten noch einmal kurz für das Vorblatt zusammen. Im nächsten Schritt können Sie dann die Inhalte für den Begründungsteil erfassen. Abschließend können Sie diese Inhalte in Ihr Dokument übernehmen.',
      inhaltTitle: 'Begründung - Allgemeiner Teil',
      inhaltText:
        'Die Gliederungsebenen der folgenden Überschriften entsprechen den Gliederungsebenen, die auch im allgemeinen Teil der Begründung des Regelungsentwurfs benutzt werden.',
      BUERGER: {
        title: 'A. VI. 4. a) Erfüllungsaufwand für Bürgerinnen und Bürger',
        jaehrlichenTitle: 'Veränderung des jährlichen Aufwandes für Bürgerinnen und Bürger',
        einmaligerTitle: 'Einmaliger Aufwand für Bürgerinnen und Bürger',
      },
      VERWALTUNG: {
        title: 'A. VI. 4. c) Erfüllungsaufwand für die Verwaltung',
        jaehrlichenTitle: 'Veränderung des jährlichen Aufwandes für die Verwaltung',
        einmaligerTitle: 'Einmaliger Aufwand für die Verwaltung',
      },
      WIRTSCHAFT: {
        title: 'A. VI. 4 b) Erfüllungsaufwand für die Wirtschaft',
        jaehrlichenTitle: 'Veränderung des jährlichen Aufwandes für die Wirtschaft (nur weitere Vorgaben)',
        einmaligerTitle: 'Einmaliger Aufwand für die Wirtschaft',
        informationspflichtenTitle: 'Bürokratiekosten aus Informationspflichten (ohne weitere Vorgaben)',
      },
      vorhabenList: {
        tableHeading: {
          bezeichnung: 'Bezeichnung',
          personalaufwand: 'Personalaufwand gesamt',
          sachaufwand: 'Sachaufwand gesamt',
          gesamtaufwand: 'Gesamtaufwand',
        },
        tableHeadingBuerger: {
          bezeichnung: 'Bezeichnung',
          personalaufwand: 'Zeitaufwand gesamt in Std.',
          sachaufwand: 'Sachaufwand gesamt',
        },
        tableHeadingINFORMATIONSPFLICHT: {
          bezeichnung: 'Bezeichnung',
          personalaufwand: 'Jährlicher Personalaufwand gesamt',
          sachaufwand: 'Jährlicher Sachaufwand gesamt',
          einmaligerPersonalaufwand: 'Einmaliger Personalaufwand gesamt',
          einmaligerSachaufwand: 'Einmaliger Sachaufwand gesamt',
          gesamtaufwand: 'Gesamtaufwand',
        },
      },
      summaryTextarea: {
        title: 'Zusammenfassung',
        BUERGER: {
          label: 'Zusammenfassung: Normadressat Bürgerinnen und Bürger',
          defaultText:
            'Für die Bürgerinnen und Bürger entsteht ein jährlicher Zeitaufwand in Höhe von {{jaehrlcherZeitaufwand}}. Es entsteht dazu einmaliger Zeitaufwand in Höhe von {{einmaligerZeitaufwand}}. \nEs entsteht außerdem jährlicher Sachaufwand in Höhe von {{jaehrlicherSachaufwand}} sowie einmaliger Sachaufwand in Höhe von {{einmaligerSachaufwand}}.',
        },
        WIRTSCHAFT: {
          label: 'Zusammenfassung: Normadressat Wirtschaft',
          defaultText:
            'Für die Wirtschaft entsteht jährlicher Personalaufwand in Höhe von {{jaehrlicherPersonalaufwand}}, davon entfallen auf Bürokratiekosten aus Informationspflichten {{jaehrlicherPersonalaufwandBuerokratiekosten}}.\nEs entsteht einmaliger Personalaufwand in Höhe von {{einmaligerPersonalaufwand}}. \n\nZudem entstehen jährliche Sachkosten in Höhe von {{jaehrlicherSachaufwand}}, davon {{jaehrlicherSachaufwandInformationspflicht}} aus Informationspflichten. Es entsteht einmaliger Sachaufwand in Höhe von {{einmaligerSachaufwand}}.',
        },
        VERWALTUNG: {
          label: 'Zusammenfassung: Normadressat Verwaltung',
          defaultText:
            'Für die Verwaltung entsteht jährlicher Sachaufwand in Höhe von {{jaehrlicherPersonalaufwand}}, davon entfallen auf die Bundesebene {{bundesebeneJaehrlicherPersonalaufwand}} und auf die Landesebene {{landesebeneJaehrlicherPersonalaufwand}}. \nEs entsteht außerdem einmaliger Sachaufwand in Höhe von {{einmaligerPersonalaufwand}}, davon entfallen auf die Bundesebene {{bundesebeneEinmaligerPersonalaufwand}} und auf die Landesebene {{landesebeneEinmaligerPersonalaufwand}}. \nDiese Kosten beinhalten ebenfalls den Umstellungsaufwand.',
        },
        resetButton: 'Auf Textvorschlag zurücksetzen',
      },
      zusammenfassungBegruendungsteil: {
        title: 'A. VI. 4 Erfüllungsaufwand',
        label: 'Zusammenfassung für den Begründungsteil',
      },
    },
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    generalInfo: {
      title: 'Allgemeine Informationen',
      anfertigungTitle: 'Anfertigung: ',
      umgesetzterTitle: 'Umgesetzter Tatbestand: ',
    },
    gesamtsummenTables: {
      BUERGER: 'Gesamtsummen des Erfüllungsaufwandes für Bürgerinnen und Bürger',
      VERWALTUNG: 'Gesamtsummen des Erfüllungsaufwands für die Verwaltung',
      WIRTSCHAFT: 'Gesamtsummen des Erfüllungsaufwands für die Wirtschaft',
    },
  },
  contactPerson: de_contacts.erfuellungsaufwand.contactPerson,
};
