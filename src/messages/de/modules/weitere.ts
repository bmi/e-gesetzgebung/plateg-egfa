// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_weitere = {
  name: 'Weitere wesentliche Auswirkungen',
  einleitung: {
    linkName: 'Einführung',
    title: 'Weitere wesentliche Auswirkungen',
    text: `<p>In diesem Modul beschreiben Sie die weiteren wesentlichen Auswirkungen Ihres Regelungsvorhabens nach <a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank" style="white-space: normal">§ 44 Absatz 1 GGO</a> , die nicht durch andere Module der Gesetzesfolgenabschätzung abgedeckt sind.</p>
          <p>Anschließend können Sie die Beschreibung für Vorblatt und Begründung Ihres Regelungsvorhabens verwenden.</p>`,
  },
  wesentlicheAuswirkungen: {
    linkName: 'Wesentliche Auswirkungen',
    title: 'Wesentliche Auswirkungen',
    question:
      'Sind weitere wesentliche Auswirkungen durch das Regelungsvorhaben zu erwarten (nach <a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank" style="white-space: normal">§ 44 Abs. 1 oder § 44 Abs. 6 GGO</a>), die noch nicht durch andere Module erfasst wurden?',
    additionalExplanation: 'Zusammenfassung der Auswirkungen',
    drawerText:
      'Die Angaben auf dieser Seite können im Anschluss an die Fertigstellung in den Begründungsteil/ in das Vorblatt des Regelungsentwurfs übernommen werden.',
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    subtitle: 'Weitere wesentliche Auswirkungen',
    noImpact: 'Es sind keine weiteren wesentlichen Auswirkungen zu erwarten.',
    impact: 'Es sind folgende weitere wesentliche Auswirkungen zu erwarten:',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung zur Darstellung der weiteren wesentlichen Auswirkungen in die Begründung des Regelungsentwurfs übernehmen.',
  },
  zusammenfassung: {
    noImpact: 'Es sind keine weiteren wesentlichen Auswirkungen zu erwarten.',
  },
  contactPerson: de_contacts.weitere.contactPerson,
};
