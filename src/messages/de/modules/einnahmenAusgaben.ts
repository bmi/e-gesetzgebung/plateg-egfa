// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de_contacts } from '../contacts';

export const de_einnahmenAusgaben = {
  name: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
  einleitung: {
    linkName: 'Einführung',
    title: 'Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte',
    content: {
      section1: `<p>Gemäß <a href="{{basePath}}/arbeitshilfen/download/34#page=34" target="_blank" style="white-space: normal">§ 44 Absatz 2 GGO</a> sind sämtliche Auswirkungen auf die Einnahmen und Ausgaben der öffentlichen Haushalte (im Folgenden: haushalterische Auswirkungen), die auf den beabsichtigten Rechtsetzungsakt zurückzuführen sind, möglichst detailliert darzustellen. Allgemeine Vorgaben hierzu hat das Bundesministerium der Finanzen (BMF) im Benehmen mit dem Bundesministerium des Innern und für Heimat mit <a href="{{basePath}}/arbeitshilfen/download/8" target="_blank" style="white-space: normal">Rundschreiben vom 14. März 2019</a> gemacht. Die wesentlichen Gesichtspunkte sind nachstehend noch einmal beschrieben und teils näher erläutert:</p>
            <p>Öffentliche Haushalte sind die Haushalte des Bundes, der Länder und der Gemeinden (Gemeindeverbände). Bei der Darstellung der haushalterischen Auswirkungen ist nach diesen Gebietskörperschaften zu differenzieren.</p>
            <p>Aufzunehmen sind ausschließlich solche Folgen einer Regelung, die sich haushalterisch, d. h. in den Einnahmen und Ausgaben auswirken, nicht sämtliche Kosten. Insofern unterscheiden sich insbesondere die haushalterischen Auswirkungen des Verwaltungsvollzugs unter Buchstabe D. vom Erfüllungsaufwand unter Buchstabe E. des Vorblatts (bzw. an entsprechender Stelle in der Begründung). Die Angaben können insbesondere deshalb voneinander abweichen, weil eventuelle Personalmehrausgaben des Bundes auf der Basis eines eventuellen Mehrbedarfs an Stellen zu errechnen sind und sich die vom BMF dafür zur Verfügung gestellten Tabellen von den Personalkostensätzen des Statistischen Bundesamts unterscheiden.</p>`,
      hiddenSection: {
        title: 'Ergänzende Hinweise zum Kostenbegriff',
        text: `<p>Den Ausgangspunkt der Betrachtung der Folgen einer Regelung bildet der betriebswirtschaftliche Kostenbegriff. Danach sind Kosten der bewertete periodisierte Verzehr von Gütern und Dienstleistungen, den die geplante Maßnahme verursacht. Dieser Kostenbegriff umfasst haushaltswirksame und nicht haushaltswirksame Bestandteile. Unter Buchstabe D. des Vorblatts und an entsprechender Stelle in der Begründung sind jedoch ausschließlich haushaltswirksame Kostenbestandteile zu berücksichtigen, d. h. es sind diejenigen Kosten auszuweisen, die zu (Mehr-/Minder-)Ausgaben oder (Mehr-/Minder-)Einnahmen führen. D. h. auch, dass z. B. kalkulatorische Kosten, wie Abschreibungen von Vermögensgegenständen oder auch kalkulatorische Zinsen auf gebundenes Vermögen, nicht zu berücksichtigen sind, weil sie keine Auswirkungen auf die Einnahmen oder die Ausgaben der öffentlichen Haushalte haben. Einnahmen und Ausgaben (tatsächliche Zahlungsflüsse) fallen in diesen Fällen mit den Kosten auseinander.</p>`,
      },
      section2: `<p>Darzustellen sind die sich aus den Maßnahmen unmittelbar oder mittelbar ergebenden Einnahmen (z. B. auch Gebühren, Geldbußen), ggf. differenziert nach Einnahmeart, und Ausgaben (Sachausgaben, Personalausgaben). Die Ausgaben sind unabhängig davon aufzuführen, ob sie durch die geplanten Maßnahmen selbst (Veränderungen bei Zweckausgaben) oder durch den die Maßnahmen begleitenden Verwaltungsvollzug hervorgerufen werden. Auch die haushalterischen Auswirkungen für die Verwaltung als Normadressatin oder Normadressat (z. B. Halterin oder Halter von Kfz, Arbeitgeberin oder Arbeitgeber außerhalb des Beamtenrechts oder Bauherrin oder Bauherr) sowie ggf. zu leistende Abgaben sind als Teil der haushalterischen Auswirkungen des Verwaltungsvollzugs anzugeben. Es ist eine getrennte Aufführung dieser Positionen vorzunehmen.</p>
            <p>Im Begründungsteil ist an entsprechender Stelle ausführlich zu beschreiben, wie sich Einnahmen/Ausgaben (einschließlich Personalbedarfe) zusammensetzen und worauf sich ihre Ermittlung stützt. Dabei muss erkennbar sein, inwieweit die Angaben auf sicheren/exakten Berechnungen, Schätzungen bzw. Erfahrungswerten oder einem Vergleich mit ähnlichen Maßnahmen/Regelungsvorhaben beruhen. Wenn dabei Berechnungen zu den Personal- und Sachausgaben notwendig werden, ist auf die jährlich veröffentlichten <a href="https://www.bundesfinanzministerium.de/Content/DE/Standardartikel/Themen/Oeffentliche_Finanzen/Bundeshaushalt/personalkostensaetze.html" target="_blank" style="white-space: normal">Rundschreiben des BMF zu den „Personal- und Sachkosten in der Bundesverwaltung für Wirtschaftlichkeitsuntersuchungen und Kostenberechnungen“</a> zurückzugreifen.</p>
            <p>Für den Bundeshaushalt sind darüber hinaus die haushalterischen Auswirkungen getrennt nach Einzelplänen sowie unterteilt in einmalige Auswirkungen auf Einnahmen und Ausgaben sowie laufende jährliche Auswirkungen auf Einnahmen und Ausgaben aufzuführen. Veränderungen der Stellenpläne für Planstellen und Stellen sind ebenfalls darzustellen.</p>
            <p>Die haushalterischen Auswirkungen sind mindestens für den Zeitraum des Finanzplans jahresbezogen auszuweisen. Tritt die volle Wirksamkeit einer Maßnahme erst in einem Jahr außerhalb des Finanzplans ein, ist auch die Jahreswirkung im Jahr der vollen Wirksamkeit der Maßnahme aufzuführen. Es sind dabei möglichst tabellarische Formate zu wählen.</p>
            <p>Darüber hinaus ist immer anzugeben, ob und inwieweit die Veränderungen bei den Ausgaben oder Einnahmen in der mehrjährigen Finanzplanung bereits berücksichtigt sind und ob und auf welche Weise bei zusätzlichen Belastungen voraussichtlich ein Ausgleich gefunden werden kann, z. B. durch Einsparungen, Umschichtungen oder Haushaltsvorbehalte. </p>
            <p>Umfasst ein Gesetzgebungsvorhaben mehrere Änderungen, sollten die haushalterischen Auswirkungen nicht nur als eine Summe ausgewiesen, sondern auch nach Teilbereichen aufgegliedert werden. Dies betrifft auch mögliche Übergangsregelungen.</p>
            <p>Über die Gewährung eines etwaigen Mehrbedarfs an Haushaltsmitteln (Ausgaben, Verpflichtungsermächtigungen sowie Planstellen/Stellen) sowie dessen Höhe wird erst im Rahmen des kommenden Haushaltsaufstellungsverfahrens entschieden.</p>
            <p>Etwaige Auswirkungen auf weitere Bereiche des Staates, z. B. die Sozialversicherungsträger oder Sondervermögen, sollten berücksichtigt und angegeben werden.</p>
            <p>Die notwendigen Angaben unter „E. Erfüllungsaufwand“ im Vorblatt bzw. unter „Gesetzesfolgen → Erfüllungsaufwand“ in der Begründung bleiben hiervon unberührt (vgl. <a href="{{basePath}}/arbeitshilfen/download/29#page=49" target="_blank" style="white-space: normal"> Leitfaden zur Ermittlung und Darstellung des Erfüllungsaufwandes in Regelungsvorhaben der Bundesregierung, Stand Januar 2022, S. 49ff.</a>).</p>
            `,
    },
  },
  zweckausgaben: {
    linkName: 'Zweckausgaben',
    title: 'Zweckausgaben',
    drawerTitleText: `<p>Zweckausgaben sind Ausgaben, die durch die geplanten Maßnahmen selbst (für den Zweck des Verwaltungshandelns) ausgelöst werden, nicht etwa durch den begleitenden Verwaltungsvollzug. Beispiele sind Ausgaben für den Straßenbau oder Geldleistungen, die an Begünstigte gezahlt werden.</p>
                      <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                      <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                      <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question: 'Sind Auswirkungen auf die öffentlichen Haushalte durch Veränderungen bei Zweckausgaben zu erwarten?',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Ausgaben der öffentlichen Haushalte durch Änderungen bei den Zweckausgaben zu erwarten sind.</p>
                            <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                            <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen durch Zweckausgaben zu erwarten sind.</p>`,
      additionalExplanation: 'Bitte beschreiben Sie die erwarteten Auswirkungen auf die Zweckausgaben.',
      additionalExplanationDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen durch die geplanten Maßnahmen selbst (Veränderungen bei Zweckausgaben).</p>
                                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                                          <ul>
                                            <li>Es sind folgende Ausgaben zu erwarten.</li>
                                          </ul>`,
    },
    influenceSummary: {
      label: 'Bitte beschreiben Sie die erwarteten Auswirkungen auf die {{newLine}}Zweckausgaben.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen durch die geplanten Maßnahmen selbst (Veränderungen bei Zweckausgaben).</p>
                                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                                          <ul>
                                            <li>Es sind folgende Ausgaben zu erwarten:</li>
                                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Ausgaben durch Änderungen bei Zweckausgaben',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich. <br /> Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    jahreswirkung: {
      title: 'Volle Jahreswirkung',
      switchLabel: 'Angaben zur vollen Jahreswirkung machen',
      drawerSwitchLabel:
        'Tritt die volle Wirksamkeit einer Maßnahme erst in einem Jahr außerhalb des Finanzplans ein, ist auch die Jahreswirkung im Jahr der vollen Wirksamkeit der Maßnahme aufzuführen. Geben Sie hier dann bitte an, in welchem Jahr die volle Jahreswirkung erwartet wird und wie hoch sie ausfallen wird.',
      jahreswirkungSummary: {
        label: 'Angaben zur vollen Jahreswirkung',
      },
    },
    bund: {
      title: 'Bund',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    laender: {
      title: 'Länder',
    },
    gemeinden: {
      title: 'Gemeinden/Gemeindeverbände',
    },
    gesamtsumme: {
      title: 'Gesamtsumme',
      headerLabel: 'Bezeichnung der Gebietskörperschaft',
      tableRowTitles: {
        bund: 'Bund gesamt',
        laender: 'Länder gesamt',
        gemeinden: 'Gemeinden/Gemeindeverbände gesamt',
        gesamtsumme: 'Gesamtsumme',
      },
    },
  },
  vollzugsaufwand: {
    linkName: 'Ausgaben für den Verwaltungsvollzug',
    title: 'Ausgaben für den Verwaltungsvollzug',
    drawerTitleText: `<p>Ausgaben für den Verwaltungsvollzug sind Ausgaben, die für das Handeln der Verwaltung anfallen. Beispiele sind Ausgaben für Personal, Gebäude und technische Einrichtungen, die für den Vollzug der geplanten Maßnahmen entstehen.</p>
                            <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                            <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                            <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question:
        'Sind Auswirkungen auf die öffentlichen Haushalte durch Veränderungen bei den Ausgaben für den Verwaltungsvollzug zu erwarten?',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Ausgaben der öffentlichen Haushalte durch den die Maßnahmen begleitenden Verwaltungsvollzug zu erwarten sind.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen durch den Verwaltungsvollzug zu erwarten sind.</p>`,
    },
    influenceSummary: {
      label:
        'Bitte beschreiben Sie die erwarteten Auswirkungen durch Veränderungen bei den Ausgaben für den Verwaltungsvollzug.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen durch den Verwaltungsvollzug.</p>
                                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                                          <ul>
                                            <li>Im Einzelplan xx (ggf. Behörde/n benennen) fallen folgende Haushaltsausgaben an:</li>
                                            <li>In den Einzelplänen xx und xy (ggf. Behörden benennen) fallen folgende Haushaltsausgaben an:</li>
                                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Ausgaben durch den Verwaltungsvollzug',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich. <br />
                    Sofern Einzelpläne des Bundes betroffen sind, differenzieren Sie bitte pro Einzelplan zwischen einmaligen und laufenden Kosten.<br />
                    Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    bund: {
      title: 'Bund',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    laender: {
      title: 'Länder',
    },
    gemeinden: {
      title: 'Gemeinden/Gemeindeverbände',
    },
    gesamtsumme: {
      title: 'Gesamtsumme',
      headerLabel: 'Bezeichnung der Gebietskörperschaft',
      tableRowTitles: {
        bund: 'Bund gesamt',
        laender: 'Länder gesamt',
        gemeinden: 'Gemeinden/Gemeindeverbände gesamt',
        gesamtsumme: 'Gesamtsumme',
      },
    },
    planstellen: {
      influenceExpected: {
        title: 'Planstellen und Stellen',
        question: 'Sind Auswirkungen auf die Stellenpläne (Planstellen und Stellen) zu {{newLine}}erwarten?',
        questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Stellenpläne für Planstellen und Stellen zu erwarten sind, die durch den Einsatz von Personal im Rahmen des Verwaltungsvollzugs entstehen. Gemeint sind hier die im Bundeshaushalt, d. h. die in den Personalhaushalten der betroffenen Ressorteinzelpläne, ausgebrachten Planstellen und Stellen.</p>
                                <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                                <p>Klicken Sie auf „Nein“, wenn keinerlei Veränderungen der Stellenpläne zu erwarten sind.</p>`,
      },
      influenceSummary: {
        label: 'Bitte beschreiben Sie kurz den Inhalt der erwartenden Auswirkungen.',
        labelDrawer: `<p>Beschreiben Sie hier bitte die erwarteten Veränderungen der Stellenpläne durch den Einsatz von Personal im Rahmen des Verwaltungsvollzugs.</p>
                            <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                            <ul>
                              <li>Es sind folgende Veränderungen bei den Stellenplänen für Planstellen/Stellen zu erwarten:</li>
                              <li>Die zusätzlichen Planstellen/Stellen werden durch Wegfall von Planstellen/Stellen in finanziell gleichwertigem Umfang bei ... ausgeglichen.</li>
                              <li>Zusätzliche Planstellen-/Stellenbedarfe stehen unter dem Vorbehalt der Entscheidung im Haushaltsaufstellungsverfahren.</li>
                            </ul>`,
      },
      ausgabenDurch: {
        title: 'Planstellen- und Stellenbedarfe',
        text: `Bitte geben Sie hier die Veränderung der Anzahl der Stellen im Bundeshaushalt an. Soweit es zweckmäßig ist, beschreiben Sie die Wertigkeit (Eingruppierungen bzw. Besoldungsgruppen) der Stellen im Textfeld oben. <br /> Geben Sie die finanziellen Auswirkungen bitte in der obigen Tabelle zu den Ausgaben durch den Verwaltungsvollzug an.`,
      },
      tableRowTitles: {
        hoehererDienst: 'Höherer Dienst',
        gehobenerDienst: 'Gehobener Dienst',
        mittlererDienst: 'Mittlerer Dienst',
        einfacherDienst: 'Einfacher Dienst',
      },
      gesamtTable: {
        title: 'Gesamtsumme der Planstellen- und Stellenbedarfe',
        headerLabel: 'Gesamt',
        tableRowTitles: {
          hoehererDienst: 'Höherer Dienst gesamt',
          gehobenerDienst: 'Gehobener Dienst gesamt',
          mittlererDienst: 'Mittlerer Dienst gesamt',
          einfacherDienst: 'Einfacher Dienst gesamt',
        },
      },
    },
  },
  normadressat: {
    linkName: 'Ausgaben für die Verwaltung als Normadressat',
    title: 'Ausgaben für die Verwaltung als Normadressat',
    drawerTitleText: `<p>Hier sind Ausgaben gemeint, die dadurch entstehen, dass die Verwaltung von den geplanten Maßnahmen als Normadressat betroffen ist. Dies kann sich beispielsweise aus der Rolle der Verwaltung als Halterin oder Halter von Kfz, Arbeitgeberin oder Arbeitgeber außerhalb des Beamtenrechts oder Bauherrin oder Bauherr ergeben.</p>
                            <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                            <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                            <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question:
        'Sind Auswirkungen auf die öffentlichen Haushalte durch Veränderungen bei Ausgaben für die Verwaltung als Normadressat zu erwarten?',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Ausgaben der öffentlichen Haushalte durch die Verwaltung als Normadressat zu erwarten sind.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen durch die Verwaltung als Normadressat zu erwarten sind.</p>`,
    },
    influenceSummary: {
      label:
        'Bitte beschreiben Sie die erwarteten Auswirkungen durch Veränderungen bei den Ausgaben für die Verwaltung als Normadressat.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen für die Verwaltung als Normadressat.</p>
                                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                                          <ul>
                                            <li>Im Einzelplan xx (ggf. Behörde/n benennen) fallen folgende Haushaltsausgaben an:</li>
                                            <li>In den Einzelplänen xx und xy (ggf. Behörden benennen) fallen folgende Haushaltsausgaben an:</li>
                                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Ausgaben durch die Verwaltung als Normadressat',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich. <br />
                    Sofern Einzelpläne des Bundes betroffen sind, differenzieren Sie bitte pro Einzelplan zwischen einmaligen und laufenden Kosten. <br />
                    Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    normadressatRegelung: {
      title: 'normadressatRegelung',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    gesamtsumme: {
      title: 'Gesamtsumme',
      headerLabel: '',
      tableRowTitles: {
        gesamtsumme: 'Gesamtsumme',
      },
    },
    planstellen: {
      influenceExpected: {
        title: 'Planstellen und Stellen',
        question: `Sind Auswirkungen auf die Stellenpläne (Planstellen und Stellen) zu {{newLine}}erwarten?`,
        questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Stellenpläne für Planstellen und Stellen zu erwarten sind, die durch den Einsatz von Personal im Rahmen des Verwaltungsvollzugs entstehen. Gemeint sind hier die im Bundeshaushalt, d. h. die in den Personalhaushalten der betroffenen Ressorteinzelpläne, ausgebrachten Planstellen und Stellen.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Veränderungen der Stellenpläne zu erwarten sind.</p>`,
      },
      influenceSummary: {
        label: 'Bitte beschreiben Sie kurz den Inhalt der erwartenden Auswirkungen.',
        labelDrawer: `<p>Beschreiben Sie hier bitte die erwarteten Veränderungen der Stellenpläne durch den Einsatz von Personal in der Verwaltung als Normadressat.</p>
                            <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                            <ul>
                              <li>Es sind folgende Veränderungen bei den Stellenplänen für Planstellen/Stellen zu erwarten:</li>
                              <li>Die zusätzlichen Planstellen/Stellen werden durch Wegfall von Planstellen/Stellen in finanziell gleichwertigem Umfang bei … ausgeglichen.</li>
                              <li>Zusätzliche Planstellen-/Stellenbedarfe stehen unter dem Vorbehalt der Entscheidung im Haushaltsaufstellungsverfahren.</li>
                            </ul>`,
      },
      ausgabenDurch: {
        title: 'Planstellen- und Stellenbedarfe',
        text: `Bitte geben Sie hier die Veränderung der Anzahl der Stellen im Bundeshaushalt an. Soweit es zweckmäßig ist, beschreiben Sie die Wertigkeit (Eingruppierungen bzw. Besoldungsgruppen) der Stellen im Textfeld oben. <br /> Geben Sie die finanziellen Auswirkungen bitte in der obigen Tabelle zu den Ausgaben durch den Verwaltungsvollzug an.`,
      },
      tableRowTitles: {
        hoehererDienst: 'Höherer Dienst',
        gehobenerDienst: 'Gehobener Dienst',
        mittlererDienst: 'Mittlerer Dienst',
        einfacherDienst: 'Einfacher Dienst',
      },
      gesamtTable: {
        title: 'Gesamtsumme der Planstellen- und Stellenbedarfe',
        headerLabel: 'Gesamt',
        tableRowTitles: {
          hoehererDienst: 'Höherer Dienst gesamt',
          gehobenerDienst: 'Gehobener Dienst gesamt',
          mittlererDienst: 'Mittlerer Dienst gesamt',
          einfacherDienst: 'Einfacher Dienst gesamt',
        },
      },
    },
  },
  verwaltungsUndSonstige: {
    linkName: 'Verwaltungseinnahmen und sonstige Einnahmen',
    title: 'Verwaltungseinnahmen und sonstige Einnahmen',
    drawerTitleText: `<p>Verwaltungseinnahmen und sonstige Einnahmen sind Einnahmen, die sich nicht aus Steuern ergeben. Hierunter fallen beispielsweise Gebühren oder Einnahmen aus der Veräußerung von Vermögensgegenständen.</p>
                            <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                            <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                            <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question:
        'Sind Auswirkungen auf die öffentlichen Haushalte durch Veränderungen bei den Verwaltungseinnahmen und sonstigen Einnahmen zu erwarten?',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Einnahmen der öffentlichen Haushalte durch Veränderungen bei den Verwaltungseinnahmen und sonstigen Einnahmen zu erwarten sind.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen durch Verwaltungseinnahmen und sonstige Einnahmen zu erwarten sind.</p>`,
    },
    influenceSummary: {
      label:
        'Bitte beschreiben Sie kurz den Inhalt der erwarteten Auswirkungen bei den Verwaltungseinnahmen und sonstigen Einnahmen.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen bei den Verwaltungseinnahmen und sonstigen Einnahmen.</p>
                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                          <ul>
                            <li>Es sind folgende Einnahmen zu erwarten:</li>
                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Verwaltungseinnahmen und sonstige Einnahmen',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich.<br /> Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    jahreswirkung: {
      title: 'Volle Jahreswirkung',
      switchLabel: 'Angaben zur vollen Jahreswirkung machen',
      drawerSwitchLabel:
        'Tritt die volle Wirksamkeit einer Maßnahme erst in einem Jahr außerhalb des Finanzplans ein, ist auch die Jahreswirkung im Jahr der vollen Wirksamkeit der Maßnahme aufzuführen. Geben Sie hier dann bitte an, in welchem Jahr die volle Jahreswirkung erwartet wird und wie hoch sie ausfallen wird.',
      jahreswirkungSummary: {
        label: 'Angaben zur vollen Jahreswirkung',
      },
    },
    bund: {
      title: 'Bund',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    laender: {
      title: 'Länder',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    gemeinden: {
      title: 'Gemeinden/Gemeindeverbände',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    gesamtsumme: {
      title: 'Gesamtsumme',
      headerLabel: 'Bezeichnung der Gebietskörperschaft',
      tableRowTitles: {
        bund: 'Bund gesamt',
        laender: 'Länder gesamt',
        gemeinden: 'Gemeinden/Gemeindeverbände gesamt',
        gesamtsumme: 'Gesamtsumme',
      },
    },
  },
  aenderungenSteuereinnahmen: {
    linkName: 'Steuereinnahmen',
    title: 'Steuereinnahmen',
    drawerTitleText: `<p>Mit Veränderungen bei Steuereinnahmen sind hier alle Mehr- oder Mindersteuereinnahmen bei Bund, Ländern oder Gemeinden/Gemeindeverbänden gemeint, die durch die geplanten Maßnahmen zu erwarten sind.</p>
                            <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                            <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                            <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question:
        'Sind Auswirkungen auf die öffentlichen Haushalte durch Veränderungen bei den Steuereinnahmen zu erwarten',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Einnahmen der öffentlichen Haushalte durch Veränderungen bei Steuereinnahmen zu erwarten sind.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen bei Steuereinnahmen zu erwarten sind.</p>`,
    },
    influenceSummary: {
      label: 'Bitte beschreiben Sie die erwarteten Auswirkungen durch Veränderungen bei Steuereinnahmen.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen durch Veränderungen bei Steuereinnahmen.</p>
                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                          <ul>
                            <li>Es sind folgende Mehr- bzw. Mindereinnahmen zu erwarten:</li>
                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Änderungen bei Steuereinnahmen',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich.<br /> Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    jahreswirkung: {
      title: 'Volle Jahreswirkung',
      switchLabel: 'Angaben zur vollen Jahreswirkung machen',
      drawerSwitchLabel:
        'Tritt die volle Wirksamkeit einer Maßnahme erst in einem Jahr außerhalb des Finanzplans ein, ist auch die Jahreswirkung im Jahr der vollen Wirksamkeit der Maßnahme aufzuführen. Geben Sie hier dann bitte an, in welchem Jahr die volle Jahreswirkung erwartet wird und wie hoch sie ausfallen wird.',
      jahreswirkungSummary: {
        label: 'Angaben zur vollen Jahreswirkung',
      },
    },
    bund: {
      title: 'Bund',
      headerLabel: 'Bezeichnung des Einzelplans',
    },
    laender: {
      title: 'Länder',
    },
    gemeinden: {
      title: 'Gemeinden/Gemeindeverbände',
    },
    gesamtsumme: {
      title: 'Gesamtsumme',
      headerLabel: 'Bezeichnung der Gebietskörperschaft',
      tableRowTitles: {
        bund: 'Bund gesamt',
        laender: 'Länder gesamt',
        gemeinden: 'Gemeinden/Gemeindeverbände gesamt',
        gesamtsumme: 'Gesamtsumme',
      },
    },
  },
  sonstigerBereicheStaates: {
    linkName: 'Ausgaben und Einnahmen weiterer Bereiche des Staates',
    title: 'Ausgaben und Einnahmen weiterer Bereiche des Staates',
    drawerTitleText: `<p>Gemeint sind alle Bereiche des Staates, die nicht bereits von der Darstellung der Auswirkungen auf Bund, Länder und Gemeinden erfasst sind. Sozialversicherungsträger und Sondervermögen dürften in der Praxis die wichtigsten Bereiche sein, die ggf. zusätzlich dargestellt werden sollten.</p>
                            <p>Haushaltswirkungen müssen so genau ermittelt werden, wie es unter Zugrundelegung eines vernünftigen Kosten-Nutzen-Verhältnisses möglich ist.</p>
                            <p>Haushaltswirkungen, die den Kern der jeweiligen gesetzlichen Regelung betreffen, sind möglichst detailliert darzustellen. Das gilt insbesondere, wenn die Haushaltswirkungen absehbar im Mittelpunkt der Gesetzeserörterung stehen werden.  Basieren kann die Ermittlung der Haushaltswirkungen u. a. auf vorhandenen Statistiken, eigenen Modellrechnungen oder Auswertungen externer Untersuchungen. Die methodisch entscheidenden Annahmen sollten in diesem Zusammenhang dargestellt werden.</p>
                            <p>Kann eine Berechnung der Haushaltswirkungen nicht durchgeführt werden, weil z. B. die erforderlichen Rechengrundlagen fehlen, sollte versucht werden, Spannbreiten oder Ober- bzw. Untergrenzen zu nennen. Können die Haushaltswirkungen nicht berechnet und auch nicht durch Angabe von Ober- und/oder Untergrenzen angegeben werden, sollte modellhaft mit Faustgrößen oder Szenarien gearbeitet werden. Sind die vorgenannten Darstellungsweisen nicht möglich, sollten Haushaltswirkungen zumindest der Tendenz nach beschrieben und die dafür maßgeblichen Umstände benannt werden.</p>`,
    influenceExpected: {
      title: 'Angaben zu den Auswirkungen',
      question:
        'Hat das Regelungsvorhaben Auswirkungen auf die Einnahmen oder Ausgaben weiterer Bereiche des Staates (z.B. Sozialversicherungsträger, Sondervermögen)?',
      questionDrawer: `<p>Klicken Sie hier auf „Ja“, wenn Auswirkungen auf die Einnahmen oder Ausgaben weiterer Bereiche des Staates zu erwarten sind.</p>
                              <p>Zum Ausfüllen stellen wir Ihnen dann passende Tabellenvorlagen zur Verfügung.</p>
                              <p>Klicken Sie auf „Nein“, wenn keinerlei Auswirkungen auf Einnahmen oder Ausgaben weiterer Bereiche des Staates zu erwarten sind.</p>`,
    },
    influenceSummary: {
      label:
        'Bitte beschreiben Sie kurz den Inhalt der erwarteten Auswirkungen auf die Einnahmen oder Ausgaben weiterer Bereiche des Staates.',
      labelDrawer: `<p>Beschreiben Sie hier bitte die Auswirkungen auf die Einnahmen oder Ausgaben weiterer Bereiche des Staates.</p>
                          <p>Hierzu können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                          <ul>
                            <li>Es sind folgende Einnahmen zu erwarten:</li>
                            <li>Es sind folgende Ausgaben zu erwarten:</li>
                          </ul>`,
    },
    ausgabenDurch: {
      title: 'Ausgaben und Einnahmen weiterer Bereiche des Staates',
      text: `Bitte beschreiben Sie die Auswirkungen des Regelungsvorhabens auf die öffentlichen Haushalte. Differenzieren Sie dabei nach Gebietskörperschaften. Für die verschiedenen Arten von Einnahmen und Ausgaben, die Sie im Hinblick auf ihr Regelungsvorhaben als relevant eingestuft haben, stehen Ihnen jeweils Tabellenvorlagen zur Verfügung. Ein einfaches Hinzufügen und Entfernen weiterer Abschnitte ist jederzeit möglich. <br /> Die Angaben in den Datenfeldern sind in T€ vorzunehmen.`,
    },
    jahreswirkung: {
      title: 'Volle Jahreswirkung',
      switchLabel: 'Angaben zur vollen Jahreswirkung machen',
      drawerSwitchLabel:
        'Tritt die volle Wirksamkeit einer Maßnahme erst in einem Jahr außerhalb des Finanzplans ein, ist auch die Jahreswirkung im Jahr der vollen Wirksamkeit der Maßnahme aufzuführen. Geben Sie hier dann bitte an, in welchem Jahr die volle Jahreswirkung erwartet wird und wie hoch sie ausfallen wird. ',
      jahreswirkungSummary: {
        label: 'Angaben zur vollen Jahreswirkung',
      },
    },
    ausgaben: {
      title: 'Ausgaben weiterer Bereiche',
      headerLabel: 'Bezeichnung des Bereichs',
      addRowLabel: 'Bereich hinzufügen',
      removeRowLabel: 'Bereich entfernen',
      tableRowTitles: {
        gesamtsumme: 'Summe',
      },
    },
    gesamtsummeAusgaben: {
      title: 'Gesamtsumme: Ausgaben weiterer Bereiche',
      tableRowTitles: {
        gesamtsumme: 'Gesamtsumme',
      },
    },
    einnahmen: {
      title: 'Einnahmen weiterer Bereiche',
      headerLabel: 'Bezeichnung des Bereichs',
      addRowLabel: 'Bereich hinzufügen',
      removeRowLabel: 'Bereich entfernen',
      tableRowTitles: {
        gesamtsumme: 'Summe',
      },
    },
    gesamtsummeEinnahmen: {
      title: 'Gesamtsumme: Einnahmen weiterer Bereiche',
      tableRowTitles: {
        gesamtsumme: 'Gesamtsumme',
      },
    },
  },
  kompensationKosten: {
    linkName: 'Geplante Kompensation der Kosten',
    title: 'Geplante Kompensation der Kosten',
    ausgabenDurch: {
      title: 'Angaben zur geplanten Kompensation der Kosten',
      text: `Bitte geben Sie hier an, ob und inwieweit die Veränderungen bei den Ausgaben oder Einnahmen in der mehrjährigen Finanzplanung bereits berücksichtigt sind und ob und auf welche Weise bei zusätzlichen Belastungen voraussichtlich ein Ausgleich gefunden werden kann, z. B. durch Einsparungen, Umschichtungen oder Haushaltsvorbehalte.`,
    },
    influenceSummary: {
      label: 'Bitte beschreiben Sie die geplante Kompensation der Kosten.',
      labelDrawer: `<p>Zur Beschreibung können Sie zum Beispiel auf folgende Textbausteine zurückgreifen:</p>
                          <p>Wenn eine Kompensation teilweise geplant ist: „... Im Übrigen sind etwaige Mehrbedarfe an Sach- und Personalmitteln finanziell und stellenmäßig im jeweiligen Einzelplan auszugleichen.“</p>
                          <p>Wenn keine Kompensation geplant ist: „Etwaige Mehrbedarfe an Sach- und Personalmitteln sollen finanziell und stellenmäßig im jeweiligen Einzelplan ausgeglichen werden.“</p>`,
    },
  },
  zusammenfassung: {
    linkName: 'Zusammenfassung',
    title: 'Zusammenfassung',
    text: 'Hier finden Sie Ihre Ausarbeitungen aus den einzelnen Bereichen. Fassen Sie diese unten noch einmal zusammen. Anschließend können Sie beide Ergebnisteile in den Regelungsentwurf übernehmen: die Ausarbeitungen aus den einzelnen Bereichen in den Begründungsteil und die Zusammenfassung in das Vorblatt.',
    auswirkungenEinzelnenBereicheTitle: 'Begründung - Allgemeiner Teil',
    onlyGesamtsummenTabels: {
      switchLabel: 'Nur die Tabellen mit Gesamtsummen anzeigen',
      drawerSwitchLabel:
        'Aktivieren Sie diese Funktion, um sich nur die Tabellen anzeigen zu lassen, die Gesamtsummen beinhalten. Möchten Sie wieder sämtliche Berechnungstabellen sehen, deaktivieren Sie die Funktion.',
    },
    keineAuswirkungen: 'Keine Auswirkungen',
  },
  vorblatt: {
    title: 'Vorblatt',
    haushaltsausgabenTitle: 'D. Haushaltsausgaben ohne Erfüllungsaufwand',
    influenceSummary: {
      label: 'Bitte fassen Sie hier Ihre Ergebnisse zusammen.',
      labelDrawer: `<p>Fassen Sie Ihre Ergebnisse hier bitte für die Darstellung unter Buchstabe D. des Vorblatts zusammen. Für die Zusammenfassung der Ergebnisse in einer Tabelle steht Ihnen eine Vorlage zur Verfügung. Berechnen Sie bitte die Summen der Ausgaben und Einnahmen auf Ebene des Bundes, der Länder und der Gemeinen/Gemeindeverbände sowie die Gesamtsummen der Ausgaben und Einnahmen. Geben Sie die Auswirkungen für den Bundeshaushalt bitte getrennt nach Einzelplänen an.</p>`,
    },
    ausgaben: {
      title: 'Ausgaben',
      bund: {
        headerLabel: 'Bund',
      },
      laender: {
        headerLabel: 'Länder',
      },
      gemeinden: {
        headerLabel: 'Gemeinden/Gemeindeverbände',
      },
      gesamtsumme: {
        headerLabel: 'Gesamtsumme',
      },
    },
    einnahmen: {
      title: 'Einnahmen',
      bund: {
        headerLabel: 'Bund',
      },
      laender: {
        headerLabel: 'Länder',
      },
      gemeinden: {
        headerLabel: 'Gemeinden/Gemeindeverbände',
      },
      gesamtsumme: {
        headerLabel: 'Gesamtsumme',
      },
    },
  },
  fertigstellung: {
    linkName: 'Fertigstellung',
    title: 'Fertigstellung',
    infoParagraph:
      'Die Angaben auf dieser Seite können Sie im Anschluss an die Fertigstellung in den Begründungsteil und in das Vorblatt des Regelungsentwurfs übernehmen.',
    generalTitlePart: 'Vorblatt',
    haushaltsausgabenTitle: 'A. VI. 3 Haushaltsausgaben ohne Erfüllungsaufwand',
  },
  anzugebendeJahre: {
    title: 'Anzugebende Jahre wählen',
    drawerTitleText: `<p>Wählen Sie hier den Zeitraum an Jahren aus, den Sie in Ihren Berechnungen berücksichtigen.</p>
                          <p>Die haushalterischen Auswirkungen sind mindestens für den Zeitraum des Finanzplans jahresbezogen auszuweisen. Dieser umfasst einen Zeitraum von fünf Jahren, beginnend mit dem laufenden Haushaltsjahr.</p>`,
    jahrVon: {
      label: 'Startjahr (jjjj)',
    },
    jahrBis: {
      label: 'Endjahr (jjjj)',
    },
    confirm: {
      label:
        'Wenn Sie die Jahreszahlen ändern, verändert sich entsprechend Ihre Tabelle und Ihre Eintragungen gehen verloren. Möchten Sie die Jahreszahlen jetzt ändern?',
      btnOk: 'Ja',
      btnCancel: 'Nein',
    },
    errorMinRange: 'Bitte wählen Sie einen Zeitraum zwischen 4 und 8 Jahren aus.',
    errorMaxRange: 'Bitte wählen Sie einen Zeitraum zwischen 4 und 8 Jahren aus.',
    errorShouldBeNumber: 'Bitte wählen Sie gültige Jahreszahl (Format: JJJJ) ein.',
    errorDateOutOfRangeStart: 'Das Startjahr muss vor dem Endjahr liegen.',
    errorDateOutOfRangeEnd: 'Das Endjahr muss nach dem Startjahr liegen.',
  },
  tableWidget: {
    addRow: 'Einzelplan hinzufügen',
    regelungLabel: 'Bezeichnung der Regelung',
    regelungUndEinzelplans: 'Bezeichnung der Regelung',
  },
  contactPerson: de_contacts.einnahmenAusgaben.contactPerson,
};
