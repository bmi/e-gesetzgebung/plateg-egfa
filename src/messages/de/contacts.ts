// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de_contacts = {
  demografie: {
    contactPerson: {
      email: 'mail@example.com',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  disability: {
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  einnahmenAusgaben: {
    contactPerson: {
      email: 'mail@example.com',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  enap: {
    contactPerson: {
      email: 'mail@example.com',
      content: `<p> 
            <strong>Fachliche Fragen zur Deutschen Nachhaltigkeitsstrategie</strong> <br>
            Bundeskanzleramt <br>
            Beispielreferat <br>
            Nachhaltige Entwicklung <br>
            Willy-Brandt-Str. 1 <br>
            10557 Berlin <br>
            E-Mail: <a href="mailto:mail@example.com">mail@example.com</a> <br>
            </p>`,
    },
  },
  erfuellungsaufwand: {
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      abteilung: 'Abteilung 1',
      fachreferat: 'Beispielreferat'
    },
  },
  evaluierung: {
    ausgestaltungDerEvaluierung: {
      intro3_email: 'mail@example.com',
    },
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      abteilung: 'Abteilung 1',
      fachreferat: 'Beispielreferat'
    },
  },
  gleichstellung: {
    contactPerson: {
      email: 'mail@example.com',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  gleichwertigkeit: {
    contactPerson: {
      email: 'mail@example.com',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  kmu: {
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      fachreferat: 'Beispielreferat',
      abteilung:
        'Abteilung 1',
      additionalInfo: `<p>Bei Fragen zur Methodik der Schätzung des Erfüllungsaufwands wenden Sie sich bitte an das Statistische Bundesamt unter der Mailadresse: <a href="mailto:mail@example.com">mail@example.com</a></p>`,
    },
  },
  preise: {
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1',
    },
  },
  sonstige: {
    contactPerson: {
      email: 'support.egesetzgebung@bmi.bund.de',
    },
  },
  verbraucher: {
    contactPerson: {
      email: 'mail@example.com',
      name: 'Martina Musterfrau',
      anrede: 'Frau',
      fachreferat: 'Beispielreferat',
      abteilung: 'Abteilung 1'
    },
  },
  weitere: {
    contactPerson: {
      email: 'support.egesetzgebung@bmi.bund.de',
    },
  },
  experimentierklauseln: {
    contactPerson: {
      email: 'mail@example.com',
      fachreferat: 'Beispielreferat',
      ressort: 'Ressort 1',
      additionalInfo:
        '<p>Weitere Informationen zu Reallaboren und Experimentierklauseln finden Sie unter <a href="https://www.bmwk.de/Redaktion/DE/Dossier/reallabore-testraeume-fuer-innovation-und-regulierung.html" target="_blank">Reallabore – Testräume für Innovation und Regulierung</a></p>',
    },
  },
};
